package com.trustaml.service.adversemedia.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.adversemedia.dao.AdverseMediaDao;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Path("/adverse-media")
public class AdverseMediaRestfulService {

	@Inject
	AdverseMediaDao adverseMediaDao;

	/**
	 * @param json
	 * @description save adverse media object
	 * @return save successful
	 * @error errorCode
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveAdverseMedia(String json) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!json.isEmpty()) {
			adverseMediaDao.insertAdverseMedia(json);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}

	}

	/**
	 * @param json
	 * @description update adverse media object
	 * @return update successful
	 * @error errorCode
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAdverseMedia(String json) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!json.isEmpty()) {
			adverseMediaDao.updateAdverseMedia(json);
			return ResponseReturn.sucess("update successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchAdverseData() throws IllegalArgumentException, SQLException, ParseException,
			JsonParseException, JsonMappingException, IOException {

		return ResponseReturn.sucess(adverseMediaDao.fetchAdverseData(new ScreeningOfMigratedData()));
	}
}
