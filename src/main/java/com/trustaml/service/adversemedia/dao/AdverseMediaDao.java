package com.trustaml.service.adversemedia.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.adversemedia.model.AdverseAddressInfo;
import com.trustaml.service.adversemedia.model.AdverseAttachment;
import com.trustaml.service.adversemedia.model.AdverseFamilyInfo;
import com.trustaml.service.adversemedia.model.AdverseMedia;
import com.trustaml.service.adversemedia.model.AdverseOfficeInfo;
import com.trustaml.service.adversemedia.model.PersonalInfo;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantSanctionType;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Stateless
public class AdverseMediaDao {
	@Inject
	AdverseMediaDaoImpl adverseMediaDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ConstantEntity constantEntity;

	/* INSERT using jsonString */
	/**
	 * 
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public void insertAdverseMedia(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, ParseException, NoSuchAlgorithmException {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		AdverseMedia adverseMedia = new ObjectMapper().readValue(jsonString, AdverseMedia.class);
		String pepPersonalInfoHashValue = hashCodeGenerator.generateHashValueForAdverseMediaPersonal(
				adverseMedia.getPersonalInfo(), adverseMedia.getUser(), includeUser);

		Long adverseId = adverseMediaDaoImpl.savePersonalInfo(adverseMedia.getPersonalInfo(), pepPersonalInfoHashValue);
		adverseMedia.getPersonalInfo().setId(adverseId);

		String pepPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseMediaPersonal(
				adverseMedia.getPersonalInfo(), adverseMedia.getUser(), doNotIncludeUser);
		adverseMediaDaoImpl.savePersonalInfoUpdateTable(adverseMedia.getPersonalInfo(), adverseMedia.getUser(),
				pepPersonalInfoUpdateHashValue);
		for (AdverseOfficeInfo adverseOfficeInfo : adverseMedia.getAdverseOfficeInfo()) {

			String adverseOfficeHashValue = hashCodeGenerator.generateHashValueForAdverseOffice(adverseOfficeInfo,
					adverseId, adverseMedia.getUser(), includeUser);
			String adverseOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseOffice(adverseOfficeInfo,
					adverseId, adverseMedia.getUser(), doNotIncludeUser);
			adverseMediaDaoImpl.insertAdverseOfficeInfo(adverseOfficeInfo, adverseId, adverseOfficeHashValue);
			adverseMediaDaoImpl.insertAdverseOfficeInfoUpdateTable(adverseOfficeInfo, adverseId, adverseMedia.getUser(),
					adverseOfficeUpdateHashValue);

		}
		if (adverseMedia.getAdverseFamilyInfo().size() > 0) {
			for (AdverseFamilyInfo adressFamilyInfo : adverseMedia.getAdverseFamilyInfo()) {
				String adverseFamilyHashValue = hashCodeGenerator.generateHashValueForAdverseFamilyInfo(
						adressFamilyInfo, adverseId, adverseMedia.getUser(), includeUser);
				String adverseFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseFamilyInfo(
						adressFamilyInfo, adverseId, adverseMedia.getUser(), doNotIncludeUser);
				adverseMediaDaoImpl.insertAdverseFamilyInfo(adressFamilyInfo, adverseId, adverseFamilyHashValue);
				adverseMediaDaoImpl.insertAdverseFamilyInfoUpdateTable(adressFamilyInfo, adverseId,
						adverseMedia.getUser(), adverseFamilyUpdateHashValue);
			}
		}
		if (adverseMedia.getAdverseAddressInfo().size() > 0) {
			for (AdverseAddressInfo adverseMediaAddressInfo : adverseMedia.getAdverseAddressInfo()) {
				String adverseAddressHashValue = hashCodeGenerator.generateHashValueForAdverseAddress(
						adverseMediaAddressInfo, adverseId, adverseMedia.getUser(), includeUser);
				String adverseAddressUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseAddress(
						adverseMediaAddressInfo, adverseId, adverseMedia.getUser(), doNotIncludeUser);
				adverseMediaDaoImpl.insertAdverseAddressInfo(adverseMediaAddressInfo, adverseId,
						adverseAddressHashValue);
				adverseMediaDaoImpl.insertAdverseAddressInfoUpdateTable(adverseMediaAddressInfo, adverseId,
						adverseMedia.getUser(), adverseAddressUpdateHashValue);
			}
		}
		if (adverseMedia.getAttachment().size() > 0) {
			for (AdverseAttachment adverseMediaInfo : adverseMedia.getAttachment()) {
				String adverseMediaHashValue = hashCodeGenerator.generateHashValueForMedia(adverseMediaInfo, adverseId,
						adverseMedia.getUser(), includeUser);
				String adverseMediaUpdateHashValue = hashCodeGenerator.generateHashValueForMedia(adverseMediaInfo,
						adverseId, adverseMedia.getUser(), doNotIncludeUser);
				adverseMediaDaoImpl.insertAdverseMediaInfo(adverseMediaInfo, adverseId, adverseMediaHashValue);
				adverseMediaDaoImpl.insertAdverseMediaInfoUpdateTable(adverseMediaInfo, adverseId,
						adverseMedia.getUser(), adverseMediaUpdateHashValue);
			}
		}
	}

	/**
	 * 
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws SQLException
	 * @throws IOException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public void updateAdverseMedia(String jsonString) throws JsonParseException, JsonMappingException, SQLException,
			IOException, ParseException, NoSuchAlgorithmException {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;

		AdverseMedia adverseMedia = new ObjectMapper().readValue(jsonString, AdverseMedia.class);
		String adversePersonalInfoHashValue = hashCodeGenerator.generateHashValueForAdverseMediaPersonal(
				adverseMedia.getPersonalInfo(), adverseMedia.getUser(), includeUser);
		if (adverseMedia.getPersonalInfo().isChange()) {
			adverseMediaDaoImpl.updatePersonalInfo(adverseMedia.getPersonalInfo());
			adverseMediaDaoImpl.savePersonalInfoUpdateTable(adverseMedia.getPersonalInfo(), adverseMedia.getUser(),
					adversePersonalInfoHashValue);
		}
		long adverseId = adverseMedia.getPersonalInfo().getId();
		for (AdverseOfficeInfo adverseOfficeInfo : adverseMedia.getAdverseOfficeInfo()) {
			String adverseOfficeHashValue = hashCodeGenerator.generateHashValueForAdverseOffice(adverseOfficeInfo,
					adverseId, adverseMedia.getUser(), includeUser);
			String adverseOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseOffice(adverseOfficeInfo,
					adverseId, adverseMedia.getUser(), doNotIncludeUser);
			if (adverseOfficeInfo.getId() != 0) {
				if (adverseOfficeInfo.isChange()) {
					adverseMediaDaoImpl.updateAdverseOfficeInfo(adverseOfficeInfo, adverseId);
					adverseMediaDaoImpl.insertAdverseOfficeInfoUpdateTable(adverseOfficeInfo, adverseId,
							adverseMedia.getUser(), adverseOfficeUpdateHashValue);
				}

			} else {
				adverseMediaDaoImpl.insertAdverseOfficeInfo(adverseOfficeInfo, adverseId, adverseOfficeHashValue);
				adverseMediaDaoImpl.insertAdverseOfficeInfoUpdateTable(adverseOfficeInfo, adverseId,
						adverseMedia.getUser(), adverseOfficeUpdateHashValue);
			}

		}
		if (adverseMedia.getAdverseFamilyInfo().size() > 0) {
			for (AdverseFamilyInfo familyInfo : adverseMedia.getAdverseFamilyInfo()) {
				String adverseFamilyHashValue = hashCodeGenerator.generateHashValueForAdverseFamilyInfo(familyInfo,
						adverseId, adverseMedia.getUser(), includeUser);
				String adverseFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseFamilyInfo(
						familyInfo, adverseId, adverseMedia.getUser(), doNotIncludeUser);
				if (familyInfo.getId() != 0) {
					if (familyInfo.isChange()) {
						adverseMediaDaoImpl.updateAdverseFamilyInfo(familyInfo, adverseId);
						adverseMediaDaoImpl.insertAdverseFamilyInfoUpdateTable(familyInfo, adverseId,
								adverseMedia.getUser(), adverseFamilyUpdateHashValue);
					}
				} else {
					adverseMediaDaoImpl.insertAdverseFamilyInfo(familyInfo, adverseId, adverseFamilyHashValue);
					adverseMediaDaoImpl.insertAdverseFamilyInfoUpdateTable(familyInfo, adverseId,
							adverseMedia.getUser(), adverseFamilyUpdateHashValue);
				}
			}
		}

		if (adverseMedia.getAdverseAddressInfo().size() > 0) {
			for (AdverseAddressInfo adverseAddressInfo : adverseMedia.getAdverseAddressInfo()) {
				String adverseAddressHashValue = hashCodeGenerator.generateHashValueForAdverseAddress(
						adverseAddressInfo, adverseId, adverseMedia.getUser(), includeUser);
				String adverseAddressUpdateHashValue = hashCodeGenerator.generateHashValueForAdverseAddress(
						adverseAddressInfo, adverseId, adverseMedia.getUser(), doNotIncludeUser);
				if (adverseAddressInfo.getId() != 0) {
					if (adverseAddressInfo.isChange()) {
						adverseMediaDaoImpl.updateAdverseAddressInfo(adverseAddressInfo, adverseId);
						adverseMediaDaoImpl.insertAdverseAddressInfoUpdateTable(adverseAddressInfo, adverseId,
								adverseMedia.getUser(), adverseAddressUpdateHashValue);
					}
				} else {
					adverseMediaDaoImpl.insertAdverseAddressInfo(adverseAddressInfo, adverseId,
							adverseAddressHashValue);
					adverseMediaDaoImpl.insertAdverseAddressInfoUpdateTable(adverseAddressInfo, adverseId,
							adverseMedia.getUser(), adverseAddressUpdateHashValue);
				}
			}
		}

		if (adverseMedia.getAttachment().size() > 0) {
			for (AdverseAttachment adverseMediaInfo : adverseMedia.getAttachment()) {

				String adverseMediaHashValue = hashCodeGenerator.generateHashValueForMedia(adverseMediaInfo, adverseId,
						adverseMedia.getUser(), includeUser);
				String adverseMediaUpdateHashValue = hashCodeGenerator.generateHashValueForMedia(adverseMediaInfo,
						adverseId, adverseMedia.getUser(), doNotIncludeUser);
				if (adverseMediaInfo.getId() != 0) {
					if (adverseMediaInfo.isChange()) {
						adverseMediaDaoImpl.updateAdverseMediaInfo(adverseMediaInfo, adverseId);
						adverseMediaDaoImpl.insertAdverseMediaInfoUpdateTable(adverseMediaInfo, adverseId,
								adverseMedia.getUser(), adverseMediaUpdateHashValue);
					}
				} else
					adverseMediaDaoImpl.insertAdverseMediaInfo(adverseMediaInfo, adverseId, adverseMediaHashValue);
				adverseMediaDaoImpl.insertAdverseMediaInfoUpdateTable(adverseMediaInfo, adverseId,
						adverseMedia.getUser(), adverseMediaUpdateHashValue);
			}
		}

	}

	public List<PersonalInfo> fetchAdverseData(ScreeningOfMigratedData adverseDataByDate)
			throws IllegalArgumentException, SQLException, ParseException, JsonParseException, JsonMappingException,
			IOException {
		String newUniqueKey = "";
		ObjectMapper mapper = new ObjectMapper();
		String existingUniqueKey = "";
		boolean generateNewKey = true;
		existingUniqueKey = adverseMediaDaoImpl.getLatestUniqueKey();
		List<PersonalInfo> listPersonalInfo = mapper.readValue(adverseMediaDaoImpl.fetchAdverseData(adverseDataByDate),
				new TypeReference<List<PersonalInfo>>() {
				});
		if (!constantEntity.isCountryCodeSet()) {
			constantEntity.setMapForCountryCode();
			constantEntity.convertListToMapForCountryCode();
		}
		for (PersonalInfo personalInfo : listPersonalInfo) {

			String CountryCode = constantEntity.getCodeFromMapCountryCode(personalInfo.getJurisdiction());
			existingUniqueKey = newUniqueKey;
			if (generateNewKey) {
				newUniqueKey = constantEntity.createNewKey(CountryCode, ConstantSanctionType.ADVERSEMEDIA);
				generateNewKey = false;
			} else {
				existingUniqueKey = newUniqueKey;
				newUniqueKey = constantEntity.generateNewKey(existingUniqueKey, CountryCode);
			}

			adverseMediaDaoImpl.updateAdversePersonalInfo(personalInfo, newUniqueKey);
		}
		return listPersonalInfo;
	}

}
