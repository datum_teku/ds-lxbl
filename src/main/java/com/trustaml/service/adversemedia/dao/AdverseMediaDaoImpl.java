package com.trustaml.service.adversemedia.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.adversemedia.model.AdverseAddressInfo;
import com.trustaml.service.adversemedia.model.AdverseAttachment;
import com.trustaml.service.adversemedia.model.AdverseFamilyInfo;
import com.trustaml.service.adversemedia.model.AdverseOfficeInfo;
import com.trustaml.service.adversemedia.model.PersonalInfo;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

public class AdverseMediaDaoImpl {
	@Inject
	DBConnection dbConnection;

	/**
	 * 
	 * @param personalInfo
	 * @param adversePersonalInfoHashValue
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public Long savePersonalInfo(PersonalInfo personalInfo, String adversePersonalInfoHashValue)
			throws SQLException, ParseException {
		long personalInfoId = 0;
		String sql = "INSERT INTO list_adverse_media_personal_info (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getAdverseCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setString(25, adversePersonalInfoHashValue);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				personalInfoId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}
		return personalInfoId;
	}

	/**
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * 
	 * @param personalInfo
	 * @param user
	 * @param adversePersonalInfoUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void savePersonalInfoUpdateTable(PersonalInfo personalInfo, User user,
			String adversePersonalInfoUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_adverse_media_personal_info_update (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,maker,checker,reason,list_adverse_media_personal_info_id,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getAdverseCategory());
			ps.setString(19, user.getUserName());
			ps.setString(20, user.getUserName());
			ps.setString(21, "No Reason");
			ps.setLong(22, personalInfo.getId());
			ps.setString(23, personalInfo.getReferenceNo());
			ps.setString(24, personalInfo.getApprovedBy());
			ps.setString(25, personalInfo.getCorrespondent());
			ps.setString(26, personalInfo.getPanNumber());
			ps.setString(27, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(28, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(28, java.sql.Types.DATE);
			}
			ps.setString(29, adversePersonalInfoUpdateHashValue);
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseOfficeInfo
	 * @param adverseId
	 * @param adverseOfficeHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertAdverseOfficeInfo(AdverseOfficeInfo adverseOfficeInfo, Long adverseId,
			String adverseOfficeHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_adverse_media_office_info (list_adverse_media_personal_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,document_code,hash) VALUES(?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseOfficeInfo.getDesignation());
			ps.setString(3, adverseOfficeInfo.getOfficeName());
			ps.setLong(4, adverseOfficeInfo.getProlongation());
			ps.setString(5, adverseOfficeInfo.getNotes());
			ps.setLong(6, adverseOfficeInfo.getTermYears());
			if (!adverseOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(adverseOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, adverseOfficeInfo.getPepOfficeStatus());
			ps.setString(9, adverseOfficeInfo.getAdverseCategory());
			ps.setString(10, adverseOfficeHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseOfficeInfo
	 * @param adverseId
	 * @param user
	 * @param adverseOfficeUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertAdverseOfficeInfoUpdateTable(AdverseOfficeInfo adverseOfficeInfo, Long adverseId, User user,
			String adverseOfficeUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_adverse_media_office_info_update"
				+ "(list_adverse_media_personal_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,maker,checker,reason,document_code,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseOfficeInfo.getDesignation());
			ps.setString(3, adverseOfficeInfo.getOfficeName());
			ps.setLong(4, adverseOfficeInfo.getProlongation());
			ps.setString(5, adverseOfficeInfo.getNotes());
			ps.setLong(6, adverseOfficeInfo.getTermYears());
			if (!adverseOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(adverseOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, adverseOfficeInfo.getPepOfficeStatus());
			ps.setString(9, user.getUserName());
			ps.setString(10, user.getUserName());
			ps.setString(11, "no reason");
			ps.setString(12, adverseOfficeInfo.getAdverseCategory());
			ps.setString(13, adverseOfficeUpdateHashValue);
			ps.execute();

		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseFamilyInfo
	 * @param adverseId
	 * @param adverseFamilyHashValue
	 * @throws SQLException
	 */
	public void insertAdverseFamilyInfo(AdverseFamilyInfo adverseFamilyInfo, Long adverseId,
			String adverseFamilyHashValue) throws SQLException {
		String sql = "INSERT INTO list_adverse_media_family_info (list_adverse_media_personal_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseFamilyInfo.getRelationshipTo());
			ps.setString(3, adverseFamilyInfo.getFirstName());
			ps.setString(4, adverseFamilyInfo.getMiddleName());
			ps.setString(5, adverseFamilyInfo.getLastName());
			ps.setString(6, adverseFamilyInfo.getLsfName());
			ps.setString(7, adverseFamilyInfo.getLsmName());
			ps.setString(8, adverseFamilyInfo.getLslName());
			ps.setString(9, adverseFamilyInfo.getSecondName());
			ps.setString(10, adverseFamilyInfo.getCalledByName());
			ps.setString(11, adverseFamilyInfo.getNotes());
			ps.setString(12, adverseFamilyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(13, adverseFamilyHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseFamilyInfo
	 * @param adverseId
	 * @param user
	 * @param adverseFamilyUpdateHashValue
	 * @throws SQLException
	 */
	public void insertAdverseFamilyInfoUpdateTable(AdverseFamilyInfo adverseFamilyInfo, Long adverseId, User user,
			String adverseFamilyUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_adverse_media_family_info_update"
				+ "(list_adverse_media_personal_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,maker,checker,reason,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseFamilyInfo.getRelationshipTo());
			ps.setString(3, adverseFamilyInfo.getFirstName());
			ps.setString(4, adverseFamilyInfo.getMiddleName());
			ps.setString(5, adverseFamilyInfo.getLastName());
			ps.setString(6, adverseFamilyInfo.getLsfName());
			ps.setString(7, adverseFamilyInfo.getLsmName());
			ps.setString(8, adverseFamilyInfo.getLslName());
			ps.setString(9, adverseFamilyInfo.getSecondName());
			ps.setString(10, adverseFamilyInfo.getCalledByName());
			ps.setString(11, adverseFamilyInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, adverseFamilyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(16, adverseFamilyUpdateHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseMediaAddressInfo
	 * @param adverseId
	 * @param adverseAddressHashValue
	 * @throws SQLException
	 */
	public void insertAdverseAddressInfo(AdverseAddressInfo adverseMediaAddressInfo, Long adverseId,
			String adverseAddressHashValue) throws SQLException {
		String sql = "INSERT INTO list_adverse_media_address_info (list_adverse_media_personal_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseMediaAddressInfo.getCountry());
			ps.setString(3, adverseMediaAddressInfo.getState());
			ps.setString(4, adverseMediaAddressInfo.getProvince());
			ps.setString(5, adverseMediaAddressInfo.getDistrict());
			ps.setString(6, adverseMediaAddressInfo.getMnVdc());
			ps.setString(7, adverseMediaAddressInfo.getTownCityVillage());
			ps.setString(8, adverseMediaAddressInfo.getTole());
			ps.setString(9, adverseMediaAddressInfo.getStreet());
			ps.setString(10, adverseMediaAddressInfo.getHouseNumber());
			ps.setString(11, adverseMediaAddressInfo.getNotes());
			ps.setString(12, adverseMediaAddressInfo.getCommunicationType());
			ps.setString(13, adverseMediaAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, adverseMediaAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, adverseMediaAddressInfo.getPhoneNo());
			ps.setString(16, adverseMediaAddressInfo.getTelexNoCountryCode());
			ps.setString(17, adverseMediaAddressInfo.getTelexNoAreaCode());
			ps.setString(18, adverseMediaAddressInfo.getTelexNo());
			ps.setString(19, adverseMediaAddressInfo.getPagerNoCountryCode());
			ps.setString(20, adverseMediaAddressInfo.getPagerNoAreaCode());
			ps.setString(21, adverseMediaAddressInfo.getPagerNo());
			ps.setString(22, adverseAddressHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseMediaAddressInfo
	 * @param adverseId
	 * @param user
	 * @param adverseAddressUpdateHashValue
	 * @throws SQLException
	 */
	public void insertAdverseAddressInfoUpdateTable(AdverseAddressInfo adverseMediaAddressInfo, Long adverseId,
			User user, String adverseAddressUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_adverse_media_address_info_update"
				+ "(list_adverse_media_personal_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,maker,checker,reason,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseMediaAddressInfo.getCountry());
			ps.setString(3, adverseMediaAddressInfo.getState());
			ps.setString(4, adverseMediaAddressInfo.getProvince());
			ps.setString(5, adverseMediaAddressInfo.getDistrict());
			ps.setString(6, adverseMediaAddressInfo.getMnVdc());
			ps.setString(7, adverseMediaAddressInfo.getTownCityVillage());
			ps.setString(8, adverseMediaAddressInfo.getTole());
			ps.setString(9, adverseMediaAddressInfo.getStreet());
			ps.setString(10, adverseMediaAddressInfo.getHouseNumber());
			ps.setString(11, adverseMediaAddressInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, adverseMediaAddressInfo.getCommunicationType());
			ps.setString(16, adverseMediaAddressInfo.getPhoneNoCountryCode());
			ps.setString(17, adverseMediaAddressInfo.getPhoneNoAreaCode());
			ps.setString(18, adverseMediaAddressInfo.getPhoneNo());
			ps.setString(19, adverseMediaAddressInfo.getTelexNoCountryCode());
			ps.setString(20, adverseMediaAddressInfo.getTelexNoAreaCode());
			ps.setString(21, adverseMediaAddressInfo.getTelexNo());
			ps.setString(22, adverseMediaAddressInfo.getPagerNoCountryCode());
			ps.setString(23, adverseMediaAddressInfo.getPagerNoAreaCode());
			ps.setString(24, adverseMediaAddressInfo.getPagerNo());
			ps.setString(25, adverseAddressUpdateHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseMediaInfo
	 * @param hotListId
	 * @param adverseMediaHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertAdverseMediaInfo(AdverseAttachment adverseMediaInfo, Long hotListId, String adverseMediaHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_adverse_media_info (list_adverse_media_personal_info_id,notes,published_date,extension_text,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, adverseMediaInfo.getNotes());
			if (!adverseMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(adverseMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, adverseMediaInfo.getExtensionText());
			ps.setString(5, adverseMediaInfo.getMediaType());
			ps.setBytes(6, adverseMediaInfo.getMediaContent().getBytes());
			ps.setString(7, adverseMediaInfo.getSourceOfInformation());
			ps.setString(8, adverseMediaInfo.getSourceType());
			ps.setString(9, adverseMediaHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * ]
	 * 
	 * @param adverseMediaInfo
	 * @param hotListId
	 * @param user
	 * @param adverseMediaUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertAdverseMediaInfoUpdateTable(AdverseAttachment adverseMediaInfo, Long hotListId, User user,
			String adverseMediaUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_adverse_media_info_update (list_adverse_media_personal_info_id,notes,published_date,extension_text,maker,checker,reason,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, adverseMediaInfo.getNotes());
			if (!adverseMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(adverseMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, adverseMediaInfo.getExtensionText());
			ps.setString(5, user.getUserName());
			ps.setString(6, user.getUserName());
			ps.setString(7, "no reason");
			ps.setString(8, adverseMediaInfo.getMediaType());
			ps.setBytes(9, adverseMediaInfo.getMediaContent().getBytes());
			ps.setString(10, adverseMediaInfo.getSourceOfInformation());
			ps.setString(11, adverseMediaInfo.getSourceType());
			ps.setString(12, adverseMediaUpdateHashValue);
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param personalInfo
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updatePersonalInfo(PersonalInfo personalInfo) throws SQLException, ParseException {
		String sql = "UPDATE list_adverse_media_personal_info SET jurisdiction=?,title=?,"
				+ "first_name=?,middle_name=?,last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,"
				+ "second_name=?,called_by_name=?,previous_name=?,gender=?,date_of_birth=?,"
				+ "place_of_birth=?,country_of_birth=?,nationality=?,notes=?,document_code=?,reference_no=?,approved_by=?,correspondent=?,pan_number=?,citizen_number=?,approved_date=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getAdverseCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setLong(25, personalInfo.getId());
			ps.executeUpdate();

		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseOfficeInfo
	 * @param adverseId
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateAdverseOfficeInfo(AdverseOfficeInfo adverseOfficeInfo, long adverseId)
			throws SQLException, ParseException {
		String sql = "UPDATE list_adverse_media_office_info SET list_adverse_media_personal_info_id=?,designation=?,office_name=?,prolongation=?,"
				+ "notes=?,term_year=?,date_of_appointment=?,office_status=?,document_code=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseOfficeInfo.getDesignation());
			ps.setString(3, adverseOfficeInfo.getOfficeName());
			ps.setLong(4, adverseOfficeInfo.getProlongation());
			ps.setString(5, adverseOfficeInfo.getNotes());
			ps.setLong(6, adverseOfficeInfo.getTermYears());
			if (!adverseOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(adverseOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, adverseOfficeInfo.getPepOfficeStatus());
			ps.setString(9, adverseOfficeInfo.getAdverseCategory());
			ps.setLong(10, adverseOfficeInfo.getId());
			ps.execute();

		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * 
	 * @param adverseFamilyInfo
	 * @param adverseId
	 * @throws SQLException
	 */
	public void updateAdverseFamilyInfo(AdverseFamilyInfo adverseFamilyInfo, long adverseId) throws SQLException {
		String sql = "UPDATE list_adverse_media_family_info SET list_adverse_media_personal_info_id=?,relationship_to=?,first_name=?,middle_name=?,"
				+ "last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,notes=?,primary_identification_document_number=? WHERE ID=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseFamilyInfo.getRelationshipTo());
			ps.setString(3, adverseFamilyInfo.getFirstName());
			ps.setString(4, adverseFamilyInfo.getMiddleName());
			ps.setString(5, adverseFamilyInfo.getLastName());
			ps.setString(6, adverseFamilyInfo.getLsfName());
			ps.setString(7, adverseFamilyInfo.getLsmName());
			ps.setString(8, adverseFamilyInfo.getLslName());
			ps.setString(9, adverseFamilyInfo.getSecondName());
			ps.setString(10, adverseFamilyInfo.getCalledByName());
			ps.setString(11, adverseFamilyInfo.getNotes());
			ps.setString(12, adverseFamilyInfo.getPrimaryIdentificationDocumentNo());
			ps.setLong(13, adverseFamilyInfo.getId());
			ps.execute();

		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param adverseMediaAddressInfo
	 * @param adverseId
	 * @throws SQLException
	 */
	public void updateAdverseAddressInfo(AdverseAddressInfo adverseMediaAddressInfo, long adverseId)
			throws SQLException {
		String sql = "UPDATE list_adverse_media_address_info SET list_adverse_media_personal_info_id=?,country=?,state=?,province=?,district=?,mn_vdc=?,"
				+ "town_city_village=?,tole=?,street=?,house_number=?,notes=?,communication_type=?,phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,pager_no=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseMediaAddressInfo.getCountry());
			ps.setString(3, adverseMediaAddressInfo.getState());
			ps.setString(4, adverseMediaAddressInfo.getProvince());
			ps.setString(5, adverseMediaAddressInfo.getDistrict());
			ps.setString(6, adverseMediaAddressInfo.getMnVdc());
			ps.setString(7, adverseMediaAddressInfo.getTownCityVillage());
			ps.setString(8, adverseMediaAddressInfo.getTole());
			ps.setString(9, adverseMediaAddressInfo.getStreet());
			ps.setString(10, adverseMediaAddressInfo.getHouseNumber());
			ps.setString(11, adverseMediaAddressInfo.getNotes());
			ps.setString(12, adverseMediaAddressInfo.getCommunicationType());
			ps.setString(13, adverseMediaAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, adverseMediaAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, adverseMediaAddressInfo.getPhoneNo());
			ps.setString(16, adverseMediaAddressInfo.getTelexNoCountryCode());
			ps.setString(17, adverseMediaAddressInfo.getTelexNoAreaCode());
			ps.setString(18, adverseMediaAddressInfo.getTelexNo());
			ps.setString(19, adverseMediaAddressInfo.getPagerNoCountryCode());
			ps.setString(20, adverseMediaAddressInfo.getPagerNoAreaCode());
			ps.setString(21, adverseMediaAddressInfo.getPagerNo());
			ps.setLong(22, adverseMediaAddressInfo.getId());
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param adverseMediaInfo
	 * @param adverseId
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateAdverseMediaInfo(AdverseAttachment adverseMediaInfo, long adverseId)
			throws SQLException, ParseException {
		String sql = "UPDATE list_adverse_media_info SET list_adverse_media_personal_info_id=?,notes=?,published_date=?,extension_text=?,media_type=?,media_content=?,source_of_information=?,source_type=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, adverseId);
			ps.setString(2, adverseMediaInfo.getNotes());
			if (!adverseMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(adverseMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, adverseMediaInfo.getExtensionText());
			ps.setString(5, adverseMediaInfo.getMediaType());
			ps.setBytes(6, adverseMediaInfo.getMediaContent().getBytes());
			ps.setString(7, adverseMediaInfo.getSourceOfInformation());
			ps.setString(8, adverseMediaInfo.getSourceType());
			ps.setLong(9, adverseMediaInfo.getId());
			ps.execute();
		} finally {
			connection.close();
			ps.close();
		}

	}

	public String fetchAdverseData(ScreeningOfMigratedData adverseDataByDate) throws SQLException, ParseException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_sanction_list(?,?)}");
			stmt.setInt(1, 2);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public String getLatestUniqueKey() throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_lastest_key_from_list_adverse_media_personal_info(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public void updateAdversePersonalInfo(PersonalInfo personalInfo, String newUniqueKey) throws SQLException {
		String sql = "UPDATE list_adverse_media_personal_info SET key=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, newUniqueKey);
			ps.setLong(2, personalInfo.getId());
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}
}
