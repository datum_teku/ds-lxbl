package com.trustaml.service.adversemedia.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdverseFamilyInfo {

	private long id;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("relationship")
	private String relationshipTo;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationDocumentNo;

	@JsonProperty("change")
	private boolean change;

	public AdverseFamilyInfo(String firstName, String middleName, String lastName, String lsfName, String lsmName,
			String lslName, String secondName, String calledByName, String relationshipTo, String notes,
			String primaryIdentificationDocumentNo, boolean change) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.secondName = secondName;
		this.calledByName = calledByName;
		this.relationshipTo = relationshipTo;
		this.notes = notes;
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
		this.change = change;
	}

	public AdverseFamilyInfo() {
		super();
		this.relationshipTo = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.secondName = "";
		this.calledByName = "";
		this.notes = "";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getRelationshipTo() {
		return relationshipTo;
	}

	public void setRelationshipTo(String relationshipTo) {
		this.relationshipTo = relationshipTo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPrimaryIdentificationDocumentNo() {
		return primaryIdentificationDocumentNo;
	}

	public void setPrimaryIdentificationDocumentNo(String primaryIdentificationDocumentNo) {
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
