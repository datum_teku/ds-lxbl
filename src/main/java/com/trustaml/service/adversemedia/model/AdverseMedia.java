package com.trustaml.service.adversemedia.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class AdverseMedia {

	@JsonProperty("personal-info")
	private PersonalInfo personalInfo;

	@JsonProperty("media-info")
	private List<AdverseAttachment> attachment;

	@JsonProperty("address-info")
	private List<AdverseAddressInfo> adverseAddressInfo;

	@JsonProperty("office-info")
	private List<AdverseOfficeInfo> adverseOfficeInfo;

	@JsonProperty("family-info")
	private List<AdverseFamilyInfo> adverseFamilyInfo;

	@JsonProperty("user")
	private User user;

	public AdverseMedia(PersonalInfo personalInfo, List<AdverseAttachment> attachment,
			List<AdverseAddressInfo> adverseAddressInfo, List<AdverseOfficeInfo> adverseOfficeInfo,
			List<AdverseFamilyInfo> adverseFamilyInfo, User user) {
		super();
		this.personalInfo = personalInfo;
		this.attachment = attachment;
		this.adverseAddressInfo = adverseAddressInfo;
		this.adverseOfficeInfo = adverseOfficeInfo;
		this.adverseFamilyInfo = adverseFamilyInfo;
		this.user = user;
	}

	public AdverseMedia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PersonalInfo getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

	public List<AdverseAttachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<AdverseAttachment> attachment) {
		this.attachment = attachment;
	}

	public List<AdverseAddressInfo> getAdverseAddressInfo() {
		return adverseAddressInfo;
	}

	public void setAdverseAddressInfo(List<AdverseAddressInfo> adverseAddressInfo) {
		this.adverseAddressInfo = adverseAddressInfo;
	}

	public List<AdverseOfficeInfo> getAdverseOfficeInfo() {
		return adverseOfficeInfo;
	}

	public void setAdverseOfficeInfo(List<AdverseOfficeInfo> adverseOfficeInfo) {
		this.adverseOfficeInfo = adverseOfficeInfo;
	}

	public List<AdverseFamilyInfo> getAdverseFamilyInfo() {
		return adverseFamilyInfo;
	}

	public void setAdverseFamilyInfo(List<AdverseFamilyInfo> adverseFamilyInfo) {
		this.adverseFamilyInfo = adverseFamilyInfo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
