package com.trustaml.service.adversemedia.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdverseMediaInfo {

	private long id;
	@JsonProperty("company-name")
	private String companyName;

	@JsonProperty("media-source")
	private String mediaSource;

	@JsonProperty("published-date")
	private java.sql.Date publishedDate;

	@JsonProperty("attachment-name")
	private String attachmentName;

	@JsonProperty("adverse-media-type")
	private String adverseMediaType;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("reason")
	private String reason;

	private AdverseMediaInfoAttachment adverseMediaInfoAttachment = new AdverseMediaInfoAttachment();

	private AdverseMediaPersonalInfo adverseMediaInfoUpdate = new AdverseMediaPersonalInfo();

	public AdverseMediaInfo() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getMediaSource() {
		return mediaSource;
	}

	public void setMediaSource(String mediaSource) {
		this.mediaSource = mediaSource;
	}

	public java.sql.Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(java.sql.Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	public String getAdverseMediaType() {
		return adverseMediaType;
	}

	public void setAdverseMediaType(String adverseMediaType) {
		this.adverseMediaType = adverseMediaType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public AdverseMediaInfoAttachment getAdverseMediaInfoAttachment() {
		return adverseMediaInfoAttachment;
	}

	public void setAdverseMediaInfoAttachment(AdverseMediaInfoAttachment adverseMediaInfoAttachment) {
		this.adverseMediaInfoAttachment = adverseMediaInfoAttachment;
	}

	public AdverseMediaPersonalInfo getAdverseMediaInfoUpdate() {
		return adverseMediaInfoUpdate;
	}

	public void setAdverseMediaInfoUpdate(AdverseMediaPersonalInfo adverseMediaInfoUpdate) {
		this.adverseMediaInfoUpdate = adverseMediaInfoUpdate;
	}

}
