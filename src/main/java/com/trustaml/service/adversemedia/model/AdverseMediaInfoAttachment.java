package com.trustaml.service.adversemedia.model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.imageio.ImageIO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdverseMediaInfoAttachment {

	private long id;
	
	private String adverseMediaAttachment;
	@JsonProperty("notes")
	
	private String notes;

	public AdverseMediaInfoAttachment(long id, String adverseMediaAttachment, String notes) {
		super();
		this.id = id;
		this.adverseMediaAttachment = adverseMediaAttachment;
		this.notes = notes;
	}

	public AdverseMediaInfoAttachment() {
		super();
	}

	/* image to base64 converter */
	public String getImageAsString(String images) {

		BufferedImage image = null;

		try {

			image = ImageIO.read(new File(images));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		String imageString = null;
		try {
			ImageIO.write(image, "jpg", bos);

			byte[] imageBytes = bos.toByteArray();
			Encoder encoder = Base64.getEncoder();
			imageString = encoder.encodeToString(imageBytes);
			bos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return imageString;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAdverseMediaAttachment() {
		return adverseMediaAttachment;
	}

	public void setAdverseMediaAttachment(String adverseMediaAttachment) {
		this.adverseMediaAttachment = adverseMediaAttachment;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
