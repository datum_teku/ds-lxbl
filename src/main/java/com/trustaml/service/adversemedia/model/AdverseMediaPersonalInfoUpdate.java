package com.trustaml.service.adversemedia.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AdverseMediaPersonalInfoUpdate {
	
	private long id;
	private String salutation;
	private String firstName;
	private String middleName;
	private String lastName;
	private String lsfName;
	private String lsmName;
	private String lslName;
	private String gender;
	private String fatherFirstName;
	private String fatherMiddleName;
	private String fatherlastName;
	private String grandfatherFirstName;
	private String grandfatherMiddleName;
	private String grandfatherLastName;
	private String spouseFirstName;
	private String spouseMiddleName;
	private String spouseLastName;
	private String notes;
	private String reason;
	private String makerId;
	private String checkerId;
	private boolean approved;
	private String updateDate;
	private String approvedDate;
	

	public AdverseMediaPersonalInfoUpdate(long id, String salutation, String firstName, String middleName,
			String lastName, String lsfName, String lsmName, String lslName, String gender, String fatherFirstName,
			String fatherMiddleName, String fatherlastName, String grandfatherFirstName, String grandfatherMiddleName,
			String grandfatherLastName, String spouseFirstName, String spouseMiddleName, String spouseLastName,
			String notes, String reason, String makerId, String checkerId, boolean approved, String updateDate,
			String approvedDate) {
		super();
		this.id = id;
		this.salutation = salutation;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.gender = gender;
		this.fatherFirstName = fatherFirstName;
		this.fatherMiddleName = fatherMiddleName;
		this.fatherlastName = fatherlastName;
		this.grandfatherFirstName = grandfatherFirstName;
		this.grandfatherMiddleName = grandfatherMiddleName;
		this.grandfatherLastName = grandfatherLastName;
		this.spouseFirstName = spouseFirstName;
		this.spouseMiddleName = spouseMiddleName;
		this.spouseLastName = spouseLastName;
		this.notes = notes;
		this.reason = reason;
		this.makerId = makerId;
		this.checkerId = checkerId;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
	}


	public AdverseMediaPersonalInfoUpdate() {
	}


	public String getCurrentDate(){
		
		SimpleDateFormat dateformat=new SimpleDateFormat("yyyy/MM/dd");
		Date date1=new Date();
		String dateString=(dateformat.format(date1));
		System.out.println(dateString);
		
		return dateString;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getSalutation() {
		return salutation;
	}


	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getLsfName() {
		return lsfName;
	}


	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}


	public String getLsmName() {
		return lsmName;
	}


	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}


	public String getLslName() {
		return lslName;
	}


	public void setLslName(String lslName) {
		this.lslName = lslName;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getFatherFirstName() {
		return fatherFirstName;
	}


	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}


	public String getFatherMiddleName() {
		return fatherMiddleName;
	}


	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}


	public String getFatherlastName() {
		return fatherlastName;
	}


	public void setFatherlastName(String fatherlastName) {
		this.fatherlastName = fatherlastName;
	}


	public String getGrandfatherFirstName() {
		return grandfatherFirstName;
	}


	public void setGrandfatherFirstName(String grandfatherFirstName) {
		this.grandfatherFirstName = grandfatherFirstName;
	}


	public String getGrandfatherMiddleName() {
		return grandfatherMiddleName;
	}


	public void setGrandfatherMiddleName(String grandfatherMiddleName) {
		this.grandfatherMiddleName = grandfatherMiddleName;
	}


	public String getGrandfatherLastName() {
		return grandfatherLastName;
	}


	public void setGrandfatherLastName(String grandfatherLastName) {
		this.grandfatherLastName = grandfatherLastName;
	}


	public String getSpouseFirstName() {
		return spouseFirstName;
	}


	public void setSpouseFirstName(String spouseFirstName) {
		this.spouseFirstName = spouseFirstName;
	}


	public String getSpouseMiddleName() {
		return spouseMiddleName;
	}


	public void setSpouseMiddleName(String spouseMiddleName) {
		this.spouseMiddleName = spouseMiddleName;
	}


	public String getSpouseLastName() {
		return spouseLastName;
	}


	public void setSpouseLastName(String spouseLastName) {
		this.spouseLastName = spouseLastName;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getMakerId() {
		return makerId;
	}


	public void setMakerId(String makerId) {
		this.makerId = makerId;
	}


	public String getCheckerId() {
		return checkerId;
	}


	public void setCheckerId(String checkerId) {
		this.checkerId = checkerId;
	}


	public boolean isApproved() {
		return approved;
	}


	public void setApproved(boolean approved) {
		this.approved = approved;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getApprovedDate() {
		return approvedDate;
	}


	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}
	
	

	
	

}
