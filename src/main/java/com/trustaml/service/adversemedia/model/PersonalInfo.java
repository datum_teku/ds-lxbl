package com.trustaml.service.adversemedia.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalInfo {

	private long id;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("adverse_category")
	private String adverseCategory;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("date_of_birth")
	private String dateOfBirth;

	@JsonProperty("jurisdiction")
	private String jurisdiction;

	@JsonProperty("previous_name")
	private String previousName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("place_of_birth")
	private String placeOfBirth;

	@JsonProperty("nationality")
	private String nationality;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("country_of_birth")
	private String countryOfBirth;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("change")
	private boolean change;

	@JsonProperty("reference_no")
	private String referenceNo;

	@JsonProperty("approved_by")
	private String approvedBy;

	@JsonProperty("correspondent")
	private String correspondent;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("citizen_number")
	private String citizenNumber;

	@JsonProperty("approved_date")
	private String approvedDate;

	public PersonalInfo(long id, String lslName, String adverseCategory, String lsfName, String notes, String gender,
			String dateOfBirth, String jurisdiction, String previousName, String lastName, String middleName,
			String placeOfBirth, String nationality, String calledByName, String countryOfBirth, String secondName,
			String salutation, String firstName, String lsmName, boolean change, String referenceNo, String approvedBy,
			String correspondent, String panNumber, String citizenNumber, String approvedDate) {
		super();
		this.id = id;
		this.lslName = lslName;
		this.adverseCategory = adverseCategory;
		this.lsfName = lsfName;
		this.notes = notes;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.jurisdiction = jurisdiction;
		this.previousName = previousName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.placeOfBirth = placeOfBirth;
		this.nationality = nationality;
		this.calledByName = calledByName;
		this.countryOfBirth = countryOfBirth;
		this.secondName = secondName;
		this.salutation = salutation;
		this.firstName = firstName;
		this.lsmName = lsmName;
		this.change = change;
		this.referenceNo = referenceNo;
		this.approvedBy = approvedBy;
		this.correspondent = correspondent;
		this.panNumber = panNumber;
		this.citizenNumber = citizenNumber;
		this.approvedDate = approvedDate;
	}

	public PersonalInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getAdverseCategory() {
		return adverseCategory;
	}

	public void setAdverseCategory(String adverseCategory) {
		this.adverseCategory = adverseCategory;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getPreviousName() {
		return previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getCorrespondent() {
		return correspondent;
	}

	public void setCorrespondent(String correspondent) {
		this.correspondent = correspondent;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getCitizenNumber() {
		return citizenNumber;
	}

	public void setCitizenNumber(String citizenNumber) {
		this.citizenNumber = citizenNumber;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

}
