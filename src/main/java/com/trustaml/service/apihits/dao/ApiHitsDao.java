package com.trustaml.service.apihits.dao;

import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ApiHitsDao {

	@Inject
	ApiHitsDaoImpl apiHitsDaoImpl;

	public void increaseCountOfApiHits(String requestEndPoints) throws SQLException {
		Long apiHits = apiHitsDaoImpl.getCountOfApiHits(requestEndPoints);
		if (apiHits == 0) {
			apiHitsDaoImpl.insertApiHitsCount(requestEndPoints);
		} else {
			apiHitsDaoImpl.increaseCountOfApiHits(requestEndPoints, apiHits);
		}

	}

}
