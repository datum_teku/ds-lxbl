package com.trustaml.service.apihits.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;

public class ApiHitsDaoImpl {
	@Inject
	DBConnection dbConnection;

	public Long getCountOfApiHits(String requestEndPoints) throws SQLException {
		long apiHitCount = 0;
		String sql = "SELECT * FROM api_hits_count WHERE api_name=?";
		Connection con = dbConnection.getConnection();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, requestEndPoints);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			apiHitCount = rs.getLong(3);
			ps.close();
			con.close();
		}
		return apiHitCount;

	}

	public void insertApiHitsCount(String requestEndPoints) throws SQLException {
		String sql = "INSERT INTO api_hits_count (api_name,number_of_hits) VALUES(?,?)";
		Connection con = dbConnection.getConnection();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, requestEndPoints);
		ps.setLong(2, 1);
		ps.execute();
		ps.close();
		con.close();
	}

	public void increaseCountOfApiHits(String requestEndPoints, Long apiHits) throws SQLException {
		String sql = "UPDATE api_hits_count SET number_of_hits=? WHERE api_name=?";
		Connection con = dbConnection.getConnection();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(2, requestEndPoints);
		ps.setLong(1, apiHits + 1);
		ps.execute();
		ps.close();
		con.close();
		

	}

}
