package com.trustaml.service.authentication;

import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import com.trustaml.service.apihits.dao.ApiHitsDao;
import com.trustaml.service.common.ConstantEntity;

@Provider
@PreMatching
public class AuthenticationFilter implements ContainerRequestFilter, ContainerResponseFilter {
	@Context
	HttpServletRequest servletRequest;

	@Context
	UriInfo uriInfo;

	@Context
	private ResourceInfo resourceInfo;

	@Context
	SecurityContext sc;

	@Inject
	ApiHitsDao apiHitsDao;

	@Inject
	ConstantEntity constantEntity;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// String methodName = requestContext.getMethod();
		String requestEndPoints = servletRequest.getPathInfo();

		// AmlLogger.printData(requestEndPoints);
		// AmlLogger.printData("Annotation invoked is::-" + methodName);
		// AmlLogger.printData(servletRequest.getRemoteAddr());
		// AmlLogger.info(AuthenticationFilter.class, "Receiving request from: "
		// + servletRequest.getRemoteAddr());
		// AmlLogger.info(AuthenticationFilter.class, "Attempt to invoke api: "
		// + requestEndPoints);
		// AmlLogger.info(AuthenticationFilter.class, "Attempt to invoke method"
		// + methodName + "\"");
		// AmlLogger.printData("Invoked Class is ::" +
		// uriInfo.getMatchedResources().get(0).getClass().toString());
		// Method theMethod = resourceInfo.getResourceMethod();
		// AmlLogger.printData("Invoked method is:: " + theMethod);
		// System.out.println("inside AutheticationFilter");
		if (!constantEntity.isSetApiAccess()) {
			try {
				constantEntity.setApiAccess();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!requestEndPoints.equals("/")) {
			try {
				apiHitsDao.increaseCountOfApiHits(requestEndPoints);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
			// AmlLogger.printData("authorizationHeader:-" +
			// authorizationHeader);
			String moduleName = constantEntity.checkAccessToApi(authorizationHeader);
			// AmlLogger.printData("Access to module :- " + moduleName);
			if (moduleName == null) {
				throw new NotAuthorizedException("module not matched");
			}
			// RestConfiguration restConfiguration = new RestConfiguration();
			// restConfiguration.getCompalianceClasses();
			if (authorizationHeader == null || authorizationHeader.isEmpty()) {
				throw new NotAuthorizedException("Authorization header must be provided");
			}
		}

	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		// AmlLogger.info(AuthenticationFilter.class, "Receiving request from: "
		// + servletRequest.getRemoteAddr());
		// AmlLogger.info(AuthenticationFilter.class, "Response Status is: " +
		// responseContext.getStatus());
		// AmlLogger.printData("Resonse code is:: " +
		// responseContext.getStatus());
		// responseContext.getHeaders().add("X-Powered-By", "TrustAML :-)");
		// AmlLogger.printData(servletRequest.getRemoteAddr());

	}

}
