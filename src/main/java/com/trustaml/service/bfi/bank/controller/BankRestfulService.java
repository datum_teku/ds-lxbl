package com.trustaml.service.bfi.bank.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.bfi.bank.dao.BankDao;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;

@Path("/bank")
public class BankRestfulService {

	@Inject
	BankDao bankDao;

	/**
	 * @return all the bank detail
	 * @description uses procedure bfi_details_json
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCategory() throws SQLException {
		return ResponseReturn.sucess(bankDao.getBankDetails());

	}

	/**
	 * @param data
	 * @return save successful
	 * @description save bank data to table
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveBank(String data)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!data.isEmpty() || !data.equals("")) {
			bankDao.insertBfi(data);
			return ResponseReturn.sucess("save SucessFul");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}

	}

	/**
	 * @param jsonData
	 * @return update successful
	 * @description update bank data using bfi id
	 * @throws TrustAmlEmptyJSONException
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateBank(String jsonData)
			throws TrustAmlEmptyJSONException, SQLException, JsonParseException, JsonMappingException, IOException {
		if (!jsonData.isEmpty() || !jsonData.equals("")) {
			bankDao.updateBfi(jsonData);
			return ResponseReturn.sucess("update SucessFul");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

}
