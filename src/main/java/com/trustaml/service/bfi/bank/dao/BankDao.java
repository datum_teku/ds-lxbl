package com.trustaml.service.bfi.bank.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.bfi.bank.model.BfiAddress;
import com.trustaml.service.bfi.bank.model.BfiContact;
import com.trustaml.service.bfi.bank.model.BfiEmail;
import com.trustaml.service.bfi.bank.model.BfiInfo;
import com.trustaml.service.bfi.bank.util.JsonStringToBfiObjects;
import com.trustaml.service.common.dto.User;

public class BankDao {
	@Inject
	BankDaoImpl bankDaoImpl;

	private long bfiInfoDefaultId = 1;

	/**
	 * @return
	 * @throws SQLException
	 */
	public Object getBankDetails() throws SQLException {
		return bankDaoImpl.bfiBankDetails();
	}

	/**
	 * @param data
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void insertBfi(String data) throws SQLException, JsonParseException, JsonMappingException, IOException {
		BfiInfo bfiInfo = JsonStringToBfiObjects.getBfiInfoObjectFromJsonString(data);
		List<BfiAddress> bfiAddressList = JsonStringToBfiObjects.getBfiAddressObjectFromJsonString(data);
		List<BfiEmail> bfiEmailList = JsonStringToBfiObjects.getBfiEmailObjectFromJsonString(data);
		List<BfiContact> bfiContactList = JsonStringToBfiObjects.getBfiContactObjectFromJsonString(data);
		User user = JsonStringToBfiObjects.getUserObjectFromJsonString(data);

		bfiInfo.setId(bfiInfoDefaultId);
		Long bfiInfoId = bankDaoImpl.insertIntoBfiInfo(bfiInfo, user);
		bankDaoImpl.insertIntoBfiInfoUpdate(bfiInfo, user);

		for (final BfiAddress bfiAddress : bfiAddressList) {
			bfiAddress.setId(bankDaoImpl.insertBfiAddress(bfiAddress, bfiInfoId));
			bankDaoImpl.insertIntoBfiAddressUpdate(bfiAddress, user);
		}

		for (final BfiContact bfiContact : bfiContactList) {
			bfiContact.setId(bankDaoImpl.insertBfiContact(bfiContact, bfiInfoId));
			bankDaoImpl.insertIntoBfiContactUpdate(bfiContact, user);
		}
		for (final BfiEmail bfiEmail : bfiEmailList) {
			bfiEmail.setId(bankDaoImpl.insertBfiEmail(bfiEmail, bfiInfoId));
			bankDaoImpl.insertIntoBfiEmailUpdate(bfiEmail, user);
		}

	}

	/**
	 * @param jsonData
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void updateBfi(String jsonData) throws SQLException, JsonParseException, JsonMappingException, IOException {
		BfiInfo bfiInfo = JsonStringToBfiObjects.getBfiInfoObjectFromJsonString(jsonData);
		List<BfiAddress> bfiAddressList = JsonStringToBfiObjects.getBfiAddressObjectFromJsonString(jsonData);
		List<BfiEmail> bfiEmailList = JsonStringToBfiObjects.getBfiEmailObjectFromJsonString(jsonData);
		List<BfiContact> bfiContactList = JsonStringToBfiObjects.getBfiContactObjectFromJsonString(jsonData);
		bfiInfo.setId(bfiInfoDefaultId);
		User user = JsonStringToBfiObjects.getUserObjectFromJsonString(jsonData);
		bankDaoImpl.updateBfiInfo(bfiInfo, user, bfiInfoDefaultId);
		bankDaoImpl.insertIntoBfiInfoUpdate(bfiInfo, user);
		for (final BfiAddress bfiAddress : bfiAddressList) {
			if (bfiAddress.getId() != 0) {
				bankDaoImpl.updateBfiAddress(bfiAddress, bfiInfo.getId());
				bankDaoImpl.insertIntoBfiAddressUpdate(bfiAddress, user);
			} else {
				bfiAddress.setId(bankDaoImpl.insertBfiAddress(bfiAddress, bfiInfoDefaultId));
				bankDaoImpl.insertIntoBfiAddressUpdate(bfiAddress, user);
			}

		}

		for (final BfiContact bfiContact : bfiContactList) {
			if (bfiContact.getId() != 0) {
				bankDaoImpl.updateBfiContact(bfiContact, bfiInfo.getId());
				bankDaoImpl.insertIntoBfiContactUpdate(bfiContact, user);
			} else {
				bfiContact.setId(bankDaoImpl.insertBfiContact(bfiContact, bfiInfoDefaultId));
				bankDaoImpl.insertIntoBfiContactUpdate(bfiContact, user);
			}
		}
		for (final BfiEmail bfiEmail : bfiEmailList) {
			if (bfiEmail.getId() != 0) {
				bankDaoImpl.updateBfiEmail(bfiEmail, bfiInfo.getId());
				bankDaoImpl.insertIntoBfiEmailUpdate(bfiEmail, user);
			} else {
				bfiEmail.setId(bankDaoImpl.insertBfiEmail(bfiEmail, bfiInfoDefaultId));
				bankDaoImpl.insertIntoBfiEmailUpdate(bfiEmail, user);
			}
		}
	}
}
