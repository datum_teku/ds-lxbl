package com.trustaml.service.bfi.bank.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.bfi.bank.model.BfiAddress;
import com.trustaml.service.bfi.bank.model.BfiAddressUpdate;
import com.trustaml.service.bfi.bank.model.BfiContact;
import com.trustaml.service.bfi.bank.model.BfiContactUpdate;
import com.trustaml.service.bfi.bank.model.BfiEmail;
import com.trustaml.service.bfi.bank.model.BfiEmailUpdate;
import com.trustaml.service.bfi.bank.model.BfiInfo;
import com.trustaml.service.bfi.bank.model.BfiInfoUpdate;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;

public class BankDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @return
	 * @throws SQLException
	 */
	public Object bfiBankDetails() throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call  bfi_details_json(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			con.close();
			stmt.close();
		}
		return result;

	}

	/*
	 * public void saveBfi(BfiInfo bfiInfo) throws SQLException {
	 * 
	 * // User validateUser = new User(); // updateBfiInfo(bfiInfo,
	 * validateUser);
	 * 
	 * }
	 */
	/**
	 * @param bfiInfo
	 * @param validatedUser
	 * @param bfiInfoDefaultId
	 * @throws SQLException
	 */
	public void updateBfiInfo(BfiInfo bfiInfo, User validatedUser, long bfiInfoDefaultId) throws SQLException {
		String sql = "UPDATE bfi_info SET bfi_name=?,fiu_reference_no=?,bank_code=?,"
				+ "local_currency=?,notes=? WHERE id = ?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, bfiInfo.getBfiName());
			ps.setString(2, bfiInfo.getFiuReferenceNo());
			ps.setString(3, bfiInfo.getBankCode());
			ps.setString(4, bfiInfo.getLocalCurrency());
			ps.setString(5, bfiInfo.getNotes());
			ps.setLong(6, bfiInfoDefaultId);
			ps.executeUpdate();

		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param bfiInfo
	 * @param validatedUser
	 * @throws SQLException
	 */
	public void insertIntoBfiInfoUpdate(BfiInfo bfiInfo, User validatedUser) throws SQLException {
		final BfiInfoUpdate update = new BfiInfoUpdate(bfiInfo);
		update.setId(1);
		final String sql = "Insert into bfi_info_update (bfi_info_id,bfi_name,fiu_reference_no,bank_code,"
				+ "local_currency,notes,approved,update_date,approved_date,reason,maker)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, update.getId());
			ps.setString(2, update.getBfiName());
			ps.setString(3, update.getFiuReferenceNo());
			ps.setString(4, update.getBankCode());
			ps.setString(5, update.getLocalCurrency());
			ps.setString(6, update.getNotes());
			ps.setBoolean(7, update.isApproved());
			ps.setDate(8, update.getUpdateDate());
			ps.setDate(9, update.getApprovedDate());
			ps.setString(10, update.getReason());
			ps.setString(11, validatedUser.getUserName());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param bfiAddress
	 * @param bfiInfoDefaultId
	 * @return
	 * @throws SQLException
	 */
	public Long insertBfiAddress(BfiAddress bfiAddress, Long bfiInfoDefaultId) throws SQLException {
		Long bfiAddressId = 0L;
		final String sql = "Insert into bfi_addresses (country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,po_box_number,pin_zip_number,notes,bfi_info_id,address_type)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, bfiAddress.getCountry());
			ps.setString(2, bfiAddress.getState());
			ps.setString(3, bfiAddress.getProvince());
			ps.setString(4, bfiAddress.getDistrict());
			ps.setString(5, bfiAddress.getMnVdc());
			ps.setString(6, bfiAddress.getTownCityVillage());
			ps.setString(7, bfiAddress.getTole());
			ps.setString(8, bfiAddress.getStreet());
			ps.setString(9, bfiAddress.getHouseNumber());
			ps.setString(10, bfiAddress.getPoBoxNumber());
			ps.setString(11, bfiAddress.getPinZipNumber());
			ps.setString(12, bfiAddress.getNotes());
			ps.setLong(13, bfiInfoDefaultId);
			ps.setString(14, bfiAddress.getAddressType());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				bfiAddressId = rs.getLong(1);
				bfiAddress.setId(bfiAddressId);
			}
		} finally {
			ps.close();
			connection.close();
		}

		return bfiAddressId;

	}

	/**
	 * @param bfiAddress
	 * @param user
	 * @throws SQLException
	 */
	public void insertIntoBfiAddressUpdate(BfiAddress bfiAddress, User user) throws SQLException {
		final BfiAddressUpdate update = new BfiAddressUpdate(bfiAddress);
		final String sql = "Insert into bfi_addresses_update" + "(bfi_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,"
				+ "po_box_number,pin_zip_number,notes,approved,update_date,approved_date,reason,maker,checker,address_type)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, update.getId());
			ps.setString(2, update.getCountry());
			ps.setString(3, update.getState());
			ps.setString(4, update.getProvince());
			ps.setString(5, update.getDistrict());
			ps.setString(6, update.getMnVdc());
			ps.setString(7, update.getTownCityVillage());
			ps.setString(8, update.getTole());
			ps.setString(9, update.getStreet());
			ps.setString(10, update.getHouseNumber());
			ps.setString(11, update.getPoBoxNumber());
			ps.setString(12, update.getPinZipNumber());
			ps.setString(13, update.getNotes());
			ps.setBoolean(14, update.isApproved());
			ps.setDate(15, update.getUpdateDate());
			ps.setDate(16, update.getApprovedDate());
			ps.setString(17, update.getReason());
			ps.setString(18, user.getUserName());
			ps.setString(19, user.getUserName());
			ps.setString(20, bfiAddress.getAddressType());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param bfiContact
	 * @param bfiInfoDefaultId
	 * @return
	 * @throws SQLException
	 */
	public Long insertBfiContact(BfiContact bfiContact, Long bfiInfoDefaultId) throws SQLException {
		final String sql = "INSERT INTO bfi_contacts (contact_number ,contact_country_prefix ,contact_extension ,"
				+ "contact_communication_type,notes,bfi_info_id )" + "VALUES(?,?,?,?,?,?)";
		Long bfiId = 0L;
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, bfiContact.getContactNumber());
			ps.setString(2, bfiContact.getContactCountryPrefix());
			ps.setString(3, bfiContact.getContactExtension());
			ps.setString(4, bfiContact.getContactCommunicationType());

			ps.setString(5, bfiContact.getNotes());
			ps.setLong(6, bfiInfoDefaultId);
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				bfiId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return bfiId;

	}

	/**
	 * @param bfiContact
	 * @param user
	 * @throws SQLException
	 */
	public void insertIntoBfiContactUpdate(final BfiContact bfiContact, User user) throws SQLException {
		final BfiContactUpdate update = new BfiContactUpdate(bfiContact);
		final String sql = "INSERT INTO bfi_contacts_update"
				+ "(bfi_info_id ,contact_number ,contact_country_prefix ,contact_extension ,"
				+ "contact_communication_type,notes,approved,update_date,approved_date,reason,maker,checker )"
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, update.getId());
			ps.setString(2, update.getContactNumber());
			ps.setString(3, update.getContactCountryPrefix());
			ps.setString(4, update.getContactExtension());
			ps.setString(5, update.getContactCommunicationType());
			ps.setString(6, update.getNotes());
			ps.setBoolean(7, update.isApproved());
			ps.setDate(8, update.getUpdateDate());
			ps.setDate(9, update.getApprovedDate());
			ps.setString(10, update.getReason());
			ps.setString(11, user.getUserName());
			ps.setString(12, user.getUserName());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param bfiEmail
	 * @param bfiInfoDefaultId
	 * @return
	 * @throws SQLException
	 */
	public Long insertBfiEmail(BfiEmail bfiEmail, Long bfiInfoDefaultId) throws SQLException {
		final String sql = "INSERT INTO bfi_emails (email,notes,bfi_info_id) VALUES(?,?,?)";
		Long bfiEmailId = 0L;

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, bfiEmail.getEmail());
			ps.setString(2, bfiEmail.getNotes());
			ps.setLong(3, bfiInfoDefaultId);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				bfiEmailId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}
		return bfiEmailId;
	}

	/**
	 * @param bfiEmail
	 * @param user
	 * @throws SQLException
	 */
	public void insertIntoBfiEmailUpdate(final BfiEmail bfiEmail, User user) throws SQLException {
		final BfiEmailUpdate update = new BfiEmailUpdate(bfiEmail);
		final String sql = "Insert into bfi_emails_update"
				+ " (bfi_info_id,email,notes,approved,update_date,approved_date,reason,maker,checker)"
				+ "values (?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, update.getId());
			ps.setString(2, update.getEmail());
			ps.setString(3, update.getNotes());
			ps.setBoolean(4, update.isApproved());
			ps.setDate(5, update.getUpdateDate());
			ps.setDate(6, update.getApprovedDate());
			ps.setString(7, update.getReason());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getUserName());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param bfiInfo
	 * @param validateUser
	 * @return
	 * @throws SQLException
	 */
	public Long insertIntoBfiInfo(BfiInfo bfiInfo, User validateUser) throws SQLException {
		String sql = "INSERT INTO bfi_info (bfi_name,fiu_reference_no,bank_code,"
				+ "local_currency,notes) VALUES(?,?,?,?,?)";
		Long bfiInfoId = 0L;
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, bfiInfo.getBfiName());
			ps.setString(2, bfiInfo.getFiuReferenceNo());
			ps.setString(3, bfiInfo.getBankCode());
			ps.setString(4, bfiInfo.getLocalCurrency());
			ps.setString(5, bfiInfo.getNotes());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				bfiInfoId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}
		return bfiInfoId;
	}

	/**
	 * @param bfiAddress
	 * @param bfiInfoId
	 * @throws SQLException
	 */
	public void updateBfiAddress(BfiAddress bfiAddress, Long bfiInfoId) throws SQLException {
		final String sql = "UPDATE bfi_addresses set bfi_info_id=?,country=?,state=?,province=?,district=?,mn_vdc=?,"
				+ "town_city_village=?,tole=?,street=?,house_number=?,"
				+ "po_box_number=?,pin_zip_number=?,notes=?,address_type=? WHERE id =?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, bfiInfoId);
			ps.setString(2, bfiAddress.getCountry());
			ps.setString(3, bfiAddress.getState());
			ps.setString(4, bfiAddress.getProvince());
			ps.setString(5, bfiAddress.getDistrict());
			ps.setString(6, bfiAddress.getMnVdc());
			ps.setString(7, bfiAddress.getTownCityVillage());
			ps.setString(8, bfiAddress.getTole());
			ps.setString(9, bfiAddress.getStreet());
			ps.setString(10, bfiAddress.getHouseNumber());
			ps.setString(11, bfiAddress.getPoBoxNumber());
			ps.setString(12, bfiAddress.getPinZipNumber());
			ps.setString(13, bfiAddress.getNotes());
			ps.setString(14, bfiAddress.getAddressType());
			ps.setLong(15, bfiAddress.getId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param bfiContact
	 * @param bfiInfoId
	 * @throws SQLException
	 */
	public void updateBfiContact(BfiContact bfiContact, Long bfiInfoId) throws SQLException {
		final String sql = "UPDATE bfi_contacts set contact_number=? ,contact_country_prefix=? ,contact_extension=? ,"
				+ "contact_communication_type=?,notes=?,bfi_info_id=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, bfiContact.getContactNumber());
			ps.setString(2, bfiContact.getContactCountryPrefix());
			ps.setString(3, bfiContact.getContactExtension());
			ps.setString(4, bfiContact.getContactCommunicationType());
			ps.setString(5, bfiContact.getNotes());
			ps.setLong(6, bfiInfoId);
			ps.setLong(7, bfiContact.getId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param bfiEmail
	 * @param bfiInfoId
	 * @throws SQLException
	 */
	public void updateBfiEmail(BfiEmail bfiEmail, Long bfiInfoId) throws SQLException {
		final String sql = "UPDATE bfi_emails set email=?,notes=?,bfi_info_id=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, bfiEmail.getEmail());
			ps.setString(2, bfiEmail.getNotes());
			ps.setLong(3, bfiInfoId);
			ps.setLong(4, bfiEmail.getId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

}
