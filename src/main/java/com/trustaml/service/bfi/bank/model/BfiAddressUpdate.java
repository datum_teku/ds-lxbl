package com.trustaml.service.bfi.bank.model;

import java.sql.Date;
import java.time.LocalDate;

public class BfiAddressUpdate {
	private long id;
	private String country;
	private String state;
	private String province;
	private String district;
	private String mnVdc;
	private String townCityVillage;
	private String tole;
	private String street;
	private String houseNumber;
	private String poBoxNumber;
	private String pinZipNumber;
	private String notes;
	
	private int makerId;
	private int checkerId;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;
	
	public BfiAddressUpdate(long id, String country, String state, String province, String district, String mnVdc,
			String townCityVillage, String tole, String street, String houseNumber, String poBoxNumber,
			String pinZipNumber, String notes,
			int makerId,int checkerId,boolean approved,Date updateDate,Date approvedDate,String reason) {
		super();
		this.id = id;
		this.country = country;
		this.state = state;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.townCityVillage = townCityVillage;
		this.tole = tole;
		this.street = street;
		this.houseNumber = houseNumber;
		this.poBoxNumber = poBoxNumber;
		this.pinZipNumber = pinZipNumber;
		this.notes = notes;
		
		this.makerId = makerId;
		this.checkerId = checkerId;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
		this.reason = reason;
	}

	public BfiAddressUpdate(BfiAddress info) {
		super();
	
		this.id = info.getId();
		this.country = info.getCountry();
		this.state = info.getState();
		this.province = info.getProvince();
		this.district = info.getDistrict();
		this.mnVdc = info.getMnVdc();
		this.townCityVillage = info.getTownCityVillage();
		this.tole = info.getTole();
		this.street = info.getStreet();
		this.houseNumber = info.getHouseNumber();
		this.poBoxNumber = info.getPoBoxNumber();
		this.pinZipNumber = info.getPinZipNumber();
		this.notes = info.getNotes();
		
		this.approved = true;
		this.updateDate = java.sql.Date.valueOf(LocalDate.now());
		this.approvedDate = java.sql.Date.valueOf(LocalDate.now());
		this.reason = "reason";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getTownCityVillage() {
		return townCityVillage;
	}

	public void setTownCityVillage(String townCityVillage) {
		this.townCityVillage = townCityVillage;
	}

	public String getTole() {
		return tole;
	}

	public void setTole(String tole) {
		this.tole = tole;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getPoBoxNumber() {
		return poBoxNumber;
	}

	public void setPoBoxNumber(String poBoxNumber) {
		this.poBoxNumber = poBoxNumber;
	}

	public String getPinZipNumber() {
		return pinZipNumber;
	}

	public void setPinZipNumber(String pinZipNumber) {
		this.pinZipNumber = pinZipNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getMakerId() {
		return makerId;
	}

	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}

	public int getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(int checkerId) {
		this.checkerId = checkerId;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
