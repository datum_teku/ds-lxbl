package com.trustaml.service.bfi.bank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BfiContact {

	private long id;
	@JsonProperty("contact-number")
	private String contactNumber;
	@JsonProperty("contact-country-prefix")
	private String contactCountryPrefix;
	@JsonProperty("contact-extension")
	private String contactExtension;
	@JsonProperty("contact-communication-type")
	private String contactCommunicationType;
	@JsonProperty("notes")
	private String notes;
	
	public BfiContact() {
		super();
		this.contactNumber = "";
		this.contactCountryPrefix = "";
		this.contactExtension = "";
		this.contactCommunicationType = "";		
		this.notes = "";
	}
	public BfiContact(long id, String contactNumber, String contactCountryPrefix, String contactExtension,
			String contactCommunicationType, String notes) {
		super();
		this.id = id;
		this.contactNumber = contactNumber;
		this.contactCountryPrefix = contactCountryPrefix;
		this.contactExtension = contactExtension;
		this.contactCommunicationType = contactCommunicationType;		
		this.notes = notes;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactCountryPrefix() {
		return contactCountryPrefix;
	}
	public void setContactCountryPrefix(String contactCountryPrefix) {
		this.contactCountryPrefix = contactCountryPrefix;
	}
	public String getContactExtension() {
		return contactExtension;
	}
	public void setContactExtension(String contactExtension) {
		this.contactExtension = contactExtension;
	}
	public String getContactCommunicationType() {
		return contactCommunicationType;
	}
	public void setContactCommunicationType(String contactCommunicationType) {
		this.contactCommunicationType = contactCommunicationType;
	}	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
