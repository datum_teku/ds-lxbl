package com.trustaml.service.bfi.bank.model;
import java.sql.Date;
import java.time.LocalDate;

public class BfiContactUpdate {

	private long id;
	private String contactNumber;
	private String contactCountryPrefix;
	private String contactExtension;
	private String contactCommunicationType;	
	private String notes;
	
	private int makerId;
	private int checkerId;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;
	
	public BfiContactUpdate(BfiContact bfiContact) {
		super();
		
		this.id = bfiContact.getId();
		this.contactNumber = bfiContact.getContactNumber();
		this.contactCountryPrefix = bfiContact.getContactCountryPrefix();
		this.contactExtension = bfiContact.getContactExtension();
		this.contactCommunicationType = bfiContact.getContactCommunicationType();		
		this.notes = bfiContact.getNotes();
		
		this.approved = true;
		this.updateDate = java.sql.Date.valueOf(LocalDate.now());
		this.approvedDate = java.sql.Date.valueOf(LocalDate.now());
		this.reason = "reason";
	}
	public BfiContactUpdate(long id, String contactNumber, String contactCountryPrefix, String contactExtension,
			String contactCommunicationType, String notes,
			int makerId,int checkerId,boolean approved,Date updateDate,Date approvedDate,String reason) {
		super();
		this.id = id;
		this.contactNumber = contactNumber;
		this.contactCountryPrefix = contactCountryPrefix;
		this.contactExtension = contactExtension;
		this.contactCommunicationType = contactCommunicationType;		
		this.notes = notes;
		this.makerId = makerId;
		this.checkerId = checkerId;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
		this.reason = reason;

	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactCountryPrefix() {
		return contactCountryPrefix;
	}
	public void setContactCountryPrefix(String contactCountryPrefix) {
		this.contactCountryPrefix = contactCountryPrefix;
	}
	public String getContactExtension() {
		return contactExtension;
	}
	public void setContactExtension(String contactExtension) {
		this.contactExtension = contactExtension;
	}
	public String getContactCommunicationType() {
		return contactCommunicationType;
	}
	public void setContactCommunicationType(String contactCommunicationType) {
		this.contactCommunicationType = contactCommunicationType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getMakerId() {
		return makerId;
	}
	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}
	public int getCheckerId() {
		return checkerId;
	}
	public void setCheckerId(int checkerId) {
		this.checkerId = checkerId;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
