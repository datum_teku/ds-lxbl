package com.trustaml.service.bfi.bank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BfiEmail {

	private long id;
	@JsonProperty("email")
	private String email;
	@JsonProperty("notes")
	private String notes;
	public BfiEmail() {
		super();
		this.id = 0;
		this.email = "";
		this.notes = "";

	}
	public BfiEmail(long id, String email, String notes) {
		super();
		this.id = id;
		this.email = email;
		this.notes = notes;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
