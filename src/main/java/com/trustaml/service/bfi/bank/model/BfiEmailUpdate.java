package com.trustaml.service.bfi.bank.model;

import java.sql.Date;
import java.time.LocalDate;

public class BfiEmailUpdate {

	private long id;
	private String email;
	private String notes;

	private int makerId;
	private int checkerId;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;

	public BfiEmailUpdate(BfiEmail bfiEmail) {
		super();
		this.id = bfiEmail.getId();
		this.email = bfiEmail.getEmail();
		this.notes = bfiEmail.getNotes();

		this.approved = true;
		this.updateDate = java.sql.Date.valueOf(LocalDate.now());
		this.approvedDate = java.sql.Date.valueOf(LocalDate.now());
		this.reason = "reason";
	}

	public BfiEmailUpdate(long id, String email, String notes, int makerId, int checkerId, boolean approved,
			Date updateDate, Date approvedDate, String reason) {
		super();
		this.id = id;
		this.email = email;
		this.notes = notes;

		this.makerId = makerId;
		this.checkerId = checkerId;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
		this.reason = reason;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getMakerId() {
		return makerId;
	}

	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}

	public int getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(int checkerId) {
		this.checkerId = checkerId;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
