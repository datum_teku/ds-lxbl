package com.trustaml.service.bfi.bank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BfiInfo {

	private long id;
	@JsonProperty("bfi-name")
	private String bfiName;
	@JsonProperty("fiu-reference-number")
	private String fiuReferenceNo;
	@JsonProperty("bank-code")
	private String bankCode;
	@JsonProperty("local-currency")
	private String localCurrency;

	@JsonProperty("notes")
	private String notes;

	public BfiInfo() {
		super();

		// this.bfiName = "bfiName";
		// this.fiuReferenceNo = "fiuReferenceNo";
		// this.bankCode = "bankCode";
		// this.localCurrency = "AED";
		// this.addressType = "Rent";
		// this.notes = "notes";
	}

	public BfiInfo(long id, String bfiName, String fiuReferenceNo, String bankCode, String localCurrency,
			String notes) {
		super();
		this.id = id;
		this.bfiName = bfiName;
		this.fiuReferenceNo = fiuReferenceNo;
		this.bankCode = bankCode;
		this.localCurrency = localCurrency;
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBfiName() {
		return bfiName;
	}

	public void setBfiName(String bfiName) {
		this.bfiName = bfiName;
	}

	public String getFiuReferenceNo() {
		return fiuReferenceNo;
	}

	public void setFiuReferenceNo(String fiuReferenceNo) {
		this.fiuReferenceNo = fiuReferenceNo;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}