package com.trustaml.service.bfi.bank.model;

import java.sql.Date;
import java.time.LocalDate;

public class BfiInfoUpdate {

	private long id;
	private String bfiName;
	private String fiuReferenceNo;
	private String bankCode;
	private String localCurrency;
	private String notes;

	private int makerId;
	private int checkerId;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;

	public BfiInfoUpdate(BfiInfo obj) {
		super();

		this.id = obj.getId();
		this.bfiName = obj.getBfiName();
		this.fiuReferenceNo = obj.getFiuReferenceNo();
		this.bankCode = obj.getBankCode();
		this.localCurrency = obj.getLocalCurrency();
		this.notes = obj.getNotes();
		this.approved = true;
		this.updateDate = java.sql.Date.valueOf(LocalDate.now());
		this.approvedDate = java.sql.Date.valueOf(LocalDate.now());
		this.reason = "reason";
	}

	public BfiInfoUpdate(long id, String bfiName, String fiuReferenceNo, String bankCode, String localCurrency,
			String notes, int makerId, int checkerId, boolean approved, Date updateDate, Date approvedDate,
			String reason) {
		super();
		this.id = id;
		this.bfiName = bfiName;
		this.fiuReferenceNo = fiuReferenceNo;
		this.bankCode = bankCode;
		this.localCurrency = localCurrency;
		this.notes = notes;

		this.makerId = makerId;
		this.checkerId = checkerId;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
		this.reason = reason;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBfiName() {
		return bfiName;
	}

	public void setBfiName(String bfiName) {
		this.bfiName = bfiName;
	}

	public String getFiuReferenceNo() {
		return fiuReferenceNo;
	}

	public void setFiuReferenceNo(String fiuReferenceNo) {
		this.fiuReferenceNo = fiuReferenceNo;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getMakerId() {
		return makerId;
	}

	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}

	public int getCheckerId() {
		return checkerId;
	}

	public void setCheckerId(int checkerId) {
		this.checkerId = checkerId;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}