package com.trustaml.service.bfi.bank.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.bfi.bank.model.BfiAddress;
import com.trustaml.service.bfi.bank.model.BfiContact;
import com.trustaml.service.bfi.bank.model.BfiEmail;
import com.trustaml.service.bfi.bank.model.BfiInfo;
import com.trustaml.service.common.dto.User;

public class JsonStringToBfiObjects {

	final static public BfiInfo getBfiInfoObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		BfiInfo bfiInfo = new BfiInfo();
		ObjectMapper mapper = new ObjectMapper();
		// DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		// mapper.setDateFormat(df);
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode bfiInfoNode = jsonNode.get("bfi-info");

		bfiInfo = mapper.treeToValue(bfiInfoNode, BfiInfo.class);

		return bfiInfo;
	}

	final static public List<BfiContact> getBfiContactObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		List<BfiContact> bfiContactList = new ArrayList<BfiContact>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode bfiContactNode = jsonNode.get("bfi-contact-info");
		if (bfiContactNode != null) {
			for (JsonNode node : bfiContactNode) {
				BfiContact bfiContact = new BfiContact();
				bfiContact = mapper.treeToValue(node, BfiContact.class);

				bfiContactList.add(bfiContact);
			}

		}

		return bfiContactList;
	}

	final static public List<BfiEmail> getBfiEmailObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		List<BfiEmail> bfiEmailList = new ArrayList<BfiEmail>();

		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode bfiEmailNode = jsonNode.get("bfi-email-info");
		if (bfiEmailNode != null) {
			for (JsonNode node : bfiEmailNode) {
				BfiEmail bfiEmail = new BfiEmail();
				bfiEmail = mapper.treeToValue(node, BfiEmail.class);

				bfiEmailList.add(bfiEmail);
			}

		}

		return bfiEmailList;
	}

	final static public List<BfiAddress> getBfiAddressObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		List<BfiAddress> bfiAddressList = new ArrayList<BfiAddress>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode bfiAddressNode = jsonNode.get("bfi-address-info");
		if (bfiAddressNode != null) {
			for (JsonNode node : bfiAddressNode) {
				BfiAddress bfiAddress = new BfiAddress();
				bfiAddress = mapper.treeToValue(node, BfiAddress.class);
				bfiAddressList.add(bfiAddress);
			}
		}

		return bfiAddressList;
	}

	public static User getUserObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode userNode = jsonNode.get("user");
		User user = mapper.treeToValue(userNode, User.class);
		return user;
	}
}
