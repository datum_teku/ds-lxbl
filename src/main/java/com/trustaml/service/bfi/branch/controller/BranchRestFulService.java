package com.trustaml.service.bfi.branch.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.bfi.branch.dao.BranchDao;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;

@Path("/branch")
public class BranchRestFulService {

	@Inject
	BranchDao branchDao;

	/**
	 * @param data
	 * @return update successful
	 * @description accepts string JSON and update branch data
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws TrustAmlEmptyJSONException
	 * @throws NoSuchAlgorithmException 
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAndInsertBranch(String data)
			throws SQLException, JsonParseException, JsonMappingException, IOException, TrustAmlEmptyJSONException, NoSuchAlgorithmException {
		if (!data.isEmpty() || !data.equals("")) {
			branchDao.updateAndInsert(data);
			return ResponseReturn.sucess("update SucessFul");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param data
	 * @return save successful
	 * @description save branch info into multiple table
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws NoSuchAlgorithmException 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveBranch(String data)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException, NoSuchAlgorithmException {
		if (!data.isEmpty() || !data.equals("")) {
			branchDao.insertBranchInfo(data);
			return ResponseReturn.sucess("save SucessFul");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}

	}

	/**
	 * @return all branch info
	 * @description uses procedure branch_info
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllBranch() throws SQLException {
		return ResponseReturn.sucess(branchDao.getAllBranchInfo());

	}

	/**
	 * @param branchId
	 * @return branch info by branch id
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/id/{branchId}")
	public Response getBranchById(@PathParam("branchId") Long branchId) throws SQLException {
		return ResponseReturn.sucess(branchDao.getDetailBranchInfo(branchId));
	}

	/**
	 * @param branchId
	 * @return conditional true of false
	 * @description check if sol id for given branch id exits
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sol-id/{branchId}")
	public Response checkIfBranchIdExist(@PathParam("branchId") String branchId) throws SQLException {
		return ResponseReturn.sucess(branchDao.checkIfBranchIdExist(branchId));
	}

	/**
	 * @param referenceNuIdmber
	 * @return conditional true or false
	 * @description check if fiu-reference-number exists in branch_info
	 * @throws SQLException
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/reference-number/{referenceNuIdmber}")
	public Response checkIfReferenceNumberExist(@PathParam("referenceNuIdmber") String referenceNuIdmber)
			throws SQLException {
		return ResponseReturn.sucess(branchDao.checkIfReferenceNumberExist(referenceNuIdmber));
	}

}
