package com.trustaml.service.bfi.branch.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.bfi.branch.model.BranchAddress;
import com.trustaml.service.bfi.branch.model.BranchContact;
import com.trustaml.service.bfi.branch.model.BranchEmail;
import com.trustaml.service.bfi.branch.model.BranchInfo;
import com.trustaml.service.bfi.branch.model.DetailBranchInfo;
import com.trustaml.service.common.HashCodeGenerator;

@Stateless
public class BranchDao {

	@Inject
	BranchDaoImpl branchDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	/**
	 * @param data
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public void updateAndInsert(String data)
			throws SQLException, JsonParseException, JsonMappingException, IOException, NoSuchAlgorithmException {

		DetailBranchInfo detail = new ObjectMapper().readValue(data, DetailBranchInfo.class);
		List<BranchAddress> addresses = detail.getListAddress();
		List<BranchContact> contacts = detail.getListContact();
		List<BranchEmail> emails = detail.getListEmail();
		// NEEDS TO BE PARSED FROM JSON

		boolean includeUser = true;
		Long bfiInfoId = 1L;
		boolean doNotIncludeUser = false;
		if (detail.getBranchInfo().isChange()) {
			String branchHashValue = hashCodeGenerator.generatehashValueForBranchInfo(detail.getBranchInfo(), bfiInfoId,
					detail.getUser(), includeUser);
			String branchUpdateHashValue = hashCodeGenerator.generatehashValueForBranchInfo(detail.getBranchInfo(),
					detail.getBranchInfo().getBfId(), detail.getUser(), doNotIncludeUser);
			branchDaoImpl.updateBranchInfo(detail.getBranchInfo(), branchHashValue);
			branchDaoImpl.insertBranchInfoUpdate(detail.getBranchInfo(), detail.getUser(), branchUpdateHashValue);
		}

		for (BranchAddress address : addresses) {
			String branchAddressHashValue = hashCodeGenerator.generateHashValueForBranchAddress(address,
					detail.getUser(), doNotIncludeUser);
			String branchAddressUpdateHashValue = hashCodeGenerator.generateHashValueForBranchAddress(address,
					detail.getUser(), includeUser);
			if (address.getId() != 0) {
				if (address.isChange()) {
					address.setBranchId(detail.getBranchInfo().getId());
					branchDaoImpl.updateBranchAddress(address, branchAddressHashValue);
					branchDaoImpl.insertBranchAddressUpdate(address, detail.getUser(), branchAddressHashValue);
				}
			} else {
				address.setBranchId(detail.getBranchInfo().getId());
				branchDaoImpl.insertBranchAddress(address, branchAddressHashValue);
				branchDaoImpl.insertBranchAddressUpdate(address, detail.getUser(), branchAddressUpdateHashValue);
			}
		}
		for (BranchContact contact : contacts) {
			String branchAddressHashValue = hashCodeGenerator.generateHashValueForBranchContact(contact,
					detail.getUser(), doNotIncludeUser);
			String branchAddressUpdateHashValue = hashCodeGenerator.generateHashValueForBranchContact(contact,
					detail.getUser(), includeUser);
			if (contact.getId() != 0) {
				if (contact.isChange()) {
					contact.setBranchId(detail.getBranchInfo().getId());
					branchDaoImpl.updateBranchContact(contact, branchAddressHashValue);
					branchDaoImpl.insertBranchContactUpdate(contact, detail.getUser(), branchAddressUpdateHashValue);
				}
			} else {
				contact.setBranchId(detail.getBranchInfo().getId());
				branchDaoImpl.insertBranchContact(contact, branchAddressHashValue);
				branchDaoImpl.insertBranchContactUpdate(contact, detail.getUser(), branchAddressUpdateHashValue);
			}
		}
		for (BranchEmail email : emails) {
			String emailHashValue = hashCodeGenerator.generateHashValueForEmail(email, detail.getUser(),
					doNotIncludeUser);
			String emailUpdateHashValue = hashCodeGenerator.generateHashValueForEmail(email, detail.getUser(),
					includeUser);
			if (email.getId() != 0) {
				if (email.isChange()) {
					email.setBranchId(detail.getBranchInfo().getId());
					branchDaoImpl.updateBranchEmail(email, emailHashValue);
					branchDaoImpl.insertBranchEmailUpdate(email, detail.getUser(), emailUpdateHashValue);
				}
			} else {
				email.setBranchId(detail.getBranchInfo().getId());
				branchDaoImpl.insertBranchEmail(email, emailHashValue);
				branchDaoImpl.insertBranchEmailUpdate(email, detail.getUser(), emailUpdateHashValue);
			}

		}
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public Object getAllBranchInfo() throws SQLException {
		// TODO Auto-generated method stub
		return branchDaoImpl.getAllBranchInfo();
	}

	/**
	 * @param branchId
	 * @return
	 * @throws SQLException
	 */
	public DetailBranchInfo getDetailBranchInfo(Long branchId) throws SQLException {
		DetailBranchInfo details = new DetailBranchInfo();
		details.setBranchInfo(branchDaoImpl.getBranchInfo(branchId));
		details.setListAddress(branchDaoImpl.getBranchAddress(branchId));
		details.setListContact(branchDaoImpl.getBranchContact(branchId));
		details.setListEmail(branchDaoImpl.getBranchEmail(branchId));
		return details;
	}

	/**
	 * @param data
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public void insertBranchInfo(String data)
			throws SQLException, JsonParseException, JsonMappingException, IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		DetailBranchInfo branchDetail = mapper.readValue(data, DetailBranchInfo.class);
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		BranchInfo branchInfo = branchDetail.getBranchInfo();
		Long bfiInfoId = 1L;
		String branchHashValue = hashCodeGenerator.generatehashValueForBranchInfo(branchDetail.getBranchInfo(),
				bfiInfoId, branchDetail.getUser(), includeUser);
		Long branchInfoId = branchDaoImpl.insertBranchInfo(branchDetail.getBranchInfo(), bfiInfoId, branchHashValue);
		branchInfo.setId(branchInfoId);
		branchInfo.setBfId(branchInfoId);
		// NEEDS TO BE PARSED FROM JSON
		// String reason = "some reason";

		String branchUpdateHashValue = hashCodeGenerator.generatehashValueForBranchInfo(branchDetail.getBranchInfo(),
				branchInfo.getBfId(), branchDetail.getUser(), doNotIncludeUser);
		branchDaoImpl.insertBranchInfoUpdate(branchInfo, branchDetail.getUser(), branchUpdateHashValue);
		List<BranchAddress> addresses = branchDetail.getListAddress();
		List<BranchContact> contacts = branchDetail.getListContact();
		List<BranchEmail> emails = branchDetail.getListEmail();

		for (BranchAddress branchAddress : addresses) {
			String branchAddressHashValue = hashCodeGenerator.generateHashValueForBranchAddress(branchAddress,
					branchDetail.getUser(), doNotIncludeUser);
			branchAddress.setBranchId(branchInfoId);
			branchAddress.setId(branchDaoImpl.insertBranchAddress(branchAddress, branchAddressHashValue));
			String branchAddressUpdateHashValue = hashCodeGenerator.generateHashValueForBranchAddress(branchAddress,
					branchDetail.getUser(), includeUser);
			branchDaoImpl.insertBranchAddressUpdate(branchAddress, branchDetail.getUser(),
					branchAddressUpdateHashValue);
		}
		for (BranchContact branchContact : contacts) {
			String branchAddressHashValue = hashCodeGenerator.generateHashValueForBranchContact(branchContact,
					branchDetail.getUser(), doNotIncludeUser);
			branchContact.setBranchId(branchInfoId);
			branchContact.setId(branchDaoImpl.insertBranchContact(branchContact, branchAddressHashValue));
			String branchAddressUpdateHashValue = hashCodeGenerator.generateHashValueForBranchContact(branchContact,
					branchDetail.getUser(), includeUser);
			branchDaoImpl.insertBranchContactUpdate(branchContact, branchDetail.getUser(),
					branchAddressUpdateHashValue);
		}
		for (BranchEmail branchEmail : emails) {
			String emailHashValue = hashCodeGenerator.generateHashValueForEmail(branchEmail, branchDetail.getUser(),
					doNotIncludeUser);
			String emailUpdateHashValue = hashCodeGenerator.generateHashValueForEmail(branchEmail,
					branchDetail.getUser(), includeUser);
			branchEmail.setBranchId(branchInfoId);
			branchEmail.setId(branchDaoImpl.insertBranchEmail(branchEmail, emailHashValue));
			branchDaoImpl.insertBranchEmailUpdate(branchEmail, branchDetail.getUser(), emailUpdateHashValue);
		}

	}

	/**
	 * @param branchId
	 * @return
	 * @throws SQLException
	 */
	public Object checkIfBranchIdExist(String branchId) throws SQLException {
		// TODO Auto-generated method stub
		return branchDaoImpl.checkIfBranchIdExist(branchId);
	}

	/**
	 * @param referenceId
	 * @return
	 * @throws SQLException
	 */
	public Object checkIfReferenceNumberExist(String referenceId) throws SQLException {
		// TODO Auto-generated method stub
		return branchDaoImpl.checkIfReferenceNumberExist(referenceId);
	}
}
