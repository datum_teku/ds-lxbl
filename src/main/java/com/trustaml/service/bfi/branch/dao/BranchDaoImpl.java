package com.trustaml.service.bfi.branch.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.trustaml.service.bfi.branch.model.BranchAddress;
import com.trustaml.service.bfi.branch.model.BranchContact;
import com.trustaml.service.bfi.branch.model.BranchEmail;
import com.trustaml.service.bfi.branch.model.BranchInfo;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;

public class BranchDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @return
	 * @throws SQLException
	 */
	public Object getAllBranchInfo() throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call branch_info(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public BranchInfo getBranchInfo(Long id) throws SQLException {
		BranchInfo branchInfo = new BranchInfo();
		String sql = "select * from branch_info where id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				branchInfo = new BranchInfo(rs.getLong("id"), rs.getLong("bfi_info_id"), rs.getString("branch_name"),
						rs.getString("fiu_reference_no"), rs.getString("sol_id"), rs.getString("local_currency"),
						rs.getString("address_type"), rs.getString("notes"), rs.getString("address"));
			}
		} finally {
			ps.close();
			connection.close();

		}
		return branchInfo;
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<BranchAddress> getBranchAddress(Long id) throws SQLException {
		String sql = "select * from branch_addresses where branch_info_id = ?";
		List<BranchAddress> list = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BranchAddress address = new BranchAddress(rs.getLong("id"), rs.getLong("branch_info_id"),
						rs.getString("address_type"), rs.getString("country"), rs.getString("state"),
						rs.getString("province"), rs.getString("district"), rs.getString("mn_vdc"),
						rs.getString("town_city_village"), rs.getString("tole"), rs.getString("street"),
						rs.getString("house_number"), rs.getString("po_box_number"), rs.getString("pin_zip_number"),
						rs.getString("notes"));
				list.add(address);
			}
		} finally {
			ps.close();
			connection.close();

		}
		return list;
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<BranchContact> getBranchContact(Long id) throws SQLException {
		String sql = "select * from branch_contacts where branch_info_id = ?";
		List<BranchContact> list = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BranchContact contact = new BranchContact(rs.getLong("id"), rs.getLong("branch_info_id"),
						rs.getString("contact_type"), rs.getString("contact_number"),
						rs.getString("contact_country_prefix"), rs.getString("contact_extension"),
						rs.getString("contact_communication_type"), rs.getString("email_address"),
						rs.getString("notes"));
				list.add(contact);
			}
		} finally {
			ps.close();
			connection.close();

		}
		return list;
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public List<BranchEmail> getBranchEmail(Long id) throws SQLException {
		String sql = "select * from branch_emails where branch_info_id = ?";
		List<BranchEmail> list = new ArrayList<>();
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BranchEmail email = new BranchEmail(rs.getLong("id"), rs.getLong("branch_info_id"),
						rs.getString("email_id"), rs.getString("email_type"));
				list.add(email);
			}
		} finally {
			ps.close();
			connection.close();

		}

		return list;
	}

	/**
	 * @param branchInfo
	 * @param branchHashValue
	 * @throws SQLException
	 */
	public void updateBranchInfo(BranchInfo branchInfo, String branchHashValue) throws SQLException {
		String sql = "update branch_info set branch_name =?, fiu_reference_no = ?, sol_id=?,"
				+ "local_currency=?, address_type=?,notes=?,address=?,hash=? where id =?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, branchInfo.getName());
			ps.setString(2, branchInfo.getReference());
			ps.setString(3, branchInfo.getSolId());
			ps.setString(4, branchInfo.getLocalCurrency());
			ps.setString(5, branchInfo.getAddressType());
			ps.setString(6, branchInfo.getNotes());
			ps.setString(7, branchInfo.getAddress());
			ps.setString(8, branchHashValue);
			ps.setLong(9, branchInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();

		}

	}

	/**
	 * @param branchInfo
	 * @param user
	 * @param branchUpdateHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertBranchInfoUpdate(BranchInfo branchInfo, User user, String branchUpdateHashValue)
			throws SQLException {
		String sql = "insert into branch_info_update(branch_info_id, branch_name, fiu_reference_no,"
				+ "sol_id, local_currency, address_type, notes, maker, checker, approved, update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Long branchInfoId = 0L;
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, branchInfo.getBfId());
			ps.setString(2, branchInfo.getName());
			ps.setString(3, branchInfo.getReference());
			ps.setString(4, branchInfo.getSolId());
			ps.setString(5, branchInfo.getLocalCurrency());
			ps.setString(6, branchInfo.getAddressType());
			ps.setString(7, branchInfo.getNotes());
			ps.setString(8, "maker1");
			ps.setString(9, "checker");
			ps.setBoolean(10, true);
			ps.setDate(11, new java.sql.Date(new Date().getTime()));
			ps.setDate(12, new java.sql.Date(new Date().getTime()));
			ps.setString(13, "");
			ps.setString(14, branchUpdateHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				branchInfoId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();

		}
		return branchInfoId;
	}

	/**
	 * @param address
	 * @param branchAddressHashValue
	 * @throws SQLException
	 */
	public void updateBranchAddress(BranchAddress address, String branchAddressHashValue) throws SQLException {
		String sql = "update branch_addresses set address_type=?,country=?,state=?,province=?,"
				+ "district=?, mn_vdc=?, town_city_village=?, tole=?, street=?, house_number=?,"
				+ "po_box_number=?,pin_zip_number=?,notes=?,hash=? where id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, address.getAddressType());
			ps.setString(2, address.getCountry());
			ps.setString(3, address.getState());
			ps.setString(4, address.getProvince());
			ps.setString(5, address.getDistrict());
			ps.setString(6, address.getMnVdc());
			ps.setString(7, address.getTownCityVillage());
			ps.setString(8, address.getTole());
			ps.setString(9, address.getStreet());
			ps.setString(10, address.getHouseNumber());
			ps.setString(11, address.getPoBoxNumber());
			ps.setString(12, address.getPinZipNumber());
			ps.setString(13, address.getNotes());
			ps.setString(14, branchAddressHashValue);
			ps.setLong(15, address.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();

		}

	}

	/**
	 * @param branchAddress
	 * @param user
	 * @param branchAddressUpdateHashValue
	 * @throws SQLException
	 */
	public void insertBranchAddressUpdate(BranchAddress branchAddress, User user, String branchAddressUpdateHashValue)
			throws SQLException {
		String sql = "insert into branch_addresses_update(branch_info_id,address_type,country,"
				+ "state,province,district,mn_vdc,town_city_village,tole,street,house_number,"
				+ "po_box_number,pin_zip_number,notes,maker,checker,approved,update_date,"
				+ "approved_date,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, branchAddress.getBranchId());
			ps.setString(2, branchAddress.getAddressType());
			ps.setString(3, branchAddress.getCountry());
			ps.setString(4, branchAddress.getState());
			ps.setString(5, branchAddress.getProvince());
			ps.setString(6, branchAddress.getDistrict());
			ps.setString(7, branchAddress.getMnVdc());
			ps.setString(8, branchAddress.getTownCityVillage());
			ps.setString(9, branchAddress.getTole());
			ps.setString(10, branchAddress.getStreet());
			ps.setString(11, branchAddress.getHouseNumber());
			ps.setString(12, branchAddress.getPoBoxNumber());
			ps.setString(13, branchAddress.getPinZipNumber());
			ps.setString(14, branchAddress.getNotes());
			ps.setString(15, user.getUserName());
			ps.setString(16, user.getUserName());
			ps.setBoolean(17, true);
			ps.setDate(18, new java.sql.Date(new Date().getTime()));
			ps.setDate(19, new java.sql.Date(new Date().getTime()));
			ps.setString(20, branchAddressUpdateHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param contact
	 * @param branchAddressHashValue
	 * @throws SQLException
	 */
	public void updateBranchContact(BranchContact contact, String branchAddressHashValue) throws SQLException {
		String sql = "update branch_contacts set contact_type=?, contact_number=?,contact_country_prefix=?,"
				+ "contact_extension=?,contact_communication_type=?,email_address=?, notes=?,hash=? where id =?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, contact.getType());
			ps.setString(2, contact.getNumber());
			ps.setString(3, contact.getPrefix());
			ps.setString(4, contact.getExtension());
			ps.setString(5, contact.getCommunicationType());
			ps.setString(6, contact.getEmail());
			ps.setString(7, contact.getNotes());
			ps.setString(8, branchAddressHashValue);
			ps.setLong(9, contact.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param branchContact
	 * @param user
	 * @param branchAddressUpdateHashValue
	 * @throws SQLException
	 */
	public void insertBranchContactUpdate(BranchContact branchContact, User user, String branchAddressUpdateHashValue)
			throws SQLException {
		String sql = "insert into branch_contacts_update(branch_info_id,contact_type,contact_number,"
				+ "contact_country_prefix,contact_extension,contact_communication_type,email_address,"
				+ "notes,maker,checker,approved,update_date,approved_date,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, branchContact.getBranchId());
			ps.setString(2, branchContact.getType());
			ps.setString(3, branchContact.getNumber());
			ps.setString(4, branchContact.getPrefix());
			ps.setString(5, branchContact.getExtension());
			ps.setString(6, branchContact.getCommunicationType());
			ps.setString(7, branchContact.getEmail());
			ps.setString(8, branchContact.getNotes());
			ps.setString(9, user.getUserName());
			ps.setString(10, user.getUserName());
			ps.setBoolean(11, true);
			ps.setDate(12, new java.sql.Date(new Date().getTime()));
			ps.setDate(13, new java.sql.Date(new Date().getTime()));
			ps.setString(14, branchAddressUpdateHashValue);
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}
	}

	/**
	 * @param email
	 * @param emailHashValue
	 * @throws SQLException
	 */
	public void updateBranchEmail(BranchEmail email, String emailHashValue) throws SQLException {
		String sql = "update branch_emails set email_id=?, email_type=?,hash=? where id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, email.getEmail());
			ps.setString(2, email.getEmailType());
			ps.setString(3, emailHashValue);
			ps.setLong(4, email.getId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}
	}

	/**
	 * @param branchEmail
	 * @param user
	 * @param emailUpdateHashValue
	 * @throws SQLException
	 */
	public void insertBranchEmailUpdate(BranchEmail branchEmail, User user, String emailUpdateHashValue)
			throws SQLException {
		String sql = "insert into branch_emails_update(branch_info_id,email_id,email_type,"
				+ "maker,checker,approved,update_date,approved_date,hash) values(?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, branchEmail.getBranchId());
			ps.setString(2, branchEmail.getEmail());
			ps.setString(3, branchEmail.getEmailType());
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.setBoolean(6, true);
			ps.setDate(7, new java.sql.Date(new Date().getTime()));
			ps.setDate(8, new java.sql.Date(new Date().getTime()));
			ps.setString(9, emailUpdateHashValue);
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}
	}

	/**
	 * @param branchInfo
	 * @param user
	 * @throws SQLException
	 */
	public void insertBranchInfoUpdate(BranchInfo branchInfo, User user) throws SQLException {
		String sql = "insert into branch_info_update(branch_info_id, branch_name, fiu_reference_no,"
				+ "sol_id, local_currency, address_type, notes, maker, checker, approved, update_date, approved_date) values (?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = dbConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql);
		try {
			ps.setLong(1, branchInfo.getBfId());
			ps.setString(2, branchInfo.getName());
			ps.setString(3, branchInfo.getReference());
			ps.setString(4, branchInfo.getSolId());
			ps.setString(5, branchInfo.getLocalCurrency());
			ps.setString(6, branchInfo.getAddressType());
			ps.setString(7, branchInfo.getNotes());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getUserName());
			ps.setBoolean(10, true);
			ps.setDate(11, new java.sql.Date(new Date().getTime()));
			ps.setDate(12, new java.sql.Date(new Date().getTime()));
			ps.executeUpdate();
		} finally {
			conn.close();
			ps.close();
		}

	}

	/**
	 * @param branchInfo
	 * @param bfiInfoId
	 * @param branchHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertBranchInfo(BranchInfo branchInfo, Long bfiInfoId, String branchHashValue) throws SQLException {
		long branchInfoId = 0;
		if (branchInfo.getName() != null && branchInfo.getReference() != null && branchInfo.getSolId() != null) {
			String sql = "insert into branch_info (bfi_info_id,branch_name,fiu_reference_no,"
					+ "sol_id,local_currency,notes,hash) values(?,?,?,?,?,?,?)";
			Connection connection = null;
			PreparedStatement ps = null;
			try {
				connection = dbConnection.getConnection();
				ps = connection.prepareStatement(sql, new String[] { "id" });
				ps.setLong(1, bfiInfoId);
				ps.setString(2, branchInfo.getName());
				ps.setString(3, branchInfo.getReference());
				ps.setString(4, branchInfo.getSolId());
				ps.setString(5, branchInfo.getLocalCurrency());
				ps.setString(6, branchInfo.getNotes());
				ps.setString(7, branchHashValue);
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					branchInfoId = rs.getLong(1);
					// System.out.println("Gen ID : " + branchInfoId);
				}
			} finally {
				connection.close();
				ps.close();
			}
			return branchInfoId;
		}
		return branchInfoId;

	}

	/**
	 * @param branchAddress
	 * @param branchAddressHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertBranchAddress(BranchAddress branchAddress, String branchAddressHashValue) throws SQLException {
		String sql = "insert into branch_addresses"
				+ "(branch_info_id,country,state,province,district,mn_vdc,town_city_village,tole,street,house_number,po_box_number,pin_zip_number,notes,address_type,hash)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Long branchAddressId = 0l;
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, branchAddress.getBranchId());
			ps.setString(2, branchAddress.getCountry());
			ps.setString(3, branchAddress.getState());
			ps.setString(4, branchAddress.getProvince());
			ps.setString(5, branchAddress.getDistrict());
			ps.setString(6, branchAddress.getMnVdc());
			ps.setString(7, branchAddress.getTownCityVillage());
			ps.setString(8, branchAddress.getTole());
			ps.setString(9, branchAddress.getStreet());
			ps.setString(10, branchAddress.getHouseNumber());
			ps.setString(11, branchAddress.getPoBoxNumber());
			ps.setString(12, branchAddress.getPinZipNumber());
			ps.setString(13, branchAddress.getNotes());
			ps.setString(14, branchAddress.getAddressType());
			ps.setString(15, branchAddressHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				branchAddressId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}

		return branchAddressId;
	}

	/**
	 * @param branchContact
	 * @param branchAddressHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertBranchContact(BranchContact branchContact, String branchAddressHashValue) throws SQLException {
		long branchContactId = 0;
		String sql = "insert into branch_contacts"
				+ "(branch_info_id,contact_number,contact_country_prefix,contact_extension,contact_type,notes,hash)"
				+ "values(?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, branchContact.getBranchId());
			ps.setString(2, branchContact.getNumber());
			ps.setString(3, branchContact.getPrefix());
			ps.setString(4, branchContact.getExtension());
			ps.setString(5, branchContact.getCommunicationType());
			ps.setString(6, branchContact.getNotes());
			ps.setString(7, branchAddressHashValue);
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				branchContactId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}

		return branchContactId;
	}

	/**
	 * @param branchEmail
	 * @param emailHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertBranchEmail(BranchEmail branchEmail, String emailHashValue) throws SQLException {
		String sql = "insert into branch_emails (branch_info_id,email_id,email_type,hash) values(?,?,?,?)";
		Long branchEmailId = 0L;
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, branchEmail.getBranchId());
			ps.setString(2, branchEmail.getEmail());
			ps.setString(3, branchEmail.getEmailType());
			ps.setString(4, emailHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				branchEmailId = rs.getLong(1);
			}
		} finally {
			connection.close();
			ps.close();
		}

		return branchEmailId;
	}

	/**
	 * @param branchId
	 * @return
	 * @throws SQLException
	 */

	public Object checkIfBranchIdExist(String branchId) throws SQLException {
		String sql = "select * from branch_info where sol_id = ?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, branchId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return true;
			}
		} finally {
			ps.close();
			connection.close();
		}
		return false;
	}

	/**
	 * @param referenceId
	 * @return
	 * @throws SQLException
	 */
	public Object checkIfReferenceNumberExist(String referenceId) throws SQLException {
		String sql = "select * from branch_info where fiu_reference_no = ?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, referenceId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return true;
			}
		} finally {
			ps.close();
			connection.close();
		}
		return false;
	}
}
