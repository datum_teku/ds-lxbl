package com.trustaml.service.bfi.branch.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class BranchAddress {

	@JsonProperty("id")
	private long id;

	@JsonProperty("branch_id")
	private Long branchId;

	@JsonProperty("address_type")
	private String addressType;

	@JsonProperty("country")
	private String country;

	@JsonProperty("state")
	private String state;

	@JsonProperty("province")
	private String province;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("town_city_village")
	private String townCityVillage;

	@JsonProperty("tole")
	private String tole;

	@JsonProperty("street")
	private String street;

	@JsonProperty("house_number")
	private String houseNumber;

	@JsonProperty("po_box_number")
	private String poBoxNumber;

	@JsonProperty("pin_zip_number")
	private String pinZipNumber;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	public BranchAddress() {
		this.id = new Long(0);
		this.branchId = new Long(0);
		this.addressType = "";
		this.country = "";
		this.state = "";
		this.province = "";
		this.district = "";
		this.mnVdc = "";
		this.townCityVillage = "";
		this.tole = "";
		this.street = "";
		this.houseNumber = "";
		this.poBoxNumber = "";
		this.pinZipNumber = "";
		this.notes = "";

	}

	public BranchAddress(long id, Long bInfoId, String addressType, String country, String state, String province,
			String district, String mnVdc, String townCityVillage, String tole, String street, String houseNumber,
			String poBoxNumber, String pinZipNumber, String notes) {
		super();
		this.id = id;
		this.branchId = bInfoId;
		this.addressType = addressType;
		this.country = country;
		this.state = state;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.townCityVillage = townCityVillage;
		this.tole = tole;
		this.street = street;
		this.houseNumber = houseNumber;
		this.poBoxNumber = poBoxNumber;
		this.pinZipNumber = pinZipNumber;
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getTownCityVillage() {
		return townCityVillage;
	}

	public void setTownCityVillage(String townCityVillage) {
		this.townCityVillage = townCityVillage;
	}

	public String getTole() {
		return tole;
	}

	public void setTole(String tole) {
		this.tole = tole;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getPoBoxNumber() {
		return poBoxNumber;
	}

	public void setPoBoxNumber(String poBoxNumber) {
		this.poBoxNumber = poBoxNumber;
	}

	public String getPinZipNumber() {
		return pinZipNumber;
	}

	public void setPinZipNumber(String pinZipNumber) {
		this.pinZipNumber = pinZipNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
