package com.trustaml.service.bfi.branch.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class BranchContact {

	@JsonProperty("id")
	private long id;

	@JsonProperty("branch_id")
	private Long branchId;

	@JsonProperty("type")
	private String type;

	@JsonProperty("contact_number")
	private String number;

	@JsonProperty("contact_country_prefix")
	private String prefix;

	@JsonProperty("contact_extension")
	private String extension;

	@JsonProperty("contact_communication_type")
	private String communicationType;

	@JsonProperty("email")
	private String email;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	public BranchContact() {
		super();
		this.id = 0;
		this.branchId = new Long(0);
		this.type = "";
		this.number = "";
		this.prefix = "";
		this.extension = "";
		this.communicationType = "";
		this.email = "";
		this.notes = "";
	}

	public BranchContact(Long id, Long biId, String type, String number, String prefix, String extension,
			String communicationType, String email, String notes) {
		super();
		this.id = id;
		this.branchId = biId;
		this.type = type;
		this.number = number;
		this.prefix = prefix;
		this.extension = extension;
		this.communicationType = communicationType;
		this.email = email;
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
