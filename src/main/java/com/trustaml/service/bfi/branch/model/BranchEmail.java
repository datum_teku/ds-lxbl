package com.trustaml.service.bfi.branch.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BranchEmail {

	@JsonProperty("id")
	private long id;

	@JsonProperty("branch_id")
	private Long branchId;

	@JsonProperty("email")
	private String email;

	@JsonProperty("email_type")
	private String emailType;

	@JsonProperty("change")
	private boolean change;

	public BranchEmail() {
		this.id = new Long(0);
		this.branchId = new Long(0);
		this.email = "";
		this.emailType = "";
	}

	public BranchEmail(long id, Long biId, String email, String emailType) {
		super();
		this.id = id;
		this.branchId = biId;
		this.email = email;
		this.emailType = emailType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
