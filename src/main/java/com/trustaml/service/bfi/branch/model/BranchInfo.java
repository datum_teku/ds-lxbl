package com.trustaml.service.bfi.branch.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class BranchInfo {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("bfId")
	private Long bfId;

	@JsonProperty("branch_name")
	private String name;

	@JsonProperty("fiu_reference_number")
	private String reference;

	@JsonProperty("branch_code")
	private String solId;

	@JsonProperty("local_currency")
	private String localCurrency;

	@JsonProperty("address_type")
	private String addressType;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("address")
	private String address;

	@JsonProperty("change")
	private boolean change;

	public BranchInfo() {
		this.id = new Long(0);
		this.bfId = new Long(0);
		this.name = "";
		this.reference = "";
		this.solId = "";
		this.localCurrency = "";
		this.addressType = "";
		this.notes = "";
		this.address = "";
	}

	public BranchInfo(Long id, Long bfId, String name, String reference, String solId, String localCurrency,
			String addressType, String notes, String address) {
		super();
		this.id = id;
		this.bfId = bfId;
		this.name = name;
		this.reference = reference;
		this.solId = solId;
		this.localCurrency = localCurrency;
		this.addressType = addressType;
		this.notes = notes;
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBfId() {
		return bfId;
	}

	public void setBfId(Long bfId) {
		this.bfId = bfId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getSolId() {
		return solId;
	}

	public void setSolId(String solId) {
		this.solId = solId;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
