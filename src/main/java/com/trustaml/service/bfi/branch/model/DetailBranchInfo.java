package com.trustaml.service.bfi.branch.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class DetailBranchInfo {

	@JsonProperty("branch_info")
	private BranchInfo branchInfo;

	@JsonProperty("list_address")
	private List<BranchAddress> listAddress;

	@JsonProperty("list_email")
	private List<BranchEmail> listEmail;

	@JsonProperty("list_contact")
	private List<BranchContact> listContact;

	@JsonProperty("user")
	private User user;

	public DetailBranchInfo() {
		this.branchInfo = null;
		this.listAddress = null;
		this.listEmail = null;
		this.listContact = null;
		this.user = new User();
	}

	public BranchInfo getBranchInfo() {
		return branchInfo;
	}

	public void setBranchInfo(BranchInfo branchInfo) {
		this.branchInfo = branchInfo;
	}

	public List<BranchAddress> getListAddress() {
		return listAddress;
	}

	public void setListAddress(List<BranchAddress> listAddress) {
		this.listAddress = listAddress;
	}

	public List<BranchEmail> getListEmail() {
		return listEmail;
	}

	public void setListEmail(List<BranchEmail> listEmail) {
		this.listEmail = listEmail;
	}

	public List<BranchContact> getListContact() {
		return listContact;
	}

	public void setListContact(List<BranchContact> listContact) {
		this.listContact = listContact;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
