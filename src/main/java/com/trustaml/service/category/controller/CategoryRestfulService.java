package com.trustaml.service.category.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.category.dao.CategoryDao;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;

@Path("/category")
public class CategoryRestfulService {

	@Inject
	CategoryDao categoryDao;

	/**
	 * @param tableName
	 * @description select all the data from table name provided
	 * @return data from table
	 * @throws SQLException
	 */

	@GET
	@Path("/{table-name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCategory(@PathParam("table-name") String tableName) throws SQLException {
		return ResponseReturn.sucess(categoryDao.getAllEnumField(tableName));

	}

	/**
	 * @param tableName
	 * @param enumCode
	 * @description check if the enum code exists providing table name and enum
	 *              code
	 * @return true or false
	 * @throws SQLException
	 */
	@GET
	@Path("/enum-code/{table-name}/{enum-code}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkIfEnumCodeExists(@PathParam("table-name") String tableName,
			@PathParam("enum-code") String enumCode) throws SQLException {
		return ResponseReturn.sucess(categoryDao.checkIfEnumCodeExists(tableName, enumCode));

	}

	/**
	 * @return data from risk table
	 * @throws SQLException
	 */
	@GET
	@Path("/risk-table")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRiskTable() throws SQLException {
		return ResponseReturn.sucess(categoryDao.getRiskTable());
	}

	/**
	 * @param categoryName
	 * @return fields of enum or fields of enum by category name
	 * @throws SQLException
	 */
	@GET
	@Path("/all-table")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTables(@QueryParam("category-name") String categoryName) throws SQLException {
		if (categoryName == null) {
			return ResponseReturn.sucess(categoryDao.getEnumField());
		} else {
			return ResponseReturn.sucess(categoryDao.getEnumFieldByCategoryName(categoryName));
		}

	}

	// @PUT
	// @Consumes(MediaType.APPLICATION_JSON)
	// public Response updateCategory(String json)
	// throws JsonParseException, JsonMappingException, IOException,
	// SQLException, TrustAmlEmptyJSONException {
	// if (!json.isEmpty()) {
	// categoryDao.UpdateEnum(json);
	// return ResponseReturn.sucess("update successful");
	// } else {
	// throw new TrustAmlEmptyJSONException("json is empty");
	// }
	//
	// }

	/**
	 * @param data
	 *            String type
	 * @description save enum object
	 * @return save successful
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveCategory(String data)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!data.isEmpty()) {
			categoryDao.saveEnum(data);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

}
