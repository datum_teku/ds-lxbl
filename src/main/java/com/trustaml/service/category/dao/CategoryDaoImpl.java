package com.trustaml.service.category.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.trustaml.service.category.model.Category;
import com.trustaml.service.category.model.CommonEnumField;
import com.trustaml.service.common.database.DBConnection;

public class CategoryDaoImpl {

	@Inject
	DBConnection dbConnection;

	public List<Object> getAllEnumField(String table) throws SQLException {
		String sql = "select * from " + table;
		List<Object> list = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				CommonEnumField field = new CommonEnumField(rs.getString("enum_code"), rs.getString("enum_description"),
						rs.getDate("disable_date"), rs.getBoolean("enabled"));
				list.add(field);
			}
		} finally {
			stmt.close();
			con.close();
		}

		return list;
	}

	public void updateEnums(String table, CommonEnumField commonEnumField) throws SQLException {
		String sql;
		if (commonEnumField.getStatus()) {
			sql = "update " + table + " set enabled=?, enum_description=? where enum_code=?";
		} else {
			sql = "update " + table + " set  enabled=?, disable_date=?, enum_description=? where enum_code=?";
		}
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, commonEnumField.getStatus());
			if (commonEnumField.getStatus()) {
				ps.setString(2, commonEnumField.getDescription());
				ps.setString(3, commonEnumField.getEnumcode());

			} else {
				ps.setDate(2, new java.sql.Date(new Date().getTime()));
				ps.setString(3, commonEnumField.getDescription());
				ps.setString(4, commonEnumField.getEnumcode());
			}
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertEnumsUpdate(String table, CommonEnumField commonEnumField, String reason, String maker,
			String checker) throws SQLException {
		String sql = "";
		if (commonEnumField.getStatus()) {
			sql = "insert into " + table
					+ "_update(enum_code,enum_description, enabled, maker,checker,approved,update_date,approved_date, reason) values(?,?,?,?,?,?,?,?,?)";
		} else {
			sql = "insert into " + table
					+ "_update(enum_code,enum_description, enabled,disable_date, maker,checker,approved,update_date,approved_date, reason) values(?,?,?,?,?,?,?,?,?,?)";
		}
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, commonEnumField.getEnumcode());
			ps.setString(2, commonEnumField.getDescription());
			ps.setBoolean(3, commonEnumField.getStatus());
			if (commonEnumField.getStatus()) {
				ps.setString(4, maker);
				ps.setString(5, checker);
				ps.setBoolean(6, true);
				ps.setDate(7, new java.sql.Date(new Date().getTime()));
				ps.setDate(8, new java.sql.Date(new Date().getTime()));
				ps.setString(9, "New Reason");
			} else {
				ps.setDate(4, new java.sql.Date(new Date().getTime()));
				ps.setString(5, maker);
				ps.setString(6, checker);
				ps.setBoolean(7, true);
				ps.setDate(8, new java.sql.Date(new Date().getTime()));
				ps.setDate(9, new java.sql.Date(new Date().getTime()));
				ps.setString(10, "New Reason");
			}
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public Object getEnumField() throws SQLException {

		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call enum_table_name(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally

		{
			stmt.close();
			con.close();
		}

		return result;
	}

	public void saveCategory(Category category) throws SQLException {
		String sql = "INSERT INTO enum_main_category (main_category,main_description,enum_code,enum_description,risk_value,schm_code,gl_sub_head_code,hash,enable,precedence,level) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, category.getMainCategory() == null ? null : category.getMainCategory());
			ps.setString(2, category.getMainDescription() == null ? null : category.getMainDescription());
			ps.setString(3, category.getEnumCode() == null ? null : category.getEnumCode());
			ps.setString(4, category.getEnumDescription() == null ? null : category.getEnumDescription());
			if (category.getRiskValue() != null) {
				ps.setInt(5, category.getRiskValue());
			} else {
				ps.setNull(5, java.sql.Types.INTEGER);
			}
			ps.setString(6, category.getSchmCode() == null ? null : category.getSchmCode());
			ps.setString(7, category.getGlSubHeadCode() == null ? null : category.getGlSubHeadCode());
			ps.setString(8, "test");
			ps.setBoolean(9, category.getEnable());
			if (category.getPrecedence() != null) {
				ps.setInt(10, category.getPrecedence());
			} else {
				ps.setNull(10, java.sql.Types.INTEGER);
			}

			ps.setString(11, category.getLevel() == null ? null : category.getLevel());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public boolean checkIfEnumCodeExists(String tableName, String enumCode) throws SQLException {
		String sql = "select * from " + tableName + " where enum_code = ?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, enumCode);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				return true;
			}
		} finally {
			ps.close();
			connection.close();
		}

		return false;

	}

	public Object getEnumFieldByCategoryName(String categoryName) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call enum_table_name_by_category_name(?,?)}");
			stmt.setString(1, categoryName);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally

		{
			stmt.close();
			con.close();
		}
		return result;
	}

	public void updateCategory(Category category) throws SQLException {
		String sql = "UPDATE enum_main_category set main_category=?,main_description=?,enum_code=?,enum_description=?,risk_value=?,schm_code=?,gl_sub_head_code=?,hash=?,enable=?,precedence=?,level=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, category.getMainCategory().isEmpty() ? null : category.getMainCategory());
			ps.setString(2, category.getMainDescription().isEmpty() ? null : category.getMainDescription());
			ps.setString(3, category.getEnumCode().isEmpty() ? null : category.getEnumCode());
			ps.setString(4, category.getEnumDescription().isEmpty() ? null : category.getEnumDescription());
			if (category.getRiskValue() != null) {
				ps.setInt(5, category.getRiskValue());
			} else {
				ps.setNull(5, java.sql.Types.INTEGER);
			}
			ps.setString(6, category.getSchmCode().isEmpty() ? null : category.getSchmCode());
			ps.setString(7, category.getGlSubHeadCode().isEmpty() ? null : category.getGlSubHeadCode());
			ps.setString(8, "test");
			ps.setBoolean(9, category.getEnable());
			if (category.getPrecedence() != null) {
				ps.setInt(10, category.getPrecedence());
			} else {
				ps.setNull(10, java.sql.Types.INTEGER);
			}
			ps.setString(11, category.getLevel().isEmpty() ? null : category.getLevel());
			ps.setLong(12, category.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void saveInCategoryUpdateTable(Category category) throws SQLException {
		String sql = "INSERT INTO enum_main_category_update (main_category,main_description,enum_code,enum_description,risk_value,schm_code,gl_sub_head_code,hash,enable,precedence,reason,enum_main_category_id,maker,level) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, category.getMainCategory() == null ? null : category.getMainCategory());
			ps.setString(2, category.getMainDescription() == null ? null : category.getMainDescription());
			ps.setString(3, category.getEnumCode() == null ? null : category.getEnumCode());
			ps.setString(4, category.getEnumDescription() == null ? null : category.getEnumDescription());
			if (category.getRiskValue() != null) {
				ps.setInt(5, category.getRiskValue());
			} else {
				ps.setNull(5, java.sql.Types.INTEGER);
			}
			ps.setString(6, category.getSchmCode() == null ? null : category.getSchmCode());
			ps.setString(7, category.getGlSubHeadCode() == null ? null : category.getGlSubHeadCode());
			ps.setString(8, "test");
			ps.setBoolean(9, category.getEnable());
			if (category.getPrecedence() != null) {
				ps.setInt(10, category.getPrecedence());
			} else {
				ps.setNull(10, java.sql.Types.INTEGER);
			}
			ps.setString(11, category.getLevel() == null ? null : category.getLevel());
			ps.setLong(12, category.getId());
			ps.setString(13, category.getUser().getUserName());
			ps.setString(14, category.getReason() == null ? null : category.getReason());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public Object getRiskTable() throws SQLException {
		Connection con = null;
		String result = "";
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call enum_risk_table_name(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			con.close();
		}

		return result;
	}
}
