package com.trustaml.service.category.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Category {
	@JsonProperty("id")
	protected long id;

	@JsonProperty("main_category")
	protected String mainCategory;

	@JsonProperty("main_description")
	protected String mainDescription;

	@JsonProperty("enum_code")
	protected String enumCode;

	@JsonProperty("enum_description")
	protected String enumDescription;

	@JsonProperty("risk_value")
	protected Integer riskValue;

	@JsonProperty("schm_code")
	protected String schmCode;

	@JsonProperty("gl_sub_head_code")
	protected String glSubHeadCode;

	@JsonProperty("precedence")
	protected Integer precedence;

	@JsonProperty("level")
	protected String level;

	@JsonProperty("enable")
	protected Boolean enable;

	@JsonProperty("reason")
	protected String reason;

	@JsonProperty("user")
	private User user;

	public Category() {
		super();
		this.id = 0;
		this.mainCategory = "";
		this.mainDescription = "";
		this.enumCode = "";
		this.enumDescription = "";
		this.schmCode = "";
		this.glSubHeadCode = "";
		this.level = "";
		this.reason = "";
		this.user = null;
	}

	public Category(long id, String mainCategory, String mainDescription, String enumCode, String enumDescription,
			Integer riskValue, String schmCode, String glSubHeadCode, Integer precedence, String level, Boolean enable,
			String reason, User user) {
		super();
		this.id = id;
		this.mainCategory = mainCategory;
		this.mainDescription = mainDescription;
		this.enumCode = enumCode;
		this.enumDescription = enumDescription;
		this.riskValue = riskValue;
		this.schmCode = schmCode;
		this.glSubHeadCode = glSubHeadCode;
		this.precedence = precedence;
		this.level = level;
		this.enable = enable;
		this.reason = reason;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}

	public String getMainDescription() {
		return mainDescription;
	}

	public void setMainDescription(String mainDescription) {
		this.mainDescription = mainDescription;
	}

	public String getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}

	public String getEnumDescription() {
		return enumDescription;
	}

	public void setEnumDescription(String enumDescription) {
		this.enumDescription = enumDescription;
	}

	public Integer getRiskValue() {
		return riskValue;
	}

	public void setRiskValue(Integer riskValue) {
		this.riskValue = riskValue;
	}

	public String getSchmCode() {
		return schmCode;
	}

	public void setSchmCode(String schmCode) {
		this.schmCode = schmCode;
	}

	public String getGlSubHeadCode() {
		return glSubHeadCode;
	}

	public void setGlSubHeadCode(String glSubHeadCode) {
		this.glSubHeadCode = glSubHeadCode;
	}

	public Integer getPrecedence() {
		return precedence;
	}

	public void setPrecedence(Integer precedence) {
		this.precedence = precedence;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
