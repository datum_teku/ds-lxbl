package com.trustaml.service.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.constant.ConstantSanctionType;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.CountryCode;
import com.trustaml.service.common.model.ApiAccessDto;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.risk.model.RiskMappingDto;

public class ConstantEntity {

	@Inject
	DBConnection dbConnection;

	public static Map<String, String> legalLinkTable = new HashMap<String, String>();
	public static Map<String, String> naturalLinkTable = new HashMap<String, String>();
	public static Map<String, String> legalLinkColumnName = new HashMap<String, String>();
	public static Map<String, String> naturalLinkColumnName = new HashMap<String, String>();
	public static Map<String, String> purposeOfScreening = new HashMap<String, String>();
	private Map<String, String> apiAccessMap = new HashMap<>();
	private boolean setApiAccess = false;
	private Map<String, String> riskMapping = new HashMap<>();
	private static List<CountryCode> listCountry = new ArrayList<>();
	private static Map<String, String> mapCountryCode = new HashMap<>();

	/**
	 * @throws SQLException
	 */

	public int getSizeOfLegalLinkTable() {
		return legalLinkTable.size();
	}

	public int getSizeOfNaturalLinkTable() {
		return naturalLinkTable.size();
	}

	public int getSizeOfLegalLinkColumnName() {
		return legalLinkColumnName.size();
	}

	public int getSizeOfNaturalLinkColumnName() {
		return naturalLinkColumnName.size();
	}

	public int getSizeOfPurposeLinkTable() {
		return purposeOfScreening.size();
	}

	public void setPurposeOfScreening() throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM enum_purpose_of_screening_n";
		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			legalLinkTable.put(rs.getString(1), rs.getString(2));
		}
		ps.close();
		connection.close();
	}

	/**
	 * @throws SQLException
	 */
	public void setlegalLinkTable() throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM screening_l_request_link_table WHERE screening_name=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, "screening_l");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			legalLinkTable.put(rs.getString(1), rs.getString(2));
		}
		ps.close();
		connection.close();
	}

	/**
	 * @throws SQLException
	 */
	public void setlegalLinkColumnName() throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM screening_l_request_link_table WHERE screening_name=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, "screening_l");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			legalLinkColumnName.put(rs.getString(1), rs.getString(4));
		}
		ps.close();
		connection.close();
	}

	/**
	 * @throws SQLException
	 */
	public void setNaturalLinkColumnName() throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM screening_l_request_link_table WHERE screening_name=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, "screening_n");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			naturalLinkColumnName.put(rs.getString(1), rs.getString(4));
		}
		ps.close();
		connection.close();
	}

	/**
	 * @throws SQLException
	 */
	public void setNaturalLinkTable() throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM screening_l_request_link_table WHERE screening_name=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, "screening_n");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			naturalLinkTable.put(rs.getString(1), rs.getString(2));
		}
		ps.close();
		connection.close();
	}

	/**
	 * @throws SQLException
	 */
	public void setApiAccess() throws SQLException {
		List<ApiAccessDto> listApiAccessDto = new ArrayList<ApiAccessDto>();
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM master_api_keys";
		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			setApiAccess = true;
			ApiAccessDto apiAccessDto = new ApiAccessDto(rs.getString(1), rs.getString(2), rs.getString(3),
					rs.getString(4), rs.getString(5));
			listApiAccessDto.add(apiAccessDto);
			System.out.println(apiAccessDto.getApiKey() + " and " + apiAccessDto.getModule());
		}
		apiAccessMap = listApiAccessDto.stream()
				.collect(Collectors.toMap(ApiAccessDto::getApiKey, ApiAccessDto::getModule));

		ps.close();
		connection.close();
	}

	public List<RiskMappingDto> getRiskMapping()
			throws SQLException, JsonParseException, JsonMappingException, IOException {
		List<RiskMappingDto> listRiskMappingDto = new ArrayList<>();
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT * FROM master_role";
		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			RiskMappingDto mappingDto = new RiskMappingDto();
			mappingDto.setRoleType(rs.getString(1));
			mappingDto.setValue(rs.getString(2));
			listRiskMappingDto.add(mappingDto);
		}
		riskMapping = listRiskMappingDto.stream()
				.collect(Collectors.toMap(RiskMappingDto::getRoleType, RiskMappingDto::getValue));
		ps.close();
		connection.close();
		return listRiskMappingDto;
	}

	public Integer getSizeOfRiskMap() {
		return riskMapping.size();
	}

	public String getValueOfRisk(String riskType) {
		return riskMapping.get(riskType);
	}

	public boolean isSetApiAccess() {
		return setApiAccess;
	}

	public void setSetApiAccess(boolean setApiAccess) {
		this.setApiAccess = setApiAccess;
	}

	public Map<String, String> getlegalLinkTable() {
		return legalLinkTable;
	}

	public String getLegalTableName(String tableName) {
		return legalLinkTable.get(tableName);
	}

	public String getNaturalTableName(String tableName) {
		return naturalLinkTable.get(tableName);
	}

	public String getNaturalColumnName(String tableName) {
		return naturalLinkColumnName.get(tableName);
	}

	public String getLegalColumnName(String tableName) {
		return legalLinkColumnName.get(tableName);
	}

	public String getPurposeOfScreening(String str) {
		return purposeOfScreening.get(str);

	}

	public String generateHashValue(Legal legal) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		byte[] buffer = legal.getAuditedFiscalYear().getBytes("UTF-8");
		md.update(buffer);
		byte[] digest = md.digest();

		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
		}
		return hexStr;
	}

	// public Integer checkApiAccessForNatural(User user) throws SQLException {
	//
	// Integer apiCount = 0;
	// Connection connection = dbConnection.getConnection();
	// String sql = "SELECT * FROM master_api_keys";
	// PreparedStatement ps = connection.prepareStatement(sql);
	// ps.setString(1, user.getApikey());
	// ResultSet rs = ps.executeQuery();
	// while (rs.next()) {
	// apiCount = rs.getInt(5);
	// }
	// ps.close();
	// connection.close();
	// return apiCount;
	// }
	/**
	 * @param apiCount
	 * @param user
	 * @throws SQLException
	 */
	public void updateApiAccessCount(Integer apiCount, User user) throws SQLException {
		Connection connection = dbConnection.getConnection();
		String sql = "UPDATE tbl_api_token SET screening_n_api_use_count=? where screening_n_api_key=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, apiCount);
		ps.setString(2, user.getApikey());
		ps.executeUpdate();
		ps.close();
		connection.close();

	}

	/**
	 * @param authorizationHeader
	 * @return
	 */
	public String checkAccessToApi(String authorizationHeader) {
		System.out.println("checking for apikey" + authorizationHeader);
		return apiAccessMap.get(authorizationHeader);
	}

	public String getRiskMappingName(String risk) throws SQLException {
		String data = null;
		Connection connection = dbConnection.getConnection();
		String sql = "SELECT mapping_field FROM risk_mapping WHERE type=?";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, risk);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			data = rs.getString(1);
		}
		ps.close();
		connection.close();
		return data;
	}

	public void saveMessageNotSentNotification(String userEmail, String subject, String body) throws SQLException {
		PreparedStatement psmt = null;
		Connection conn = null;
		String sql = "INSERT INTO email_schedular value(section_name,email_id,email_subject,email_body,is_valid,email_sent,request_id)";
		try {
			conn = dbConnection.getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, "test");
			psmt.setString(2, userEmail);
			psmt.setString(3, subject);
			psmt.setString(4, body);
			psmt.setBoolean(5, true);
			psmt.setBoolean(6, false);
			psmt.setLong(7, 6l);

		} finally {
			conn.close();
			psmt.close();
		}
	}

	public void setMapForCountryCode() throws SQLException, JsonParseException, JsonMappingException, IOException {
		// String result = "";
		// Connection con = null;
		// CallableStatement stmt = null;
		// try {
		// con = dbConnection.getConnection();
		// stmt = con.prepareCall("{call enum_country_code_3a(?)}");
		// stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
		// stmt.executeUpdate();
		// result = stmt.getString(1);
		// } finally {
		// con.close();
		// stmt.close();
		// }
		// countryCode = new ObjectMapper().readValue(result, new
		// TypeReference<Map<String, Object>>() {
		// });

		String sql = "SELECT enum_code,enum_description from enum_country_code_3a";
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				CountryCode countryCode = new CountryCode(rs.getString(1), rs.getString(2));
				listCountry.add(countryCode);

			}
		} finally {
			stmt.close();
			con.close();
		}
	}

	public boolean isCountryCodeSet() {
		if (listCountry.size() > 0) {
			return true;
		}
		return false;
	}

	public void convertListToMapForCountryCode() {
		for (CountryCode countryCode : listCountry) {
			mapCountryCode.put(countryCode.getEnumDescription(), countryCode.getEnumCode());
		}

	}

	public String getCodeFromMapCountryCode(String countryOfBirth) {
		// TODO Auto-generated method stub
		return mapCountryCode.get(countryOfBirth);
	}

	public String generateNewKey(String existingUniqueKey, String country1) {

		StringBuffer strBuff = new StringBuffer();
		String uniqueKey = existingUniqueKey;
		String country = country1;
		String sanctionType = uniqueKey.substring(3, 5);
		String typeOfRisk = uniqueKey.substring(12, 13);
		// String dataInsertedBy = uniqueKey.substring(13, 14);

		String serialNumber = uniqueKey.substring(5, 12);
		int key = Integer.valueOf(serialNumber);
		key++;
		String intKey = String.valueOf(key);
		int length = intKey.length();
		int totalNumberOfZeroToAppend = 7 - length;
		strBuff.append(country + sanctionType);
		while (totalNumberOfZeroToAppend > 0) {
			strBuff.append("0");
			totalNumberOfZeroToAppend--;
		}

		strBuff.append(intKey);
		strBuff.append(typeOfRisk + ConstantSanctionType.USER_FOR_SANCTION);
		System.out.println(strBuff);
		return strBuff.toString();
	}

	public String createNewKey(String countryOfBirth, String sanctionType) {
		// user.for.sanction.list
		String serialNumber = "0000001";
		String newKey = countryOfBirth + sanctionType + serialNumber + "1D" + ConstantSanctionType.USER_FOR_SANCTION;
		return newKey;
	}

}
