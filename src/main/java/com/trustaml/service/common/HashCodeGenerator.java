package com.trustaml.service.common;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import com.trustaml.service.adversemedia.model.AdverseAddressInfo;
import com.trustaml.service.adversemedia.model.AdverseAttachment;
import com.trustaml.service.adversemedia.model.AdverseFamilyInfo;
import com.trustaml.service.adversemedia.model.AdverseOfficeInfo;
import com.trustaml.service.adversemedia.model.PersonalInfo;
import com.trustaml.service.bfi.branch.model.BranchAddress;
import com.trustaml.service.bfi.branch.model.BranchContact;
import com.trustaml.service.bfi.branch.model.BranchEmail;
import com.trustaml.service.bfi.branch.model.BranchInfo;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.hotlist.model.Attachment;
import com.trustaml.service.hotlist.model.HotListAddressInfo;
import com.trustaml.service.hotlist.model.HotListFamilyInfo;
import com.trustaml.service.hotlist.model.HotListOfficeInfo;
import com.trustaml.service.kyc.kycl.model.AccountInfo;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.AuditInfo;
import com.trustaml.service.kyc.kycl.model.BusinessInfo;
import com.trustaml.service.kyc.kycl.model.ComplianceInfo;
import com.trustaml.service.kyc.kycl.model.DocumentStatus;
import com.trustaml.service.kyc.kycl.model.FinancialInfo;
import com.trustaml.service.kyc.kycl.model.LandlordInfo;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationAddress;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycl.model.RelatedEntity;
import com.trustaml.service.kyc.kycl.model.RelatedPerson;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAmlInformationInfo;
import com.trustaml.service.kyc.kycn.model.KycnDocumentStatusInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfo;
import com.trustaml.service.kyc.kycn.model.KycnFinancialInfo;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfo;
import com.trustaml.service.kyc.kycn.model.KycnObservationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedEntityInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedPersonInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfo;
import com.trustaml.service.pepentry.model.ElectedRegion;
import com.trustaml.service.pepentry.model.PepAddressInfo;
import com.trustaml.service.pepentry.model.PepContactInfo;
import com.trustaml.service.pepentry.model.PepFamilyInfo;
import com.trustaml.service.pepentry.model.PepMediaInfo;
import com.trustaml.service.pepentry.model.PepOfficeInfo;
import com.trustaml.service.pepentry.model.PepPersonalInfo;
import com.trustaml.service.risk.model.BankService;
import com.trustaml.service.risk.model.BusinessEnvironment;
import com.trustaml.service.risk.model.Card;
import com.trustaml.service.risk.model.Country;
import com.trustaml.service.risk.model.CrimeEnvironment;
import com.trustaml.service.risk.model.Delivery;
import com.trustaml.service.risk.model.Industry;
import com.trustaml.service.risk.model.ListRisk;
import com.trustaml.service.risk.model.Occupational;
import com.trustaml.service.risk.model.Pep;
import com.trustaml.service.risk.model.ProductDeposit;
import com.trustaml.service.risk.model.ProductLoan;
import com.trustaml.service.risk.model.Remittance;
import com.trustaml.service.screening.legal.model.RequestPrimaryMatchedData;
import com.trustaml.service.screening.legal.model.RequestPrimaryRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityMatchData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedMatchedPersonData;
import com.trustaml.service.screening.legal.model.RequestRelatedPerson;
import com.trustaml.service.screening.legal.model.ScreeningLegalRequestAttachments;
import com.trustaml.service.swift.model.mt103prtfile.FileMeta;
import com.trustaml.service.swift.model.mt103prtmessage.Field20;
import com.trustaml.service.swift.model.mt103prtmessage.Field23B;
import com.trustaml.service.swift.model.mt103prtmessage.Field32A;
import com.trustaml.service.swift.model.mt103prtmessage.Field50K;
import com.trustaml.service.swift.model.mt103prtmessage.Field52A;
import com.trustaml.service.swift.model.mt103prtmessage.Field53A;
import com.trustaml.service.swift.model.mt103prtmessage.Field57A;
import com.trustaml.service.swift.model.mt103prtmessage.Field59;
import com.trustaml.service.swift.model.mt103prtmessage.Field70;
import com.trustaml.service.swift.model.mt103prtmessage.Field71A;
import com.trustaml.service.swift.model.mt103prtmessage.InstanceTypeAndTransmission;
import com.trustaml.service.swift.model.mt103prtmessage.Interventions;
import com.trustaml.service.swift.model.mt103prtmessage.MessageHeader;
import com.trustaml.service.swift.model.mt103prtmessage.MessageTrailer;
import com.trustaml.service.swift.model.mt103prtmessage.Receiver;
import com.trustaml.service.swift.model.mt103prtmessage.Sender;
import com.trustaml.service.toandfro.reply.model.AccountReply;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalInfo;
import com.trustaml.service.virtualaccount.model.VirtualAccount;

/**
 * 
 * @author sugam
 *
 */
public class HashCodeGenerator {
	/**
	 * 
	 * @param legal
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 */
	public final static String ENCRYPTED_KEY = "AAAAB3NzaC1yc2EAAAADAQABAAABAQC6aZzMlZZsX+z1m/3AOgT0ttMekVmEK2XfuDxCKUHgdo25RitVP6wqMpWvI/w3iD8IW50yhcVQztN7RIxUdIz+0WRM1H9t2diTVFC9K+gXWf3CmleBMpWByQ+izA6Di6LYr+BunZ/+ly4AqA5LdtGLpKIzpbFqog/9YJ5dB1Kr2ZX38aQ46A59Ug7JmcjjyRXOWbHUGPZTCr/7TO62Ty5zSatxAIJ03q3gJSfCwQWtf5OCcYQMOaWlRaNc29rmxumRm+jW6Jh3LtGLWzrwzjgIzrVp+0jcJ8Xv3XLiB+YDn6Fo6atVsslPhcVKwA6Mi4dvjNZjFaBMZHw/gRoOMd+7";

	public String generateHashValueForKyclInfo(Legal legal, boolean doNotIncludeUser, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = legal.getPrimarySolId() + legal.getSalutation() + legal.getNameOfTheInstitution()
				+ legal.getLsName() + legal.getCustShortName() + legal.getDateOfEstablishment() + legal.getNotes()
				+ legal.getCustomerType() + legal.getCustomerGroup() + legal.getCustomerConstitution()
				+ legal.getCustomerCommunity() + legal.getCustomerEmployeeId() + legal.getCustomerOpenDate()
				+ legal.getCustomerMaker() + legal.getScreeningId() + legal.getPanNumber() + legal.isMinor()
				+ legal.getCustomerStatusCode() + legal.isCardHolder() + legal.isNonResidentExternal()
				+ legal.getCustId() + legal.getKyclStatus() + legal.getLastUpdateDate() + legal.getLastScreenedDate()
				+ legal.isVerifiedRecord() + legal.getAuditedFiscalYear() + legal.getNetProfit() + legal.getNetLoss()
				+ legal.getTaxClearenceCertificateDate() + legal.getTaxClearenceFiscalYear()
				+ legal.getNextClearenceCertificateDate() + legal.getTaxableAmount() + legal.getTaxableIncome()
				+ legal.getTaxPaid() + legal.getTaxPaidDate() + legal.getTypeOfIndustry();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	private String generateEncryptedHashValue(String hashValue)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {

		byte[] key = (ENCRYPTED_KEY + hashValue).getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		// key = Arrays.copyOf(key, 16); // use only first 128 bit

		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		String encodedKey = Base64.getEncoder().encodeToString(secretKeySpec.getEncoded());
		return encodedKey;
	}

	/**
	 * 
	 * @param accountInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAccountInfo(AccountInfo accountInfo, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kyclId + accountInfo.getAccountId() + accountInfo.getForAccountId()
				+ accountInfo.getCurrencyOfAccount() + accountInfo.getAccountNo() + accountInfo.getAccountName()
				+ accountInfo.getAccountShortName() + accountInfo.getAccountOwnership() + accountInfo.getSchemeType()
				+ accountInfo.getSchemeCode() + accountInfo.getGlSubHeadCode() + accountInfo.getProductGroup()
				+ accountInfo.getLastTransactionDate() + accountInfo.getAccountOpenDate()
				+ accountInfo.getEstimatedYearlyTransactions() + accountInfo.getEstimatedMonthlyTransactions()
				+ accountInfo.getEstimatedYearlyTurnover() + accountInfo.getEstimatedMonthlyTurnover()
				+ accountInfo.getRegularSourceOfIncome() + accountInfo.getSourceOfFund() + accountInfo.getNotes()
				+ accountInfo.getNatureOfAccount() + accountInfo.getSchemeDescription()
				+ accountInfo.getDrBalanceLimit() + accountInfo.getDepositeAmount() + accountInfo.getCustomerPan()
				+ accountInfo.getCustomerCurrency();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;

	}

	/**
	 * 
	 * @param address
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAddress(Address address, long kyclId, boolean doNotIncludeUser, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kyclId + address.getAddressType() + address.getCountry() + address.getZone()
				+ address.getDistrict() + address.getMnVdc() + address.getPinzip() + address.getWardNumber()
				+ address.getToleArea() + address.getStreet() + address.getHouseNo() + address.getUnitNumber()
				+ address.getNearestLandmark() + address.getLatitude() + address.getLongitude()
				+ address.getPhoneNoCountryCode() + address.getPhoneNoAreaCode() + address.getPhoneNo()
				+ address.getTelexNoCountryCode() + address.getTelexNoAreaCode() + address.getTelexNo()
				+ address.getPagerNoCountryCode() + address.getPagerNoAreaCode() + address.getPagerNo()
				+ address.getEmailId() + address.getNotes() + address.getState();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param auditInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAuditInfo(AuditInfo auditInfo, long kyclId, boolean doNotIncludeUser, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kyclId + auditInfo.getAuditedReport() + auditInfo.getAuthorizedLetter()
				+ auditInfo.getAuthorizedLetterDate() + auditInfo.getAuthorizedLetterAgency() + auditInfo.getNotes();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param businessInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValuForBusinessInfo(BusinessInfo businessInfo, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kyclId + businessInfo.getBranchesName() + businessInfo.getNatureOfBusiness()
				+ businessInfo.getMainBranchesOffices() + businessInfo.getGeographicalCoverage()
				+ businessInfo.getCountry() + businessInfo.getProvince() + businessInfo.getDistrict()
				+ businessInfo.getMnVdc() + businessInfo.getWardNo() + businessInfo.getTownCity()
				+ businessInfo.getNotes() + businessInfo.getPhoneNoCountryCode() + businessInfo.getPhoneNoAreaCode()
				+ businessInfo.getPhoneNo() + businessInfo.getTelexNoCountryCode() + businessInfo.getTelexNoAreaCode()
				+ businessInfo.getTelexNo() + businessInfo.getPagerNoCountryCode() + businessInfo.getPagerNoAreaCode()
				+ businessInfo.getPagerNo() + businessInfo.getEmailId();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param documentStatus
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForDocumentStatus(DocumentStatus documentStatus, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kyclId + documentStatus.getDocumentType() + documentStatus.getDocumentStatus()
				+ documentStatus.getApplictionSubmittedDate() + documentStatus.getRefreshDate()
				+ documentStatus.getNotes();
		if (doNotIncludeUser) {
			hashValue = hashValue + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param landlordInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForLandlordStatus(LandlordInfo landlordInfo, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + landlordInfo.getLandLordFirstName() + landlordInfo.getLandLordMiddleName()
				+ landlordInfo.getLandLordLastName() + landlordInfo.getCountry() + landlordInfo.getProvince()
				+ landlordInfo.getDistrict() + landlordInfo.getMnVdc() + landlordInfo.getWardNo()
				+ landlordInfo.getTownCity() + landlordInfo.getNotes() + landlordInfo.getPhoneNoCountryCode()
				+ landlordInfo.getPagerNoAreaCode() + landlordInfo.getPhoneNo() + landlordInfo.getTelexNoCountryCode()
				+ landlordInfo.getTelexNoAreaCode() + landlordInfo.getTelexNo() + landlordInfo.getPagerNoCountryCode()
				+ landlordInfo.getPagerNoAreaCode() + landlordInfo.getPagerNo();

		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param registrationAddress
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRegistrationAddressStatus(RegistrationAddress registrationAddress, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + registrationAddress.getCountry() + registrationAddress.getState()
				+ registrationAddress.getProvince() + registrationAddress.getMnVdc() + registrationAddress.getWardNo()
				+ registrationAddress.getTownCity() + registrationAddress.getNotes()
				+ registrationAddress.getPhoneNoCountryCode() + registrationAddress.getPhoneNoAreaCode()
				+ registrationAddress.getPhoneNo() + registrationAddress.getTelexNoCountryCode()
				+ registrationAddress.getTelexNoAreaCode() + registrationAddress.getTelexNo()
				+ registrationAddress.getPagerNoCountryCode() + registrationAddress.getPagerNoAreaCode()
				+ registrationAddress.getPagerNo() + registrationAddress.getEmailId()
				+ registrationAddress.getDistrict();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param registrationInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRegistrationAddressStatus(RegistrationInfo registrationInfo, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + registrationInfo.getRegistrationAuthority()
				+ registrationInfo.getSelfRegulatoryBody() + registrationInfo.getRegdNumber()
				+ registrationInfo.getLicensingAuthority() + registrationInfo.getLicenseNumber()
				+ registrationInfo.getNotes();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param relatedEntity
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRelatedEntityStatus(RelatedEntity relatedEntity, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + relatedEntity.getEntityType() + relatedEntity.getCustId() + relatedEntity.getKycnId()
				+ relatedEntity.getSalutation() + relatedEntity.getName() + relatedEntity.getLsName()
				+ relatedEntity.getCalledByName() + relatedEntity.getPrimaryIdentificationDocumentType()
				+ relatedEntity.getRegistrationNo() + relatedEntity.getCountry() + relatedEntity.getZone()
				+ relatedEntity.getDistrict() + relatedEntity.getMnVdc() + relatedEntity.getPinzip()
				+ relatedEntity.getWardNumber() + relatedEntity.getToleArea() + relatedEntity.getStreet()
				+ relatedEntity.getHouseNo() + relatedEntity.getUnitNumber() + relatedEntity.getNearestLandmark()
				+ relatedEntity.getLatitude() + relatedEntity.getLongitude() + relatedEntity.getPhoneNoCountryCode()
				+ relatedEntity.getPhoneNoAreaCode() + relatedEntity.getPhoneNo()
				+ relatedEntity.getTelexNoCountryCode() + relatedEntity.getTelexNoAreaCode()
				+ relatedEntity.getTelexNo();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param relatedPerson
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRelatedPersonStatus(RelatedPerson relatedPerson, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + relatedPerson.getPersonType() + relatedPerson.getCustId() + relatedPerson.getKycnId()
				+ relatedPerson.getSalutation() + relatedPerson.getFirstName() + relatedPerson.getMiddleName()
				+ relatedPerson.getLastName() + relatedPerson.getLsfName() + relatedPerson.getLsmName()
				+ relatedPerson.getLslName() + relatedPerson.getSecondName() + relatedPerson.getExpiryDate()
				+ relatedPerson.getZone() + relatedPerson.getDistrict() + relatedPerson.getMnVdc()
				+ relatedPerson.getPinzip() + relatedPerson.getWardNumber() + relatedPerson.getToleArea()
				+ relatedPerson.getStreet() + relatedPerson.getHouseNo() + relatedPerson.getUnitNumber()
				+ relatedPerson.getNearestLandmark() + relatedPerson.getLatitude() + relatedPerson.getLongitude()
				+ relatedPerson.getPhoneNoCountryCode() + relatedPerson.getPhoneNoAreaCode()
				+ relatedPerson.getPhoneNo() + relatedPerson.getTelexNoCountryCode()
				+ relatedPerson.getTelexNoAreaCode() + relatedPerson.getTelexNo() + relatedPerson.getEmailId()
				+ relatedPerson.getNotes();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param complianceInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForComplainceInfo(ComplianceInfo complianceInfo, long kyclId,
			boolean doNotIncludeUser, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + complianceInfo.getLevelOfComplianceOnAml()
				+ complianceInfo.getLevelOfComplianceOnTax() + complianceInfo.getLevelOfComplianceOnCorruption()
				+ complianceInfo.getLevelOfComplianceOnOthers() + complianceInfo.getNotes();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param financialInfo
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForFinancialInfo(FinancialInfo financialInfo, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + financialInfo.getCustomerPan() + financialInfo.getCustomerCurrency()
				+ financialInfo.getNotes();
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param individual
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForIndividualAccountInfo(String individual, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + individual + "";
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param service
	 * @param kyclId
	 * @param doNotIncludeUser
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForServiceAccountInfo(String service, long kyclId, boolean doNotIncludeUser,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = kyclId + service + "";
		if (doNotIncludeUser) {
			hashCode = hashCode + user.getUserName() + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param pepOfficeInfo
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForPepOffice(PepOfficeInfo pepOfficeInfo, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = personalInfoId + pepOfficeInfo.getDesignation() + pepOfficeInfo.getOfficeName()
				+ pepOfficeInfo.getProlongation() + pepOfficeInfo.getNotes() + pepOfficeInfo.getTermYears()
				+ pepOfficeInfo.getPepOfficeStatus() + pepOfficeInfo.getPepCategory();
		pepOfficeInfo.getSqlDateOfAppointment();
		if (includeUser) {
			hashCode = hashCode + user.getUserName();
		}

		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param familyInfo
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForFamily(PepFamilyInfo familyInfo, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfoId + familyInfo.getRelationshipTo() + familyInfo.getFirstName()
				+ familyInfo.getMiddleName() + familyInfo.getLastName() + familyInfo.getLsfName()
				+ familyInfo.getLsmName() + familyInfo.getLslName() + familyInfo.getSecondName()
				+ familyInfo.getCalledByName() + familyInfo.getNotes()
				+ familyInfo.getPrimaryIdentificationDocumentNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param pepAddressInfo
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForPepAddress(PepAddressInfo pepAddressInfo, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfoId + pepAddressInfo.getCountry() + pepAddressInfo.getState()
				+ pepAddressInfo.getProvince() + pepAddressInfo.getDistrict() + pepAddressInfo.getMnVdc()
				+ pepAddressInfo.getTownCityVillage() + pepAddressInfo.getTole() + pepAddressInfo.getStreet()
				+ pepAddressInfo.getHouseNumber() + pepAddressInfo.getNotes() + pepAddressInfo.getCommunicationType()
				+ pepAddressInfo.getPhoneNoCountryCode() + pepAddressInfo.getPhoneNoAreaCode()
				+ pepAddressInfo.getPhoneNo() + pepAddressInfo.getTelexNoCountryCode()
				+ pepAddressInfo.getTelexNoAreaCode() + pepAddressInfo.getTelexNo()
				+ pepAddressInfo.getPagerNoCountryCode() + pepAddressInfo.getPagerNoAreaCode()
				+ pepAddressInfo.getPagerNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param pepContactInfo
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForContact(PepContactInfo pepContactInfo, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfoId + pepContactInfo.getContactNumber() + pepContactInfo.getContactCountryPrefix()
				+ pepContactInfo.getContactExtension() + pepContactInfo.getContactCommunicationType()
				+ pepContactInfo.getEmailAddress() + pepContactInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param pepMediaInfo
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForMedia(PepMediaInfo pepMediaInfo, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfoId + pepMediaInfo.getNotes() + pepMediaInfo.getPublishedDate()
				+ pepMediaInfo.getExtensionText() + pepMediaInfo.getMediaType()
				+ pepMediaInfo.getMediaContent().getBytes() + pepMediaInfo.getSourceOfInformation()
				+ pepMediaInfo.getSourceType();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param personalInfo
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForPepPersonalInfo(PepPersonalInfo personalInfo, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfo.getJurisdiction() + personalInfo.getSalutation() + personalInfo.getFirstName()
				+ personalInfo.getMiddleName() + personalInfo.getLastName() + personalInfo.getLsfName()
				+ personalInfo.getLsmName() + personalInfo.getLslName() + personalInfo.getSecondName()
				+ personalInfo.getCalledByName() + personalInfo.getPreviousName() + personalInfo.getGender()
				+ personalInfo.getDateOfBirth() + personalInfo.getPlaceOfBirth() + personalInfo.getCountryOfBirth()
				+ personalInfo.getNationality() + personalInfo.getNotes() + personalInfo.getPepCategory()
				+ personalInfo.getReferenceNo() + personalInfo.getApprovedBy() + personalInfo.getCorrespondent()
				+ personalInfo.getPanNumber() + personalInfo.getCitizenNumber() + personalInfo.getApprovedDate();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param electedRegion
	 * @param personalInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForPepElectedRegion(ElectedRegion electedRegion, long personalInfoId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfoId + electedRegion.getElectedDistrict() + electedRegion.getAreaNumber()
				+ electedRegion.getPartyName() + electedRegion.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param personalInfo
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAdverseMediaPersonal(PersonalInfo personalInfo, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfo.getJurisdiction() + personalInfo.getSalutation() + personalInfo.getFirstName()
				+ personalInfo.getMiddleName() + personalInfo.getLastName() + personalInfo.getLsfName()
				+ personalInfo.getLsmName() + personalInfo.getLslName() + personalInfo.getSecondName()
				+ personalInfo.getCalledByName() + personalInfo.getPreviousName() + personalInfo.getGender()
				+ personalInfo.getDateOfBirth() + personalInfo.getPlaceOfBirth() + personalInfo.getCountryOfBirth()
				+ personalInfo.getNationality() + personalInfo.getNotes() + personalInfo.getAdverseCategory()
				+ personalInfo.getReferenceNo() + personalInfo.getApprovedBy() + personalInfo.getCorrespondent()
				+ personalInfo.getPanNumber() + personalInfo.getCitizenNumber() + personalInfo.getApprovedDate();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param adverseOfficeInfo
	 * @param adverseId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForAdverseOffice(AdverseOfficeInfo adverseOfficeInfo, Long adverseId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = adverseId + adverseOfficeInfo.getDesignation() + adverseOfficeInfo.getOfficeName()
				+ adverseOfficeInfo.getProlongation() + adverseOfficeInfo.getNotes() + adverseOfficeInfo.getTermYears()
				+ adverseOfficeInfo.getPepOfficeStatus() + adverseOfficeInfo.getAdverseCategory();
		adverseOfficeInfo.getSqlDateOfAppointment();
		if (includeUser) {
			hashCode = hashCode + user.getUserName();
		}

		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param adressFamilyInfo
	 * @param adverseId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForAdverseFamilyInfo(AdverseFamilyInfo adressFamilyInfo, Long adverseId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = adverseId + adressFamilyInfo.getRelationshipTo() + adressFamilyInfo.getFirstName()
				+ adressFamilyInfo.getMiddleName() + adressFamilyInfo.getLastName() + adressFamilyInfo.getLsfName()
				+ adressFamilyInfo.getLsmName() + adressFamilyInfo.getLslName() + adressFamilyInfo.getSecondName()
				+ adressFamilyInfo.getCalledByName() + adressFamilyInfo.getNotes()
				+ adressFamilyInfo.getPrimaryIdentificationDocumentNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param adverseMediaAddressInfo
	 * @param adverseId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAdverseAddress(AdverseAddressInfo adverseMediaAddressInfo, Long adverseId,
			User user, boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = adverseId + adverseMediaAddressInfo.getCountry() + adverseMediaAddressInfo.getState()
				+ adverseMediaAddressInfo.getProvince() + adverseMediaAddressInfo.getDistrict()
				+ adverseMediaAddressInfo.getMnVdc() + adverseMediaAddressInfo.getTownCityVillage()
				+ adverseMediaAddressInfo.getTole() + adverseMediaAddressInfo.getStreet()
				+ adverseMediaAddressInfo.getHouseNumber() + adverseMediaAddressInfo.getNotes()
				+ adverseMediaAddressInfo.getCommunicationType() + adverseMediaAddressInfo.getPhoneNoCountryCode()
				+ adverseMediaAddressInfo.getPhoneNoAreaCode() + adverseMediaAddressInfo.getPhoneNo()
				+ adverseMediaAddressInfo.getTelexNoCountryCode() + adverseMediaAddressInfo.getTelexNoAreaCode()
				+ adverseMediaAddressInfo.getTelexNo() + adverseMediaAddressInfo.getPagerNoCountryCode()
				+ adverseMediaAddressInfo.getPagerNoAreaCode() + adverseMediaAddressInfo.getPagerNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param adverseMediaInfo
	 * @param adverseId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForMedia(AdverseAttachment adverseMediaInfo, Long adverseId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = adverseId + adverseMediaInfo.getNotes() + adverseMediaInfo.getPublishedDate()
				+ adverseMediaInfo.getExtensionText() + adverseMediaInfo.getMediaType()
				+ adverseMediaInfo.getMediaContent().getBytes() + adverseMediaInfo.getSourceOfInformation()
				+ adverseMediaInfo.getSourceType();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param personalInfo
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForHotListPersonalInfo(com.trustaml.service.hotlist.model.PersonalInfo personalInfo,
			User user, boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = personalInfo.getJurisdiction() + personalInfo.getSalutation() + personalInfo.getFirstName()
				+ personalInfo.getMiddleName() + personalInfo.getLastName() + personalInfo.getLsfName()
				+ personalInfo.getLsmName() + personalInfo.getLslName() + personalInfo.getSecondName()
				+ personalInfo.getCalledByName() + personalInfo.getPreviousName() + personalInfo.getGender()
				+ personalInfo.getDateOfBirth() + personalInfo.getPlaceOfBirth() + personalInfo.getCountryOfBirth()
				+ personalInfo.getNationality() + personalInfo.getNotes() + personalInfo.getHotlistCategory()
				+ personalInfo.getReferenceNo() + personalInfo.getApprovedBy() + personalInfo.getCorrespondent()
				+ personalInfo.getPanNumber() + personalInfo.getCitizenNumber() + personalInfo.getApprovedDate();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;

	}

	/**
	 * 
	 * @param hotListOfficeInfo
	 * @param hotListId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForHotListOffice(HotListOfficeInfo hotListOfficeInfo, Long hotListId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = hotListId + hotListOfficeInfo.getDesignation() + hotListOfficeInfo.getOfficeName()
				+ hotListOfficeInfo.getProlongation() + hotListOfficeInfo.getNotes() + hotListOfficeInfo.getTermYears()
				+ hotListOfficeInfo.getPepOfficeStatus() + hotListOfficeInfo.getHotlistCategory();
		hotListOfficeInfo.getSqlDateOfAppointment();
		if (includeUser) {
			hashCode = hashCode + user.getUserName();
		}

		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param familyInfo
	 * @param hotListId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForHotListFamily(HotListFamilyInfo familyInfo, Long hotListId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = hotListId + familyInfo.getRelationshipTo() + familyInfo.getFirstName()
				+ familyInfo.getMiddleName() + familyInfo.getLastName() + familyInfo.getLsfName()
				+ familyInfo.getLsmName() + familyInfo.getLslName() + familyInfo.getSecondName()
				+ familyInfo.getCalledByName() + familyInfo.getNotes()
				+ familyInfo.getPrimaryIdentificationDocumentNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param hotListAddressInfo
	 * @param hotListId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForHotListAddress(HotListAddressInfo hotListAddressInfo, Long hotListId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = hotListId + hotListAddressInfo.getCountry() + hotListAddressInfo.getState()
				+ hotListAddressInfo.getProvince() + hotListAddressInfo.getDistrict() + hotListAddressInfo.getMnVdc()
				+ hotListAddressInfo.getTownCityVillage() + hotListAddressInfo.getTole()
				+ hotListAddressInfo.getStreet() + hotListAddressInfo.getHouseNumber() + hotListAddressInfo.getNotes()
				+ hotListAddressInfo.getCommunicationType() + hotListAddressInfo.getPhoneNoCountryCode()
				+ hotListAddressInfo.getPhoneNoAreaCode() + hotListAddressInfo.getPhoneNo()
				+ hotListAddressInfo.getTelexNoCountryCode() + hotListAddressInfo.getTelexNoAreaCode()
				+ hotListAddressInfo.getTelexNo() + hotListAddressInfo.getPagerNoCountryCode()
				+ hotListAddressInfo.getPagerNoAreaCode() + hotListAddressInfo.getPagerNo();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param hotListMediaInfo
	 * @param hotListId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForHotListMedia(Attachment hotListMediaInfo, Long hotListId, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = hotListId + hotListMediaInfo.getNotes() + hotListMediaInfo.getPublishedDate()
				+ hotListMediaInfo.getExtensionText() + hotListMediaInfo.getMediaType()
				+ hotListMediaInfo.getMediaContent().getBytes() + hotListMediaInfo.getSourceOfInformation()
				+ hotListMediaInfo.getSourceType();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param virtualAccount
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForVirtualAccount(VirtualAccount virtualAccount)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = virtualAccount.getRefDr() + virtualAccount.getNprAmount() + virtualAccount.getVirtualAccNo()
				+ virtualAccount.getChannel() + virtualAccount.getRemitter() + virtualAccount.getReferenceNo()
				+ virtualAccount.getTransferAmount() + virtualAccount.getDate() + virtualAccount.getActualIncome()
				+ virtualAccount.getSerialNo() + virtualAccount.getTrxDate() + virtualAccount.getInWords()
				+ virtualAccount.getRefNo() + virtualAccount.getBeneficiaryName() + virtualAccount.getNetAmount()
				+ virtualAccount.getBranch() + virtualAccount.getGmail() + virtualAccount.getInitDate()
				+ virtualAccount.getInrAmount() + virtualAccount.getBeneficiaryAccNo()
				+ virtualAccount.getBusinessDate() + virtualAccount.getVaNo();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param branchInfo
	 * @param bfiInfoId
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generatehashValueForBranchInfo(BranchInfo branchInfo, Long bfiInfoId, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = bfiInfoId + branchInfo.getName() + branchInfo.getReference() + branchInfo.getSolId()
				+ branchInfo.getLocalCurrency() + branchInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param branchAddress
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForBranchAddress(BranchAddress branchAddress, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = branchAddress.getBranchId() + branchAddress.getCountry() + branchAddress.getState()
				+ branchAddress.getProvince() + branchAddress.getDistrict() + branchAddress.getMnVdc()
				+ branchAddress.getTownCityVillage() + branchAddress.getTole() + branchAddress.getStreet()
				+ branchAddress.getHouseNumber() + branchAddress.getPoBoxNumber() + branchAddress.getPinZipNumber()
				+ branchAddress.getNotes() + branchAddress.getAddressType();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param branchContact
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForBranchContact(BranchContact branchContact, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = branchContact.getBranchId() + branchContact.getNumber() + branchContact.getPrefix()
				+ branchContact.getExtension() + branchContact.getCommunicationType() + branchContact.getNotes();

		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param branchEmail
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForEmail(BranchEmail branchEmail, User user, boolean includeUser)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = branchEmail.getBranchId() + branchEmail.getEmail() + branchEmail.getEmailType();

		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param meta
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForSwiftFileMeta(FileMeta meta)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = meta.getDescription() + meta.getDate() + meta.getTime() + meta.getOthers();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param messageHeader
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForMessageHeader(MessageHeader messageHeader, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + messageHeader.getSwiftInput();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field50k
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForFile50K(Field50K field50k, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field50k.getAccount() + field50k.getAddress() + field50k.getFirstName()
				+ field50k.getMiddleName() + field50k.getLastName();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field59
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForFile59K(Field59 field59, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field59.getDescription() + field59.getAccount() + field59.getName()
				+ field59.getAddress();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param sender
	 * @param headerId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForSender(Sender sender, Long headerId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = headerId + sender.getSwiftCode() + sender.getBankName() + sender.getCity()
				+ sender.getCode();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param receiver
	 * @param headerId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForReceiver(Receiver receiver, Long headerId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = headerId + receiver.getSwiftCode() + receiver.getBankName() + receiver.getBranchName()
				+ receiver.getCity() + receiver.getCode();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param instanceTypeAndTransmission
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForInstanceTypeAndTransmission(
			InstanceTypeAndTransmission instanceTypeAndTransmission, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + instanceTypeAndTransmission.getReference()
				+ instanceTypeAndTransmission.getDescription() + instanceTypeAndTransmission.getStatus()
				+ instanceTypeAndTransmission.getPriority();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field57a
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField57A(Field57A field57a, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field57a.getDescription() + field57a.getSwiftCode() + field57a.getBankName()
				+ field57a.getAddress() + field57a.getCode();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field70
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField70(Field70 field70, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field70.getDescription() + field70.getValue();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field71a
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField71A(Field71A field71a, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field71a.getDescription() + field71a.getValue();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field52a
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForField52A(Field52A field52a, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field52a.getSwiftCode() + field52a.getBankName() + field52a.getAddress()
				+ field52a.getCode();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field53a
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */

	public String generateHashValueForField53A(Field53A field53a, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field53a.getSwiftCode() + field53a.getBankName() + field53a.getAddress()
				+ field53a.getCode() + field53a.getAccount();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param interventions
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForInterventions(Interventions interventions, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + interventions.getOperator() + interventions.getCategory()
				+ interventions.getCreationTime() + interventions.getApplication();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param messageTrailer
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForMessageTrailer(MessageTrailer messageTrailer, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + messageTrailer.getSignature();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field20
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField20HashValue(Field20 field20, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field20.getDescription() + field20.getValue();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field23b
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField23B(Field23B field23b, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field23b.getDescription() + field23b.getValue();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param field32a
	 * @param metaId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForField32A(Field32A field32a, Long metaId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = metaId + field32a.getDescription() + field32a.getDate().getTime() + field32a.getAmount()
				+ field32a.getCurrency();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnPersonalInfo
	 * @param user
	 * @param includeUser
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnPersonalInfo(KycnPersonalInfo kycnPersonalInfo, User user,
			boolean includeUser) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfo.getSalutation() + kycnPersonalInfo.getFirstName()
				+ kycnPersonalInfo.getMiddleName() + kycnPersonalInfo.getLastName() + kycnPersonalInfo.getLsTitle()
				+ kycnPersonalInfo.getLsfName() + kycnPersonalInfo.getLsmName() + kycnPersonalInfo.getLslName()
				+ kycnPersonalInfo.getSecondName() + kycnPersonalInfo.getCalledByName()
				+ kycnPersonalInfo.getPreviousName() + kycnPersonalInfo.getGender() + kycnPersonalInfo.getDateOfBirth()
				+ kycnPersonalInfo.getAge() + kycnPersonalInfo.isMinor() + kycnPersonalInfo.getMaritalStatus()
				+ kycnPersonalInfo.getNotes() + kycnPersonalInfo.getCustomerType() + kycnPersonalInfo.getCustomerGroup()
				+ kycnPersonalInfo.getCustomerConstitution() + kycnPersonalInfo.getCustomerCommunity()
				+ kycnPersonalInfo.getCustomerCaste() + kycnPersonalInfo.getCustomerEmployeeId()
				+ kycnPersonalInfo.getCustomerOpenDate() + kycnPersonalInfo.getCustomerStatusCode()
				+ kycnPersonalInfo.isNonResidentExternal() + kycnPersonalInfo.isCardHolder() + user.getUserName()
				+ kycnPersonalInfo.getScreeningId() + kycnPersonalInfo.getCustomerId() + kycnPersonalInfo.isVerified();

		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnIdentificationInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnIdentificationInfo(KycnIdentificationInfo kycnIdentificationInfo, User user,
			boolean includeUser, Long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + kycnIdentificationInfo.getPrimaryIdentificationDocumentType()
				+ kycnIdentificationInfo.getPrimaryIdentificationDocumentNo()
				+ kycnIdentificationInfo.getCountryOfIssue() + kycnIdentificationInfo.getPassportNo()
				+ kycnIdentificationInfo.getPassportCountry() + kycnIdentificationInfo.getPassportIssuingAuthority()
				+ kycnIdentificationInfo.getPassportPlaceOfIssue() + kycnIdentificationInfo.getPassportIssueDate()
				+ kycnIdentificationInfo.getExpiryDate() + kycnIdentificationInfo.getVisaNo()
				+ kycnIdentificationInfo.getVisaExpiryDate() + kycnIdentificationInfo.getNepalEntryDate()
				+ kycnIdentificationInfo.getNotes();

		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param financialInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnFinancialInfo(KycnFinancialInfo financialInfo, User user, boolean includeUser,
			long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + financialInfo.getCustomerPan() + financialInfo.getCustomerCurrency();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param individual
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnSubscribedIndividualInfo(String individual, User user, boolean includeUser,
			long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + individual;
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param service
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnSubscribedServiceInfo(String service, User user, boolean includeUser,
			long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + service;
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnRelatedEntityInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnRelatedEntityInfo(KycnRelatedEntityInfo kycnRelatedEntityInfo, User user,
			boolean includeUser, long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + kycnRelatedEntityInfo.getRelatedEntityType()
				+ kycnRelatedEntityInfo.getCustId() + kycnRelatedEntityInfo.getKycnId()
				+ kycnRelatedEntityInfo.getSalutatiton() + kycnRelatedEntityInfo.getName()
				+ kycnRelatedEntityInfo.getLsName() + kycnRelatedEntityInfo.getCalledByName()
				+ kycnRelatedEntityInfo.getPrimaryIdentificationDocumentType()
				+ kycnRelatedEntityInfo.getRegistrationNo() + kycnRelatedEntityInfo.getRelatedEntityCountry()
				+ kycnRelatedEntityInfo.getZone() + kycnRelatedEntityInfo.getDistrict()
				+ kycnRelatedEntityInfo.getMnVdc() + kycnRelatedEntityInfo.getPinzip()
				+ kycnRelatedEntityInfo.getWardNumber() + kycnRelatedEntityInfo.getToleArea()
				+ kycnRelatedEntityInfo.getStreet() + kycnRelatedEntityInfo.getHouseNo()
				+ kycnRelatedEntityInfo.getNearestLandmark() + kycnRelatedEntityInfo.getLatitude()
				+ kycnRelatedEntityInfo.getLongitude() + kycnRelatedEntityInfo.getPhoneNoCountryCode()
				+ kycnRelatedEntityInfo.getPhoneNoAreaCode() + kycnRelatedEntityInfo.getPhoneNo()
				+ kycnRelatedEntityInfo.getTelexNoCountryCode() + kycnRelatedEntityInfo.getTelexNoAreaCode()
				+ kycnRelatedEntityInfo.getTelexNo() + kycnRelatedEntityInfo.getEmailId()
				+ kycnRelatedEntityInfo.getNotes() + kycnRelatedEntityInfo.getUnitNumber();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnRelatedPersonInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnRelatedPersonInfo(KycnRelatedPersonInfo kycnRelatedPersonInfo, User user,
			boolean includeUser, long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + kycnRelatedPersonInfo.getPersonType() + kycnRelatedPersonInfo.getCustId()
				+ kycnRelatedPersonInfo.getKycnId() + kycnRelatedPersonInfo.getSalutation()
				+ kycnRelatedPersonInfo.getFirstName() + kycnRelatedPersonInfo.getMiddleName()
				+ kycnRelatedPersonInfo.getLastName() + kycnRelatedPersonInfo.getLsfName()
				+ kycnRelatedPersonInfo.getLsmName() + kycnRelatedPersonInfo.getLslName()
				+ kycnRelatedPersonInfo.getSecondName() + kycnRelatedPersonInfo.getCalledByName()
				+ kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType() + kycnRelatedPersonInfo.getCountry()
				+ kycnRelatedPersonInfo.getIssuingAuthority() + kycnRelatedPersonInfo.getPlaceOfIssue()
				+ kycnRelatedPersonInfo.getIssueDate() + kycnRelatedPersonInfo.getExpiryDate()
				+ kycnRelatedPersonInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnDocumentStatusInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnDocumentStatusInfo(KycnDocumentStatusInfo kycnDocumentStatusInfo, User user,
			boolean includeUser, Long kycnPersonalId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalId + kycnDocumentStatusInfo.getDocumentType()
				+ kycnDocumentStatusInfo.getDocumentStatus() + kycnDocumentStatusInfo.getDate()
				+ kycnDocumentStatusInfo.getRefershDate() + kycnDocumentStatusInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnAmlInformation
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnAmlInformationInfo(KycnAmlInformationInfo kycnAmlInformation, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnAmlInformation.getRiskCategorization()
				+ kycnAmlInformation.getRecommendations() + kycnAmlInformation.getAmlNotes()
				+ kycnAmlInformation.isAmlCheck() + kycnAmlInformation.isFatcaCountry()
				+ kycnAmlInformation.isUsNonUs();

		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnObservationInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnObservationInfo(KycnObservationInfo kycnObservationInfo, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnObservationInfo.getObservationType()
				+ kycnObservationInfo.getInternalObservationPhysical()
				+ kycnObservationInfo.getInternalObservationFinancial()
				+ kycnObservationInfo.getInternalObservationBehavioral()
				+ kycnObservationInfo.getInternalObservationConnectedPerson()
				+ kycnObservationInfo.getConnectedPersonId()
				+ kycnObservationInfo.getInternalObservationIntendedObjectiveOfBusinessRelation()
				+ kycnObservationInfo.getObservationMediaSource() + kycnObservationInfo.getObservationDate()
				+ kycnObservationInfo.getObservationTime() + kycnObservationInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnRelationInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnRelationInfo(KycnRelationInfo kycnRelationInfo, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnRelationInfo.getRelationType() + kycnRelationInfo.getCustId()
				+ kycnRelationInfo.getKycnId() + kycnRelationInfo.getFirstName() + kycnRelationInfo.getMiddleName()
				+ kycnRelationInfo.getLastName() + kycnRelationInfo.getLsfName() + kycnRelationInfo.getLsmName()
				+ kycnRelationInfo.getLslName() + kycnRelationInfo.getSecondName() + kycnRelationInfo.getCalledByName()
				+ kycnRelationInfo.getPrimaryIdentificationDocumentType()
				+ kycnRelationInfo.getPrimaryIdentificationDocumentNo() + kycnRelationInfo.getRelationCountry()
				+ kycnRelationInfo.getIssuingAuthority() + kycnRelationInfo.getPlaceOfIssue()
				+ kycnRelationInfo.getIssueDate() + kycnRelationInfo.getExpiryDate() + kycnRelationInfo.getNotes()
				+ kycnRelationInfo.getSalutation();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnInvolvementInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnInvolvementInfo(KycnInvolvementInfo kycnInvolvementInfo, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnInvolvementInfo.getKyclId()
				+ kycnInvolvementInfo.getOrganizationNature() + kycnInvolvementInfo.getCountry()
				+ kycnInvolvementInfo.getZone() + kycnInvolvementInfo.getDistrict() + kycnInvolvementInfo.getMnVdc()
				+ kycnInvolvementInfo.getPinZip() + kycnInvolvementInfo.getInvolvementWardNumber()
				+ kycnInvolvementInfo.getToleArea() + kycnInvolvementInfo.getStreet() + kycnInvolvementInfo.getHouseNo()
				+ kycnInvolvementInfo.getUnitNumber() + kycnInvolvementInfo.getNearestLandMark()
				+ kycnInvolvementInfo.getLatitude() + kycnInvolvementInfo.getLongitude()
				+ kycnInvolvementInfo.getPhoneNoCountryCode() + kycnInvolvementInfo.getPhoneNoAreaCode()
				+ kycnInvolvementInfo.getPhoneNo() + kycnInvolvementInfo.getFaxNoCountryCode()
				+ kycnInvolvementInfo.getFaxNoAreaCode() + kycnInvolvementInfo.getFaxNo()
				+ kycnInvolvementInfo.getEmailId() + kycnInvolvementInfo.getWebsite() + kycnInvolvementInfo.getPanVat()
				+ kycnInvolvementInfo.getPoBoxNo() + kycnInvolvementInfo.getNature()
				+ kycnInvolvementInfo.getDesignation() + kycnInvolvementInfo.getStartDate()
				+ kycnInvolvementInfo.getEndDate() + kycnInvolvementInfo.getNotes()
				+ kycnInvolvementInfo.getInvolvementNature() + kycnInvolvementInfo.getOccupationType();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnEducationInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnEducationInfo(KycnEducationInfo kycnEducationInfo, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnEducationInfo.getQualification()
				+ kycnEducationInfo.getNameOfInstitute() + kycnEducationInfo.getFieldOfStudy()
				+ kycnEducationInfo.getYearOfGraduation() + kycnEducationInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnAddressInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnAddressInfo(KycnAddressInfo kycnAddressInfo, User user, boolean includeUser,
			Long kycnPersonalInfoId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnAddressInfo.getType() + kycnAddressInfo.getCountry()
				+ kycnAddressInfo.getZone() + kycnAddressInfo.getDistrict() + kycnAddressInfo.getMnVdc()
				+ kycnAddressInfo.getPinZip() + kycnAddressInfo.getWardNumber() + kycnAddressInfo.getToleArea()
				+ kycnAddressInfo.getStreet() + kycnAddressInfo.getHouseNumber() + kycnAddressInfo.getUnitNumber()
				+ kycnAddressInfo.getNearestLandMark() + kycnAddressInfo.getLatitude() + kycnAddressInfo.getLongitude()
				+ kycnAddressInfo.getPhoneNoCountryCode() + kycnAddressInfo.getPhoneNoAreaCode()
				+ kycnAddressInfo.getPhoneNo() + kycnAddressInfo.getTelexNoCountryCode()
				+ kycnAddressInfo.getTelexNoAreaCode() + kycnAddressInfo.getTelexNo()
				+ kycnAddressInfo.getPagerNoCountryCode() + kycnAddressInfo.getPagerNoAreaCode()
				+ kycnAddressInfo.getPagerNo() + kycnAddressInfo.getEmailId() + kycnAddressInfo.getNotes();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param kycnAccountsInfo
	 * @param user
	 * @param includeUser
	 * @param kycnPersonalInfoId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForKycnAccountsInfo(KycnAccountsInfo kycnAccountsInfo, User user,
			boolean includeUser, Long kycnPersonalInfoId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = kycnPersonalInfoId + kycnAccountsInfo.getAccountId() + kycnAccountsInfo.getForAccountId()
				+ kycnAccountsInfo.getCurrencyOfAccount() + kycnAccountsInfo.getAccountNo()
				+ kycnAccountsInfo.getAccountName() + kycnAccountsInfo.getAccountShortName()
				+ kycnAccountsInfo.getAccountOwnership() + kycnAccountsInfo.getSchemeType()
				+ kycnAccountsInfo.getGlSubHeadCode() + kycnAccountsInfo.getProductGroup()
				+ kycnAccountsInfo.getLastTransactionDate() + kycnAccountsInfo.getAccountOpenDate()
				+ kycnAccountsInfo.getEstimatedYearlyTransaction() + kycnAccountsInfo.getEstimatedMonthlyTransaction()
				+ kycnAccountsInfo.getEstimatedYearlyTurnOver() + kycnAccountsInfo.getEstimatedMonthlyTurnOver()
				+ kycnAccountsInfo.getRegularSourceOfIncome() + kycnAccountsInfo.getSourceOfFund()
				+ kycnAccountsInfo.getNotes() + kycnAccountsInfo.getSchemeCode();
		if (includeUser) {
			hashValue = hashValue + user.getUserName();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestPrimaryRequestData
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForScreeningLegal(RequestPrimaryRequestData requestPrimaryRequestData, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = requestPrimaryRequestData.getNameOfInstitution() + requestPrimaryRequestData.getLsName()
				+ requestPrimaryRequestData.getDateOfEstablishment() + requestPrimaryRequestData.getCountryOfIssue()
				+ requestPrimaryRequestData.getState() + requestPrimaryRequestData.getDistrict()
				+ requestPrimaryRequestData.getMnVdc() + requestPrimaryRequestData.getTypeOfIndustry() + user.getSolId()
				+ requestPrimaryRequestData.getCustId() + requestPrimaryRequestData.getStatus()
				+ requestPrimaryRequestData.getPurposeOfScreening() + requestPrimaryRequestData.getRiskValue()
				+ requestPrimaryRequestData.getZone() + requestPrimaryRequestData.getWardNo()
				+ requestPrimaryRequestData.getPanNumber() + requestPrimaryRequestData.getContactNumber()
				+ requestPrimaryRequestData.getEmailId() + requestPrimaryRequestData.getNatureOfAccount()
				+ requestPrimaryRequestData.getSchemeDescription() + requestPrimaryRequestData.getDepositAmount()
				+ requestPrimaryRequestData.getNotes() + requestPrimaryRequestData.isHaskyc()
				+ requestPrimaryRequestData.getKyclId() + user.getUserName()
				+ requestPrimaryRequestData.getAccountsLSubType() + requestPrimaryRequestData.isHasCustId();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestPrimaryMatchedData
	 * @param screeningId
	 * @param tableName
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRequestPrimaryMatchedData(RequestPrimaryMatchedData requestPrimaryMatchedData,
			long screeningId, String tableName) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = String.valueOf(screeningId) + String.valueOf(requestPrimaryMatchedData.getId());
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestRelatedEntityRequestData
	 * @param screeningId
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRelatedEntity(RequestRelatedEntityRequestData requestRelatedEntityRequestData,
			Long screeningId, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = requestRelatedEntityRequestData.getNameOfInstitution()
				+ requestRelatedEntityRequestData.getLsName() + requestRelatedEntityRequestData.getDateOfEstablishment()
				+ requestRelatedEntityRequestData.getDistrict() + requestRelatedEntityRequestData.getMnVdc()
				+ requestRelatedEntityRequestData.getTypeOfIndustry() + requestRelatedEntityRequestData.getCustId()
				+ requestRelatedEntityRequestData.getZone() + requestRelatedEntityRequestData.getWardNo()
				+ requestRelatedEntityRequestData.getPanNumber() + requestRelatedEntityRequestData.getContactNumber()
				+ requestRelatedEntityRequestData.getEmailId() + requestRelatedEntityRequestData.getNotes()
				+ requestRelatedEntityRequestData.getKyclId() + user.getUserName()
				+ requestRelatedEntityRequestData.getAccountsLSubType()
				+ requestRelatedEntityRequestData.getRegistrationNo()
				+ requestRelatedEntityRequestData.getCountryOfIssue() + requestRelatedEntityRequestData.isHasCustId()
				+ requestRelatedEntityRequestData.isHaskyc();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestRelatedId
	 * @param screeningId
	 * @param requestRelatedEntityRequestData
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForReqestRelatedRelatedEntity(Long requestRelatedId, Long screeningId,
			RequestRelatedEntityRequestData requestRelatedEntityRequestData)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = screeningId + requestRelatedId + requestRelatedEntityRequestData.getAccountsLSubType()
				+ requestRelatedEntityRequestData.getNotes();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	public String generateHashValueForReqestRelatedMatchedRelatedEntity(
			RequestRelatedEntityMatchData requestRelatedEntityMatchData, Long requestRelatedId, String tableName)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = String.valueOf(requestRelatedId) + String.valueOf(requestRelatedEntityMatchData.getId());
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestRelatedPerson
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForReqestRelatedPerson(RequestRelatedPerson requestRelatedPerson, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = requestRelatedPerson.getFirstName() + requestRelatedPerson.getMiddleName()
				+ requestRelatedPerson.getLastName() + requestRelatedPerson.getLsfName()
				+ requestRelatedPerson.getLsmName() + requestRelatedPerson.getLslName()
				+ requestRelatedPerson.getDateOfBirth() + requestRelatedPerson.getCountryOfIssue()
				+ requestRelatedPerson.getDistrict() + requestRelatedPerson.getMnVdc() + user.getUserName()
				+ user.getSolId() + requestRelatedPerson.getKycnId() + requestRelatedPerson.getAccountsLSubType()
				+ ConstantStatus.PROCEED_MAKER_CHECKER + requestRelatedPerson.getNotes()
				+ requestRelatedPerson.getZone() + requestRelatedPerson.getSalutation()
				+ requestRelatedPerson.getGender() + requestRelatedPerson.getMobileNumber()
				+ requestRelatedPerson.getEmailId() + requestRelatedPerson.isHaskycl()
				+ requestRelatedPerson.isHasCustId() + requestRelatedPerson.getCustId();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param screeningId
	 * @param accountsLSubType
	 * @param relatedPersonScreeningId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRelatedPerson(Long screeningId, String accountsLSubType,
			Long relatedPersonScreeningId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = screeningId + accountsLSubType + relatedPersonScreeningId;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param requestRelatedMatchedPersonData
	 * @param relatedPersonScreeningId
	 * @param tableName
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRelatedMatchedPerson(
			RequestRelatedMatchedPersonData requestRelatedMatchedPersonData, Long relatedPersonScreeningId,
			String tableName) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = String.valueOf(relatedPersonScreeningId)
				+ String.valueOf(requestRelatedMatchedPersonData.getId());
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param screeningLegalRequestAttachment
	 * @param screeningId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAttachment(ScreeningLegalRequestAttachments screeningLegalRequestAttachment,
			Long screeningId) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = screeningId + screeningLegalRequestAttachment.getScannedDocumentType()
				+ screeningLegalRequestAttachment.getScannedContent().getBytes()
				+ screeningLegalRequestAttachment.getExtensionText() + screeningLegalRequestAttachment.getNotes();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param status
	 * @param screeningId
	 * @param condtion
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForWorkFlowTable(String status, Long screeningId, boolean condtion)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = status + screeningId + "true";
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param repairedId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForWorkFlowAfterRepair(long repairedId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = "true" + repairedId;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param screeningId
	 * @param repairedId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForPreviousCrrentScreeningMapping(Long screeningId, long repairedId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = String.valueOf(repairedId) + String.valueOf(screeningId);
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param parseInt
	 * @return
	 */
	public String generateHashValueForRisk(int parseInt) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @param card
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskCard(Card card, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = card.getId() + card.getDocumentCode() + card.getProductCode() + card.getName()
				+ card.getType() + card.getDomestic() + card.getForeign() + card.getThershold() + card.getActive()
				+ card.getActiveDate().getTime() + card.getRiskB() + risk + card.getNotes() + user.getUserName()
				+ user.getUserName() + "true" + new Date().getTime() + new Date().getTime() + reason;
		if (card.getDisableDate() != null) {
			hashValue += card.getDisableDate().getTime();
		}
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param deposit
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskProductDeposit(ProductDeposit deposit, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = deposit.getId() + deposit.getDocumentCode() + deposit.getProductCode() + deposit.getName()
				+ deposit.getActive() + new java.sql.Date(deposit.getActiveDate().getTime())
				+ new java.sql.Date(deposit.getDisableDate().getTime()) + deposit.getRiskB() + risk
				+ deposit.getLocalCurrency() + deposit.getForeign() + deposit.getForeignCurrency() + deposit.getNotes()
				+ user.getUserName() + user.getUserName() + true + new java.sql.Date(new Date().getTime())
				+ new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param listRisk
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskListRisk(ListRisk listRisk, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = listRisk.getId() + listRisk.getDocumentcode() + listRisk.getType() + listRisk.getRiskB()
				+ risk + listRisk.getNotes() + listRisk.getActive()
				+ new java.sql.Date(listRisk.getActiveDate().getTime())
				+ new java.sql.Date(listRisk.getDisabeDate().getTime()) + user.getUserName() + user.getUserName() + true
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param remittance
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskRemittance(Remittance remittance, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = remittance.getId() + remittance.getDocumentCode() + remittance.getProductCode()
				+ remittance.getName() + remittance.getInoutWard() + remittance.getActive()
				+ new java.sql.Date(remittance.getActiveDate().getTime())
				+ new java.sql.Date(remittance.getDisableDate().getTime()) + remittance.getRiskB() + risk
				+ remittance.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param pep
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskPep(Pep pep, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = pep.getId() + pep.getCategory() + pep.getLevel() + pep.getName() + pep.getActive()
				+ new java.sql.Date(pep.getActiveDate().getTime()) + new java.sql.Date(pep.getDisableDate().getTime())
				+ pep.getRiskB() + risk + pep.getNotes() + user.getUserName() + user.getUserName() + true
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param occupational
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskOccupational(Occupational occupational, Integer risk, String reason,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = occupational.getId() + occupational.getDocumentCode() + occupational.getMajorGroupCode()
				+ occupational.getMinorGroupCode() + occupational.getBroadGroupCode() + occupational.getDetailCode()
				+ occupational.getName() + occupational.getActive()
				+ new java.sql.Date(occupational.getActiveDate().getTime())
				+ new java.sql.Date(occupational.getDisableDate().getTime()) + occupational.getRiskB() + risk
				+ occupational.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param industry
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskIndustry(Industry industry, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = industry.getId() + industry.getDocumentCode() + industry.getMajorGroupCode()
				+ industry.getBroadGroupCode() + industry.getGroupDetailCode() + industry.getDetailCode()
				+ industry.getName() + industry.getActive() + new java.sql.Date(industry.getActiveDate().getTime())
				+ new java.sql.Date(industry.getDisableDate().getTime()) + industry.getRiskB() + risk
				+ industry.getNotes() + user.getUserName() + user.getUserName() + true
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param environment
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskCrimeEnvironment(CrimeEnvironment environment, Integer risk, String reason,
			User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = environment.getId() + environment.getDocumentCode() + environment.getGroupCode()
				+ environment.getCode() + environment.getLocation() + environment.getName() + environment.getActive()
				+ new java.sql.Date(environment.getActiveDate().getTime())
				+ new java.sql.Date(environment.getDisableDate().getTime()) + environment.getRiskB() + risk
				+ environment.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param businessEnvironment
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskBusinessEnvironment(BusinessEnvironment businessEnvironment, Integer risk,
			String reason, User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashCode = businessEnvironment.getId() + businessEnvironment.getDocumentCode()
				+ businessEnvironment.getGroupCode() + businessEnvironment.getCode() + businessEnvironment.getLocation()
				+ businessEnvironment.getName() + businessEnvironment.getActive()
				+ new java.sql.Date(businessEnvironment.getActiveDate().getTime())
				+ new java.sql.Date(businessEnvironment.getDisableDate().getTime()) + businessEnvironment.getRiskB()
				+ risk + businessEnvironment.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashCode);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param delivery
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForDelivery(Delivery delivery, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = delivery.getId() + delivery.getDocumentCode() + delivery.getProductCode()
				+ delivery.getAction() + delivery.getName() + delivery.getActive()
				+ new java.sql.Date(delivery.getActiveDate().getTime())
				+ new java.sql.Date(delivery.getDisableDate().getTime()) + delivery.getRiskB() + risk
				+ delivery.getNotes() + user.getUserName() + user.getUserName() + true
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param country
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForCountry(Country country, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = country.getId() + country.getCountryCode() + country.getCounty() + country.getName()
				+ country.getNumericCode() + country.getActive() + new java.sql.Date(country.getActiveDate().getTime())
				+ new java.sql.Date(country.getDisabledDate().getTime()) + country.getRiskB() + risk
				+ country.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param bankService
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskBankService(BankService bankService, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = bankService.getId() + bankService.getDocumentCode() + bankService.getServiceCode()
				+ bankService.getName() + bankService.getActive()
				+ new java.sql.Date(bankService.getActiveDate().getTime())
				+ new java.sql.Date(bankService.getDisabledDate().getTime()) + bankService.getRiskB() + risk
				+ bankService.getNotes() + user.getUserName() + user.getUserName() + true
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param productLoan
	 * @param risk
	 * @param reason
	 * @param user
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForRiskProductLoan(ProductLoan productLoan, Integer risk, String reason, User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = productLoan.getId() + productLoan.getDocumentCode() + productLoan.getProductCode()
				+ productLoan.getName() + productLoan.getActive()
				+ new java.sql.Date(productLoan.getActiveDate().getTime())
				+ new java.sql.Date(productLoan.getDisableDate().getTime()) + productLoan.getRiskB() + risk
				+ productLoan.getNotes() + user.getUserName() + user.getUserName() + "true"
				+ new java.sql.Date(new Date().getTime()) + new java.sql.Date(new Date().getTime()) + reason;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param screeningLegalInfo
	 * @param status
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForScreeningWorkFlow(ScreeningLegalInfo screeningLegalInfo, String status)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = "false" + "false" + status;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param status
	 * @param scCompleted
	 * @param screeningLId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForWorkFlowTableReply(String status, boolean scCompleted, long screeningLId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = status + scCompleted + screeningLId;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param status
	 * @param screeningCompleted
	 * @param workflowCompleted
	 * @param screeningLId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForWorkFlowCompleted(String status, boolean screeningCompleted,
			boolean workflowCompleted, long screeningLId)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = status + screeningCompleted + workflowCompleted + screeningLId;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param accountLegalReply
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForAccountReply(AccountReply accountLegalReply)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = accountLegalReply.getAccountLegalReply().getAccountsLegalId()
				+ accountLegalReply.getUser().getUserName() + "true";
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param accountLegalReplyId
	 * @param accountLegalReply
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForLegalAction(Long accountLegalReplyId, AccountReply accountLegalReply)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = accountLegalReplyId + accountLegalReply.getUser().getUserName()
				+ accountLegalReply.getAccountLegalReply().getReason()
				+ accountLegalReply.getAccountLegalReply().getActionType()
				+ accountLegalReply.getAccountLegalReply().getActionSubType()
				+ accountLegalReply.getAccountLegalReply().getGeneratedCustomerId()
				+ accountLegalReply.getAccountLegalReply().getForacId()
				+ accountLegalReply.getAccountLegalReply().getCustName();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param status
	 * @param accountOpened
	 * @param accountCompleted
	 * @param workflowCompleted
	 * @param accountsLegalId
	 * @param freeze
	 * @param acoountRequested
	 * @param requestToUnfreeze
	 * @param aofReceived
	 * @param documentSent
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForUpdateWorkflow(String status, boolean accountOpened, boolean accountCompleted,
			boolean workflowCompleted, long accountsLegalId, boolean freeze, boolean acoountRequested,
			boolean requestToUnfreeze, boolean aofReceived, boolean documentSent)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = status + accountOpened + accountCompleted + workflowCompleted + freeze + acoountRequested
				+ requestToUnfreeze + aofReceived + documentSent + accountsLegalId;
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

	/**
	 * 
	 * @param accountLegalReply
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public String generateHashValueForUdpateAccountLegalRequest(AccountReply accountLegalReply)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String hashValue = accountLegalReply.getAccountLegalReply().getGeneratedCustomerId()
				+ accountLegalReply.getAccountLegalReply().getForacId()
				+ accountLegalReply.getAccountLegalReply().getAccountsLegalId();
		String generatedEncryptedHashValue = generateEncryptedHashValue(hashValue);
		return generatedEncryptedHashValue;
	}

}
