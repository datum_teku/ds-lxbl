package com.trustaml.service.common.constant;

public class ConstantSanctionType {

	public static final String PEP_TYPE = "01";
	public static final String ADVERSEMEDIA = "02";
	public static final String INVESTIGATION = "03";
	public static final String USER_FOR_SANCTION = System.getProperty("user.for.sanction.list");
}
