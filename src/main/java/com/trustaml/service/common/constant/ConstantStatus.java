package com.trustaml.service.common.constant;

public class ConstantStatus {

	// checker-maker-cap
	public static final String PROCEED_CHECKER_MAKER = "ProceedCheckerMaker";
	public static final String PROCEED_MAKER_CHECKER = "ProceedMakerChecker";
	public static final String PROCEED_CHECKER_CAP = "ProceedCheckerCap";
	public static final String OPEN_NEW_ACCOUNT = "Open new account";
	public static final String APPLY_FOR_LOAN = "Apply for loan";
	public static final String INWARD_REMITTANCE = "Inward remittance";
	public static final String OUTWARD_REMITTANCE = "Outward remittance";
	public static final String MONEY_EXCHANGE = "Money exchange";
	public static final String OTHERS = "Others";

	// Risk Action Type

	public static final String PROCEED_HIGH_RISK = "Proceed - High Risk";
	public static final String PROCEED_MEDIUM_RISK = "Proceed - Medium Risk";
	public static final String PROCEED_LOW_RISK = "Proceed - Low Risk";
	public static final String ADDITIONAL_DOCUMENTS_CAP = "Additional documents";
	public static final String ADDITIONAL_DOCUMENTS_CHECKER = "Additional Documents";
	public static final String REPAIR = "Repair";
	public static final String REJECT = "Reject";
	public static final String FORWARD_TO = "Forward To";
	public static final String REMITTANCE="Inward remittance";
	
	// cap-maker-cap
	public static final String PROCEED_CAP_MAKER = "ProceedCapMaker";
	public static final String PROCEED_MAKER_CAP = "ProceedMakerCap";
	public static final String FREEZE_ACCOUNT = "Freeze account";
	public static final String ACCOUNT_OPENED = "Account opened";
	public static final String DOCUMENT_AOF_PENDING = "Document/AOF pending";
	public static final String DOCUMENT_AOF_RECEIVED = "Document/AOF received";
	public static final String DEFICIT_DOCUMENTS = "Deficit documents";
	public static final String PROCEED_FOR_KYC = "Proceed for KYC";
	public static final String REJECTED = "Rejected";
	public static final String DUE_DATE_ACKNOWLEDGED = "Due date acknowledged";
	public static final String DUE_DATE_REJECTED = "Due date rejected";
	public static final String UN_FREEZE_ACCOUNT = "Unfreeze account";
	
}
