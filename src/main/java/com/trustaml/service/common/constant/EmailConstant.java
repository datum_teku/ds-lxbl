package com.trustaml.service.common.constant;

public class EmailConstant {

	public static final String KYC_REFRESH_DATE_REACHING_SUBJECT = "KYC Refresh Date";
	public static final String EMAIL_OF_REFRESH_DATE_REACHING = System.getProperty("email.id.to.send.kyc.refresh.data");
	public static final String BODY_OF_EMAIL_FOR_REFRESH_DATE_REACHING = "Dear Sir,</p> This is to notify you that following are the info of the user whose kyc refresh date reached to end as set in database determine by admin.</p>";

}
