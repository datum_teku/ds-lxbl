package com.trustaml.service.common.constant;

public class MatchConstant {

	public static final String UN_MATCH_CASE = "UN";
	public static final String UN_MATCH_CASE_LEGAL = "UN-ENTITY";

	
	public static final String HMT_MATCH_CASE = "HMT";
	public static final String HMT_ENTITY_MATCH_CASE = "HMT-ENTITY";
	
	public static final String EU_MATCH_CASE = "EU";
	public static final String EU_ENTITY_MATCH_CASE= "EU-ENTITY";
	
	
	
	public static final String OFAC_MATCH_CASE = "OFAC";
	public static final String OFAC_ENTITY_MATCH_CASE = "OFAC-ENTITY";
	
	public static final String PEP_MATCH_CASE = "PEP";
	
	public static final String ADVERSE_MATCH_CASE = "ADVERSE-MEDIA";
	public static final String ADVERSE_ENTITY_MATCH_CASE = "ADVERSE-MEDIA-ENTITY";
	
	public static final String HOT_LIST_MATCH_CASE = "HOT-LIST";
	public static final String HOT_LIST_ENTITY_MATCH_CASE = "HOT-LIST-ENTITY";
	
	public static final String INVESTIGATION_MATCH_CASE = "INVESTIGATION";
	public static final String INVESTIGATION_ENTITY_MATCH_CASE = "INVESTIGATION-ENTITY";
	
	public static final String ACCUITY_MATCH_CASE = "ACCUITY-PEP";
	public static final String ACCUITY_PEP_ENTITY_MATCH_CASE = "ACCUITY-PEP-ENTITY";
	
	public static final String ACCUITY_SDN_MATCH_CASE = "ACCUITY-SDN";
	public static final String ACCUITY_SDN_ENTITY_MATCH_CASE = "ACCUITY-SDN-ENTITY";
	
	public static final String ACCUITY_ADVERSE_MATCH_CASE = "ACCUITY-ADVERSE";
	public static final String ACCUITY_ADVERSE_ENTITY_MATCH_CASE = "ACCUITY-ADVERSE-ENTITY";

}

