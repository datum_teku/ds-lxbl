package com.trustaml.service.common.database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

public class DBConnection {

	@Resource(mappedName = "java:/TamlMasterDS")
	DataSource dataSource;

	public Connection getConnection() throws SQLException {
		Connection conn = null;
		conn = dataSource.getConnection();
		return conn;
	}

}
