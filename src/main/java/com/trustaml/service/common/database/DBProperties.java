package com.trustaml.service.common.database;

import java.io.IOException;
import java.util.Properties;

public enum DBProperties {
	DBCONNECTION("connection"), DRIVERMANAGER("drivermanager"), USERNAME("dbuser"), PASSWORD("dbpassword");

	private final static Properties dbroperties = new Properties();
	private static String dbPropFilename = "dbconfig.properties";

	private final String key;

	static {
		try {
			dbroperties.load(DBProperties.class.getClassLoader().getResourceAsStream(dbPropFilename));
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public String val() {
		return dbroperties.getProperty(key, "");
	}

	private DBProperties(String key) {
		this.key = key;
	}

}
