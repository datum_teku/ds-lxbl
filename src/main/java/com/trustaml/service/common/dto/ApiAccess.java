package com.trustaml.service.common.dto;

public class ApiAccess {

	private String bfi;
	private String module;
	private String validYear;
	private String apiKey;

	public ApiAccess(String bfi, String module, String validYear, String apiKey) {
		super();
		this.bfi = bfi;
		this.module = module;
		this.validYear = validYear;
		this.apiKey = apiKey;
	}

	public ApiAccess() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getBfi() {
		return bfi;
	}

	public void setBfi(String bfi) {
		this.bfi = bfi;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getValidYear() {
		return validYear;
	}

	public void setValidYear(String validYear) {
		this.validYear = validYear;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

}
