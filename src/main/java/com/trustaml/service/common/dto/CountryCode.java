package com.trustaml.service.common.dto;

public class CountryCode {
	private String enumCode;
	private String enumDescription;

	public CountryCode(String enumCode, String enumDescription) {
		super();
		this.enumCode = enumCode;
		this.enumDescription = enumDescription;
	}

	public String getEnumCode() {
		return enumCode;
	}

	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}

	public String getEnumDescription() {
		return enumDescription;
	}

	public void setEnumDescription(String enumDescription) {
		this.enumDescription = enumDescription;
	}

}
