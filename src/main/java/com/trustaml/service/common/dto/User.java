package com.trustaml.service.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	@JsonProperty("id")
	private int id;

	@JsonProperty("username")
	private String userName;

	@JsonProperty("password")
	private String password;

	@JsonProperty("sol_id")
	private String solId;

	@JsonProperty("authenticated")
	private boolean authenticated;

	@JsonProperty("apikey")
	private String apikey;

	@JsonProperty("designation")
	private String designation;

	public User() {
		super();
		this.userName = "maker1";
		this.password = "";
		this.solId = "";
		this.authenticated = false;
		this.apikey = "";
		this.designation = "";
	}

	public User(int id, String userName, String password, String solId, boolean authenticated, String apiKey,
			String designation) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.solId = solId;
		this.authenticated = authenticated;
		this.apikey = apiKey;
		this.designation = designation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSolId() {
		return solId;
	}

	public void setSolId(String solId) {
		this.solId = solId;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public void userDetails() {
		System.out.println("User ID: " + userName + " Password: " + password + ".");
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

}
