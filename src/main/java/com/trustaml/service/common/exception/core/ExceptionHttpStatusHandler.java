package com.trustaml.service.common.exception.core;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.ejb.EJBException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.model.TrustAmlResponse;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;

@Provider
public class ExceptionHttpStatusHandler implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		String errorLevelCode = ExceptionGenLayer.UNKNOWN.getValue();
		Integer httpStatusCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
		String errorCode = ExceptionCode.UNKNOWN_EXCEPTION.getValue();
		Integer severityLevel = ExceptionSeverityLevel.UNKNOWN.getValue();
		String errorMessage = exception.getMessage();

		// Throwable rootException = ExceptionUtils.getRootCause(exception);
		if (exception instanceof JsonMappingException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERIALIZATION.getValue();
			errorCode = ExceptionCode.JSON_MAPPING_EXCEPTION.getValue();
			severityLevel = ExceptionSeverityLevel.JSON_PARSE.getValue();
			errorMessage = exception.getMessage();

		} else if (exception instanceof IndexOutOfBoundsException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERIALIZATION.getValue();
			errorCode = ExceptionCode.INDEX_BOUND.getValue();
			severityLevel = ExceptionSeverityLevel.CODE_LEVEL_SEVERITY.getValue();
			errorMessage = exception.getMessage();

		} else if (exception instanceof NullPointerException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERIALIZATION.getValue();
			errorCode = ExceptionCode.NULL_POINTER.getValue();
			severityLevel = ExceptionSeverityLevel.CODE_LEVEL_SEVERITY.getValue();
			errorMessage = exception.getMessage();

		} else if (exception instanceof SQLException) {
			if (((SQLException) exception).getSQLState().startsWith("23")) {
				httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
				errorLevelCode = ExceptionGenLayer.DATABASE.getValue();
				severityLevel = ExceptionSeverityLevel.DATABASE_LEVEL.getValue();
				errorCode = ExceptionCode.MYSQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION.getValue();
				errorMessage = exception.getMessage();
			} else if (((SQLException) exception).getSQLState().startsWith("42")) {
				httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
				errorLevelCode = ExceptionGenLayer.DATABASE.getValue();
				severityLevel = ExceptionSeverityLevel.DATABASE_LEVEL.getValue();
				errorCode = ExceptionCode.COLUMN_MISSING.getValue();
				errorMessage = exception.getMessage();
			} else {
				httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
				errorLevelCode = ExceptionGenLayer.DATABASE.getValue();
				errorCode = ExceptionCode.NULL_POINTER.getValue();
				severityLevel = ExceptionSeverityLevel.DATABASE_LEVEL.getValue();
				errorMessage = exception.getMessage();
			}
			// if
			// (SQLException.class.isAssignableFrom(SQLIntegrityConstraintViolationException.class))
			// {
			// errorLevelCode = ExceptionGenLayer.DATABASE.getValue();
			// errorCode =
			// ExceptionCode.MYSQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION.getValue();
			// severityLevel =
			// ExceptionSeverityLevel.INVALID_REQUEST.getValue();
			// } else {

			// }

		} else if (exception instanceof JsonParseException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERIALIZATION.getValue();
			errorCode = ExceptionCode.JSON_MAPPING_EXCEPTION.getValue();
			severityLevel = ExceptionSeverityLevel.JSON_PARSE.getValue();
			errorMessage = exception.getMessage();

		} else if (exception instanceof javax.ws.rs.NotFoundException) {
			httpStatusCode = Response.Status.UNAUTHORIZED.getStatusCode();
			errorLevelCode = ExceptionGenLayer.AUTHORIZATION.getValue();
			severityLevel = ExceptionSeverityLevel.INVALID_REQUEST.getValue();
			errorCode = ExceptionCode.UNKNOWN_EXCEPTION.getValue();
		}

		else if (exception instanceof JsonProcessingException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERIALIZATION.getValue();
			errorCode = ExceptionCode.JSON_PROCESSING_EXCEPTION.getValue();
			severityLevel = ExceptionSeverityLevel.JSON_PARSE.getValue();
			errorMessage = exception.getMessage();

		} else if (exception instanceof IOException) {
			httpStatusCode = Response.Status.BAD_REQUEST.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERVICE.getValue();
			errorCode = ExceptionCode.INPUT_OUTPUT.getValue();
			severityLevel = ExceptionSeverityLevel.UNKNOWN.getValue();

		} else if (exception instanceof TrustAmlEmptyJSONException) {
			httpStatusCode = Response.Status.UNAUTHORIZED.getStatusCode();
			errorLevelCode = ExceptionGenLayer.SERVICE.getValue();
			errorCode = ExceptionCode.NULL_POINTER.getValue();
			severityLevel = ExceptionSeverityLevel.UNKNOWN.getValue();
		} else if (exception instanceof EJBException) {
			httpStatusCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
		} else if (exception instanceof ParseException) {
			// data format exception
		}

		TrustAmlResponse response = new TrustAmlResponse(httpStatusCode, errorLevelCode, errorCode, severityLevel,
				errorMessage);

		return ResponseReturn.response(response);
	}

}
