package com.trustaml.service.common.exception.type;

public class TrustAmlEmptyJSONException extends Exception {
	private static final long serialVersionUID = 7583980612503963914L;

	public TrustAmlEmptyJSONException(String msg) {
		super();
	}

}
