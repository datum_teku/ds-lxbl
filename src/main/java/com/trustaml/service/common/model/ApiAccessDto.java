package com.trustaml.service.common.model;

public class ApiAccessDto {
	private String bfi;
	private String module;
	private String validFrom;
	private String validUpto;
	private String apiKey;

	public ApiAccessDto(String bfi, String module, String validFrom, String validUpto, String apiKey) {
		super();
		this.bfi = bfi;
		this.module = module;
		this.validFrom = validFrom;
		this.validUpto = validUpto;
		this.apiKey = apiKey;
	}

	public ApiAccessDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getBfi() {
		return bfi;
	}

	public void setBfi(String bfi) {
		this.bfi = bfi;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(String validUpto) {
		this.validUpto = validUpto;
	}

}
