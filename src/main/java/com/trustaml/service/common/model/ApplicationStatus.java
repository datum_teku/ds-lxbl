package com.trustaml.service.common.model;

public class ApplicationStatus {
	private long id;
	private int message;

	public ApplicationStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ApplicationStatus(long id, int message) {
		super();
		this.id = id;
		this.message = message;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getMessage() {
		return message;
	}

	public void setMessage(int message) {
		this.message = message;
	}

}
