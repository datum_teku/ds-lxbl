package com.trustaml.service.common.service;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.adversemedia.model.AdverseMediaInfo;
import com.trustaml.service.adversemedia.model.AdverseMediaInfoAttachment;
import com.trustaml.service.adversemedia.model.AdverseMediaPersonalInfo;

public class JsonStringToAdverseMediaDaoImplementation {

	public AdverseMediaPersonalInfo getAdverseMediaPersonalInfoObjects(String jsonStrings)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		AdverseMediaPersonalInfo adverseMediaPersonalInfo = new AdverseMediaPersonalInfo();

		JsonNode jsonNode = mapper.readValue(new File(jsonStrings), JsonNode.class);
		JsonNode adverseMediaPersonalInfoJsonNode = jsonNode.get("adverse-media-personal-info");
		adverseMediaPersonalInfo = mapper.treeToValue(adverseMediaPersonalInfoJsonNode, AdverseMediaPersonalInfo.class);

		return adverseMediaPersonalInfo;
	}

	public AdverseMediaInfo getAdverseMediaInfoObjects(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		AdverseMediaInfo adverseMediaInfo = new AdverseMediaInfo();

		DateFormat dateFormat = new SimpleDateFormat("mm/dd/yyyy");
		mapper.setDateFormat(dateFormat);

		JsonNode jsonNode = mapper.readValue(new File(jsonString), JsonNode.class);
		JsonNode adverseMediaInfoJsonNode = jsonNode.get("adverse-media-info");
		adverseMediaInfo = mapper.treeToValue(adverseMediaInfoJsonNode, AdverseMediaInfo.class);

		return adverseMediaInfo;
	}

	public AdverseMediaInfoAttachment getAdverseMediaInfoAttachmentObject(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		AdverseMediaInfoAttachment adverseMediaInfoAttachment = new AdverseMediaInfoAttachment();

		JsonNode jsonNode = mapper.readValue(new File(jsonString), JsonNode.class);
		JsonNode adverseMediaInfoAttachmentJsonNode = jsonNode.get("adverse-media-info-attachment");
		adverseMediaInfoAttachment = mapper.treeToValue(adverseMediaInfoAttachmentJsonNode,
				AdverseMediaInfoAttachment.class);

		return adverseMediaInfoAttachment;
	}

}
