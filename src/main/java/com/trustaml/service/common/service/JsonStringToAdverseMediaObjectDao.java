package com.trustaml.service.common.service;

import java.io.IOException;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.adversemedia.model.AdverseMediaInfo;
import com.trustaml.service.adversemedia.model.AdverseMediaInfoAttachment;
import com.trustaml.service.adversemedia.model.AdverseMediaPersonalInfo;

public class JsonStringToAdverseMediaObjectDao {

	@Inject
	JsonStringToAdverseMediaDaoImplementation jsonStringToAdverseMediaDAOImplementation;

	public AdverseMediaPersonalInfo getAdverseMediaPersonalInfoObjects(String jsonStrings)
			throws JsonParseException, JsonMappingException, IOException {
		return jsonStringToAdverseMediaDAOImplementation.getAdverseMediaPersonalInfoObjects(jsonStrings);
	}

	public AdverseMediaInfo getAdverseMediaInfoObjects(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		return jsonStringToAdverseMediaDAOImplementation.getAdverseMediaInfoObjects(jsonString);
	}

	public AdverseMediaInfoAttachment getAdverseMediaInfoAttachmentObject(String jsonFilePath)
			throws JsonParseException, JsonMappingException, IOException {
		return jsonStringToAdverseMediaDAOImplementation.getAdverseMediaInfoAttachmentObject(jsonFilePath);
	}
}
