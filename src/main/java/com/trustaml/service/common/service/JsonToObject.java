package com.trustaml.service.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAmlInformationInfo;
import com.trustaml.service.kyc.kycn.model.KycnDocumentStatusInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfo;
import com.trustaml.service.kyc.kycn.model.KycnFinancialInfo;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfo;
import com.trustaml.service.kyc.kycn.model.KycnObservationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedEntityInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedPersonInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfo;

public class JsonToObject {

	public static JsonNode getMainJsonNode(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		// System.out.println("JSON string received: " + jsonString);

		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;

		jsonNode = mapper.readValue(jsonString, JsonNode.class);

		return jsonNode;
	}

	final static public KycnPersonalInfo getKycnPersonalInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
		ObjectMapper mapper = new ObjectMapper();

		JsonNode personalInfoNode = rootNode.get("personal-info");
		kycnPersonalInfo = mapper.treeToValue(personalInfoNode, KycnPersonalInfo.class);

		return kycnPersonalInfo;
	}

	final static public List<KycnIdentificationInfo> getKycnIdentificationInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {
		List<KycnIdentificationInfo> kycnIdentificationInfoList = new ArrayList<KycnIdentificationInfo>();
		ObjectMapper mapper = new ObjectMapper();

		JsonNode identificationInfoNode = rootNode.get("identification-info");

		for (JsonNode node : identificationInfoNode) {
			KycnIdentificationInfo kycnIdentificationInfo = mapper.treeToValue(node, KycnIdentificationInfo.class);
			kycnIdentificationInfoList.add(kycnIdentificationInfo);
		}

		return kycnIdentificationInfoList;
	}

	final static public List<KycnAddressInfo> getKycnAddressInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		List<KycnAddressInfo> kycnAddressInfoList = new ArrayList<KycnAddressInfo>();
		ObjectMapper mapper = new ObjectMapper();

		KycnAddressInfo kycnAddressInfo = new KycnAddressInfo();
		JsonNode addressInfoNodes = rootNode.get("address-info");
		// System.out.println(addressInfoNodes.toString());

		for (JsonNode node : addressInfoNodes) {
			kycnAddressInfo = mapper.treeToValue(node, KycnAddressInfo.class);
			kycnAddressInfoList.add(kycnAddressInfo);
		}

		return kycnAddressInfoList;
	}

	final static public List<KycnRelationInfo> getKycnRelationInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		KycnRelationInfo kycnRelationInfo = new KycnRelationInfo();
		List<KycnRelationInfo> kycnRelationInfoList = new ArrayList<KycnRelationInfo>();
		ObjectMapper mapper = new ObjectMapper();

		JsonNode relationInfoNodes = rootNode.get("relation-info");

		for (JsonNode node : relationInfoNodes) {
			kycnRelationInfo = mapper.treeToValue(node, KycnRelationInfo.class);
			kycnRelationInfoList.add(kycnRelationInfo);
		}

		return kycnRelationInfoList;
	}

	final static public List<KycnEducationInfo> getKycnEducationInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		KycnEducationInfo kycnEducationInfo = new KycnEducationInfo();
		List<KycnEducationInfo> kycnEducationInfoList = new ArrayList<KycnEducationInfo>();
		ObjectMapper mapper = new ObjectMapper();

		JsonNode educationInfoNodes = rootNode.get("education-info");
		// System.out.println(educationInfoNodes.asText());

		for (JsonNode node : educationInfoNodes) {
			kycnEducationInfo = mapper.treeToValue(node, KycnEducationInfo.class);
			kycnEducationInfoList.add(kycnEducationInfo);
		}

		return kycnEducationInfoList;
	}

	final static public List<KycnInvolvementInfo> getKycnInvolvementInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		KycnInvolvementInfo kycnInvolvementInfo = new KycnInvolvementInfo();
		List<KycnInvolvementInfo> kycnInvolvementInfoList = new ArrayList<KycnInvolvementInfo>();
		ObjectMapper mapper = new ObjectMapper();

		JsonNode involvementInfoNodes = rootNode.get("involvement-info");
		// System.out.println(involvementInfoNodes.asText());

		for (JsonNode node : involvementInfoNodes) {
			kycnInvolvementInfo = mapper.treeToValue(node, KycnInvolvementInfo.class);
			kycnInvolvementInfoList.add(kycnInvolvementInfo);
		}

		return kycnInvolvementInfoList;
	}

	final static public List<KycnAccountsInfo> getKycnAccountsInfoObjectsFromJson(JsonNode rootNode)
			throws JsonProcessingException {

		KycnAccountsInfo kycnAccountsInfo = new KycnAccountsInfo();
		List<KycnAccountsInfo> kycnAccountsInfoList = new ArrayList<KycnAccountsInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode accountsInfoNodes = rootNode.get("accounts-info");
		System.out.println(accountsInfoNodes.toString());

		for (JsonNode node : accountsInfoNodes) {
			kycnAccountsInfo = mapper.treeToValue(node, KycnAccountsInfo.class);
			kycnAccountsInfoList.add(kycnAccountsInfo);
		}

		return kycnAccountsInfoList;
	}

	public static User getUserInfoObjectsFromJson(JsonNode kycnRootNode) throws JsonProcessingException {
		// User user = new User();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode personalInfoNode = kycnRootNode.get("user");
		User user = mapper.treeToValue(personalInfoNode, User.class);
		return user;
	}

	public static KycnObservationInfo getKycnObservationInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {
		// User user = new User();

		KycnObservationInfo kycnObservationInfo = new KycnObservationInfo();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode kycnObservationNode = kycnRootNode.get("internal-observation-info");
		kycnObservationInfo = mapper.treeToValue(kycnObservationNode, KycnObservationInfo.class);
		return kycnObservationInfo;

	}

	public static KycnAmlInformationInfo getKycnAmlInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {
		// User user = new User();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode amlInfoNode = kycnRootNode.get("aml-info");
		KycnAmlInformationInfo ObservationInfo = mapper.treeToValue(amlInfoNode, KycnAmlInformationInfo.class);

		return ObservationInfo;

	}

	public static List<KycnDocumentStatusInfo> getKycnDocumentStatusInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {
		// User user = new User();
		ObjectMapper mapper = new ObjectMapper();
		List<KycnDocumentStatusInfo> listKycnKycnDocumentStatus = new ArrayList<KycnDocumentStatusInfo>();

		JsonNode documnetStatusInfoNode = kycnRootNode.get("document-status-info");

		for (JsonNode node : documnetStatusInfoNode) {
			KycnDocumentStatusInfo ObservationInfo = mapper.treeToValue(node, KycnDocumentStatusInfo.class);
			listKycnKycnDocumentStatus.add(ObservationInfo);
		}

		return listKycnKycnDocumentStatus;

	}

	public static KycnFinancialInfo getKycnFinancialInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode financialInfoNode = kycnRootNode.get("financial-info");

		KycnFinancialInfo kycnFinacialInfo = mapper.treeToValue(financialInfoNode, KycnFinancialInfo.class);

		return kycnFinacialInfo;
	}

	public static List<KycnRelatedPersonInfo> getKycnRelatedPersonalInfoInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {
		List<KycnRelatedPersonInfo> listKycnRelatedPersonInfo = new ArrayList<KycnRelatedPersonInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode relatedPersonInfoNode = kycnRootNode.get("related-person-info");
		for (JsonNode node : relatedPersonInfoNode) {
			KycnRelatedPersonInfo kycnRelatedPersonInfo = mapper.treeToValue(node, KycnRelatedPersonInfo.class);
			listKycnRelatedPersonInfo.add(kycnRelatedPersonInfo);
		}
		return listKycnRelatedPersonInfo;
	}

	public static List<KycnRelatedEntityInfo> getKycnRelatedEntityInfoInfoObjectsFromJson(JsonNode kycnRootNode)
			throws JsonProcessingException {
		List<KycnRelatedEntityInfo> listKycnRelatedEntityInfo = new ArrayList<KycnRelatedEntityInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode realtedEntityInfoNode = kycnRootNode.get("related-entity-info");
		for (JsonNode jsonNode : realtedEntityInfoNode) {
			KycnRelatedEntityInfo kycnRelatedEntityInfo = mapper.treeToValue(jsonNode, KycnRelatedEntityInfo.class);
			listKycnRelatedEntityInfo.add(kycnRelatedEntityInfo);
		}
		return listKycnRelatedEntityInfo;
	}
}
