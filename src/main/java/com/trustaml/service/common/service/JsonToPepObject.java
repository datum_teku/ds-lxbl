package com.trustaml.service.common.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.pepentry.model.PepAddressInfo;
import com.trustaml.service.pepentry.model.PepContactInfo;
import com.trustaml.service.pepentry.model.PepElectedRegionInfo;
import com.trustaml.service.pepentry.model.PepFamilyInfo;
import com.trustaml.service.pepentry.model.PepMediaInfo;
import com.trustaml.service.pepentry.model.PepOfficeInfo;
import com.trustaml.service.pepentry.model.PepPersonalInfo;
import com.trustaml.service.pepentry.model.PepTypeInfo;

public class JsonToPepObject {

	final static public PepPersonalInfo getPepPersonalInfoFromJsonString(String jsonString) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		DateFormat df = new SimpleDateFormat("dd-mm-yyyy");
		mapper.setDateFormat(df);
		PepPersonalInfo personalInfo = new PepPersonalInfo();
		JsonNode jsonNode = null;
		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode pepPepPersonalInfo = jsonNode.get("personal-info");

		if (pepPepPersonalInfo != null) {
			personalInfo = mapper.treeToValue(pepPepPersonalInfo, PepPersonalInfo.class);
		} else {
			// System.out.println("personal-info is not found in json string");
		}

		return personalInfo;
	}

	final static public User getUserInfoFromJsonString(String jsonString) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		User user = new User();
		JsonNode jsonNode = null;
		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode pepPepPersonalInfo = jsonNode.get("user");

		if (pepPepPersonalInfo != null) {
			user = mapper.treeToValue(pepPepPersonalInfo, User.class);
		} else {
			System.out.println("personal-info is not found in json string");
		}

		return user;
	}

	final static public List<PepOfficeInfo> getPepOfficeInfoFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		List<PepOfficeInfo> listOfficeInfo = new ArrayList<PepOfficeInfo>();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode officeInfo = jsonNode.get("office-info");

		for (JsonNode node : officeInfo) {
			PepOfficeInfo pepOfficeInfo = mapper.treeToValue(node, PepOfficeInfo.class);
			listOfficeInfo.add(pepOfficeInfo);
		}

		return listOfficeInfo;
	}

	final static public PepElectedRegionInfo getPepElectedRegionInfoFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		PepElectedRegionInfo pepElectedRegionInfo = new PepElectedRegionInfo();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode electedRegionInfo = jsonNode.get("elected-region-info");

		if (electedRegionInfo != null) {
			pepElectedRegionInfo = mapper.treeToValue(electedRegionInfo, PepElectedRegionInfo.class);
		} else {
			System.out.println("elected-region-info is not found in json string");
		}

		return pepElectedRegionInfo;
	}

	final static public PepTypeInfo getPepTypeInfoFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		PepTypeInfo pepTypeInfo = new PepTypeInfo();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode pepType = jsonNode.get("type-info");

		if (pepType != null) {
			pepTypeInfo = mapper.treeToValue(pepType, PepTypeInfo.class);
		} else {
			System.out.println("pep-type-info is not found in json string");
		}

		return pepTypeInfo;
	}

	final static public List<PepFamilyInfo> getPepFamilyInfoFromJsonNode(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		PepFamilyInfo pepFamilyInfo = new PepFamilyInfo();
		List<PepFamilyInfo> familyInfoList = new ArrayList<PepFamilyInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;

		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode familyInfo = jsonNode.get("family-info");

		if (familyInfo != null) {
			for (JsonNode node : familyInfo) {
				pepFamilyInfo = mapper.treeToValue(node, PepFamilyInfo.class);
				familyInfoList.add(pepFamilyInfo);
			}

		} else {
			System.out.println("family-info not found in json string");
			// familyInfoList=null;
		}

		return familyInfoList;
	}

	final static public List<PepAddressInfo> getPepAddressInfoFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		List<PepAddressInfo> listPepAddressInfo = new ArrayList<PepAddressInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;

		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode addressInfo = jsonNode.get("address-info");

		if (addressInfo != null) {

			for (JsonNode node : addressInfo) {
				PepAddressInfo pepAddressInfo = mapper.treeToValue(node, PepAddressInfo.class);
				listPepAddressInfo.add(pepAddressInfo);
			}
		} else {
			System.out.println("address-info is not found in json string");
		}

		return listPepAddressInfo;
	}

	final static public List<PepContactInfo> getPepContactInfoFromJsonNode(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		PepContactInfo pepContactInfo = new PepContactInfo();
		List<PepContactInfo> contactInfoList = new ArrayList<PepContactInfo>();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;

		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode contactInfo = jsonNode.get("contact-info");
		if (contactInfo != null) {

			for (JsonNode node : contactInfo) {

				pepContactInfo = mapper.treeToValue(node, PepContactInfo.class);
				contactInfoList.add(pepContactInfo);

			}

		} else {
			System.out.println("contact-info not found in json string");
			// familyInfoList=null;
		}

		return contactInfoList;
	}

	final static public List<PepMediaInfo> getPepMediaInfoFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		PepMediaInfo pepMediaInfo = new PepMediaInfo();
		List<PepMediaInfo> pepMediaInfoList = new ArrayList<PepMediaInfo>();
		JsonNode jsonNode = null;

		jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode mediaInfo = jsonNode.get("media-info");

		if (mediaInfo != null) {
			for (JsonNode node : mediaInfo) {
				pepMediaInfo = mapper.treeToValue(node, PepMediaInfo.class);
				pepMediaInfoList.add(pepMediaInfo);
			}
		} else {
			System.out.println("media-info is not found in json string");
		}

		return pepMediaInfoList;
	}

}
