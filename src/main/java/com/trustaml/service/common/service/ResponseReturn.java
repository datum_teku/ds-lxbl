package com.trustaml.service.common.service;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.exception.model.TrustAmlResponse;

public class ResponseReturn {

	/**
	 * 
	 * @param str
	 * @return TrustAmlResponse
	 */
	public static Response sucess(String str) {
		TrustAmlResponse response = new TrustAmlResponse(200, "00", "00", 00, str);
		Response myResponse = Response.status(Response.Status.OK).entity(response)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}

	/**
	 * 
	 * @param str
	 * @return TrustAmlResponse
	 */
	public static Response failure(String str) {
		Response myResponse = Response.status(Response.Status.BAD_REQUEST).entity(str)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}

	/**
	 * 
	 * @param object
	 * @return TrustAmlResponse
	 */
	public static Response sucess(Object object) {
		Response myResponse = Response.status(Response.Status.OK).entity(object)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}

	/**
	 * @param listObject
	 * @return
	 */
	public static Response sucess(List<Object> listObject) {
		Response myResponse = Response.status(Response.Status.OK).entity(listObject)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}

	/**
	 * @param response
	 * @return
	 */
	public static Response response(TrustAmlResponse response) {
		Response myResponse = Response.status(response.getHttpStatusCode()).entity(response)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}
}
