package com.trustaml.service.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;

@Stateless
public class DbTestDao {
	@Inject
	DBConnection dbConnection;

	public Connection getEntry() throws SQLException {
		System.out.println("inside get connection");
		return dbConnection.getConnection();
	}

}
