package com.trustaml.service.hotlist.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.hotlist.dao.HotListDao;

@Path("/hot-list")
public class HotListRestFulService {

	@Inject
	HotListDao hotListDao;

	/**
	 * @param jsonString
	 * @return save successful
	 * @description save hot list data to table
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveHotList(String jsonString) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		if (jsonString != null && !jsonString.isEmpty()) {
			hotListDao.saveHotList(jsonString);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param json
	 * @return update successful
	 * @description update hot list data by id
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateHotList(String json) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!json.isEmpty()) {
			hotListDao.updateHotList(json);
			return ResponseReturn.sucess("update successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param json
	 * @return update successful
	 * @description activate or deactivate hot list row
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/deactivate-hot-list-account")
	public Response deaActivateHotList(String json) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException {
		if (!json.isEmpty()) {
			hotListDao.deActivateHotList(json);
			return ResponseReturn.sucess("update successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}
}
