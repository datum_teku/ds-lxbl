package com.trustaml.service.hotlist.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.hotlist.model.Attachment;
import com.trustaml.service.hotlist.model.DeactivateHotListAccount;
import com.trustaml.service.hotlist.model.HotList;
import com.trustaml.service.hotlist.model.HotListAddressInfo;
import com.trustaml.service.hotlist.model.HotListFamilyInfo;
import com.trustaml.service.hotlist.model.HotListOfficeInfo;

@Stateless
public class HotListDao {

	@Inject
	HotListDaoImpl hotListDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	public void saveHotList(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		HotList hotList = new ObjectMapper().readValue(jsonString, HotList.class);
		String hotListPersonalInfoHashValue = hashCodeGenerator
				.generateHashValueForHotListPersonalInfo(hotList.getPersonalInfo(), hotList.getUser(), includeUser);
		Long hotListId = hotListDaoImpl.savePersonalInfo(hotList.getPersonalInfo(), hotListPersonalInfoHashValue);
		hotList.getPersonalInfo().setHotListPersonalInfoId(hotListId);

		String hotListPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
				hotList.getPersonalInfo(), hotList.getUser(), doNotIncludeUser);
		hotListDaoImpl.savePersonalInfoUpdateTable(hotList.getPersonalInfo(), hotList.getUser(),
				hotListPersonalInfoUpdateHashValue);
		for (HotListOfficeInfo hotListOfficeInfo : hotList.getHotListOfficeInfo()) {
			String hotListOfficeHashValue = hashCodeGenerator.generateHashValueForHotListOffice(hotListOfficeInfo,
					hotListId, hotList.getUser(), includeUser);
			String hotListOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForHotListOffice(hotListOfficeInfo,
					hotListId, hotList.getUser(), doNotIncludeUser);
			hotListDaoImpl.insertHotListOfficeInfo(hotListOfficeInfo, hotListId, hotListOfficeHashValue);
			hotListDaoImpl.insertHotListOfficeInfoUpdateTable(hotListOfficeInfo, hotListId, hotList.getUser(),
					hotListOfficeUpdateHashValue);

		}
		if (hotList.getHotListFamilyInfos().size() > 0) {
			for (HotListFamilyInfo familyInfo : hotList.getHotListFamilyInfos()) {
				String hotListFamilyHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						hotListId, hotList.getUser(), includeUser);
				String hotListFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						hotListId, hotList.getUser(), doNotIncludeUser);
				hotListDaoImpl.insertHotListFamilyInfo(familyInfo, hotListId, hotListFamilyHashValue);
				hotListDaoImpl.insertHotListFamilyInfoUpdateTable(familyInfo, hotListId, hotList.getUser(),
						hotListFamilyUpdateHashValue);
			}
		}
		if (hotList.getHotListAddressInfo().size() > 0) {
			for (HotListAddressInfo hotListAddressInfo : hotList.getHotListAddressInfo()) {
				String hotListAddressHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						hotListAddressInfo, hotListId, hotList.getUser(), includeUser);
				String hotListAddressUpdateHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						hotListAddressInfo, hotListId, hotList.getUser(), doNotIncludeUser);
				hotListDaoImpl.insertHotListAddressInfo(hotListAddressInfo, hotListId, hotListAddressHashValue);
				hotListDaoImpl.insertHotListAddressInfoUpdateTable(hotListAddressInfo, hotListId, hotList.getUser(),
						hotListAddressUpdateHashValue);
			}
		}
		if (hotList.getAttachment().size() > 0) {
			for (Attachment hotListMediaInfo : hotList.getAttachment()) {
				String hotListMediaHashValue = hashCodeGenerator.generateHashValueForHotListMedia(hotListMediaInfo,
						hotListId, hotList.getUser(), includeUser);
				String hotListMediaUpdateHashValue = hashCodeGenerator.generateHashValueForHotListMedia(
						hotListMediaInfo, hotListId, hotList.getUser(), doNotIncludeUser);
				hotListDaoImpl.insertHotListMediaInfo(hotListMediaInfo, hotListId, hotListMediaHashValue);
				hotListDaoImpl.insertHotListMediaInfoUpdateTable(hotListMediaInfo, hotListId, hotList.getUser(),
						hotListMediaUpdateHashValue);
			}
		}
	}

	/**
	 * @param jsonString
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	public void updateHotList(String jsonString)
			throws SQLException, JsonParseException, JsonMappingException, IOException, ParseException, NoSuchAlgorithmException {

		boolean includeUser = true;
		boolean doNotIncludeUser = false;

		HotList hotList = new ObjectMapper().readValue(jsonString, HotList.class);
		if (hotList.getPersonalInfo().isChange()) {
			String hotListPersonalInfoHashValue = hashCodeGenerator
					.generateHashValueForHotListPersonalInfo(hotList.getPersonalInfo(), hotList.getUser(), includeUser);
			String hotListPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
					hotList.getPersonalInfo(), hotList.getUser(), doNotIncludeUser);
			hotListDaoImpl.updatePersonalInfo(hotList.getPersonalInfo(), hotListPersonalInfoHashValue);
			hotList.getPersonalInfo().setHotListPersonalInfoId(hotList.getPersonalInfo().getId());
			hotListDaoImpl.savePersonalInfoUpdateTable(hotList.getPersonalInfo(), hotList.getUser(),
					hotListPersonalInfoUpdateHashValue);
		}
		long hotListId = hotList.getPersonalInfo().getId();
		for (HotListOfficeInfo hotListOfficeInfo : hotList.getHotListOfficeInfo()) {
			String hotListOfficeHashValue = hashCodeGenerator.generateHashValueForHotListOffice(hotListOfficeInfo,
					hotListId, hotList.getUser(), includeUser);
			String hotListOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForHotListOffice(hotListOfficeInfo,
					hotListId, hotList.getUser(), doNotIncludeUser);
			if (hotListOfficeInfo.getId() != 0) {
				if (hotListOfficeInfo.isChange()) {
					hotListDaoImpl.updateHotListOfficeInfo(hotListOfficeInfo, hotListId, hotListOfficeHashValue);
					hotListDaoImpl.insertHotListOfficeInfoUpdateTable(hotListOfficeInfo, hotListId, hotList.getUser(),
							hotListOfficeUpdateHashValue);
				}

			} else {
				hotListDaoImpl.insertHotListOfficeInfo(hotListOfficeInfo, hotListId, hotListOfficeHashValue);
				hotListDaoImpl.insertHotListOfficeInfoUpdateTable(hotListOfficeInfo, hotListId, hotList.getUser(),
						hotListOfficeUpdateHashValue);
			}

		}
		if (hotList.getHotListFamilyInfos().size() > 0) {
			for (HotListFamilyInfo familyInfo : hotList.getHotListFamilyInfos()) {
				String hotListFamilyHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						hotListId, hotList.getUser(), includeUser);
				String hotListFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						hotListId, hotList.getUser(), doNotIncludeUser);
				if (familyInfo.getId() != 0) {
					if (familyInfo.isChange()) {
						hotListDaoImpl.updateHotListFamilyInfo(familyInfo, hotListId, hotListFamilyHashValue);
						hotListDaoImpl.insertHotListFamilyInfoUpdateTable(familyInfo, hotListId, hotList.getUser(),
								hotListFamilyUpdateHashValue);
					}
				} else {
					hotListDaoImpl.insertHotListFamilyInfo(familyInfo, hotListId, hotListFamilyHashValue);
					hotListDaoImpl.insertHotListFamilyInfoUpdateTable(familyInfo, hotListId, hotList.getUser(),
							hotListFamilyUpdateHashValue);
				}
			}
		}

		if (hotList.getHotListAddressInfo().size() > 0) {
			for (HotListAddressInfo hotListAddressInfo : hotList.getHotListAddressInfo()) {
				String hotListAddressHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						hotListAddressInfo, hotListId, hotList.getUser(), includeUser);
				String hotListAddressUpdateHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						hotListAddressInfo, hotListId, hotList.getUser(), doNotIncludeUser);
				if (hotListAddressInfo.getId() != 0) {
					if (hotListAddressInfo.isChange()) {
						hotListDaoImpl.updateHotListAddressInfo(hotListAddressInfo, hotListId, hotListAddressHashValue);
						hotListDaoImpl.insertHotListAddressInfoUpdateTable(hotListAddressInfo, hotListId,
								hotList.getUser(), hotListAddressUpdateHashValue);
					}
				} else {
					hotListDaoImpl.insertHotListAddressInfo(hotListAddressInfo, hotListId, hotListAddressHashValue);
					hotListDaoImpl.insertHotListAddressInfoUpdateTable(hotListAddressInfo, hotListId, hotList.getUser(),
							hotListAddressUpdateHashValue);
				}
			}
		}
		if (hotList.getAttachment().size() > 0) {
			for (Attachment hotListMediaInfo : hotList.getAttachment()) {
				String hotListMediaHashValue = hashCodeGenerator.generateHashValueForHotListMedia(hotListMediaInfo,
						hotListId, hotList.getUser(), includeUser);
				String hotListMediaUpdateHashValue = hashCodeGenerator.generateHashValueForHotListMedia(
						hotListMediaInfo, hotListId, hotList.getUser(), doNotIncludeUser);
				if (hotListMediaInfo.getId() != 0) {
					if (hotListMediaInfo.isChange()) {
						hotListDaoImpl.updateHotListMediaInfo(hotListMediaInfo, hotListId, hotListMediaHashValue);
						hotListDaoImpl.insertHotListMediaInfoUpdateTable(hotListMediaInfo, hotListId, hotList.getUser(),
								hotListMediaUpdateHashValue);
					}
				} else
					hotListDaoImpl.insertHotListMediaInfo(hotListMediaInfo, hotListId, hotListMediaHashValue);
				hotListDaoImpl.insertHotListMediaInfoUpdateTable(hotListMediaInfo, hotListId, hotList.getUser(),
						hotListMediaUpdateHashValue);
			}
		}
	}

	public void deActivateHotList(String json)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		DeactivateHotListAccount deactivateHotListAccount = new ObjectMapper().readValue(json,
				DeactivateHotListAccount.class);
		hotListDaoImpl.deActivateHotList(deactivateHotListAccount);
		hotListDaoImpl.insertIntoHotListPersonalInfoUpdate(deactivateHotListAccount);

	}
}
