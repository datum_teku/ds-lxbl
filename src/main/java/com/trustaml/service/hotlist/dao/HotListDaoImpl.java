package com.trustaml.service.hotlist.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.hotlist.model.Attachment;
import com.trustaml.service.hotlist.model.DeactivateHotListAccount;
import com.trustaml.service.hotlist.model.HotListAddressInfo;
import com.trustaml.service.hotlist.model.HotListFamilyInfo;
import com.trustaml.service.hotlist.model.HotListOfficeInfo;
import com.trustaml.service.hotlist.model.PersonalInfo;
import com.trustaml.service.common.dto.User;

public class HotListDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @param hotListOfficeInfo
	 * @param hotListId
	 * @param hotListOfficeHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertHotListOfficeInfo(HotListOfficeInfo hotListOfficeInfo, Long hotListId,
			String hotListOfficeHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_hot_list_office_info (hot_list_personal_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,document_code,hash) VALUES(?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListOfficeInfo.getDesignation());
			ps.setString(3, hotListOfficeInfo.getOfficeName());
			ps.setLong(4, hotListOfficeInfo.getProlongation());
			ps.setString(5, hotListOfficeInfo.getNotes());
			ps.setLong(6, hotListOfficeInfo.getTermYears());
			if (!hotListOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(hotListOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, hotListOfficeInfo.getPepOfficeStatus());
			ps.setString(9, hotListOfficeInfo.getHotlistCategory());
			ps.setString(10, hotListOfficeHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param personalInfo
	 * @param hotListPersonalInfoHashValue
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public Long savePersonalInfo(PersonalInfo personalInfo, String hotListPersonalInfoHashValue)
			throws SQLException, ParseException {
		long personalInfoId = 0;
		String sql = "INSERT INTO list_hot_list_personal_info (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setString(25, hotListPersonalInfoHashValue);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				personalInfoId = rs.getLong(1);
			}
		} finally

		{
			ps.close();
			connection.close();
		}

		return personalInfoId;
	}

	/**
	 * @param personalInfo
	 * @param user
	 * @param hotListPersonalInfoUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void savePersonalInfoUpdateTable(PersonalInfo personalInfo, User user,
			String hotListPersonalInfoUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_hot_list_personal_info_update (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,maker,checker,reason,hot_list_personal_info_id,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, user.getUserName());
			ps.setString(20, user.getUserName());
			ps.setString(21, "No Reason");
			ps.setLong(22, personalInfo.getHotListId());
			ps.setString(23, personalInfo.getReferenceNo());
			ps.setString(24, personalInfo.getApprovedBy());
			ps.setString(25, personalInfo.getCorrespondent());
			ps.setString(26, personalInfo.getPanNumber());
			ps.setString(27, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(28, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(28, java.sql.Types.DATE);
			}
			ps.setString(29, hotListPersonalInfoUpdateHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListOfficeInfo
	 * @param hotListId
	 * @param user
	 * @param hotListOfficeUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertHotListOfficeInfoUpdateTable(HotListOfficeInfo hotListOfficeInfo, Long hotListId, User user,
			String hotListOfficeUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_hot_list_office_info_update"
				+ "(hot_list_personal_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,maker,checker,reason,document_code,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListOfficeInfo.getDesignation());
			ps.setString(3, hotListOfficeInfo.getOfficeName());
			ps.setLong(4, hotListOfficeInfo.getProlongation());
			ps.setString(5, hotListOfficeInfo.getNotes());
			ps.setLong(6, hotListOfficeInfo.getTermYears());
			if (!hotListOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(hotListOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, hotListOfficeInfo.getPepOfficeStatus());
			ps.setString(9, user.getUserName());
			ps.setString(10, user.getUserName());
			ps.setString(11, "no reason");
			ps.setString(12, hotListOfficeInfo.getHotlistCategory());
			ps.setString(13, hotListOfficeUpdateHashValue);
			ps.execute();

		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param familyInfo
	 * @param hotListId
	 * @param hotListFamilyHashValue
	 * @throws SQLException
	 */
	public void insertHotListFamilyInfo(HotListFamilyInfo familyInfo, Long hotListId, String hotListFamilyHashValue)
			throws SQLException {
		String sql = "INSERT INTO list_hot_list_family_info (hot_list_personal_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, familyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(13, hotListFamilyHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param familyInfo
	 * @param hotListId
	 * @param user
	 * @param hotListFamilyUpdateHashValue
	 * @throws SQLException
	 */
	public void insertHotListFamilyInfoUpdateTable(HotListFamilyInfo familyInfo, Long hotListId, User user,
			String hotListFamilyUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_hot_list_family_info_update"
				+ "(hot_list_personal_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,maker,checker,reason,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, familyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(16, hotListFamilyUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListAddressInfo
	 * @param hotListId
	 * @param hotListAddressHashValue
	 * @throws SQLException
	 */
	public void insertHotListAddressInfo(HotListAddressInfo hotListAddressInfo, Long hotListId,
			String hotListAddressHashValue) throws SQLException {
		String sql = "INSERT INTO list_hot_list_address_info (hot_list_personal_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListAddressInfo.getCountry());
			ps.setString(3, hotListAddressInfo.getState());
			ps.setString(4, hotListAddressInfo.getProvince());
			ps.setString(5, hotListAddressInfo.getDistrict());
			ps.setString(6, hotListAddressInfo.getMnVdc());
			ps.setString(7, hotListAddressInfo.getTownCityVillage());
			ps.setString(8, hotListAddressInfo.getTole());
			ps.setString(9, hotListAddressInfo.getStreet());
			ps.setString(10, hotListAddressInfo.getHouseNumber());
			ps.setString(11, hotListAddressInfo.getNotes());
			ps.setString(12, hotListAddressInfo.getCommunicationType());
			ps.setString(13, hotListAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, hotListAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, hotListAddressInfo.getPhoneNo());
			ps.setString(16, hotListAddressInfo.getTelexNoCountryCode());
			ps.setString(17, hotListAddressInfo.getTelexNoAreaCode());
			ps.setString(18, hotListAddressInfo.getTelexNo());
			ps.setString(19, hotListAddressInfo.getPagerNoCountryCode());
			ps.setString(20, hotListAddressInfo.getPagerNoAreaCode());
			ps.setString(21, hotListAddressInfo.getPagerNo());
			ps.setString(22, hotListAddressHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListAddressInfo
	 * @param hotListId
	 * @param user
	 * @param hotListAddressUpdateHashValue
	 * @throws SQLException
	 */
	public void insertHotListAddressInfoUpdateTable(HotListAddressInfo hotListAddressInfo, Long hotListId, User user,
			String hotListAddressUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_hot_list_address_info_update"
				+ "(hot_list_personal_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,maker,checker,reason,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListAddressInfo.getCountry());
			ps.setString(3, hotListAddressInfo.getState());
			ps.setString(4, hotListAddressInfo.getProvince());
			ps.setString(5, hotListAddressInfo.getDistrict());
			ps.setString(6, hotListAddressInfo.getMnVdc());
			ps.setString(7, hotListAddressInfo.getTownCityVillage());
			ps.setString(8, hotListAddressInfo.getTole());
			ps.setString(9, hotListAddressInfo.getStreet());
			ps.setString(10, hotListAddressInfo.getHouseNumber());
			ps.setString(11, hotListAddressInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, hotListAddressInfo.getCommunicationType());
			ps.setString(16, hotListAddressInfo.getPhoneNoCountryCode());
			ps.setString(17, hotListAddressInfo.getPhoneNoAreaCode());
			ps.setString(18, hotListAddressInfo.getPhoneNo());
			ps.setString(19, hotListAddressInfo.getTelexNoCountryCode());
			ps.setString(20, hotListAddressInfo.getTelexNoAreaCode());
			ps.setString(21, hotListAddressInfo.getTelexNo());
			ps.setString(22, hotListAddressInfo.getPagerNoCountryCode());
			ps.setString(23, hotListAddressInfo.getPagerNoAreaCode());
			ps.setString(24, hotListAddressInfo.getPagerNo());
			ps.setString(25, hotListAddressUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListMediaInfo
	 * @param hotListId
	 * @param hotListMediaHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertHotListMediaInfo(Attachment hotListMediaInfo, Long hotListId, String hotListMediaHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_hot_list_media_info (hot_list_personal_info_id,notes,published_date,extension_text,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListMediaInfo.getNotes());
			if (!hotListMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(hotListMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, hotListMediaInfo.getExtensionText());
			ps.setString(5, hotListMediaInfo.getMediaType());
			ps.setBytes(6, hotListMediaInfo.getMediaContent().getBytes());
			ps.setString(7, hotListMediaInfo.getSourceOfInformation());
			ps.setString(8, hotListMediaInfo.getSourceType());
			ps.setString(9, hotListMediaHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListMediaInfo
	 * @param hotListId
	 * @param user
	 * @param hotListMediaUpdateHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void insertHotListMediaInfoUpdateTable(Attachment hotListMediaInfo, Long hotListId, User user,
			String hotListMediaUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_hot_list_media_info_update (hot_list_personal_info_id,notes,published_date,extension_text,maker,checker,reason,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListMediaInfo.getNotes());
			if (!hotListMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(hotListMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, hotListMediaInfo.getExtensionText());
			ps.setString(5, user.getUserName());
			ps.setString(6, user.getUserName());
			ps.setString(7, "no reason");
			ps.setString(8, hotListMediaInfo.getMediaType());
			ps.setBytes(9, hotListMediaInfo.getMediaContent().getBytes());
			ps.setString(10, hotListMediaInfo.getSourceOfInformation());
			ps.setString(11, hotListMediaInfo.getSourceType());
			ps.setString(12, hotListMediaUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * @param personalInfo
	 * @param hotListPersonalInfoHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updatePersonalInfo(PersonalInfo personalInfo, String hotListPersonalInfoHashValue)
			throws SQLException, ParseException {

		String sql = "UPDATE list_hot_list_personal_info SET jurisdiction=?,title=?,"
				+ "first_name=?,middle_name=?,last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,"
				+ "second_name=?,called_by_name=?,previous_name=?,gender=?,date_of_birth=?,"
				+ "place_of_birth=?,country_of_birth=?,nationality=?,notes=?,document_code=?,reference_no=?,approved_by=?,correspondent=?,pan_number=?,citizen_number=?,approved_date=?,hash=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setString(25, hotListPersonalInfoHashValue);
			ps.setLong(26, personalInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListOfficeInfo
	 * @param hotListId
	 * @param hotListOfficeHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateHotListOfficeInfo(HotListOfficeInfo hotListOfficeInfo, long hotListId,
			String hotListOfficeHashValue) throws SQLException, ParseException {
		String sql = "UPDATE list_hot_list_office_info SET hot_list_personal_info_id=?,designation=?,office_name=?,prolongation=?,"
				+ "notes=?,term_year=?,date_of_appointment=?,office_status=?,document_code=?,hash=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListOfficeInfo.getDesignation());
			ps.setString(3, hotListOfficeInfo.getOfficeName());
			ps.setLong(4, hotListOfficeInfo.getProlongation());
			ps.setString(5, hotListOfficeInfo.getNotes());
			ps.setLong(6, hotListOfficeInfo.getTermYears());
			if (!hotListOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(hotListOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, hotListOfficeInfo.getPepOfficeStatus());
			ps.setString(9, hotListOfficeInfo.getHotlistCategory());
			ps.setString(10, hotListOfficeHashValue);
			ps.setLong(11, hotListOfficeInfo.getId());
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param familyInfo
	 * @param hotListId
	 * @param hotListFamilyHashValue
	 * @throws SQLException
	 */
	public void updateHotListFamilyInfo(HotListFamilyInfo familyInfo, long hotListId, String hotListFamilyHashValue)
			throws SQLException {
		String sql = "UPDATE list_hot_list_family_info SET hot_list_personal_info_id=?,relationship_to=?,first_name=?,middle_name=?,"
				+ "last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,notes=?,primary_identification_document_number=?,hash=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, familyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(13, hotListFamilyHashValue);
			ps.setLong(13, familyInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListAddressInfo
	 * @param hotListId
	 * @param hotListAddressHashValue
	 * @throws SQLException
	 */
	public void updateHotListAddressInfo(HotListAddressInfo hotListAddressInfo, long hotListId,
			String hotListAddressHashValue) throws SQLException {
		String sql = "UPDATE list_hot_list_address_info SET hot_list_personal_info_id=?,country=?,state=?,province=?,district=?,mn_vdc=?,"
				+ "town_city_village=?,tole=?,street=?,house_number=?,notes=?,communication_type=?,phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,pager_no=?,hash=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListAddressInfo.getCountry());
			ps.setString(3, hotListAddressInfo.getState());
			ps.setString(4, hotListAddressInfo.getProvince());
			ps.setString(5, hotListAddressInfo.getDistrict());
			ps.setString(6, hotListAddressInfo.getMnVdc());
			ps.setString(7, hotListAddressInfo.getTownCityVillage());
			ps.setString(8, hotListAddressInfo.getTole());
			ps.setString(9, hotListAddressInfo.getStreet());
			ps.setString(10, hotListAddressInfo.getHouseNumber());
			ps.setString(11, hotListAddressInfo.getNotes());
			ps.setString(12, hotListAddressInfo.getCommunicationType());
			ps.setString(13, hotListAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, hotListAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, hotListAddressInfo.getPhoneNo());
			ps.setString(16, hotListAddressInfo.getTelexNoCountryCode());
			ps.setString(17, hotListAddressInfo.getTelexNoAreaCode());
			ps.setString(18, hotListAddressInfo.getTelexNo());
			ps.setString(19, hotListAddressInfo.getPagerNoCountryCode());
			ps.setString(20, hotListAddressInfo.getPagerNoAreaCode());
			ps.setString(21, hotListAddressInfo.getPagerNo());
			ps.setString(22, hotListAddressHashValue);
			ps.setLong(23, hotListAddressInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param hotListMediaInfo
	 * @param hotListId
	 * @param hotListMediaHashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateHotListMediaInfo(Attachment hotListMediaInfo, long hotListId, String hotListMediaHashValue)
			throws SQLException, ParseException {
		String sql = "UPDATE list_hot_list_media_info SET hot_list_personal_info_id=?,notes=?,published_date=?,extension_text=?,media_type=?,media_content=?,source_of_information=?,source_type=?,hash=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, hotListId);
			ps.setString(2, hotListMediaInfo.getNotes());
			if (!hotListMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(hotListMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, hotListMediaInfo.getExtensionText());
			ps.setString(5, hotListMediaInfo.getMediaType());
			ps.setBytes(6, hotListMediaInfo.getMediaContent().getBytes());
			ps.setString(7, hotListMediaInfo.getSourceOfInformation());
			ps.setString(8, hotListMediaInfo.getSourceType());
			ps.setString(9, hotListMediaHashValue);
			ps.setLong(10, hotListMediaInfo.getId());
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void deActivateHotList(DeactivateHotListAccount deactivateHotListAccount) throws SQLException {
		String sql = "UPDATE list_hot_list_personal_info SET active=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, deactivateHotListAccount.isStatus());
			ps.setLong(2, deactivateHotListAccount.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertIntoHotListPersonalInfoUpdate(DeactivateHotListAccount deactivateHotListAccount)
			throws SQLException {
		String sql = "INSERT INTO list_hot_list_personal_info_update (hot_list_personal_info_id,active,checker,maker)"
				+ "VALUES(?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, deactivateHotListAccount.getId());
			ps.setBoolean(2, deactivateHotListAccount.isStatus());
			ps.setString(3, deactivateHotListAccount.getUser().getUserName());
			ps.setString(4, deactivateHotListAccount.getUser().getUserName());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
}
