package com.trustaml.service.hotlist.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

	private long id;

	@JsonProperty("list_hot_list_personal_info_id")
	private long listHotListPersonalInfoId;

	@JsonProperty("country")
	private String country;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("state")
	private String state;

	@JsonProperty("province")
	private String province;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("town_city_village")
	private String townCityVillage;

	@JsonProperty("tole")
	private String tole;

	@JsonProperty("street")
	private String street;

	@JsonProperty("house_number")
	private String houseNumber;

	@JsonProperty("phone_number")
	private String phoneNumber;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("maker")
	private String maker;

	@JsonProperty("checker")
	private String checker;

	@JsonProperty("approved")
	private Date approved;

	@JsonProperty("update_date")
	private Date updateDate;

	@JsonProperty("approved_date")
	private Date approvedDate;

	@JsonProperty("reason")
	private String reason;

	public Address(long id, long listHotListPersonalInfoId, String country, String zone, String state, String province,
			String district, String mnVdc, String townCityVillage, String tole, String street, String houseNumber,
			String phoneNumber, String notes, String maker, String checker, Date approved, Date updateDate,
			Date approvedDate, String reason) {
		super();
		this.id = id;
		this.listHotListPersonalInfoId = listHotListPersonalInfoId;
		this.country = country;
		this.zone = zone;
		this.state = state;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.townCityVillage = townCityVillage;
		this.tole = tole;
		this.street = street;
		this.houseNumber = houseNumber;
		this.phoneNumber = phoneNumber;
		this.notes = notes;
		this.maker = maker;
		this.checker = checker;
		this.approved = approved;
		this.updateDate = updateDate;
		this.approvedDate = approvedDate;
		this.reason = reason;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getListHotListPersonalInfoId() {
		return listHotListPersonalInfoId;
	}

	public void setListHotListPersonalInfoId(long listHotListPersonalInfoId) {
		this.listHotListPersonalInfoId = listHotListPersonalInfoId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getTownCityVillage() {
		return townCityVillage;
	}

	public void setTownCityVillage(String townCityVillage) {
		this.townCityVillage = townCityVillage;
	}

	public String getTole() {
		return tole;
	}

	public void setTole(String tole) {
		this.tole = tole;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public Date getApproved() {
		return approved;
	}

	public void setApproved(Date approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
