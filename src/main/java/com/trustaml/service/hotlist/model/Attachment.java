package com.trustaml.service.hotlist.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Attachment {

	private long id;

	@JsonProperty("source_type")
	private String sourceType;

	@JsonProperty("extension_text")
	private String extensionText;

	@JsonProperty("media_content")
	private String mediaContent;

	@JsonProperty("source_name")
	private String sourceOfInformation;

	@JsonProperty("published_date")
	private String publishedDate;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("media_type")
	private String mediaType;

	@JsonProperty("change")
	private boolean change;

	public Attachment(long id, String sourceType, String extensionText, String mediaContent, String sourceOfInformation,
			String publishedDate, String notes, String mediaType, boolean change) {
		super();
		this.id = id;
		this.sourceType = sourceType;
		this.extensionText = extensionText;
		this.mediaContent = mediaContent;
		this.sourceOfInformation = sourceOfInformation;
		this.publishedDate = publishedDate;
		this.notes = notes;
		this.mediaType = mediaType;
		this.change = change;
	}

	public Attachment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getExtensionText() {
		return extensionText;
	}

	public void setExtensionText(String extensionText) {
		this.extensionText = extensionText;
	}

	public String getMediaContent() {
		return mediaContent;
	}

	public void setMediaContent(String mediaContent) {
		this.mediaContent = mediaContent;
	}

	public String getSourceOfInformation() {
		return sourceOfInformation;
	}

	public void setSourceOfInformation(String sourceOfInformation) {
		this.sourceOfInformation = sourceOfInformation;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
