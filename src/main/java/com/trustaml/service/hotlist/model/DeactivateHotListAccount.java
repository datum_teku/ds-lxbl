package com.trustaml.service.hotlist.model;

import com.trustaml.service.common.dto.User;

public class DeactivateHotListAccount {

	private Long id;
	private User user;
	private boolean status;

	public DeactivateHotListAccount(Long id, User user, boolean status) {
		super();
		this.id = id;
		this.user = user;
		this.status = status;
	}

	public DeactivateHotListAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
