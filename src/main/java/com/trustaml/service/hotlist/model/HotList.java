package com.trustaml.service.hotlist.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.investigation.model.InvestigationMediaInfo;

public class HotList {

	@JsonProperty("personal-info")
	private PersonalInfo personalInfo;

	@JsonProperty("media-info")
	private List<Attachment> attachment;

	@JsonProperty("address-info")
	private List<HotListAddressInfo> hotListAddressInfo;

	@JsonProperty("office-info")
	private List<HotListOfficeInfo> hotListOfficeInfo;

	@JsonProperty("family-info")
	private List<HotListFamilyInfo> hotListFamilyInfos;

	@JsonProperty("investigation-media-info")
	private List<InvestigationMediaInfo> investigationMediaInfo;

	@JsonProperty("user")
	private User user;

	public HotList(PersonalInfo personalInfo, List<Attachment> attachment, List<HotListAddressInfo> hotListAddressInfo,
			List<HotListOfficeInfo> hotListOfficeInfo, List<HotListFamilyInfo> hotListFamilyInfos,
			List<InvestigationMediaInfo> investigationMediaInfo, User user) {
		super();
		this.personalInfo = personalInfo;
		this.attachment = attachment;
		this.hotListAddressInfo = hotListAddressInfo;
		this.hotListOfficeInfo = hotListOfficeInfo;
		this.hotListFamilyInfos = hotListFamilyInfos;
		this.investigationMediaInfo = investigationMediaInfo;
		this.user = user;
	}

	public HotList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PersonalInfo getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

	public List<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	public List<HotListAddressInfo> getHotListAddressInfo() {
		return hotListAddressInfo;
	}

	public void setHotListAddressInfo(List<HotListAddressInfo> hotListAddressInfo) {
		this.hotListAddressInfo = hotListAddressInfo;
	}

	public List<HotListOfficeInfo> getHotListOfficeInfo() {
		return hotListOfficeInfo;
	}

	public void setHotListOfficeInfo(List<HotListOfficeInfo> hotListOfficeInfo) {
		this.hotListOfficeInfo = hotListOfficeInfo;
	}

	public List<HotListFamilyInfo> getHotListFamilyInfos() {
		return hotListFamilyInfos;
	}

	public void setHotListFamilyInfos(List<HotListFamilyInfo> hotListFamilyInfos) {
		this.hotListFamilyInfos = hotListFamilyInfos;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<InvestigationMediaInfo> getInvestigationMediaInfo() {
		return investigationMediaInfo;
	}

	public void setInvestigationMediaInfo(List<InvestigationMediaInfo> investigationMediaInfo) {
		this.investigationMediaInfo = investigationMediaInfo;
	}

}
