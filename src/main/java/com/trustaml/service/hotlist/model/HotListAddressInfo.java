package com.trustaml.service.hotlist.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HotListAddressInfo {

	private long id;

	@JsonProperty("country")
	private String country;

	@JsonProperty("state")
	private String state;

	@JsonProperty("province")
	private String province;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("town_city")
	private String townCityVillage;

	@JsonProperty("tole")
	private String tole;

	@JsonProperty("street")
	private String street;

	@JsonProperty("house_number")
	private String houseNumber;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("pager_no")
	private String pagerNo;

	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;

	@JsonProperty("telex_no_country_code")
	private String telexNoCountryCode;

	@JsonProperty("pager_no_area_code")
	private String pagerNoAreaCode;

	@JsonProperty("telex_no_area_code")
	private String telexNoAreaCode;

	@JsonProperty("pager_no_country_code")
	private String pagerNoCountryCode;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("telex_no")
	private String telexNo;

	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;

	@JsonProperty("contact_type")
	private String communicationType;

	@JsonProperty("change")
	private boolean change;

	public HotListAddressInfo(long id, String country, String state, String province, String district, String mnVdc,
			String townCityVillage, String tole, String street, String houseNumber, String notes, String pagerNo,
			String phoneNoCountryCode, String telexNoCountryCode, String pagerNoAreaCode, String telexNoAreaCode,
			String pagerNoCountryCode, String phoneNo, String telexNo, String phoneNoAreaCode, String communicationType,
			boolean change) {
		super();
		this.id = id;
		this.country = country;
		this.state = state;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.townCityVillage = townCityVillage;
		this.tole = tole;
		this.street = street;
		this.houseNumber = houseNumber;
		this.notes = notes;
		this.pagerNo = pagerNo;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.telexNoCountryCode = telexNoCountryCode;
		this.pagerNoAreaCode = pagerNoAreaCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.pagerNoCountryCode = pagerNoCountryCode;
		this.phoneNo = phoneNo;
		this.telexNo = telexNo;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.communicationType = communicationType;
		this.change = change;
	}

	public HotListAddressInfo() {
		super();
		this.country = "";
		this.state = "";
		this.province = "";
		this.district = "";
		this.mnVdc = "";
		this.townCityVillage = "";
		this.tole = "";
		this.street = "";
		this.houseNumber = "";
		this.notes = "";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getTownCityVillage() {
		return townCityVillage;
	}

	public void setTownCityVillage(String townCityVillage) {
		this.townCityVillage = townCityVillage;
	}

	public String getTole() {
		return tole;
	}

	public void setTole(String tole) {
		this.tole = tole;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPagerNo() {
		return pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getPagerNoAreaCode() {
		return pagerNoAreaCode;
	}

	public void setPagerNoAreaCode(String pagerNoAreaCode) {
		this.pagerNoAreaCode = pagerNoAreaCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getPagerNoCountryCode() {
		return pagerNoCountryCode;
	}

	public void setPagerNoCountryCode(String pagerNoCountryCode) {
		this.pagerNoCountryCode = pagerNoCountryCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
