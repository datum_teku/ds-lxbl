package com.trustaml.service.hotlist.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HotListOfficeInfo {

	private long id;

	@JsonProperty("designation")
	private String designation;

	@JsonProperty("office_name")
	private String officeName;

	@JsonProperty("term_years")
	private long termYears;

	@JsonProperty("date_of_appointment")
	private String dateOfAppointment;

	@JsonProperty("prolongation")
	private long prolongation;

	@JsonProperty("pep_office_status")
	private String pepOfficeStatus;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("elected_district")
	private String electedDistrict;

	@JsonProperty("area_number")
	private int areaNumber;

	@JsonProperty("party_name")
	private String partyName;

	@JsonProperty("hotlist_category")
	private String hotlistCategory;

	@JsonProperty("change")
	private boolean change;

	public HotListOfficeInfo(String designation, String officeName, long termYears, String dateOfAppointment,
			long prolongation, String pepOfficeStatus, String notes, String electedDistrict, int areaNumber,
			String partyName, String hotlistCategory, boolean change) {
		super();
		this.designation = designation;
		this.officeName = officeName;
		this.termYears = termYears;
		this.dateOfAppointment = dateOfAppointment;
		this.prolongation = prolongation;
		this.pepOfficeStatus = pepOfficeStatus;
		this.notes = notes;
		this.electedDistrict = electedDistrict;
		this.areaNumber = areaNumber;
		this.partyName = partyName;
		this.hotlistCategory = hotlistCategory;
		this.change = change;
	}

	public HotListOfficeInfo() {
		super();
		this.designation = "";
		this.officeName = "";
		this.termYears = 0;
		this.dateOfAppointment = null;
		this.prolongation = 0;
		this.notes = "";
		this.hotlistCategory = "";
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getSqlDateOfAppointment() {
		return dateOfAppointment;
	}

	public String getElectedDistrict() {
		return electedDistrict;
	}

	public void setElectedDistrict(String electedDistrict) {
		this.electedDistrict = electedDistrict;
	}

	public int getAreaNumber() {
		return areaNumber;
	}

	public void setAreaNumber(int areaNumber) {
		this.areaNumber = areaNumber;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public long getProlongation() {
		return prolongation;
	}

	public void setProlongation(long prolongation) {
		this.prolongation = prolongation;
	}

	public String getPepOfficeStatus() {
		return pepOfficeStatus;
	}

	public void setPepOfficeStatus(String pepOfficeStatus) {
		this.pepOfficeStatus = pepOfficeStatus;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public long getTermYears() {
		return termYears;
	}

	public void setTermYears(long termYears) {
		this.termYears = termYears;
	}

	public String getDateOfAppointment() {
		return dateOfAppointment;
	}

	public void setDateOfAppointment(String dateOfAppointment) {
		this.dateOfAppointment = dateOfAppointment;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getHotlistCategory() {
		return hotlistCategory;
	}

	public void setHotlistCategory(String hotlistCategory) {
		this.hotlistCategory = hotlistCategory;
	}

}
