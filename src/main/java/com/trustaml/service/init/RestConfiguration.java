package com.trustaml.service.init;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.trustaml.service.adversemedia.controller.AdverseMediaRestfulService;
import com.trustaml.service.authentication.AuthenticationFilter;
import com.trustaml.service.bfi.bank.controller.BankRestfulService;
import com.trustaml.service.bfi.branch.controller.BranchRestFulService;
import com.trustaml.service.category.controller.CategoryRestfulService;
import com.trustaml.service.common.exception.core.ExceptionHttpStatusHandler;
import com.trustaml.service.hotlist.controller.HotListRestFulService;
import com.trustaml.service.investigation.controller.InvestigationRestfulService;
import com.trustaml.service.kyc.common.controller.KycRestfulService;
import com.trustaml.service.kyc.kycl.controller.KyclRestFulService;
import com.trustaml.service.kyc.kycn.controller.KYCNRestfulService;
import com.trustaml.service.pepentry.controller.PepRestFulService;
import com.trustaml.service.report.controller.Report;
import com.trustaml.service.risk.controller.CalculateRiskRestfulService;
import com.trustaml.service.risk.controller.FindMatch;
import com.trustaml.service.risk.controller.RiskRestfulService;
import com.trustaml.service.screening.common.controller.ScreeningRestfulService;
import com.trustaml.service.screening.legal.controller.ScreeningLRestFulService;
import com.trustaml.service.screening.natural.controller.ScreeningNaturalRestfulService;
import com.trustaml.service.swift.controller.SwiftRestFulService;
import com.trustaml.service.toandfro.reply.controller.ReplyAndActionRestFulService;
import com.trustaml.service.ttr.controller.TTRRestfulService;
import com.trustaml.service.virtualaccount.controller.VirtualAccountController;

@ApplicationPath("/")
public class RestConfiguration extends Application {

	public Set<Class<?>> getClasses() {
		return new HashSet<Class<?>>(Arrays.asList(KYCNRestfulService.class, Welcome.class, BranchRestFulService.class,
				ExceptionHttpStatusHandler.class, PepRestFulService.class, RiskRestfulService.class,
				AdverseMediaRestfulService.class, CategoryRestfulService.class, SwiftRestFulService.class,
				BankRestfulService.class, ScreeningLRestFulService.class, ReplyAndActionRestFulService.class,
				HotListRestFulService.class, AuthenticationFilter.class, KyclRestFulService.class,
				VirtualAccountController.class, ScreeningNaturalRestfulService.class, FindMatch.class,
				CalculateRiskRestfulService.class, Report.class, TTRRestfulService.class,
				InvestigationRestfulService.class, KycRestfulService.class, ScreeningRestfulService.class));

	}
}
