package com.trustaml.service.init;

import java.util.Arrays;
import java.util.HashSet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.adversemedia.controller.AdverseMediaRestfulService;
import com.trustaml.service.bfi.bank.controller.BankRestfulService;
import com.trustaml.service.bfi.branch.controller.BranchRestFulService;
import com.trustaml.service.category.controller.CategoryRestfulService;
import com.trustaml.service.common.exception.core.ExceptionHttpStatusHandler;
import com.trustaml.service.kyc.kycn.controller.KYCNRestfulService;
import com.trustaml.service.pepentry.controller.PepRestFulService;
import com.trustaml.service.risk.controller.RiskRestfulService;
import com.trustaml.service.screening.legal.controller.ScreeningLRestFulService;
import com.trustaml.service.swift.controller.SwiftRestFulService;
import com.trustaml.service.toandfro.reply.controller.ReplyAndActionRestFulService;

@Path("/")
public class Welcome {
	@GET
	public Response getClasses() {

		/** Exposing the Class Name to URLS **/

		/** Here you have to expose the class to display in general **/
		Response myResponse = Response.status(Response.Status.OK)
				.entity(new HashSet<String>(Arrays.asList(KYCNRestfulService.class.getName(), Welcome.class.getName(),
						BranchRestFulService.class.getName(), ExceptionHttpStatusHandler.class.getName(),
						PepRestFulService.class.getName(), RiskRestfulService.class.getName(),
						AdverseMediaRestfulService.class.getName(), CategoryRestfulService.class.getName(),
						SwiftRestFulService.class.getName(), BankRestfulService.class.getName(),
						ScreeningLRestFulService.class.getName(), ReplyAndActionRestFulService.class.getName())))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON + "; charset=UTF-8").build();
		return myResponse;
	}
}
