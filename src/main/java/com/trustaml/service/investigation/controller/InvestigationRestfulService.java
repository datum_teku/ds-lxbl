package com.trustaml.service.investigation.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.investigation.dao.InvestigationDao;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Path("/investigation")
public class InvestigationRestfulService {

	@Inject
	InvestigationDao investigationDao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveHotList(String jsonString) throws Exception {
		if (jsonString != null && !jsonString.isEmpty()) {
			// MailSender.sendSubmittedEmail("sugamachr@gmail.com", "test",
			// "test");
			// EmailSender.sendHtmlEmail("sugamachr@gmail.com", "test", "test");
			investigationDao.saveInvestigation(jsonString);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateHotList(String jsonString) throws Exception {
		if (!jsonString.isEmpty()) {
			investigationDao.updateInvestion(jsonString);
			return ResponseReturn.sucess("update successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchInvestigationData() throws IllegalArgumentException, SQLException, ParseException,
			JsonParseException, JsonMappingException, IOException {
		return ResponseReturn.sucess(investigationDao.fetchInvestigationData(new ScreeningOfMigratedData()));
	}

}
