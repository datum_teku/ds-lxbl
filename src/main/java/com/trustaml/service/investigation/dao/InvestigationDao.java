package com.trustaml.service.investigation.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantSanctionType;
import com.trustaml.service.hotlist.model.HotList;
import com.trustaml.service.hotlist.model.HotListAddressInfo;
import com.trustaml.service.hotlist.model.HotListFamilyInfo;
import com.trustaml.service.hotlist.model.HotListOfficeInfo;
import com.trustaml.service.hotlist.model.PersonalInfo;
import com.trustaml.service.investigation.model.InvestigationMediaInfo;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Stateless
public class InvestigationDao {

	@Inject
	InvestigationDaoImpl investigationDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ConstantEntity constantEntity;

	public void saveInvestigation(String jsonString) throws Exception {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		String newUniqueKey = "";
		HotList investigation = new ObjectMapper().readValue(jsonString, HotList.class);

		if (!constantEntity.isCountryCodeSet()) {
			constantEntity.setMapForCountryCode();
			constantEntity.convertListToMapForCountryCode();
		}
		String CountryCode = constantEntity
				.getCodeFromMapCountryCode(investigation.getPersonalInfo().getCountryOfBirth());
		String existingUniqueKey = investigationDaoImpl.getLatestUniqueKey();
		if (existingUniqueKey != null) {
			newUniqueKey = constantEntity.generateNewKey(existingUniqueKey, CountryCode);
		} else {
			newUniqueKey = constantEntity.createNewKey(CountryCode, "03");
		}

		String investigationPersonalInfoHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
				investigation.getPersonalInfo(), investigation.getUser(), includeUser);
		Long investigationId = investigationDaoImpl.savePersonalInfo(investigation.getPersonalInfo(),
				investigationPersonalInfoHashValue, newUniqueKey);
		investigation.getPersonalInfo().setHotListPersonalInfoId(investigationId);

		String investigationPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
				investigation.getPersonalInfo(), investigation.getUser(), doNotIncludeUser);
		investigationDaoImpl.savePersonalInfoUpdateTable(investigation.getPersonalInfo(), investigation.getUser(),
				investigationPersonalInfoUpdateHashValue);
		for (HotListOfficeInfo investigationOfficeInfo : investigation.getHotListOfficeInfo()) {
			String investigationOfficeHashValue = hashCodeGenerator.generateHashValueForHotListOffice(
					investigationOfficeInfo, investigationId, investigation.getUser(), includeUser);
			String investigationOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForHotListOffice(
					investigationOfficeInfo, investigationId, investigation.getUser(), doNotIncludeUser);
			investigationDaoImpl.insertInvestigationOfficeInfo(investigationOfficeInfo, investigationId,
					investigationOfficeHashValue);
			investigationDaoImpl.insertInvestigationOfficeInfoUpdateTable(investigationOfficeInfo, investigationId,
					investigation.getUser(), investigationOfficeUpdateHashValue);

		}
		if (investigation.getHotListFamilyInfos().size() > 0) {
			for (HotListFamilyInfo familyInfo : investigation.getHotListFamilyInfos()) {
				String investigationFamilyHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						investigationId, investigation.getUser(), includeUser);
				String investigationFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForHotListFamily(
						familyInfo, investigationId, investigation.getUser(), doNotIncludeUser);
				investigationDaoImpl.insertInvestigationFamilyInfo(familyInfo, investigationId,
						investigationFamilyHashValue);
				investigationDaoImpl.insertInvestigationFamilyInfoUpdateTable(familyInfo, investigationId,
						investigation.getUser(), investigationFamilyUpdateHashValue);
			}
		}
		if (investigation.getHotListAddressInfo().size() > 0) {
			for (HotListAddressInfo investigationAddressInfo : investigation.getHotListAddressInfo()) {
				String investigationAddressHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						investigationAddressInfo, investigationId, investigation.getUser(), includeUser);
				String investigationAddressUpdateHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						investigationAddressInfo, investigationId, investigation.getUser(), doNotIncludeUser);
				investigationDaoImpl.insertInvestigationAddressInfo(investigationAddressInfo, investigationId,
						investigationAddressHashValue);
				investigationDaoImpl.insertInvestigationAddressInfoUpdateTable(investigationAddressInfo,
						investigationId, investigation.getUser(), investigationAddressUpdateHashValue);
			}
		}
		if (investigation.getInvestigationMediaInfo().size() > 0) {
			for (InvestigationMediaInfo investigationMediaInfo : investigation.getInvestigationMediaInfo()) {
				// String investigationMediaHashValue =
				// hashCodeGenerator.generateHashValueForHotListMedia(
				// investigationMediaInfo, investigationId,
				// investigation.getUser(), includeUser);
				// String investigationMediaUpdateHashValue =
				// hashCodeGenerator.generateHashValueForHotListMedia(
				// investigationMediaInfo, investigationId,
				// investigation.getUser(), doNotIncludeUser);
				investigationDaoImpl.insertInvestigationMediaInfo(investigationMediaInfo, investigationId, "hashValue");
				investigationDaoImpl.insertInvestigationMediaInfoUpdateTable(investigationMediaInfo, investigationId,
						investigation.getUser(), "hashValue");
			}
		}

	}

	public void updateInvestion(String jsonString) throws Exception {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;

		HotList investigation = new ObjectMapper().readValue(jsonString, HotList.class);
		if (investigation.getPersonalInfo().isChange()) {
			String investigationPersonalInfoHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
					investigation.getPersonalInfo(), investigation.getUser(), includeUser);
			String investigationPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForHotListPersonalInfo(
					investigation.getPersonalInfo(), investigation.getUser(), doNotIncludeUser);
			investigationDaoImpl.updatePersonalInfo(investigation.getPersonalInfo(),
					investigationPersonalInfoHashValue);
			investigation.getPersonalInfo().setHotListPersonalInfoId(investigation.getPersonalInfo().getId());
			investigationDaoImpl.savePersonalInfoUpdateTable(investigation.getPersonalInfo(), investigation.getUser(),
					investigationPersonalInfoUpdateHashValue);
		}
		long investigationId = investigation.getPersonalInfo().getId();
		for (HotListOfficeInfo investigationOfficeInfo : investigation.getHotListOfficeInfo()) {
			String investigationOfficeHashValue = hashCodeGenerator.generateHashValueForHotListOffice(
					investigationOfficeInfo, investigationId, investigation.getUser(), includeUser);
			String investigationOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForHotListOffice(
					investigationOfficeInfo, investigationId, investigation.getUser(), doNotIncludeUser);
			if (investigationOfficeInfo.getId() != 0) {
				if (investigationOfficeInfo.isChange()) {
					investigationDaoImpl.updateInvestigationOfficeInfo(investigationOfficeInfo, investigationId,
							investigationOfficeHashValue);
					investigationDaoImpl.insertInvestigationOfficeInfoUpdateTable(investigationOfficeInfo,
							investigationId, investigation.getUser(), investigationOfficeUpdateHashValue);
				}

			} else {
				investigationDaoImpl.insertInvestigationOfficeInfo(investigationOfficeInfo, investigationId,
						investigationOfficeHashValue);
				investigationDaoImpl.insertInvestigationOfficeInfoUpdateTable(investigationOfficeInfo, investigationId,
						investigation.getUser(), investigationOfficeUpdateHashValue);
			}

		}
		if (investigation.getHotListFamilyInfos().size() > 0) {
			for (HotListFamilyInfo familyInfo : investigation.getHotListFamilyInfos()) {
				String investigationFamilyHashValue = hashCodeGenerator.generateHashValueForHotListFamily(familyInfo,
						investigationId, investigation.getUser(), includeUser);
				String investigationFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForHotListFamily(
						familyInfo, investigationId, investigation.getUser(), doNotIncludeUser);
				if (familyInfo.getId() != 0) {
					if (familyInfo.isChange()) {
						investigationDaoImpl.updateInvestigationFamilyInfo(familyInfo, investigationId,
								investigationFamilyHashValue);
						investigationDaoImpl.insertInvestigationFamilyInfoUpdateTable(familyInfo, investigationId,
								investigation.getUser(), investigationFamilyUpdateHashValue);
					}
				} else {
					investigationDaoImpl.insertInvestigationFamilyInfo(familyInfo, investigationId,
							investigationFamilyHashValue);
					investigationDaoImpl.insertInvestigationFamilyInfoUpdateTable(familyInfo, investigationId,
							investigation.getUser(), investigationFamilyUpdateHashValue);
				}
			}
		}

		if (investigation.getHotListAddressInfo().size() > 0) {
			for (HotListAddressInfo investigationAddressInfo : investigation.getHotListAddressInfo()) {
				String investigationAddressHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						investigationAddressInfo, investigationId, investigation.getUser(), includeUser);
				String investigationAddressUpdateHashValue = hashCodeGenerator.generateHashValueForHotListAddress(
						investigationAddressInfo, investigationId, investigation.getUser(), doNotIncludeUser);
				if (investigationAddressInfo.getId() != 0) {
					if (investigationAddressInfo.isChange()) {
						investigationDaoImpl.updateInvestigationAddressInfo(investigationAddressInfo, investigationId,
								investigationAddressHashValue);
						investigationDaoImpl.insertInvestigationAddressInfoUpdateTable(investigationAddressInfo,
								investigationId, investigation.getUser(), investigationAddressUpdateHashValue);
					}
				} else {
					investigationDaoImpl.insertInvestigationAddressInfo(investigationAddressInfo, investigationId,
							investigationAddressHashValue);
					investigationDaoImpl.insertInvestigationAddressInfoUpdateTable(investigationAddressInfo,
							investigationId, investigation.getUser(), investigationAddressUpdateHashValue);
				}
			}
		}
		if (investigation.getInvestigationMediaInfo().size() > 0) {
			for (InvestigationMediaInfo investigationMediaInfo : investigation.getInvestigationMediaInfo()) {
				// String investigationMediaHashValue =
				// hashCodeGenerator.generateHashValueForHotListMedia(
				// investigationMediaInfo, investigationId,
				// investigation.getUser(), includeUser);
				// String investigationMediaUpdateHashValue =
				// hashCodeGenerator.generateHashValueForHotListMedia(
				// investigationMediaInfo, investigationId,
				// investigation.getUser(), doNotIncludeUser);
				if (investigationMediaInfo.getId() != 0) {
					if (investigationMediaInfo.isChange()) {
						investigationDaoImpl.updateInvestigationMediaInfo(investigationMediaInfo, investigationId,
								"hashValue", investigation.getUser());
						investigationDaoImpl.insertInvestigationMediaInfoUpdateTable(investigationMediaInfo,
								investigationId, investigation.getUser(), "hashValue");
					}
				} else
					investigationDaoImpl.insertInvestigationMediaInfo(investigationMediaInfo, investigationId,
							"hashValue");
				investigationDaoImpl.insertInvestigationMediaInfoUpdateTable(investigationMediaInfo, investigationId,
						investigation.getUser(), "hashValue");
			}
		}
	}

	public List<PersonalInfo> fetchInvestigationData(ScreeningOfMigratedData investigationDataByDate)
			throws IllegalArgumentException, SQLException, ParseException, JsonParseException, JsonMappingException,
			IOException {
		String newUniqueKey = "";
		ObjectMapper mapper = new ObjectMapper();
		String existingUniqueKey = "";
		boolean generateNewKey = true;
		existingUniqueKey = investigationDaoImpl.getLatestUniqueKey();
		List<PersonalInfo> listPersonalInfo = mapper.readValue(
				investigationDaoImpl.fetchInvestigationData(investigationDataByDate),
				new TypeReference<List<PersonalInfo>>() {
				});
		if (!constantEntity.isCountryCodeSet()) {

			constantEntity.setMapForCountryCode();
			constantEntity.convertListToMapForCountryCode();
		}
		for (PersonalInfo personalInfo : listPersonalInfo) {

			String CountryCode = constantEntity.getCodeFromMapCountryCode(personalInfo.getJurisdiction());
			existingUniqueKey = newUniqueKey;
			if (generateNewKey) {
				newUniqueKey = constantEntity.createNewKey(CountryCode, ConstantSanctionType.INVESTIGATION);
				generateNewKey = false;
			} else {
				existingUniqueKey = newUniqueKey;
				newUniqueKey = constantEntity.generateNewKey(existingUniqueKey, CountryCode);
			}

			investigationDaoImpl.updateInvestigationPersonalInfo(personalInfo, newUniqueKey);
		}
		return listPersonalInfo;
	}

	// HotList investigationInfo = new ObjectMapper()
	// .convertValue(investigationDaoImpl.fetchInvestigationData(investigationDataByDate),
	// HotList.class);return investigationInfo;

}

// public void deActivateHotList(String json)
// throws JsonParseException, JsonMappingException, IOException,
// SQLException {
// DeactivateHotListAccount deactivateHotListAccount = new
// ObjectMapper().readValue(json,
// DeactivateHotListAccount.class);
// investigationDaoImpl.deActivateHotList(deactivateHotListAccount);
// investigationDaoImpl.insertIntoHotListPersonalInfoUpdate(deactivateHotListAccount);
//
// }
