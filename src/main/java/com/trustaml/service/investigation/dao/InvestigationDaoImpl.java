package com.trustaml.service.investigation.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.hotlist.model.HotListAddressInfo;
import com.trustaml.service.hotlist.model.HotListFamilyInfo;
import com.trustaml.service.hotlist.model.HotListOfficeInfo;
import com.trustaml.service.hotlist.model.PersonalInfo;
import com.trustaml.service.investigation.model.InvestigationMediaInfo;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

public class InvestigationDaoImpl {

	@Inject
	DBConnection dbConnection;

	public Long savePersonalInfo(PersonalInfo personalInfo, String investigationPersonalInfoHashValue,
			String newUniqueKey) throws SQLException, ParseException {
		long personalInfoId = 0;
		String sql = "INSERT INTO list_investigation_info (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,hash,key)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, investigationPersonalInfoHashValue);
			ps.setString(20, newUniqueKey);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				personalInfoId = rs.getLong(1);
			}
		} finally {
			con.close();
			ps.close();
		}

		return personalInfoId;
	}

	public void savePersonalInfoUpdateTable(PersonalInfo personalInfo, User user,
			String investigationPersonalInfoUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_investigation_info_update (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,maker,checker,reason,list_investigation_info_id,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, user.getUserName());
			ps.setString(20, user.getUserName());
			ps.setString(21, "No Reason");
			ps.setLong(22, personalInfo.getHotListId());
			ps.setString(23, investigationPersonalInfoUpdateHashValue);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationOfficeInfo(HotListOfficeInfo investigationOfficeInfo, Long investigationId,
			String investigationOfficeHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_investigation_office_info (list_investigation_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,document_code,hash) VALUES(?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationOfficeInfo.getDesignation());
			ps.setString(3, investigationOfficeInfo.getOfficeName());
			ps.setLong(4, investigationOfficeInfo.getProlongation());
			ps.setString(5, investigationOfficeInfo.getNotes());
			ps.setLong(6, investigationOfficeInfo.getTermYears());
			if (!investigationOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(investigationOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, investigationOfficeInfo.getPepOfficeStatus());
			ps.setString(9, investigationOfficeInfo.getHotlistCategory());
			ps.setString(10, investigationOfficeHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationOfficeInfoUpdateTable(HotListOfficeInfo investigationOfficeInfo,
			Long investigationId, User user, String investigationOfficeUpdateHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_investigation_office_info_update"
				+ "(list_investigation_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,maker,checker,reason,document_code,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationOfficeInfo.getDesignation());
			ps.setString(3, investigationOfficeInfo.getOfficeName());
			ps.setLong(4, investigationOfficeInfo.getProlongation());
			ps.setString(5, investigationOfficeInfo.getNotes());
			ps.setLong(6, investigationOfficeInfo.getTermYears());
			if (!investigationOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(investigationOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, investigationOfficeInfo.getPepOfficeStatus());
			ps.setString(9, user.getUserName());
			ps.setString(10, user.getUserName());
			ps.setString(11, "no reason");
			ps.setString(12, investigationOfficeInfo.getHotlistCategory());
			ps.setString(13, investigationOfficeUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			con.close();
		}

	}

	public void insertInvestigationFamilyInfo(HotListFamilyInfo familyInfo, Long investigationId,
			String investigationFamilyHashValue) throws SQLException {
		String sql = "INSERT INTO list_investigation_family_info (list_investigation_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, investigationFamilyHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationFamilyInfoUpdateTable(HotListFamilyInfo familyInfo, Long investigationId, User user,
			String investigationFamilyUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_investigation_family_info_update"
				+ "(list_investigation_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,maker,checker,reason,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, investigationFamilyUpdateHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationAddressInfo(HotListAddressInfo investigationAddressInfo, Long investigationId,
			String investigationAddressHashValue) throws SQLException {
		String sql = "INSERT INTO list_investigation_address_info (list_investigation_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationAddressInfo.getCountry());
			ps.setString(3, investigationAddressInfo.getState());
			ps.setString(4, investigationAddressInfo.getProvince());
			ps.setString(5, investigationAddressInfo.getDistrict());
			ps.setString(6, investigationAddressInfo.getMnVdc());
			ps.setString(7, investigationAddressInfo.getTownCityVillage());
			ps.setString(8, investigationAddressInfo.getTole());
			ps.setString(9, investigationAddressInfo.getStreet());
			ps.setString(10, investigationAddressInfo.getHouseNumber());
			ps.setString(11, investigationAddressInfo.getNotes());
			ps.setString(12, investigationAddressHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationAddressInfoUpdateTable(HotListAddressInfo investigationAddressInfo,
			Long investigationId, User user, String investigationAddressUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_investigation_address_info_update"
				+ "(list_investigation_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,maker,checker,reason,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationAddressInfo.getCountry());
			ps.setString(3, investigationAddressInfo.getState());
			ps.setString(4, investigationAddressInfo.getProvince());
			ps.setString(5, investigationAddressInfo.getDistrict());
			ps.setString(6, investigationAddressInfo.getMnVdc());
			ps.setString(7, investigationAddressInfo.getTownCityVillage());
			ps.setString(8, investigationAddressInfo.getTole());
			ps.setString(9, investigationAddressInfo.getStreet());
			ps.setString(10, investigationAddressInfo.getHouseNumber());
			ps.setString(11, investigationAddressInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, investigationAddressUpdateHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationMediaInfo(InvestigationMediaInfo investigationMediaInfo, Long investigationId,
			String investigationMediaHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_investigation_media_info(list_investigation_id,photo_id_number,photo_serial_number,photo_comment,social_media,social_media_publication_date,adverse_media,adverse_media_publication_date,date_of_entry,date_of_validation,source_of_information,type_of_source,confidence_level_of_source,notes,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationMediaInfo.getPhotoIdNumber());
			ps.setString(3, investigationMediaInfo.getPhotoSerialNumber());
			ps.setString(4, investigationMediaInfo.getPhotoComment());
			ps.setString(5, investigationMediaInfo.getSocialMedia());
			if (!investigationMediaInfo.getSocialMediaPublicationDate().equals("")) {
				ps.setDate(6, getBirthDate(investigationMediaInfo.getSocialMediaPublicationDate()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, investigationMediaInfo.getAdverseMedia());
			if (!investigationMediaInfo.getAdverseMediaPublicationDate().equals("")) {
				ps.setDate(8, getBirthDate(investigationMediaInfo.getAdverseMediaPublicationDate()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfEntry().equals("")) {
				ps.setDate(9, getBirthDate(investigationMediaInfo.getDateOfEntry()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfValidation().equals("")) {
				ps.setDate(10, getBirthDate(investigationMediaInfo.getDateOfValidation()));
			} else {
				ps.setNull(10, java.sql.Types.DATE);
			}
			ps.setString(11, investigationMediaInfo.getSourceOfInformation());
			ps.setString(12, investigationMediaInfo.getTypeOfSource());
			ps.setString(13, investigationMediaInfo.getConfidenceLevelOfSource());
			ps.setString(14, investigationMediaInfo.getNotes());
			ps.setString(15, investigationMediaHashValue);
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void insertInvestigationMediaInfoUpdateTable(InvestigationMediaInfo investigationMediaInfo,
			Long investigationId, User user, String investigationMediaUpdateHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_investigation_media_info_update(list_investigation_info_id,photo_id_number,photo_serial_number,photo_comment,social_media,social_media_publication_date,adverse_media,adverse_media_publication_date,date_of_entry,date_of_validation,source_of_information,type_of_source,confidence_level_of_source,notes,hash,maker,checker) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationMediaInfo.getPhotoIdNumber());
			ps.setString(3, investigationMediaInfo.getPhotoSerialNumber());
			ps.setString(4, investigationMediaInfo.getPhotoComment());
			ps.setString(5, investigationMediaInfo.getSocialMedia());
			if (!investigationMediaInfo.getSocialMediaPublicationDate().equals("")) {
				ps.setDate(6, getBirthDate(investigationMediaInfo.getSocialMediaPublicationDate()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, investigationMediaInfo.getAdverseMedia());
			if (!investigationMediaInfo.getAdverseMediaPublicationDate().equals("")) {
				ps.setDate(8, getBirthDate(investigationMediaInfo.getAdverseMediaPublicationDate()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfEntry().equals("")) {
				ps.setDate(9, getBirthDate(investigationMediaInfo.getDateOfEntry()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfValidation().equals("")) {
				ps.setDate(10, getBirthDate(investigationMediaInfo.getDateOfValidation()));
			} else {
				ps.setNull(10, java.sql.Types.DATE);
			}
			ps.setString(11, investigationMediaInfo.getSourceOfInformation());
			ps.setString(12, investigationMediaInfo.getTypeOfSource());
			ps.setString(13, investigationMediaInfo.getConfidenceLevelOfSource());
			ps.setString(14, investigationMediaInfo.getNotes());
			ps.setString(15, investigationMediaUpdateHashValue);
			ps.setString(16, user.getUserName());
			ps.setString(17, user.getUserName());

			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	public void updatePersonalInfo(PersonalInfo personalInfo, String investigationPersonalInfoHashValue)
			throws SQLException, ParseException {
		String sql = "UPDATE list_investigation_info SET jurisdiction=?,title=?,"
				+ "first_name=?,middle_name=?,last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,"
				+ "second_name=?,called_by_name=?,previous_name=?,gender=?,date_of_birth=?,"
				+ "place_of_birth=?,country_of_birth=?,nationality=?,notes=?,document_code=?,hash=? WHERE id=?";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getHotlistCategory());
			ps.setString(19, investigationPersonalInfoHashValue);
			ps.setLong(20, personalInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateInvestigationOfficeInfo(HotListOfficeInfo investigationOfficeInfo, long investigationId,
			String investigationOfficeHashValue) throws SQLException, ParseException {
		String sql = "UPDATE list_hot_list_office_info SET list_investigation_info_id=?,designation=?,office_name=?,prolongation=?,"
				+ "notes=?,term_year=?,date_of_appointment=?,office_status=?,document_code=?,hash=? WHERE id=?";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationOfficeInfo.getDesignation());
			ps.setString(3, investigationOfficeInfo.getOfficeName());
			ps.setLong(4, investigationOfficeInfo.getProlongation());
			ps.setString(5, investigationOfficeInfo.getNotes());
			ps.setLong(6, investigationOfficeInfo.getTermYears());
			if (!investigationOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(investigationOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, investigationOfficeInfo.getPepOfficeStatus());
			ps.setString(9, investigationOfficeInfo.getHotlistCategory());
			ps.setString(10, investigationOfficeHashValue);
			ps.setLong(11, investigationOfficeInfo.getId());
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateInvestigationFamilyInfo(HotListFamilyInfo familyInfo, long investigationId,
			String investigationFamilyHashValue) throws SQLException {
		String sql = "UPDATE list_investigation_family_info SET list_investigation_info_id=?,relationship_to=?,first_name=?,middle_name=?,"
				+ "last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,notes=?,hash=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, investigationFamilyHashValue);
			ps.setLong(13, familyInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateInvestigationAddressInfo(HotListAddressInfo investigationAddressInfo, long investigationId,
			String investigationAddressHashValue) throws SQLException {
		String sql = "UPDATE list_investigation_address_info SET list_investigation_info_id=?,country=?,state=?,province=?,district=?,mn_vdc=?,"
				+ "town_city_village=?,tole=?,street=?,house_number=?,notes=?,hash=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationAddressInfo.getCountry());
			ps.setString(3, investigationAddressInfo.getState());
			ps.setString(4, investigationAddressInfo.getProvince());
			ps.setString(5, investigationAddressInfo.getDistrict());
			ps.setString(6, investigationAddressInfo.getMnVdc());
			ps.setString(7, investigationAddressInfo.getTownCityVillage());
			ps.setString(8, investigationAddressInfo.getTole());
			ps.setString(9, investigationAddressInfo.getStreet());
			ps.setString(10, investigationAddressInfo.getHouseNumber());
			ps.setString(11, investigationAddressInfo.getNotes());
			ps.setString(12, investigationAddressHashValue);
			ps.setLong(13, investigationAddressInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateInvestigationMediaInfo(InvestigationMediaInfo investigationMediaInfo, long investigationId,
			String investigationMediaHashValue, User user) throws SQLException, ParseException {
		String sql = "UPDATE list_investigation_media_info SET list_investigation_id=?,photo_id_number=?,photo_serial_number=?,photo_comment=?,social_media=?,social_media_publication_date=?,adverse_media=?,adverse_media_publication_date=?,date_of_entry=?,date_of_validation=?,source_of_information=?,type_of_source=?,confidence_level_of_source=?,notes=?,hash=?,maker=?,checker=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, investigationId);
			ps.setString(2, investigationMediaInfo.getPhotoIdNumber());
			ps.setString(3, investigationMediaInfo.getPhotoSerialNumber());
			ps.setString(4, investigationMediaInfo.getPhotoComment());
			ps.setString(5, investigationMediaInfo.getSocialMedia());
			if (!investigationMediaInfo.getSocialMediaPublicationDate().equals("")) {
				ps.setDate(6, getBirthDate(investigationMediaInfo.getSocialMediaPublicationDate()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, investigationMediaInfo.getAdverseMedia());
			if (!investigationMediaInfo.getAdverseMediaPublicationDate().equals("")) {
				ps.setDate(8, getBirthDate(investigationMediaInfo.getAdverseMediaPublicationDate()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfEntry().equals("")) {
				ps.setDate(9, getBirthDate(investigationMediaInfo.getDateOfEntry()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			if (!investigationMediaInfo.getDateOfValidation().equals("")) {
				ps.setDate(10, getBirthDate(investigationMediaInfo.getDateOfValidation()));
			} else {
				ps.setNull(10, java.sql.Types.DATE);
			}
			ps.setString(11, investigationMediaInfo.getSourceOfInformation());
			ps.setString(12, investigationMediaInfo.getTypeOfSource());
			ps.setString(13, investigationMediaInfo.getConfidenceLevelOfSource());
			ps.setString(14, investigationMediaInfo.getNotes());
			ps.setString(15, investigationMediaHashValue);
			ps.setString(16, user.getUserName());
			ps.setString(17, user.getUserName());
			ps.setLong(18, investigationMediaInfo.getId());
			ps.execute();
		} finally {
			con.close();
			ps.close();
		}

	}

	public String fetchInvestigationData(ScreeningOfMigratedData investigationDataByDate)
			throws SQLException, ParseException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		// String dateRange = investigationDataByDate.getDateRange();
		// String dateFrom = dateRange.substring(0,
		// dateRange.indexOf("to")).trim();
		// String dateTo = dateRange.substring(dateRange.indexOf("to") + 2,
		// dateRange.length()).trim();
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_sanction_list(?,?)}");
			// stmt.setDate(1, getDate(dateFrom));
			// stmt.setDate(2, getDate(dateTo));
			stmt.setInt(1, 3);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public String getLatestUniqueKey() throws SQLException {
		String uniquekey = null;
		String sql = "SELECT key from list_investigation_info order by id desc limit 1";
		Connection con = null;
		Statement psmt = null;
		try {
			con = dbConnection.getConnection();
			psmt = con.createStatement();
			ResultSet rs = psmt.executeQuery(sql);
			while (rs.next()) {
				uniquekey = rs.getString(1);
			}

		} finally {
			psmt.close();
			con.close();
		}
		return uniquekey;
	}

	public void updateInvestigationPersonalInfo(PersonalInfo personalInfo, String newUniqueKey) throws SQLException {
		String sql = "UPDATE list_investigation_info SET key=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, newUniqueKey);
			ps.setLong(2, personalInfo.getId());
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}

}
