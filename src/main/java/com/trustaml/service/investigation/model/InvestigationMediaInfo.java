package com.trustaml.service.investigation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InvestigationMediaInfo {
	private long id;

	@JsonProperty("list_investigation_id")
	private Long listInvestigationId;

	@JsonProperty("photo_id_number")
	private String photoIdNumber;

	@JsonProperty("photo_serial_number")
	private String photoSerialNumber;

	@JsonProperty("photo_comment")
	private String photoComment;

	@JsonProperty("social_media")
	private String socialMedia;

	@JsonProperty("social_media_publication_date")
	private String socialMediaPublicationDate;

	@JsonProperty("adverse_media")
	private String adverseMedia;

	@JsonProperty("adverse_media_publication_date")
	private String adverseMediaPublicationDate;

	@JsonProperty("date_of_entry")
	private String dateOfEntry;

	@JsonProperty("date_of_validation")
	private String dateOfValidation;

	@JsonProperty("source_of_information")
	private String sourceOfInformation;

	@JsonProperty("type_of_source")
	private String typeOfSource;

	@JsonProperty("confidence_level_of_source")
	private String confidenceLevelOfSource;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean isChange;

	public InvestigationMediaInfo() {
		super();
		this.id = 0;
		// TODO Auto-generated constructor stub
	}

	public InvestigationMediaInfo(Long id, Long listInvestigationId, String photoIdNumber, String photoSerialNumber,
			String photoComment, String socialMedia, String socialMediaPublicationDate, String adverseMedia,
			String adverseMediaPublicationDate, String dateOfEntry, String dateOfValidation, String sourceOfInformation,
			String typeOfSource, String confidenceLevelOfSource, String notes, boolean isChange) {
		super();
		this.id = id;
		this.listInvestigationId = listInvestigationId;
		this.photoIdNumber = photoIdNumber;
		this.photoSerialNumber = photoSerialNumber;
		this.photoComment = photoComment;
		this.socialMedia = socialMedia;
		this.socialMediaPublicationDate = socialMediaPublicationDate;
		this.adverseMedia = adverseMedia;
		this.adverseMediaPublicationDate = adverseMediaPublicationDate;
		this.dateOfEntry = dateOfEntry;
		this.dateOfValidation = dateOfValidation;
		this.sourceOfInformation = sourceOfInformation;
		this.typeOfSource = typeOfSource;
		this.confidenceLevelOfSource = confidenceLevelOfSource;
		this.notes = notes;
		this.isChange = isChange;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getListInvestigationId() {
		return listInvestigationId;
	}

	public void setListInvestigationId(Long listInvestigationId) {
		this.listInvestigationId = listInvestigationId;
	}

	public String getPhotoIdNumber() {
		return photoIdNumber;
	}

	public void setPhotoIdNumber(String photoIdNumber) {
		this.photoIdNumber = photoIdNumber;
	}

	public String getPhotoSerialNumber() {
		return photoSerialNumber;
	}

	public void setPhotoSerialNumber(String photoSerialNumber) {
		this.photoSerialNumber = photoSerialNumber;
	}

	public String getPhotoComment() {
		return photoComment;
	}

	public void setPhotoComment(String photoComment) {
		this.photoComment = photoComment;
	}

	public String getSocialMedia() {
		return socialMedia;
	}

	public void setSocialMedia(String socialMedia) {
		this.socialMedia = socialMedia;
	}

	public String getSocialMediaPublicationDate() {
		return socialMediaPublicationDate;
	}

	public void setSocialMediaPublicationDate(String socialMediaPublicationDate) {
		this.socialMediaPublicationDate = socialMediaPublicationDate;
	}

	public String getAdverseMedia() {
		return adverseMedia;
	}

	public void setAdverseMedia(String adverseMedia) {
		this.adverseMedia = adverseMedia;
	}

	public String getAdverseMediaPublicationDate() {
		return adverseMediaPublicationDate;
	}

	public void setAdverseMediaPublicationDate(String adverseMediaPublicationDate) {
		this.adverseMediaPublicationDate = adverseMediaPublicationDate;
	}

	public String getDateOfEntry() {
		return dateOfEntry;
	}

	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}

	public String getDateOfValidation() {
		return dateOfValidation;
	}

	public void setDateOfValidation(String dateOfValidation) {
		this.dateOfValidation = dateOfValidation;
	}

	public String getSourceOfInformation() {
		return sourceOfInformation;
	}

	public void setSourceOfInformation(String sourceOfInformation) {
		this.sourceOfInformation = sourceOfInformation;
	}

	public String getTypeOfSource() {
		return typeOfSource;
	}

	public void setTypeOfSource(String typeOfSource) {
		this.typeOfSource = typeOfSource;
	}

	public String getConfidenceLevelOfSource() {
		return confidenceLevelOfSource;
	}

	public void setConfidenceLevelOfSource(String confidenceLevelOfSource) {
		this.confidenceLevelOfSource = confidenceLevelOfSource;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return isChange;
	}

	public void setChange(boolean isChange) {
		this.isChange = isChange;
	}

}
