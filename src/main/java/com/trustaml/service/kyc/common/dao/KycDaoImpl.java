package com.trustaml.service.kyc.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;

public class KycDaoImpl {

	@Inject
	DBConnection dbConnection;

	public int fetchDayBeforeKycRefesh() throws SQLException {
		int duration = 0;
		String sql = "SELECT duration from kyc_notify";
		Connection con = null;
		Statement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.createStatement();
			ResultSet rs = ps.executeQuery(sql);
			while (rs.next()) {
				duration = Integer.valueOf(rs.getString(1));
			}

		} finally {
			con.close();
			ps.close();
		}
		return duration;

	}

	public String fetchKycnBeyondDate(int dateBefore30Days) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_kyc_info(?,?,?)}");
			stmt.setInt(1, 1);
			stmt.setInt(2, dateBefore30Days);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			con.close();
			stmt.close();
		}
		return result;

	}

	public String fetchKyclBeyondDate(int dateBefore30Days) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_kyc_info(?,?,?)}");
			stmt.setInt(1, 2);
			stmt.setInt(2, dateBefore30Days);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			con.close();
			stmt.close();
		}
		return result;

	}

}
