package com.trustaml.service.kyc.common.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RefreshDataBeforeSchedule {

	@JsonProperty("kyc_id")
	private long kycId;

	@JsonProperty("name_of_customer")
	private String nameOfCustomer;

	@JsonProperty("date_of_birth")
	private Date dateOfBirth;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("last_kyc_updated_date")
	private Date lastKycUpdatedDate;

	@JsonProperty("date_of_establishment")
	private Date dateOfEstablishment;

	public RefreshDataBeforeSchedule() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RefreshDataBeforeSchedule(long kycId, String nameOfCustomer, Date dateOfBirth, String phoneNo,
			String emailId, String custId, Date lastKycUpdatedDate, Date dateOfEstablishment) {
		super();
		this.kycId = kycId;
		this.nameOfCustomer = nameOfCustomer;
		this.dateOfBirth = dateOfBirth;
		this.phoneNo = phoneNo;
		this.emailId = emailId;
		this.custId = custId;
		this.lastKycUpdatedDate = lastKycUpdatedDate;
		this.dateOfEstablishment = dateOfEstablishment;
	}

	public long getKycId() {
		return kycId;
	}

	public void setKycId(long kycId) {
		this.kycId = kycId;
	}

	public String getNameOfCustomer() {
		return nameOfCustomer;
	}

	public void setNameOfCustomer(String nameOfCustomer) {
		this.nameOfCustomer = nameOfCustomer;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public Date getLastKycUpdatedDate() {
		return lastKycUpdatedDate;
	}

	public void setLastKycUpdatedDate(Date lastKycUpdatedDate) {
		this.lastKycUpdatedDate = lastKycUpdatedDate;
	}

	public Date getDateOfEstablishment() {
		return dateOfEstablishment;
	}

	public void setDateOfEstablishment(Date dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}

}
