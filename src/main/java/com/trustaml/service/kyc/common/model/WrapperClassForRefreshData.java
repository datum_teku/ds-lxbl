package com.trustaml.service.kyc.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WrapperClassForRefreshData {

	@JsonProperty("kyclData")
	private String kyclData;

	@JsonProperty("kycnData")
	private String kycnData;

	public WrapperClassForRefreshData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WrapperClassForRefreshData(String kyclData, String kycnData) {
		super();
		this.kyclData = kyclData;
		this.kycnData = kycnData;
	}

	public String getKyclData() {
		return kyclData;
	}

	public void setKyclData(String kyclData) {
		this.kyclData = kyclData;
	}

	public String getKycnData() {
		return kycnData;
	}

	public void setKycnData(String kycnData) {
		this.kycnData = kycnData;
	}

}
