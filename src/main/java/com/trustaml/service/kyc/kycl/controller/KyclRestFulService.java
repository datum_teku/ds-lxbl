package com.trustaml.service.kyc.kycl.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.kyc.kycl.dao.KyclDao;

@Path("/kycl-registration")
public class KyclRestFulService {

	@Inject
	KyclDao kyclDao;

	/**
	 * @param jsonString
	 * @return generated id after the object of kycl is saved
	 * @throws Exception
	 * @error errorCode
	 * @description saves kycl object into multiple table
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveKycl(String jsonString) throws Exception {
		if (jsonString != null && !jsonString.isEmpty()) {
			Long kyclSaveId = kyclDao.saveKycl(jsonString);
			return ResponseReturn.sucess(String.valueOf(kyclSaveId));
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return update successful
	 * @throws Exception
	 * @description update the kycl data using kycl id known as refresh
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response kyclRefresh(String jsonString) throws Exception {
		if (jsonString != null && !jsonString.isEmpty()) {
			return ResponseReturn.sucess(String.valueOf(kyclDao.udateKycl(jsonString)));
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

}
