package com.trustaml.service.kyc.kycl.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycl.model.AccountInfo;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.AuditInfo;
import com.trustaml.service.kyc.kycl.model.BusinessInfo;
import com.trustaml.service.kyc.kycl.model.DocumentStatus;
import com.trustaml.service.kyc.kycl.model.Kycl;
import com.trustaml.service.kyc.kycl.model.LandlordInfo;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationAddress;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycl.model.RelatedEntity;
import com.trustaml.service.kyc.kycl.model.RelatedPerson;
import com.trustaml.service.kyc.kycn.dao.KYCNDaoImpl;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;

@Stateless
public class KyclDao {

	@Inject
	KyclDaoImpl kyclDaoImpl;

	@Inject
	KYCNDaoImpl kycnDaoImpl;

	@Inject
	ConstantEntity constantEntity;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	private boolean includeUser = true;
	private boolean doNotIncludeUser = false;

	/**
	 * @param jsonString
	 * @throws Exception
	 */
	public Long saveKycl(String jsonString) throws Exception {
		Kycl kycl = new ObjectMapper().readValue(jsonString, Kycl.class);
		boolean includeUser = true;
		boolean doNotIncludeUser = false;

		String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kycl.getLegal(), doNotIncludeUser,
				kycl.getUser());
		String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kycl.getLegal(), includeUser,
				kycl.getUser());
		Long kyclId = kyclDaoImpl.saveLegalInfo(kycl.getLegal(), kycl.getUser(), kyclHashCode);
		kyclDaoImpl.saveLegalInfoInUpdateTable(kycl.getLegal(), kycl.getUser(), kyclUpdateHashCode);

		for (AccountInfo accountInfo : kycl.getListAccountInfo()) {
			String accountHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo, kyclId,
					doNotIncludeUser, kycl.getUser());
			String accountUpdateHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo, kyclId,
					includeUser, kycl.getUser());
			kyclDaoImpl.saveAccountInfo(accountInfo, kyclId, accountHashCode);
			kyclDaoImpl.saveAccountInfoInUpdateTable(accountInfo, kyclId, kycl.getUser(), accountUpdateHashCode);
			if (accountInfo.getAccountsRelatedPersonList() != null
					&& !(accountInfo.getAccountsRelatedPersonList().isEmpty())) {
				long kycnID = 0;
				for (RelatedPerson relatedPerson : accountInfo.getAccountsRelatedPersonList()) {
					relatedPerson.setAccountNo(accountInfo.getAccountNo());
					String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
							relatedPerson, kyclId, doNotIncludeUser, kycl.getUser());
					String relatedPersonUpdateHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
							relatedPerson, kyclId, includeUser, kycl.getUser());

					/** Save separate new KYCN of added new related person **/
					if (relatedPerson.getKycnId() <= 0) {
						
						long newRelatedPersonKYCNId = 0;
						if (relatedPerson.getPersonType().equalsIgnoreCase("Director") && relatedPerson.getIsDirector().equals("true")) {
							newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
									kycl.getUser());
							newRelatedPersonKYCNId=relatedPerson.setKycnId(newRelatedPersonKYCNId);
							kycnID=newRelatedPersonKYCNId;

						}else if(relatedPerson.getIsDirector().equals("false")){
							newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
									kycl.getUser());
							relatedPerson.setKycnId(newRelatedPersonKYCNId);
							kycnID=newRelatedPersonKYCNId;
						}else{
							relatedPerson.setKycnId(kycnID);
						}
						
					}
				/***/

					kyclDaoImpl.saveRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
					kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
							relatedPersonUpdateHashCode);
				}
			}
		}
		// for (AccountCards accountCard : kycl.getListAccountCard()) {
		// kyclDaoImpl.saveAccountCard(accountCard, kyclId);
		// }
		// for (AccountService accountService : kycl.getListAccountService()) {
		// kyclDaoImpl.saveAccountService(accountService, kyclId);
		// }
		for (Address address : kycl.getListAddress()) {
			String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId, doNotIncludeUser,
					kycl.getUser());
			String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId, includeUser,
					kycl.getUser());
			kyclDaoImpl.saveAddress(address, kyclId, addressHashCode,kycl.getUser());
			kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, kycl.getUser(), addressUpdateHashCode);
		}
		for (AuditInfo auditInfo : kycl.getListAuditInfo()) {
			String auditInfoHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
					doNotIncludeUser, kycl.getUser());
			String auditInfoUpdateHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
					includeUser, kycl.getUser());
			kyclDaoImpl.saveAuditInfo(auditInfo, kyclId, auditInfoHashCode);
			kyclDaoImpl.saveAuditInfoInUpdateTable(auditInfo, kyclId, kycl.getUser(), auditInfoUpdateHashCode);
		}
		for (BusinessInfo businessInfo : kycl.getListBusinessInfo()) {
			String businessInfoHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo, kyclId,
					doNotIncludeUser, kycl.getUser());
			String businessInfoUpdateHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo, kyclId,
					includeUser, kycl.getUser());
			kyclDaoImpl.saveBusinessInfo(businessInfo, kyclId, businessInfoHashCode);
			kyclDaoImpl.saveBusinessInfoInUpdateTable(businessInfo, kyclId, kycl.getUser(), businessInfoUpdateHashCode);
		}

		for (DocumentStatus documentStatus : kycl.getListDocumentStatus()) {

			String documentStatusHashCode = hashCodeGenerator.generateHashValueForDocumentStatus(documentStatus, kyclId,
					doNotIncludeUser, kycl.getUser());
			String documentStatusUpdateHashCode = hashCodeGenerator.generateHashValueForDocumentStatus(documentStatus,
					kyclId, includeUser, kycl.getUser());
			kyclDaoImpl.saveDocumentStatus(documentStatus, kyclId, documentStatusHashCode);
			kyclDaoImpl.saveDocumentStatusInUpdateTable(documentStatus, kyclId, kycl.getUser(),
					documentStatusUpdateHashCode);
		}
		for (LandlordInfo landlordInfo : kycl.getListLandlordAddress()) {
			String landlordInfoHashCode = hashCodeGenerator.generateHashValueForLandlordStatus(landlordInfo, kyclId,
					doNotIncludeUser, kycl.getUser());
			String landlordInfoUpdateHashCode = hashCodeGenerator.generateHashValueForLandlordStatus(landlordInfo,
					kyclId, includeUser, kycl.getUser());
			kyclDaoImpl.saveLandlordInfo(landlordInfo, kyclId, landlordInfoHashCode);
			kyclDaoImpl.saveLandlordInfoInUpdateTable(landlordInfo, kyclId, kycl.getUser(), landlordInfoUpdateHashCode);
		}
		for (RegistrationAddress registrationAddress : kycl.getListRegistrationAddress()) {
			String registrationAddressHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
					registrationAddress, kyclId, doNotIncludeUser, kycl.getUser());
			String registrationAddressUpdateHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
					registrationAddress, kyclId, includeUser, kycl.getUser());
			kyclDaoImpl.saveRegistrationAddress(registrationAddress, kyclId, registrationAddressHashCode);
			kyclDaoImpl.saveRegistrationAddressInUpdateTable(registrationAddress, kyclId, kycl.getUser(),
					registrationAddressUpdateHashCode);
		}
		for (RegistrationInfo registrationInfo : kycl.getListRegistrationInfo()) {
			String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
					registrationInfo, kyclId, doNotIncludeUser, kycl.getUser());
			String registrationInfoUpdateHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
					registrationInfo, kyclId, includeUser, kycl.getUser());
			kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
			kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId, kycl.getUser(),
					registrationInfoUpdateHashCode);
		}
		for (RelatedEntity relatedEntity : kycl.getListRelatedEntityInfo()) {
			String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(relatedEntity,
					kyclId, doNotIncludeUser, kycl.getUser());

			String relatedEntityUpdateHashCode = hashCodeGenerator
					.generateHashValueForRelatedEntityStatus(relatedEntity, kyclId, includeUser, kycl.getUser());

			/** Save separate new KYC of added new related entity **/
			if (relatedEntity.getKycnId() <= 0) {
				Long newKyclId = insertNewRelatedEntityAddedInKYC(relatedEntity, kycl.getUser());
				relatedEntity.setKycnId(newKyclId);
			}
			/***/

			kyclDaoImpl.saveRelatedEntityInfo(relatedEntity, kyclId, relatedEntityHashCode);
			kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntity, kyclId, kycl.getUser(),
					relatedEntityUpdateHashCode);
		}
		for (RelatedPerson relatedPerson : kycl.getListRelatedPerson()) {
			String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(relatedPerson,
					kyclId, doNotIncludeUser, kycl.getUser());

			String relatedPersonUpdateHashCode = hashCodeGenerator
					.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser, kycl.getUser());

			/** Save separate new KYCN of added new related person **/
			if (relatedPerson.getKycnId() <= 0) {
				long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson, kycl.getUser());
				relatedPerson.setKycnId(newRelatedPersonKYCNId);
			}
			/***/

			kyclDaoImpl.saveRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
			kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
					relatedPersonUpdateHashCode);
		}
		String complainceInfoHashCode = hashCodeGenerator.generateHashValueForComplainceInfo(kycl.getComplianceInfo(),
				kyclId, doNotIncludeUser, kycl.getUser());

		String complainceInfoUpdateHashCode = hashCodeGenerator
				.generateHashValueForComplainceInfo(kycl.getComplianceInfo(), kyclId, includeUser, kycl.getUser());
		kyclDaoImpl.saveComplainceInfo(kycl.getComplianceInfo(), kyclId, complainceInfoHashCode);
		kyclDaoImpl.saveComplainceInfoInUpdateTable(kycl.getComplianceInfo(), kyclId, kycl.getUser(),
				complainceInfoUpdateHashCode);

		String financialInfoHashCode = hashCodeGenerator.generateHashValueForFinancialInfo(kycl.getFinancialInfo(),
				kyclId, doNotIncludeUser, kycl.getUser());

		String financialInfoUpdateHashCode = hashCodeGenerator
				.generateHashValueForFinancialInfo(kycl.getFinancialInfo(), kyclId, includeUser, kycl.getUser());
		kyclDaoImpl.saveFinancialInfo(kycl.getFinancialInfo(), kyclId, financialInfoHashCode);
		kyclDaoImpl.saveFinancialInfoInUpdateTable(kycl.getFinancialInfo(), kyclId, kycl.getUser(),
				financialInfoUpdateHashCode);
		for (String individual : kycl.getFinancialInfo().getIndividualCardSubscribed()) {

			if (!kyclDaoImpl.checkIfSubscribedIndividualExists(individual, kyclId)) {

				String individualAccountInfoHashCode = hashCodeGenerator.generateHashValueForIndividualAccountInfo(
						individual, kyclId, doNotIncludeUser, kycl.getUser());

				String individualAccountInfoUpdateHashCode = hashCodeGenerator
						.generateHashValueForIndividualAccountInfo(individual, kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.insertCardSubscribedIndividual(individual, kyclId, individualAccountInfoHashCode);
				kyclDaoImpl.insertCardSubscribedIndividualUpdateTable(individual, kyclId, kycl.getUser(),
						individualAccountInfoUpdateHashCode);
			}
		}
		for (String service : kycl.getFinancialInfo().getIndividualServiceSubscribed()) {
			if (!kyclDaoImpl.checkIfSubscribedServiceExists(service, kyclId)) {

				String serviceAccountInfoHashCode = hashCodeGenerator.generateHashValueForServiceAccountInfo(service,
						kyclId, doNotIncludeUser, kycl.getUser());

				String serviceAccountInfoUpdateHashCode = hashCodeGenerator
						.generateHashValueForServiceAccountInfo(service, kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.insertServiceSubscribedIndividual(service, kyclId, serviceAccountInfoHashCode);
				kyclDaoImpl.insertServiceSubscribedIndividualUpdateTable(service, kyclId, kycl.getUser(),
						serviceAccountInfoUpdateHashCode);
			}
		}

		kyclDaoImpl.insertIntoKyclApprovedTable(kyclId);
		// Long accountLId =
		// kyclDaoImpl.getAccountLegalIdByScreeningId(kycl.getLegal().getScreeningId());
		// kyclDaoImpl.updateAccountLegalWorkflow(accountLId);
		/// MORE COMMENT NEEDED// DANGEROUS LOGIC
		
		kyclDaoImpl.updateScreeningLegalWorkflow(kycl.getLegal().getScreeningId());
		kyclDaoImpl.deleteCardSubscribedIndividual(kyclId);
		for (String individual : kycl.getFinancialInfo().getIndividualCardSubscribed()) {
			if (!kyclDaoImpl.checkIfSubscribedIndividualExists(individual, kyclId)) {
				kyclDaoImpl.insertCardSubscribedIndividual(individual, kyclId, "");
				kyclDaoImpl.insertCardSubscribedIndividualUpdateTable(individual, kyclId, kycl.getUser(), "");
			}
		}

		kyclDaoImpl.deleteServiceSubscribedIndividual(kyclId);
		for (String service : kycl.getFinancialInfo().getIndividualServiceSubscribed()) {
			if (!kyclDaoImpl.checkIfSubscribedServiceExists(service, kyclId)) {
				kyclDaoImpl.insertServiceSubscribedIndividual(service, kyclId,"");
				kyclDaoImpl.insertServiceSubscribedIndividualUpdateTable(service, kyclId, kycl.getUser(), "");
			}
		}
		
		kyclDaoImpl.updateCommentTableInCaseOfFoward(kyclId, kycl.getLegal().getScreeningId());

		kyclDaoImpl.updateScreeningLegalWorkflowTable(kycl.getLegal().getScreeningId(), true);
		return kyclId;
	}

	/**
	 * @param jsonString
	 * @throws Exception
	 */
	public long udateKycl(String jsonString) throws Exception {
		Kycl kycl = new ObjectMapper().readValue(jsonString, Kycl.class);
		boolean includeUser = true;
		boolean doNotIncludeUser = false;

		String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kycl.getLegal(), doNotIncludeUser,
				kycl.getUser());
		String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kycl.getLegal(), includeUser,
				kycl.getUser());
		long kyclId = kycl.getLegal().getId();
		if (kycl.getLegal().isChange()) {
			kyclDaoImpl.updateKyclInfo(kycl.getLegal(), kyclHashCode);
			kyclDaoImpl.saveLegalInfoInUpdateTable(kycl.getLegal(), kycl.getUser(), kyclUpdateHashCode);
		}

		for (AccountInfo accountInfo : kycl.getListAccountInfo()) {
			long kycnID = 0;
			if (accountInfo.getId() != 0) {
				if (accountInfo.isChange()) {
					String accountUpdateHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo,
							kyclId, includeUser, kycl.getUser());
					String accountHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo, kyclId,
							doNotIncludeUser, kycl.getUser());
					kyclDaoImpl.updateAccountInfo(accountInfo,kycl.getUser(), kyclId, accountHashCode);
					kyclDaoImpl.saveAccountInfoInUpdateTable(accountInfo, kyclId, kycl.getUser(),
							accountUpdateHashCode);
				}
				if (accountInfo.getAccountsRelatedPersonList() != null
						&& !(accountInfo.getAccountsRelatedPersonList().isEmpty())) {
					for (RelatedPerson relatedPerson : accountInfo.getAccountsRelatedPersonList()) {
						relatedPerson.setAccountNo(accountInfo.getAccountNo());
						if (relatedPerson.getId() != 0) {
							if (relatedPerson.isChange() || accountInfo.isChange() || relatedPerson.getKycnId() == 0) {

								/**
								 * Save separate new KYCN of added new related
								 * person
								 **/
								if (relatedPerson.getKycnId() <= 0) {
									long newRelatedPersonKYCNId = 0;
									if (relatedPerson.getPersonType().equalsIgnoreCase("Director") && relatedPerson.getIsDirector().equals("true")) {
										newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
												kycl.getUser());
										newRelatedPersonKYCNId=relatedPerson.setKycnId(newRelatedPersonKYCNId);
										kycnID=newRelatedPersonKYCNId;

									}else if(relatedPerson.getIsDirector().equals("false")){
										newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
												kycl.getUser());
										relatedPerson.setKycnId(newRelatedPersonKYCNId);
										kycnID=newRelatedPersonKYCNId;
									}else{
										relatedPerson.setKycnId(kycnID);
									}

								}
								/***/

								String relatedPersonHashCode = hashCodeGenerator
										.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId,
												doNotIncludeUser, kycl.getUser());
								String relatedPersonUpdateHashCode = hashCodeGenerator
										.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser,
												kycl.getUser());
								kyclDaoImpl.updateRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
								kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
										relatedPersonUpdateHashCode);
							}
						} else {
							String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
									relatedPerson, kyclId, doNotIncludeUser, kycl.getUser());
							String relatedPersonUpdateHashCode = hashCodeGenerator
									.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser,
											kycl.getUser());

							/**
							 * Save separate new KYCN of added new related
							 * person
							 **/
							if (relatedPerson.getKycnId() <= 0) {
								long newRelatedPersonKYCNId = 0;
								if (relatedPerson.getPersonType().equalsIgnoreCase("Director") && relatedPerson.getIsDirector().equals("true")) {
									newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
											kycl.getUser());
									newRelatedPersonKYCNId=relatedPerson.setKycnId(newRelatedPersonKYCNId);
									kycnID=newRelatedPersonKYCNId;

								}else if(relatedPerson.getIsDirector().equals("false")){
									newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
											kycl.getUser());
									relatedPerson.setKycnId(newRelatedPersonKYCNId);
									kycnID=newRelatedPersonKYCNId;
								}else{
									relatedPerson.setKycnId(kycnID);
								}
							}
							/***/

							kyclDaoImpl.saveRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
							kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
									relatedPersonUpdateHashCode);
						}
					}
				}

			} else {

				if (accountInfo.getAccountsRelatedPersonList() != null
						&& !(accountInfo.getAccountsRelatedPersonList().isEmpty())) {
					for (RelatedPerson relatedPerson : accountInfo.getAccountsRelatedPersonList()) {
						relatedPerson.setAccountNo(accountInfo.getAccountNo());
						if (relatedPerson.getId() != 0) {
							if (relatedPerson.isChange() || accountInfo.isChange() || relatedPerson.getKycnId() == 0) {

								/**
								 * Save separate new KYCN of added new related
								 * person
								 **/
								if (relatedPerson.getKycnId() <= 0) {
									long newRelatedPersonKYCNId = 0;
									if (relatedPerson.getPersonType().equalsIgnoreCase("Director") && relatedPerson.getIsDirector().equals("true")) {
										newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
												kycl.getUser());
										newRelatedPersonKYCNId=relatedPerson.setKycnId(newRelatedPersonKYCNId);
										kycnID=newRelatedPersonKYCNId;

									}else if(relatedPerson.getIsDirector().equals("false")){
										newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
												kycl.getUser());
										relatedPerson.setKycnId(newRelatedPersonKYCNId);
										kycnID=newRelatedPersonKYCNId;
									}else{
										relatedPerson.setKycnId(kycnID);
									}

								}
								/***/

								String relatedPersonHashCode = hashCodeGenerator
										.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId,
												doNotIncludeUser, kycl.getUser());
								String relatedPersonUpdateHashCode = hashCodeGenerator
										.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser,
												kycl.getUser());
								kyclDaoImpl.updateRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
								kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
										relatedPersonUpdateHashCode);
							}
						} else {
							String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
									relatedPerson, kyclId, doNotIncludeUser, kycl.getUser());
							String relatedPersonUpdateHashCode = hashCodeGenerator
									.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser,
											kycl.getUser());

							/**
							 * Save separate new KYCN of added new related
							 * person
							 **/
							if (relatedPerson.getKycnId() <= 0) {
								long newRelatedPersonKYCNId = 0;
								if (relatedPerson.getPersonType().equalsIgnoreCase("Director") && relatedPerson.getIsDirector().equals("true")) {
									newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
											kycl.getUser());
									newRelatedPersonKYCNId=relatedPerson.setKycnId(newRelatedPersonKYCNId);
									kycnID=newRelatedPersonKYCNId;

								}else if(relatedPerson.getIsDirector().equals("false")){
									newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson,
											kycl.getUser());
									relatedPerson.setKycnId(newRelatedPersonKYCNId);
									kycnID=newRelatedPersonKYCNId;
								}else{
									relatedPerson.setKycnId(kycnID);
								}
							}
							/***/

							kyclDaoImpl.saveRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
							kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
									relatedPersonUpdateHashCode);
						}
					}
				}

				String accountHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo, kyclId,
						doNotIncludeUser, kycl.getUser());
				String accountUpdateHashCode = hashCodeGenerator.generateHashValueForAccountInfo(accountInfo, kyclId,
						includeUser, kycl.getUser());
				kyclDaoImpl.saveAccountInfo(accountInfo, kyclId, accountHashCode);
				kyclDaoImpl.saveAccountInfoInUpdateTable(accountInfo, kyclId, kycl.getUser(), accountUpdateHashCode);
			}
		}
		for (Address address : kycl.getListAddress()) {
			if (address.getId() != 0) {
				if (address.isChange()) {
					String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							doNotIncludeUser, kycl.getUser());
					String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							includeUser, kycl.getUser());
					kyclDaoImpl.updateAddress(address, kyclId, addressHashCode,kycl.getUser());
					kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, kycl.getUser(), addressUpdateHashCode);
				}
			} else {
				String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
						doNotIncludeUser, kycl.getUser());
				String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
						includeUser, kycl.getUser());
				kyclDaoImpl.saveAddress(address, kyclId, addressHashCode, kycl.getUser());
				kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, kycl.getUser(), addressUpdateHashCode);
			}
		}
		for (AuditInfo auditInfo : kycl.getListAuditInfo()) {
			if (auditInfo.getId() != 0) {
				if (auditInfo.isChange()) {
					String auditInfoHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
							doNotIncludeUser, kycl.getUser());
					String auditInfoUpdateHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
							includeUser, kycl.getUser());
					kyclDaoImpl.updateAuditInfo(auditInfo, kyclId, auditInfoHashCode);
					kyclDaoImpl.saveAuditInfoInUpdateTable(auditInfo, kyclId, kycl.getUser(), auditInfoUpdateHashCode);
				}
			} else {
				String auditInfoHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
						doNotIncludeUser, kycl.getUser());
				String auditInfoUpdateHashCode = hashCodeGenerator.generateHashValueForAuditInfo(auditInfo, kyclId,
						includeUser, kycl.getUser());
				kyclDaoImpl.saveAuditInfo(auditInfo, kyclId, auditInfoHashCode);
				kyclDaoImpl.saveAuditInfoInUpdateTable(auditInfo, kyclId, kycl.getUser(), auditInfoUpdateHashCode);
			}
		}
		for (BusinessInfo businessInfo : kycl.getListBusinessInfo()) {
			if (businessInfo.getId() != 0) {
				if (businessInfo.isChange()) {
					String businessInfoUpdateHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo,
							kyclId, includeUser, kycl.getUser());
					String businessInfoHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo,
							kyclId, doNotIncludeUser, kycl.getUser());

					kyclDaoImpl.updateBusinessInfo(businessInfo, kyclId, businessInfoHashCode);
					kyclDaoImpl.saveBusinessInfoInUpdateTable(businessInfo, kyclId, kycl.getUser(),
							businessInfoUpdateHashCode);
				}
			} else {
				String businessInfoHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo, kyclId,
						doNotIncludeUser, kycl.getUser());
				String businessInfoUpdateHashCode = hashCodeGenerator.generateHashValuForBusinessInfo(businessInfo,
						kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.saveBusinessInfo(businessInfo, kyclId, businessInfoHashCode);
				kyclDaoImpl.saveBusinessInfoInUpdateTable(businessInfo, kyclId, kycl.getUser(),
						businessInfoUpdateHashCode);
			}
		}

		for (DocumentStatus documentStatus : kycl.getListDocumentStatus()) {
			if (documentStatus.getId() != 0) {
				if (documentStatus.isChange()) {
					String documentStatusHashCode = hashCodeGenerator.generateHashValueForDocumentStatus(documentStatus,
							kyclId, doNotIncludeUser, kycl.getUser());
					String documentStatusUpdateHashCode = hashCodeGenerator
							.generateHashValueForDocumentStatus(documentStatus, kyclId, includeUser, kycl.getUser());
					kyclDaoImpl.updateDocumentStatus(documentStatus, kyclId, documentStatusHashCode);
					kyclDaoImpl.saveDocumentStatusInUpdateTable(documentStatus, kyclId, kycl.getUser(),
							documentStatusUpdateHashCode);
				}
			} else {
				String documentStatusHashCode = hashCodeGenerator.generateHashValueForDocumentStatus(documentStatus,
						kyclId, doNotIncludeUser, kycl.getUser());
				String documentStatusUpdateHashCode = hashCodeGenerator
						.generateHashValueForDocumentStatus(documentStatus, kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.saveDocumentStatus(documentStatus, kyclId, documentStatusHashCode);
				kyclDaoImpl.saveDocumentStatusInUpdateTable(documentStatus, kyclId, kycl.getUser(),
						documentStatusUpdateHashCode);
			}
		}
		for (LandlordInfo landlordInfo : kycl.getListLandlordAddress()) {
			if (landlordInfo.getId() != 0) {
				if (landlordInfo.isChange()) {
					String landlordInfoHashCode = hashCodeGenerator.generateHashValueForLandlordStatus(landlordInfo,
							kyclId, doNotIncludeUser, kycl.getUser());
					String landlordInfoUpdateHashCode = hashCodeGenerator
							.generateHashValueForLandlordStatus(landlordInfo, kyclId, includeUser, kycl.getUser());
					kyclDaoImpl.updateLandlordInfo(landlordInfo, kyclId, landlordInfoHashCode);
					kyclDaoImpl.saveLandlordInfoInUpdateTable(landlordInfo, kyclId, kycl.getUser(),
							landlordInfoUpdateHashCode);
				}
			} else {
				String landlordInfoHashCode = hashCodeGenerator.generateHashValueForLandlordStatus(landlordInfo, kyclId,
						doNotIncludeUser, kycl.getUser());
				String landlordInfoUpdateHashCode = hashCodeGenerator.generateHashValueForLandlordStatus(landlordInfo,
						kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.saveLandlordInfo(landlordInfo, kyclId, landlordInfoHashCode);
				kyclDaoImpl.saveLandlordInfoInUpdateTable(landlordInfo, kyclId, kycl.getUser(),
						landlordInfoUpdateHashCode);
			}
		}
		for (RegistrationAddress registrationAddress : kycl.getListRegistrationAddress()) {
			if (registrationAddress.getId() != 0) {
				if (registrationAddress.isChange()) {
					String registrationAddressHashCode = hashCodeGenerator
							.generateHashValueForRegistrationAddressStatus(registrationAddress, kyclId,
									doNotIncludeUser, kycl.getUser());
					String registrationAddressUpdateHashCode = hashCodeGenerator
							.generateHashValueForRegistrationAddressStatus(registrationAddress, kyclId, includeUser,
									kycl.getUser());
					kyclDaoImpl.updateRegistrationAddress(registrationAddress, kyclId, registrationAddressHashCode);
					kyclDaoImpl.saveRegistrationAddressInUpdateTable(registrationAddress, kyclId, kycl.getUser(),
							registrationAddressUpdateHashCode);
				}
			} else {
				String registrationAddressHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
						registrationAddress, kyclId, doNotIncludeUser, kycl.getUser());
				String registrationAddressUpdateHashCode = hashCodeGenerator
						.generateHashValueForRegistrationAddressStatus(registrationAddress, kyclId, includeUser,
								kycl.getUser());

				kyclDaoImpl.saveRegistrationAddress(registrationAddress, kyclId, registrationAddressHashCode);
				kyclDaoImpl.saveRegistrationAddressInUpdateTable(registrationAddress, kyclId, kycl.getUser(),
						registrationAddressUpdateHashCode);
			}
		}
		for (RegistrationInfo registrationInfo : kycl.getListRegistrationInfo()) {
			if (registrationInfo.getId() != 0) {
				if (registrationInfo.isChange()) {
					String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
							registrationInfo, kyclId, doNotIncludeUser, kycl.getUser());
					String registrationInfoUpdateHashCode = hashCodeGenerator
							.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId, includeUser,
									kycl.getUser());
					kyclDaoImpl.updateRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
					kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId, kycl.getUser(),
							registrationInfoUpdateHashCode);
				}
			} else {
				String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
						registrationInfo, kyclId, doNotIncludeUser, kycl.getUser());
				String registrationInfoUpdateHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
						registrationInfo, kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
				kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId, kycl.getUser(),
						registrationInfoUpdateHashCode);
			}
		}
		for (RelatedEntity relatedEntity : kycl.getListRelatedEntityInfo()) {
			if (relatedEntity.getId() != 0) {
				if (relatedEntity.isChange()) {
					String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntity, kyclId, doNotIncludeUser, kycl.getUser());
					String relatedEntityUpdateHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntity, kyclId, includeUser, kycl.getUser());
					kyclDaoImpl.updateRelatedEntityInfo(relatedEntity, kyclId, relatedEntityHashCode);
					kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntity, kyclId, kycl.getUser(),
							relatedEntityUpdateHashCode);
				}
			} else {
				String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(relatedEntity,
						kyclId, doNotIncludeUser, kycl.getUser());
				String relatedEntityUpdateHashCode = hashCodeGenerator
						.generateHashValueForRelatedEntityStatus(relatedEntity, kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.saveRelatedEntityInfo(relatedEntity, kyclId, relatedEntityHashCode);
				kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntity, kyclId, kycl.getUser(),
						relatedEntityUpdateHashCode);
			}
		}
		for (RelatedPerson relatedPerson : kycl.getListRelatedPerson()) {
			if (relatedPerson.getId() != 0) {
				if (relatedPerson.isChange()) {
					
					/** Save separate new KYCN of added new related person **/
					if(relatedPerson.getKycnId()<=0||relatedPerson.getKycnId()==0){
						long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson, kycl.getUser());
						relatedPerson.setKycnId(newRelatedPersonKYCNId);
					}
					/***/
					
					
					String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
							relatedPerson, kyclId, doNotIncludeUser, kycl.getUser());
					String relatedPersonUpdateHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(
							relatedPerson, kyclId, includeUser, kycl.getUser());
					kyclDaoImpl.updateRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
					kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
							relatedPersonUpdateHashCode);
				}
			} else {
				String relatedPersonHashCode = hashCodeGenerator.generateHashValueForRelatedPersonStatus(relatedPerson,
						kyclId, doNotIncludeUser, kycl.getUser());
				String relatedPersonUpdateHashCode = hashCodeGenerator
						.generateHashValueForRelatedPersonStatus(relatedPerson, kyclId, includeUser, kycl.getUser());
				/** Save separate new KYCN of added new related person **/
				if (relatedPerson.getKycnId() <= 0) {
					long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(relatedPerson, kycl.getUser());
					relatedPerson.setKycnId(newRelatedPersonKYCNId);
				}
				/***/

				kyclDaoImpl.saveRelatedPerson(relatedPerson, kyclId, relatedPersonHashCode);
				kyclDaoImpl.saveRelatedPersonInUpdateTable(relatedPerson, kyclId, kycl.getUser(),
						relatedPersonUpdateHashCode);
			}
		}
		if (kycl.getComplianceInfo().getId() != 0) {
			if (kycl.getComplianceInfo().isChange()) {
				String complainceInfoHashCode = hashCodeGenerator.generateHashValueForComplainceInfo(
						kycl.getComplianceInfo(), kyclId, doNotIncludeUser, kycl.getUser());
				String complainceInfoUpdateHashCode = hashCodeGenerator.generateHashValueForComplainceInfo(
						kycl.getComplianceInfo(), kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.updateComplainceInfo(kycl.getComplianceInfo(), kyclId, complainceInfoHashCode);
				kyclDaoImpl.saveComplainceInfoInUpdateTable(kycl.getComplianceInfo(), kyclId, kycl.getUser(),
						complainceInfoUpdateHashCode);
			}
		} else {
			String complainceInfoHashCode = hashCodeGenerator.generateHashValueForComplainceInfo(
					kycl.getComplianceInfo(), kyclId, doNotIncludeUser, kycl.getUser());

			String complainceInfoUpdateHashCode = hashCodeGenerator
					.generateHashValueForComplainceInfo(kycl.getComplianceInfo(), kyclId, includeUser, kycl.getUser());
			kyclDaoImpl.saveComplainceInfo(kycl.getComplianceInfo(), kyclId, complainceInfoHashCode);
			kyclDaoImpl.saveComplainceInfoInUpdateTable(kycl.getComplianceInfo(), kyclId, kycl.getUser(),
					complainceInfoUpdateHashCode);
		}
		if (kycl.getFinancialInfo().getId() != 0) {
			if (kycl.getFinancialInfo().isChange()) {
				String financialInfoHashCode = hashCodeGenerator.generateHashValueForFinancialInfo(
						kycl.getFinancialInfo(), kyclId, doNotIncludeUser, kycl.getUser());

				String financialInfoUpdateHashCode = hashCodeGenerator.generateHashValueForFinancialInfo(
						kycl.getFinancialInfo(), kyclId, includeUser, kycl.getUser());
				kyclDaoImpl.updateFinancialInfo(kycl.getFinancialInfo(), kyclId, financialInfoHashCode);
				kyclDaoImpl.saveFinancialInfoInUpdateTable(kycl.getFinancialInfo(), kyclId, kycl.getUser(),
						financialInfoUpdateHashCode);
			}
		} else {
			String financialInfoUpdateHashCode = hashCodeGenerator
					.generateHashValueForFinancialInfo(kycl.getFinancialInfo(), kyclId, includeUser, kycl.getUser());
			String financialInfoHashCode = hashCodeGenerator.generateHashValueForFinancialInfo(kycl.getFinancialInfo(),
					kyclId, doNotIncludeUser, kycl.getUser());
			kyclDaoImpl.saveFinancialInfo(kycl.getFinancialInfo(), kyclId, financialInfoHashCode);
			kyclDaoImpl.saveFinancialInfoInUpdateTable(kycl.getFinancialInfo(), kyclId, kycl.getUser(),
					financialInfoUpdateHashCode);
		}

		// Long accountLId =
		// kyclDaoImpl.getAccountLegalIdByScreeningId(kycl.getLegal().getScreeningId());
		kyclDaoImpl.updateScreeningLegalWorkflow(kycl.getLegal().getScreeningId());
		// for (String individual :
		// kycl.getFinancialInfo().getIndividualCardSubscribed()) {
		// if (!kyclDaoImpl.checkIfSubscribedIndividualExists(individual,
		// kyclId)) {
		// kyclDaoImpl.updateCardSubscribedIndividual(individual, kyclId);
		// kyclDaoImpl.insertCardSubscribedIndividualUpdateTable(individual,
		// kyclId, kycl.getUser());
		// }
		// }
		// for (String service :
		// kycl.getFinancialInfo().getIndividualServiceSubscribed()) {
		// if (!kyclDaoImpl.checkIfSubscribedServiceExists(service, kyclId)) {
		// kyclDaoImpl.updateServiceSubscribedIndividual(service, kyclId);
		// kyclDaoImpl.insertServiceSubscribedIndividualUpdateTable(service,
		// kyclId, kycl.getUser());
		// }
		// }
		return kyclId;

	}

	private long insertNewKycOfRelatedPersonAddedInKYC(RelatedPerson kycnRelatedPersonInfo, User user)
			throws Exception {
		KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
		kycnPersonalInfo.setFirstName(kycnRelatedPersonInfo.getFirstName());
		kycnPersonalInfo.setMiddleName(kycnRelatedPersonInfo.getMiddleName());
		kycnPersonalInfo.setLastName(kycnRelatedPersonInfo.getLastName());
		kycnPersonalInfo.setCustomerId(kycnRelatedPersonInfo.getCustId());
		kycnPersonalInfo.setSalutation(kycnRelatedPersonInfo.getSalutation());
		kycnPersonalInfo.setLsfName(kycnRelatedPersonInfo.getLsfName());
		kycnPersonalInfo.setLsmName(kycnRelatedPersonInfo.getLsmName());
		kycnPersonalInfo.setLslName(kycnRelatedPersonInfo.getLslName());
		kycnPersonalInfo.setSecondName(kycnRelatedPersonInfo.getSecondName());
		kycnPersonalInfo.setCalledByName(kycnRelatedPersonInfo.getCalledByName());
		kycnPersonalInfo.setNotes(kycnRelatedPersonInfo.getNotes());
		kycnPersonalInfo.setPrimary(false);

		KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
		kycnIdentificationInfo
				.setPrimaryIdentificationDocumentNo(kycnRelatedPersonInfo.getPrimaryIdentificationDocumentNo());
		kycnIdentificationInfo
				.setPrimaryIdentificationDocumentType(kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType());
		kycnIdentificationInfo.setCountryOfIssue(kycnRelatedPersonInfo.getCountry());
		kycnIdentificationInfo.setIssuingAuthority(kycnRelatedPersonInfo.getIssuingAuthority());
		kycnIdentificationInfo.setPlaceOfIssue(kycnRelatedPersonInfo.getPlaceOfIssue());

		String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(kycnPersonalInfo,
				user, includeUser);
		String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
				.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, includeUser);

		Long kycnId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo, user, hashValueForKycnPersonInfo);
		kycnPersonalInfo.setId(kycnId);
		kycnDaoImpl.insertIntoKycnPersonalInfoUpdateTable(kycnPersonalInfo, user, hashValueForKycnPersonInfoUpdate);
		String hashValueForKycnIdentificationInfo = hashCodeGenerator
				.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, doNotIncludeUser, kycnId);
		String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
				.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, includeUser, kycnId);
		kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnId, hashValueForKycnIdentificationInfo,
				user);
		kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnId,
				hashValueForKycnIdentificationInfoUpdate, user);

		return kycnId;

	}

	private Long insertNewRelatedEntityAddedInKYC(RelatedEntity kyclRelatedEntityInfo, User user) throws Exception {

		Legal kyclInfo = new Legal();
		kyclInfo.setNameOfTheInstitution(kyclRelatedEntityInfo.getName());
		kyclInfo.setCustId(kyclRelatedEntityInfo.getCustId());
		kyclInfo.setLsName(kyclRelatedEntityInfo.getLsName());
		kyclInfo.setNotes(kyclRelatedEntityInfo.getNotes());
		kyclInfo.setPrimary(false);
		
		Address kyclAddress = new Address();
		kyclAddress.setAddressType("Permanent");
		kyclAddress.setDistrict(kyclRelatedEntityInfo.getDistrict());
		kyclAddress.setCountry(kyclRelatedEntityInfo.getCountry());
		kyclAddress.setZone(kyclRelatedEntityInfo.getZone());
		kyclAddress.setProvince(kyclRelatedEntityInfo.getProvince());
		kyclAddress.setMnVdc(kyclRelatedEntityInfo.getMnVdc());
		kyclAddress.setWardNumber(kyclRelatedEntityInfo.getWardNumber());
		kyclAddress.setPinzip(kyclRelatedEntityInfo.getPinzip());
		kyclAddress.setToleArea(kyclRelatedEntityInfo.getToleArea());
		kyclAddress.setStreet(kyclRelatedEntityInfo.getStreet());
		kyclAddress.setHouseNo(kyclRelatedEntityInfo.getHouseNo());
		kyclAddress.setUnitNumber(kyclRelatedEntityInfo.getUnitNumber());
		kyclAddress.setNearestLandmark(kyclRelatedEntityInfo.getNearestLandmark());
		kyclAddress.setLatitude(kyclRelatedEntityInfo.getLatitude());
		kyclAddress.setLongitude(kyclRelatedEntityInfo.getLongitude());
		kyclAddress.setPhoneNoAreaCode(kyclRelatedEntityInfo.getPhoneNoAreaCode());
		kyclAddress.setPhoneNoCountryCode(kyclRelatedEntityInfo.getPhoneNoCountryCode());
		kyclAddress.setPhoneNo(kyclRelatedEntityInfo.getPhoneNo());
		kyclAddress.setTelexNoAreaCode(kyclRelatedEntityInfo.getTelexNoAreaCode());
		kyclAddress.setTelexNoCountryCode(kyclRelatedEntityInfo.getTelexNoCountryCode());
		kyclAddress.setTelexNo(kyclRelatedEntityInfo.getTelexNo());
		RegistrationInfo registrationInfo = new RegistrationInfo();
		registrationInfo.setRegdNumber(kyclRelatedEntityInfo.getRegistrationNo());

		String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kyclInfo, doNotIncludeUser, user);
		String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kyclInfo, includeUser, user);

		Long kyclId = kyclDaoImpl.saveLegalInfo(kyclInfo, user, kyclHashCode);
		kyclDaoImpl.saveLegalInfoInUpdateTable(kyclInfo, user, kyclUpdateHashCode);

		String addressHashCode = hashCodeGenerator.generateHashValueForAddress(kyclAddress, kyclId, doNotIncludeUser,
				user);
		String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(kyclAddress, kyclId, includeUser,
				user);
		kyclDaoImpl.saveAddress(kyclAddress, kyclId, addressHashCode, user);
		kyclDaoImpl.saveAddressInUpdateTable(kyclAddress, kyclId, user, addressUpdateHashCode);

		String registrationInfoHashCode = hashCodeGenerator
				.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId, doNotIncludeUser, user);
		String registrationInfoUpdateHashCode = hashCodeGenerator
				.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId, includeUser, user);
		kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
		kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId, user, registrationInfoUpdateHashCode);

		return kyclId;
	}

}
