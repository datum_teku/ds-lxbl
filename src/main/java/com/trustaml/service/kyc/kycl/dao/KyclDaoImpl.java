package com.trustaml.service.kyc.kycl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycl.model.AccountCards;
import com.trustaml.service.kyc.kycl.model.AccountInfo;
import com.trustaml.service.kyc.kycl.model.AccountService;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.AuditInfo;
import com.trustaml.service.kyc.kycl.model.BusinessInfo;
import com.trustaml.service.kyc.kycl.model.ComplianceInfo;
import com.trustaml.service.kyc.kycl.model.DocumentStatus;
import com.trustaml.service.kyc.kycl.model.FinancialInfo;
import com.trustaml.service.kyc.kycl.model.LandlordInfo;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationAddress;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycl.model.RelatedEntity;
import com.trustaml.service.kyc.kycl.model.RelatedPerson;

public class KyclDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @param legal
	 * @param kyclHashCode
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public Long saveLegalInfo(Legal legal, User user, String kyclHashCode) throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_info"
				+ "(primary_sol_id,salutation,name_of_the_institution,ls_name,cust_short_name,date_of_establishment,"
				+ "notes,customer_type,customer_group,customer_constitution,customer_community,customer_employee_id,"
				+ "customer_open_date,customer_maker,screening_id,pan_number,minor,"
				+ " customer_status_code,card_holder,non_resident_external,cust_id,kycl_status,last_update_date,last_screened_date,"
				+ "verified_record,audited_fiscal_year,net_profit,net_loss,tax_clearence_certificate_date,tax_clearence_fiscal_year,"
				+ "next_clearence_certificate_date,taxable_amount,taxable_income,tax_paid,tax_paid_date,types_of_industry,hash,date_of_establishment_bs,is_updated,goaml_legal_form_type,incomplete,"
				+ "incorporation_country,incorporation_country_code,incorporation_province,business_description,is_active,modified_date,is_primary) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		Long legalId = 0L;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, legal.getPrimarySolId());
			ps.setString(2, legal.getSalutation());
			ps.setString(3, legal.getNameOfTheInstitution());
			ps.setString(4, legal.getLsName());
			ps.setString(5, legal.getCustShortName());
			if (legal.getDateOfEstablishment() != null && !legal.getDateOfEstablishment().equals("")) {
				ps.setDate(6, getBirthDate(legal.getDateOfEstablishment()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, legal.getNotes());
			ps.setString(8, legal.getCustomerType());
			ps.setString(9, legal.getCustomerGroup());
			ps.setString(10, legal.getCustomerConstitution());
			ps.setString(11, legal.getCustomerCommunity());
			ps.setString(12, legal.getCustomerEmployeeId());
			if (legal.getCustomerOpenDate() != null && !legal.getCustomerOpenDate().equals("")) {
				ps.setDate(13, getBirthDate(legal.getCustomerOpenDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, legal.getCustomerMaker());
			ps.setLong(15, legal.getScreeningId());
			ps.setString(16, legal.getPanNumber());
			ps.setBoolean(17, legal.isMinor());
			ps.setString(18, legal.getCustomerStatusCode());
			ps.setBoolean(19, legal.isCardHolder());
			ps.setBoolean(20, legal.isNonResidentExternal());
			ps.setString(21, legal.getCustId());
			ps.setString(22, legal.getKyclStatus());
			if (legal.getLastUpdateDate() != null && !legal.getLastUpdateDate().equals("")) {
				ps.setDate(23, getBirthDate(legal.getLastUpdateDate()));
			} else {
				ps.setNull(23, java.sql.Types.DATE);
			}
			if (legal.getLastScreenedDate() != null && !legal.getLastScreenedDate().equals("")) {
				ps.setDate(24, getBirthDate(legal.getLastScreenedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setBoolean(25, legal.isVerifiedRecord());
			ps.setString(26, legal.getAuditedFiscalYear());
			ps.setString(27, legal.getNetProfit());
			ps.setString(28, legal.getNetLoss());
			if (legal.getTaxClearenceCertificateDate() != null && !legal.getTaxClearenceCertificateDate().equals("")) {
				ps.setDate(29, getBirthDate(legal.getTaxClearenceCertificateDate()));
			} else {
				ps.setNull(29, java.sql.Types.DATE);
			}
			if (legal.getTaxClearenceFiscalYear() != null && !legal.getTaxClearenceFiscalYear().equals("")) {
				ps.setDate(30, getBirthDate(legal.getTaxClearenceFiscalYear()));
			} else {
				ps.setNull(30, java.sql.Types.DATE);
			}
			if (legal.getNextClearenceCertificateDate() != null
					&& !legal.getNextClearenceCertificateDate().equals("")) {
				ps.setDate(31, getBirthDate(legal.getNextClearenceCertificateDate()));
			} else {
				ps.setNull(31, java.sql.Types.DATE);
			}
			ps.setString(32, legal.getTaxableAmount());
			ps.setString(33, legal.getTaxableIncome());
			ps.setString(34, legal.getTaxPaid());
			if (legal.getTaxPaidDate() != null && !legal.getTaxPaidDate().equals("")) {
				ps.setDate(35, getBirthDate(legal.getTaxPaidDate()));
			} else {
				ps.setNull(35, java.sql.Types.DATE);
			}
			ps.setString(36, legal.getTypeOfIndustry());
			ps.setString(37, kyclHashCode);
			ps.setDate(38, legal.getDateOfEstablishmentBS());
			ps.setBoolean(39, true);
			ps.setString(40, legal.getGoAMLCustConst());
			ps.setBoolean(41, legal.isIncomplete());
			ps.setString(42, legal.getIncorporationCountry());
			ps.setString(43, legal.getGoAMLIncorporationCountry());
			ps.setString(44, legal.getIncorporationProvince());
			ps.setString(45, legal.getBusinessDescription());
			ps.setBoolean(46, true);
			ps.setDate(47, new java.sql.Date(System.currentTimeMillis()));
			ps.setBoolean(48, legal.isPrimary());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				legalId = rs.getLong(1);
			}

		} finally {
			con.close();
			ps.close();
		}
		return legalId;

	}

	/**
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * @param address
	 * @param kyclId
	 * @param addressHashCode
	 * @throws SQLException
	 */
	public void saveAddress(Address address, long kyclId, String addressHashCode, User user) throws SQLException {
		String sql = "INSERT INTO kycl_address_info(kycl_info_id,address_type,country,zone,district,mn_vdc,"
				+ "pinzip,ward_number,tole_area,street,house_no,unit_number,"
				+ " nearest_landmark,latitude,longitude,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,"
				+ "telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,email_id,notes,state,hash,province,"
				+ "goaml_address_type,address,city,goaml_country,is_active,address_removed_by) "
				+ "VALUES (?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, address.getAddressType());
			ps.setString(3, address.getCountry());
			ps.setString(4, address.getZone());
			ps.setString(5, address.getDistrict());
			ps.setString(6, address.getMnVdc());
			ps.setString(7, address.getPinzip());
			ps.setString(8, address.getWardNumber());
			ps.setString(9, address.getToleArea());
			ps.setString(10, address.getStreet());
			ps.setString(11, address.getHouseNo());
			ps.setString(12, address.getUnitNumber());
			ps.setString(13, address.getNearestLandmark());
			ps.setString(14, address.getLatitude());
			ps.setString(15, address.getLongitude());
			ps.setString(16, address.getPhoneNoCountryCode());
			ps.setString(17, address.getPhoneNoAreaCode());
			ps.setString(18, address.getPhoneNo());
			ps.setString(19, address.getTelexNoCountryCode());
			ps.setString(20, address.getTelexNoAreaCode());
			ps.setString(21, address.getTelexNo());
			ps.setString(22, address.getPagerNoCountryCode());
			ps.setString(23, address.getPagerNoAreaCode());
			ps.setString(24, address.getPagerNo());
			ps.setString(25, address.getEmailId());
			ps.setString(26, address.getNotes());
			ps.setString(27, address.getState());
			ps.setString(28, addressHashCode);
			ps.setString(29, address.getProvince());
			ps.setString(30, address.getGoAMLAddressType());
			ps.setString(31, address.getAddress());
			ps.setString(32, address.getCity());
			ps.setString(33, address.getGoAMLCountryType());
			ps.setBoolean(34, Boolean.parseBoolean(address.getRemovedAddressStatus()));
			ps.setString(35, user.getUserName());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param auditInfo
	 * @param kyclId
	 * @param auditHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveAuditInfo(AuditInfo auditInfo, long kyclId, String auditHashCode)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_audit_info"
				+ "(kycl_info_id,audited_report,authorized_letter,authorized_letter_date,authorized_letter_agency,notes,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, auditInfo.getAuditedReport());
			ps.setString(3, auditInfo.getAuthorizedLetter());
			if (!auditInfo.getAuthorizedLetterDate().equals("")) {
				ps.setDate(4, getBirthDate(auditInfo.getAuthorizedLetterDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			ps.setString(5, auditInfo.getAuthorizedLetterAgency());
			ps.setString(6, auditInfo.getNotes());
			ps.setString(7, auditHashCode);
			ps.setBoolean(8, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param complianceInfo
	 * @param kyclId
	 * @param hashCode
	 * @throws SQLException
	 */
	public void saveComplainceInfo(ComplianceInfo complianceInfo, long kyclId, String hashCode) throws SQLException {
		String sql = "INSERT INTO kycl_compliance_info"
				+ "(kycl_info_id,level_of_compliance_on_aml,level_of_compliance_on_tax,level_of_compliance_on_corruption,level_of_compliance_on_others,notes,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, complianceInfo.getLevelOfComplianceOnAml());
			ps.setString(3, complianceInfo.getLevelOfComplianceOnTax());
			ps.setString(4, complianceInfo.getLevelOfComplianceOnCorruption());
			ps.setString(5, complianceInfo.getLevelOfComplianceOnOthers());
			ps.setString(6, complianceInfo.getNotes());
			ps.setString(7, hashCode);
			ps.setBoolean(8, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}
	}

	/**
	 * @param documentStatus
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveDocumentStatus(DocumentStatus documentStatus, long kyclId, String hashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_document_status"
				+ "(kycl_info_id,document_type,document_status,appliction_submitted_date,refresh_date,notes,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, documentStatus.getDocumentType());
			ps.setString(3, documentStatus.getDocumentStatus());
			if (!documentStatus.getApplictionSubmittedDate().equals("")) {
				ps.setDate(4, getBirthDate(documentStatus.getApplictionSubmittedDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			if (!documentStatus.getRefreshDate().equals("")) {
				ps.setDate(5, getBirthDate(documentStatus.getRefreshDate()));
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}
			ps.setString(6, documentStatus.getNotes());
			ps.setString(7, hashValue);
			ps.setBoolean(8, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationInfo
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveRegistrationInfo(RegistrationInfo registrationInfo, long kyclId, String hashValue)
			throws SQLException {
		String sql = "INSERT INTO kycl_registration_info"
				+ "(kycl_info_id,registration_authority,self_regulatory_body,regd_number,licensing_authority,license_number,notes,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationInfo.getRegistrationAuthority());
			ps.setString(3, registrationInfo.getSelfRegulatoryBody());
			ps.setString(4, registrationInfo.getRegdNumber());
			ps.setString(5, registrationInfo.getLicensingAuthority());
			ps.setString(6, registrationInfo.getLicenseNumber());
			ps.setString(7, registrationInfo.getNotes());
			ps.setString(8, hashValue);
			ps.setBoolean(9, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param relatedEntity
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveRelatedEntityInfo(RelatedEntity relatedEntity, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_related_entity_info"
				+ "(kycl_info_id,entity_type,cust_id,kycl_id,salutation,name,"
				+ "ls_name,called_by_name,primary_identification_document_type,registration_no,country,zone,"
				+ " district,mn_vdc,pinzip,ward_number,tole_area,street,house_no,"
				+ "unit_number,nearest_landmark,latitude,longitude,phone_no_country_code,"
				+ "phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,hash,province,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedEntity.getEntityType());
			ps.setString(3, relatedEntity.getCustId());
			ps.setLong(4, relatedEntity.getKycnId());
			ps.setString(5, relatedEntity.getSalutation());
			ps.setString(6, relatedEntity.getName());
			ps.setString(7, relatedEntity.getLsName());
			ps.setString(8, relatedEntity.getCalledByName());
			ps.setString(9, relatedEntity.getPrimaryIdentificationDocumentType());
			ps.setString(10, relatedEntity.getRegistrationNo());
			ps.setString(11, relatedEntity.getCountry());
			ps.setString(12, relatedEntity.getZone());
			ps.setString(13, relatedEntity.getDistrict());
			ps.setString(14, relatedEntity.getMnVdc());
			ps.setString(15, relatedEntity.getPinzip());
			ps.setString(16, relatedEntity.getWardNumber());
			ps.setString(17, relatedEntity.getToleArea());
			ps.setString(18, relatedEntity.getStreet());
			ps.setString(19, relatedEntity.getHouseNo());
			ps.setString(20, relatedEntity.getUnitNumber());
			ps.setString(21, relatedEntity.getNearestLandmark());
			ps.setString(22, relatedEntity.getLatitude());
			ps.setString(23, relatedEntity.getLongitude());
			ps.setString(24, relatedEntity.getPhoneNoCountryCode());
			ps.setString(25, relatedEntity.getPhoneNoAreaCode());
			ps.setString(26, relatedEntity.getPhoneNo());
			ps.setString(27, relatedEntity.getTelexNoCountryCode());
			ps.setString(28, relatedEntity.getTelexNoAreaCode());
			ps.setString(29, relatedEntity.getTelexNo());
			ps.setString(30, hashValue);
			ps.setString(31, relatedEntity.getProvince());
			ps.setBoolean(32, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}
	}

	/**
	 * @param relatedPerson
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveRelatedPerson(RelatedPerson relatedPerson, long kyclId, String hashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_related_person_info"
				+ "(kycl_info_id,person_type,cust_id,kycn_id,salutation,first_name,"
				+ "middle_name,last_name,lsf_name,lsm_name,lsl_name,second_name,"
				+ " expiry_date,zone,district,mn_vdc,pinzip,ward_number,tole_area,"
				+ "street,house_no,unit_number,nearest_landmark,latitude,longitude,"
				+ "phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,"
				+ "telex_no_area_code,telex_no,email_id,notes,hash,province,account_no,"
				+ "is_active,goaml_person_type,is_director,"
				+ "called_by_name,primary_identification_document_type,primary_identification_document_no,country,issuing_authority,place_of_issue,issue_date,goaml_account_person_role_type) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedPerson.getPersonType());
			ps.setString(3, relatedPerson.getCustId());
			ps.setLong(4, relatedPerson.getKycnId());
			ps.setString(5, relatedPerson.getSalutation());
			ps.setString(6, relatedPerson.getFirstName());
			ps.setString(7, relatedPerson.getMiddleName());
			ps.setString(8, relatedPerson.getLastName());
			ps.setString(9, relatedPerson.getLsfName());
			ps.setString(10, relatedPerson.getLsmName());
			ps.setString(11, relatedPerson.getLslName());
			ps.setString(12, relatedPerson.getSecondName());
			if (!relatedPerson.getExpiryDate().equals("")) {
				ps.setDate(13, getBirthDate(relatedPerson.getExpiryDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, relatedPerson.getZone());
			ps.setString(15, relatedPerson.getDistrict());
			ps.setString(16, relatedPerson.getMnVdc());
			ps.setString(17, relatedPerson.getPinzip());
			ps.setString(18, relatedPerson.getWardNumber());
			ps.setString(19, relatedPerson.getToleArea());
			ps.setString(20, relatedPerson.getStreet());
			ps.setString(21, relatedPerson.getHouseNo());
			ps.setString(22, relatedPerson.getUnitNumber());
			ps.setString(23, relatedPerson.getNearestLandmark());
			ps.setString(24, relatedPerson.getLatitude());
			ps.setString(25, relatedPerson.getLongitude());
			ps.setString(26, relatedPerson.getPhoneNoCountryCode());
			ps.setString(27, relatedPerson.getPhoneNoAreaCode());
			ps.setString(28, relatedPerson.getPhoneNo());
			ps.setString(29, relatedPerson.getTelexNoCountryCode());
			ps.setString(30, relatedPerson.getTelexNoAreaCode());
			ps.setString(31, relatedPerson.getTelexNo());
			ps.setString(32, relatedPerson.getEmailId());
			ps.setString(33, relatedPerson.getNotes());
			ps.setString(34, hashValue);
			ps.setString(35, relatedPerson.getProvince());
			ps.setString(36, relatedPerson.getAccountNo());
			ps.setBoolean(37, Boolean.parseBoolean(relatedPerson.getRemovedSignatoryStatus()));
			ps.setString(38, relatedPerson.getGoAMLPersonType());
			ps.setBoolean(39, Boolean.parseBoolean(relatedPerson.getIsDirector()));
			ps.setString(40, relatedPerson.getCalledByName());
			ps.setString(41, relatedPerson.getPrimaryIdentificationDocumentType());
			ps.setString(42, relatedPerson.getPrimaryIdentificationDocumentNo());
			ps.setString(43, relatedPerson.getCountry());
			ps.setString(44, relatedPerson.getIssuingAuthority());
			ps.setString(45, relatedPerson.getPlaceOfIssue());
			
			if (!relatedPerson.getIssueDate().equals("")) {
				ps.setDate(46, getBirthDate(relatedPerson.getIssueDate()));
			} else {
				ps.setNull(46, java.sql.Types.DATE);
			}
			
			ps.setString(47, relatedPerson.getGoAMLPersonType());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param accountInfo
	 * @param kyclId
	 * @param accountHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveAccountInfo(AccountInfo accountInfo, long kyclId, String accountHashCode)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_account_info"
				+ "(kycl_info_id,account_id,for_account_id,currency_of_account,account_no,account_name,"
				+ "account_short_name,account_ownership,scheme_type,scheme_code,gl_sub_head_code,product_group,"
				+ " last_transaction_date,account_open_date,estimated_yearly_transactions,estimated_monthly_transactions,estimated_yearly_turnover,estimated_monthly_turnover,regular_source_of_income,"
				+ "source_of_fund,notes,nature_of_account,scheme_description,dr_balance_limit,deposite_amount,customer_pan,customer_currency,hash,is_blocked,"
				+ "account_status_type,account_type,goaml_account_status_type,goaml_personal_account_type,branch_sol_id,"
				+ "is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, accountInfo.getAccountId());
			ps.setString(3, accountInfo.getForAccountId());
			ps.setString(4, accountInfo.getCurrencyOfAccount());
			ps.setString(5, accountInfo.getAccountNo());
			ps.setString(6, accountInfo.getAccountName());
			ps.setString(7, accountInfo.getAccountShortName());
			ps.setString(8, accountInfo.getAccountOwnership());
			ps.setString(9, accountInfo.getSchemeType());
			ps.setString(10, accountInfo.getSchemeCode());
			ps.setString(11, accountInfo.getGlSubHeadCode());
			ps.setString(12, accountInfo.getProductGroup());
			if (!accountInfo.getLastTransactionDate().equals("")) {
				ps.setDate(13, getBirthDate(accountInfo.getLastTransactionDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			if (!accountInfo.getAccountOpenDate().equals("")) {
				ps.setDate(14, getBirthDate(accountInfo.getAccountOpenDate()));
			} else {
				ps.setNull(14, java.sql.Types.DATE);
			}
			ps.setLong(15, accountInfo.getEstimatedYearlyTransactions());
			ps.setLong(16, accountInfo.getEstimatedMonthlyTransactions());
			ps.setLong(17, accountInfo.getEstimatedYearlyTurnover());
			ps.setLong(18, accountInfo.getEstimatedMonthlyTurnover());
			ps.setString(19, accountInfo.getRegularSourceOfIncome());
			ps.setString(20, accountInfo.getSourceOfFund());
			ps.setString(21, accountInfo.getNotes());
			ps.setString(22, accountInfo.getNatureOfAccount());
			ps.setString(23, accountInfo.getSchemeDescription());
			ps.setString(24, accountInfo.getDrBalanceLimit());
			ps.setString(25, accountInfo.getDepositeAmount());
			ps.setString(26, accountInfo.getCustomerPan());
			ps.setString(27, accountInfo.getCustomerCurrency());
			ps.setString(28, accountHashCode);
			ps.setString(29, accountInfo.getIsBlocked());
			ps.setString(30, accountInfo.getAccountStatusType());
			ps.setString(31, accountInfo.getAccountType());
			ps.setString(32, accountInfo.getGoAMLAccountStatusType());
			ps.setString(33, accountInfo.getGoAMLaccountType());
			ps.setString(34, accountInfo.getBranchSolId());
			ps.setBoolean(35, Boolean.parseBoolean(accountInfo.getRemovedAccountStatus()));
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param businessInfo
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveBusinessInfo(BusinessInfo businessInfo, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_business_info"
				+ "(kycl_info_id,branches_name,nature_of_business,main_branches_offices,geographical_coverage,country,"
				+ "province,district,mn_vdc,ward_no,town_city,notes,"
				+ " phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,"
				+ "pager_no_area_code,pager_no,email_id,hash,zone,goaml_country,is_active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, businessInfo.getBranchesName());
			ps.setString(3, businessInfo.getNatureOfBusiness());
			ps.setString(4, businessInfo.getMainBranchesOffices());
			ps.setString(5, businessInfo.getGeographicalCoverage());
			ps.setString(6, businessInfo.getCountry());
			ps.setString(7, businessInfo.getProvince());
			ps.setString(8, businessInfo.getDistrict());
			ps.setString(9, businessInfo.getMnVdc());
			ps.setString(10, businessInfo.getWardNo());
			ps.setString(11, businessInfo.getTownCity());
			ps.setString(12, businessInfo.getNotes());
			ps.setString(13, businessInfo.getPhoneNoCountryCode());
			ps.setString(14, businessInfo.getPhoneNoAreaCode());
			ps.setString(15, businessInfo.getPhoneNo());
			ps.setString(16, businessInfo.getTelexNoCountryCode());
			ps.setString(17, businessInfo.getTelexNoAreaCode());
			ps.setString(18, businessInfo.getTelexNo());
			ps.setString(19, businessInfo.getPagerNoCountryCode());
			ps.setString(20, businessInfo.getPagerNoAreaCode());
			ps.setString(21, businessInfo.getPagerNo());
			ps.setString(22, businessInfo.getEmailId());
			ps.setString(23, hashValue);
			ps.setString(24, businessInfo.getZone());
			ps.setString(25, businessInfo.getGoAMLCountry());
			ps.setBoolean(26, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationAddress
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveRegistrationAddress(RegistrationAddress registrationAddress, long kyclId, String hashValue)
			throws SQLException {
		String sql = "INSERT INTO kycl_registration_address_info" + "(kycl_info_id,country,state,province,mn_vdc,"
				+ "ward_no,town_city,notes,phone_no_country_code,phone_no_area_code,phone_no,"
				+ " telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,email_id,district,hash,zone,is_active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationAddress.getCountry());
			ps.setString(3, registrationAddress.getState());
			ps.setString(4, registrationAddress.getProvince());
			ps.setString(5, registrationAddress.getMnVdc());
			ps.setString(6, registrationAddress.getWardNo());
			ps.setString(7, registrationAddress.getTownCity());
			ps.setString(8, registrationAddress.getNotes());
			ps.setString(9, registrationAddress.getPhoneNoCountryCode());
			ps.setString(10, registrationAddress.getPhoneNoAreaCode());
			ps.setString(11, registrationAddress.getPhoneNo());
			ps.setString(12, registrationAddress.getTelexNoCountryCode());
			ps.setString(13, registrationAddress.getTelexNoAreaCode());
			ps.setString(14, registrationAddress.getTelexNo());
			ps.setString(15, registrationAddress.getPagerNoCountryCode());
			ps.setString(16, registrationAddress.getPagerNoAreaCode());
			ps.setString(17, registrationAddress.getPagerNo());
			ps.setString(18, registrationAddress.getEmailId());
			ps.setString(19, registrationAddress.getDistrict());
			ps.setString(20, hashValue);
			ps.setString(21, registrationAddress.getZone());
			ps.setBoolean(22, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param accountCard
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveAccountCard(AccountCards accountCard, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_cards_subscribed (kycl_account_info_id,cards_subscribed,notes,hash) "
				+ "VALUES (?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, accountCard.getCardsSubscribed());
			ps.setString(3, accountCard.getNotes());
			ps.setString(4, hashValue);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param accountService
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveAccountService(AccountService accountService, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_services_subscribed (kycl_account_info_id,service_subscribed,notes,hash) "
				+ "VALUES (?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, accountService.getServiceSubscribed());
			ps.setString(3, accountService.getNotes());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}
	}

	/**
	 * @param legal
	 * @param user
	 * @param kyclUpdateHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveLegalInfoInUpdateTable(Legal legal, User user, String kyclUpdateHashCode)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_info_update"
				+ "(primary_sol_id,salutation,name_of_the_institution,ls_name,cust_short_name,date_of_establishment,"
				+ "notes,customer_type,customer_group,customer_constitution,customer_community,customer_employee_id,"
				+ "customer_open_date,customer_maker,screening_id,pan_number,minor,"
				+ " customer_status_code,card_holder,non_resident_external,cust_id,kycl_status,last_update_date,last_screened_date,"
				+ "verified_record,audited_fiscal_year,net_profit,net_loss,tax_clearence_certificate_date,tax_clearence_fiscal_year,"
				+ "next_clearence_certificate_date,taxable_amount,taxable_income,tax_paid,tax_paid_date,maker,checker,types_of_industry,hash,date_of_establishment_bs,goaml_legal_form_type,incomplete,"
				+ "incorporation_country,incorporation_country_code,incorporation_province,business_description,is_active,modified_date) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, legal.getPrimarySolId());
			ps.setString(2, legal.getSalutation());
			ps.setString(3, legal.getNameOfTheInstitution());
			ps.setString(4, legal.getLsName());
			ps.setString(5, legal.getCustShortName());
			if (legal.getDateOfEstablishment() != null && !legal.getDateOfEstablishment().equals("")) {
				ps.setDate(6, getBirthDate(legal.getDateOfEstablishment()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, legal.getNotes());
			ps.setString(8, legal.getCustomerType());
			ps.setString(9, legal.getCustomerGroup());
			ps.setString(10, legal.getCustomerConstitution());
			ps.setString(11, legal.getCustomerCommunity());
			ps.setString(12, legal.getCustomerEmployeeId());
			if (legal.getCustomerOpenDate() != null && !legal.getCustomerOpenDate().equals("")) {
				ps.setDate(13, getBirthDate(legal.getCustomerOpenDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, legal.getCustomerMaker());
			ps.setLong(15, legal.getScreeningId());
			ps.setString(16, legal.getPanNumber());
			ps.setBoolean(17, legal.isMinor());
			ps.setString(18, legal.getCustomerStatusCode());
			ps.setBoolean(19, legal.isCardHolder());
			ps.setBoolean(20, legal.isNonResidentExternal());
			ps.setString(21, legal.getCustId());
			ps.setString(22, legal.getKyclStatus());
			if (legal.getLastUpdateDate() != null && !legal.getLastUpdateDate().equals("")) {
				ps.setDate(23, getBirthDate(legal.getLastUpdateDate()));
			} else {
				ps.setNull(23, java.sql.Types.DATE);
			}
			if (legal.getLastScreenedDate() != null && !legal.getLastScreenedDate().equals("")) {
				ps.setDate(24, getBirthDate(legal.getLastScreenedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setBoolean(25, legal.isVerifiedRecord());
			ps.setString(26, legal.getAuditedFiscalYear());
			ps.setString(27, legal.getNetProfit());
			ps.setString(28, legal.getNetLoss());
			if (legal.getTaxClearenceCertificateDate() != null && !legal.getTaxClearenceCertificateDate().equals("")) {
				ps.setDate(29, getBirthDate(legal.getTaxClearenceCertificateDate()));
			} else {
				ps.setNull(29, java.sql.Types.DATE);
			}

			if (legal.getTaxClearenceFiscalYear() != null && !legal.getTaxClearenceFiscalYear().equals("")) {
				ps.setDate(30, getBirthDate(legal.getTaxClearenceFiscalYear()));
			} else {
				ps.setNull(30, java.sql.Types.DATE);
			}
			if (legal.getNextClearenceCertificateDate() != null
					&& !legal.getNextClearenceCertificateDate().equals("")) {

				ps.setDate(31, getBirthDate(legal.getNextClearenceCertificateDate()));
			} else {
				ps.setNull(31, java.sql.Types.DATE);
			}
			ps.setString(32, legal.getTaxableAmount());
			ps.setString(33, legal.getTaxableIncome());
			ps.setString(34, legal.getTaxPaid());
			if (legal.getTaxPaidDate() != null && !legal.getTaxPaidDate().equals("")) {

				ps.setDate(35, getBirthDate(legal.getTaxPaidDate()));
			} else {
				ps.setNull(35, java.sql.Types.DATE);
			}
			ps.setString(36, user.getUserName());
			ps.setString(37, user.getUserName());
			ps.setString(38, legal.getTypeOfIndustry());
			ps.setString(39, kyclUpdateHashCode);
			ps.setDate(40, legal.getDateOfEstablishmentBS());
			ps.setString(41, legal.getGoAMLCustConst());
			ps.setBoolean(42, legal.isIncomplete());
			ps.setString(43, legal.getIncorporationCountry());
			ps.setString(44, legal.getGoAMLIncorporationCountry());
			ps.setString(45, legal.getIncorporationProvince());
			ps.setString(46, legal.getBusinessDescription());
			ps.setBoolean(47, true);
			ps.setDate(48, new java.sql.Date(System.currentTimeMillis()));
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param accountInfo
	 * @param kyclId
	 * @param user
	 * @param accountUpdateHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveAccountInfoInUpdateTable(AccountInfo accountInfo, long kyclId, User user,
			String accountUpdateHashCode) throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_account_info_update"
				+ "(kycl_info_id,account_id,for_account_id,currency_of_account,account_no,account_name,"
				+ "account_short_name,account_ownership,scheme_type,scheme_code,gl_sub_head_code,product_group,"
				+ " last_transaction_date,account_open_date,estimated_yearly_transactions,estimated_monthly_transactions,estimated_yearly_turnover,estimated_monthly_turnover,regular_source_of_income,"
				+ "source_of_fund,notes,nature_of_account,scheme_description,maker,checker,dr_balance_limit,deposite_amount,customer_pan,customer_currency,hash,"
				+ "account_status_type,account_type,goaml_account_status_type,goaml_personal_account_type,branch_sol_id,"
				+ "is_active) " // ,is_blocked
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, accountInfo.getAccountId());
			ps.setString(3, accountInfo.getForAccountId());
			ps.setString(4, accountInfo.getCurrencyOfAccount());
			ps.setString(5, accountInfo.getAccountNo());
			ps.setString(6, accountInfo.getAccountName());
			ps.setString(7, accountInfo.getAccountShortName());
			ps.setString(8, accountInfo.getAccountOwnership());
			ps.setString(9, accountInfo.getSchemeType());
			ps.setString(10, accountInfo.getSchemeCode());
			ps.setString(11, accountInfo.getGlSubHeadCode());
			ps.setString(12, accountInfo.getProductGroup());
			if (!accountInfo.getLastTransactionDate().equals("")) {
				ps.setDate(13, getBirthDate(accountInfo.getLastTransactionDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			if (!accountInfo.getAccountOpenDate().equals("")) {
				ps.setDate(14, getBirthDate(accountInfo.getAccountOpenDate()));
			} else {
				ps.setNull(14, java.sql.Types.DATE);
			}
			ps.setLong(15, accountInfo.getEstimatedYearlyTransactions());
			ps.setLong(16, accountInfo.getEstimatedMonthlyTransactions());
			ps.setLong(17, accountInfo.getEstimatedYearlyTurnover());
			ps.setLong(18, accountInfo.getEstimatedMonthlyTurnover());
			ps.setString(19, accountInfo.getRegularSourceOfIncome());
			ps.setString(20, accountInfo.getSourceOfFund());
			ps.setString(21, accountInfo.getNotes());
			ps.setString(22, accountInfo.getNatureOfAccount());
			ps.setString(23, accountInfo.getSchemeDescription());
			ps.setString(24, user.getUserName());
			ps.setString(25, user.getUserName());
			ps.setString(26, accountInfo.getDrBalanceLimit());
			ps.setString(27, accountInfo.getDepositeAmount());
			ps.setString(28, accountInfo.getCustomerPan());
			ps.setString(29, accountInfo.getCustomerCurrency());
			ps.setString(30, accountUpdateHashCode);
			ps.setString(31, accountInfo.getAccountStatusType());
			ps.setString(32, accountInfo.getAccountType());
			ps.setString(33, accountInfo.getGoAMLAccountStatusType());
			ps.setString(34, accountInfo.getGoAMLaccountType());
			ps.setString(35, accountInfo.getBranchSolId());
			ps.setBoolean(36, Boolean.parseBoolean(accountInfo.getRemovedAccountStatus()));
			// ps.setString(31, accountInfo.getIsBlocked());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param address
	 * @param kyclId
	 * @param user
	 * @param addressUpdateHashCode
	 * @throws SQLException
	 */
	public void saveAddressInUpdateTable(Address address, long kyclId, User user, String addressUpdateHashCode)
			throws SQLException {
		String sql = "INSERT INTO kycl_address_info_update(kycl_info_id,address_type,country,zone,district,mn_vdc,"
				+ "pinzip,ward_number,tole_area,street,house_no,unit_number,"
				+ " nearest_landmark,latitude,longitude,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,"
				+ "telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,email_id,notes,state,maker,checker,hash,province,"
				+ "goaml_address_type,address,city,goaml_country,is_active,address_removed_by) "
				+ "VALUES (?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?,?,?,?,"
				+ "?,?)";

		// STREET IS MISSING
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, address.getAddressType());
			ps.setString(3, address.getCountry());
			ps.setString(4, address.getZone());
			ps.setString(5, address.getDistrict());
			ps.setString(6, address.getMnVdc());
			ps.setString(7, address.getPinzip());
			ps.setString(8, address.getWardNumber());
			ps.setString(9, address.getToleArea());
			ps.setString(10, address.getStreet());
			ps.setString(11, address.getHouseNo());
			ps.setString(12, address.getUnitNumber());
			ps.setString(13, address.getNearestLandmark());
			ps.setString(14, address.getLatitude());
			ps.setString(15, address.getLongitude());
			ps.setString(16, address.getPhoneNoCountryCode());
			ps.setString(17, address.getPhoneNoAreaCode());
			ps.setString(18, address.getPhoneNo());
			ps.setString(19, address.getTelexNoCountryCode());
			ps.setString(20, address.getTelexNoAreaCode());
			ps.setString(21, address.getTelexNo());
			ps.setString(22, address.getPagerNoCountryCode());
			ps.setString(23, address.getPagerNoAreaCode());
			ps.setString(24, address.getPagerNo());
			ps.setString(25, address.getEmailId());
			ps.setString(26, address.getNotes());
			ps.setString(27, address.getState());
			ps.setString(28, user.getUserName());
			ps.setString(29, user.getUserName());
			ps.setString(30, addressUpdateHashCode);
			ps.setString(31, address.getProvince());
			ps.setString(32, address.getGoAMLAddressType());
			ps.setString(33, address.getAddress());
			ps.setString(34, address.getCity());
			ps.setString(35, address.getGoAMLCountryType());
			ps.setBoolean(36, Boolean.parseBoolean(address.getRemovedAddressStatus()));
			ps.setString(37, user.getUserName());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param auditInfo
	 * @param kyclId
	 * @param user
	 * @param auditInfoUpdateHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveAuditInfoInUpdateTable(AuditInfo auditInfo, long kyclId, User user, String auditInfoUpdateHashCode)
			throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_audit_info_update"
				+ "(kycl_info_id,audited_report,authorized_letter,authorized_letter_date,authorized_letter_agency,notes,maker,checker,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, auditInfo.getAuditedReport());
			ps.setString(3, auditInfo.getAuthorizedLetter());
			if (!auditInfo.getAuthorizedLetterDate().equals("")) {
				ps.setDate(4, getBirthDate(auditInfo.getAuthorizedLetterDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			ps.setString(5, auditInfo.getAuthorizedLetterAgency());
			ps.setString(6, auditInfo.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, auditInfoUpdateHashCode);
			ps.setBoolean(10, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param businessInfo
	 * @param kyclId
	 * @param user
	 * @param businessInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void saveBusinessInfoInUpdateTable(BusinessInfo businessInfo, long kyclId, User user,
			String businessInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_business_info_update"
				+ "(kycl_info_id,branches_name,nature_of_business,main_branches_offices,geographical_coverage,country,"
				+ "province,district,mn_vdc,ward_no,town_city,notes,"
				+ " phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,"
				+ "pager_no_area_code,pager_no,email_id,maker,checker,hash,zone,goaml_country,is_active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, businessInfo.getBranchesName());
			ps.setString(3, businessInfo.getNatureOfBusiness());
			ps.setString(4, businessInfo.getMainBranchesOffices());
			ps.setString(5, businessInfo.getGeographicalCoverage());
			ps.setString(6, businessInfo.getCountry());
			ps.setString(7, businessInfo.getProvince());
			ps.setString(8, businessInfo.getDistrict());
			ps.setString(9, businessInfo.getMnVdc());
			ps.setString(10, businessInfo.getWardNo());
			ps.setString(11, businessInfo.getTownCity());
			ps.setString(12, businessInfo.getNotes());
			ps.setString(13, businessInfo.getPhoneNoCountryCode());
			ps.setString(14, businessInfo.getPhoneNoAreaCode());
			ps.setString(15, businessInfo.getPhoneNo());
			ps.setString(16, businessInfo.getTelexNoCountryCode());
			ps.setString(17, businessInfo.getTelexNoAreaCode());
			ps.setString(18, businessInfo.getTelexNo());
			ps.setString(19, businessInfo.getPagerNoCountryCode());
			ps.setString(20, businessInfo.getPagerNoAreaCode());
			ps.setString(21, businessInfo.getPagerNo());
			ps.setString(22, businessInfo.getEmailId());
			ps.setString(23, user.getUserName());
			ps.setString(24, user.getUserName());
			ps.setString(25, businessInfoUpdateHashCode);
			ps.setString(26, businessInfo.getZone());
			ps.setString(27, businessInfo.getGoAMLCountry());
			ps.setBoolean(28, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param complianceInfo
	 * @param kyclId
	 * @param user
	 * @param complainceInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void saveComplainceInfoInUpdateTable(ComplianceInfo complianceInfo, long kyclId, User user,
			String complainceInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_compliance_info_update"
				+ "(kycl_info_id,level_of_compliance_on_aml,level_of_compliance_on_tax,level_of_compliance_on_corruption,level_of_compliance_on_others,notes,maker,checker,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, complianceInfo.getLevelOfComplianceOnAml());
			ps.setString(3, complianceInfo.getLevelOfComplianceOnTax());
			ps.setString(4, complianceInfo.getLevelOfComplianceOnCorruption());
			ps.setString(5, complianceInfo.getLevelOfComplianceOnOthers());
			ps.setString(6, complianceInfo.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, complainceInfoUpdateHashCode);
			ps.setBoolean(10, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param documentStatus
	 * @param kyclId
	 * @param user
	 * @param documentStatusUpdateHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveDocumentStatusInUpdateTable(DocumentStatus documentStatus, long kyclId, User user,
			String documentStatusUpdateHashCode) throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_document_status_update"
				+ "(kycl_info_id,document_type,document_status,appliction_submitted_date,refresh_date,notes,maker,checker,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, documentStatus.getDocumentType());
			ps.setString(3, documentStatus.getDocumentStatus());
			if (!documentStatus.getApplictionSubmittedDate().equals("")) {
				ps.setDate(4, getBirthDate(documentStatus.getApplictionSubmittedDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			if (!documentStatus.getRefreshDate().equals("")) {
				ps.setDate(5, getBirthDate(documentStatus.getRefreshDate()));
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}
			ps.setString(6, documentStatus.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, documentStatusUpdateHashCode);
			ps.setBoolean(10, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationAddress
	 * @param kyclId
	 * @param user
	 * @param registrationAddressUpdateHashCode
	 * @throws SQLException
	 */
	public void saveRegistrationAddressInUpdateTable(RegistrationAddress registrationAddress, long kyclId, User user,
			String registrationAddressUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_registration_address_info_update (kycl_info_id,country,state,province,mn_vdc,"
				+ "ward_no,town_city,notes,phone_no_country_code,phone_no_area_code,phone_no,"
				+ " telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,email_id,district,maker,checker,hash,zone,is_active) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationAddress.getCountry());
			ps.setString(3, registrationAddress.getState());
			ps.setString(4, registrationAddress.getProvince());
			ps.setString(5, registrationAddress.getMnVdc());
			ps.setString(6, registrationAddress.getWardNo());
			ps.setString(7, registrationAddress.getTownCity());
			ps.setString(8, registrationAddress.getNotes());
			ps.setString(9, registrationAddress.getPhoneNoCountryCode());
			ps.setString(10, registrationAddress.getPhoneNoAreaCode());
			ps.setString(11, registrationAddress.getPhoneNo());
			ps.setString(12, registrationAddress.getTelexNoCountryCode());
			ps.setString(13, registrationAddress.getTelexNoAreaCode());
			ps.setString(14, registrationAddress.getTelexNo());
			ps.setString(15, registrationAddress.getPagerNoCountryCode());
			ps.setString(16, registrationAddress.getPagerNoAreaCode());
			ps.setString(17, registrationAddress.getPagerNo());
			ps.setString(18, registrationAddress.getEmailId());
			ps.setString(19, registrationAddress.getDistrict());
			ps.setString(20, user.getUserName());
			ps.setString(21, user.getUserName());
			ps.setString(22, registrationAddressUpdateHashCode);
			ps.setString(23, registrationAddress.getZone());
			ps.setBoolean(24, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationInfo
	 * @param kyclId
	 * @param user
	 * @param registrationInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void saveRegistrationInfoInUpdateTable(RegistrationInfo registrationInfo, long kyclId, User user,
			String registrationInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_registration_info_update"
				+ "(kycl_info_id,registration_authority,self_regulatory_body,regd_number,licensing_authority,license_number,notes,maker,checker,hash,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationInfo.getRegistrationAuthority());
			ps.setString(3, registrationInfo.getSelfRegulatoryBody());
			ps.setString(4, registrationInfo.getRegdNumber());
			ps.setString(5, registrationInfo.getLicensingAuthority());
			ps.setString(6, registrationInfo.getLicenseNumber());
			ps.setString(7, registrationInfo.getNotes());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getUserName());
			ps.setString(10, registrationInfoUpdateHashCode);
			ps.setBoolean(11, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param relatedEntity
	 * @param kyclId
	 * @param user
	 * @param relatedEntityUpdateHashCode
	 * @throws SQLException
	 */
	public void saveRelatedEntityInfoInUpdateTable(RelatedEntity relatedEntity, long kyclId, User user,
			String relatedEntityUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_related_entity_info_update"
				+ "(kycl_info_id,entity_type,cust_id,kycl_id,salutation,name,"
				+ "ls_name,called_by_name,primary_identification_document_type,registration_no,country,zone,"
				+ " district,mn_vdc,pinzip,ward_number,tole_area,street,house_no,"
				+ "unit_number,nearest_landmark,latitude,longitude,phone_no_country_code,"
				+ "phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,"
				+ "telex_no,maker,checker,hash,province,is_active) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedEntity.getEntityType());
			ps.setString(3, relatedEntity.getCustId());
			ps.setLong(4, relatedEntity.getKycnId());
			ps.setString(5, relatedEntity.getSalutation());
			ps.setString(6, relatedEntity.getName());
			ps.setString(7, relatedEntity.getLsName());
			ps.setString(8, relatedEntity.getCalledByName());
			ps.setString(9, relatedEntity.getPrimaryIdentificationDocumentType());
			ps.setString(10, relatedEntity.getRegistrationNo());
			ps.setString(11, relatedEntity.getCountry());
			ps.setString(12, relatedEntity.getZone());
			ps.setString(13, relatedEntity.getDistrict());
			ps.setString(14, relatedEntity.getMnVdc());
			ps.setString(15, relatedEntity.getPinzip());
			ps.setString(16, relatedEntity.getWardNumber());
			ps.setString(17, relatedEntity.getToleArea());
			ps.setString(18, relatedEntity.getStreet());
			ps.setString(19, relatedEntity.getHouseNo());
			ps.setString(20, relatedEntity.getUnitNumber());
			ps.setString(21, relatedEntity.getNearestLandmark());
			ps.setString(22, relatedEntity.getLatitude());
			ps.setString(23, relatedEntity.getLongitude());
			ps.setString(24, relatedEntity.getPhoneNoCountryCode());
			ps.setString(25, relatedEntity.getPhoneNoAreaCode());
			ps.setString(26, relatedEntity.getPhoneNo());
			ps.setString(27, relatedEntity.getTelexNoCountryCode());
			ps.setString(28, relatedEntity.getTelexNoAreaCode());
			ps.setString(29, relatedEntity.getTelexNo());
			ps.setString(30, user.getUserName());
			ps.setString(31, user.getUserName());
			ps.setString(32, relatedEntityUpdateHashCode);
			ps.setString(33, relatedEntity.getProvince());
			ps.setBoolean(34, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param relatedPerson
	 * @param kyclId
	 * @param user
	 * @param relatedPersonUpdateHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveRelatedPersonInUpdateTable(RelatedPerson relatedPerson, long kyclId, User user,
			String relatedPersonUpdateHashCode) throws SQLException, ParseException {
		String sql = "INSERT INTO kycl_related_person_info_update"
				+ "(kycl_info_id,person_type,cust_id,kycn_id,salutation,first_name,"
				+ "middle_name,last_name,lsf_name,lsm_name,lsl_name,second_name,"
				+ " expiry_date,zone,district,mn_vdc,pinzip,ward_number,tole_area,"
				+ "street,house_no,unit_number,nearest_landmark,latitude,longitude,"
				+ "phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,"
				+ "telex_no_area_code,telex_no,email_id,notes,maker,checker,hash,province,account_no,"
				+ "is_active,goaml_person_type,is_director,"
				+ "called_by_name,primary_identification_document_type,primary_identification_document_no,country,issuing_authority,place_of_issue,issue_date,goaml_account_person_role_type) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedPerson.getPersonType());
			ps.setString(3, relatedPerson.getCustId());
			ps.setLong(4, relatedPerson.getKycnId());
			ps.setString(5, relatedPerson.getSalutation());
			ps.setString(6, relatedPerson.getFirstName());
			ps.setString(7, relatedPerson.getMiddleName());
			ps.setString(8, relatedPerson.getLastName());
			ps.setString(9, relatedPerson.getLsfName());
			ps.setString(10, relatedPerson.getLsmName());
			ps.setString(11, relatedPerson.getLslName());
			ps.setString(12, relatedPerson.getSecondName());
			if (!relatedPerson.getExpiryDate().equals("")) {
				ps.setDate(13, getBirthDate(relatedPerson.getExpiryDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, relatedPerson.getZone());
			ps.setString(15, relatedPerson.getDistrict());
			ps.setString(16, relatedPerson.getMnVdc());
			ps.setString(17, relatedPerson.getPinzip());
			ps.setString(18, relatedPerson.getWardNumber());
			ps.setString(19, relatedPerson.getToleArea());
			ps.setString(20, relatedPerson.getStreet());
			ps.setString(21, relatedPerson.getHouseNo());
			ps.setString(22, relatedPerson.getUnitNumber());
			ps.setString(23, relatedPerson.getNearestLandmark());
			ps.setString(24, relatedPerson.getLatitude());
			ps.setString(25, relatedPerson.getLongitude());
			ps.setString(26, relatedPerson.getPhoneNoCountryCode());
			ps.setString(27, relatedPerson.getPhoneNoAreaCode());
			ps.setString(28, relatedPerson.getPhoneNo());
			ps.setString(29, relatedPerson.getTelexNoCountryCode());
			ps.setString(30, relatedPerson.getTelexNoAreaCode());
			ps.setString(31, relatedPerson.getTelexNo());
			ps.setString(32, relatedPerson.getEmailId());
			ps.setString(33, relatedPerson.getNotes());
			ps.setString(34, user.getUserName());
			ps.setString(35, user.getUserName());
			ps.setString(36, relatedPersonUpdateHashCode);
			ps.setString(37, relatedPerson.getProvince());
			ps.setString(38, relatedPerson.getAccountNo());
			ps.setBoolean(39,Boolean.parseBoolean(relatedPerson.getRemovedSignatoryStatus()));
			ps.setString(40, relatedPerson.getGoAMLPersonType());
			ps.setBoolean(41, Boolean.parseBoolean(relatedPerson.getIsDirector()));
			ps.setString(42, relatedPerson.getCalledByName());
			ps.setString(43, relatedPerson.getPrimaryIdentificationDocumentType());
			ps.setString(44, relatedPerson.getPrimaryIdentificationDocumentNo());
			ps.setString(45, relatedPerson.getCountry());
			ps.setString(46, relatedPerson.getIssuingAuthority());
			ps.setString(47, relatedPerson.getPlaceOfIssue());
			
			if (!relatedPerson.getIssueDate().equals("")) {
				ps.setDate(48, getBirthDate(relatedPerson.getIssueDate()));
			} else {
				ps.setNull(48, java.sql.Types.DATE);
			}
			ps.setString(49, relatedPerson.getGoAMLPersonType());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kyclId
	 * @param financialInfoHashCode
	 * @throws SQLException
	 */
	public void saveFinancialInfo(FinancialInfo financialInfo, long kyclId, String financialInfoHashCode)
			throws SQLException {
		String sql = "INSERT INTO kycl_financial_info"
				+ "(kycl_info_id,pan_gir_of_customer,currency_of_customer,notes,hash) VALUES (?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, financialInfo.getNotes());
			ps.setString(5, financialInfoHashCode);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kyclId
	 * @param user
	 * @param financialInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void saveFinancialInfoInUpdateTable(FinancialInfo financialInfo, long kyclId, User user,
			String financialInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_financial_info_update"
				+ "(kycl_info_id,pan_gir_of_customer,currency_of_customer,notes,maker,checker) VALUES (?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, financialInfo.getNotes());
			ps.setString(5, user.getUserName());
			ps.setString(6, user.getUserName());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param individual
	 * @param kyclId
	 * @return
	 * @throws SQLException
	 */
	public boolean checkIfSubscribedIndividualExists(String individual, long kyclId) throws SQLException {
		String sql = "SELECT * from  kycl_cards_subscribed_individual WHERE cards_subscribed=? AND kycl_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, individual);
			ps.setLong(2, kyclId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.getString(1);

				return true;
			}
		} finally {
			con.close();
			ps.close();
		}
		return false;
	}

	/**
	 * @param individual
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void insertCardSubscribedIndividual(String individual, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_cards_subscribed_individual"
				+ "(kycl_info_id,cards_subscribed,notes,hash) VALUES(?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, individual);
			ps.setString(3, "");
			ps.setString(4, hashValue);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param individual
	 * @param kyclId
	 * @param user
	 * @param individualAccountInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void insertCardSubscribedIndividualUpdateTable(String individual, long kyclId, User user,
			String individualAccountInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_cards_subscribed_individual_update"
				+ "(kycl_info_id,cards_subscribed,notes,maker,checker) VALUES(?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, individual);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param service
	 * @param kyclId
	 * @return
	 * @throws SQLException
	 */
	public boolean checkIfSubscribedServiceExists(String service, long kyclId) throws SQLException {
		String sql = "SELECT * from  kycl_service_subscribed_individual WHERE services_subscribed=? AND kycl_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, service);
			ps.setLong(2, kyclId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.getString(1);
				return true;
			}

		} finally {
			con.close();
			ps.close();
		}
		return false;
	}

	/**
	 * @param service
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void insertServiceSubscribedIndividual(String service, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_service_subscribed_individual"
				+ "(kycl_info_id,services_subscribed,notes,hash) VALUES(?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, service);
			ps.setString(3, "");
			ps.setString(4, hashValue);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param service
	 * @param kyclId
	 * @param user
	 * @param serviceAccountInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void insertServiceSubscribedIndividualUpdateTable(String service, long kyclId, User user,
			String serviceAccountInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_service_subscribed_individual_update"
				+ "(kycl_info_id,services_subscribed,notes,maker,checker) VALUES(?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, service);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param landlordInfo
	 * @param kyclId
	 * @param hashValue
	 * @throws SQLException
	 */
	public void saveLandlordInfo(LandlordInfo landlordInfo, long kyclId, String hashValue) throws SQLException {
		String sql = "INSERT INTO kycl_landlord_info"
				+ "(kycl_info_id,land_lord_first_name,land_lord_middle_name,land_lord_last_name,country,province,"
				+ "district,mn_vdc,ward_no,town_city,notes,phone_no_country_code,"
				+ " phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,"
				+ "pager_no,hash,zone,email_id,is_active)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, landlordInfo.getLandLordFirstName());
			ps.setString(3, landlordInfo.getLandLordMiddleName());
			ps.setString(4, landlordInfo.getLandLordLastName());
			ps.setString(5, landlordInfo.getCountry());
			ps.setString(6, landlordInfo.getProvince());
			ps.setString(7, landlordInfo.getDistrict());
			ps.setString(8, landlordInfo.getMnVdc());
			ps.setString(9, landlordInfo.getWardNo());
			ps.setString(10, landlordInfo.getTownCity());
			ps.setString(11, landlordInfo.getNotes());
			ps.setString(12, landlordInfo.getPhoneNoCountryCode());
			ps.setString(13, landlordInfo.getPagerNoAreaCode());
			ps.setString(14, landlordInfo.getPhoneNo());
			ps.setString(15, landlordInfo.getTelexNoCountryCode());
			ps.setString(16, landlordInfo.getTelexNoAreaCode());
			ps.setString(17, landlordInfo.getTelexNo());
			ps.setString(18, landlordInfo.getPagerNoCountryCode());
			ps.setString(19, landlordInfo.getPagerNoAreaCode());
			ps.setString(20, landlordInfo.getPagerNo());
			ps.setString(21, hashValue);
			ps.setString(22, landlordInfo.getZone());
			ps.setString(23, landlordInfo.getEmailId());
			ps.setBoolean(24, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param landlordInfo
	 * @param kyclId
	 * @param user
	 * @param landlordInfoUpdateHashCode
	 * @throws SQLException
	 */
	public void saveLandlordInfoInUpdateTable(LandlordInfo landlordInfo, long kyclId, User user,
			String landlordInfoUpdateHashCode) throws SQLException {
		String sql = "INSERT INTO kycl_landlord_info_update"
				+ "(kycl_info_id,land_lord_first_name,land_lord_middle_name,land_lord_last_name,country,province,"
				+ "district,mn_vdc,ward_no,town_city,notes,phone_no_country_code,"
				+ " phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,"
				+ "pager_no,maker,checker,zone,email_id,is_active)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, landlordInfo.getLandLordFirstName());
			ps.setString(3, landlordInfo.getLandLordMiddleName());
			ps.setString(4, landlordInfo.getLandLordLastName());
			ps.setString(5, landlordInfo.getCountry());
			ps.setString(6, landlordInfo.getProvince());
			ps.setString(7, landlordInfo.getDistrict());
			ps.setString(8, landlordInfo.getMnVdc());
			ps.setString(9, landlordInfo.getWardNo());
			ps.setString(10, landlordInfo.getTownCity());
			ps.setString(11, landlordInfo.getNotes());
			ps.setString(12, landlordInfo.getPhoneNoCountryCode());
			ps.setString(13, landlordInfo.getPagerNoAreaCode());
			ps.setString(14, landlordInfo.getPhoneNo());
			ps.setString(15, landlordInfo.getTelexNoCountryCode());
			ps.setString(16, landlordInfo.getTelexNoAreaCode());
			ps.setString(17, landlordInfo.getTelexNo());
			ps.setString(18, landlordInfo.getPagerNoCountryCode());
			ps.setString(19, landlordInfo.getPagerNoAreaCode());
			ps.setString(20, landlordInfo.getPagerNo());
			ps.setString(21, user.getUserName());
			ps.setString(22, user.getUserName());
			ps.setString(23, landlordInfo.getZone());
			ps.setString(24, landlordInfo.getEmailId());
			ps.setBoolean(25, true);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param legal
	 * @param kyclHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateKyclInfo(Legal legal, String kyclHashCode) throws SQLException, ParseException {
		String sql = "UPDATE kycl_info"
				+ " SET primary_sol_id=?,salutation=?,name_of_the_institution=?,ls_name=?,cust_short_name=?,date_of_establishment=?,"
				+ "notes=?,customer_type=?,customer_group=?,customer_constitution=?,customer_community=?,customer_employee_id=?,"
				+ "customer_open_date=?,customer_maker=?,screening_id=?,pan_number=?,minor=?,"
				+ " customer_status_code=?,card_holder=?,non_resident_external=?,cust_id=?,kycl_status=?,last_update_date=?,last_screened_date=?,"
				+ "verified_record=?,audited_fiscal_year=?,net_profit=?,net_loss=?,tax_clearence_certificate_date=?,tax_clearence_fiscal_year=?,"
				+ "next_clearence_certificate_date=?,taxable_amount=?,taxable_income=?,tax_paid=?,tax_paid_date=?,types_of_industry=?,hash=?,is_updated_on_time=?,"
				+ "is_updated=?,goaml_legal_form_type=?,incomplete=?,"
				+ "incorporation_country=?,incorporation_country_code=?,incorporation_province=?,business_description=?,is_active=?,modified_date=? WHERE id=? ";

		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, legal.getPrimarySolId());
			ps.setString(2, legal.getSalutation());
			ps.setString(3, legal.getNameOfTheInstitution());
			ps.setString(4, legal.getLsName());
			ps.setString(5, legal.getCustShortName());
			if (!legal.getDateOfEstablishment().equals("")) {
				ps.setDate(6, getBirthDate(legal.getDateOfEstablishment()));
			} else {
				ps.setNull(6, java.sql.Types.DATE);
			}
			ps.setString(7, legal.getNotes());
			ps.setString(8, legal.getCustomerType());
			ps.setString(9, legal.getCustomerGroup());
			ps.setString(10, legal.getCustomerConstitution());
			ps.setString(11, legal.getCustomerCommunity());
			ps.setString(12, legal.getCustomerEmployeeId());
			if (!legal.getCustomerOpenDate().equals("")) {
				ps.setDate(13, getBirthDate(legal.getCustomerOpenDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, legal.getCustomerMaker());
			ps.setLong(15, legal.getScreeningId());
			ps.setString(16, legal.getPanNumber());
			ps.setBoolean(17, legal.isMinor());
			ps.setString(18, legal.getCustomerStatusCode());
			ps.setBoolean(19, legal.isCardHolder());
			ps.setBoolean(20, legal.isNonResidentExternal());
			ps.setString(21, legal.getCustId());
			ps.setString(22, legal.getKyclStatus());
			if (!legal.getLastUpdateDate().equals("")) {
				ps.setDate(23, getBirthDate(legal.getLastUpdateDate()));
			} else {
				ps.setNull(23, java.sql.Types.DATE);
			}
			if (!legal.getLastScreenedDate().equals("")) {
				ps.setDate(24, getBirthDate(legal.getLastScreenedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setBoolean(25, legal.isVerifiedRecord());
			ps.setString(26, legal.getAuditedFiscalYear());
			ps.setString(27, legal.getNetProfit());
			ps.setString(28, legal.getNetLoss());
			if (!legal.getTaxClearenceCertificateDate().equals("")) {
				ps.setDate(29, getBirthDate(legal.getTaxClearenceCertificateDate()));
			} else {
				ps.setNull(29, java.sql.Types.DATE);
			}
			if (!legal.getTaxClearenceFiscalYear().equals("")) {
				ps.setDate(30, getBirthDate(legal.getTaxClearenceFiscalYear()));
			} else {
				ps.setNull(30, java.sql.Types.DATE);
			}
			if (!legal.getNextClearenceCertificateDate().equals("")) {
				ps.setDate(31, getBirthDate(legal.getNextClearenceCertificateDate()));
			} else {
				ps.setNull(31, java.sql.Types.DATE);
			}
			ps.setString(32, legal.getTaxableAmount());
			ps.setString(33, legal.getTaxableIncome());
			ps.setString(34, legal.getTaxPaid());
			if (!legal.getTaxPaidDate().equals("")) {
				ps.setDate(35, getBirthDate(legal.getTaxPaidDate()));
			} else {
				ps.setNull(35, java.sql.Types.DATE);
			}
			ps.setString(36, legal.getTypeOfIndustry());
			ps.setString(37, kyclHashCode);
			ps.setBoolean(38, true);
			ps.setBoolean(39, true);
			ps.setString(40, legal.getGoAMLCustConst());
			ps.setBoolean(41, legal.isIncomplete());
			ps.setString(42, legal.getIncorporationCountry());
			ps.setString(43, legal.getGoAMLIncorporationCountry());
			ps.setString(44, legal.getIncorporationProvince());
			ps.setString(45, legal.getBusinessDescription());
			ps.setBoolean(46, true);
			ps.setDate(47, new java.sql.Date(System.currentTimeMillis()));
			ps.setLong(48, legal.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param accountInfo
	 * @param kyclId
	 * @param accountHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateAccountInfo(AccountInfo accountInfo,User user, long kyclId, String accountHashCode)
			throws SQLException, ParseException {
		String sql = "UPDATE kycl_account_info"
				+ " SET kycl_info_id=?,account_id=?,for_account_id=?,currency_of_account=?,account_no=?,account_name=?,"
				+ "account_short_name=?,account_ownership=?,scheme_type=?,scheme_code=?,gl_sub_head_code=?,product_group=?,"
				+ " last_transaction_date=?,account_open_date=?,estimated_yearly_transactions=?,estimated_monthly_transactions=?,estimated_yearly_turnover=?,estimated_monthly_turnover=?,regular_source_of_income=?,"
				+ "source_of_fund=?,notes=?,nature_of_account=?,scheme_description=?,dr_balance_limit=?,deposite_amount=?,customer_pan=?,customer_currency=?,hash=?,"
				+ "account_status_type=?,account_type=?,goaml_account_status_type=?,goaml_personal_account_type=?,branch_sol_id=?,"
				+ "is_active=?,account_removed_by=? WHERE id=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, accountInfo.getAccountId());
			ps.setString(3, accountInfo.getForAccountId());
			ps.setString(4, accountInfo.getCurrencyOfAccount());
			ps.setString(5, accountInfo.getAccountNo());
			ps.setString(6, accountInfo.getAccountName());
			ps.setString(7, accountInfo.getAccountShortName());
			ps.setString(8, accountInfo.getAccountOwnership());
			ps.setString(9, accountInfo.getSchemeType());
			ps.setString(10, accountInfo.getSchemeCode());
			ps.setString(11, accountInfo.getGlSubHeadCode());
			ps.setString(12, accountInfo.getProductGroup());
			if (!accountInfo.getLastTransactionDate().equals("")) {
				ps.setDate(13, getBirthDate(accountInfo.getLastTransactionDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			if (!accountInfo.getAccountOpenDate().equals("")) {
				ps.setDate(14, getBirthDate(accountInfo.getAccountOpenDate()));
			} else {
				ps.setNull(14, java.sql.Types.DATE);
			}
			ps.setLong(15, accountInfo.getEstimatedYearlyTransactions());
			ps.setLong(16, accountInfo.getEstimatedMonthlyTransactions());
			ps.setLong(17, accountInfo.getEstimatedYearlyTurnover());
			ps.setLong(18, accountInfo.getEstimatedMonthlyTurnover());
			ps.setString(19, accountInfo.getRegularSourceOfIncome());
			ps.setString(20, accountInfo.getSourceOfFund());
			ps.setString(21, accountInfo.getNotes());
			ps.setString(22, accountInfo.getNatureOfAccount());
			ps.setString(23, accountInfo.getSchemeDescription());
			ps.setString(24, accountInfo.getDrBalanceLimit());
			ps.setString(25, accountInfo.getDepositeAmount());
			ps.setString(26, accountInfo.getCustomerPan());
			ps.setString(27, accountInfo.getCustomerCurrency());
			ps.setString(28, accountHashCode);
			ps.setString(29, accountInfo.getAccountStatusType());
			ps.setString(30, accountInfo.getAccountType());
			ps.setString(31, accountInfo.getGoAMLAccountStatusType());
			ps.setString(32, accountInfo.getGoAMLaccountType());
			ps.setString(33, accountInfo.getBranchSolId());
			ps.setBoolean(34, Boolean.parseBoolean(accountInfo.getRemovedAccountStatus()));
			if(accountInfo.getRemovedAccountStatus().equals("false")){
				ps.setString(35, user.getUserName());
			}else{
				ps.setString(35, null);
			}
			ps.setLong(36, accountInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param address
	 * @param kyclId
	 * @param addressHashCode
	 * @throws SQLException
	 */
	public void updateAddress(Address address, long kyclId, String addressHashCode, User user) throws SQLException {
		String sql = "UPDATE kycl_address_info SET kycl_info_id=?,address_type=?,country=?,zone=?,district=?,mn_vdc=?,"
				+ "pinzip=?,ward_number=?,tole_area=?,street=?,house_no=?,unit_number=?,"
				+ " nearest_landmark=?,latitude=?,longitude=?,phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,"
				+ "telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,pager_no=?,email_id=?,notes=?,state=?,hash=?,province=?,"
				+ "goaml_address_type=?,address=?,city=?,goaml_country=?,"
				+ "is_active=?,address_removed_by=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, address.getAddressType());
			ps.setString(3, address.getCountry());
			ps.setString(4, address.getZone());
			ps.setString(5, address.getDistrict());
			ps.setString(6, address.getMnVdc());
			ps.setString(7, address.getPinzip());
			ps.setString(8, address.getWardNumber());
			ps.setString(9, address.getToleArea());
			ps.setString(10, address.getStreet());
			ps.setString(11, address.getHouseNo());
			ps.setString(12, address.getUnitNumber());
			ps.setString(13, address.getNearestLandmark());
			ps.setString(14, address.getLatitude());
			ps.setString(15, address.getLongitude());
			ps.setString(16, address.getPhoneNoCountryCode());
			ps.setString(17, address.getPhoneNoAreaCode());
			ps.setString(18, address.getPhoneNo());
			ps.setString(19, address.getTelexNoCountryCode());
			ps.setString(20, address.getTelexNoAreaCode());
			ps.setString(21, address.getTelexNo());
			ps.setString(22, address.getPagerNoCountryCode());
			ps.setString(23, address.getPagerNoAreaCode());
			ps.setString(24, address.getPagerNo());
			ps.setString(25, address.getEmailId());
			ps.setString(26, address.getNotes());
			ps.setString(27, address.getState());
			ps.setString(28, addressHashCode);
			ps.setString(29, address.getProvince());
			ps.setString(30, address.getGoAMLAddressType());
			ps.setString(31, address.getAddress());
			ps.setString(32, address.getCity());
			ps.setString(33, address.getGoAMLCountryType());
			ps.setBoolean(34, Boolean.parseBoolean(address.getRemovedAddressStatus()));
			ps.setString(35, user.getUserName());
			ps.setLong(36, address.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param auditInfo
	 * @param kyclId
	 * @param auditInfoHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateAuditInfo(AuditInfo auditInfo, long kyclId, String auditInfoHashCode)
			throws SQLException, ParseException {
		String sql = "UPDATE kycl_audit_info"
				+ " SET kycl_info_id=?,audited_report=?,authorized_letter=?,authorized_letter_date=?,authorized_letter_agency=?,notes=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, auditInfo.getAuditedReport());
			ps.setString(3, auditInfo.getAuthorizedLetter());
			if (!auditInfo.getAuthorizedLetterDate().equals("")) {
				ps.setDate(4, getBirthDate(auditInfo.getAuthorizedLetterDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			ps.setString(5, auditInfo.getAuthorizedLetterAgency());
			ps.setString(6, auditInfo.getNotes());
			ps.setString(7, auditInfoHashCode);
			ps.setBoolean(8, true);
			ps.setLong(9, auditInfo.getId());
			
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param businessInfo
	 * @param kyclId
	 * @param businessInfoHashCode
	 * @throws SQLException
	 */
	public void updateBusinessInfo(BusinessInfo businessInfo, long kyclId, String businessInfoHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_business_info SET "
				+ "kycl_info_id=?,branches_name=?,nature_of_business=?,main_branches_offices=?,geographical_coverage=?,country=?,"
				+ "province=?,district=?,mn_vdc=?,ward_no=?,town_city=?,notes=?,"
				+ " phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,"
				+ "pager_no_area_code=?,pager_no=?,email_id=?,hash=?,zone=?,goaml_country=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, businessInfo.getBranchesName());
			ps.setString(3, businessInfo.getNatureOfBusiness());
			ps.setString(4, businessInfo.getMainBranchesOffices());
			ps.setString(5, businessInfo.getGeographicalCoverage());
			ps.setString(6, businessInfo.getCountry());
			ps.setString(7, businessInfo.getProvince());
			ps.setString(8, businessInfo.getDistrict());
			ps.setString(9, businessInfo.getMnVdc());
			ps.setString(10, businessInfo.getWardNo());
			ps.setString(11, businessInfo.getTownCity());
			ps.setString(12, businessInfo.getNotes());
			ps.setString(13, businessInfo.getPhoneNoCountryCode());
			ps.setString(14, businessInfo.getPhoneNoAreaCode());
			ps.setString(15, businessInfo.getPhoneNo());
			ps.setString(16, businessInfo.getTelexNoCountryCode());
			ps.setString(17, businessInfo.getTelexNoAreaCode());
			ps.setString(18, businessInfo.getTelexNo());
			ps.setString(19, businessInfo.getPagerNoCountryCode());
			ps.setString(20, businessInfo.getPagerNoAreaCode());
			ps.setString(21, businessInfo.getPagerNo());
			ps.setString(22, businessInfo.getEmailId());
			ps.setString(23, businessInfoHashCode);
			ps.setString(24, businessInfo.getZone());
			ps.setString(25, businessInfo.getGoAMLCountry());
			ps.setBoolean(26, true);
			ps.setLong(27, businessInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param documentStatus
	 * @param kyclId
	 * @param documentStatusHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateDocumentStatus(DocumentStatus documentStatus, long kyclId, String documentStatusHashCode)
			throws SQLException, ParseException {
		String sql = "UPDATE kycl_document_status SET "
				+ "kycl_info_id=?,document_type=?,document_status=?,appliction_submitted_date=?,refresh_date=?,notes=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, documentStatus.getDocumentType());
			ps.setString(3, documentStatus.getDocumentStatus());
			if (!documentStatus.getApplictionSubmittedDate().equals("")) {
				ps.setDate(4, getBirthDate(documentStatus.getApplictionSubmittedDate()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			if (!documentStatus.getRefreshDate().equals("")) {
				ps.setDate(5, getBirthDate(documentStatus.getRefreshDate()));
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}
			ps.setString(6, documentStatus.getNotes());
			ps.setString(7, documentStatusHashCode);
			ps.setBoolean(8, true);
			ps.setLong(9, documentStatus.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param landlordInfo
	 * @param kyclId
	 * @param landlordInfoHashCode
	 * @throws SQLException
	 */
	public void updateLandlordInfo(LandlordInfo landlordInfo, long kyclId, String landlordInfoHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_landlord_info SET "
				+ "kycl_info_id=?,land_lord_first_name=?,land_lord_middle_name=?,land_lord_last_name=?,country=?,province=?,"
				+ "district=?,mn_vdc=?,ward_no=?,town_city=?,notes=?,phone_no_country_code=?,"
				+ " phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,"
				+ "pager_no=?,hash=?,zone=?,email_id=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, landlordInfo.getLandLordFirstName());
			ps.setString(3, landlordInfo.getLandLordMiddleName());
			ps.setString(4, landlordInfo.getLandLordLastName());
			ps.setString(5, landlordInfo.getCountry());
			ps.setString(6, landlordInfo.getProvince());
			ps.setString(7, landlordInfo.getDistrict());
			ps.setString(8, landlordInfo.getMnVdc());
			ps.setString(9, landlordInfo.getWardNo());
			ps.setString(10, landlordInfo.getTownCity());
			ps.setString(11, landlordInfo.getNotes());
			ps.setString(12, landlordInfo.getPhoneNoCountryCode());
			ps.setString(13, landlordInfo.getPagerNoAreaCode());
			ps.setString(14, landlordInfo.getPhoneNo());
			ps.setString(15, landlordInfo.getTelexNoCountryCode());
			ps.setString(16, landlordInfo.getTelexNoAreaCode());
			ps.setString(17, landlordInfo.getTelexNo());
			ps.setString(18, landlordInfo.getPagerNoCountryCode());
			ps.setString(19, landlordInfo.getPagerNoAreaCode());
			ps.setString(20, landlordInfo.getPagerNo());
			ps.setString(21, landlordInfoHashCode);
			ps.setString(22, landlordInfo.getZone());
			ps.setString(23, landlordInfo.getEmailId());
			ps.setBoolean(24, true);
			ps.setLong(25, landlordInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationAddress
	 * @param kyclId
	 * @param registrationAddressHashCode
	 * @throws SQLException
	 */
	public void updateRegistrationAddress(RegistrationAddress registrationAddress, long kyclId,
			String registrationAddressHashCode) throws SQLException {
		String sql = "UPDATE kycl_registration_address_info SET kycl_info_id=?,country=?,state=?,province=?,mn_vdc=?,"
				+ "ward_no=?,town_city=?,notes=?,phone_no_country_code=?,phone_no_area_code=?,phone_no=?,"
				+ " telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,pager_no=?,email_id=?,district=?,hash=?,"
				+ "zone=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationAddress.getCountry());
			ps.setString(3, registrationAddress.getState());
			ps.setString(4, registrationAddress.getProvince());
			ps.setString(5, registrationAddress.getMnVdc());
			ps.setString(6, registrationAddress.getWardNo());
			ps.setString(7, registrationAddress.getTownCity());
			ps.setString(8, registrationAddress.getNotes());
			ps.setString(9, registrationAddress.getPhoneNoCountryCode());
			ps.setString(10, registrationAddress.getPhoneNoAreaCode());
			ps.setString(11, registrationAddress.getPhoneNo());
			ps.setString(12, registrationAddress.getTelexNoCountryCode());
			ps.setString(13, registrationAddress.getTelexNoAreaCode());
			ps.setString(14, registrationAddress.getTelexNo());
			ps.setString(15, registrationAddress.getPagerNoCountryCode());
			ps.setString(16, registrationAddress.getPagerNoAreaCode());
			ps.setString(17, registrationAddress.getPagerNo());
			ps.setString(18, registrationAddress.getEmailId());
			ps.setString(19, registrationAddress.getDistrict());
			ps.setString(20, registrationAddressHashCode);
			ps.setString(21, registrationAddress.getZone());
			ps.setBoolean(22, true);
			ps.setLong(23, registrationAddress.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param registrationInfo
	 * @param kyclId
	 * @param registrationInfoHashCode
	 * @throws SQLException
	 */
	public void updateRegistrationInfo(RegistrationInfo registrationInfo, long kyclId, String registrationInfoHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_registration_info SET "
				+ "kycl_info_id=?,registration_authority=?,self_regulatory_body=?,regd_number=?,licensing_authority=?,license_number=?,notes=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, registrationInfo.getRegistrationAuthority());
			ps.setString(3, registrationInfo.getSelfRegulatoryBody());
			ps.setString(4, registrationInfo.getRegdNumber());
			ps.setString(5, registrationInfo.getLicensingAuthority());
			ps.setString(6, registrationInfo.getLicenseNumber());
			ps.setString(7, registrationInfo.getNotes());
			ps.setString(8, registrationInfoHashCode);
			ps.setBoolean(9, true);
			ps.setLong(10, registrationInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param relatedEntity
	 * @param kyclId
	 * @param relatedEntityHashCode
	 * @throws SQLException
	 */
	public void updateRelatedEntityInfo(RelatedEntity relatedEntity, long kyclId, String relatedEntityHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_related_entity_info SET "
				+ " kycl_info_id=?,entity_type=?,cust_id=?,kycl_id=?,salutation=?,name=?,"
				+ "ls_name=?,called_by_name=?,primary_identification_document_type=?,registration_no=?,country=?,zone=?,"
				+ " district=?,mn_vdc=?,pinzip=?,ward_number=?,tole_area=?,street=?,house_no=?,"
				+ "unit_number=?,nearest_landmark=?,latitude=?,longitude=?,phone_no_country_code=?,phone_no_area_code=?,"
				+ "phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,hash=?,province=?,is_active=? WHERE id=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedEntity.getEntityType());
			ps.setString(3, relatedEntity.getCustId());
			ps.setLong(4, relatedEntity.getKycnId());
			ps.setString(5, relatedEntity.getSalutation());
			ps.setString(6, relatedEntity.getName());
			ps.setString(7, relatedEntity.getLsName());
			ps.setString(8, relatedEntity.getCalledByName());
			ps.setString(9, relatedEntity.getPrimaryIdentificationDocumentType());
			ps.setString(10, relatedEntity.getRegistrationNo());
			ps.setString(11, relatedEntity.getCountry());
			ps.setString(12, relatedEntity.getZone());
			ps.setString(13, relatedEntity.getDistrict());
			ps.setString(14, relatedEntity.getMnVdc());
			ps.setString(15, relatedEntity.getPinzip());
			ps.setString(16, relatedEntity.getWardNumber());
			ps.setString(17, relatedEntity.getToleArea());
			ps.setString(18, relatedEntity.getStreet());
			ps.setString(19, relatedEntity.getHouseNo());
			ps.setString(20, relatedEntity.getUnitNumber());
			ps.setString(21, relatedEntity.getNearestLandmark());
			ps.setString(22, relatedEntity.getLatitude());
			ps.setString(23, relatedEntity.getLongitude());
			ps.setString(24, relatedEntity.getPhoneNoCountryCode());
			ps.setString(25, relatedEntity.getPhoneNoAreaCode());
			ps.setString(26, relatedEntity.getPhoneNo());
			ps.setString(27, relatedEntity.getTelexNoCountryCode());
			ps.setString(28, relatedEntity.getTelexNoAreaCode());
			ps.setString(29, relatedEntity.getTelexNo());
			ps.setString(30, relatedEntityHashCode);
			ps.setString(31, relatedEntity.getProvince());
			ps.setBoolean(32, true);
			ps.setLong(33, relatedEntity.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param relatedPerson
	 * @param kyclId
	 * @param relatedPersonHashCode
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateRelatedPerson(RelatedPerson relatedPerson, long kyclId, String relatedPersonHashCode)
			throws SQLException, ParseException {
		String sql = "UPDATE kycl_related_person_info"
				+ " SET kycl_info_id=?,person_type=?,cust_id=?,kycn_id=?,salutation=?,first_name=?,"
				+ "middle_name=?,last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,"
				+ " expiry_date=?,zone=?,district=?,mn_vdc=?,pinzip=?,ward_number=?,tole_area=?,"
				+ "street=?,house_no=?,unit_number=?,nearest_landmark=?,latitude=?,longitude=?,"
				+ "phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,"
				+ "telex_no_area_code=?,telex_no=?,email_id=?,notes=?,hash=?,province=?,"
				+ "is_active=?,goaml_person_type=?,is_director=?,"
				+ "called_by_name=?,primary_identification_document_type=?,"
				+ "primary_identification_document_no=?,country=?,issuing_authority=?,place_of_issue=?,issue_date=?,goaml_account_person_role_type=? WHERE id=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, relatedPerson.getPersonType());
			ps.setString(3, relatedPerson.getCustId());
			ps.setLong(4, relatedPerson.getKycnId());
			ps.setString(5, relatedPerson.getSalutation());
			ps.setString(6, relatedPerson.getFirstName());
			ps.setString(7, relatedPerson.getMiddleName());
			ps.setString(8, relatedPerson.getLastName());
			ps.setString(9, relatedPerson.getLsfName());
			ps.setString(10, relatedPerson.getLsmName());
			ps.setString(11, relatedPerson.getLslName());
			ps.setString(12, relatedPerson.getSecondName());
			if (!relatedPerson.getExpiryDate().equals("")) {
				ps.setDate(13, getBirthDate(relatedPerson.getExpiryDate()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, relatedPerson.getZone());
			ps.setString(15, relatedPerson.getDistrict());
			ps.setString(16, relatedPerson.getMnVdc());
			ps.setString(17, relatedPerson.getPinzip());
			ps.setString(18, relatedPerson.getWardNumber());
			ps.setString(19, relatedPerson.getToleArea());
			ps.setString(20, relatedPerson.getStreet());
			ps.setString(21, relatedPerson.getHouseNo());
			ps.setString(22, relatedPerson.getUnitNumber());
			ps.setString(23, relatedPerson.getNearestLandmark());
			ps.setString(24, relatedPerson.getLatitude());
			ps.setString(25, relatedPerson.getLongitude());
			ps.setString(26, relatedPerson.getPhoneNoCountryCode());
			ps.setString(27, relatedPerson.getPhoneNoAreaCode());
			ps.setString(28, relatedPerson.getPhoneNo());
			ps.setString(29, relatedPerson.getTelexNoCountryCode());
			ps.setString(30, relatedPerson.getTelexNoAreaCode());
			ps.setString(31, relatedPerson.getTelexNo());
			ps.setString(32, relatedPerson.getEmailId());
			ps.setString(33, relatedPerson.getNotes());
			ps.setString(34, relatedPersonHashCode);
			ps.setString(35, relatedPerson.getProvince());
			ps.setBoolean(36,Boolean.parseBoolean(relatedPerson.getRemovedSignatoryStatus()));
			ps.setString(37, relatedPerson.getGoAMLPersonType());
			ps.setBoolean(38, Boolean.parseBoolean(relatedPerson.getIsDirector()));
			
			ps.setString(39, relatedPerson.getCalledByName());
			ps.setString(40, relatedPerson.getPrimaryIdentificationDocumentType());
			ps.setString(41, relatedPerson.getPrimaryIdentificationDocumentNo());
			ps.setString(42, relatedPerson.getCountry());
			ps.setString(43, relatedPerson.getIssuingAuthority());
			ps.setString(44, relatedPerson.getPlaceOfIssue());
			
			if (!relatedPerson.getIssueDate().equals("")) {
				ps.setDate(45, getBirthDate(relatedPerson.getIssueDate()));
			} else {
				ps.setNull(45, java.sql.Types.DATE);
			}
			
			ps.setString(46, relatedPerson.getGoAMLPersonType());
			
			ps.setLong(47, relatedPerson.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param complianceInfo
	 * @param kyclId
	 * @param complainceInfoHashCode
	 * @throws SQLException
	 */
	public void updateComplainceInfo(ComplianceInfo complianceInfo, long kyclId, String complainceInfoHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_compliance_info"
				+ " SET kycl_info_id=?,level_of_compliance_on_aml=?,level_of_compliance_on_tax=?,level_of_compliance_on_corruption=?,level_of_compliance_on_others=?,notes=?,hash=?,is_active=? WHERE id=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, complianceInfo.getLevelOfComplianceOnAml());
			ps.setString(3, complianceInfo.getLevelOfComplianceOnTax());
			ps.setString(4, complianceInfo.getLevelOfComplianceOnCorruption());
			ps.setString(5, complianceInfo.getLevelOfComplianceOnOthers());
			ps.setString(6, complianceInfo.getNotes());
			ps.setString(7, complainceInfoHashCode);
			ps.setBoolean(8, true);
			ps.setLong(9, complianceInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kyclId
	 * @param financialInfoHashCode
	 * @throws SQLException
	 */
	public void updateFinancialInfo(FinancialInfo financialInfo, long kyclId, String financialInfoHashCode)
			throws SQLException {
		String sql = "UPDATE kycl_financial_info SET "
				+ "kycl_info_id=?,pan_gir_of_customer=?,currency_of_customer=?,notes=?,hash=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, financialInfo.getNotes());
			ps.setString(5, financialInfoHashCode);
			ps.setLong(6, financialInfo.getId());
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param screeningId
	 * @return
	 * @throws SQLException
	 */
	// public Long getAccountLegalIdByScreeningId(long screeningId) throws
	// SQLException {
	// String sql = "SELECT id FROM accounts_l_request WHERE
	// screening_l_request_id =?";
	// Connection connection = dbConnection.getConnection();
	// PreparedStatement ps = connection.prepareStatement(sql);
	// ps.setLong(1, screeningId);
	// ResultSet rs = ps.executeQuery();
	// long accountLegalId = 0;
	// while (rs.next()) {
	// accountLegalId = rs.getLong(1);
	// }
	// connection.close();
	// ps.close();
	// return accountLegalId;
	// }

	/**
	 * @param accountLId
	 * @throws SQLException
	 */
	public void updateAccountLegalWorkflow(Long accountLId) throws SQLException {
		String sql = "UPDATE accounts_l_workflow SET " + "kycn_saved=? WHERE accounts_l_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, accountLId);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param kyclId
	 * @param screeningId
	 * @throws SQLException
	 */
	public void updateCommentTableInCaseOfFoward(long kyclId, long screeningId) throws SQLException {
		String sql = "UPDATE screening_l_action_forward_to set kycl_id=? WHERE screening_request_l_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kyclId);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}

	/**
	 * @param kyclId
	 * @throws SQLException
	 */
	public void insertIntoKyclApprovedTable(long kyclId) throws SQLException {
		PreparedStatement ps = null;
		String sql = "INSERT INTO  kycl_approved (kycl_info_id) VALUES(?) ";
		Connection connection = dbConnection.getConnection();
		ps = connection.prepareStatement(sql);
		ps.setLong(1, kyclId);
		ps.executeUpdate();
		ps.close();
		connection.close();

	}

	/**
	 * @param screeningId
	 * @param kyclSaved
	 * @throws SQLException
	 */
	public void updateScreeningLegalWorkflowTable(long screeningId, boolean kyclSaved) throws SQLException {
		String sql = "UPDATE screening_l_workflow set kycl_saved=? WHERE screening_l_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, kyclSaved);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateScreeningLegalWorkflow(long screeningId) throws SQLException {
		String sql = "UPDATE screening_l_workflow set kycl_refresh=? WHERE screening_l_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	// public void updateCardSubscribedIndividual(String individual, long
	// kyclId) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// public void updateServiceSubscribedIndividual(String service, long
	// kyclId) {
	// // TODO Auto-generated method stub
	//
	// }

	
	public void deleteCardSubscribedIndividual(long KyclId) throws SQLException {
		String sql = "DELETE FROM kycl_cards_subscribed_individual  WHERE kycl_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, KyclId);
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}
	
	public void deleteServiceSubscribedIndividual(long KyclId) throws SQLException {
		String sql = "DELETE FROM kycl_service_subscribed_individual  WHERE kycl_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, KyclId);
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}
}
