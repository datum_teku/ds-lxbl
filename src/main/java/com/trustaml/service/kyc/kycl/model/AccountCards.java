package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountCards {
	private long id;
	
	@JsonProperty("cards_subscribed")
	String cardsSubscribed;
	
	@JsonProperty("notes")
	String notes;

	public AccountCards(long id, String cardsSubscribed, String notes) {
		super();
		this.id = id;
		this.cardsSubscribed = cardsSubscribed;
		this.notes = notes;
	}

	public AccountCards() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCardsSubscribed() {
		return cardsSubscribed;
	}

	public void setCardsSubscribed(String cardsSubscribed) {
		this.cardsSubscribed = cardsSubscribed;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
