package com.trustaml.service.kyc.kycl.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountInfo {

	@JsonProperty("account_id")
	String accountId;

	@JsonProperty("for_account_id")
	String forAccountId;

	@JsonProperty("currency_of_account")
	String currencyOfAccount;

	@JsonProperty("account_no")
	String accountNo;

	@JsonProperty("account_name")
	String accountName;

	@JsonProperty("account_short_name")
	String accountShortName;

	@JsonProperty("account_ownership")
	String accountOwnership;

	@JsonProperty("scheme_type")
	String schemeType;

	@JsonProperty("scheme_code")
	String schemeCode;

	@JsonProperty("gl_sub_head_code")
	String glSubHeadCode;

	@JsonProperty("product_group")
	String productGroup;

	@JsonProperty("last_transaction_date")
	String lastTransactionDate;

	@JsonProperty("account_open_date")
	String accountOpenDate;

	@JsonProperty("estimated_yearly_transactions")
	long estimatedYearlyTransactions;

	@JsonProperty("estimated_monthly_transactions")
	long estimatedMonthlyTransactions;

	@JsonProperty("estimated_yearly_turnover")
	long estimatedYearlyTurnover;

	@JsonProperty("estimated_monthly_turnover")
	long estimatedMonthlyTurnover;

	@JsonProperty("regular_source_of_income")
	String regularSourceOfIncome;

	@JsonProperty("source_of_fund")
	String sourceOfFund;

	@JsonProperty("notes")
	String notes;

	@JsonProperty("nature_of_account")
	String natureOfAccount;

	@JsonProperty("scheme_description")
	String schemeDescription;

	@JsonProperty("dr_balance_limit")
	String drBalanceLimit;

	@JsonProperty("deposite_amount")
	String depositeAmount;

	@JsonProperty("customer_pan")
	String customerPan;

	@JsonProperty("customer_currency")
	String customerCurrency;
	
	@JsonProperty("is_blocked")
	String isBlocked;
	
	@JsonProperty("accounts_related_person")
 	private List<RelatedPerson> accountsRelatedPersonList;
	
	@JsonProperty("account_status_type")
	private String accountStatusType;
	
	@JsonProperty("account_type")
	private String accountType;
	
	
	@JsonProperty("goaml_account_status_type")
	private String goAMLAccountStatusType;
	
	@JsonProperty("goaml_account_type")
	private String goAMLaccountType;
	
	@JsonProperty("branch_sol_id")
	private String branchSolId;

	private long id;

	@JsonProperty("change")
	boolean change;
	
	@JsonProperty("is_active")
	String removedAccountStatus;

	public AccountInfo() {
		super();
		this.estimatedYearlyTransactions = 0L;
		this.estimatedMonthlyTransactions = 0L;
		this.estimatedYearlyTurnover = 0L;
		this.estimatedMonthlyTurnover = 0L;
		// TODO Auto-generated constructor stub
	}

	public AccountInfo(String accountId, String forAccountId, String currencyOfAccount, String accountNo,
			String accountName, String accountShortName, String accountOwnership, String schemeType, String schemeCode,
			String glSubHeadCode, String productGroup, String lastTransactionDate, String accountOpenDate,
			long estimatedYearlyTransactions, long estimatedMonthlyTransactions, long estimatedYearlyTurnover,
			long estimatedMonthlyTurnover, String regularSourceOfIncome, String sourceOfFund, String notes,
			String natureOfAccount, String schemeDescription, String drBalanceLimit, String depositeAmount,
			String customerPan, String customerCurrency, long id, boolean change) {
		super();
		this.accountId = accountId;
		this.forAccountId = forAccountId;
		this.currencyOfAccount = currencyOfAccount;
		this.accountNo = accountNo;
		this.accountName = accountName;
		this.accountShortName = accountShortName;
		this.accountOwnership = accountOwnership;
		this.schemeType = schemeType;
		this.schemeCode = schemeCode;
		this.glSubHeadCode = glSubHeadCode;
		this.productGroup = productGroup;
		this.lastTransactionDate = lastTransactionDate;
		this.accountOpenDate = accountOpenDate;
		this.estimatedYearlyTransactions = estimatedYearlyTransactions;
		this.estimatedMonthlyTransactions = estimatedMonthlyTransactions;
		this.estimatedYearlyTurnover = estimatedYearlyTurnover;
		this.estimatedMonthlyTurnover = estimatedMonthlyTurnover;
		this.regularSourceOfIncome = regularSourceOfIncome;
		this.sourceOfFund = sourceOfFund;
		this.notes = notes;
		this.natureOfAccount = natureOfAccount;
		this.schemeDescription = schemeDescription;
		this.drBalanceLimit = drBalanceLimit;
		this.depositeAmount = depositeAmount;
		this.customerPan = customerPan;
		this.customerCurrency = customerCurrency;
		this.id = id;
		this.change = change;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getForAccountId() {
		return forAccountId;
	}

	public void setForAccountId(String forAccountId) {
		this.forAccountId = forAccountId;
	}

	public String getCurrencyOfAccount() {
		return currencyOfAccount;
	}

	public void setCurrencyOfAccount(String currencyOfAccount) {
		this.currencyOfAccount = currencyOfAccount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountShortName() {
		return accountShortName;
	}

	public void setAccountShortName(String accountShortName) {
		this.accountShortName = accountShortName;
	}

	public String getAccountOwnership() {
		return accountOwnership;
	}

	public void setAccountOwnership(String accountOwnership) {
		this.accountOwnership = accountOwnership;
	}

	public String getSchemeType() {
		return schemeType;
	}

	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public String getGlSubHeadCode() {
		return glSubHeadCode;
	}

	public void setGlSubHeadCode(String glSubHeadCode) {
		this.glSubHeadCode = glSubHeadCode;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getLastTransactionDate() {
		return lastTransactionDate;
	}

	public void setLastTransactionDate(String lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	public String getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public long getEstimatedYearlyTransactions() {
		return estimatedYearlyTransactions;
	}

	public void setEstimatedYearlyTransactions(long estimatedYearlyTransactions) {
		this.estimatedYearlyTransactions = estimatedYearlyTransactions;
	}

	public long getEstimatedMonthlyTransactions() {
		return estimatedMonthlyTransactions;
	}

	public void setEstimatedMonthlyTransactions(long estimatedMonthlyTransactions) {
		this.estimatedMonthlyTransactions = estimatedMonthlyTransactions;
	}

	public long getEstimatedYearlyTurnover() {
		return estimatedYearlyTurnover;
	}

	public void setEstimatedYearlyTurnover(long estimatedYearlyTurnover) {
		this.estimatedYearlyTurnover = estimatedYearlyTurnover;
	}

	public long getEstimatedMonthlyTurnover() {
		return estimatedMonthlyTurnover;
	}

	public void setEstimatedMonthlyTurnover(long estimatedMonthlyTurnover) {
		this.estimatedMonthlyTurnover = estimatedMonthlyTurnover;
	}

	public String getRegularSourceOfIncome() {
		return regularSourceOfIncome;
	}

	public void setRegularSourceOfIncome(String regularSourceOfIncome) {
		this.regularSourceOfIncome = regularSourceOfIncome;
	}

	public String getSourceOfFund() {
		return sourceOfFund;
	}

	public void setSourceOfFund(String sourceOfFund) {
		this.sourceOfFund = sourceOfFund;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNatureOfAccount() {
		return natureOfAccount;
	}

	public void setNatureOfAccount(String natureOfAccount) {
		this.natureOfAccount = natureOfAccount;
	}

	public String getSchemeDescription() {
		return schemeDescription;
	}

	public void setSchemeDescription(String schemeDescription) {
		this.schemeDescription = schemeDescription;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDrBalanceLimit() {
		return drBalanceLimit;
	}

	public void setDrBalanceLimit(String drBalanceLimit) {
		this.drBalanceLimit = drBalanceLimit;
	}

	public String getDepositeAmount() {
		return depositeAmount;
	}

	public void setDepositeAmount(String depositeAmount) {
		this.depositeAmount = depositeAmount;
	}

	public String getCustomerPan() {
		return customerPan;
	}

	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}

	public String getCustomerCurrency() {
		return customerCurrency;
	}

	public void setCustomerCurrency(String customerCurrency) {
		this.customerCurrency = customerCurrency;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(String isBlocked) {
		this.isBlocked = isBlocked;
	}

	
	public List<RelatedPerson> getAccountsRelatedPersonList() {
		return accountsRelatedPersonList;
	}

	public void setAccountsRelatedPersonList(List<RelatedPerson> accountsRelatedPersonList) {
		this.accountsRelatedPersonList = accountsRelatedPersonList;
	}

	public String getAccountStatusType() {
		return accountStatusType;
	}

	public void setAccountStatusType(String accountStatusType) {
		this.accountStatusType = accountStatusType;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getGoAMLAccountStatusType() {
		return goAMLAccountStatusType;
	}

	public void setGoAMLAccountStatusType(String goAMLAccountStatusType) {
		this.goAMLAccountStatusType = goAMLAccountStatusType;
	}

	public String getGoAMLaccountType() {
		return goAMLaccountType;
	}

	public void setGoAMLaccountType(String goAMLaccountType) {
		this.goAMLaccountType = goAMLaccountType;
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public String getRemovedAccountStatus() {
		return removedAccountStatus;
	}

	public void setRemovedAccountStatus(String removedAccountStatus) {
		this.removedAccountStatus = removedAccountStatus;
	}
	
	
}
