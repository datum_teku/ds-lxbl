package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountService {
	private long id;
	
	@JsonProperty("service_subscribed")
	String serviceSubscribed;
	
	@JsonProperty("notes")
	String notes;

	public AccountService(long id, String serviceSubscribed, String notes) {
		super();
		this.id = id;
		this.serviceSubscribed = serviceSubscribed;
		this.notes = notes;
	}

	public AccountService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServiceSubscribed() {
		return serviceSubscribed;
	}

	public void setServiceSubscribed(String serviceSubscribed) {
		this.serviceSubscribed = serviceSubscribed;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
