package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {

	@JsonProperty("address_type")
	String addressType;

	@JsonProperty("country")
	String country;

	@JsonProperty("zone")
	String zone;

	@JsonProperty("district")
	String district;

	@JsonProperty("mn_vdc")
	String mnVdc;

	@JsonProperty("pinzip")
	String pinzip;

	@JsonProperty("ward_number")
	String wardNumber;

	@JsonProperty("tole_area")
	String toleArea;

	@JsonProperty("street")
	String street;

	@JsonProperty("house_no")
	String houseNo;

	@JsonProperty("unit_number")
	String unitNumber;

	@JsonProperty("nearest_landmark")
	String nearestLandmark;

	@JsonProperty("latitude")
	String latitude;

	@JsonProperty("longitude")
	String longitude;

	@JsonProperty("phone_no_country_code")
	String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	String phoneNoAreaCode;

	@JsonProperty("phone_no")
	String phoneNo;

	@JsonProperty("telex_no_country_code")
	String telexNoCountryCode;

	@JsonProperty("telex_no_area_code")
	String telexNoAreaCode;

	@JsonProperty("telex_no")
	String telexNo;

	@JsonProperty("pager_no_country_code")
	String pagerNoCountryCode;

	@JsonProperty("pager_no_area_code")
	String pagerNoAreaCode;

	@JsonProperty("pager_no")
	String pagerNo;

	@JsonProperty("email_id")
	String emailId;

	@JsonProperty("notes")
	String notes;

	@JsonProperty("state")
	String state;

	private long id;

	@JsonProperty("change")
	boolean change;

	@JsonProperty("province")
	private String province;
	
	@JsonProperty("city")
	private String city;

	@JsonProperty("address")
	private String address;
	
	@JsonProperty("goaml_country")
	private String goAMLCountryType;
	
	@JsonProperty("goaml_address_type")
	String goAMLAddressType;
	
	@JsonProperty("is_active")
	String removedAddressStatus;
	
	@JsonProperty("address_removed_by")
	String addressRemovedBy;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(String addressType, String country, String zone, String district, String mnVdc, String pinzip,
			String wardNumber, String toleArea, String street, String houseNo, String unitNumber,
			String nearestLandmark, String latitude, String longitude, String phoneNoCountryCode,
			String phoneNoAreaCode, String phoneNo, String telexNoCountryCode, String telexNoAreaCode, String telexNo,
			String pagerNoCountryCode, String pagerNoAreaCode, String pagerNo, String emailId, String notes,
			String state, long id, boolean change) {
		super();
		this.addressType = addressType;
		this.country = country;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.pinzip = pinzip;
		this.wardNumber = wardNumber;
		this.toleArea = toleArea;
		this.street = street;
		this.houseNo = houseNo;
		this.unitNumber = unitNumber;
		this.nearestLandmark = nearestLandmark;
		this.latitude = latitude;
		this.longitude = longitude;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.pagerNoCountryCode = pagerNoCountryCode;
		this.pagerNoAreaCode = pagerNoAreaCode;
		this.pagerNo = pagerNo;
		this.emailId = emailId;
		this.notes = notes;
		this.state = state;
		this.id = id;
		this.change = change;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getPinzip() {
		return pinzip;
	}

	public void setPinzip(String pinzip) {
		this.pinzip = pinzip;
	}

	public String getWardNumber() {
		return wardNumber;
	}

	public void setWardNumber(String wardNumber) {
		this.wardNumber = wardNumber;
	}

	public String getToleArea() {
		return toleArea;
	}

	public void setToleArea(String toleArea) {
		this.toleArea = toleArea;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getNearestLandmark() {
		return nearestLandmark;
	}

	public void setNearestLandmark(String nearestLandmark) {
		this.nearestLandmark = nearestLandmark;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getPagerNoCountryCode() {
		return pagerNoCountryCode;
	}

	public void setPagerNoCountryCode(String pagerNoCountryCode) {
		this.pagerNoCountryCode = pagerNoCountryCode;
	}

	public String getPagerNoAreaCode() {
		return pagerNoAreaCode;
	}

	public void setPagerNoAreaCode(String pagerNoAreaCode) {
		this.pagerNoAreaCode = pagerNoAreaCode;
	}

	public String getPagerNo() {
		return pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGoAMLCountryType() {
		return goAMLCountryType;
	}

	public void setGoAMLCountryType(String goAMLCountryType) {
		this.goAMLCountryType = goAMLCountryType;
	}

	public String getGoAMLAddressType() {
		return goAMLAddressType;
	}

	public void setGoAMLAddressType(String goAMLAddressType) {
		this.goAMLAddressType = goAMLAddressType;
	}

	public String getRemovedAddressStatus() {
		return removedAddressStatus;
	}

	public void setRemovedAddressStatus(String removedAddressStatus) {
		this.removedAddressStatus = removedAddressStatus;
	}

	public String getAddressRemovedBy() {
		return addressRemovedBy;
	}

	public void setAddressRemovedBy(String addressRemovedBy) {
		this.addressRemovedBy = addressRemovedBy;
	}
	
	
	
	

}
