package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuditInfo {

	private long id;

	@JsonProperty("audited_report")
	String auditedReport;

	@JsonProperty("authorized_letter")
	String authorizedLetter;

	@JsonProperty("authorized_letter_date")
	String authorizedLetterDate;

	@JsonProperty("authorized_letter_agency")
	String authorizedLetterAgency;

	@JsonProperty("notes")
	String notes;

	@JsonProperty("change")
	boolean change;

	public AuditInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuditInfo(long id, String auditedReport, String authorizedLetter, String authorizedLetterDate,
			String authorizedLetterAgency, String notes, boolean change) {
		super();
		this.id = id;
		this.auditedReport = auditedReport;
		this.authorizedLetter = authorizedLetter;
		this.authorizedLetterDate = authorizedLetterDate;
		this.authorizedLetterAgency = authorizedLetterAgency;
		this.notes = notes;
		this.change = change;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuditedReport() {
		return auditedReport;
	}

	public void setAuditedReport(String auditedReport) {
		this.auditedReport = auditedReport;
	}

	public String getAuthorizedLetter() {
		return authorizedLetter;
	}

	public void setAuthorizedLetter(String authorizedLetter) {
		this.authorizedLetter = authorizedLetter;
	}

	public String getAuthorizedLetterDate() {
		return authorizedLetterDate;
	}

	public void setAuthorizedLetterDate(String authorizedLetterDate) {
		this.authorizedLetterDate = authorizedLetterDate;
	}

	public String getAuthorizedLetterAgency() {
		return authorizedLetterAgency;
	}

	public void setAuthorizedLetterAgency(String authorizedLetterAgency) {
		this.authorizedLetterAgency = authorizedLetterAgency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
