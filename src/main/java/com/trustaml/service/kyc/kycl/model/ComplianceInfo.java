package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ComplianceInfo {

	@JsonProperty("level_of_compliance_on_aml")
	String levelOfComplianceOnAml;

	@JsonProperty("level_of_compliance_on_tax")
	String levelOfComplianceOnTax;

	@JsonProperty("level_of_compliance_on_corruption")
	String levelOfComplianceOnCorruption;

	@JsonProperty("level_of_compliance_on_others")
	String levelOfComplianceOnOthers;

	@JsonProperty("notes")
	String notes;

	private long id;

	@JsonProperty("change")
	boolean change;

	public ComplianceInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ComplianceInfo(String levelOfComplianceOnAml, String levelOfComplianceOnTax,
			String levelOfComplianceOnCorruption, String levelOfComplianceOnOthers, String notes, long id,
			boolean change) {
		super();
		this.levelOfComplianceOnAml = levelOfComplianceOnAml;
		this.levelOfComplianceOnTax = levelOfComplianceOnTax;
		this.levelOfComplianceOnCorruption = levelOfComplianceOnCorruption;
		this.levelOfComplianceOnOthers = levelOfComplianceOnOthers;
		this.notes = notes;
		this.id = id;
		this.change = change;
	}

	public String getLevelOfComplianceOnAml() {
		return levelOfComplianceOnAml;
	}

	public void setLevelOfComplianceOnAml(String levelOfComplianceOnAml) {
		this.levelOfComplianceOnAml = levelOfComplianceOnAml;
	}

	public String getLevelOfComplianceOnTax() {
		return levelOfComplianceOnTax;
	}

	public void setLevelOfComplianceOnTax(String levelOfComplianceOnTax) {
		this.levelOfComplianceOnTax = levelOfComplianceOnTax;
	}

	public String getLevelOfComplianceOnCorruption() {
		return levelOfComplianceOnCorruption;
	}

	public void setLevelOfComplianceOnCorruption(String levelOfComplianceOnCorruption) {
		this.levelOfComplianceOnCorruption = levelOfComplianceOnCorruption;
	}

	public String getLevelOfComplianceOnOthers() {
		return levelOfComplianceOnOthers;
	}

	public void setLevelOfComplianceOnOthers(String levelOfComplianceOnOthers) {
		this.levelOfComplianceOnOthers = levelOfComplianceOnOthers;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
