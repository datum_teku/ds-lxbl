package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocumentStatus {

	@JsonProperty("document_type")
	String documentType;

	@JsonProperty("document_status")
	String documentStatus;

	@JsonProperty("application_submitted_date")
	String applictionSubmittedDate;

	@JsonProperty("refresh_date")
	String refreshDate;

	@JsonProperty("notes")
	String notes;

	private long id;

	@JsonProperty("change")
	boolean change;

	public DocumentStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DocumentStatus(String documentType, String documentStatus, String applictionSubmittedDate,
			String refreshDate, String notes, long id, boolean change) {
		super();
		this.documentType = documentType;
		this.documentStatus = documentStatus;
		this.applictionSubmittedDate = applictionSubmittedDate;
		this.refreshDate = refreshDate;
		this.notes = notes;
		this.id = id;
		this.change = change;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getApplictionSubmittedDate() {
		return applictionSubmittedDate;
	}

	public void setApplictionSubmittedDate(String applictionSubmittedDate) {
		this.applictionSubmittedDate = applictionSubmittedDate;
	}

	public String getRefreshDate() {
		return refreshDate;
	}

	public void setRefreshDate(String refreshDate) {
		this.refreshDate = refreshDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
