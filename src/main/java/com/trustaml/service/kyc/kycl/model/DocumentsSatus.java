package com.trustaml.service.kyc.kycl.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocumentsSatus {

	@JsonProperty("document_type")
	String documentType;
	@JsonProperty("document_status")
	String documentStatus;
	@JsonProperty("appliction_submitted_date")
	java.sql.Date applictionSubmittedDate;
	@JsonProperty("refresh_date")
	java.sql.Date refreshDate;
	@JsonProperty("notes")
	String notes;
	private long id;

	public DocumentsSatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DocumentsSatus(String documentType, String documentStatus, Date applictionSubmittedDate, Date refreshDate,
			String notes, long id) {
		super();
		this.documentType = documentType;
		this.documentStatus = documentStatus;
		this.applictionSubmittedDate = applictionSubmittedDate;
		this.refreshDate = refreshDate;
		this.notes = notes;
		this.id = id;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public java.sql.Date getApplictionSubmittedDate() {
		return applictionSubmittedDate;
	}

	public void setApplictionSubmittedDate(java.sql.Date applictionSubmittedDate) {
		this.applictionSubmittedDate = applictionSubmittedDate;
	}

	public java.sql.Date getRefreshDate() {
		return refreshDate;
	}

	public void setRefreshDate(java.sql.Date refreshDate) {
		this.refreshDate = refreshDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
