package com.trustaml.service.kyc.kycl.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinancialInfo {

	private long id;

	@JsonProperty("customer_pan")
	private String customerPan;

	@JsonProperty("customer_currency")
	private String customerCurrency;

	@JsonProperty("individual_services_subscribed")
	private List<String> individualServiceSubscribed;

	@JsonProperty("individual_cards_subscribed")
	private List<String> individualCardSubscribed;

	@JsonProperty("change")
	private boolean change;

	@JsonProperty("notes")
	private String notes;

	public FinancialInfo(long id, String customerPan, String customerCurrency, List<String> individualServiceSubscribed,
			List<String> individualCardSubscribed, boolean change, String notes) {
		super();
		this.id = id;
		this.customerPan = customerPan;
		this.customerCurrency = customerCurrency;
		this.individualServiceSubscribed = individualServiceSubscribed;
		this.individualCardSubscribed = individualCardSubscribed;
		this.change = change;
		this.notes = notes;
	}

	public FinancialInfo() {
		super();
	}

	public String getCustomerPan() {
		return customerPan;
	}

	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}

	public String getCustomerCurrency() {
		return customerCurrency;
	}

	public void setCustomerCurrency(String customerCurrency) {
		this.customerCurrency = customerCurrency;
	}

	public List<String> getIndividualServiceSubscribed() {
		return individualServiceSubscribed;
	}

	public void setIndividualServiceSubscribed(List<String> individualServiceSubscribed) {
		this.individualServiceSubscribed = individualServiceSubscribed;
	}

	public List<String> getIndividualCardSubscribed() {
		return individualCardSubscribed;
	}

	public void setIndividualCardSubscribed(List<String> individualCardSubscribed) {
		this.individualCardSubscribed = individualCardSubscribed;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
