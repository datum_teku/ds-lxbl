package com.trustaml.service.kyc.kycl.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class Kycl {

	@JsonProperty("kycl-info")
	private Legal legal;

	// @JsonProperty("account_id")
	// private List<AccountCards> listAccountCard;

	@JsonProperty("accounts-info")
	private List<AccountInfo> listAccountInfo;

	// @JsonProperty("account_id")
	// private List<AccountService> listAccountService;

	@JsonProperty("address-info")
	private List<Address> listAddress;

	@JsonProperty("audit-info")
	private List<AuditInfo> listAuditInfo;

	@JsonProperty("business-info")
	private List<BusinessInfo> listBusinessInfo;

	@JsonProperty("compliance-info")
	private ComplianceInfo complianceInfo;

	@JsonProperty("document-status-info")
	private List<DocumentStatus> listDocumentStatus;

	@JsonProperty("landlord-info")
	private List<LandlordInfo> listLandlordAddress;

	@JsonProperty("registration-address-info")
	private List<RegistrationAddress> listRegistrationAddress;

	@JsonProperty("registration-info")
	private List<RegistrationInfo> listRegistrationInfo;

	@JsonProperty("related-entity-info")
	private List<RelatedEntity> listRelatedEntityInfo;

	@JsonProperty("related-person-info")
	private List<RelatedPerson> listRelatedPerson;

	@JsonProperty("user")
	private User user;

	@JsonProperty("financial-info")
	private FinancialInfo financialInfo;

	public Kycl(Legal legal, List<AccountInfo> listAccountInfo, List<Address> listAddress,
			List<AuditInfo> listAuditInfo, List<BusinessInfo> listBusinessInfo, ComplianceInfo complianceInfo,
			List<DocumentStatus> listDocumentStatus, List<LandlordInfo> listLandlordAddress,
			List<RegistrationAddress> listRegistrationAddress, List<RegistrationInfo> listRegistrationInfo,
			List<RelatedEntity> listRelatedEntityInfo, List<RelatedPerson> listRelatedPerson, User user,
			FinancialInfo financialInfo) {
		super();
		this.legal = legal;
		this.listAccountInfo = listAccountInfo;
		this.listAddress = listAddress;
		this.listAuditInfo = listAuditInfo;
		this.listBusinessInfo = listBusinessInfo;
		this.complianceInfo = complianceInfo;
		this.listDocumentStatus = listDocumentStatus;
		this.listLandlordAddress = listLandlordAddress;
		this.listRegistrationAddress = listRegistrationAddress;
		this.listRegistrationInfo = listRegistrationInfo;
		this.listRelatedEntityInfo = listRelatedEntityInfo;
		this.listRelatedPerson = listRelatedPerson;
		this.user = user;
		this.financialInfo = financialInfo;
	}

	public Kycl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Legal getLegal() {
		return legal;
	}

	public void setLegal(Legal legal) {
		this.legal = legal;
	}

	public List<AccountInfo> getListAccountInfo() {
		return listAccountInfo;
	}

	public void setListAccountInfo(List<AccountInfo> listAccountInfo) {
		this.listAccountInfo = listAccountInfo;
	}

	public List<Address> getListAddress() {
		return listAddress;
	}

	public void setListAddress(List<Address> listAddress) {
		this.listAddress = listAddress;
	}

	public List<AuditInfo> getListAuditInfo() {
		return listAuditInfo;
	}

	public void setListAuditInfo(List<AuditInfo> listAuditInfo) {
		this.listAuditInfo = listAuditInfo;
	}

	public List<BusinessInfo> getListBusinessInfo() {
		return listBusinessInfo;
	}

	public void setListBusinessInfo(List<BusinessInfo> listBusinessInfo) {
		this.listBusinessInfo = listBusinessInfo;
	}

	public ComplianceInfo getComplianceInfo() {
		return complianceInfo;
	}

	public void setComplianceInfo(ComplianceInfo complianceInfo) {
		this.complianceInfo = complianceInfo;
	}

	public List<DocumentStatus> getListDocumentStatus() {
		return listDocumentStatus;
	}

	public void setListDocumentStatus(List<DocumentStatus> listDocumentStatus) {
		this.listDocumentStatus = listDocumentStatus;
	}

	public List<LandlordInfo> getListLandlordAddress() {
		return listLandlordAddress;
	}

	public void setListLandlordAddress(List<LandlordInfo> listLandlordAddress) {
		this.listLandlordAddress = listLandlordAddress;
	}

	public List<RegistrationAddress> getListRegistrationAddress() {
		return listRegistrationAddress;
	}

	public void setListRegistrationAddress(List<RegistrationAddress> listRegistrationAddress) {
		this.listRegistrationAddress = listRegistrationAddress;
	}

	public List<RegistrationInfo> getListRegistrationInfo() {
		return listRegistrationInfo;
	}

	public void setListRegistrationInfo(List<RegistrationInfo> listRegistrationInfo) {
		this.listRegistrationInfo = listRegistrationInfo;
	}

	public List<RelatedEntity> getListRelatedEntityInfo() {
		return listRelatedEntityInfo;
	}

	public void setListRelatedEntityInfo(List<RelatedEntity> listRelatedEntityInfo) {
		this.listRelatedEntityInfo = listRelatedEntityInfo;
	}

	public List<RelatedPerson> getListRelatedPerson() {
		return listRelatedPerson;
	}

	public void setListRelatedPerson(List<RelatedPerson> listRelatedPerson) {
		this.listRelatedPerson = listRelatedPerson;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public FinancialInfo getFinancialInfo() {
		return financialInfo;
	}

	public void setFinancialInfo(FinancialInfo financialInfo) {
		this.financialInfo = financialInfo;
	}

	@Override
	public String toString() {
		return "Kycl [legal=" + legal + ", listAccountInfo=" + listAccountInfo + ", listAddress=" + listAddress
				+ ", listAuditInfo=" + listAuditInfo + ", listBusinessInfo=" + listBusinessInfo + ", complianceInfo="
				+ complianceInfo + ", listDocumentStatus=" + listDocumentStatus + ", listLandlordAddress="
				+ listLandlordAddress + ", listRegistrationAddress=" + listRegistrationAddress
				+ ", listRegistrationInfo=" + listRegistrationInfo + ", listRelatedEntityInfo=" + listRelatedEntityInfo
				+ ", listRelatedPerson=" + listRelatedPerson + ", user=" + user + ", financialInfo=" + financialInfo
				+ "]";
	}

}
