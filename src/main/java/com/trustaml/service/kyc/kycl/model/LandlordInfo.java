package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LandlordInfo {

	@JsonProperty("land_lord_first_name")
	private String landLordFirstName;

	@JsonProperty("land_lord_middle_name")
	private String landLordMiddleName;

	@JsonProperty("land_lord_last_name")
	private String landLordLastName;

	@JsonProperty("country")
	private String country;

	@JsonProperty("province")
	private String province;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("ward_no")
	private String wardNo;

	@JsonProperty("town_city")
	private String townCity;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("telex_no_country_code")
	private String telexNoCountryCode;

	@JsonProperty("telex_no_area_code")
	private String telexNoAreaCode;

	@JsonProperty("telex_no")
	private String telexNo;

	@JsonProperty("pager_no_country_code")
	private String pagerNoCountryCode;

	@JsonProperty("pager_no_area_code")
	private String pagerNoAreaCode;

	@JsonProperty("pager_no")
	private String pagerNo;

	@JsonProperty("email_id")
	private String emailId;
	
	@JsonProperty("zone")
	private String zone;

	private long id;

	@JsonProperty("change")
	boolean change;

	public LandlordInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LandlordInfo(String landLordFirstName, String landLordMiddleName, String landLordLastName, String country,
			String province, String district, String mnVdc, String wardNo, String townCity, String notes,
			String phoneNoCountryCode, String phoneNoAreaCode, String phoneNo, String telexNoCountryCode,
			String telexNoAreaCode, String telexNo, String pagerNoCountryCode, String pagerNoAreaCode, String pagerNo,
			String emailId, long id, boolean change) {
		super();
		this.landLordFirstName = landLordFirstName;
		this.landLordMiddleName = landLordMiddleName;
		this.landLordLastName = landLordLastName;
		this.country = country;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.wardNo = wardNo;
		this.townCity = townCity;
		this.notes = notes;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.pagerNoCountryCode = pagerNoCountryCode;
		this.pagerNoAreaCode = pagerNoAreaCode;
		this.pagerNo = pagerNo;
		this.emailId = emailId;
		this.id = id;
		this.change = change;
	}

	public String getLandLordFirstName() {
		return landLordFirstName;
	}

	public void setLandLordFirstName(String landLordFirstName) {
		this.landLordFirstName = landLordFirstName;
	}

	public String getLandLordMiddleName() {
		return landLordMiddleName;
	}

	public void setLandLordMiddleName(String landLordMiddleName) {
		this.landLordMiddleName = landLordMiddleName;
	}

	public String getLandLordLastName() {
		return landLordLastName;
	}

	public void setLandLordLastName(String landLordLastName) {
		this.landLordLastName = landLordLastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getTownCity() {
		return townCity;
	}

	public void setTownCity(String townCity) {
		this.townCity = townCity;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getPagerNoCountryCode() {
		return pagerNoCountryCode;
	}

	public void setPagerNoCountryCode(String pagerNoCountryCode) {
		this.pagerNoCountryCode = pagerNoCountryCode;
	}

	public String getPagerNoAreaCode() {
		return pagerNoAreaCode;
	}

	public void setPagerNoAreaCode(String pagerNoAreaCode) {
		this.pagerNoAreaCode = pagerNoAreaCode;
	}

	public String getPagerNo() {
		return pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	
	
}
