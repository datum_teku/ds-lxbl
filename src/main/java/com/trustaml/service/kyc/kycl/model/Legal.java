package com.trustaml.service.kyc.kycl.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Legal {

	@JsonProperty("primary_sol_id")
	private String primarySolId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("name_of_the_institution")
	private String nameOfTheInstitution;

	@JsonProperty("ls_name")
	private String lsName;

	@JsonProperty("cust_short_name")
	private String custShortName;

	@JsonProperty("date_of_establishment")
	private String dateOfEstablishment;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("customer_type")
	private String customerType;

	@JsonProperty("customer_group")
	private String customerGroup;

	@JsonProperty("customer_constitution")
	private String customerConstitution;

	@JsonProperty("customer_community")
	private String customerCommunity;

	@JsonProperty("customer_employee_id")
	private String customerEmployeeId;

	@JsonProperty("customer_open_date")
	private String customerOpenDate;

	@JsonProperty("customer_maker")
	private String customerMaker;

	@JsonProperty("screening_id")
	long screeningId;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("minor")
	boolean minor;

	@JsonProperty("customer_status_code")
	String customerStatusCode;

	@JsonProperty("card_holder")
	boolean cardHolder;

	@JsonProperty("non_resident_external")
	boolean nonResidentExternal;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("kycl_status")
	private String kyclStatus;

	@JsonProperty("last_update_date")
	private String lastUpdateDate;

	@JsonProperty("last_screened_date")
	private String lastScreenedDate;

	@JsonProperty("verified_record")
	boolean verifiedRecord;

	@JsonProperty("audited_fiscal_year")
	private String auditedFiscalYear;

	@JsonProperty("net_profit")
	private String netProfit;

	@JsonProperty("net_loss")
	private String netLoss;

	@JsonProperty("tax_clearence_certificate_date")
	private String taxClearenceCertificateDate;

	@JsonProperty("tax_clearence_fiscal_year")
	private String taxClearenceFiscalYear;

	@JsonProperty("next_clearence_certificate_date")
	private String nextClearenceCertificateDate;

	@JsonProperty("taxable_amount")
	private String taxableAmount;

	@JsonProperty("taxable_income")
	private String taxableIncome;

	@JsonProperty("tax_paid")
	private String taxPaid;

	@JsonProperty("tax_paid_date")
	private String taxPaidDate;

	@JsonProperty("types_of_industry")
	private String typeOfIndustry;

	@JsonProperty("change")
	boolean change;

	@JsonProperty("date_of_establishment_bs")
	private Date dateOfEstablishmentBS;
	
	@JsonProperty("goaml_customer_constitution")
	private String goAMLCustConst;
	
	@JsonProperty("incomplete")
	private boolean incomplete;
	
	@JsonProperty("incorporation_country")
	private String incorporationCountry;
	
	@JsonProperty("goaml_incorporation_country")
	private String goAMLIncorporationCountry;
	
	@JsonProperty("incorporation_province")
	private String incorporationProvince;
	
	@JsonProperty("business_description")
	private String businessDescription ;
	
	@JsonProperty("is_primary")
	private boolean isPrimary ;
	

	long id;

	public Legal() {
		super();
		this.customerOpenDate = "";
		this.taxClearenceCertificateDate = "";
		this.nextClearenceCertificateDate = "";
		this.taxPaidDate = "";
		this.dateOfEstablishment = "";
		this.taxClearenceFiscalYear = "";
		this.lastUpdateDate = "";
		// TODO Auto-generated constructor stub
	}

	public Legal(String primarySolId, String salutation, String nameOfTheInstitution, String lsName,
			String custShortName, String dateOfEstablishment, String notes, String customerType, String customerGroup,
			String customerConstitution, String customerCommunity, String customerEmployeeId, String customerOpenDate,
			String customerMaker, long screeningId, String panNumber, boolean minor, String customerStatusCode,
			boolean cardHolder, boolean nonResidentExternal, String custId, String kyclStatus, String lastUpdateDate,
			String lastScreenedDate, boolean verifiedRecord, String auditedFiscalYear, String netProfit, String netLoss,
			String taxClearenceCertificateDate, String taxClearenceFiscalYear, String nextClearenceCertificateDate,
			String taxableAmount, String taxableIncome, String taxPaid, String taxPaidDate, String typeOfIndustry,
			long id, boolean change) {
		super();
		this.primarySolId = primarySolId;
		this.salutation = salutation;
		this.nameOfTheInstitution = nameOfTheInstitution;
		this.lsName = lsName;
		this.custShortName = custShortName;
		this.dateOfEstablishment = dateOfEstablishment;
		this.notes = notes;
		this.customerType = customerType;
		this.customerGroup = customerGroup;
		this.customerConstitution = customerConstitution;
		this.customerCommunity = customerCommunity;
		this.customerEmployeeId = customerEmployeeId;
		this.customerOpenDate = customerOpenDate;
		this.customerMaker = customerMaker;
		this.screeningId = screeningId;
		this.panNumber = panNumber;
		this.minor = minor;
		this.customerStatusCode = customerStatusCode;
		this.cardHolder = cardHolder;
		this.nonResidentExternal = nonResidentExternal;
		this.custId = custId;
		this.kyclStatus = kyclStatus;
		this.lastUpdateDate = lastUpdateDate;
		this.lastScreenedDate = lastScreenedDate;
		this.verifiedRecord = verifiedRecord;
		this.auditedFiscalYear = auditedFiscalYear;
		this.netProfit = netProfit;
		this.netLoss = netLoss;
		this.taxClearenceCertificateDate = taxClearenceCertificateDate;
		this.taxClearenceFiscalYear = taxClearenceFiscalYear;
		this.nextClearenceCertificateDate = nextClearenceCertificateDate;
		this.taxableAmount = taxableAmount;
		this.taxableIncome = taxableIncome;
		this.taxPaid = taxPaid;
		this.taxPaidDate = taxPaidDate;
		this.typeOfIndustry = typeOfIndustry;
		this.id = id;
		this.change = change;
	}

	public String getPrimarySolId() {
		return primarySolId;
	}

	public void setPrimarySolId(String primarySolId) {
		this.primarySolId = primarySolId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getNameOfTheInstitution() {
		return nameOfTheInstitution;
	}

	public void setNameOfTheInstitution(String nameOfTheInstitution) {
		this.nameOfTheInstitution = nameOfTheInstitution;
	}

	public String getLsName() {
		return lsName;
	}

	public void setLsName(String lsName) {
		this.lsName = lsName;
	}

	public String getCustShortName() {
		return custShortName;
	}

	public void setCustShortName(String custShortName) {
		this.custShortName = custShortName;
	}

	public String getDateOfEstablishment() {
		return dateOfEstablishment;
	}

	public void setDateOfEstablishment(String dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}

	public String getCustomerConstitution() {
		return customerConstitution;
	}

	public void setCustomerConstitution(String customerConstitution) {
		this.customerConstitution = customerConstitution;
	}

	public String getCustomerCommunity() {
		return customerCommunity;
	}

	public void setCustomerCommunity(String customerCommunity) {
		this.customerCommunity = customerCommunity;
	}

	public String getCustomerEmployeeId() {
		return customerEmployeeId;
	}

	public void setCustomerEmployeeId(String customerEmployeeId) {
		this.customerEmployeeId = customerEmployeeId;
	}

	public String getCustomerOpenDate() {
		return customerOpenDate;
	}

	public void setCustomerOpenDate(String customerOpenDate) {
		this.customerOpenDate = customerOpenDate;
	}

	public String getCustomerMaker() {
		return customerMaker;
	}

	public void setCustomerMaker(String customerMaker) {
		this.customerMaker = customerMaker;
	}

	public long getScreeningId() {
		return screeningId;
	}

	public void setScreeningId(long screeningId) {
		this.screeningId = screeningId;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public boolean isMinor() {
		return minor;
	}

	public void setMinor(boolean minor) {
		this.minor = minor;
	}

	public String getCustomerStatusCode() {
		return customerStatusCode;
	}

	public void setCustomerStatusCode(String customerStatusCode) {
		this.customerStatusCode = customerStatusCode;
	}

	public boolean isCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(boolean cardHolder) {
		this.cardHolder = cardHolder;
	}

	public boolean isNonResidentExternal() {
		return nonResidentExternal;
	}

	public void setNonResidentExternal(boolean nonResidentExternal) {
		this.nonResidentExternal = nonResidentExternal;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getKyclStatus() {
		return kyclStatus;
	}

	public void setKyclStatus(String kyclStatus) {
		this.kyclStatus = kyclStatus;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastScreenedDate() {
		return lastScreenedDate;
	}

	public void setLastScreenedDate(String lastScreenedDate) {
		this.lastScreenedDate = lastScreenedDate;
	}

	public boolean isVerifiedRecord() {
		return verifiedRecord;
	}

	public void setVerifiedRecord(boolean verifiedRecord) {
		this.verifiedRecord = verifiedRecord;
	}

	public String getAuditedFiscalYear() {
		return auditedFiscalYear;
	}

	public void setAuditedFiscalYear(String auditedFiscalYear) {
		this.auditedFiscalYear = auditedFiscalYear;
	}

	public String getNetProfit() {
		return netProfit;
	}

	public void setNetProfit(String netProfit) {
		this.netProfit = netProfit;
	}

	public String getNetLoss() {
		return netLoss;
	}

	public void setNetLoss(String netLoss) {
		this.netLoss = netLoss;
	}

	public String getTaxClearenceCertificateDate() {
		return taxClearenceCertificateDate;
	}

	public void setTaxClearenceCertificateDate(String taxClearenceCertificateDate) {
		this.taxClearenceCertificateDate = taxClearenceCertificateDate;
	}

	public String getTaxClearenceFiscalYear() {
		return taxClearenceFiscalYear;
	}

	public void setTaxClearenceFiscalYear(String taxClearenceFiscalYear) {
		this.taxClearenceFiscalYear = taxClearenceFiscalYear;
	}

	public String getNextClearenceCertificateDate() {
		return nextClearenceCertificateDate;
	}

	public void setNextClearenceCertificateDate(String nextClearenceCertificateDate) {
		this.nextClearenceCertificateDate = nextClearenceCertificateDate;
	}

	public String getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(String taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public String getTaxableIncome() {
		return taxableIncome;
	}

	public void setTaxableIncome(String taxableIncome) {
		this.taxableIncome = taxableIncome;
	}

	public String getTaxPaid() {
		return taxPaid;
	}

	public void setTaxPaid(String taxPaid) {
		this.taxPaid = taxPaid;
	}

	public String getTaxPaidDate() {
		return taxPaidDate;
	}

	public void setTaxPaidDate(String taxPaidDate) {
		this.taxPaidDate = taxPaidDate;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public Date getDateOfEstablishmentBS() {
		return dateOfEstablishmentBS;
	}

	public void setDateOfEstablishmentBS(Date dateOfEstablishmentBS) {
		this.dateOfEstablishmentBS = dateOfEstablishmentBS;
	}

	public String getGoAMLCustConst() {
		return goAMLCustConst;
	}

	public void setGoAMLCustConst(String goAMLCustConst) {
		this.goAMLCustConst = goAMLCustConst;
	}

	public boolean isIncomplete() {
		return incomplete;
	}

	public void setIncomplete(boolean incomplete) {
		this.incomplete = incomplete;
	}

	public String getIncorporationCountry() {
		return incorporationCountry;
	}

	public void setIncorporationCountry(String incorporationCountry) {
		this.incorporationCountry = incorporationCountry;
	}

	public String getGoAMLIncorporationCountry() {
		return goAMLIncorporationCountry;
	}

	public void setGoAMLIncorporationCountry(String goAMLIncorporationCountry) {
		this.goAMLIncorporationCountry = goAMLIncorporationCountry;
	}

	public String getIncorporationProvince() {
		return incorporationProvince;
	}

	public void setIncorporationProvince(String incorporationProvince) {
		this.incorporationProvince = incorporationProvince;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	
	
	
	
	
}
