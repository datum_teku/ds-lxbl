package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationAddress {

	@JsonProperty("country")
	String country;

	@JsonProperty("state")
	String state;

	@JsonProperty("province")
	String province;

	@JsonProperty("district")
	String district;

	@JsonProperty("mn_vdc")
	String mnVdc;

	@JsonProperty("ward_no")
	String wardNo;

	@JsonProperty("town_city")
	String townCity;

	@JsonProperty("notes")
	String notes;

	@JsonProperty("phone_no_country_code")
	String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	String phoneNoAreaCode;

	@JsonProperty("phone_no")
	String phoneNo;

	@JsonProperty("telex_no_country_code")
	String telexNoCountryCode;

	@JsonProperty("telex_no_area_code")
	String telexNoAreaCode;

	@JsonProperty("telex_no")
	String telexNo;

	@JsonProperty("pager_no_country_code")
	String pagerNoCountryCode;

	@JsonProperty("pager_no_area_code")
	String pagerNoAreaCode;

	@JsonProperty("pager_no")
	String pagerNo;

	@JsonProperty("email_id")
	String emailId;
	
	@JsonProperty("zone")
	String zone;

	private long id;

	@JsonProperty("change")
	boolean change;
	

	public RegistrationAddress() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrationAddress(String country, String state, String province, String district, String mnVdc,
			String wardNo, String townCity, String notes, String phoneNoCountryCode, String phoneNoAreaCode,
			String phoneNo, String telexNoCountryCode, String telexNoAreaCode, String telexNo,
			String pagerNoCountryCode, String pagerNoAreaCode, String pagerNo, String emailId, long id,
			boolean change) {
		super();
		this.country = country;
		this.state = state;
		this.province = province;
		this.district = district;
		this.mnVdc = mnVdc;
		this.wardNo = wardNo;
		this.townCity = townCity;
		this.notes = notes;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.pagerNoCountryCode = pagerNoCountryCode;
		this.pagerNoAreaCode = pagerNoAreaCode;
		this.pagerNo = pagerNo;
		this.emailId = emailId;
		this.id = id;
		this.change = change;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getTownCity() {
		return townCity;
	}

	public void setTownCity(String townCity) {
		this.townCity = townCity;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getPagerNoCountryCode() {
		return pagerNoCountryCode;
	}

	public void setPagerNoCountryCode(String pagerNoCountryCode) {
		this.pagerNoCountryCode = pagerNoCountryCode;
	}

	public String getPagerNoAreaCode() {
		return pagerNoAreaCode;
	}

	public void setPagerNoAreaCode(String pagerNoAreaCode) {
		this.pagerNoAreaCode = pagerNoAreaCode;
	}

	public String getPagerNo() {
		return pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
	
	

}
