package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationInfo {

	@JsonProperty("registration_authority")
	String registrationAuthority;

	@JsonProperty("self_regulatory_body")
	String selfRegulatoryBody;

	@JsonProperty("regd_number")
	String regdNumber;

	@JsonProperty("licensing_authority")
	String licensingAuthority;

	@JsonProperty("license_number")
	String licenseNumber;

	@JsonProperty("notes")
	String notes;

	private long id;

	@JsonProperty("change")
	boolean change;

	public RegistrationInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistrationInfo(String registrationAuthority, String selfRegulatoryBody, String regdNumber,
			String licensingAuthority, String licenseNumber, String notes, long id, boolean change) {
		super();
		this.registrationAuthority = registrationAuthority;
		this.selfRegulatoryBody = selfRegulatoryBody;
		this.regdNumber = regdNumber;
		this.licensingAuthority = licensingAuthority;
		this.licenseNumber = licenseNumber;
		this.notes = notes;
		this.id = id;
		this.change = change;
	}

	public String getRegistrationAuthority() {
		return registrationAuthority;
	}

	public void setRegistrationAuthority(String registrationAuthority) {
		this.registrationAuthority = registrationAuthority;
	}

	public String getSelfRegulatoryBody() {
		return selfRegulatoryBody;
	}

	public void setSelfRegulatoryBody(String selfRegulatoryBody) {
		this.selfRegulatoryBody = selfRegulatoryBody;
	}

	public String getRegdNumber() {
		return regdNumber;
	}

	public void setRegdNumber(String regdNumber) {
		this.regdNumber = regdNumber;
	}

	public String getLicensingAuthority() {
		return licensingAuthority;
	}

	public void setLicensingAuthority(String licensingAuthority) {
		this.licensingAuthority = licensingAuthority;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
