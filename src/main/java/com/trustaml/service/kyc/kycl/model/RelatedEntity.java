package com.trustaml.service.kyc.kycl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RelatedEntity {

	@JsonProperty("entity_type")
	private String entityType;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("kycn_id")
	private long kycnId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("name")
	private String name;

	@JsonProperty("ls_name")
	private String lsName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;

	@JsonProperty("registration_no")
	private String registrationNo;

	@JsonProperty("country")
	private String country;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("pinzip")
	private String pinzip;

	@JsonProperty("ward_number")
	private String wardNumber;

	@JsonProperty("tole_area")
	private String toleArea;

	@JsonProperty("street")
	private String street;

	@JsonProperty("house_no")
	private String houseNo;

	@JsonProperty("unit_number")
	private String unitNumber;

	@JsonProperty("nearest_landmark")
	private String nearestLandmark;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("longitude")
	private String longitude;

	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("telex_no_country_code")
	private String telexNoCountryCode;

	@JsonProperty("telex_no_area_code")
	private String telexNoAreaCode;

	@JsonProperty("telex_no")
	private String telexNo;

	@JsonProperty("notes")
	private String notes;

	private long id;

	@JsonProperty("change")
	private boolean change;

	@JsonProperty("province")
	private String province;

	public RelatedEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RelatedEntity(String entityType, String custId, long kycnId, String salutation, String name, String lsName,
			String calledByName, String primaryIdentificationDocumentType, String registrationNo, String country,
			String zone, String district, String mnVdc, String pinzip, String wardNumber, String toleArea,
			String street, String houseNo, String unitNumber, String nearestLandmark, String latitude, String longitude,
			String phoneNoCountryCode, String phoneNoAreaCode, String phoneNo, String telexNoCountryCode,
			String telexNoAreaCode, String telexNo, long id, String notes, boolean change) {
		super();
		this.entityType = entityType;
		this.custId = custId;
		this.kycnId = kycnId;
		this.salutation = salutation;
		this.name = name;
		this.lsName = lsName;
		this.calledByName = calledByName;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.registrationNo = registrationNo;
		this.country = country;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.pinzip = pinzip;
		this.wardNumber = wardNumber;
		this.toleArea = toleArea;
		this.street = street;
		this.houseNo = houseNo;
		this.unitNumber = unitNumber;
		this.nearestLandmark = nearestLandmark;
		this.latitude = latitude;
		this.longitude = longitude;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.id = id;
		this.notes = notes;
		this.change = change;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public long getKycnId() {
		return kycnId;
	}

	public void setKycnId(long kycnId) {
		this.kycnId = kycnId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLsName() {
		return lsName;
	}

	public void setLsName(String lsName) {
		this.lsName = lsName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getPinzip() {
		return pinzip;
	}

	public void setPinzip(String pinzip) {
		this.pinzip = pinzip;
	}

	public String getWardNumber() {
		return wardNumber;
	}

	public void setWardNumber(String wardNumber) {
		this.wardNumber = wardNumber;
	}

	public String getToleArea() {
		return toleArea;
	}

	public void setToleArea(String toleArea) {
		this.toleArea = toleArea;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getNearestLandmark() {
		return nearestLandmark;
	}

	public void setNearestLandmark(String nearestLandmark) {
		this.nearestLandmark = nearestLandmark;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
