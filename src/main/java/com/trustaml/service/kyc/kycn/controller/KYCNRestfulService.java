package com.trustaml.service.kyc.kycn.controller;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.kyc.kycn.dao.KYCNDao;

@Path("/kycn-registration")
public class KYCNRestfulService {

	@Inject
	KYCNDao kycDao;

	/**
	 * Save and Resfresh kycn
	 * 
	 * @param str
	 * @return kycn id
	 * @throws Exception
	 * @description accepts String JSON and saves data for KYCN. Return kycn id
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveKYCNData(String jsonStr) throws Exception {
		if ("" != jsonStr || !jsonStr.isEmpty()) {
			Long kycnId = kycDao.saveKycnData(jsonStr);
			return ResponseReturn.sucess(String.valueOf(kycnId));
			// return null;
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonStr
	 * @return refresh successful
	 * @throws Exception
	 * @description update kync data using kycn id
	 */

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response refreshKYCNData(String jsonStr) throws Exception {
		if ("" != jsonStr || !jsonStr.isEmpty()) {
			return ResponseReturn.sucess(String.valueOf(kycDao.updateKycnData(jsonStr)));
			// return null;
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}
}
