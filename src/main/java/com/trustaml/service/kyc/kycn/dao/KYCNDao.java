package com.trustaml.service.kyc.kycn.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.service.JsonToObject;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycl.dao.KyclDaoImpl;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfo;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnAmlInformationInfo;
import com.trustaml.service.kyc.kycn.model.KycnDocumentStatusInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnFinancialInfo;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfo;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnObservationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedEntityInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedPersonInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfoUpdate;

@Stateless
public class KYCNDao {

	@Inject
	KYCNDaoImpl kycnDaoImpl;
	
	@Inject
	KyclDaoImpl kyclDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	boolean includeUser = true;
	boolean doNotIncludeUser = false;

	/**
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public Long saveKycnData(String str) throws Exception {
		JsonNode kycnRootNode = JsonToObject.getMainJsonNode(str);
		Long kycnPersonalId = 0L;
		KycnPersonalInfo kycnPersonalInfo = JsonToObject.getKycnPersonalInfoObjectsFromJson(kycnRootNode);
		List<KycnIdentificationInfo> kycnIdentificationInfoList = JsonToObject
				.getKycnIdentificationInfoObjectsFromJson(kycnRootNode);
		List<KycnRelationInfo> kycnRelationInfoList = JsonToObject.getKycnRelationInfoObjectsFromJson(kycnRootNode);
		List<KycnEducationInfo> kycnEducationInfoList = JsonToObject.getKycnEducationInfoObjectsFromJson(kycnRootNode);
		List<KycnAccountsInfo> kycnAccountsInfoList = JsonToObject.getKycnAccountsInfoObjectsFromJson(kycnRootNode);
		List<KycnInvolvementInfo> kycnInvolvementInfoList = JsonToObject
				.getKycnInvolvementInfoObjectsFromJson(kycnRootNode);
		List<KycnAddressInfo> kycnAddressInfoList = JsonToObject.getKycnAddressInfoObjectsFromJson(kycnRootNode);
		KycnObservationInfo kycnObservation = JsonToObject.getKycnObservationInfoObjectsFromJson(kycnRootNode);
		KycnAmlInformationInfo kycnAmlInfo = JsonToObject.getKycnAmlInfoObjectsFromJson(kycnRootNode);
		List<KycnDocumentStatusInfo> documentStatusInfoList = JsonToObject
				.getKycnDocumentStatusInfoObjectsFromJson(kycnRootNode);
		KycnFinancialInfo financialInfo = JsonToObject.getKycnFinancialInfoObjectsFromJson(kycnRootNode);
		List<KycnRelatedPersonInfo> listKycnRelatedPersonInfo = JsonToObject
				.getKycnRelatedPersonalInfoInfoObjectsFromJson(kycnRootNode);
		List<KycnRelatedEntityInfo> listKycnRelatedEntityInfo = JsonToObject
				.getKycnRelatedEntityInfoInfoObjectsFromJson(kycnRootNode);

		User user = JsonToObject.getUserInfoObjectsFromJson(kycnRootNode);
		if (user != null) {
			String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(kycnPersonalInfo,
					user, includeUser);
			kycnPersonalId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo, user, hashValueForKycnPersonInfo);
			kycnPersonalInfo.setId(kycnPersonalId);
			String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
					.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, includeUser);
			kycnDaoImpl.insertIntoKycnPersonalInfoUpdateTable(kycnPersonalInfo, user, hashValueForKycnPersonInfoUpdate);
			for (KycnIdentificationInfo kycnIdentificationInfo : kycnIdentificationInfoList) {
				String hashValueForKycnIdentificationInfo = hashCodeGenerator
						.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, doNotIncludeUser,
								kycnPersonalId);
				String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, includeUser,
								kycnPersonalId);
				kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalId,
						hashValueForKycnIdentificationInfo, user);
				kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalId,
						hashValueForKycnIdentificationInfoUpdate, user);
			}
			updateAndInsertKycnFinancialInfo(financialInfo, kycnPersonalInfo.getId(), user);
			updateAndInsertKycnRelatedPersonInfo(listKycnRelatedPersonInfo, kycnPersonalId, user);
			updateAndInsertKycnRelatedEntityInfo(listKycnRelatedEntityInfo, kycnPersonalId, user);
			updateAndInsertKycnAccountInfo(kycnAccountsInfoList, kycnPersonalId, user);
			updateAndInsertKycnAddressInfo(kycnAddressInfoList, kycnPersonalId, user);
			updateAndInsertKycnEducationInfo(kycnEducationInfoList, kycnPersonalId, user);
			updateAndInsertKycnInvolvementInfo(kycnInvolvementInfoList, kycnPersonalId, user);
			updateAndInsertKycnRealtionInfo(kycnRelationInfoList, kycnPersonalId, user);
			updateAndInsertKycnObservationInfo(kycnObservation, kycnPersonalId, user);
			updateAndInsertKycnAMlInfo(kycnAmlInfo, kycnPersonalId, user);
			updateAndInsertKycnDocumentStatus(documentStatusInfoList, kycnPersonalId, user);

			/// MORE COMMENT NEEDED// DANGEROUS LOGIC
			kycnDaoImpl.updateCommentTableInCaseOfFoward(kycnPersonalId, kycnPersonalInfo.getScreeningId());

			kycnDaoImpl.insertIntoKYCNApprovedTable(kycnPersonalId);
			kycnDaoImpl.updateScreeningNaturalWork(kycnPersonalInfo.getScreeningId(), true);

		}
		return kycnPersonalId;
	}

	/**
	 * @param listKycnRelatedEntityInfo
	 * @param kycnPersonalId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnRelatedEntityInfo(List<KycnRelatedEntityInfo> listKycnRelatedEntityInfo,
			long kycnPersonalId, User user) throws Exception {
		for (KycnRelatedEntityInfo kycnRelatedEntityInfo : listKycnRelatedEntityInfo) {
			String hashValueForKycnRelatedEntityInfo = hashCodeGenerator.generateHashValueForKycnRelatedEntityInfo(
					kycnRelatedEntityInfo, user, doNotIncludeUser, kycnPersonalId);
			String hashValueForKycnRelatedEntityInfoUpdate = hashCodeGenerator
					.generateHashValueForKycnRelatedEntityInfo(kycnRelatedEntityInfo, user, includeUser,
							kycnPersonalId);
			if (kycnRelatedEntityInfo.getId() != 0) {
				if (kycnRelatedEntityInfo.isChange()) {
					kycnDaoImpl.updateKycnRelatedEntityInfo(kycnRelatedEntityInfo, kycnPersonalId,
							hashValueForKycnRelatedEntityInfo);
					kycnDaoImpl.insertKycnRelatedEntityInfoUpdateTable(kycnRelatedEntityInfo, kycnPersonalId, user,
							hashValueForKycnRelatedEntityInfoUpdate);
				}
			} else {
				/** Save separate new KYC of added new related entity **/
				if(kycnRelatedEntityInfo.getKycnId()<=0 ){
					Long newKyclId = insertNewRelatedEntityAddedInKYC(kycnRelatedEntityInfo,user);
					kycnRelatedEntityInfo.setKycnId(newKyclId);
				}
				/***/
				
				kycnDaoImpl.insertKycnRelatedEntityInfo(kycnRelatedEntityInfo, kycnPersonalId,
						hashValueForKycnRelatedEntityInfo);
				kycnDaoImpl.insertKycnRelatedEntityInfoUpdateTable(kycnRelatedEntityInfo, kycnPersonalId, user,
						hashValueForKycnRelatedEntityInfoUpdate);
			}
		}

	}
	
	
private Long insertNewRelatedEntityAddedInKYC(KycnRelatedEntityInfo kycnRelatedEntityInfo, User user) throws Exception {
		
		Legal kyclInfo = new Legal();
		kyclInfo.setNameOfTheInstitution(kycnRelatedEntityInfo.getName());
		kyclInfo.setCustId(kycnRelatedEntityInfo.getCustId());
		kyclInfo.setLsName(kycnRelatedEntityInfo.getLsName());
		kyclInfo.setNotes(kycnRelatedEntityInfo.getNotes());
		kyclInfo.setPrimary(false);
		
		Address kyclAddress = new Address();
		kyclAddress.setAddressType("Permanent");
		kyclAddress.setDistrict(kycnRelatedEntityInfo.getDistrict());
		kyclAddress.setCountry(kycnRelatedEntityInfo.getRelatedEntityCountry());
		kyclAddress.setZone(kycnRelatedEntityInfo.getZone());
		kyclAddress.setProvince(kycnRelatedEntityInfo.getProvince());
		kyclAddress.setMnVdc(kycnRelatedEntityInfo.getMnVdc());
		kyclAddress.setWardNumber(kycnRelatedEntityInfo.getWardNumber());
		kyclAddress.setPinzip(kycnRelatedEntityInfo.getPinzip());
		kyclAddress.setToleArea(kycnRelatedEntityInfo.getToleArea());
		kyclAddress.setStreet(kycnRelatedEntityInfo.getStreet());
		kyclAddress.setHouseNo(kycnRelatedEntityInfo.getHouseNo());
		kyclAddress.setUnitNumber(kycnRelatedEntityInfo.getUnitNumber());
		kyclAddress.setNearestLandmark(kycnRelatedEntityInfo.getNearestLandmark());
		kyclAddress.setLatitude(kycnRelatedEntityInfo.getLatitude());
		kyclAddress.setLongitude(kycnRelatedEntityInfo.getLongitude());
		kyclAddress.setPhoneNoAreaCode(kycnRelatedEntityInfo.getPhoneNoAreaCode());
		kyclAddress.setPhoneNoCountryCode(kycnRelatedEntityInfo.getPhoneNoCountryCode());
		kyclAddress.setPhoneNo(kycnRelatedEntityInfo.getPhoneNo());
		kyclAddress.setTelexNoAreaCode(kycnRelatedEntityInfo.getTelexNoAreaCode());
		kyclAddress.setTelexNoCountryCode(kycnRelatedEntityInfo.getTelexNoCountryCode());
		kyclAddress.setTelexNo(kycnRelatedEntityInfo.getTelexNo());
		kyclAddress.setEmailId(kycnRelatedEntityInfo.getEmailId());
		RegistrationInfo registrationInfo = new RegistrationInfo();
		registrationInfo.setRegdNumber(kycnRelatedEntityInfo.getRegistrationNo());
		
		
		String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kyclInfo, doNotIncludeUser,
				user);
		String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(kyclInfo, includeUser,
				user);
		
		Long kyclId = kyclDaoImpl.saveLegalInfo(kyclInfo, user, kyclHashCode);
		kyclDaoImpl.saveLegalInfoInUpdateTable(kyclInfo, user, kyclUpdateHashCode);
		
		String addressHashCode = hashCodeGenerator.generateHashValueForAddress(kyclAddress, kyclId, doNotIncludeUser,
				user);
		String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(kyclAddress, kyclId, includeUser,
				user);
		kyclDaoImpl.saveAddress(kyclAddress, kyclId, addressHashCode,user);
		kyclDaoImpl.saveAddressInUpdateTable(kyclAddress, kyclId, user, addressUpdateHashCode);
		
		String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
				registrationInfo, kyclId, doNotIncludeUser, user);
		String registrationInfoUpdateHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
				registrationInfo, kyclId, includeUser, user);
		kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
		kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId, user, registrationInfoUpdateHashCode);
		
		
		
		return kyclId;
	}

	/**
	 * @param listKycnRelatedPersonInfo
	 * @param kycnPersonalId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnRelatedPersonInfo(List<KycnRelatedPersonInfo> listKycnRelatedPersonInfo,
			long kycnPersonalId, User user) throws Exception {
		for (KycnRelatedPersonInfo kycnRelatedPersonInfo : listKycnRelatedPersonInfo) {
			String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnRelatedPersonInfo(
					kycnRelatedPersonInfo, user, doNotIncludeUser, kycnPersonalId);
			String hashValueForKycnRelatedPersonInfoUpdate = hashCodeGenerator
					.generateHashValueForKycnRelatedPersonInfo(kycnRelatedPersonInfo, user, includeUser,
							kycnPersonalId);
			if (kycnRelatedPersonInfo.getId() != 0) {
				if (kycnRelatedPersonInfo.isChange()) {
					
					/** Save separate new KYCN of added new related person **/
					
					if(kycnRelatedPersonInfo.getKycnId()<=0||kycnRelatedPersonInfo.getKycnId()==0 ){
						long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(kycnRelatedPersonInfo,user); 
						kycnRelatedPersonInfo.setKycnId(newRelatedPersonKYCNId);
					}
					/***/
					kycnDaoImpl.updateKycnRelatedPersonInfo(kycnRelatedPersonInfo, kycnPersonalId,
							hashValueForKycnPersonInfo);
					kycnDaoImpl.insertKycnRelatedPersonInfoUpdateTable(kycnRelatedPersonInfo, kycnPersonalId, user,
							hashValueForKycnRelatedPersonInfoUpdate);
				}
			} else {
				
				/** Save separate new KYCN of added new related person **/
				
				if(kycnRelatedPersonInfo.getKycnId()<=0 ){
					long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(kycnRelatedPersonInfo,user); 
					kycnRelatedPersonInfo.setKycnId(newRelatedPersonKYCNId);
				}
				/***/
				
				kycnDaoImpl.insertKycnRelatedPersonInfo(kycnRelatedPersonInfo, kycnPersonalId,
						hashValueForKycnPersonInfo);
				kycnDaoImpl.insertKycnRelatedPersonInfoUpdateTable(kycnRelatedPersonInfo, kycnPersonalId, user,
						hashValueForKycnRelatedPersonInfoUpdate);
			}
		}

	}
	
	
	private long insertNewKycOfRelatedPersonAddedInKYC(KycnRelatedPersonInfo kycnRelatedPersonInfo,User user) throws Exception {
		KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
		kycnPersonalInfo.setFirstName(kycnRelatedPersonInfo.getFirstName());
		kycnPersonalInfo.setMiddleName(kycnRelatedPersonInfo.getMiddleName());
		kycnPersonalInfo.setLastName(kycnRelatedPersonInfo.getLastName());
		kycnPersonalInfo.setCustomerId(kycnRelatedPersonInfo.getCustId());
		kycnPersonalInfo.setSalutation(kycnRelatedPersonInfo.getSalutation());
		kycnPersonalInfo.setLsfName(kycnRelatedPersonInfo.getLsfName());
		kycnPersonalInfo.setLsmName(kycnRelatedPersonInfo.getLsmName());
		kycnPersonalInfo.setLslName(kycnRelatedPersonInfo.getLslName());
		kycnPersonalInfo.setSecondName(kycnRelatedPersonInfo.getSecondName());
		kycnPersonalInfo.setCalledByName(kycnRelatedPersonInfo.getCalledByName());
		kycnPersonalInfo.setNotes(kycnRelatedPersonInfo.getNotes());
		kycnPersonalInfo.setPrimary(false);
		
		KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
		kycnIdentificationInfo.setPrimaryIdentificationDocumentNo(kycnRelatedPersonInfo.getPrimaryIdentificationDocumentNo());
		kycnIdentificationInfo.setPrimaryIdentificationDocumentType(kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType());
		kycnIdentificationInfo.setCountryOfIssue(kycnRelatedPersonInfo.getCountry());
		kycnIdentificationInfo.setIssueDate(kycnRelatedPersonInfo.getIssueDate());
		kycnIdentificationInfo.setExpiryDate(kycnRelatedPersonInfo.getExpiryDate());
		kycnIdentificationInfo.setIssuingAuthority(kycnRelatedPersonInfo.getIssuingAuthority());
		kycnIdentificationInfo.setPlaceOfIssue(kycnRelatedPersonInfo.getPlaceOfIssue());
		
		String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(kycnPersonalInfo,
				user, includeUser);
		String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
				.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, includeUser);
		
		Long kycnId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo, user,hashValueForKycnPersonInfo );
		kycnPersonalInfo.setId(kycnId);
		kycnDaoImpl.insertIntoKycnPersonalInfoUpdateTable(kycnPersonalInfo, user, hashValueForKycnPersonInfoUpdate);
		String hashValueForKycnIdentificationInfo = hashCodeGenerator
				.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, doNotIncludeUser,
						kycnId);
		String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
				.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, includeUser,
						kycnId);
		kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnId, hashValueForKycnIdentificationInfo, user);
		kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnId, hashValueForKycnIdentificationInfoUpdate, user);
		
		return kycnId;
		
	}


	/**
	 * @param financialInfo
	 * @param kycnPersonalId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnFinancialInfo(KycnFinancialInfo financialInfo, long kycnPersonalId, User user)
			throws Exception {
		String hashValueForKycnFinancialInfo = hashCodeGenerator.generateHashValueForKycnFinancialInfo(financialInfo,
				user, doNotIncludeUser, kycnPersonalId);
		String hashValueForKycnFinancialInfoUpdate = hashCodeGenerator
				.generateHashValueForKycnFinancialInfo(financialInfo, user, includeUser, kycnPersonalId);
		if (financialInfo.getId() != 0) {
			if (financialInfo.isChange()) {
				kycnDaoImpl.updateKycnFinancialInfo(financialInfo, kycnPersonalId, hashValueForKycnFinancialInfo);
				kycnDaoImpl.insertKycnFinancialInfoUpdateTable(financialInfo, kycnPersonalId, user,
						hashValueForKycnFinancialInfoUpdate);
			}

			for (String individual : financialInfo.getIndividualCardSubscribed()) {
				if (!kycnDaoImpl.checkIfSubscribedIndividualExists(individual, kycnPersonalId)) {
					String hashValueForKycnSubscribedIndividualInfo = hashCodeGenerator
							.generateHashValueForKycnSubscribedIndividualInfo(individual, user, doNotIncludeUser,
									kycnPersonalId);
					String hashValueForKycnSubscribedIndividualInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnSubscribedIndividualInfo(individual, user, includeUser,
									kycnPersonalId);
					kycnDaoImpl.insertCardSubscribedIndividual(individual, kycnPersonalId,
							hashValueForKycnSubscribedIndividualInfo);
					kycnDaoImpl.insertCardSubscribedIndividualUpdateTable(individual, kycnPersonalId, user,
							hashValueForKycnSubscribedIndividualInfoUpdate);
				}

			}

			for (String service : financialInfo.getIndividualServiceSubscribed()) {
				if (!kycnDaoImpl.checkIfSubscribedServiceExists(service, kycnPersonalId)) {

					String hashValueForKycnSubscribedServiceInfo = hashCodeGenerator
							.generateHashValueForKycnSubscribedServiceInfo(service, user, doNotIncludeUser,
									kycnPersonalId);
					String hashValueForKycnSubscribedServiceInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnSubscribedServiceInfo(service, user, includeUser, kycnPersonalId);
					kycnDaoImpl.insertServiceSubscribedIndividual(service, kycnPersonalId,
							hashValueForKycnSubscribedServiceInfo);
					kycnDaoImpl.insertServiceSubscribedIndividualUpdateTable(service, kycnPersonalId, user,
							hashValueForKycnSubscribedServiceInfoUpdate);
				}

			}
		} else {
			kycnDaoImpl.insertKycnFinancialInfo(financialInfo, kycnPersonalId, hashValueForKycnFinancialInfo);
			kycnDaoImpl.insertKycnFinancialInfoUpdateTable(financialInfo, kycnPersonalId, user,
					hashValueForKycnFinancialInfo);
			for (String individual : financialInfo.getIndividualCardSubscribed()) {
				if (!kycnDaoImpl.checkIfSubscribedIndividualExists(individual, kycnPersonalId)) {
					String hashValueForKycnSubscribedIndividualInfo = hashCodeGenerator
							.generateHashValueForKycnSubscribedIndividualInfo(individual, user, doNotIncludeUser,
									kycnPersonalId);
					String hashValueForKycnSubscribedIndividualInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnSubscribedIndividualInfo(individual, user, includeUser,
									kycnPersonalId);
					kycnDaoImpl.insertCardSubscribedIndividual(individual, kycnPersonalId,
							hashValueForKycnSubscribedIndividualInfo);
					kycnDaoImpl.insertCardSubscribedIndividualUpdateTable(individual, kycnPersonalId, user,
							hashValueForKycnSubscribedIndividualInfoUpdate);
				}

			}

			for (String service : financialInfo.getIndividualServiceSubscribed()) {
				if (!kycnDaoImpl.checkIfSubscribedServiceExists(service, kycnPersonalId)) {

					String hashValueForKycnSubscribedServiceInfo = hashCodeGenerator
							.generateHashValueForKycnSubscribedServiceInfo(service, user, doNotIncludeUser,
									kycnPersonalId);
					String hashValueForKycnSubscribedServiceInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnSubscribedServiceInfo(service, user, includeUser, kycnPersonalId);
					kycnDaoImpl.insertServiceSubscribedIndividual(service, kycnPersonalId,
							hashValueForKycnSubscribedServiceInfo);
					kycnDaoImpl.insertServiceSubscribedIndividualUpdateTable(service, kycnPersonalId, user,
							hashValueForKycnSubscribedServiceInfoUpdate);
				}

			}

		}

	}

	/**
	 * @param documentStatusInfoList
	 * @param kycnPersonalId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnDocumentStatus(List<KycnDocumentStatusInfo> documentStatusInfoList,
			Long kycnPersonalId, User user) throws Exception {
		if (documentStatusInfoList != null && documentStatusInfoList.size() > 0) {
			for (KycnDocumentStatusInfo kycnDocumentStatusInfo : documentStatusInfoList) {
				String hashValueForKycnDocumentStatusInfo = hashCodeGenerator
						.generateHashValueForKycnDocumentStatusInfo(kycnDocumentStatusInfo, user, doNotIncludeUser,
								kycnPersonalId);
				String hashValueForKycnDocumentStatusInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnDocumentStatusInfo(kycnDocumentStatusInfo, user, includeUser,
								kycnPersonalId);
				if (kycnDocumentStatusInfo.getId() != 0) {
					if (kycnDocumentStatusInfo.isChange()) {
						kycnDaoImpl.updateDocumentStatusInfo(kycnDocumentStatusInfo, kycnPersonalId,
								hashValueForKycnDocumentStatusInfo);
						kycnDaoImpl.insertKycnDocumnetUpdateKycnStatusInfo(kycnPersonalId,
								new KycnDocumentStatusInfo(kycnDocumentStatusInfo), user,
								hashValueForKycnDocumentStatusInfoUpdate);
					}
				} else {
					kycnDaoImpl.insertKycnDocumentStatusInfo(kycnDocumentStatusInfo, kycnPersonalId,
							hashValueForKycnDocumentStatusInfo);
					kycnDaoImpl.insertKycnDocumnetUpdateKycnStatusInfo(kycnPersonalId,
							new KycnDocumentStatusInfo(kycnDocumentStatusInfo), user,
							hashValueForKycnDocumentStatusInfoUpdate);
				}
			}
		}
	}

	/**
	 * @param kycnAmlInformationInfo
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnAMlInfo(KycnAmlInformationInfo kycnAmlInformationInfo, Long kycnPersonalInfoId,
			User user) throws Exception {
		if (kycnAmlInformationInfo != null) {
			String hashValueForKycnAmlInformationInfo = hashCodeGenerator.generateHashValueForKycnAmlInformationInfo(
					kycnAmlInformationInfo, user, doNotIncludeUser, kycnPersonalInfoId);
			String hashValueForKycnAmlInformationInfoUpdate = hashCodeGenerator
					.generateHashValueForKycnAmlInformationInfo(kycnAmlInformationInfo, user, includeUser,
							kycnPersonalInfoId);
			if (kycnAmlInformationInfo.getId() != 0L) {
				if (kycnAmlInformationInfo.isChange()) {
					kycnDaoImpl.updateKycnAmlInfo(kycnAmlInformationInfo, kycnPersonalInfoId,
							hashValueForKycnAmlInformationInfo);
					kycnDaoImpl.updateKycnAmlInformationInfo(kycnPersonalInfoId,
							new KycnAmlInformationInfo(kycnAmlInformationInfo), user,
							hashValueForKycnAmlInformationInfoUpdate);
				}
			} else {
				kycnDaoImpl.insertKycnAmlInfo(kycnAmlInformationInfo, kycnPersonalInfoId,
						hashValueForKycnAmlInformationInfo);
				kycnDaoImpl.updateKycnAmlInformationInfo(kycnPersonalInfoId,
						new KycnAmlInformationInfo(kycnAmlInformationInfo), user,
						hashValueForKycnAmlInformationInfoUpdate);
			}
		}
	}

	/**
	 * @param kycnObservationInfo
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */

	private void updateAndInsertKycnObservationInfo(KycnObservationInfo kycnObservationInfo, Long kycnPersonalInfoId,
			User user) throws Exception {
		if (kycnObservationInfo != null) {

			String hashValueForKycnObservationInfo = hashCodeGenerator.generateHashValueForKycnObservationInfo(
					kycnObservationInfo, user, doNotIncludeUser, kycnPersonalInfoId);
			String hashValueForKycnObservationInfoUpdate = hashCodeGenerator.generateHashValueForKycnObservationInfo(
					kycnObservationInfo, user, includeUser, kycnPersonalInfoId);
			if (kycnObservationInfo.getId() != 0L) {
				if (kycnObservationInfo.isChange()) {
					kycnDaoImpl.updateKycnObservationInfo(kycnObservationInfo, kycnPersonalInfoId,
							hashValueForKycnObservationInfo);
					kycnDaoImpl.insertKycnObservationInfoUpdateTable(new KycnObservationInfo(kycnObservationInfo),
							kycnPersonalInfoId, user, hashValueForKycnObservationInfoUpdate);
				}
			} else {
				kycnDaoImpl.insertKycnObservationInfo(kycnObservationInfo, kycnPersonalInfoId,
						hashValueForKycnObservationInfo);
				kycnDaoImpl.insertKycnObservationInfoUpdateTable(new KycnObservationInfo(kycnObservationInfo),
						kycnPersonalInfoId, user, hashValueForKycnObservationInfoUpdate);
			}
		}
	}

	/**
	 * @param kycnRelationInfoList
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnRealtionInfo(List<KycnRelationInfo> kycnRelationInfoList, Long kycnPersonalInfoId,
			User user) throws Exception {
		if (kycnRelationInfoList != null && kycnRelationInfoList.size() > 0) {
			for (KycnRelationInfo kycnRelationInfo : kycnRelationInfoList) {
				String hashValueForKycnRelationInfo = hashCodeGenerator.generateHashValueForKycnRelationInfo(
						kycnRelationInfo, user, doNotIncludeUser, kycnPersonalInfoId);
				String hashValueForKycnRelationInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnRelationInfo(kycnRelationInfo, user, includeUser, kycnPersonalInfoId);
				if (kycnRelationInfo.getId() != 0L) {
					if (kycnRelationInfo.isChange()) {
						kycnDaoImpl.updateKycnRelationInfo(kycnRelationInfo, kycnPersonalInfoId,
								hashValueForKycnRelationInfo);
						kycnDaoImpl.insertKycnRelationInfoUpdateTable(new KycnRelationInfoUpdate(kycnRelationInfo),
								kycnPersonalInfoId, user, hashValueForKycnRelationInfoUpdate);
					}
				} else {
					kycnDaoImpl.insertKycnRelationInfo(kycnRelationInfo, kycnPersonalInfoId,
							hashValueForKycnRelationInfo);
					kycnDaoImpl.insertKycnRelationInfoUpdateTable(new KycnRelationInfoUpdate(kycnRelationInfo),
							kycnPersonalInfoId, user, hashValueForKycnRelationInfoUpdate);
				}
			}
		}
	}

	/**
	 * @param kycnInvolvementInfoList
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnInvolvementInfo(List<KycnInvolvementInfo> kycnInvolvementInfoList,
			Long kycnPersonalInfoId, User user) throws Exception {
		if (kycnInvolvementInfoList != null && kycnInvolvementInfoList.size() > 0) {
			for (KycnInvolvementInfo kycnInvolvementInfo : kycnInvolvementInfoList) {

				String hashValueForKycnInvolvementInfo = hashCodeGenerator.generateHashValueForKycnInvolvementInfo(
						kycnInvolvementInfo, user, doNotIncludeUser, kycnPersonalInfoId);
				String hashValueForKycnInvolvementInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnInvolvementInfo(kycnInvolvementInfo, user, includeUser,
								kycnPersonalInfoId);
				if (kycnInvolvementInfo.getId() != 0L) {
					if (kycnInvolvementInfo.isChange()) {
						kycnDaoImpl.updateKycnInvolvementInfo(kycnInvolvementInfo, kycnPersonalInfoId,
								hashValueForKycnInvolvementInfo);
						kycnDaoImpl.insertKycnInvolvementInfoUpdateTable(
								new KycnInvolvementInfoUpdate(kycnInvolvementInfo), kycnPersonalInfoId, user,
								hashValueForKycnInvolvementInfoUpdate);
					}
				} else {
					kycnDaoImpl.insertKycnInvolvementInfo(kycnInvolvementInfo, kycnPersonalInfoId,
							hashValueForKycnInvolvementInfo);
					kycnDaoImpl.insertKycnInvolvementInfoUpdateTable(new KycnInvolvementInfoUpdate(kycnInvolvementInfo),
							kycnPersonalInfoId, user, hashValueForKycnInvolvementInfoUpdate);
				}
			}
		}
	}

	/**
	 * @param kycnEducationInfoList
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnEducationInfo(List<KycnEducationInfo> kycnEducationInfoList,
			Long kycnPersonalInfoId, User user) throws Exception {
		if (kycnEducationInfoList != null && kycnEducationInfoList.size() > 0) {
			for (KycnEducationInfo kycnEducationInfo : kycnEducationInfoList) {
				String hashValueForKycnEducationInfo = hashCodeGenerator.generateHashValueForKycnEducationInfo(
						kycnEducationInfo, user, doNotIncludeUser, kycnPersonalInfoId);
				String hashValueForKycnEducationInfoUpdate = hashCodeGenerator.generateHashValueForKycnEducationInfo(
						kycnEducationInfo, user, includeUser, kycnPersonalInfoId);
				if (kycnEducationInfo.getId() != 0L) {
					if (kycnEducationInfo.isChange()) {
						kycnDaoImpl.updateKycnEducationInfo(kycnEducationInfo, kycnPersonalInfoId,
								hashValueForKycnEducationInfo);
						kycnDaoImpl.insertKycnEducationInfoUpdateTable(new KycnEducationInfoUpdate(kycnEducationInfo),
								kycnPersonalInfoId, user, hashValueForKycnEducationInfoUpdate);
					}
				} else {
					kycnDaoImpl.insertKycnEducationInfo(kycnEducationInfo, kycnPersonalInfoId,
							hashValueForKycnEducationInfo);
					kycnDaoImpl.insertKycnEducationInfoUpdateTable(new KycnEducationInfoUpdate(kycnEducationInfo),
							kycnPersonalInfoId, user, hashValueForKycnEducationInfoUpdate);
				}
			}
		}
	}

	/**
	 * @param kycnAddressInfoList
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnAddressInfo(List<KycnAddressInfo> kycnAddressInfoList, Long kycnPersonalInfoId,
			User user) throws Exception {
		if (kycnAddressInfoList != null) {
			for (KycnAddressInfo kycnAddressInfo : kycnAddressInfoList) {
				String hashValueForKycnAddressInfo = hashCodeGenerator.generateHashValueForKycnAddressInfo(
						kycnAddressInfo, user, doNotIncludeUser, kycnPersonalInfoId);
				String hashValueForKycnAddressInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnAddressInfo(kycnAddressInfo, user, includeUser, kycnPersonalInfoId);
				if (kycnAddressInfo.getId() != 0L) {
					if (kycnAddressInfo.isChange()) {
						kycnDaoImpl.updateKycnAddressInfo(kycnAddressInfo, kycnPersonalInfoId,
								hashValueForKycnAddressInfo,user);
						kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
								kycnPersonalInfoId, user, hashValueForKycnAddressInfoUpdate);
					}
				} else {
					kycnDaoImpl.insertKycnAddressInfo(kycnAddressInfo, kycnPersonalInfoId, hashValueForKycnAddressInfo,user);					kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
							kycnPersonalInfoId, user, hashValueForKycnAddressInfoUpdate);
				}
			}
		}
	}

	/**
	 * @param kycnAccountsInfoList
	 * @param kycnPersonalInfoId
	 * @param user
	 * @throws Exception
	 */
	private void updateAndInsertKycnAccountInfo(List<KycnAccountsInfo> kycnAccountsInfoList, Long kycnPersonalInfoId,
			User user) throws Exception {
		if (kycnAccountsInfoList != null) {
			for (KycnAccountsInfo kycnAccountsInfo : kycnAccountsInfoList) {
				String hashValueForKycnAccountsInfo = hashCodeGenerator.generateHashValueForKycnAccountsInfo(
						kycnAccountsInfo, user, doNotIncludeUser, kycnPersonalInfoId);
				String hashValueForKycnAccountsInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnAccountsInfo(kycnAccountsInfo, user, includeUser, kycnPersonalInfoId);
				if (kycnAccountsInfo.getId() != 0L) {
					if (kycnAccountsInfo.isChange()) {
						kycnDaoImpl.updateKycnAccountInfo(kycnAccountsInfo,user, kycnPersonalInfoId,
								hashValueForKycnAccountsInfo);
						kycnDaoImpl.insertKycnAccountsInfoUpdateTable(new KycnAccountsInfoUpdate(kycnAccountsInfo),
								kycnPersonalInfoId, user, hashValueForKycnAccountsInfoUpdate);
					}
					/*
					 * for (String accountCardSubscribed :
					 * kycnAccountsInfo.getAccountCardSubscribed()) {
					 * kycnDaoImpl.updateCardSubscribedInfo(kycnAccountsInfo.
					 * getId(), accountCardSubscribed);
					 * kycnDaoImpl.insertCardSubscribedInfoUpdatetable(
					 * kycnAccountsInfo.getId(), accountCardSubscribed, user); }
					 * for (String accountServiceSubscribed :
					 * kycnAccountsInfo.getAccountServiceSubscribed()) {
					 * kycnDaoImpl.updateServiceSubscribedInfo(kycnAccountsInfo.
					 * getId(), accountServiceSubscribed);
					 * kycnDaoImpl.insertServiceSubscribedInfoUpdateTable(
					 * kycnAccountsInfo.getId(), accountServiceSubscribed,
					 * user); }
					 */
					
					if(kycnAccountsInfo.getAccountCardSubscribed()!=null){
						
						kycnDaoImpl.deleteCardsSubscribed(kycnAccountsInfo.getId());
						for (String accountCardSubscribed :
							 kycnAccountsInfo.getAccountCardSubscribed()) { 
							 kycnDaoImpl.insertCardSubscribedInfoUpdatetable(
							 kycnAccountsInfo.getId(), accountCardSubscribed, user);
							 kycnDaoImpl.insertCardSubscribedInfoTable(kycnAccountsInfo.getId(), accountCardSubscribed, user);
							 
						}
						
					}
					
					if(kycnAccountsInfo.getAccountServiceSubscribed()!=null){
						
						kycnDaoImpl.deleteServicesSubscribed(kycnAccountsInfo.getId());	
						for (String accountServiceSubscribed :
							 kycnAccountsInfo.getAccountServiceSubscribed()) {
							kycnDaoImpl.insertServiceSubscribedInfoUpdateTable(
									 kycnAccountsInfo.getId(), accountServiceSubscribed,
									 user);
									 kycnDaoImpl.insertServiceSubscribedInfoTable(
									 kycnAccountsInfo.getId(), accountServiceSubscribed);
						}
						
					}
					
					if(kycnAccountsInfo.getAccountsRelatedPerson()!=null && !(kycnAccountsInfo.getAccountsRelatedPerson().isEmpty())){
						for(KycnRelatedPersonInfo kycnRelatedPersonInfo: kycnAccountsInfo.getAccountsRelatedPerson()){
							kycnRelatedPersonInfo.setAccountNo(kycnAccountsInfo.getAccountNo());
							if(kycnRelatedPersonInfo.getId()!=0L){
								if(kycnRelatedPersonInfo.isChange()||kycnAccountsInfo.isChange()||kycnRelatedPersonInfo.getKycnId()==0){
									
									/** Save separate new KYCN of added new related person **/
									if(kycnRelatedPersonInfo.getKycnId()<=0 ){
										long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(kycnRelatedPersonInfo,user); 
										kycnRelatedPersonInfo.setKycnId(newRelatedPersonKYCNId);
									}
									/***/
									
									kycnDaoImpl.updateKycnRelatedPersonInfo(kycnRelatedPersonInfo, kycnPersonalInfoId,
											"");
									kycnDaoImpl.insertKycnRelatedPersonInfoUpdateTable(kycnRelatedPersonInfo, kycnPersonalInfoId, user,
											"");
								}
							}else{
								/** Save separate new KYCN of added new related person **/
								if(kycnRelatedPersonInfo.getKycnId()<=0 ){
									long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(kycnRelatedPersonInfo,user); 
									kycnRelatedPersonInfo.setKycnId(newRelatedPersonKYCNId);
								}
								/***/
								kycnDaoImpl.insertKycnRelatedPersonInfo(kycnRelatedPersonInfo, kycnPersonalInfoId,
										"hash");
								kycnDaoImpl.insertKycnRelatedPersonInfoUpdateTable(kycnRelatedPersonInfo, kycnPersonalInfoId, user,
										"hash");
							}
						}
					}

				} else {
					Long kycnAccountsId = kycnDaoImpl.insertKycnAccountsInfo(kycnAccountsInfo, kycnPersonalInfoId,
							hashValueForKycnAccountsInfo);
					kycnDaoImpl.insertKycnAccountsInfoUpdateTable(new KycnAccountsInfoUpdate(kycnAccountsInfo),
							kycnPersonalInfoId, user, hashValueForKycnAccountsInfoUpdate);
					
					if(kycnAccountsInfo.getAccountCardSubscribed()!=null){
						for (String accountCardSubscribed :
							 kycnAccountsInfo.getAccountCardSubscribed()) {
							
							 kycnDaoImpl.insertCardSubscribedInfoUpdatetable(
									 kycnAccountsId, accountCardSubscribed, user);
							 
							 kycnDaoImpl.insertCardSubscribedInfoTable(
									 kycnAccountsId, accountCardSubscribed, user);
							 
							 
							}	
					}
					
					if(kycnAccountsInfo.getAccountServiceSubscribed()!=null){
						
						for (String accountServiceSubscribed :
							 kycnAccountsInfo.getAccountServiceSubscribed()) {
							
							 kycnDaoImpl.insertServiceSubscribedInfoUpdateTable(
							 kycnAccountsId, accountServiceSubscribed,
							 user);
							 kycnDaoImpl.insertServiceSubscribedInfoTable(
							 kycnAccountsId, accountServiceSubscribed);
						}	
					}
					
					
					/*
					 * for (String accountCardSubscribed :
					 * kycnAccountsInfo.getAccountCardSubscribed()) {
					 * kycnDaoImpl.insertCardSubscribedInfo(accountsInfoId,
					 * accountCardSubscribed);
					 * kycnDaoImpl.insertCardSubscribedInfoUpdatetable(
					 * accountsInfoId, accountCardSubscribed, user); } for
					 * (String accountServiceSubscribed :
					 * kycnAccountsInfo.getAccountServiceSubscribed()) {
					 * kycnDaoImpl.insertServiceSubscribedInfo(accountsInfoId,
					 * accountServiceSubscribed);
					 * kycnDaoImpl.insertServiceSubscribedInfoUpdateTable(
					 * accountsInfoId, accountServiceSubscribed, user); }
					 */
					
					/** Insert related from accounts of KYC **/
					if(kycnAccountsInfo.getAccountsRelatedPerson()!=null){
						for(KycnRelatedPersonInfo kycnRelatedPersonInfo: kycnAccountsInfo.getAccountsRelatedPerson()){
							
							kycnRelatedPersonInfo.setAccountNo(kycnAccountsInfo.getAccountNo());
							/** Save separate new KYCN of added new related person **/
							if(kycnRelatedPersonInfo.getKycnId()<=0 ){
								long newRelatedPersonKYCNId = insertNewKycOfRelatedPersonAddedInKYC(kycnRelatedPersonInfo,user); 
								kycnRelatedPersonInfo.setKycnId(newRelatedPersonKYCNId);
							}
							/***/
							kycnDaoImpl.insertKycnRelatedPersonInfo(kycnRelatedPersonInfo, kycnPersonalInfoId,
									"hash");
							kycnDaoImpl.insertKycnRelatedPersonInfoUpdateTable(kycnRelatedPersonInfo, kycnPersonalInfoId, user,
									"hash");
						}
					}
				}
			}
		}
	}
	
	

	/**
	 * @param jsonStr
	 * @throws Exception
	 */
	public long updateKycnData(String jsonStr) throws Exception {
		JsonNode kycnRootNode = JsonToObject.getMainJsonNode(jsonStr);

		KycnPersonalInfo kycnPersonalInfo = JsonToObject.getKycnPersonalInfoObjectsFromJson(kycnRootNode);
		List<KycnIdentificationInfo> kycnIdentificationInfoList = JsonToObject
				.getKycnIdentificationInfoObjectsFromJson(kycnRootNode);
		List<KycnRelationInfo> kycnRelationInfoList = JsonToObject.getKycnRelationInfoObjectsFromJson(kycnRootNode);
		List<KycnEducationInfo> kycnEducationInfoList = JsonToObject.getKycnEducationInfoObjectsFromJson(kycnRootNode);
		List<KycnAccountsInfo> kycnAccountsInfoList = JsonToObject.getKycnAccountsInfoObjectsFromJson(kycnRootNode);
		List<KycnInvolvementInfo> kycnInvolvementInfoList = JsonToObject
				.getKycnInvolvementInfoObjectsFromJson(kycnRootNode);
		List<KycnAddressInfo> kycnAddressInfoList = JsonToObject.getKycnAddressInfoObjectsFromJson(kycnRootNode);
		KycnObservationInfo kycnObservation = JsonToObject.getKycnObservationInfoObjectsFromJson(kycnRootNode);
		KycnAmlInformationInfo kycnAmlInfo = JsonToObject.getKycnAmlInfoObjectsFromJson(kycnRootNode);
		List<KycnDocumentStatusInfo> documentStatusInfoList = JsonToObject
				.getKycnDocumentStatusInfoObjectsFromJson(kycnRootNode);
		KycnFinancialInfo financialInfo = JsonToObject.getKycnFinancialInfoObjectsFromJson(kycnRootNode);
		List<KycnRelatedPersonInfo> listKycnRelatedPersonInfo = JsonToObject
				.getKycnRelatedPersonalInfoInfoObjectsFromJson(kycnRootNode);
		List<KycnRelatedEntityInfo> listKycnRelatedEntityInfo = JsonToObject
				.getKycnRelatedEntityInfoInfoObjectsFromJson(kycnRootNode);

		User user = JsonToObject.getUserInfoObjectsFromJson(kycnRootNode);
		long kycnPersonalInfoId = kycnPersonalInfo.getId();
		if (user != null) {

			if (kycnPersonalInfo.isChange()) {
				String hashValueForKycnPersonInfo = hashCodeGenerator
						.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, includeUser);
				String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
						.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, includeUser);
				kycnDaoImpl.updateKycnPersonalInfo(kycnPersonalInfo, user, hashValueForKycnPersonInfo);
				kycnDaoImpl.insertIntoKycnPersonalInfoUpdateTable(kycnPersonalInfo, user,
						hashValueForKycnPersonInfoUpdate);
			}
			if (kycnIdentificationInfoList != null && kycnIdentificationInfoList.size() != 0) {
				for (KycnIdentificationInfo kycnIdentificationInfo : kycnIdentificationInfoList) {

					String hashValueForKycnIdentificationInfo = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, doNotIncludeUser,
									kycnPersonalInfo.getId());
					String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo, user, includeUser,
									kycnPersonalInfo.getId());
					if (kycnIdentificationInfo.getId() != 0) {
						if (kycnIdentificationInfo.isChange()) {
							kycnDaoImpl.updateKycnIndentificationInfo(kycnIdentificationInfo, kycnPersonalInfoId,
									hashValueForKycnIdentificationInfo, user);
							kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo,
									kycnPersonalInfoId, hashValueForKycnIdentificationInfoUpdate, user);
						}
					} else {
						kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalInfoId,
								hashValueForKycnIdentificationInfo, user);
						kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalInfoId,
								hashValueForKycnIdentificationInfoUpdate, user);
					}
				}
			}
			updateAndInsertKycnFinancialInfo(financialInfo, kycnPersonalInfoId, user);
			updateAndInsertKycnRelatedPersonInfo(listKycnRelatedPersonInfo, kycnPersonalInfoId, user);
			updateAndInsertKycnRelatedEntityInfo(listKycnRelatedEntityInfo, kycnPersonalInfoId, user);
			updateAndInsertKycnAccountInfo(kycnAccountsInfoList, kycnPersonalInfoId, user);
			updateAndInsertKycnAddressInfo(kycnAddressInfoList, kycnPersonalInfoId, user);
			updateAndInsertKycnEducationInfo(kycnEducationInfoList, kycnPersonalInfoId, user);
			updateAndInsertKycnInvolvementInfo(kycnInvolvementInfoList, kycnPersonalInfoId, user);
			updateAndInsertKycnRealtionInfo(kycnRelationInfoList, kycnPersonalInfoId, user);
			updateAndInsertKycnObservationInfo(kycnObservation, kycnPersonalInfoId, user);
			updateAndInsertKycnAMlInfo(kycnAmlInfo, kycnPersonalInfoId, user);
			updateAndInsertKycnDocumentStatus(documentStatusInfoList, kycnPersonalInfoId, user);
			kycnDaoImpl.updateScreeningNaturalWorkflow(kycnPersonalInfo.getScreeningId());
		}
		return kycnPersonalInfoId;
	}
}
