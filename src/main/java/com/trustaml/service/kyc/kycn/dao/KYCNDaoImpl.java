package com.trustaml.service.kyc.kycn.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfo;
import com.trustaml.service.kyc.kycn.model.KycnAccountsInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnAmlInformationInfo;
import com.trustaml.service.kyc.kycn.model.KycnDocumentStatusInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfo;
import com.trustaml.service.kyc.kycn.model.KycnEducationInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnFinancialInfo;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfo;
import com.trustaml.service.kyc.kycn.model.KycnInvolvementInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnObservationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnRelatedEntityInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelatedPersonInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfo;
import com.trustaml.service.kyc.kycn.model.KycnRelationInfoUpdate;

public class KYCNDaoImpl {

	@Inject
	DBConnection dbConnection;

	Long kycnPersonalInfoId = 0L;
	
	Long kycnAccountsInfoId = 0L;

	public String saveKYC(String str) {
		return str;
	}

	/**
	 * @param kycnPersonalInfo
	 * @param user
	 * @param hashValueForKycnPersonInfo
	 * @return
	 * @throws SQLException
	 */
	public Long insertKycnPersonalInfo(KycnPersonalInfo kycnPersonalInfo, User user, String hashValueForKycnPersonInfo)
			throws SQLException {
		String sql = "INSERT INTO kycn_personal_info"
				+ "(salutation,first_name,middle_name,last_name,ls_title,lsf_name,"
				+ "lsm_name,lsl_name,second_name,called_by_name,previous_name,gender,"
				+ "date_of_birth,age,minor,marital_status,notes,"
				+ " customer_type,customer_group,customer_constitution,customer_community,customer_caste,customer_employee_id,customer_open_date,"
				+ "customer_status_code,non_resident_external,card_holder,customer_maker,screening_id,cust_id,verified_record,hash,branch_sol_id,date_of_birth_bs,is_updated,goaml_gender_type,incomplete,"
				+ "is_active,modified_date,is_simplified_kyc,is_primary) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, kycnPersonalInfo.getSalutation());
			ps.setString(2, kycnPersonalInfo.getFirstName());
			ps.setString(3, kycnPersonalInfo.getMiddleName());
			ps.setString(4, kycnPersonalInfo.getLastName());
			ps.setString(5, kycnPersonalInfo.getLsTitle());
			ps.setString(6, kycnPersonalInfo.getLsfName());
			ps.setString(7, kycnPersonalInfo.getLsmName());
			ps.setString(8, kycnPersonalInfo.getLslName());
			ps.setString(9, kycnPersonalInfo.getSecondName());
			ps.setString(10, kycnPersonalInfo.getCalledByName());
			ps.setString(11, kycnPersonalInfo.getPreviousName());
			ps.setString(12, kycnPersonalInfo.getGender());
			ps.setDate(13, kycnPersonalInfo.getDateOfBirth());
			ps.setString(14, kycnPersonalInfo.getAge());
			ps.setBoolean(15, kycnPersonalInfo.isMinor());
			ps.setString(16, kycnPersonalInfo.getMaritalStatus());
			ps.setString(17, kycnPersonalInfo.getNotes());
			ps.setString(18, kycnPersonalInfo.getCustomerType());
			ps.setString(19, kycnPersonalInfo.getCustomerGroup());
			ps.setString(20, kycnPersonalInfo.getCustomerConstitution());
			ps.setString(21, kycnPersonalInfo.getCustomerCommunity());
			ps.setString(22, kycnPersonalInfo.getCustomerCaste());
			ps.setString(23, kycnPersonalInfo.getCustomerEmployeeId());
			ps.setDate(24, kycnPersonalInfo.getCustomerOpenDate());
			ps.setString(25, kycnPersonalInfo.getCustomerStatusCode());
			ps.setBoolean(26, kycnPersonalInfo.isNonResidentExternal());
			ps.setBoolean(27, kycnPersonalInfo.isCardHolder());
			ps.setString(28, user.getUserName());
			ps.setLong(29, kycnPersonalInfo.getScreeningId());
			ps.setString(30, kycnPersonalInfo.getCustomerId());
			ps.setBoolean(31, kycnPersonalInfo.isVerified());
			ps.setString(32, hashValueForKycnPersonInfo);
			ps.setString(33, user.getSolId());
			ps.setDate(34, kycnPersonalInfo.getDateOfBirthBS());
			ps.setBoolean(35, true);
			ps.setString(36, kycnPersonalInfo.getGoAMLGender());
			ps.setBoolean(37, kycnPersonalInfo.isIncomplete());
			ps.setBoolean(38, true);
			ps.setDate(39,new java.sql.Date( System.currentTimeMillis()));
			ps.setBoolean(40, kycnPersonalInfo.isSimplifiedKyc());
			ps.setBoolean(41, kycnPersonalInfo.isPrimary() );
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				kycnPersonalInfoId = rs.getLong(1);
			}

		} finally {
			ps.close();
			con.close();
		}
		return kycnPersonalInfoId;
	}

	/**
	 * @param kycnPersonalInfo
	 * @param user
	 * @param hashValueForKycnPersonInfoUpdate
	 * @throws SQLException
	 */
	public void insertIntoKycnPersonalInfoUpdateTable(KycnPersonalInfo kycnPersonalInfo, User user,
			String hashValueForKycnPersonInfoUpdate) throws SQLException {
		KycnPersonalInfoUpdate kycnPersonalInfoUpdate = new KycnPersonalInfoUpdate(kycnPersonalInfo);

		String sql = "INSERT INTO kycn_personal_info_update"
				+ "(salutation,first_name,middle_name,last_name,ls_title,lsf_name,"
				+ "lsm_name,lsl_name,second_name,called_by_name,previous_name,gender,"
				+ "date_of_birth,age,minor,marital_status,notes,"
				+ " customer_type,customer_group,customer_constitution,customer_community,customer_caste,customer_employee_id,customer_open_date,"
				+ "customer_status_code,non_resident_external,card_holder,customer_maker,"
				+ "maker,checker,approved,reason,kycn_personal_info_id,screening_id,cust_id,verified_record,hash,date_of_birth_bs,goaml_gender_type,incomplete,modified_date) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, kycnPersonalInfoUpdate.getSalutation());
			ps.setString(2, kycnPersonalInfoUpdate.getFirstName());
			ps.setString(3, kycnPersonalInfoUpdate.getMiddleName());
			ps.setString(4, kycnPersonalInfoUpdate.getLastName());
			ps.setString(5, kycnPersonalInfoUpdate.getLsTitle());
			ps.setString(6, kycnPersonalInfoUpdate.getLsfName());
			ps.setString(7, kycnPersonalInfoUpdate.getLsmName());
			ps.setString(8, kycnPersonalInfoUpdate.getLslName());
			ps.setString(9, kycnPersonalInfoUpdate.getSecondName());
			ps.setString(10, kycnPersonalInfoUpdate.getCalledByName());
			ps.setString(11, kycnPersonalInfoUpdate.getPreviousName());
			ps.setString(12, kycnPersonalInfoUpdate.getGender());
			ps.setDate(13, kycnPersonalInfoUpdate.getDateOfBirth());
			ps.setString(14, kycnPersonalInfoUpdate.getAge());
			ps.setBoolean(15, kycnPersonalInfoUpdate.isMinor());
			ps.setString(16, kycnPersonalInfoUpdate.getMaritalStatus());
			ps.setString(17, kycnPersonalInfoUpdate.getNotes());
			ps.setString(18, kycnPersonalInfoUpdate.getCustomerType());
			ps.setString(19, kycnPersonalInfoUpdate.getCustomerGroup());
			ps.setString(20, kycnPersonalInfoUpdate.getCustomerConstitution());
			ps.setString(21, kycnPersonalInfoUpdate.getCustomerCommunity());
			ps.setString(22, kycnPersonalInfoUpdate.getCustomerCaste());
			ps.setString(23, kycnPersonalInfoUpdate.getCustomerEmployeeId());
			ps.setDate(24, kycnPersonalInfoUpdate.getCustomerOpenDate());
			ps.setString(25, kycnPersonalInfoUpdate.getCustomerStatusCode());
			ps.setBoolean(26, kycnPersonalInfoUpdate.isNonResidentExternal());
			ps.setBoolean(27, kycnPersonalInfoUpdate.isCardHolder());
			ps.setString(28, user.getUserName());
			// Update fields
			ps.setString(29, user.getUserName());
			ps.setString(30, user.getUserName());
			ps.setBoolean(31, kycnPersonalInfoUpdate.isApproved());
			ps.setString(32, kycnPersonalInfoUpdate.getNotes());
			ps.setLong(33, kycnPersonalInfo.getId());
			ps.setLong(34, kycnPersonalInfo.getScreeningId());
			ps.setString(35, kycnPersonalInfo.getCustomerId());
			ps.setBoolean(36, kycnPersonalInfo.isVerified());
			ps.setString(37, hashValueForKycnPersonInfoUpdate);
			ps.setDate(38, kycnPersonalInfo.getDateOfBirthBS());
			ps.setString(39, kycnPersonalInfo.getGoAMLGender());
			ps.setBoolean(40, kycnPersonalInfo.isIncomplete());
			ps.setDate(41,new java.sql.Date( System.currentTimeMillis()));
			
			
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnIdentificationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnIdentificationInfo
	 * @param user
	 * @throws SQLException
	 */
	public void insertKycnIdentificationInfo(KycnIdentificationInfo kycnIdentificationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnIdentificationInfo, User user) throws SQLException {
		String sql = "INSERT INTO kycn_identification_info(kycn_personal_info_id,primary_identification_document_type"
				+ ",primary_identification_document_no,country_of_issue,passport_no,passport_country,"
				+ "passport_issuing_authority,passport_place_of_issue,passport_issue_date,expiry_date,"
				+ "visa_no,visa_expiry_date,nepal_entry_date,notes,hash,goaml_type,goaml_country_of_issue,is_active) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnIdentificationInfo.getPrimaryIdentificationDocumentType());
			ps.setString(3, kycnIdentificationInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(4, kycnIdentificationInfo.getCountryOfIssue());
			ps.setString(5, kycnIdentificationInfo.getPassportNo());
			ps.setString(6, kycnIdentificationInfo.getPassportCountry());
			ps.setString(7, kycnIdentificationInfo.getPassportIssuingAuthority());
			ps.setString(8, kycnIdentificationInfo.getPassportPlaceOfIssue());
			ps.setDate(9, kycnIdentificationInfo.getPassportIssueDate());
			ps.setDate(10, kycnIdentificationInfo.getExpiryDate());
			ps.setString(11, kycnIdentificationInfo.getVisaNo());
			ps.setDate(12, kycnIdentificationInfo.getVisaExpiryDate());
			ps.setDate(13, kycnIdentificationInfo.getNepalEntryDate());
			ps.setString(14, kycnIdentificationInfo.getNotes());
			ps.setString(15, hashValueForKycnIdentificationInfo);
			ps.setString(16, kycnIdentificationInfo.getGoAMLPrimaryIdentificationDocumentType());
			ps.setString(17, kycnIdentificationInfo.getGoAMLCountryOfIssue());
			ps.setBoolean(18, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnIdentificationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnIdentificationInfoUpdate
	 * @param user
	 * @throws SQLException
	 */
	public void insertKycnIdentificationInfoUpdateTable(KycnIdentificationInfo kycnIdentificationInfo,
			Long kycnPersonalInfoId, String hashValueForKycnIdentificationInfoUpdate, User user) throws SQLException {
		KycnIdentificationInfoUpdate kycnIdentificationInfoUpdate = new KycnIdentificationInfoUpdate(
				kycnIdentificationInfo);
		String sql = "INSERT INTO kycn_identification_info_update(kycn_personal_info_id,primary_identification_document_type"
				+ ",primary_identification_document_no,country_of_issue,passport_no,passport_country,"
				+ "passport_issuing_authority,passport_place_of_issue,passport_issue_date,expiry_date,visa_no,visa_expiry_date,nepal_entry_date,notes,maker,checker,hash,goaml_type,goaml_country_of_issue,is_active)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnIdentificationInfoUpdate.getPrimaryIdentificationDocumentType());
			ps.setString(3, kycnIdentificationInfoUpdate.getPrimaryIdentificationDocumentNo());
			ps.setString(4, kycnIdentificationInfoUpdate.getCountryOfIssue());
			ps.setString(5, kycnIdentificationInfoUpdate.getPassportNo());
			ps.setString(6, kycnIdentificationInfoUpdate.getPassportCountry());
			ps.setString(7, kycnIdentificationInfoUpdate.getPassportIssuingAuthority());
			ps.setString(8, kycnIdentificationInfoUpdate.getPassportPlaceOfIssue());
			ps.setDate(9, kycnIdentificationInfoUpdate.getPassportIssueDate());
			ps.setDate(10, kycnIdentificationInfoUpdate.getExpiryDate());
			ps.setString(11, kycnIdentificationInfoUpdate.getVisaNo());
			ps.setDate(12, kycnIdentificationInfoUpdate.getVisaExpiryDate());
			ps.setDate(13, kycnIdentificationInfoUpdate.getNepalEntryDate());
			ps.setString(14, kycnIdentificationInfoUpdate.getNotes());
			ps.setString(15, user.getUserName());
			ps.setString(16, user.getUserName());
			ps.setString(17, hashValueForKycnIdentificationInfoUpdate);
			ps.setString(18, kycnIdentificationInfoUpdate.getGoAMLPrimaryIdentificationDocumentType());
			ps.setString(19, kycnIdentificationInfoUpdate.getGoAMLCountryOfIssue());
			ps.setBoolean(20, true);
			ps.executeUpdate();
		} finally

		{
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAddressInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAddressInfo
	 * @throws SQLException
	 */
	public void insertKycnAddressInfo(KycnAddressInfo kycnAddressInfo, Long kycnPersonalInfoId,
			String hashValueForKycnAddressInfo, User user) throws SQLException {
		String sql = "INSERT INTO kycn_address_info "
				+ "(kycn_personal_info_id,address_type,country,zone,district,mn_vdc,pinzip,ward_number,tole_area,"
				+ "street,house_no,unit_number,nearest_landmark,latitude,longitude,phone_no_country_code,phone_no_area_code,"
				+ "phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,"
				+ "pager_no,email_id,notes,hash,province,address,city,goaml_address_type,goaml_country,"
				+ "is_active,address_removed_by) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAddressInfo.getType());
			ps.setString(3, kycnAddressInfo.getCountry());
			ps.setString(4, kycnAddressInfo.getZone());
			ps.setString(5, kycnAddressInfo.getDistrict());
			ps.setString(6, kycnAddressInfo.getMnVdc());
			ps.setString(7, kycnAddressInfo.getPinZip());
			ps.setString(8, kycnAddressInfo.getWardNumber());
			ps.setString(9, kycnAddressInfo.getToleArea());
			ps.setString(10, kycnAddressInfo.getStreet());
			ps.setString(11, kycnAddressInfo.getHouseNumber());
			ps.setString(12, kycnAddressInfo.getUnitNumber());
			ps.setString(13, kycnAddressInfo.getNearestLandMark());
			ps.setString(14, kycnAddressInfo.getLatitude());
			ps.setString(15, kycnAddressInfo.getLongitude());
			ps.setString(16, kycnAddressInfo.getPhoneNoCountryCode());
			ps.setString(17, kycnAddressInfo.getPhoneNoAreaCode());
			ps.setString(18, kycnAddressInfo.getPhoneNo());
			ps.setString(19, kycnAddressInfo.getTelexNoCountryCode());
			ps.setString(20, kycnAddressInfo.getTelexNoAreaCode());
			ps.setString(21, kycnAddressInfo.getTelexNo());
			ps.setString(22, kycnAddressInfo.getPagerNoCountryCode());
			ps.setString(23, kycnAddressInfo.getPagerNoAreaCode());
			ps.setString(24, kycnAddressInfo.getPagerNo());
			ps.setString(25, kycnAddressInfo.getEmailId());
			ps.setString(26, kycnAddressInfo.getNotes());
			ps.setString(27, hashValueForKycnAddressInfo);
			ps.setString(28, kycnAddressInfo.getProvince());
			ps.setString(29, kycnAddressInfo.getAddress());
			ps.setString(30, kycnAddressInfo.getCity());
			ps.setString(31, kycnAddressInfo.getGoAMLAddressType());
			ps.setString(32, kycnAddressInfo.getGoAMLCountryType());
			ps.setBoolean(33, Boolean.parseBoolean(kycnAddressInfo.getRemovedAddressStatus()));
			ps.setString(34, user.getUserName());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAddressInfoUpdate
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnAddressInfoUpdate
	 * @throws SQLException
	 */

	public void insertKycnAddressInfoUpdateTable(KycnAddressInfoUpdate kycnAddressInfoUpdate, Long kycnPersonalInfoId,
			User user, String hashValueForKycnAddressInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_address_info_update "
				+ "(kycn_personal_info_id,address_type,country,zone,district,mn_vdc,pinzip,ward_number,tole_area,"
				+ "street,house_no,unit_number,nearest_landmark,latitude,longitude,phone_no_country_code,phone_no_area_code,"
				+ "phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,"
				+ "pager_no,email_id,notes,maker,checker,hash,province,address,city,goaml_address_type,goaml_country,"
				+ "is_active,address_removed_by) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAddressInfoUpdate.getType());
			ps.setString(3, kycnAddressInfoUpdate.getCountry());
			ps.setString(4, kycnAddressInfoUpdate.getZone());
			ps.setString(5, kycnAddressInfoUpdate.getDistrict());
			ps.setString(6, kycnAddressInfoUpdate.getMnVdc());
			ps.setString(7, kycnAddressInfoUpdate.getPinZip());
			ps.setString(8, kycnAddressInfoUpdate.getWardNumber());
			ps.setString(9, kycnAddressInfoUpdate.getToleArea());
			ps.setString(10, kycnAddressInfoUpdate.getStreet());
			ps.setString(11, kycnAddressInfoUpdate.getHouseNumber());
			ps.setString(12, kycnAddressInfoUpdate.getUnitNumber());
			ps.setString(13, kycnAddressInfoUpdate.getNearestLandMark());
			ps.setString(14, kycnAddressInfoUpdate.getLatitude());
			ps.setString(15, kycnAddressInfoUpdate.getLongitude());
			ps.setString(16, kycnAddressInfoUpdate.getPhoneNoCountryCode());
			ps.setString(17, kycnAddressInfoUpdate.getPhoneNoAreaCode());
			ps.setString(18, kycnAddressInfoUpdate.getPhoneNo());
			ps.setString(19, kycnAddressInfoUpdate.getTelexNoCountryCode());
			ps.setString(20, kycnAddressInfoUpdate.getTelexNoAreaCode());
			ps.setString(21, kycnAddressInfoUpdate.getTelexNo());
			ps.setString(22, kycnAddressInfoUpdate.getPagerNoCountryCode());
			ps.setString(23, kycnAddressInfoUpdate.getPagerNoAreaCode());
			ps.setString(24, kycnAddressInfoUpdate.getPagerNo());
			ps.setString(25, kycnAddressInfoUpdate.getEmailId());
			ps.setString(26, kycnAddressInfoUpdate.getNotes());
			ps.setString(27, user.getUserName());
			ps.setString(28, user.getUserName());
			ps.setString(29, hashValueForKycnAddressInfoUpdate);
			ps.setString(30, kycnAddressInfoUpdate.getProvince());
			ps.setString(31, kycnAddressInfoUpdate.getAddress());
			ps.setString(32, kycnAddressInfoUpdate.getCity());
			ps.setString(33, kycnAddressInfoUpdate.getGoAMLAddressType());
			ps.setString(34, kycnAddressInfoUpdate.getGoAMLCountryType());
			ps.setBoolean(35, Boolean.parseBoolean(kycnAddressInfoUpdate.getRemovedAddressStatus()));
			ps.setString(36, user.getUserName());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnRelationInfo
	 * @throws SQLException
	 */
	public void insertKycnRelationInfo(KycnRelationInfo kycnRelationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnRelationInfo) throws SQLException {
		String sql = "INSERT INTO kycn_relation_info"
				+ "(kycn_personal_info_id,relation_type,cust_id,kycn_id,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,"
				+ "primary_identification_document_type,primary_identification_document_no,"
				+ "relation_country,issuing_authority,place_of_issue,issue_date,expiry_date,notes,salutation,hash,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnRelationInfo.getRelationType());
			ps.setString(3, kycnRelationInfo.getCustId());
			ps.setString(4, kycnRelationInfo.getKycnId());
			ps.setString(5, kycnRelationInfo.getFirstName());
			ps.setString(6, kycnRelationInfo.getMiddleName());
			ps.setString(7, kycnRelationInfo.getLastName());
			ps.setString(8, kycnRelationInfo.getLsfName());
			ps.setString(9, kycnRelationInfo.getLsmName());
			ps.setString(10, kycnRelationInfo.getLslName());
			ps.setString(11, kycnRelationInfo.getSecondName());
			ps.setString(12, kycnRelationInfo.getCalledByName());
			ps.setString(13, kycnRelationInfo.getPrimaryIdentificationDocumentType());
			ps.setString(14, kycnRelationInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(15, kycnRelationInfo.getRelationCountry());
			ps.setString(16, kycnRelationInfo.getIssuingAuthority());
			ps.setString(17, kycnRelationInfo.getPlaceOfIssue());
			ps.setDate(18, kycnRelationInfo.getIssueDate());
			ps.setDate(19, kycnRelationInfo.getExpiryDate());
			ps.setString(20, kycnRelationInfo.getNotes());
			ps.setString(21, kycnRelationInfo.getSalutation());
			ps.setString(22, hashValueForKycnRelationInfo);
			ps.setBoolean(23, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnRelationInfoUpdate
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnRelationInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnRelationInfoUpdateTable(KycnRelationInfoUpdate kycnRelationInfoUpdate,
			Long kycnPersonalInfoId, User user, String hashValueForKycnRelationInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_relation_info_update"
				+ "(kycn_personal_info_id,relation_type,cust_id,kycn_id,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,"
				+ "primary_identification_document_type,primary_identification_document_no,"
				+ "relation_country,issuing_authority,place_of_issue,issue_date,expiry_date,notes,maker,checker,salutation,hash,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnRelationInfoUpdate.getRelationType());
			ps.setString(3, kycnRelationInfoUpdate.getCustId());
			ps.setString(4, kycnRelationInfoUpdate.getKycnId());
			ps.setString(5, kycnRelationInfoUpdate.getFirstName());
			ps.setString(6, kycnRelationInfoUpdate.getMiddleName());
			ps.setString(7, kycnRelationInfoUpdate.getLastName());
			ps.setString(8, kycnRelationInfoUpdate.getLsfName());
			ps.setString(9, kycnRelationInfoUpdate.getLsmName());
			ps.setString(10, kycnRelationInfoUpdate.getLslName());
			ps.setString(11, kycnRelationInfoUpdate.getSecondName());
			ps.setString(12, kycnRelationInfoUpdate.getCalledByName());
			ps.setString(13, kycnRelationInfoUpdate.getPrimaryIdentificationDocumentType());
			ps.setString(14, kycnRelationInfoUpdate.getPrimaryIdentificationDocumentNo());
			ps.setString(15, kycnRelationInfoUpdate.getRelationCountry());
			ps.setString(16, kycnRelationInfoUpdate.getIssuingAuthority());
			ps.setString(17, kycnRelationInfoUpdate.getPlaceOfIssue());
			ps.setDate(18, kycnRelationInfoUpdate.getIssueDate());
			ps.setDate(19, kycnRelationInfoUpdate.getExpiryDate());
			ps.setString(20, kycnRelationInfoUpdate.getNotes());
			ps.setString(21, user.getUserName());
			ps.setString(22, user.getUserName());
			ps.setString(23, kycnRelationInfoUpdate.getSalutation());
			ps.setString(24, hashValueForKycnRelationInfoUpdate);
			ps.setBoolean(25, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnEducationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnEducationInfo
	 * @throws SQLException
	 */
	public void insertKycnEducationInfo(KycnEducationInfo kycnEducationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnEducationInfo) throws SQLException {
		String sql = "INSERT INTO kycn_education_info"
				+ "(kycn_personal_info_id,qualification,name_of_institute,field_of_study,year_of_graduation,notes,hash,is_active)"
				+ "	VALUES(?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnEducationInfo.getQualification());
			ps.setString(3, kycnEducationInfo.getNameOfInstitute());
			ps.setString(4, kycnEducationInfo.getFieldOfStudy());
			ps.setString(5, kycnEducationInfo.getYearOfGraduation());
			ps.setString(6, kycnEducationInfo.getNotes());
			ps.setString(7, hashValueForKycnEducationInfo);
			ps.setBoolean(8, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnEducationInfoUpdate
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnEducationInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnEducationInfoUpdateTable(KycnEducationInfoUpdate kycnEducationInfoUpdate,
			Long kycnPersonalInfoId, User user, String hashValueForKycnEducationInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_education_info_update"
				+ "(kycn_personal_info_id,qualification,name_of_institute,field_of_study,year_of_graduation,notes,maker,checker,hash,is_active)"
				+ "	VALUES(?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnEducationInfoUpdate.getQualification());
			ps.setString(3, kycnEducationInfoUpdate.getNameOfInstitute());
			ps.setString(4, kycnEducationInfoUpdate.getFieldOfStudy());
			ps.setString(5, kycnEducationInfoUpdate.getYearOfGraduation());
			ps.setString(6, kycnEducationInfoUpdate.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, hashValueForKycnEducationInfoUpdate);
			ps.setBoolean(10, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAccountsInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAccountsInfo
	 * @throws SQLException
	 */
	public Long insertKycnAccountsInfo(KycnAccountsInfo kycnAccountsInfo, Long kycnPersonalInfoId,
			String hashValueForKycnAccountsInfo) throws SQLException {
		String sql = "INSERT INTO kycn_accounts_info"
				+ "(kycn_personal_info_id,account_id,for_account_id,currency_of_account,account_no,account_name,"
				+ "account_short_name,account_ownership,scheme_type,gl_sub_head_code,product_group,"
				+ "last_transaction_date, account_open_date,estimated_yearly_transactions, "
				+ "estimated_monthly_transactions,estimated_yearly_turnover,estimated_monthly_turnover,"
				+ "regular_source_of_income, source_of_fund,notes,scheme_code,hash,scheme_description,nature_of_account,is_blocked,"
				+ "account_status_type,account_type,goaml_account_status_type,goaml_personal_account_type,branch_sol_id,is_active) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			//ps = con.prepareStatement(sql);
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAccountsInfo.getAccountId());
			ps.setString(3, kycnAccountsInfo.getForAccountId());
			ps.setString(4, kycnAccountsInfo.getCurrencyOfAccount());
			ps.setString(5, kycnAccountsInfo.getAccountNo());
			ps.setString(6, kycnAccountsInfo.getAccountName());
			ps.setString(7, kycnAccountsInfo.getAccountShortName());
			ps.setString(8, kycnAccountsInfo.getAccountOwnership());
			ps.setString(9, kycnAccountsInfo.getSchemeType());
			ps.setString(10, kycnAccountsInfo.getGlSubHeadCode());
			ps.setString(11, kycnAccountsInfo.getProductGroup());
			ps.setDate(12, kycnAccountsInfo.getLastTransactionDate());
			ps.setDate(13, kycnAccountsInfo.getAccountOpenDate());
			/*ps.setString(14, kycnAccountsInfo.getEstimatedYearlyTransaction());
			ps.setString(15, kycnAccountsInfo.getEstimatedMonthlyTransaction());
			ps.setString(16, kycnAccountsInfo.getEstimatedYearlyTurnOver());
			ps.setString(17, kycnAccountsInfo.getEstimatedMonthlyTurnOver());*/
			
			if (kycnAccountsInfo.getEstimatedYearlyTransaction().equals("")) {
				ps.setNull(14, java.sql.Types.INTEGER);
			} else {
				ps.setInt(14, Integer.parseInt(kycnAccountsInfo.getEstimatedYearlyTransaction()));
			}
			if (kycnAccountsInfo.getEstimatedMonthlyTransaction().equals("")) {
				ps.setNull(15, java.sql.Types.INTEGER);
			} else {
				ps.setInt(15, Integer.parseInt(kycnAccountsInfo.getEstimatedMonthlyTransaction()));
			}
			if (kycnAccountsInfo.getEstimatedYearlyTurnOver().equals("")) {
				ps.setNull(16, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(16, new BigDecimal(kycnAccountsInfo.getEstimatedYearlyTurnOver()));
			}
			if (kycnAccountsInfo.getEstimatedMonthlyTurnOver().equals("")) {
				ps.setNull(17, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(17,new BigDecimal(kycnAccountsInfo.getEstimatedMonthlyTurnOver()));
			}
			
			
			ps.setString(18, kycnAccountsInfo.getRegularSourceOfIncome());
			ps.setString(19, kycnAccountsInfo.getSourceOfFund());
			ps.setString(20, kycnAccountsInfo.getNotes());
			ps.setString(21, kycnAccountsInfo.getSchemeCode());
			ps.setString(22, hashValueForKycnAccountsInfo);
			ps.setString(23, kycnAccountsInfo.getSchemeDescription());
			ps.setString(24, kycnAccountsInfo.getNatureOfAccount());
			ps.setString(25, kycnAccountsInfo.getIsBlocked());
			ps.setString(26,kycnAccountsInfo.getAccountStatusType());
			ps.setString(27,kycnAccountsInfo.getAccountType());
			ps.setString(28,kycnAccountsInfo.getGoAMLAccountStatusType());
			ps.setString(29,kycnAccountsInfo.getGoAMLaccountType());
			ps.setString(30,kycnAccountsInfo.getBranchSolId());
			ps.setBoolean(31, Boolean.parseBoolean(kycnAccountsInfo.getRemovedAccountStatus()));
			
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				kycnAccountsInfoId = rs.getLong(1);
			}
			
		} finally {
			ps.close();
			con.close();
		}
		return kycnAccountsInfoId;
	}

	/**
	 * @param kycnAccountsInfoUpdate
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnAccountsInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnAccountsInfoUpdateTable(KycnAccountsInfoUpdate kycnAccountsInfoUpdate,
			Long kycnPersonalInfoId, User user, String hashValueForKycnAccountsInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_accounts_info_update"
				+ "(kycn_personal_info_id,account_id,for_account_id,currency_of_account,account_no,account_name,"
				+ "account_short_name,account_ownership,scheme_type,gl_sub_head_code,product_group,"
				+ "last_transaction_date, account_open_date,estimated_yearly_transactions, "
				+ "estimated_monthly_transactions,estimated_yearly_turnover,estimated_monthly_turnover,"
				+ "regular_source_of_income, source_of_fund,notes,maker,checker,scheme_code,hash,nature_of_account,scheme_description,"
				+ "account_status_type,account_type,goaml_account_status_type,goaml_personal_account_type,branch_sol_id,is_active) " // ,is_blocked
																																			// to
																																			// be
																																			// added
																																			// in
																																			// database
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAccountsInfoUpdate.getAccountId());
			ps.setString(3, kycnAccountsInfoUpdate.getForAccountId());
			ps.setString(4, kycnAccountsInfoUpdate.getCurrencyOfAccount());
			ps.setString(5, kycnAccountsInfoUpdate.getAccountNo());
			ps.setString(6, kycnAccountsInfoUpdate.getAccountName());
			ps.setString(7, kycnAccountsInfoUpdate.getAccountShortName());
			ps.setString(8, kycnAccountsInfoUpdate.getAccountOwnership());
			ps.setString(9, kycnAccountsInfoUpdate.getSchemeType());
			ps.setString(10, kycnAccountsInfoUpdate.getGlSubHeadCode());
			ps.setString(11, kycnAccountsInfoUpdate.getProductGroup());
			ps.setDate(12, kycnAccountsInfoUpdate.getLastTransactionDate());
			ps.setDate(13, kycnAccountsInfoUpdate.getAccountOpenDate());
			/*ps.setString(14, kycnAccountsInfoUpdate.getEstimatedYearlyTransaction());
			ps.setString(15, kycnAccountsInfoUpdate.getEstimatedMonthlyTransaction());
			ps.setString(16, kycnAccountsInfoUpdate.getEstimatedYearlyTurnOver());
			ps.setString(17, kycnAccountsInfoUpdate.getEstimatedMonthlyTurnOver());*/
			
			
			if (kycnAccountsInfoUpdate.getEstimatedYearlyTransaction().equals("")) {
				ps.setNull(14, java.sql.Types.INTEGER);
			} else {
				ps.setInt(14, Integer.parseInt(kycnAccountsInfoUpdate.getEstimatedYearlyTransaction()));
			}
			if (kycnAccountsInfoUpdate.getEstimatedMonthlyTransaction().equals("")) {
				ps.setNull(15, java.sql.Types.INTEGER);
			} else {
				ps.setInt(15, Integer.parseInt(kycnAccountsInfoUpdate.getEstimatedMonthlyTransaction()));
			}
			if (kycnAccountsInfoUpdate.getEstimatedYearlyTurnOver().equals("")) {
				ps.setNull(16, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(16, new BigDecimal(kycnAccountsInfoUpdate.getEstimatedYearlyTurnOver()));
			}
			if (kycnAccountsInfoUpdate.getEstimatedMonthlyTurnOver().equals("")) {
				ps.setNull(17, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(17,new BigDecimal(kycnAccountsInfoUpdate.getEstimatedMonthlyTurnOver()));
			}
			
			ps.setString(18, kycnAccountsInfoUpdate.getRegularSourceOfIncome());
			ps.setString(19, kycnAccountsInfoUpdate.getSourceOfFund());
			ps.setString(20, kycnAccountsInfoUpdate.getNotes());
			ps.setString(21, user.getUserName());
			ps.setString(22, user.getUserName());
			ps.setString(23, kycnAccountsInfoUpdate.getSchemeCode());
			ps.setString(24, hashValueForKycnAccountsInfoUpdate);
			ps.setString(25, kycnAccountsInfoUpdate.getSchemeDescription());
			ps.setString(26, kycnAccountsInfoUpdate.getNatureOfAccount());
			ps.setString(27,kycnAccountsInfoUpdate.getAccountStatusType());
			ps.setString(28,kycnAccountsInfoUpdate.getAccountType());
			ps.setString(29,kycnAccountsInfoUpdate.getGoAMLAccountStatusType());
			ps.setString(30,kycnAccountsInfoUpdate.getGoAMLaccountType());
			ps.setString(31,kycnAccountsInfoUpdate.getBranchSolId());
			ps.setBoolean(32, Boolean.parseBoolean(kycnAccountsInfoUpdate.getRemovedAccountStatus()));
			// ps.setString(26, kycnAccountsInfoUpdate.getIsBlocked());
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnInvolvementInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnInvolvementInfo
	 * @throws SQLException
	 */
	public void insertKycnInvolvementInfo(KycnInvolvementInfo kycnInvolvementInfo, Long kycnPersonalInfoId,
			String hashValueForKycnInvolvementInfo) throws SQLException {
		String sql = "INSERT INTO kycn_involvement_info"
				+ "(kycn_personal_info_id,kycl_id,organization_nature,country,zone,district,mn_vdc,pinzip,"
				+ "involvement_ward_number,tole_area,street,house_no,unit_number,nearest_landmark,latitude,longitude,"
				+ "phone_no_country_code,phone_no_area_code,phone_no,fax_no_country_code,fax_no_area_code,fax_no,email_id,"
				+ "website,panvat,poboxno,nature,designation,start_date,end_date,involvement_notes,nature_of_involvement,occupation_type,hash,province,"
				+ "organization_name,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setLong(2, kycnInvolvementInfo.getKyclId());
			ps.setString(3, kycnInvolvementInfo.getOrganizationNature());
			ps.setString(4, kycnInvolvementInfo.getCountry());
			ps.setString(5, kycnInvolvementInfo.getZone());
			ps.setString(6, kycnInvolvementInfo.getDistrict());
			ps.setString(7, kycnInvolvementInfo.getMnVdc());
			ps.setString(8, kycnInvolvementInfo.getPinZip());
			ps.setString(9, kycnInvolvementInfo.getInvolvementWardNumber());
			ps.setString(10, kycnInvolvementInfo.getToleArea());
			ps.setString(11, kycnInvolvementInfo.getStreet());
			ps.setString(12, kycnInvolvementInfo.getHouseNo());
			ps.setString(13, kycnInvolvementInfo.getUnitNumber());
			ps.setString(14, kycnInvolvementInfo.getNearestLandMark());
			ps.setString(15, kycnInvolvementInfo.getLatitude());
			ps.setString(16, kycnInvolvementInfo.getLongitude());
			ps.setString(17, kycnInvolvementInfo.getPhoneNoCountryCode());
			ps.setString(18, kycnInvolvementInfo.getPhoneNoAreaCode());
			ps.setString(19, kycnInvolvementInfo.getPhoneNo());
			ps.setString(20, kycnInvolvementInfo.getFaxNoCountryCode());
			ps.setString(21, kycnInvolvementInfo.getFaxNoAreaCode());
			ps.setString(22, kycnInvolvementInfo.getFaxNo());
			ps.setString(23, kycnInvolvementInfo.getEmailId());
			ps.setString(24, kycnInvolvementInfo.getWebsite());
			ps.setString(25, kycnInvolvementInfo.getPanVat());
			ps.setString(26, kycnInvolvementInfo.getPoBoxNo());
			ps.setString(27, kycnInvolvementInfo.getNature());
			ps.setString(28, kycnInvolvementInfo.getDesignation());
			ps.setDate(29, kycnInvolvementInfo.getStartDate());
			ps.setDate(30, kycnInvolvementInfo.getEndDate());
			ps.setString(31, kycnInvolvementInfo.getNotes());
			ps.setString(32, kycnInvolvementInfo.getInvolvementNature());
			ps.setString(33, kycnInvolvementInfo.getOccupationType());
			ps.setString(34, hashValueForKycnInvolvementInfo);
			ps.setString(35, kycnInvolvementInfo.getProvince());
			ps.setString(36, kycnInvolvementInfo.getOrganizationName());
			ps.setBoolean(37, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnInvolvementInfoUpdate
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnInvolvementInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnInvolvementInfoUpdateTable(KycnInvolvementInfoUpdate kycnInvolvementInfoUpdate,
			Long kycnPersonalInfoId, User user, String hashValueForKycnInvolvementInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_involvement_info_update"
				+ "(kycn_personal_info_id,kycl_id,organization_nature,country,zone,district,mn_vdc,pinzip,"
				+ "involvement_ward_number,tole_area,street,house_no,unit_number,nearest_landmark,latitude,longitude,"
				+ "phone_no_country_code,phone_no_area_code,phone_no,fax_no_country_code,fax_no_area_code,fax_no,email_id,"
				+ "website,panvat,poboxno,nature,designation,start_date,end_date,involvement_notes,maker,checker,nature_of_involvement,occupation_type,hash,province,"
				+ "organization_name,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setLong(2, kycnInvolvementInfoUpdate.getKyclId());
			ps.setString(3, kycnInvolvementInfoUpdate.getOrganizationNature());
			ps.setString(4, kycnInvolvementInfoUpdate.getCountry());
			ps.setString(5, kycnInvolvementInfoUpdate.getZone());
			ps.setString(6, kycnInvolvementInfoUpdate.getDistrict());
			ps.setString(7, kycnInvolvementInfoUpdate.getMnVdc());
			ps.setString(8, kycnInvolvementInfoUpdate.getPinZip());
			ps.setString(9, kycnInvolvementInfoUpdate.getInvolvementWardNumber());
			ps.setString(10, kycnInvolvementInfoUpdate.getToleArea());
			ps.setString(11, kycnInvolvementInfoUpdate.getStreet());
			ps.setString(12, kycnInvolvementInfoUpdate.getHouseNo());
			ps.setString(13, kycnInvolvementInfoUpdate.getUnitNumber());
			ps.setString(14, kycnInvolvementInfoUpdate.getNearestLandMark());
			ps.setString(15, kycnInvolvementInfoUpdate.getLatitude());
			ps.setString(16, kycnInvolvementInfoUpdate.getLongitude());
			ps.setString(17, kycnInvolvementInfoUpdate.getPhoneNoCountryCode());
			ps.setString(18, kycnInvolvementInfoUpdate.getPhoneNoAreaCode());
			ps.setString(19, kycnInvolvementInfoUpdate.getPhoneNo());
			ps.setString(20, kycnInvolvementInfoUpdate.getFaxNoCountryCode());
			ps.setString(21, kycnInvolvementInfoUpdate.getFaxNoAreaCode());
			ps.setString(22, kycnInvolvementInfoUpdate.getFaxNo());
			ps.setString(23, kycnInvolvementInfoUpdate.getEmailId());
			ps.setString(24, kycnInvolvementInfoUpdate.getWebsite());
			ps.setString(25, kycnInvolvementInfoUpdate.getPanVat());
			ps.setString(26, kycnInvolvementInfoUpdate.getPoBoxNo());
			ps.setString(27, kycnInvolvementInfoUpdate.getNature());
			ps.setString(28, kycnInvolvementInfoUpdate.getDesignation());
			ps.setDate(29, kycnInvolvementInfoUpdate.getStartDate());
			ps.setDate(30, kycnInvolvementInfoUpdate.getEndDate());
			ps.setString(31, kycnInvolvementInfoUpdate.getNotes());
			ps.setString(32, user.getUserName());
			ps.setString(33, user.getUserName());
			ps.setString(34, kycnInvolvementInfoUpdate.getInvolvementNature());
			ps.setString(35, kycnInvolvementInfoUpdate.getOccupationType());
			ps.setString(36, hashValueForKycnInvolvementInfoUpdate);
			ps.setString(37, kycnInvolvementInfoUpdate.getProvince());
			ps.setString(38, kycnInvolvementInfoUpdate.getOrganizationName());
			ps.setBoolean(39, true);

			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnObservationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnObservationInfo
	 * @throws SQLException
	 */
	public void insertKycnObservationInfo(KycnObservationInfo kycnObservationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnObservationInfo) throws SQLException {
		String sql = "INSERT INTO kycn_internal_observation(kycn_personal_info_id,observation_type"
				+ ",internal_observation_physical,internal_observation_financial,internal_observation_behavioral,internal_observation_connected_person,"
				+ "connected_person_id,internal_observation_intended_objective_of_business_relation,observation_media_source,"
				+ "observation_date,observation_time,notes,hash,is_active) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnObservationInfo.getObservationType());
			ps.setString(3, kycnObservationInfo.getInternalObservationPhysical());
			ps.setString(4, kycnObservationInfo.getInternalObservationFinancial());
			ps.setString(5, kycnObservationInfo.getInternalObservationBehavioral());
			ps.setString(6, kycnObservationInfo.getInternalObservationConnectedPerson());
			ps.setString(7, kycnObservationInfo.getConnectedPersonId());
			ps.setString(8, kycnObservationInfo.getInternalObservationIntendedObjectiveOfBusinessRelation());
			ps.setString(9, kycnObservationInfo.getObservationMediaSource());
			ps.setDate(10, kycnObservationInfo.getObservationDate());
			ps.setString(11, kycnObservationInfo.getObservationTime());
			ps.setString(12, kycnObservationInfo.getNotes());
			ps.setString(13, hashValueForKycnObservationInfo);
			ps.setBoolean(14, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnObservationInfo
	 * @param kycnPersonalInfoId
	 * @param user
	 * @param hashValueForKycnObservationInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnObservationInfoUpdateTable(KycnObservationInfo kycnObservationInfo, Long kycnPersonalInfoId,
			User user, String hashValueForKycnObservationInfoUpdate) throws SQLException {

		String sql = "INSERT INTO kycn_internal_observation_update(kycn_personal_info_id,observation_type"
				+ ",internal_observation_physical,internal_observation_financial,internal_observation_behavioral,internal_observation_connected_person,"
				+ "connected_person_id,internal_observation_intended_objective_of_business_relation,observation_media_source,"
				+ "observation_date,observation_time,notes,maker,checker,hash,is_active) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnObservationInfo.getObservationType());
			ps.setString(3, kycnObservationInfo.getInternalObservationPhysical());
			ps.setString(4, kycnObservationInfo.getInternalObservationFinancial());
			ps.setString(5, kycnObservationInfo.getInternalObservationBehavioral());
			ps.setString(6, kycnObservationInfo.getInternalObservationConnectedPerson());
			ps.setString(7, kycnObservationInfo.getConnectedPersonId());
			ps.setString(8, kycnObservationInfo.getInternalObservationIntendedObjectiveOfBusinessRelation());
			ps.setString(9, kycnObservationInfo.getObservationMediaSource());
			ps.setDate(10, kycnObservationInfo.getObservationDate());
			ps.setString(11, kycnObservationInfo.getObservationTime());
			ps.setString(12, kycnObservationInfo.getNotes());
			ps.setString(13, user.getUserName());
			ps.setString(14, user.getUserName());
			ps.setString(15, hashValueForKycnObservationInfoUpdate);
			ps.setBoolean(16, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAmlInformation
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAmlInformationInfo
	 * @throws SQLException
	 */
	public void insertKycnAmlInfo(KycnAmlInformationInfo kycnAmlInformation, Long kycnPersonalInfoId,
			String hashValueForKycnAmlInformationInfo) throws SQLException {
		String sql = "INSERT INTO kycn_aml_information(kycn_personal_info_id,fatca_country"
				+ ",us_non_us,risk_categorization,recommendations,aml_notes,aml_check,hash,is_active) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setBoolean(2, kycnAmlInformation.isFatcaCountry());
			ps.setBoolean(3, kycnAmlInformation.isUsNonUs());
			ps.setString(4, kycnAmlInformation.getRiskCategorization());
			ps.setString(5, kycnAmlInformation.getRecommendations());
			ps.setString(6, kycnAmlInformation.getAmlNotes());
			ps.setBoolean(7, kycnAmlInformation.isAmlCheck());
			ps.setString(8, hashValueForKycnAmlInformationInfo);
			ps.setBoolean(9, true);
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnPersonalInfoId
	 * @param kycnAmlInformation
	 * @param user
	 * @param hashValueForKycnAmlInformationInfoUpdate
	 * @throws SQLException
	 */
	public void updateKycnAmlInformationInfo(Long kycnPersonalInfoId, KycnAmlInformationInfo kycnAmlInformation,
			User user, String hashValueForKycnAmlInformationInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_aml_information_update(kycn_personal_info_id,fatca_country"
				+ ",us_non_us,risk_categorization,recommendations,aml_notes,aml_check,maker,checker,hash,is_active)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setBoolean(2, kycnAmlInformation.isFatcaCountry());
			ps.setBoolean(3, kycnAmlInformation.isUsNonUs());
			ps.setString(4, kycnAmlInformation.getRiskCategorization());
			ps.setString(5, kycnAmlInformation.getRecommendations());
			ps.setString(6, kycnAmlInformation.getAmlNotes());
			ps.setBoolean(7, kycnAmlInformation.isAmlCheck());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getUserName());
			ps.setString(10, hashValueForKycnAmlInformationInfoUpdate);
			ps.setBoolean(11, true);
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnDocumentStatusInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnDocumentStatusInfo
	 * @throws SQLException
	 */
	public void insertKycnDocumentStatusInfo(KycnDocumentStatusInfo kycnDocumentStatusInfo, Long kycnPersonalInfoId,
			String hashValueForKycnDocumentStatusInfo) throws SQLException {
		String sql = "INSERT INTO kycn_document_status(kycn_personal_info_id,document_type"
				+ ",document_status,date,refersh_date,notes,hash,is_active) VALUES(?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnDocumentStatusInfo.getDocumentType());
			ps.setString(3, kycnDocumentStatusInfo.getDocumentStatus());
			ps.setDate(4, kycnDocumentStatusInfo.getDate());
			ps.setDate(5, kycnDocumentStatusInfo.getRefershDate());
			ps.setString(6, kycnDocumentStatusInfo.getNotes());
			ps.setString(7, hashValueForKycnDocumentStatusInfo);
			ps.setBoolean(8, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnPersonalId
	 * @param kycnDocumentStatusInfo
	 * @param user
	 * @param hashValueForKycnDocumentStatusInfoUpdate
	 * @throws SQLException
	 */

	public void insertKycnDocumnetUpdateKycnStatusInfo(Long kycnPersonalId,
			KycnDocumentStatusInfo kycnDocumentStatusInfo, User user, String hashValueForKycnDocumentStatusInfoUpdate)
			throws SQLException {
		String sql = "INSERT INTO kycn_document_status_update(kycn_personal_info_id,document_type"
				+ ",document_status,date,refersh_date,notes,maker,checker,hash,is_active) VALUES(?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnDocumentStatusInfo.getDocumentType());
			ps.setString(3, kycnDocumentStatusInfo.getDocumentStatus());
			ps.setDate(4, kycnDocumentStatusInfo.getDate());
			ps.setDate(5, kycnDocumentStatusInfo.getRefershDate());
			ps.setString(6, kycnDocumentStatusInfo.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, hashValueForKycnDocumentStatusInfoUpdate);
			ps.setBoolean(10, true);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnPersonalInfo
	 * @param user
	 * @param hashValueForKycnPersonInfo
	 * @throws SQLException
	 */

	public void updateKycnPersonalInfo(KycnPersonalInfo kycnPersonalInfo, User user, String hashValueForKycnPersonInfo)
			throws SQLException {
		String sql = "Update kycn_personal_info set "
				+ "salutation=?,first_name=?,middle_name=?,last_name=?,ls_title=?,lsf_name=?,"
				+ "lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,previous_name=?,gender=?,"
				+ "date_of_birth=?,age=?,minor=?,marital_status=?,notes=?,"
				+ " customer_type=?,customer_group=?,customer_constitution=?,customer_community=?,customer_caste=?"
				+ ",customer_employee_id=?,customer_open_date=?,"
				+ "customer_status_code=?,non_resident_external=?,card_holder=?,customer_maker=?,screening_id=?,cust_id=?,verified_record=?,hash=?,is_updated_on_time=?,is_updated=?,goaml_gender_type=?,"
				+ "incomplete=?,is_active=?,modified_date=? where id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, kycnPersonalInfo.getSalutation());
			ps.setString(2, kycnPersonalInfo.getFirstName());
			ps.setString(3, kycnPersonalInfo.getMiddleName());
			ps.setString(4, kycnPersonalInfo.getLastName());
			ps.setString(5, kycnPersonalInfo.getLsTitle());
			ps.setString(6, kycnPersonalInfo.getLsfName());
			ps.setString(7, kycnPersonalInfo.getLsmName());
			ps.setString(8, kycnPersonalInfo.getLslName());
			ps.setString(9, kycnPersonalInfo.getSecondName());
			ps.setString(10, kycnPersonalInfo.getCalledByName());
			ps.setString(11, kycnPersonalInfo.getPreviousName());
			ps.setString(12, kycnPersonalInfo.getGender());
			ps.setDate(13, kycnPersonalInfo.getDateOfBirth());
			ps.setString(14, kycnPersonalInfo.getAge());
			ps.setBoolean(15, kycnPersonalInfo.isMinor());
			ps.setString(16, kycnPersonalInfo.getMaritalStatus());
			ps.setString(17, kycnPersonalInfo.getNotes());
			ps.setString(18, kycnPersonalInfo.getCustomerType());
			ps.setString(19, kycnPersonalInfo.getCustomerGroup());
			ps.setString(20, kycnPersonalInfo.getCustomerConstitution());
			ps.setString(21, kycnPersonalInfo.getCustomerCommunity());
			ps.setString(22, kycnPersonalInfo.getCustomerCaste());
			ps.setString(23, kycnPersonalInfo.getCustomerEmployeeId());
			ps.setDate(24, kycnPersonalInfo.getCustomerOpenDate());
			ps.setString(25, kycnPersonalInfo.getCustomerStatusCode());
			ps.setBoolean(26, kycnPersonalInfo.isNonResidentExternal());
			ps.setBoolean(27, kycnPersonalInfo.isCardHolder());
			ps.setString(28, user.getUserName());
			ps.setLong(29, kycnPersonalInfo.getScreeningId());
			ps.setString(30, kycnPersonalInfo.getCustomerId());
			ps.setBoolean(31, kycnPersonalInfo.isVerified());
			ps.setString(32, hashValueForKycnPersonInfo);
			ps.setBoolean(33, true);
			ps.setBoolean(34, true);
			ps.setString(35, kycnPersonalInfo.getGoAMLGender());
			ps.setBoolean(36, kycnPersonalInfo.isIncomplete());
			ps.setBoolean(37, true);
			ps.setDate(38, new java.sql.Date(System.currentTimeMillis()));
			ps.setLong(39, kycnPersonalInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnIdentificationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnIdentificationInfo
	 * @param user
	 * @throws SQLException
	 */

	public void updateKycnIndentificationInfo(KycnIdentificationInfo kycnIdentificationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnIdentificationInfo, User user) throws SQLException {
		String sql = "UPDATE kycn_identification_info set kycn_personal_info_id=?,primary_identification_document_type=?"
				+ ",primary_identification_document_no=?,country_of_issue=?,passport_no=?,passport_country=?,"
				+ "passport_issuing_authority=?,passport_place_of_issue=?,passport_issue_date=?,expiry_date=?,"
				+ "visa_no=?,visa_expiry_date=?,nepal_entry_date=?,notes=?,hash=?,goaml_type=?,goaml_country_of_issue=?,is_active=? where kycn_personal_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnIdentificationInfo.getPrimaryIdentificationDocumentType());
			ps.setString(3, kycnIdentificationInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(4, kycnIdentificationInfo.getCountryOfIssue());
			ps.setString(5, kycnIdentificationInfo.getPassportNo());
			ps.setString(6, kycnIdentificationInfo.getPassportCountry());
			ps.setString(7, kycnIdentificationInfo.getPassportIssuingAuthority());
			ps.setString(8, kycnIdentificationInfo.getPassportPlaceOfIssue());
			ps.setDate(9, kycnIdentificationInfo.getPassportIssueDate());
			ps.setDate(10, kycnIdentificationInfo.getExpiryDate());
			ps.setString(11, kycnIdentificationInfo.getVisaNo());
			ps.setDate(12, kycnIdentificationInfo.getVisaExpiryDate());
			ps.setDate(13, kycnIdentificationInfo.getNepalEntryDate());
			ps.setString(14, kycnIdentificationInfo.getNotes());
			ps.setString(15, hashValueForKycnIdentificationInfo);
			ps.setString(16, kycnIdentificationInfo.getGoAMLPrimaryIdentificationDocumentType());
			ps.setString(17, kycnIdentificationInfo.getGoAMLCountryOfIssue());
			ps.setBoolean(18, true);
			ps.setLong(19, kycnPersonalInfoId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAddressInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAddressInfo
	 * @throws SQLException
	 */
	public void updateKycnAddressInfo(KycnAddressInfo kycnAddressInfo, Long kycnPersonalInfoId,
			String hashValueForKycnAddressInfo,User user) throws SQLException {
		String sql = "Update kycn_address_info "
				+ "set kycn_personal_info_id=?,address_type=?,country=?,zone=?,district=?,mn_vdc=?,pinzip=?,ward_number=?,tole_area=?,"
				+ "street=?,house_no=?,unit_number=?,nearest_landmark=?,latitude=?,longitude=?,phone_no_country_code=?,phone_no_area_code=?,"
				+ "phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,"
				+ "pager_no=?,email_id=?,notes=?,hash=?,province=?,address=?,city=?,goaml_address_type=?,goaml_country=?,"
				+ "is_active=?,address_removed_by=? where id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAddressInfo.getType());
			ps.setString(3, kycnAddressInfo.getCountry());
			ps.setString(4, kycnAddressInfo.getZone());
			ps.setString(5, kycnAddressInfo.getDistrict());
			ps.setString(6, kycnAddressInfo.getMnVdc());
			ps.setString(7, kycnAddressInfo.getPinZip());
			ps.setString(8, kycnAddressInfo.getWardNumber());
			ps.setString(9, kycnAddressInfo.getToleArea());
			ps.setString(10, kycnAddressInfo.getStreet());
			ps.setString(11, kycnAddressInfo.getHouseNumber());
			ps.setString(12, kycnAddressInfo.getUnitNumber());
			ps.setString(13, kycnAddressInfo.getNearestLandMark());
			ps.setString(14, kycnAddressInfo.getLatitude());
			ps.setString(15, kycnAddressInfo.getLongitude());
			ps.setString(16, kycnAddressInfo.getPhoneNoCountryCode());
			ps.setString(17, kycnAddressInfo.getPhoneNoAreaCode());
			ps.setString(18, kycnAddressInfo.getPhoneNo());
			ps.setString(19, kycnAddressInfo.getTelexNoCountryCode());
			ps.setString(20, kycnAddressInfo.getTelexNoAreaCode());
			ps.setString(21, kycnAddressInfo.getTelexNo());
			ps.setString(22, kycnAddressInfo.getPagerNoCountryCode());
			ps.setString(23, kycnAddressInfo.getPagerNoAreaCode());
			ps.setString(24, kycnAddressInfo.getPagerNo());
			ps.setString(25, kycnAddressInfo.getEmailId());
			ps.setString(26, kycnAddressInfo.getNotes());
			ps.setString(27, hashValueForKycnAddressInfo);
			ps.setString(28, kycnAddressInfo.getProvince());
			ps.setString(29, kycnAddressInfo.getAddress());
			ps.setString(30, kycnAddressInfo.getCity());
			ps.setString(31, kycnAddressInfo.getGoAMLAddressType());
			ps.setString(32, kycnAddressInfo.getGoAMLCountryType());
			ps.setBoolean(33, Boolean.parseBoolean(kycnAddressInfo.getRemovedAddressStatus()));
			ps.setString(34, user.getUserName());
			ps.setLong(35, kycnAddressInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnRelationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnRelationInfo
	 * @throws SQLException
	 */
	public void updateKycnRelationInfo(KycnRelationInfo kycnRelationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnRelationInfo) throws SQLException {
		String sql = "UPDATE kycn_relation_info "
				+ " SET kycn_personal_info_id=?,relation_type=?,cust_id=?,kycn_id=?,first_name=?,middle_name=?,"
				+ "last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,"
				+ "primary_identification_document_type=?,primary_identification_document_no=?,"
				+ "relation_country=?,issuing_authority=?,place_of_issue=?,issue_date=?,expiry_date=?,notes=?,salutation=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnRelationInfo.getRelationType());
			ps.setString(3, kycnRelationInfo.getCustId());
			ps.setString(4, kycnRelationInfo.getKycnId());
			ps.setString(5, kycnRelationInfo.getFirstName());
			ps.setString(6, kycnRelationInfo.getMiddleName());
			ps.setString(7, kycnRelationInfo.getLastName());
			ps.setString(8, kycnRelationInfo.getLsfName());
			ps.setString(9, kycnRelationInfo.getLsmName());
			ps.setString(10, kycnRelationInfo.getLslName());
			ps.setString(11, kycnRelationInfo.getSecondName());
			ps.setString(12, kycnRelationInfo.getCalledByName());
			ps.setString(13, kycnRelationInfo.getPrimaryIdentificationDocumentType());
			ps.setString(14, kycnRelationInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(15, kycnRelationInfo.getRelationCountry());
			ps.setString(16, kycnRelationInfo.getIssuingAuthority());
			ps.setString(17, kycnRelationInfo.getPlaceOfIssue());
			ps.setDate(18, kycnRelationInfo.getIssueDate());
			ps.setDate(19, kycnRelationInfo.getExpiryDate());
			ps.setString(20, kycnRelationInfo.getNotes());
			ps.setString(21, kycnRelationInfo.getSalutation());
			ps.setString(22, hashValueForKycnRelationInfo);
			ps.setBoolean(23, true);
			ps.setLong(24, kycnRelationInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnEducationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnEducationInfo
	 * @throws SQLException
	 */

	public void updateKycnEducationInfo(KycnEducationInfo kycnEducationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnEducationInfo) throws SQLException {
		String sql = "UPDATE kycn_education_info set kycn_personal_info_id=?,qualification=?,name_of_institute=?,field_of_study=?,year_of_graduation=?,notes=?,hash=?,is_active=?"
				+ "	WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnEducationInfo.getQualification());
			ps.setString(3, kycnEducationInfo.getNameOfInstitute());
			ps.setString(4, kycnEducationInfo.getFieldOfStudy());
			ps.setString(5, kycnEducationInfo.getYearOfGraduation());
			ps.setString(6, kycnEducationInfo.getNotes());
			ps.setString(7, hashValueForKycnEducationInfo);
			ps.setBoolean(8, true);
			ps.setLong(9, kycnEducationInfo.getId());
			
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnInvolvementInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnInvolvementInfo
	 * @throws SQLException
	 */
	public void updateKycnInvolvementInfo(KycnInvolvementInfo kycnInvolvementInfo, Long kycnPersonalInfoId,
			String hashValueForKycnInvolvementInfo) throws SQLException {
		String sql = "UPDATE kycn_involvement_info "
				+ "SET kycn_personal_info_id=?,kycl_id=?,organization_nature=?,country=?,zone=?,district=?,mn_vdc=?,pinzip=?,"
				+ "involvement_ward_number=?,tole_area=?,street=?,house_no=?,unit_number=?,nearest_landmark=?,latitude=?,longitude=?,"
				+ "phone_no_country_code=?,phone_no_area_code=?,phone_no=?,fax_no_country_code=?,fax_no_area_code=?,fax_no=?,email_id=?,"
				+ "website=?,panvat=?,poboxno=?,nature=?,designation=?,start_date=?,end_date=?,involvement_notes=?,nature_of_involvement=?,occupation_type=?,hash=?,province=?,"
				+ "organization_name=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);

			ps.setLong(1, kycnPersonalInfoId);
			ps.setLong(2, kycnInvolvementInfo.getKyclId());
			ps.setString(3, kycnInvolvementInfo.getOrganizationNature());
			ps.setString(4, kycnInvolvementInfo.getCountry());
			ps.setString(5, kycnInvolvementInfo.getZone());
			ps.setString(6, kycnInvolvementInfo.getDistrict());
			ps.setString(7, kycnInvolvementInfo.getMnVdc());
			ps.setString(8, kycnInvolvementInfo.getPinZip());
			ps.setString(9, kycnInvolvementInfo.getInvolvementWardNumber());
			ps.setString(10, kycnInvolvementInfo.getToleArea());
			ps.setString(11, kycnInvolvementInfo.getStreet());
			ps.setString(12, kycnInvolvementInfo.getHouseNo());
			ps.setString(13, kycnInvolvementInfo.getUnitNumber());
			ps.setString(14, kycnInvolvementInfo.getNearestLandMark());
			ps.setString(15, kycnInvolvementInfo.getLatitude());
			ps.setString(16, kycnInvolvementInfo.getLongitude());
			ps.setString(17, kycnInvolvementInfo.getPhoneNoCountryCode());
			ps.setString(18, kycnInvolvementInfo.getPhoneNoAreaCode());
			ps.setString(19, kycnInvolvementInfo.getPhoneNo());
			ps.setString(20, kycnInvolvementInfo.getFaxNoCountryCode());
			ps.setString(21, kycnInvolvementInfo.getFaxNoAreaCode());
			ps.setString(22, kycnInvolvementInfo.getFaxNo());
			ps.setString(23, kycnInvolvementInfo.getEmailId());
			ps.setString(24, kycnInvolvementInfo.getWebsite());
			ps.setString(25, kycnInvolvementInfo.getPanVat());
			ps.setString(26, kycnInvolvementInfo.getPoBoxNo());
			ps.setString(27, kycnInvolvementInfo.getNature());
			ps.setString(28, kycnInvolvementInfo.getDesignation());
			ps.setDate(29, kycnInvolvementInfo.getStartDate());
			ps.setDate(30, kycnInvolvementInfo.getEndDate());
			ps.setString(31, kycnInvolvementInfo.getNotes());
			ps.setString(32, kycnInvolvementInfo.getInvolvementNature());
			ps.setString(33, kycnInvolvementInfo.getOccupationType());
			ps.setString(34, hashValueForKycnInvolvementInfo);
			ps.setString(35, kycnInvolvementInfo.getProvince());
			ps.setString(36, kycnInvolvementInfo.getOrganizationName());
			ps.setBoolean(37, true);
			ps.setLong(38, kycnInvolvementInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAccountsInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAccountsInfo
	 * @throws SQLException
	 */
	public void updateKycnAccountInfo(KycnAccountsInfo kycnAccountsInfo,User user, Long kycnPersonalInfoId,
			String hashValueForKycnAccountsInfo) throws SQLException {
		String sql = "UPDATE kycn_accounts_info set "
				+ "kycn_personal_info_id=?,account_id=?,for_account_id=?,currency_of_account=?,account_no=?,account_name=?,"
				+ "account_short_name=?,account_ownership=?,scheme_type=?,gl_sub_head_code=?,product_group=?,"
				+ "last_transaction_date=?, account_open_date=?,estimated_yearly_transactions=?, "
				+ "estimated_monthly_transactions=?,estimated_yearly_turnover=?,estimated_monthly_turnover=?,"
				+ "regular_source_of_income=?, source_of_fund=?,notes=?,scheme_code=?,hash=?,scheme_description=?,nature_of_account=?,is_blocked=?,"
				+ "account_status_type=?, account_type=?, goaml_account_status_type=?,goaml_personal_account_type=?,"
				+ "branch_sol_id=?,is_active=?,account_removed_by=? where id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnAccountsInfo.getAccountId());
			ps.setString(3, kycnAccountsInfo.getForAccountId());
			ps.setString(4, kycnAccountsInfo.getCurrencyOfAccount());
			ps.setString(5, kycnAccountsInfo.getAccountNo());
			ps.setString(6, kycnAccountsInfo.getAccountName());
			ps.setString(7, kycnAccountsInfo.getAccountShortName());
			ps.setString(8, kycnAccountsInfo.getAccountOwnership());
			ps.setString(9, kycnAccountsInfo.getSchemeType());
			ps.setString(10, kycnAccountsInfo.getGlSubHeadCode());
			ps.setString(11, kycnAccountsInfo.getProductGroup());
			ps.setDate(12, kycnAccountsInfo.getLastTransactionDate());
			ps.setDate(13, kycnAccountsInfo.getAccountOpenDate());
			/*ps.setString(14, kycnAccountsInfo.getEstimatedYearlyTransaction());
			ps.setString(15, kycnAccountsInfo.getEstimatedMonthlyTransaction());
			ps.setString(16, kycnAccountsInfo.getEstimatedYearlyTurnOver());
			ps.setString(17, kycnAccountsInfo.getEstimatedMonthlyTurnOver());*/
			
			if (kycnAccountsInfo.getEstimatedYearlyTransaction().equals("")) {
				ps.setNull(14, java.sql.Types.INTEGER);
			} else {
				ps.setInt(14, Integer.parseInt(kycnAccountsInfo.getEstimatedYearlyTransaction()));
			}
			if (kycnAccountsInfo.getEstimatedMonthlyTransaction().equals("")) {
				ps.setNull(15, java.sql.Types.INTEGER);
			} else {
				ps.setInt(15, Integer.parseInt(kycnAccountsInfo.getEstimatedMonthlyTransaction()));
			}
			if (kycnAccountsInfo.getEstimatedYearlyTurnOver().equals("")) {
				ps.setNull(16, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(16, new BigDecimal(kycnAccountsInfo.getEstimatedYearlyTurnOver()));
			}
			if (kycnAccountsInfo.getEstimatedMonthlyTurnOver().equals("")) {
				ps.setNull(17, java.sql.Types.NUMERIC);
			} else {
				ps.setBigDecimal(17,new BigDecimal(kycnAccountsInfo.getEstimatedMonthlyTurnOver()));
			}
			
			ps.setString(18, kycnAccountsInfo.getRegularSourceOfIncome());
			ps.setString(19, kycnAccountsInfo.getSourceOfFund());
			ps.setString(20, kycnAccountsInfo.getNotes());
			ps.setString(21, kycnAccountsInfo.getSchemeCode());
			ps.setString(22, hashValueForKycnAccountsInfo);
			ps.setString(23, kycnAccountsInfo.getSchemeDescription());
			ps.setString(24, kycnAccountsInfo.getNatureOfAccount());
			ps.setString(25, kycnAccountsInfo.getIsBlocked());
			ps.setString(26,kycnAccountsInfo.getAccountStatusType());
			ps.setString(27,kycnAccountsInfo.getAccountType());
			ps.setString(28,kycnAccountsInfo.getGoAMLAccountStatusType());
			ps.setString(29,kycnAccountsInfo.getGoAMLaccountType());
			ps.setString(30,kycnAccountsInfo.getBranchSolId());
			ps.setBoolean(31, Boolean.parseBoolean(kycnAccountsInfo.getRemovedAccountStatus()));
			if(kycnAccountsInfo.getRemovedAccountStatus().equals("false")){
				ps.setString(32, user.getUserName());
			}else{
				ps.setString(32, null);
			}
			ps.setLong(33, kycnAccountsInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnObservationInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnObservationInfo
	 * @throws SQLException
	 */
	public void updateKycnObservationInfo(KycnObservationInfo kycnObservationInfo, Long kycnPersonalInfoId,
			String hashValueForKycnObservationInfo) throws SQLException {
		String sql = "UPDATE kycn_internal_observation SET kycn_personal_info_id=?,observation_type=?"
				+ ",internal_observation_physical=?,internal_observation_financial=?,internal_observation_behavioral=?,internal_observation_connected_person=?,"
				+ "connected_person_id=?,internal_observation_intended_objective_of_business_relation=?,observation_media_source=?,"
				+ "observation_date=?,observation_time=?,notes=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnObservationInfo.getObservationType());
			ps.setString(3, kycnObservationInfo.getInternalObservationPhysical());
			ps.setString(4, kycnObservationInfo.getInternalObservationFinancial());
			ps.setString(5, kycnObservationInfo.getInternalObservationBehavioral());
			ps.setString(6, kycnObservationInfo.getInternalObservationConnectedPerson());
			ps.setString(7, kycnObservationInfo.getConnectedPersonId());
			ps.setString(8, kycnObservationInfo.getInternalObservationIntendedObjectiveOfBusinessRelation());
			ps.setString(9, kycnObservationInfo.getObservationMediaSource());
			ps.setDate(10, kycnObservationInfo.getObservationDate());
			ps.setString(11, kycnObservationInfo.getObservationTime());
			ps.setString(12, kycnObservationInfo.getNotes());
			ps.setString(13, hashValueForKycnObservationInfo);
			ps.setBoolean(14, true);	
			ps.setLong(15, kycnObservationInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnAmlInformation
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnAmlInformationInfo
	 * @throws SQLException
	 */
	public void updateKycnAmlInfo(KycnAmlInformationInfo kycnAmlInformation, Long kycnPersonalInfoId,
			String hashValueForKycnAmlInformationInfo) throws SQLException {
		String sql = "UPDATE kycn_aml_information SET kycn_personal_info_id=?,fatca_country=?"
				+ ",us_non_us=?,risk_categorization=?,recommendations=?,aml_notes=?,aml_check=?,hash=?,is_active=? WHERE  id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setBoolean(2, kycnAmlInformation.isFatcaCountry());
			ps.setBoolean(3, kycnAmlInformation.isUsNonUs());
			ps.setString(4, kycnAmlInformation.getRiskCategorization());
			ps.setString(5, kycnAmlInformation.getRecommendations());
			ps.setString(6, kycnAmlInformation.getAmlNotes());
			ps.setBoolean(7, kycnAmlInformation.isAmlCheck());
			ps.setString(8, hashValueForKycnAmlInformationInfo);
			ps.setBoolean(9, true);
			ps.setLong(10, kycnAmlInformation.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnDocumentStatusInfo
	 * @param kycnPersonalInfoId
	 * @param hashValueForKycnDocumentStatusInfo
	 * @throws SQLException
	 */

	public void updateDocumentStatusInfo(KycnDocumentStatusInfo kycnDocumentStatusInfo, Long kycnPersonalInfoId,
			String hashValueForKycnDocumentStatusInfo) throws SQLException {
		String sql = "UPDATE kycn_document_status SET kycn_personal_info_id=?,document_type=?"
				+ ",document_status=?,date=?,refersh_date=?,notes=?,hash=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfoId);
			ps.setString(2, kycnDocumentStatusInfo.getDocumentType());
			ps.setString(3, kycnDocumentStatusInfo.getDocumentStatus());
			ps.setDate(4, kycnDocumentStatusInfo.getDate());
			ps.setDate(5, kycnDocumentStatusInfo.getRefershDate());
			ps.setString(6, kycnDocumentStatusInfo.getNotes());
			ps.setString(7, hashValueForKycnDocumentStatusInfo);
			ps.setBoolean(8, true);
			ps.setLong(9, kycnDocumentStatusInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param accountsInfoId
	 * @param cardSubscribed
	 * @throws SQLException
	 */
	public void insertCardSubscribedInfo(Long accountsInfoId, String cardSubscribed) throws SQLException {
		String sql = "INSERT INTO kycn_cards_subscribed_account(kycn_account_info_id,cards_subscribed,notes) VALUES(?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, cardSubscribed);
			ps.setString(3, "");
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param accountsInfoId
	 * @param cardSubscribed
	 * @throws SQLException
	 */
	public void updateCardSubscribedInfo(long accountsInfoId, String cardSubscribed) throws SQLException {
		String sql = "UPDATE kycn_cards_subscribed_account set kycn_account_info_id=?,cards_subscribed=?,notes=? WHERE kycn_account_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, cardSubscribed);
			ps.setString(3, "");
			ps.setLong(4, accountsInfoId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param accountsInfoId
	 * @param cardSubscribed
	 * @param user
	 * @throws SQLException
	 */
	public void insertCardSubscribedInfoUpdatetable(Long accountsInfoId, String cardSubscribed, User user)
			throws SQLException {
		String sql = "INSERT INTO kycn_cards_subscribed_accounts_update(kycn_account_info_id,cards_subscribed,notes,maker,checker)"
				+ "VALUES(?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, cardSubscribed);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param accountsInfoId
	 * @param serviceSubscribed
	 * @throws SQLException
	 */
	public void insertServiceSubscribedInfo(Long accountsInfoId, String serviceSubscribed) throws SQLException {
		String sql = "INSERT INTO kycn_services_subscribed_accounts(kycn_accounts_info_id,services_subscribed,notes) VALUES(?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, serviceSubscribed);
			ps.setString(3, "");
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param accountsInfoId
	 * @param serviceSubscribed
	 * @param user
	 * @throws SQLException
	 */
	public void insertServiceSubscribedInfoUpdateTable(Long accountsInfoId, String serviceSubscribed, User user)
			throws SQLException {
		String sql = "INSERT INTO kycn_services_subscribed_accounts_update(kycn_accounts_info_id,services_subscribed,notes,maker,checker)"
				+ "VALUES(?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, serviceSubscribed);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param accountsInfoId
	 * @param serviceSubscribed
	 * @throws SQLException
	 */
	public void updateServiceSubscribedInfo(long accountsInfoId, String serviceSubscribed) throws SQLException {
		String sql = "UPDATE kycn_services_subscribed_accounts set kycn_accounts_info_id=?,services_subscribed=?,notes=? WHERE kycn_account_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, serviceSubscribed);
			ps.setString(3, "");
			ps.setLong(4, accountsInfoId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelatedEntityInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnRelatedEntityInfo
	 * @throws SQLException
	 */
	public void updateKycnRelatedEntityInfo(KycnRelatedEntityInfo kycnRelatedEntityInfo, long kycnPersonalId,
			String hashValueForKycnRelatedEntityInfo) throws SQLException {
		String sql = "UPDATE kycn_related_entity_info"
				+ " SET kycn_personal_info_id=?,person_type=?,cust_id=?,kycl_id=?,salutation=?,name=?,ls_name=?,called_by_name=?,"
				+ "primary_identification_document_type=?,registration_no=?,country=?,zone=?,district=?,mn_vdc=?,pinzip=?,ward_number=?,"
				+ "tole_area=?,street=?,house_no=?,nearest_landmark=?,latitude=?,longitude=?,phone_no_country_code=?,"
				+ "phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,email_id=?,notes=?,unit_number=?,hash=?,province=?,is_active=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedEntityInfo.getRelatedEntityType());
			ps.setString(3, kycnRelatedEntityInfo.getCustId());
			ps.setLong(4, kycnRelatedEntityInfo.getKycnId());
			ps.setString(5, kycnRelatedEntityInfo.getSalutatiton());
			ps.setString(6, kycnRelatedEntityInfo.getName());
			ps.setString(7, kycnRelatedEntityInfo.getLsName());
			ps.setString(8, kycnRelatedEntityInfo.getCalledByName());
			ps.setString(9, kycnRelatedEntityInfo.getPrimaryIdentificationDocumentType());
			ps.setString(10, kycnRelatedEntityInfo.getRegistrationNo());
			ps.setString(11, kycnRelatedEntityInfo.getRelatedEntityCountry());
			ps.setString(12, kycnRelatedEntityInfo.getZone());
			ps.setString(13, kycnRelatedEntityInfo.getDistrict());
			ps.setString(14, kycnRelatedEntityInfo.getMnVdc());
			ps.setString(15, kycnRelatedEntityInfo.getPinzip());
			ps.setString(16, kycnRelatedEntityInfo.getWardNumber());
			ps.setString(17, kycnRelatedEntityInfo.getToleArea());
			ps.setString(18, kycnRelatedEntityInfo.getStreet());
			ps.setString(19, kycnRelatedEntityInfo.getHouseNo());
			ps.setString(20, kycnRelatedEntityInfo.getNearestLandmark());
			ps.setString(21, kycnRelatedEntityInfo.getLatitude());
			ps.setString(22, kycnRelatedEntityInfo.getLongitude());
			ps.setString(23, kycnRelatedEntityInfo.getPhoneNoCountryCode());
			ps.setString(24, kycnRelatedEntityInfo.getPhoneNoAreaCode());
			ps.setString(25, kycnRelatedEntityInfo.getPhoneNo());
			ps.setString(26, kycnRelatedEntityInfo.getTelexNoCountryCode());
			ps.setString(27, kycnRelatedEntityInfo.getTelexNoAreaCode());
			ps.setString(28, kycnRelatedEntityInfo.getTelexNo());
			ps.setString(29, kycnRelatedEntityInfo.getEmailId());
			ps.setString(30, kycnRelatedEntityInfo.getNotes());
			ps.setString(31, kycnRelatedEntityInfo.getUnitNumber());
			ps.setString(32, hashValueForKycnRelatedEntityInfo);
			ps.setString(33, kycnRelatedEntityInfo.getProvince());
			ps.setBoolean(34, true);
			ps.setLong(35, kycnRelatedEntityInfo.getId());

			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelatedEntityInfo
	 * @param kycnPersonalId
	 * @param user
	 * @param hashValueForKycnRelatedEntityInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnRelatedEntityInfoUpdateTable(KycnRelatedEntityInfo kycnRelatedEntityInfo, long kycnPersonalId,
			User user, String hashValueForKycnRelatedEntityInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_related_entity_info_update"
				+ "(kycn_personal_info_id,person_type,cust_id,kycl_id,salutation,name, ls_name,called_by_name,"
				+ "primary_identification_document_type,registration_no,country,zone,district,mn_vdc,pinzip,ward_number,"
				+ "tole_area,street,house_no,nearest_landmark,latitude,longitude,phone_no_country_code,"
				+ "phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,email_id,"
				+ "notes,unit_number,maker,checker,hash,province,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedEntityInfo.getRelatedEntityType());
			ps.setString(3, kycnRelatedEntityInfo.getCustId());
			ps.setLong(4, kycnRelatedEntityInfo.getKycnId());
			ps.setString(5, kycnRelatedEntityInfo.getSalutatiton());
			ps.setString(6, kycnRelatedEntityInfo.getName());
			ps.setString(7, kycnRelatedEntityInfo.getLsName());
			ps.setString(8, kycnRelatedEntityInfo.getCalledByName());
			ps.setString(9, kycnRelatedEntityInfo.getPrimaryIdentificationDocumentType());
			ps.setString(10, kycnRelatedEntityInfo.getRegistrationNo());
			ps.setString(11, kycnRelatedEntityInfo.getRelatedEntityCountry());
			ps.setString(12, kycnRelatedEntityInfo.getZone());
			ps.setString(13, kycnRelatedEntityInfo.getDistrict());
			ps.setString(14, kycnRelatedEntityInfo.getMnVdc());
			ps.setString(15, kycnRelatedEntityInfo.getPinzip());
			ps.setString(16, kycnRelatedEntityInfo.getWardNumber());
			ps.setString(17, kycnRelatedEntityInfo.getToleArea());
			ps.setString(18, kycnRelatedEntityInfo.getStreet());
			ps.setString(19, kycnRelatedEntityInfo.getHouseNo());
			ps.setString(20, kycnRelatedEntityInfo.getNearestLandmark());
			ps.setString(21, kycnRelatedEntityInfo.getLatitude());
			ps.setString(22, kycnRelatedEntityInfo.getLongitude());
			ps.setString(23, kycnRelatedEntityInfo.getPhoneNoCountryCode());
			ps.setString(24, kycnRelatedEntityInfo.getPhoneNoAreaCode());
			ps.setString(25, kycnRelatedEntityInfo.getPhoneNo());
			ps.setString(26, kycnRelatedEntityInfo.getTelexNoCountryCode());
			ps.setString(27, kycnRelatedEntityInfo.getTelexNoAreaCode());
			ps.setString(28, kycnRelatedEntityInfo.getTelexNo());
			ps.setString(29, kycnRelatedEntityInfo.getEmailId());
			ps.setString(30, kycnRelatedEntityInfo.getNotes());
			ps.setString(31, kycnRelatedEntityInfo.getUnitNumber());
			ps.setString(32, user.getUserName());
			ps.setString(33, user.getUserName());
			ps.setString(34, hashValueForKycnRelatedEntityInfoUpdate);
			ps.setString(35, kycnRelatedEntityInfo.getProvince());
			ps.setBoolean(36, true);

			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param kycnRelatedEntityInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnRelatedEntityInfo
	 * @throws SQLException
	 */
	public void insertKycnRelatedEntityInfo(KycnRelatedEntityInfo kycnRelatedEntityInfo, long kycnPersonalId,
			String hashValueForKycnRelatedEntityInfo) throws SQLException {
		String sql = "INSERT INTO kycn_related_entity_info"
				+ "(kycn_personal_info_id,person_type,cust_id,kycl_id,salutation,name,ls_name,called_by_name,"
				+ "primary_identification_document_type,registration_no,country,zone,district,mn_vdc,pinzip,ward_number,"
				+ "tole_area,street,house_no,nearest_landmark,latitude,longitude,phone_no_country_code,"
				+ "phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,email_id,notes,unit_number,hash,province,is_active)"
				+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedEntityInfo.getRelatedEntityType());
			ps.setString(3, kycnRelatedEntityInfo.getCustId());
			if (kycnRelatedEntityInfo.getKycnId() == 0) {
				ps.setNull(4, java.sql.Types.BIGINT);
			} else {
				ps.setLong(4, kycnRelatedEntityInfo.getKycnId());
			}
			ps.setString(5, kycnRelatedEntityInfo.getSalutatiton());
			ps.setString(6, kycnRelatedEntityInfo.getName());
			ps.setString(7, kycnRelatedEntityInfo.getLsName());
			ps.setString(8, kycnRelatedEntityInfo.getCalledByName());
			ps.setString(9, kycnRelatedEntityInfo.getPrimaryIdentificationDocumentType());
			ps.setString(10, kycnRelatedEntityInfo.getRegistrationNo());
			ps.setString(11, kycnRelatedEntityInfo.getRelatedEntityCountry());
			ps.setString(12, kycnRelatedEntityInfo.getZone());
			ps.setString(13, kycnRelatedEntityInfo.getDistrict());
			ps.setString(14, kycnRelatedEntityInfo.getMnVdc());
			ps.setString(15, kycnRelatedEntityInfo.getPinzip());
			ps.setString(16, kycnRelatedEntityInfo.getWardNumber());
			ps.setString(17, kycnRelatedEntityInfo.getToleArea());
			ps.setString(18, kycnRelatedEntityInfo.getStreet());
			ps.setString(19, kycnRelatedEntityInfo.getHouseNo());
			ps.setString(20, kycnRelatedEntityInfo.getNearestLandmark());
			ps.setString(21, kycnRelatedEntityInfo.getLatitude());
			ps.setString(22, kycnRelatedEntityInfo.getLongitude());
			ps.setString(23, kycnRelatedEntityInfo.getPhoneNoCountryCode());
			ps.setString(24, kycnRelatedEntityInfo.getPhoneNoAreaCode());
			ps.setString(25, kycnRelatedEntityInfo.getPhoneNo());
			ps.setString(26, kycnRelatedEntityInfo.getTelexNoCountryCode());
			ps.setString(27, kycnRelatedEntityInfo.getTelexNoAreaCode());
			ps.setString(28, kycnRelatedEntityInfo.getTelexNo());
			ps.setString(29, kycnRelatedEntityInfo.getEmailId());
			ps.setString(30, kycnRelatedEntityInfo.getNotes());
			ps.setString(31, kycnRelatedEntityInfo.getUnitNumber());
			ps.setString(32, hashValueForKycnRelatedEntityInfo);
			ps.setString(33, kycnRelatedEntityInfo.getProvince());
			ps.setBoolean(34, true);

			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelatedPersonInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnPersonInfo
	 * @throws SQLException
	 */
	public void updateKycnRelatedPersonInfo(KycnRelatedPersonInfo kycnRelatedPersonInfo, long kycnPersonalId,
			String hashValueForKycnPersonInfo) throws SQLException {
		String sql = "UPDATE kycn_related_person_info"
				+ " SET kycn_personal_info_id=?,person_type=?,cust_id=?,kycn_id=?,salutation=?,first_name=?,middle_name=?,last_name=?,"
				+ "lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,primary_identification_document_type=?,country=?,issuing_authority=?,"
				+ "place_of_issue=?,issue_date=?,expiry_date=?,notes=?,hash=?,goaml_person_type=?,is_active=?,goaml_account_person_role_type=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedPersonInfo.getPersonType());
			ps.setString(3, kycnRelatedPersonInfo.getCustId());
			ps.setLong(4, kycnRelatedPersonInfo.getKycnId());
			ps.setString(5, kycnRelatedPersonInfo.getSalutation());
			ps.setString(6, kycnRelatedPersonInfo.getFirstName());
			ps.setString(7, kycnRelatedPersonInfo.getMiddleName());
			ps.setString(8, kycnRelatedPersonInfo.getLastName());
			ps.setString(9, kycnRelatedPersonInfo.getLsfName());
			ps.setString(10, kycnRelatedPersonInfo.getLsmName());
			ps.setString(11, kycnRelatedPersonInfo.getLslName());
			ps.setString(12, kycnRelatedPersonInfo.getSecondName());
			ps.setString(13, kycnRelatedPersonInfo.getCalledByName());
			ps.setString(14, kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType());
			ps.setString(15, kycnRelatedPersonInfo.getCountry());
			ps.setString(16, kycnRelatedPersonInfo.getIssuingAuthority());
			ps.setString(17, kycnRelatedPersonInfo.getPlaceOfIssue());
			ps.setDate(18, kycnRelatedPersonInfo.getIssueDate());
			ps.setDate(19, kycnRelatedPersonInfo.getExpiryDate());
			ps.setString(20, kycnRelatedPersonInfo.getNotes());
			ps.setString(21, hashValueForKycnPersonInfo);
			ps.setString(22, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.setBoolean(23, Boolean.parseBoolean(kycnRelatedPersonInfo.getRemovedSignatoryStatus()));
			ps.setString(24, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.setLong(25, kycnRelatedPersonInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelatedPersonInfo
	 * @param kycnPersonalId
	 * @param user
	 * @param hashValueForKycnRelatedPersonInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnRelatedPersonInfoUpdateTable(KycnRelatedPersonInfo kycnRelatedPersonInfo, long kycnPersonalId,
			User user, String hashValueForKycnRelatedPersonInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_related_person_info_update"
				+ "(kycn_personal_info_id,person_type,cust_id,kycn_id,salutation,first_name,middle_name,last_name,"
				+ "lsf_name,lsm_name,lsl_name,second_name,called_by_name,primary_identification_document_type,country,issuing_authority,"
				+ "place_of_issue,issue_date,expiry_date,notes,maker,checker,hash,primary_identification_document_no,account_no,goaml_person_type,is_active,goaml_account_person_role_type)  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedPersonInfo.getPersonType());
			ps.setString(3, kycnRelatedPersonInfo.getCustId());
			ps.setLong(4, kycnRelatedPersonInfo.getKycnId());
			ps.setString(5, kycnRelatedPersonInfo.getSalutation());
			ps.setString(6, kycnRelatedPersonInfo.getFirstName());
			ps.setString(7, kycnRelatedPersonInfo.getMiddleName());
			ps.setString(8, kycnRelatedPersonInfo.getLastName());
			ps.setString(9, kycnRelatedPersonInfo.getLsfName());
			ps.setString(10, kycnRelatedPersonInfo.getLsmName());
			ps.setString(11, kycnRelatedPersonInfo.getLslName());
			ps.setString(12, kycnRelatedPersonInfo.getSecondName());
			ps.setString(13, kycnRelatedPersonInfo.getCalledByName());
			ps.setString(14, kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType());
			ps.setString(15, kycnRelatedPersonInfo.getCountry());
			ps.setString(16, kycnRelatedPersonInfo.getIssuingAuthority());
			ps.setString(17, kycnRelatedPersonInfo.getPlaceOfIssue());
			ps.setDate(18, kycnRelatedPersonInfo.getIssueDate());
			ps.setDate(19, kycnRelatedPersonInfo.getExpiryDate());
			ps.setString(20, kycnRelatedPersonInfo.getNotes());
			ps.setString(21, user.getUserName());
			ps.setString(22, user.getUserName());
			ps.setString(23, hashValueForKycnRelatedPersonInfoUpdate);
			ps.setString(24, kycnRelatedPersonInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(25, kycnRelatedPersonInfo.getAccountNo());
			ps.setString(26, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.setBoolean(27, Boolean.parseBoolean(kycnRelatedPersonInfo.getRemovedSignatoryStatus()));
			ps.setString(28, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnRelatedPersonInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnPersonInfo
	 * @throws SQLException
	 */
	public void insertKycnRelatedPersonInfo(KycnRelatedPersonInfo kycnRelatedPersonInfo, long kycnPersonalId,
			String hashValueForKycnPersonInfo) throws SQLException {

		String sql = "INSERT INTO kycn_related_person_info"
				+ "(kycn_personal_info_id,person_type,cust_id,kycn_id,salutation,first_name,middle_name,last_name,"
				+ "lsf_name,lsm_name,lsl_name,second_name,called_by_name,primary_identification_document_type,country,issuing_authority,"
				+ "place_of_issue,issue_date,expiry_date,notes,hash,primary_identification_document_no,account_no,goaml_person_type,is_active,goaml_account_person_role_type) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, kycnRelatedPersonInfo.getPersonType());
			ps.setString(3, kycnRelatedPersonInfo.getCustId());
			ps.setLong(4, kycnRelatedPersonInfo.getKycnId());
			ps.setString(5, kycnRelatedPersonInfo.getSalutation());
			ps.setString(6, kycnRelatedPersonInfo.getFirstName());
			ps.setString(7, kycnRelatedPersonInfo.getMiddleName());
			ps.setString(8, kycnRelatedPersonInfo.getLastName());
			ps.setString(9, kycnRelatedPersonInfo.getLsfName());
			ps.setString(10, kycnRelatedPersonInfo.getLsmName());
			ps.setString(11, kycnRelatedPersonInfo.getLslName());
			ps.setString(12, kycnRelatedPersonInfo.getSecondName());
			ps.setString(13, kycnRelatedPersonInfo.getCalledByName());
			ps.setString(14, kycnRelatedPersonInfo.getPrimaryIdentificationDocumentType());
			ps.setString(15, kycnRelatedPersonInfo.getCountry());
			ps.setString(16, kycnRelatedPersonInfo.getIssuingAuthority());
			ps.setString(17, kycnRelatedPersonInfo.getPlaceOfIssue());
			ps.setDate(18, kycnRelatedPersonInfo.getIssueDate());
			ps.setDate(19, kycnRelatedPersonInfo.getExpiryDate());
			ps.setString(20, kycnRelatedPersonInfo.getNotes());
			ps.setString(21, hashValueForKycnPersonInfo);
			ps.setString(22, kycnRelatedPersonInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(23, kycnRelatedPersonInfo.getAccountNo());
			ps.setString(24, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.setBoolean(25, Boolean.parseBoolean(kycnRelatedPersonInfo.getRemovedSignatoryStatus()));
			ps.setString(26, kycnRelatedPersonInfo.getGoAMLPersonType());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnFinancialInfo
	 * @throws SQLException
	 */
	public void updateKycnFinancialInfo(KycnFinancialInfo financialInfo, long kycnPersonalId,
			String hashValueForKycnFinancialInfo) throws SQLException {
		String sql = "UPDATE kycn_financial_info"
				+ " SET kycn_personal_info_id=?,pan_gir_of_customer=?,currency_of_customer=?,hash=? WHERE id =?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, hashValueForKycnFinancialInfo);
			ps.setLong(5, financialInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kycnPersonalId
	 * @param user
	 * @param hashValueForKycnFinancialInfoUpdate
	 * @throws SQLException
	 */
	public void insertKycnFinancialInfoUpdateTable(KycnFinancialInfo financialInfo, long kycnPersonalId, User user,
			String hashValueForKycnFinancialInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_financial_info_update"
				+ "(kycn_personal_info_id,pan_gir_of_customer,currency_of_customer,maker,checker,hash) VALUES(?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.setString(6, hashValueForKycnFinancialInfoUpdate);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param financialInfo
	 * @param kycnPersonalId
	 * @param hashValueForKycnFinancialInfo
	 * @throws SQLException
	 */
	public void insertKycnFinancialInfo(KycnFinancialInfo financialInfo, long kycnPersonalId,
			String hashValueForKycnFinancialInfo) throws SQLException {
		String sql = "INSERT INTO kycn_financial_info"
				+ "(kycn_personal_info_id,pan_gir_of_customer,currency_of_customer,hash) VALUES(?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, financialInfo.getCustomerPan());
			ps.setString(3, financialInfo.getCustomerCurrency());
			ps.setString(4, hashValueForKycnFinancialInfo);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param individual
	 * @param kycnPersonalId
	 * @param hashValueForKycnSubscribedIndividualInfo
	 * @throws SQLException
	 */

	public void insertCardSubscribedIndividual(String individual, long kycnPersonalId,
			String hashValueForKycnSubscribedIndividualInfo) throws SQLException {
		String sql = "INSERT INTO kycn_cards_subscribed_individual"
				+ "(kycn_personal_info_id,cards_subscribed,notes,hash) VALUES(?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, individual);
			ps.setString(3, "");
			ps.setString(4, hashValueForKycnSubscribedIndividualInfo);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param individual
	 * @param kycnPersonalId
	 * @param user
	 * @param hashValueForKycnSubscribedIndividualInfoUpdate
	 * @throws SQLException
	 */
	public void insertCardSubscribedIndividualUpdateTable(String individual, long kycnPersonalId, User user,
			String hashValueForKycnSubscribedIndividualInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_cards_subscribed_individual_update"
				+ "(kycn_personal_info_id,cards_subscribed,notes,maker,checker,hash) VALUES(?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, individual);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.setString(6, hashValueForKycnSubscribedIndividualInfoUpdate);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param service
	 * @param kycnPersonalId
	 * @param hashValueForKycnSubscribedServiceInfo
	 * @throws SQLException
	 */
	public void insertServiceSubscribedIndividual(String service, long kycnPersonalId,
			String hashValueForKycnSubscribedServiceInfo) throws SQLException {
		String sql = "INSERT INTO kycn_service_subscribed_individual"
				+ "(kycn_personal_info_id,services_subscribed,notes,hash) VALUES(?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, service);
			ps.setString(3, "");
			ps.setString(4, hashValueForKycnSubscribedServiceInfo);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param service
	 * @param kycnPersonalId
	 * @param user
	 * @param hashValueForKycnSubscribedServiceInfoUpdate
	 * @throws SQLException
	 */
	public void insertServiceSubscribedIndividualUpdateTable(String service, long kycnPersonalId, User user,
			String hashValueForKycnSubscribedServiceInfoUpdate) throws SQLException {
		String sql = "INSERT INTO kycn_service_subscribed_individual_update"
				+ "(kycn_personal_info_id,services_subscribed,notes,maker,checker,hash) VALUES(?,?,?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setString(2, service);
			ps.setString(3, "");
			ps.setString(4, user.getUserName());
			ps.setString(5, user.getUserName());
			ps.setString(6, hashValueForKycnSubscribedServiceInfoUpdate);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param individual
	 * @param kycnPersonalId
	 * @return
	 * @throws SQLException
	 */
	public boolean checkIfSubscribedIndividualExists(String individual, long kycnPersonalId) throws SQLException {
		String sql = "SELECT * from  kycn_cards_subscribed_individual WHERE cards_subscribed=? AND kycn_personal_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, individual);
			ps.setLong(2, kycnPersonalId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.getString(1);
				return true;
			}
		} finally {
			ps.close();
			con.close();
		}
		return false;

	}

	/**
	 * @param service
	 * @param kycnPersonalId
	 * @return
	 * @throws SQLException
	 */

	public boolean checkIfSubscribedServiceExists(String service, long kycnPersonalId) throws SQLException {
		String sql = "SELECT * from  kycn_service_subscribed_individual WHERE services_subscribed=? AND kycn_personal_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, service);
			ps.setLong(2, kycnPersonalId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs.getString(1);
				return true;
			}
		} finally {
			ps.close();
			con.close();
		}
		return false;

	}

	/**
	 * @param kycnPersonalId
	 * @param screeningId
	 * @throws SQLException
	 */
	public void updateCommentTableInCaseOfFoward(long kycnPersonalId, long screeningId) throws SQLException {
		String sql = "UPDATE screening_n_action_forward_to set kycn_id=? WHERE screening_request_n_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param kycnPersonalId
	 * @throws SQLException
	 */

	public void insertIntoKYCNApprovedTable(long kycnPersonalId) throws SQLException {
		String sql = "INSERT INTO  kycn_approved (kycn_personal_info_id) VALUES(?) ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, kycnPersonalId);

			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @param screeningId
	 * @param kycnSaved
	 * @throws SQLException
	 */

	public void updateScreeningNaturalWork(long screeningId, boolean kycnSaved) throws SQLException {
		String sql = "UPDATE screening_n_workflow set kycn_saved=? WHERE screening_n_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, kycnSaved);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

	public void updateScreeningNaturalWorkflow(long screeningId) throws SQLException {
		String sql = "UPDATE screening_n_workflow set kycn_refresh=? WHERE screening_n_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, screeningId);
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}
	
	
	public void updateScreeningKycId(long kycnPersonalId,long screeningId) throws SQLException {
		// TODO Auto-generated method stub
		
		String query="UPDATE screening_n_request SET kyc_id=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(query);
			
			ps.setString(1, String.valueOf(kycnPersonalId));
			ps.setLong(2, screeningId);
			ps.executeUpdate();
			
		}finally {
			con.close();
			ps.close();
			
		}
		
	}
	
	public void deleteCardsSubscribed(long accountsInfoId) throws SQLException {
		String sql = "DELETE FROM kycn_cards_subscribed_account WHERE kycn_account_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}
	
	public void insertCardSubscribedInfoTable(Long accountsInfoId, String cardSubscribed, User user)
			throws SQLException {
		String sql = "INSERT INTO kycn_cards_subscribed_account(kycn_account_info_id,cards_subscribed,notes)"
				+ "VALUES(?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, cardSubscribed);
			ps.setString(3, "");
			
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}
	}
	
	public void deleteServicesSubscribed(long accountsInfoId) throws SQLException {
		String sql = "DELETE FROM kycn_services_subscribed_accounts WHERE kycn_accounts_info_id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}
	
	public void insertServiceSubscribedInfoTable(Long accountsInfoId, String serviceSubscribed)
			throws SQLException {
		String sql = "INSERT INTO kycn_services_subscribed_accounts(kycn_accounts_info_id,services_subscribed,notes)"
				+ "VALUES(?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, accountsInfoId);
			ps.setString(2, serviceSubscribed);
			ps.setString(3, "");
			
			ps.executeUpdate();

		} finally {
			con.close();
			ps.close();
		}

	}

}