package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//class for table kycn_account_info
@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnAccountsInfo {

	private long id;
	@JsonProperty("account_id")
	private String accountId;

	@JsonProperty("for_account_id")
	private String forAccountId;

	@JsonProperty("currency_of_account")
	private String currencyOfAccount;

	@JsonProperty("account_no")
	private String accountNo;

	@JsonProperty("account_name")
	private String accountName;

	@JsonProperty("account_short_name")
	private String accountShortName;

	@JsonProperty("account_ownership")
	private String accountOwnership;

	@JsonProperty("scheme_type")
	private String schemeType;

	@JsonProperty("scheme_code")
	private String schemeCode;

	@JsonProperty("gl_sub_head_code")
	private String glSubHeadCode;

	@JsonProperty("product_group")
	private String productGroup;
	

	@JsonProperty("last_transaction_date")
	private Date lastTransactionDate;

	@JsonProperty("account_open_date")
	private Date accountOpenDate;

	@JsonProperty("estimated_monthly_turnover")
	private String estimatedMonthlyTurnOver;

	@JsonProperty("estimated_yearly_turnover")
	private String estimatedYearlyTurnOver;

	@JsonProperty("estimated_monthly_transactions")
	private String estimatedMonthlyTransaction;

	@JsonProperty("estimated_yearly_transactions")
	private String estimatedYearlyTransaction;

	@JsonProperty("source_of_fund")
	private String sourceOfFund;

	@JsonProperty("regular_source_of_income")
	private String regularSourceOfIncome;

	@JsonProperty("notes")
	private String notes;
	
	@JsonProperty("accounts_services_subscribed")
	private List<String> accountServiceSubscribed;

	@JsonProperty("accounts_cards_subscribed")
	private List<String> accountCardSubscribed;

	@JsonProperty("change")
	private boolean change;
	
	@JsonProperty("nature_of_account")
	String natureOfAccount;

	@JsonProperty("scheme_description")
	String schemeDescription;
	
	@JsonProperty("is_blocked")
	private String isBlocked;
	
	@JsonProperty("accounts_related_person")
	private List<KycnRelatedPersonInfo> accountsRelatedPerson;
	
	@JsonProperty("account_status_type")
	private String accountStatusType;
	
	@JsonProperty("account_type")
	private String accountType;
	
	
	@JsonProperty("goaml_account_status_type")
	private String goAMLAccountStatusType;
	
	@JsonProperty("goaml_account_type")
	private String goAMLaccountType;
	
	@JsonProperty("branch_sol_id")
	private String branchSolId;
	
	@JsonProperty("is_active")
	private String removedAccountStatus;

	public KycnAccountsInfo() {
		super();
		this.id = 0;
		this.accountId = "";
		this.forAccountId = "";
		this.currencyOfAccount = "";
		this.accountNo = "";
		this.accountName = "";
		this.accountShortName = "";
		this.accountOwnership = "";
		this.schemeType = "";
		this.schemeCode = "";
		this.glSubHeadCode = "";
		this.productGroup = "";
		// this.lastTransactionDate = lastTransactionDate;
		// this.accountOpenDate = accountOpenDate;
		this.estimatedMonthlyTurnOver = "";
		this.estimatedYearlyTurnOver = "";
		this.estimatedMonthlyTransaction = "";
		this.estimatedYearlyTransaction = "";
		this.sourceOfFund = "";
		this.regularSourceOfIncome = "";
		this.notes = "";
		this.accountCardSubscribed = null;
		this.accountServiceSubscribed = null;
	}

	public KycnAccountsInfo(long id, String accountId, String forAccountId, String currencyOfAccount, String accountNo,
			String accountName, String accountShortName, String accountOwnership, String schemeType, String schemeCode,
			String glSubHeadCode, String productGroup, Date lastTransactionDate, Date accountOpenDate,
			String estimatedMonthlyTurnOver, String estimatedYearlyTurnOver, String estimatedMonthlyTransaction,
			String estimatedYearlyTransaction, String sourceOfFund, String regularSourceOfIncome, String notes,
			List<String> accountServiceSubscribed, List<String> accountCardSubscribed, boolean change) {
		super();
		this.id = id;
		this.accountId = accountId;
		this.forAccountId = forAccountId;
		this.currencyOfAccount = currencyOfAccount;
		this.accountNo = accountNo;
		this.accountName = accountName;
		this.accountShortName = accountShortName;
		this.accountOwnership = accountOwnership;
		this.schemeType = schemeType;
		this.schemeCode = schemeCode;
		this.glSubHeadCode = glSubHeadCode;
		this.productGroup = productGroup;
		this.lastTransactionDate = lastTransactionDate;
		this.accountOpenDate = accountOpenDate;
		this.estimatedMonthlyTurnOver = estimatedMonthlyTurnOver;
		this.estimatedYearlyTurnOver = estimatedYearlyTurnOver;
		this.estimatedMonthlyTransaction = estimatedMonthlyTransaction;
		this.estimatedYearlyTransaction = estimatedYearlyTransaction;
		this.sourceOfFund = sourceOfFund;
		this.regularSourceOfIncome = regularSourceOfIncome;
		this.notes = notes;
		this.accountServiceSubscribed = accountServiceSubscribed;
		this.accountCardSubscribed = accountCardSubscribed;
		this.change = change;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getForAccountId() {
		return forAccountId;
	}

	public void setForAccountId(String forAccountId) {
		this.forAccountId = forAccountId;
	}

	public String getCurrencyOfAccount() {
		return currencyOfAccount;
	}

	public void setCurrencyOfAccount(String currencyOfAccount) {
		this.currencyOfAccount = currencyOfAccount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountShortName() {
		return accountShortName;
	}

	public void setAccountShortName(String accountShortName) {
		this.accountShortName = accountShortName;
	}

	public String getAccountOwnership() {
		return accountOwnership;
	}

	public void setAccountOwnership(String accountOwnership) {
		this.accountOwnership = accountOwnership;
	}

	public String getSchemeType() {
		return schemeType;
	}

	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public String getSchemeCode() {
		return schemeCode;
	}

	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}

	public String getGlSubHeadCode() {
		return glSubHeadCode;
	}

	public void setGlSubHeadCode(String glSubHeadCode) {
		this.glSubHeadCode = glSubHeadCode;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public Date getLastTransactionDate() {
		return lastTransactionDate;
	}

	public void setLastTransactionDate(Date lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public String getEstimatedMonthlyTurnOver() {
		return estimatedMonthlyTurnOver;
	}

	public void setEstimatedMonthlyTurnOver(String estimatedMonthlyTurnOver) {
		this.estimatedMonthlyTurnOver = estimatedMonthlyTurnOver;
	}

	public String getEstimatedYearlyTurnOver() {
		return estimatedYearlyTurnOver;
	}

	public void setEstimatedYearlyTurnOver(String estimatedYearlyTurnOver) {
		this.estimatedYearlyTurnOver = estimatedYearlyTurnOver;
	}

	public String getEstimatedMonthlyTransaction() {
		return estimatedMonthlyTransaction;
	}

	public void setEstimatedMonthlyTransaction(String estimatedMonthlyTransaction) {
		this.estimatedMonthlyTransaction = estimatedMonthlyTransaction;
	}

	public String getEstimatedYearlyTransaction() {
		return estimatedYearlyTransaction;
	}

	public void setEstimatedYearlyTransaction(String estimatedYearlyTransaction) {
		this.estimatedYearlyTransaction = estimatedYearlyTransaction;
	}

	public String getSourceOfFund() {
		return sourceOfFund;
	}

	public void setSourceOfFund(String sourceOfFund) {
		this.sourceOfFund = sourceOfFund;
	}

	public String getRegularSourceOfIncome() {
		return regularSourceOfIncome;
	}

	public void setRegularSourceOfIncome(String regularSourceOfIncome) {
		this.regularSourceOfIncome = regularSourceOfIncome;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<String> getAccountServiceSubscribed() {
		return accountServiceSubscribed;
	}

	public void setAccountServiceSubscribed(List<String> accountServiceSubscribed) {
		this.accountServiceSubscribed = accountServiceSubscribed;
	}

	public List<String> getAccountCardSubscribed() {
		return accountCardSubscribed;
	}

	public void setAccountCardSubscribed(List<String> accountCardSubscribed) {
		this.accountCardSubscribed = accountCardSubscribed;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getNatureOfAccount() {
		return natureOfAccount;
	}

	public void setNatureOfAccount(String natureOfAccount) {
		this.natureOfAccount = natureOfAccount;
	}

	public String getSchemeDescription() {
		return schemeDescription;
	}

	public void setSchemeDescription(String schemeDescription) {
		this.schemeDescription = schemeDescription;
	}

	public String getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(String isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	public List<KycnRelatedPersonInfo> getAccountsRelatedPerson() {
		return accountsRelatedPerson;
	}

	public void setAccountsRelatedPerson(List<KycnRelatedPersonInfo> accountsRelatedPerson) {
		this.accountsRelatedPerson = accountsRelatedPerson;
	}

	public String getAccountStatusType() {
		return accountStatusType;
	}

	public void setAccountStatusType(String accountStatusType) {
		this.accountStatusType = accountStatusType;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getGoAMLAccountStatusType() {
		return goAMLAccountStatusType;
	}

	public void setGoAMLAccountStatusType(String goAMLAccountStatusType) {
		this.goAMLAccountStatusType = goAMLAccountStatusType;
	}

	public String getGoAMLaccountType() {
		return goAMLaccountType;
	}

	public void setGoAMLaccountType(String goAMLaccountType) {
		this.goAMLaccountType = goAMLaccountType;
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public String getRemovedAccountStatus() {
		return removedAccountStatus;
	}

	public void setRemovedAccountStatus(String removedAccountStatus) {
		this.removedAccountStatus = removedAccountStatus;
	}
	
	
	
	
	
	
	
	
}
