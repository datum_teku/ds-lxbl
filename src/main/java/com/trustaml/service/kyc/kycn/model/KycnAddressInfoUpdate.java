package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;



public class KycnAddressInfoUpdate {

	@JsonProperty("id")
	private long id;
	@JsonProperty("country")
	private String country;
	@JsonProperty("state")
	private String state;
	@JsonProperty("zone")
	private String zone;
	@JsonProperty("district")
	private String district;
	@JsonProperty("province") // added looking into jsp
	private String province;
	@JsonProperty("mn_vdc")
	private String mnVdc;
	@JsonProperty("pinzip")
	private String pinZip;
	@JsonProperty("ward_number")
	private String wardNumber;
	@JsonProperty("tole_area")
	private String toleArea;
	@JsonProperty("street")
	private String street;
	@JsonProperty("latitude")
	private String latitude;
	@JsonProperty("longitude")
	private String longitude;
	@JsonProperty("nearest_landmark")
	private String nearestLandMark;
	@JsonProperty("house_no")
	private String houseNo;
	@JsonProperty("unit_number")
	private String unitNumber;
	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;
	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;
	@JsonProperty("phone_no")
	private String phoneNo;
	@JsonProperty("telex_no_country_code")
	private String telexNoCountryCode;
	@JsonProperty("telex_no_area_code")
	private String telexNoAreaCode;
	@JsonProperty("telex_no")
	private String telexNo;
	@JsonProperty("pager_no_country_code")
	private String pagerNoCountryCode;
	@JsonProperty("pager_no_area_code")
	private String pagerNoAreaCode;
	@JsonProperty("pager_no")
	private String pagerNo;
	@JsonProperty("email_id")
	private String emailId;
	@JsonProperty("notes")
	private String notes;
	@JsonProperty("type")
	private String type;

	@JsonProperty("town") // Edited as per jsp
	private String townCity;
	@JsonProperty("primary_contact")
	private String primaryContactNumber;
	@JsonProperty("fax_number")
	private String faxNumber;
	@JsonProperty("secondary_contact_number")
	private String secondaryContactNumber;
	@JsonProperty("email_address")
	private String emailAddress;
	@JsonProperty("post_box_number")
	private String postBoxNumber;
	@JsonProperty("is_rent")
	private String isRent;
	@JsonProperty("landlord_name")
	private String landlordName;
	@JsonProperty("utility_bill_number")
	private String utilityBillnuber;
	@JsonProperty("utility_bill_type")
	private String utilityBillType;
	@JsonProperty("address_type")
	private String addressType;

	private String maker;
	private String checker;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;
	
	@JsonProperty("goaml_address_type")
	private String goAMLAddressType;
	
	@JsonProperty("address")
	private String address;
	
	@JsonProperty("city")
	private String city;
	
	@JsonProperty("goaml_country")
	private String goAMLCountryType;
	
	@JsonProperty("is_active")
	private String removedAddressStatus;

	public KycnAddressInfoUpdate() {
		super();
		this.id = 0;
		this.country = "";
		this.state = "";
		this.zone = "";
		this.district = "";
		this.province = "";
		this.mnVdc = "";
		this.pinZip = "";
		this.wardNumber = "";
		this.toleArea = "";
		this.street = "";
		this.latitude = "";
		this.longitude = "";
		this.nearestLandMark = "";
		this.houseNo = "";
		this.unitNumber = "";
		this.phoneNoCountryCode = "";
		this.phoneNoAreaCode = "";
		this.phoneNo = "";
		this.telexNoCountryCode = "";
		this.telexNoAreaCode = "";
		this.telexNo = "";
		this.pagerNoCountryCode = "";
		this.pagerNoAreaCode = "";
		this.pagerNo = "";
		this.emailId = "";
		this.notes = "";
		this.type = "";
		this.townCity = "";
		this.primaryContactNumber = "";
		this.faxNumber = "";
		this.secondaryContactNumber = "";
		this.emailAddress = "";
		this.postBoxNumber = "";
		this.isRent = "";
		this.landlordName = "";
		this.utilityBillnuber = "";
		this.utilityBillType = "";
		this.addressType = "";

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
	}

	public KycnAddressInfoUpdate(KycnAddressInfo kycnAddressInfo) {
		super();
		this.id = 0;
		this.country = kycnAddressInfo.getCountry();
		this.state = kycnAddressInfo.getState();
		this.zone = kycnAddressInfo.getZone();
		this.district = kycnAddressInfo.getDistrict();
		this.province = kycnAddressInfo.getProvince();
		this.mnVdc = kycnAddressInfo.getMnVdc();
		this.pinZip = kycnAddressInfo.getPinZip();
		this.wardNumber = kycnAddressInfo.getWardNumber();
		this.toleArea = kycnAddressInfo.getToleArea();
		this.street = kycnAddressInfo.getStreet();
		this.latitude = kycnAddressInfo.getLatitude();
		this.longitude = kycnAddressInfo.getLongitude();
		this.nearestLandMark = kycnAddressInfo.getNearestLandMark();
		this.houseNo = kycnAddressInfo.getHouseNumber();
		this.unitNumber = kycnAddressInfo.getUnitNumber();
		this.phoneNoCountryCode = kycnAddressInfo.getPhoneNoCountryCode();
		this.phoneNoAreaCode = kycnAddressInfo.getPhoneNoAreaCode();
		this.phoneNo = kycnAddressInfo.getPhoneNo();
		this.telexNoCountryCode = kycnAddressInfo.getTelexNoCountryCode();
		this.telexNoAreaCode = kycnAddressInfo.getTelexNoAreaCode();
		this.telexNo = kycnAddressInfo.getTelexNo();
		this.pagerNoCountryCode = kycnAddressInfo.getPagerNoCountryCode();
		this.pagerNoAreaCode = kycnAddressInfo.getPagerNoAreaCode();
		this.pagerNo = kycnAddressInfo.getPagerNo();
		this.emailId = kycnAddressInfo.getEmailId();
		this.notes = kycnAddressInfo.getNotes();
		this.type = kycnAddressInfo.getType();
		this.townCity = kycnAddressInfo.getTownCity();
		this.primaryContactNumber = kycnAddressInfo.getPrimaryContactNumber();
		this.faxNumber = kycnAddressInfo.getFaxNumber();
		this.secondaryContactNumber = kycnAddressInfo.getSecondaryContactNumber();
		this.emailAddress = kycnAddressInfo.getEmailAddress();
		this.postBoxNumber = kycnAddressInfo.getPostBoxNumber();
		this.isRent = kycnAddressInfo.getIsRent();
		this.landlordName = kycnAddressInfo.getLandlordName();
		this.utilityBillnuber = kycnAddressInfo.getUtilityBillnuber();
		this.utilityBillType = kycnAddressInfo.getUtilityBillType();
		this.addressType = kycnAddressInfo.getAddressType();

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
	}

	public KycnAddressInfoUpdate(long id, String country, String state, String zone, String district, String province,
			String mnVdc, String pinZip, String wardNumber, String toleArea, String street, String latitude,
			String longitude, String nearestLandMark, String houseNumber, String unitNumber, String phoneNoCountryCode,
			String phoneNoAreaCode, String phoneNo, String telexNoCountryCode, String telexNoAreaCode, String telexNo,
			String pagerNoCountryCode, String pagerNoAreaCode, String pagerNo, String emailId, String notes,
			String type, String townCity, String primaryContactNumber, String faxNumber, String secondaryContactNumber,
			String emailAddress, String postBoxNumber, String isRent, String landlordName, String utilityBillnuber,
			String utilityBillType, String addressType) {
		super();
		this.id = id;
		this.country = country;
		this.state = state;
		this.zone = zone;
		this.district = district;
		this.province = province;
		this.mnVdc = mnVdc;
		this.pinZip = pinZip;
		this.wardNumber = wardNumber;
		this.toleArea = toleArea;
		this.street = street;
		this.latitude = latitude;
		this.longitude = longitude;
		this.nearestLandMark = nearestLandMark;
		this.houseNo = houseNumber;
		this.unitNumber = unitNumber;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.pagerNoCountryCode = pagerNoCountryCode;
		this.pagerNoAreaCode = pagerNoAreaCode;
		this.pagerNo = pagerNo;
		this.emailId = emailId;
		this.notes = notes;
		this.type = type;
		this.townCity = townCity;
		this.primaryContactNumber = primaryContactNumber;
		this.faxNumber = faxNumber;
		this.secondaryContactNumber = secondaryContactNumber;
		this.emailAddress = emailAddress;
		this.postBoxNumber = postBoxNumber;
		this.isRent = isRent;
		this.landlordName = landlordName;
		this.utilityBillnuber = utilityBillnuber;
		this.utilityBillType = utilityBillType;
		this.addressType = addressType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getPinZip() {
		return pinZip;
	}

	public void setPinZip(String pinZip) {
		this.pinZip = pinZip;
	}

	public String getWardNumber() {
		return wardNumber;
	}

	public void setWardNumber(String wardNumber) {
		this.wardNumber = wardNumber;
	}

	public String getToleArea() {
		return toleArea;
	}

	public void setToleArea(String toleArea) {
		this.toleArea = toleArea;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNearestLandMark() {
		return nearestLandMark;
	}

	public void setNearestLandMark(String nearestLandMark) {
		this.nearestLandMark = nearestLandMark;
	}

	public String getHouseNumber() {
		return houseNo;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNo = houseNumber;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getPagerNoCountryCode() {
		return pagerNoCountryCode;
	}

	public void setPagerNoCountryCode(String pagerNoCountryCode) {
		this.pagerNoCountryCode = pagerNoCountryCode;
	}

	public String getPagerNoAreaCode() {
		return pagerNoAreaCode;
	}

	public void setPagerNoAreaCode(String pagerNoAreaCode) {
		this.pagerNoAreaCode = pagerNoAreaCode;
	}

	public String getPagerNo() {
		return pagerNo;
	}

	public void setPagerNo(String pagerNo) {
		this.pagerNo = pagerNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTownCity() {
		return townCity;
	}

	public void setTownCity(String townCity) {
		this.townCity = townCity;
	}

	public String getPrimaryContactNumber() {
		return primaryContactNumber;
	}

	public void setPrimaryContactNumber(String primaryContactNumber) {
		this.primaryContactNumber = primaryContactNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getSecondaryContactNumber() {
		return secondaryContactNumber;
	}

	public void setSecondaryContactNumber(String secondaryContactNumber) {
		this.secondaryContactNumber = secondaryContactNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPostBoxNumber() {
		return postBoxNumber;
	}

	public void setPostBoxNumber(String postBoxNumber) {
		this.postBoxNumber = postBoxNumber;
	}

	public String getIsRent() {
		return isRent;
	}

	public void setIsRent(String isRent) {
		this.isRent = isRent;
	}

	public String getLandlordName() {
		return landlordName;
	}

	public void setLandlordName(String landlordName) {
		this.landlordName = landlordName;
	}

	public String getUtilityBillnuber() {
		return utilityBillnuber;
	}

	public void setUtilityBillnuber(String utilityBillnuber) {
		this.utilityBillnuber = utilityBillnuber;
	}

	public String getUtilityBillType() {
		return utilityBillType;
	}

	public void setUtilityBillType(String utilityBillType) {
		this.utilityBillType = utilityBillType;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getGoAMLAddressType() {
		return goAMLAddressType;
	}

	public void setGoAMLAddressType(String goAMLAddressType) {
		this.goAMLAddressType = goAMLAddressType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGoAMLCountryType() {
		return goAMLCountryType;
	}

	public void setGoAMLCountryType(String goAMLCountryType) {
		this.goAMLCountryType = goAMLCountryType;
	}

	public String getRemovedAddressStatus() {
		return removedAddressStatus;
	}

	public void setRemovedAddressStatus(String removedAddressStatus) {
		this.removedAddressStatus = removedAddressStatus;
	}

	
	
	
	
}