package com.trustaml.service.kyc.kycn.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KycnAmlInformationInfo {
	@JsonProperty("fatca_country")
	boolean fatcaCountry;

	@JsonProperty("us_non_us")
	boolean usNonUs;

	@JsonProperty("risk_categorization")
	String riskCategorization;

	@JsonProperty("recommendations")
	String recommendations;

	@JsonProperty("notes")
	String amlNotes;

	@JsonProperty("check")
	boolean amlCheck;

	@JsonProperty("id")
	long id;

	@JsonProperty("change")
	private boolean change;

	public KycnAmlInformationInfo(boolean fatcaCountry, boolean usNonUs, String riskCategorization,
			String recommendations, String amlNotes, boolean amlCheck, long id, boolean change) {
		this.fatcaCountry = fatcaCountry;
		this.usNonUs = usNonUs;
		this.riskCategorization = riskCategorization;
		this.recommendations = recommendations;
		this.amlNotes = amlNotes;
		this.amlCheck = amlCheck;
		this.id = id;
		this.change = change;
	}

	public KycnAmlInformationInfo() {
		this.fatcaCountry = false;
		this.usNonUs = false;
		this.riskCategorization = "";
		this.recommendations = "";
		this.amlNotes = "";
		this.amlCheck = false;
		this.id = 0;
	}

	public KycnAmlInformationInfo(KycnAmlInformationInfo kycnAmlInformation) {
		this.fatcaCountry = kycnAmlInformation.isFatcaCountry();
		this.usNonUs = kycnAmlInformation.isUsNonUs();
		this.riskCategorization = kycnAmlInformation.getRiskCategorization();
		this.recommendations = kycnAmlInformation.getRecommendations();
		this.amlNotes = kycnAmlInformation.getAmlNotes();
		this.amlCheck = kycnAmlInformation.isAmlCheck();
		this.change = kycnAmlInformation.isChange();
	}

	public boolean isFatcaCountry() {
		return fatcaCountry;
	}

	public void setFatcaCountry(boolean fatcaCountry) {
		this.fatcaCountry = fatcaCountry;
	}

	public boolean isUsNonUs() {
		return usNonUs;
	}

	public void setUsNonUs(boolean usNonUs) {
		this.usNonUs = usNonUs;
	}

	public String getRiskCategorization() {
		return riskCategorization;
	}

	public void setRiskCategorization(String riskCategorization) {
		this.riskCategorization = riskCategorization;
	}

	public String getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(String recommendations) {
		this.recommendations = recommendations;
	}

	public String getAmlNotes() {
		return amlNotes;
	}

	public void setAmlNotes(String amlNotes) {
		this.amlNotes = amlNotes;
	}

	public boolean isAmlCheck() {
		return amlCheck;
	}

	public void setAmlCheck(boolean amlCheck) {
		this.amlCheck = amlCheck;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
