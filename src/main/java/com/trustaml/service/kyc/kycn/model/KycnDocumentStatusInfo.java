package com.trustaml.service.kyc.kycn.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KycnDocumentStatusInfo {

	@JsonProperty("document_type")
	String documentType;

	@JsonProperty("document_status")
	String documentStatus;

	@JsonProperty("date")
	java.sql.Date date;

	@JsonProperty("refresh_date")
	java.sql.Date refershDate;

	@JsonProperty("notes")
	String notes;

	@JsonProperty("id")
	long id;

	@JsonProperty("change")
	private boolean change;

	public KycnDocumentStatusInfo(String documentType, String documentStatus, java.sql.Date date,
			java.sql.Date refershDate, String notes, long id, boolean change) {
		this.documentType = documentType;
		this.documentStatus = documentStatus;
		this.date = date;
		this.refershDate = refershDate;
		this.notes = notes;
		this.id = id;
		this.change = change;
	}

	public KycnDocumentStatusInfo() {
		this.documentType = "";
		this.documentStatus = "";
		this.date = null;
		this.refershDate = null;
		this.notes = "";
		this.id = 0;
	}

	public KycnDocumentStatusInfo(KycnDocumentStatusInfo kycnDocumentStatusInfo) {
		this.documentType = kycnDocumentStatusInfo.getDocumentType();
		this.documentStatus = kycnDocumentStatusInfo.getDocumentStatus();
		this.date = kycnDocumentStatusInfo.getDate();
		this.refershDate = kycnDocumentStatusInfo.getRefershDate();
		this.notes = kycnDocumentStatusInfo.getNotes();
		this.change = kycnDocumentStatusInfo.isChange();
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public java.sql.Date getRefershDate() {
		return refershDate;
	}

	public void setRefershDate(java.sql.Date refershDate) {
		this.refershDate = refershDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
