package com.trustaml.service.kyc.kycn.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//class for table kycn_education_info
@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnEducationInfo {

	private long id;

	@JsonProperty("qualification")
	private String qualification;
	@JsonProperty("name_of_institute")
	private String nameOfInstitute;
	@JsonProperty("field_of_study")
	private String fieldOfStudy;
	@JsonProperty("year_of_graduation")
	private String yearOfGraduation;
	@JsonProperty("notes")
	private String notes;
	@JsonProperty("change")
	private boolean change;

	public KycnEducationInfo() {
		super();
		this.id = 0;
		this.qualification = "";
		this.nameOfInstitute = "";
		this.fieldOfStudy = "";
		this.yearOfGraduation = "";
		this.notes = "";
	}

	public KycnEducationInfo(long id, String qualification, String nameOfInstitute, String fieldOfStudy,
			String yearOfGraduation, String notes, boolean change) {
		super();
		this.id = id;
		this.qualification = qualification;
		this.nameOfInstitute = nameOfInstitute;
		this.fieldOfStudy = fieldOfStudy;
		this.yearOfGraduation = yearOfGraduation;
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getNameOfInstitute() {
		return nameOfInstitute;
	}

	public void setNameOfInstitute(String nameOfInstitute) {
		this.nameOfInstitute = nameOfInstitute;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getYearOfGraduation() {
		return yearOfGraduation;
	}

	public void setYearOfGraduation(String yearOfGraduation) {
		this.yearOfGraduation = yearOfGraduation;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}