package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



//class for table kycn_education_info
@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnEducationInfoUpdate {

	private long id;

	@JsonProperty("qualification")
	private String qualification;
	@JsonProperty("name_of_institute")
	private String nameOfInstitute;
	@JsonProperty("field_of_study")
	private String fieldOfStudy;
	@JsonProperty("year_of_graduation")
	private String yearOfGraduation;
	@JsonProperty("notes")
	private String notes;

	private String maker;
	private String checker;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;

	public KycnEducationInfoUpdate() {
		super();
		this.id = 0;
		this.qualification = "";
		this.nameOfInstitute = "";
		this.fieldOfStudy = "";
		this.yearOfGraduation = "";
		this.notes = "";

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
	}

	public KycnEducationInfoUpdate(KycnEducationInfo kycnEducationInfo) {
		super();
		this.id = 0;
		this.qualification = kycnEducationInfo.getQualification();
		this.nameOfInstitute = kycnEducationInfo.getNameOfInstitute();
		this.fieldOfStudy = kycnEducationInfo.getFieldOfStudy();
		this.yearOfGraduation = kycnEducationInfo.getYearOfGraduation();
		this.notes = kycnEducationInfo.getNotes();

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
	}

	public KycnEducationInfoUpdate(long id, String qualification, String nameOfInstitute, String fieldOfStudy,
			String yearOfGraduation, String notes) {
		super();
		this.id = id;
		this.qualification = qualification;
		this.nameOfInstitute = nameOfInstitute;
		this.fieldOfStudy = fieldOfStudy;
		this.yearOfGraduation = yearOfGraduation;
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getNameOfInstitute() {
		return nameOfInstitute;
	}

	public void setNameOfInstitute(String nameOfInstitute) {
		this.nameOfInstitute = nameOfInstitute;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getYearOfGraduation() {
		return yearOfGraduation;
	}

	public void setYearOfGraduation(String yearOfGraduation) {
		this.yearOfGraduation = yearOfGraduation;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}