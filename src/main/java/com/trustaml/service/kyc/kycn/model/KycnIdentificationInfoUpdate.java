package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnIdentificationInfoUpdate {
	
	@JsonProperty("id")
	private long id;
	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;
	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationDocumentNo;
	@JsonProperty("country_of_issue")
	private String countryOfIssue;
	/** new FIELDS **/
	@JsonProperty("passport_no")
	private String passportNo;
	
	@JsonProperty("passport_country")
	private String passportCountry;
	@JsonProperty("passport_issuing_authority")
	private String passportIssuingAuthority;
	@JsonProperty("passport_place_of_issue")
	private String passportPlaceOfIssue;
	@JsonProperty("passport_issue_date")
	private Date passportIssueDate;
	@JsonProperty("expiry_date")
	private Date expiryDate;
	@JsonProperty("visa_no")
	private String visaNo;
	@JsonProperty("visa_expiry_date")
	private Date visaExpiryDate;
	@JsonProperty("nepal_entry_date")
	private Date nepalEntryDate;
	@JsonProperty("notes")
	private String notes;
	
	
	
	@JsonProperty("identification_number")
	private String identificationNumber;
	@JsonProperty("issuing_authority")
	private String issuingAuthority;
	@JsonProperty("place_of_issue")
	private String placeOfIssue;
	@JsonProperty("issue_date")
	private java.sql.Date issueDate;
	
	@JsonProperty("goaml_primary_identification_document_type")
	private String goAMLPrimaryIdentificationDocumentType;
	
	
	@JsonProperty("goaml_country_of_issue")
	private String goAMLCountryOfIssue;
	
	private String maker;
	private String checker;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;
	
	

	public KycnIdentificationInfoUpdate(long id, String primaryIdentificationDocumentType,
			String primaryIdentificationDocumentNo, String countryOfIssue, String passportNo, String passportCountry,
			String passportIssuingAuthority, String passportPlaceOfIssue, Date passportIssueDate, Date expiryDate,
			String visaNo, Date visaExpiryDate, Date nepalEntryDate, String notes, String identificationNumber,
			String issuingAuthority, String placeOfIssue, Date issueDate) {
		super();
		this.id = id;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
		this.countryOfIssue = countryOfIssue;
		this.passportNo = passportNo;
		this.passportCountry = passportCountry;
		this.passportIssuingAuthority = passportIssuingAuthority;
		this.passportPlaceOfIssue = passportPlaceOfIssue;
		this.passportIssueDate = passportIssueDate;
		this.expiryDate = expiryDate;
		this.visaNo = visaNo;
		this.visaExpiryDate = visaExpiryDate;
		this.nepalEntryDate = nepalEntryDate;
		this.notes = notes;
		this.identificationNumber = identificationNumber;
		this.issuingAuthority = issuingAuthority;
		this.placeOfIssue = placeOfIssue;
		this.issueDate = issueDate;
	}

	public KycnIdentificationInfoUpdate() {
		super();
		this.id = 0;
		this.primaryIdentificationDocumentType = "";
		this.countryOfIssue = "";
		this.identificationNumber = "";
		this.issuingAuthority = "";
		this.placeOfIssue = "";
//		this.issueDate = "";
		//this.expiryDate = "";
		this.notes = "";
		this.passportNo = "";
		this.passportCountry = "";
		this.passportIssuingAuthority = "";
		this.passportPlaceOfIssue = "";
	//	this.passportIssueDate = "";
		//this.expiryDate = "";
		this.visaNo = "";
		//this.visaExpiryDate = "";
		//this.nepalEntryDate = "";
		this.notes = "";
		this.identificationNumber = "";
		this.issuingAuthority = "";
		this.placeOfIssue = "";
		
		this.maker="";
		this.checker ="";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason ="";
		
	}
	
	public KycnIdentificationInfoUpdate(KycnIdentificationInfo kycnIdentificationInfo) {
		super();
		this.id = 0;
		this.primaryIdentificationDocumentType = kycnIdentificationInfo.getPrimaryIdentificationDocumentType();
		this.primaryIdentificationDocumentNo = kycnIdentificationInfo.getPrimaryIdentificationDocumentNo();
		this.countryOfIssue = kycnIdentificationInfo.getCountryOfIssue();
		this.identificationNumber = kycnIdentificationInfo.getIdentificationNumber();
		this.issuingAuthority = kycnIdentificationInfo.getIssuingAuthority();
		this.placeOfIssue = kycnIdentificationInfo.getPlaceOfIssue();
//		this.issueDate = kycnIdentificationInfo;
		//this.expiryDate = kycnIdentificationInfo;
		this.notes = kycnIdentificationInfo.getNotes();
		this.passportNo = kycnIdentificationInfo.getPassportNo();
		this.passportCountry = kycnIdentificationInfo.getPassportCountry();
		this.passportIssuingAuthority = kycnIdentificationInfo.getPassportIssuingAuthority();
		this.passportPlaceOfIssue = kycnIdentificationInfo.getPassportPlaceOfIssue();
	//	this.passportIssueDate = kycnIdentificationInfo;
		//this.expiryDate = kycnIdentificationInfo;
		this.visaNo = kycnIdentificationInfo.getVisaNo();
		//this.visaExpiryDate = kycnIdentificationInfo;
		//this.nepalEntryDate = kycnIdentificationInfo;
		
		
		this.maker="";
		this.checker ="";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason ="";
		
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getPrimaryIdentificationDocumentNo() {
		return primaryIdentificationDocumentNo;
	}

	public void setPrimaryIdentificationDocumentNo(String primaryIdentificationDocumentNo) {
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(String passportCountry) {
		this.passportCountry = passportCountry;
	}

	public String getPassportIssuingAuthority() {
		return passportIssuingAuthority;
	}

	public void setPassportIssuingAuthority(String passportIssuingAuthority) {
		this.passportIssuingAuthority = passportIssuingAuthority;
	}

	public String getPassportPlaceOfIssue() {
		return passportPlaceOfIssue;
	}

	public void setPassportPlaceOfIssue(String passportPlaceOfIssue) {
		this.passportPlaceOfIssue = passportPlaceOfIssue;
	}

	public Date getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(Date passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getVisaNo() {
		return visaNo;
	}

	public void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}

	public Date getVisaExpiryDate() {
		return visaExpiryDate;
	}

	public void setVisaExpiryDate(Date visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}

	public Date getNepalEntryDate() {
		return nepalEntryDate;
	}

	public void setNepalEntryDate(Date nepalEntryDate) {
		this.nepalEntryDate = nepalEntryDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public java.sql.Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(java.sql.Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getGoAMLPrimaryIdentificationDocumentType() {
		return goAMLPrimaryIdentificationDocumentType;
	}

	public void setGoAMLPrimaryIdentificationDocumentType(String goAMLPrimaryIdentificationDocumentType) {
		this.goAMLPrimaryIdentificationDocumentType = goAMLPrimaryIdentificationDocumentType;
	}

	public String getGoAMLCountryOfIssue() {
		return goAMLCountryOfIssue;
	}

	public void setGoAMLCountryOfIssue(String goAMLCountryOfIssue) {
		this.goAMLCountryOfIssue = goAMLCountryOfIssue;
	}

	
	
	
}
