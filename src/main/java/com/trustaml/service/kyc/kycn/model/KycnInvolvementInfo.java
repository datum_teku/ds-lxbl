package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnInvolvementInfo {

	@JsonProperty("id")
	private long id;
	@JsonProperty("kycl_id")
	private long kyclId;

	@JsonProperty("organization_nature")
	private String organizationNature;

	@JsonProperty("country")
	private String country;

	@JsonProperty("state")
	private String state;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;

	@JsonProperty("province") // added looking into jsp
	private String province;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("pinzip")
	private String pinZip;

	@JsonProperty("involvement_ward_number")
	private String involvementWardNumber;

	@JsonProperty("tole_area")
	private String toleArea;

	@JsonProperty("street")
	private String street;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("longitude")
	private String longitude;

	@JsonProperty("nearest_landmark")
	private String nearestLandMark;

	@JsonProperty("house_no")
	private String houseNo;

	@JsonProperty("unit_number")
	private String unitNumber;

	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("fax_no_country_code")
	private String faxNoCountryCode;

	@JsonProperty("fax_no_area_code")
	private String faxNoAreaCode;

	@JsonProperty("fax_no")
	private String faxNo;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("website")
	private String website;

	@JsonProperty("panvat")
	private String panVat;

	@JsonProperty("poboxno")
	private String poBoxNo;

	@JsonProperty("nature")
	private String nature;

	@JsonProperty("designation")
	private String designation;

	@JsonProperty("start_date")
	private Date startDate;

	@JsonProperty("end_date")
	private Date endDate;

	@JsonProperty("involvement_notes")
	private String involvementNotes;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("involvement_nature")
	private String involvementNature;

	@JsonProperty("occupation_type")
	private String occupationType;

	@JsonProperty("change")
	private boolean change;
	
	
	@JsonProperty("organization_name")
	private String organizationName;
	

	public KycnInvolvementInfo(long id, long kyclId, String organizationNature, String country, String state,
			String zone, String district, String province, String mnVdc, String pinZip, String involvementWardNumber,
			String toleArea, String street, String latitude, String longitude, String nearestLandMark, String houseNo,
			String unitNumber, String phoneNoCountryCode, String phoneNoAreaCode, String phoneNo,
			String faxNoCountryCode, String faxNoAreaCode, String faxNo, String emailId, String website, String panVat,
			String poBoxNo, String nature, String designation, Date startDate, Date endDate, String involvementNotes,
			String notes, String involvementNature, String occupationType, boolean change) {
		super();
		this.id = id;
		this.kyclId = kyclId;
		this.organizationNature = organizationNature;
		this.country = country;
		this.state = state;
		this.zone = zone;
		this.district = district;
		this.province = province;
		this.mnVdc = mnVdc;
		this.pinZip = pinZip;
		this.involvementWardNumber = involvementWardNumber;
		this.toleArea = toleArea;
		this.street = street;
		this.latitude = latitude;
		this.longitude = longitude;
		this.nearestLandMark = nearestLandMark;
		this.houseNo = houseNo;
		this.unitNumber = unitNumber;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.faxNoCountryCode = faxNoCountryCode;
		this.faxNoAreaCode = faxNoAreaCode;
		this.faxNo = faxNo;
		this.emailId = emailId;
		this.website = website;
		this.panVat = panVat;
		this.poBoxNo = poBoxNo;
		this.nature = nature;
		this.designation = designation;
		this.startDate = startDate;
		this.endDate = endDate;
		this.involvementNotes = involvementNotes;
		this.notes = notes;
		this.involvementNature = involvementNature;
		this.occupationType = occupationType;
		this.change = change;
	}

	public KycnInvolvementInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getKyclId() {
		return kyclId;
	}

	public void setKyclId(long kyclId) {
		this.kyclId = kyclId;
	}

	public String getOrganizationNature() {
		return organizationNature;
	}

	public void setOrganizationNature(String organizationNature) {
		this.organizationNature = organizationNature;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getPinZip() {
		return pinZip;
	}

	public void setPinZip(String pinZip) {
		this.pinZip = pinZip;
	}

	public String getInvolvementWardNumber() {
		return involvementWardNumber;
	}

	public void setInvolvementWardNumber(String involvementWardNumber) {
		this.involvementWardNumber = involvementWardNumber;
	}

	public String getToleArea() {
		return toleArea;
	}

	public void setToleArea(String toleArea) {
		this.toleArea = toleArea;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getNearestLandMark() {
		return nearestLandMark;
	}

	public void setNearestLandMark(String nearestLandMark) {
		this.nearestLandMark = nearestLandMark;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getFaxNoCountryCode() {
		return faxNoCountryCode;
	}

	public void setFaxNoCountryCode(String faxNoCountryCode) {
		this.faxNoCountryCode = faxNoCountryCode;
	}

	public String getFaxNoAreaCode() {
		return faxNoAreaCode;
	}

	public void setFaxNoAreaCode(String faxNoAreaCode) {
		this.faxNoAreaCode = faxNoAreaCode;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPanVat() {
		return panVat;
	}

	public void setPanVat(String panVat) {
		this.panVat = panVat;
	}

	public String getPoBoxNo() {
		return poBoxNo;
	}

	public void setPoBoxNo(String poBoxNo) {
		this.poBoxNo = poBoxNo;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getInvolvementNotes() {
		return involvementNotes;
	}

	public void setInvolvementNotes(String involvementNotes) {
		this.involvementNotes = involvementNotes;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getInvolvementNature() {
		return involvementNature;
	}

	public void setInvolvementNature(String involvementNature) {
		this.involvementNature = involvementNature;
	}

	public String getOccupationType() {
		return occupationType;
	}

	public void setOccupationType(String occupationType) {
		this.occupationType = occupationType;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	
	

}