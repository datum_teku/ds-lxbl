package com.trustaml.service.kyc.kycn.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KycnObservationInfo {

	@JsonProperty("observation_type")
	private String observationType;

	@JsonProperty("internal_observation_physical")
	private String internalObservationPhysical;

	@JsonProperty("internal_observation_financial")
	private String internalObservationFinancial;

	@JsonProperty("internal_observation_behavioural")
	private String internalObservationBehavioral;

	@JsonProperty("connected_person")
	private String internalObservationConnectedPerson;

	@JsonProperty("connected_person_id")
	private String connectedPersonId;

	@JsonProperty("intended_objective_of_business_relation")
	private String internalObservationIntendedObjectiveOfBusinessRelation;

	@JsonProperty("observation_media_source")
	private String observationMediaSource;

	@JsonProperty("observation_date")
	private java.sql.Date observationDate;

	@JsonProperty("hour_of_day")
	private String observationTime;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("id")
	private long id;

	@JsonProperty("change")
	private boolean change;

	public KycnObservationInfo() {
		this.observationType = "";
		this.internalObservationPhysical = "";
		this.internalObservationFinancial = "";
		this.internalObservationBehavioral = "";
		this.internalObservationConnectedPerson = "";
		this.connectedPersonId = "";
		this.internalObservationIntendedObjectiveOfBusinessRelation = "";
		this.observationMediaSource = "";
		this.observationDate = null;
		this.observationTime = "00:00:00";
		this.notes = "";
		this.id = 0L;
	}

	public KycnObservationInfo(String observationType, String internalObservationPhysical,
			String internalObservationFinancial, String internalObservationBehavioral,
			String internalObservationConnectedPerson, String connectedPersonId,
			String internalObservationIntendedObjectiveOfBusinessRelation, String observationMediaSource,
			java.sql.Date observationDate, String observationTime, String notes, long id, boolean change) {
		this.observationType = observationType;
		this.internalObservationPhysical = internalObservationPhysical;
		this.internalObservationFinancial = internalObservationFinancial;
		this.internalObservationBehavioral = internalObservationBehavioral;
		this.internalObservationConnectedPerson = internalObservationConnectedPerson;
		this.connectedPersonId = connectedPersonId;
		this.internalObservationIntendedObjectiveOfBusinessRelation = internalObservationIntendedObjectiveOfBusinessRelation;
		this.observationMediaSource = observationMediaSource;
		this.observationDate = observationDate;
		this.observationTime = observationTime;
		this.notes = notes;
		this.id = id;
		this.change = change;
	}

	public KycnObservationInfo(KycnObservationInfo kycnObservationInfo) {
		this.observationType = kycnObservationInfo.getObservationType();
		this.internalObservationPhysical = kycnObservationInfo.getInternalObservationPhysical();
		this.internalObservationFinancial = kycnObservationInfo.getInternalObservationFinancial();
		this.internalObservationBehavioral = kycnObservationInfo.getInternalObservationBehavioral();
		this.internalObservationConnectedPerson = kycnObservationInfo.getInternalObservationConnectedPerson();
		this.connectedPersonId = kycnObservationInfo.getConnectedPersonId();
		this.internalObservationIntendedObjectiveOfBusinessRelation = kycnObservationInfo
				.getInternalObservationIntendedObjectiveOfBusinessRelation();
		this.observationMediaSource = kycnObservationInfo.getObservationMediaSource();
		this.observationDate = kycnObservationInfo.getObservationDate();
		this.observationTime = kycnObservationInfo.getObservationTime();
		this.notes = kycnObservationInfo.getNotes();
		this.change = kycnObservationInfo.isChange();
	}

	public String getObservationType() {
		return observationType;
	}

	public void setObservationType(String observationType) {
		this.observationType = observationType;
	}

	public String getInternalObservationPhysical() {
		return internalObservationPhysical;
	}

	public void setInternalObservationPhysical(String internalObservationPhysical) {
		this.internalObservationPhysical = internalObservationPhysical;
	}

	public String getInternalObservationFinancial() {
		return internalObservationFinancial;
	}

	public void setInternalObservationFinancial(String internalObservationFinancial) {
		this.internalObservationFinancial = internalObservationFinancial;
	}

	public String getInternalObservationBehavioral() {
		return internalObservationBehavioral;
	}

	public void setInternalObservationBehavioral(String internalObservationBehavioral) {
		this.internalObservationBehavioral = internalObservationBehavioral;
	}

	public String getInternalObservationConnectedPerson() {
		return internalObservationConnectedPerson;
	}

	public void setInternalObservationConnectedPerson(String internalObservationConnectedPerson) {
		this.internalObservationConnectedPerson = internalObservationConnectedPerson;
	}

	public String getConnectedPersonId() {
		return connectedPersonId;
	}

	public void setConnectedPersonId(String connectedPersonId) {
		this.connectedPersonId = connectedPersonId;
	}

	public String getInternalObservationIntendedObjectiveOfBusinessRelation() {
		return internalObservationIntendedObjectiveOfBusinessRelation;
	}

	public void setInternalObservationIntendedObjectiveOfBusinessRelation(
			String internalObservationIntendedObjectiveOfBusinessRelation) {
		this.internalObservationIntendedObjectiveOfBusinessRelation = internalObservationIntendedObjectiveOfBusinessRelation;
	}

	public String getObservationMediaSource() {
		return observationMediaSource;
	}

	public void setObservationMediaSource(String observationMediaSource) {
		this.observationMediaSource = observationMediaSource;
	}

	public java.sql.Date getObservationDate() {
		return observationDate;
	}

	public void setObservationDate(java.sql.Date observationDate) {
		this.observationDate = observationDate;
	}

	public String getObservationTime() {
		return observationTime;
	}

	public void setObservationTime(String observationTime) {
		this.observationTime = observationTime;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
