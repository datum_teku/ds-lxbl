package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnPersonalInfoUpdate {

	@JsonProperty("cust_id")
	long id;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("cbs_generated_name")
	private String cbsGeneratedName;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("ls_title")
	private String lsTitle;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("previous_name")
	private String previousName;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("date_of_birth")
	private Date dateOfBirth;

	@JsonProperty("age")
	private String age;

	@JsonProperty("marital_status")
	private String maritalStatus;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("customer_type")
	private String customerType;

	@JsonProperty("customer_group")
	private String customerGroup;

	@JsonProperty("customer_constitution")
	private String customerConstitution;

	@JsonProperty("customer_community")
	private String customerCommunity;

	@JsonProperty("customer_caste")
	private String customerCaste;

	@JsonProperty("customer_employee_id")
	private String customerEmployeeId;

	@JsonProperty("customer_open_date")
	private Date customerOpenDate;

	@JsonProperty("customer_maker")
	private String customerMaker;

	@JsonProperty("customer_status_code")
	private String customerStatusCode;

	@JsonProperty("minor")
	private boolean minor;

	@JsonProperty("customer_nre_flag")
	private boolean nonResidentExternal;

	@JsonProperty("customer_cardholder_flag")
	private boolean cardHolder;

	@JsonProperty("pan_number")
	private String panNumber;

	private String maker;

	private String checker;

	private boolean approved;

	private Date updateDate;

	private Date approvedDate;

	private String reason;

	@JsonProperty("user")
	private User user;

	public KycnPersonalInfoUpdate() {
		super();
		this.id = 0;
		this.salutation = "";
		this.cbsGeneratedName = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsTitle = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.secondName = "";
		this.calledByName = "";
		this.previousName = "";
		this.gender = "";
		// this.dateOfBirth = "";
		this.age = "";
		this.maritalStatus = "";
		this.notes = "";
		this.customerType = "";
		this.customerGroup = "";
		this.customerConstitution = "";
		this.customerCommunity = "";
		this.customerCaste = "";
		this.customerEmployeeId = "";
		// this.customerOpenDate = "";
		this.customerMaker = "";
		this.customerStatusCode = "";
		this.minor = false;
		this.nonResidentExternal = false;
		this.cardHolder = false;
		this.panNumber = "";

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
		this.user = new User();

	}

	public KycnPersonalInfoUpdate(KycnPersonalInfo kycnPersonalInfo) {
		super();
		this.id = kycnPersonalInfo.getId();
		this.salutation = kycnPersonalInfo.getSalutation();
		this.cbsGeneratedName = kycnPersonalInfo.getCbsGeneratedName();
		this.firstName = kycnPersonalInfo.getFirstName();
		this.middleName = kycnPersonalInfo.getMiddleName();
		this.lastName = kycnPersonalInfo.getLastName();
		this.lsTitle = kycnPersonalInfo.getLsTitle();
		this.lsfName = kycnPersonalInfo.getLsfName();
		this.lsmName = kycnPersonalInfo.getLsmName();
		this.lslName = kycnPersonalInfo.getLslName();
		this.secondName = kycnPersonalInfo.getSecondName();
		this.calledByName = kycnPersonalInfo.getCalledByName();
		this.previousName = kycnPersonalInfo.getPreviousName();
		this.gender = kycnPersonalInfo.getGender();
		// this.dateOfBirth = kycnPersonalInfo;
		this.age = kycnPersonalInfo.getAge();
		this.maritalStatus = kycnPersonalInfo.getMaritalStatus();
		this.notes = kycnPersonalInfo.getNotes();
		this.customerType = kycnPersonalInfo.getCustomerType();
		this.customerGroup = kycnPersonalInfo.getCustomerGroup();
		this.customerConstitution = kycnPersonalInfo.getCustomerConstitution();
		this.customerCommunity = kycnPersonalInfo.getCustomerCommunity();
		this.customerCaste = kycnPersonalInfo.getCustomerCaste();
		this.customerEmployeeId = kycnPersonalInfo.getCustomerEmployeeId();
		// this.customerOpenDate = "";
		this.customerMaker = kycnPersonalInfo.getCustomerMaker();
		this.customerStatusCode = kycnPersonalInfo.getCustomerStatusCode();
		this.minor = false;
		this.nonResidentExternal = false;
		this.cardHolder = false;
		this.panNumber = kycnPersonalInfo.getPanNumber();
		this.user = kycnPersonalInfo.getUser();

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
		this.user = kycnPersonalInfo.getUser();

	}

	public KycnPersonalInfoUpdate(long id, String salutation, String cbsGeneratedName, String firstName,
			String middleName, String lastName, String lsTitle, String lsfName, String lsmName, String lslName,
			String secondName, String calledByName, String previousName, String gender, Date dateOfBirth, String age,
			String maritalStatus, String notes, String customerType, String customerGroup, String customerConstitution,
			String customerCommunity, String customerCaste, String customerEmployeeId, Date customerOpenDate,
			String customerMaker, String customerStatusCode, boolean minor, boolean nonResidentExternal,
			boolean cardHolder, String panNUmber, User user) {
		super();
		this.id = id;
		this.salutation = salutation;
		this.cbsGeneratedName = cbsGeneratedName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsTitle = lsTitle;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.secondName = secondName;
		this.calledByName = calledByName;
		this.previousName = previousName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.age = age;
		this.maritalStatus = maritalStatus;
		this.notes = notes;
		this.customerType = customerType;
		this.customerGroup = customerGroup;
		this.customerConstitution = customerConstitution;
		this.customerCommunity = customerCommunity;
		this.customerCaste = customerCaste;
		this.customerEmployeeId = customerEmployeeId;
		this.customerOpenDate = customerOpenDate;
		this.customerMaker = customerMaker;
		this.customerStatusCode = customerStatusCode;
		this.minor = minor;
		this.nonResidentExternal = nonResidentExternal;
		this.cardHolder = cardHolder;
		this.panNumber = panNUmber;
		this.user = user;
	}

	public String getLsTitle() {
		return lsTitle;
	}

	public void setLsTitle(String lsTitle) {
		this.lsTitle = lsTitle;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}

	public String getCustomerConstitution() {
		return customerConstitution;
	}

	public void setCustomerConstitution(String customerConstitution) {
		this.customerConstitution = customerConstitution;
	}

	public String getCustomerCommunity() {
		return customerCommunity;
	}

	public void setCustomerCommunity(String customerCommunity) {
		this.customerCommunity = customerCommunity;
	}

	public String getCustomerCaste() {
		return customerCaste;
	}

	public void setCustomerCaste(String customerCaste) {
		this.customerCaste = customerCaste;
	}

	public String getCustomerEmployeeId() {
		return customerEmployeeId;
	}

	public void setCustomerEmployeeId(String customerEmployeeId) {
		this.customerEmployeeId = customerEmployeeId;
	}

	public Date getCustomerOpenDate() {
		return customerOpenDate;
	}

	public void setCustomerOpenDate(Date customerOpenDate) {
		this.customerOpenDate = customerOpenDate;
	}

	public String getCustomerMaker() {
		return customerMaker;
	}

	public void setCustomerMaker(String customerMaker) {
		this.customerMaker = customerMaker;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCbsGeneratedName() {
		return cbsGeneratedName;
	}

	public void setCbsGeneratedName(String cbsGeneratedName) {
		this.cbsGeneratedName = cbsGeneratedName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPreviousName() {
		return previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustomerStatusCode() {
		return customerStatusCode;
	}

	public void setCustomerStatusCode(String customerStatusCode) {
		this.customerStatusCode = customerStatusCode;
	}

	public boolean isMinor() {
		return minor;
	}

	public void setMinor(boolean minor) {
		this.minor = minor;
	}

	public boolean isNonResidentExternal() {
		return nonResidentExternal;
	}

	public void setNonResidentExternal(boolean nonResidentExternal) {
		this.nonResidentExternal = nonResidentExternal;
	}

	public boolean isCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(boolean cardHolder) {
		this.cardHolder = cardHolder;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}