package com.trustaml.service.kyc.kycn.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KycnRelatedEntityInfo {

	private long id;

	@JsonProperty("related_entity_type")
	private String relatedEntityType;

	@JsonProperty("salutatiton")
	private String salutatiton;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("kycn_id")
	private long kycnId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("ls_name")
	private String lsName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;

	@JsonProperty("registration_no")
	private String registrationNo;

	@JsonProperty("related_entity_country")
	private String relatedEntityCountry;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("pinzip")
	private String pinzip;

	@JsonProperty("ward_number")
	private String wardNumber;

	@JsonProperty("tole_area")
	private String toleArea;

	@JsonProperty("street")
	private String street;

	@JsonProperty("house_no")
	private String houseNo;

	@JsonProperty("unit_number")
	private String unitNumber;

	@JsonProperty("nearest_landmark")
	private String nearestLandmark;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("longitude")
	private String longitude;

	@JsonProperty("phone_no_country_code")
	private String phoneNoCountryCode;

	@JsonProperty("phone_no_area_code")
	private String phoneNoAreaCode;

	@JsonProperty("phone_no")
	private String phoneNo;

	@JsonProperty("telex_no_country_code")
	private String telexNoCountryCode;

	@JsonProperty("telex_no_area_code")
	private String telexNoAreaCode;

	@JsonProperty("telex_no")
	private String telexNo;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	@JsonProperty("province")
	private String province;

	public KycnRelatedEntityInfo() {
		super();
	}

	public KycnRelatedEntityInfo(long id, String relatedEntityType, String custId, long kycnId, String name,
			String lsName, String calledByName, String primaryIdentificationDocumentType, String registrationNo,
			String relatedEntityCountry, String zone, String district, String mnVdc, String pinzip, String wardNumber,
			String toleArea, String street, String houseNo, String unitNumber, String nearestLandmark, String latitude,
			String longitude, String phoneNoCountryCode, String phoneNoAreaCode, String phoneNo,
			String telexNoCountryCode, String telexNoAreaCode, String telexNo, String emailId, String notes,
			String salutatiton, boolean change) {
		super();
		this.id = id;
		this.relatedEntityType = relatedEntityType;
		this.custId = custId;
		this.kycnId = kycnId;
		this.name = name;
		this.lsName = lsName;
		this.calledByName = calledByName;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.registrationNo = registrationNo;
		this.relatedEntityCountry = relatedEntityCountry;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.pinzip = pinzip;
		this.wardNumber = wardNumber;
		this.toleArea = toleArea;
		this.street = street;
		this.houseNo = houseNo;
		this.unitNumber = unitNumber;
		this.nearestLandmark = nearestLandmark;
		this.latitude = latitude;
		this.longitude = longitude;
		this.phoneNoCountryCode = phoneNoCountryCode;
		this.phoneNoAreaCode = phoneNoAreaCode;
		this.phoneNo = phoneNo;
		this.telexNoCountryCode = telexNoCountryCode;
		this.telexNoAreaCode = telexNoAreaCode;
		this.telexNo = telexNo;
		this.emailId = emailId;
		this.notes = notes;
		this.salutatiton = salutatiton;
		this.change = change;
	}

	public String getRelatedEntityType() {
		return relatedEntityType;
	}

	public void setRelatedEntityType(String relatedEntityType) {
		this.relatedEntityType = relatedEntityType;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public long getKycnId() {
		return kycnId;
	}

	public void setKycnId(long kycnId) {
		this.kycnId = kycnId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLsName() {
		return lsName;
	}

	public void setLsName(String lsName) {
		this.lsName = lsName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getRelatedEntityCountry() {
		return relatedEntityCountry;
	}

	public void setRelatedEntityCountry(String relatedEntityCountry) {
		this.relatedEntityCountry = relatedEntityCountry;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getPinzip() {
		return pinzip;
	}

	public void setPinzip(String pinzip) {
		this.pinzip = pinzip;
	}

	public String getWardNumber() {
		return wardNumber;
	}

	public void setWardNumber(String wardNumber) {
		this.wardNumber = wardNumber;
	}

	public String getToleArea() {
		return toleArea;
	}

	public void setToleArea(String toleArea) {
		this.toleArea = toleArea;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getNearestLandmark() {
		return nearestLandmark;
	}

	public void setNearestLandmark(String nearestLandmark) {
		this.nearestLandmark = nearestLandmark;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhoneNoCountryCode() {
		return phoneNoCountryCode;
	}

	public void setPhoneNoCountryCode(String phoneNoCountryCode) {
		this.phoneNoCountryCode = phoneNoCountryCode;
	}

	public String getPhoneNoAreaCode() {
		return phoneNoAreaCode;
	}

	public void setPhoneNoAreaCode(String phoneNoAreaCode) {
		this.phoneNoAreaCode = phoneNoAreaCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getTelexNoCountryCode() {
		return telexNoCountryCode;
	}

	public void setTelexNoCountryCode(String telexNoCountryCode) {
		this.telexNoCountryCode = telexNoCountryCode;
	}

	public String getTelexNoAreaCode() {
		return telexNoAreaCode;
	}

	public void setTelexNoAreaCode(String telexNoAreaCode) {
		this.telexNoAreaCode = telexNoAreaCode;
	}

	public String getTelexNo() {
		return telexNo;
	}

	public void setTelexNo(String telexNo) {
		this.telexNo = telexNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSalutatiton() {
		return salutatiton;
	}

	public void setSalutatiton(String salutatiton) {
		this.salutatiton = salutatiton;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
