package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KycnRelatedPersonInfo {

	private long id;

	@JsonProperty("person_type")
	private String personType;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("kycn_id")
	private long kycnId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;

	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationDocumentNo;

	@JsonProperty("country")
	private String country;

	@JsonProperty("issuing_authority")
	private String issuingAuthority;

	@JsonProperty("place_of_issue")
	private String placeOfIssue;

	@JsonProperty("issue_date")
	private Date issueDate;

	@JsonProperty("expiry_date")
	private Date expiryDate;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;
	
	@JsonProperty("account_no")
	private String accountNo;
	
	@JsonProperty("is_active")
	private String removedSignatoryStatus;
	
	@JsonProperty("goaml_person_type")
	private String goAMLPersonType;

	public KycnRelatedPersonInfo() {
		super();
	}

	public KycnRelatedPersonInfo(long id, String personType, String custId, long kycnId, String salutation,
			String firstName, String middleName, String lastName, String lsfName, String lsmName, String lslName,
			String secondName, String calledByName, String primaryIdentificationDocumentType,
			String primaryIdentificationDocumentNo, String country, String issuingAuthority, String placeOfIssue,
			Date issueDate, Date expiryDate, String notes, boolean change) {
		super();
		this.id = id;
		this.personType = personType;
		this.custId = custId;
		this.kycnId = kycnId;
		this.salutation = salutation;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.secondName = secondName;
		this.calledByName = calledByName;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
		this.country = country;
		this.issuingAuthority = issuingAuthority;
		this.placeOfIssue = placeOfIssue;
		this.issueDate = issueDate;
		this.expiryDate = expiryDate;
		this.notes = notes;
		this.change = change;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public long getKycnId() {
		return kycnId;
	}

	public void setKycnId(long kycnId) {
		this.kycnId = kycnId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getPrimaryIdentificationDocumentNo() {
		return primaryIdentificationDocumentNo;
	}

	public void setPrimaryIdentificationDocumentNo(String primaryIdentificationDocumentNo) {
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getRemovedSignatoryStatus() {
		return removedSignatoryStatus;
	}

	public void setRemovedSignatoryStatus(String removedSignatoryStatus) {
		this.removedSignatoryStatus = removedSignatoryStatus;
	}

	public String getGoAMLPersonType() {
		return goAMLPersonType;
	}

	public void setGoAMLPersonType(String goAMLPersonType) {
		this.goAMLPersonType = goAMLPersonType;
	}
	
	

}
