package com.trustaml.service.kyc.kycn.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KycnRelationInfoUpdate {

	@JsonProperty("id")
	private long id;

	@JsonProperty("relation_type")
	private String relationType;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("kycn_id")
	private String kycnId;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;

	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationDocumentNo;

	@JsonProperty("expiry_date")
	private Date expiryDate;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("issuing_authority")
	private String issuingAuthority;

	@JsonProperty("place_of_issue")
	private String placeOfIssue;

	@JsonProperty("issue_date")
	private java.sql.Date issueDate;

	@JsonProperty("relation_country")
	private String relationCountry;

	private String maker;
	private String checker;
	private boolean approved;
	private Date updateDate;
	private Date approvedDate;
	private String reason;

	public KycnRelationInfoUpdate(long id, String relationType, String custId, String kycnId, String firstName,
			String middleName, String lastName, String lsfName, String lsmName, String lslName, String secondName,
			String calledByName, String primaryIdentificationDocumentType, String primaryIdentificationDocumentNo,
			Date expiryDate, String notes, String issuingAuthority, String placeOfIssue, Date issueDate,
			String relationCountry, String salutation) {
		super();
		this.id = id;
		this.relationType = relationType;
		this.custId = custId;
		this.kycnId = kycnId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.secondName = secondName;
		this.calledByName = calledByName;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
		this.expiryDate = expiryDate;
		this.notes = notes;
		this.issuingAuthority = issuingAuthority;
		this.placeOfIssue = placeOfIssue;
		this.issueDate = issueDate;
		this.relationCountry = relationCountry;
		this.salutation = salutation;
	}

	public KycnRelationInfoUpdate() {
		super();
		this.id = 0;
		this.relationType = "";
		this.custId = "";
		this.kycnId = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.secondName = "";
		this.calledByName = "";
		this.primaryIdentificationDocumentType = "";
		this.primaryIdentificationDocumentNo = "";
		// this.expiryDate = ;
		this.notes = "";
		this.issuingAuthority = "";
		this.placeOfIssue = "";
		// this.issueDate = issueDate;
		this.relationCountry = "";

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
		this.salutation = "";
	}

	public KycnRelationInfoUpdate(KycnRelationInfo kycnRelationInfo) {
		super();
		this.id = 0;
		this.relationType = kycnRelationInfo.getRelationType();
		this.custId = kycnRelationInfo.getCustId();
		this.kycnId = kycnRelationInfo.getKycnId();
		this.firstName = kycnRelationInfo.getFirstName();
		this.middleName = kycnRelationInfo.getMiddleName();
		this.lastName = kycnRelationInfo.getLastName();
		this.lsfName = kycnRelationInfo.getLsfName();
		this.lsmName = kycnRelationInfo.getLsmName();
		this.lslName = kycnRelationInfo.getLslName();
		this.secondName = kycnRelationInfo.getSecondName();
		this.calledByName = kycnRelationInfo.getCalledByName();
		this.primaryIdentificationDocumentType = kycnRelationInfo.getPrimaryIdentificationDocumentType();
		this.primaryIdentificationDocumentNo = kycnRelationInfo.getPrimaryIdentificationDocumentNo();
		// this.expiryDate = ;
		this.notes = kycnRelationInfo.getNotes();
		this.issuingAuthority = kycnRelationInfo.getIssuingAuthority();
		this.placeOfIssue = kycnRelationInfo.getPlaceOfIssue();
		// this.issueDate = issueDate;
		this.relationCountry = kycnRelationInfo.getRelationCountry();

		this.maker = "";
		this.checker = "";
		this.approved = false;
		this.updateDate = null;
		this.approvedDate = null;
		this.reason = "";
		this.salutation = kycnRelationInfo.getSalutation();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getKycnId() {
		return kycnId;
	}

	public void setKycnId(String kycnId) {
		this.kycnId = kycnId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getPrimaryIdentificationDocumentNo() {
		return primaryIdentificationDocumentNo;
	}

	public void setPrimaryIdentificationDocumentNo(String primaryIdentificationDocumentNo) {
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getIssuingAuthority() {
		return issuingAuthority;
	}

	public void setIssuingAuthority(String issuingAuthority) {
		this.issuingAuthority = issuingAuthority;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public java.sql.Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(java.sql.Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getRelationCountry() {
		return relationCountry;
	}

	public void setRelationCountry(String relationCountry) {
		this.relationCountry = relationCountry;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

}
