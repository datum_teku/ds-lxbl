package com.trustaml.service.mail;

import java.io.File;
import java.io.IOException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailSender {

	public static void sendEmail(String toAddress, String subject, String msgBody)
			throws AddressException, MessagingException {
		if (MailConfiguration.session == null) {
			MailConfiguration.getSession();
		}

		try {
			MimeMessage message = new MimeMessage(MailConfiguration.session);
			message.setFrom(new InternetAddress(MailProperties.MAIL_USERNAME.val()));
			System.out.println(MailProperties.MAIL_USERNAME.val());
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			message.setSubject(subject);
			message.setText(msgBody);
			Transport.send(message);
			System.out.println("Message sent successfully.");
		} finally {

		}
	}

	/**
	 * Html Content Email Sender : Sends email with Custom (Donot Reply) footer
	 * Attached
	 * 
	 * @param toAddress
	 * @param subject
	 * @param msgBody
	 */
	public static void sendHtmlEmail(String toAddress, String subject, String msgBody) {
		if (MailConfiguration.session == null) {
			MailConfiguration.getSession();
		}
		try {
			MimeMessage message = new MimeMessage(MailConfiguration.session);
			message.setFrom(new InternetAddress(MailProperties.MAIL_USERNAME.val()));
			System.out.println(MailProperties.MAIL_USERNAME.val());
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			message.setSubject(subject, "UTF-8");
			message.setContent(msgBody + "<div class=\"footer\"><p>This is test footer</p></div>",
					"text/html; charset=utf-8");

			// Send message
			Transport.send(message);
			System.out.println("Message sent successfully.");
		} catch (Exception e) {
			System.out.println("Message cannot be sent.");
			e.printStackTrace();
		}
	}

	public static void addAttachment(File file, String toAddress, String subject, String msgBody) {
		try {
			MimeMessage message = new MimeMessage(MailConfiguration.session);
			message.setFrom(new InternetAddress(MailProperties.MAIL_USERNAME.val()));
			System.out.println(MailProperties.MAIL_USERNAME.val());
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			message.setSubject(subject, "UTF-8");

			MimeBodyPart attachPart = new MimeBodyPart();
			attachPart.setContent(message, "text/html");

			// multipart.addBodyPart(attachPart);

			// adds attachments

			try {
				attachPart.attachFile(file);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(attachPart);
			message.setContent(multipart);

			// sets the multi-part as e-mail's content
			// message.setContent(multipart);

			// sends the e-mail
			Transport.send(message);
		} catch (Exception e) {
			System.out.println("Message cannot be sent.");
			e.printStackTrace();
		}
	}
}
