package com.trustaml.service.mail;

import javax.mail.Session;
import java.util.Properties;

public class MailConfiguration {

	private static Properties properties;

	public static Session session;

	private static MailAuthenticator mailAuthenticator;

	private static void initialiseProperties() {
		properties = new Properties();
		properties.put("mail.smtp.host", MailProperties.MAIL_HOST.val());
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.socketFactory.port", MailProperties.MAIL_PORT.val());
		properties.put("mail.smtp.PORT", MailProperties.MAIL_PORT.val());
		properties.put("mail.smtp.auth", "true");

		properties.put("mail.smtp.starttls.enable", "true");
	}

	public static Session getSession() {
		if (properties == null) {
			initialiseProperties();
		}
		if (mailAuthenticator == null) {
			mailAuthenticator = new MailAuthenticator();
		}
		if (session == null) {
			session = Session.getInstance(properties, mailAuthenticator);
		}
		return session;
	}
}
