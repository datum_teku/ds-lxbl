package com.trustaml.service.mail;

import java.io.IOException;
import java.util.Properties;

public enum MailProperties {
	MAIL_HOST("email.host"), MAIL_PORT("email.port"), MAIL_USERNAME("email.username"), MAIL_PASSWORD("email.password");

	private final static Properties mailProperties = new Properties();
	private static String emailPropFilename = "email.properties";

	private final String key;

	static {
		try {
			mailProperties.load(MailProperties.class.getClassLoader().getResourceAsStream(emailPropFilename));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	public String val() {
		return mailProperties.getProperty(key, "");
	}

	private MailProperties(String key) {
		this.key = key;
	}
}
