package com.trustaml.service.mail;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class VerifyEmail {

	public static boolean emailVerification(String email) throws AddressException {
		boolean isValid = false;
		try {
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
			isValid = true;
		} finally {

		}
		return isValid;
	}

}
