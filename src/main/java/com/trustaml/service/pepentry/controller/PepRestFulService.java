package com.trustaml.service.pepentry.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.pepentry.dao.PepDao;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Path("/pep")
public class PepRestFulService {

	@Inject
	PepDao pepDao;

	/**
	 * @param jsonString
	 * @return save successful
	 * @description accepts JSON String. Save Pep to database
	 * @error errorCode
	 * @throws SQLException
	 * @throws IOException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response savePep(String jsonString)
			throws SQLException, IOException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty()) {
			pepDao.savePep(jsonString);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return update Successful
	 * @description accepts JSON String and update pep data using pep id
	 * @throws TrustAmlEmptyJSONException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePep(String jsonString)
			throws TrustAmlEmptyJSONException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty()) {
			pepDao.updatePep(jsonString);
			return ResponseReturn.sucess("update successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchData() throws IllegalArgumentException, SQLException, ParseException, JsonParseException,
			JsonMappingException, IOException {
		return ResponseReturn.sucess(pepDao.fectchPepData(new ScreeningOfMigratedData()));
	}
}
