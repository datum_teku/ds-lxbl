package com.trustaml.service.pepentry.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantSanctionType;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.common.service.JsonToPepObject;
import com.trustaml.service.pepentry.model.PepAddressInfo;
import com.trustaml.service.pepentry.model.PepContactInfo;
import com.trustaml.service.pepentry.model.PepFamilyInfo;
import com.trustaml.service.pepentry.model.PepMediaInfo;
import com.trustaml.service.pepentry.model.PepOfficeInfo;
import com.trustaml.service.pepentry.model.PepPersonalInfo;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Stateless
public class PepDao {
	@Inject
	PepDaoImpl pepDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ConstantEntity constantEntity;

	public void savePep(String jsonString) throws SQLException, IOException, ParseException, NoSuchAlgorithmException {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		PepPersonalInfo personalInfo = JsonToPepObject.getPepPersonalInfoFromJsonString(jsonString);
		List<PepOfficeInfo> listPepOfficeInfo = JsonToPepObject.getPepOfficeInfoFromJsonString(jsonString);
		List<PepFamilyInfo> pepFamilyInfoList = JsonToPepObject.getPepFamilyInfoFromJsonNode(jsonString);
		List<PepAddressInfo> listPepAddressInfo = JsonToPepObject.getPepAddressInfoFromJsonString(jsonString);
		List<PepContactInfo> pepContactInfoList = JsonToPepObject.getPepContactInfoFromJsonNode(jsonString);
		List<PepMediaInfo> pepMediaInfoList = JsonToPepObject.getPepMediaInfoFromJsonString(jsonString);
		User user = JsonToPepObject.getUserInfoFromJsonString(jsonString);

		String pepPersonalInfoHashValue = hashCodeGenerator.generateHashValueForPepPersonalInfo(personalInfo, user,
				includeUser);

		long personalInfoId = pepDaoImpl.insertPepPersonalInfo(personalInfo, pepPersonalInfoHashValue);
		personalInfo.setId(personalInfoId);

		String pepPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForPepPersonalInfo(personalInfo,
				user, doNotIncludeUser);
		pepDaoImpl.insertPepPersonalInfoUpdateTable(personalInfo, user, pepPersonalInfoUpdateHashValue);

		if (personalInfoId != 0) {
			/* for pep office info */

			for (PepOfficeInfo pepOfficeInfo : listPepOfficeInfo) {
				String pepOfficeHashValue = hashCodeGenerator.generateHashValueForPepOffice(pepOfficeInfo,
						personalInfoId, user, includeUser);
				String pepOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForPepOffice(pepOfficeInfo,
						personalInfoId, user, doNotIncludeUser);
				pepDaoImpl.insertPepOfficeInfo(pepOfficeInfo, personalInfoId, pepOfficeHashValue);
				pepDaoImpl.insertPepOfficeInfoUpdateTable(pepOfficeInfo, personalInfoId, user,
						pepOfficeUpdateHashValue);
				if (pepOfficeInfo.getElectedRegion() != null) {
					String pepElectedRegionHashValue = hashCodeGenerator.generateHashValueForPepElectedRegion(
							pepOfficeInfo.getElectedRegion(), personalInfoId, user, includeUser);
					String pepElectedRegionUpdateHashValue = hashCodeGenerator.generateHashValueForPepElectedRegion(
							pepOfficeInfo.getElectedRegion(), personalInfoId, user, doNotIncludeUser);
					pepDaoImpl.insertPepElectedRegion(pepOfficeInfo.getElectedRegion(), personalInfoId,
							pepElectedRegionHashValue);
					pepDaoImpl.insertPepElectedRegionUpdateTable(pepOfficeInfo.getElectedRegion(), personalInfoId, user,
							pepElectedRegionUpdateHashValue);
				}
			}
			/* for pep family info */

			if (pepFamilyInfoList.size() > 0) {
				for (PepFamilyInfo familyInfo : pepFamilyInfoList) {

					String pepFamilyHashValue = hashCodeGenerator.generateHashValueForFamily(familyInfo, personalInfoId,
							user, includeUser);
					String pepFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForFamily(familyInfo,
							personalInfoId, user, doNotIncludeUser);
					pepDaoImpl.insertPepFamilyInfo(familyInfo, personalInfoId, pepFamilyHashValue);
					pepDaoImpl.insertPepFamilyInfoUpdateTable(familyInfo, personalInfoId, user,
							pepFamilyUpdateHashValue);
				}
			}

			/* for pep address info */

			if (listPepAddressInfo.size() > 0) {
				for (PepAddressInfo pepAddressInfo : listPepAddressInfo) {
					String pepAddressHashValue = hashCodeGenerator.generateHashValueForPepAddress(pepAddressInfo,
							personalInfoId, user, includeUser);
					String pepAddressUpdateHashValue = hashCodeGenerator.generateHashValueForPepAddress(pepAddressInfo,
							personalInfoId, user, doNotIncludeUser);
					pepDaoImpl.insertPepAddressInfo(pepAddressInfo, personalInfoId, pepAddressHashValue);
					pepDaoImpl.insertPepAddressInfoUpdateTable(pepAddressInfo, personalInfoId, user,
							pepAddressUpdateHashValue);
				}
			}

			/* for pep contact info */

			if (pepContactInfoList.size() > 0) {
				for (PepContactInfo contactInfo : pepContactInfoList) {
					String pepContactHashValue = hashCodeGenerator.generateHashValueForContact(contactInfo,
							personalInfoId, user, includeUser);
					String pepContactUpdateHashValue = hashCodeGenerator.generateHashValueForContact(contactInfo,
							personalInfoId, user, doNotIncludeUser);
					pepDaoImpl.insertPepContactInfo(contactInfo, personalInfoId, pepContactHashValue);
					pepDaoImpl.insertPepContactInfoUpdateTable(contactInfo, personalInfoId, user,
							pepContactUpdateHashValue);
				}
			}

			/* for pep media info */

			if (pepMediaInfoList.size() > 0) {
				for (PepMediaInfo pepMediaInfo : pepMediaInfoList) {
					String pepMedaiHashValue = hashCodeGenerator.generateHashValueForMedia(pepMediaInfo, personalInfoId,
							user, includeUser);
					String pepMedaiUpdateHashValue = hashCodeGenerator.generateHashValueForMedia(pepMediaInfo,
							personalInfoId, user, doNotIncludeUser);
					pepDaoImpl.insertPepMediaInfo(pepMediaInfo, personalInfoId, pepMedaiHashValue);
					pepDaoImpl.insertPepMediaInfoUpdateTable(pepMediaInfo, personalInfoId, user,
							pepMedaiUpdateHashValue);
				}
			}
		}
	}

	public void updatePep(String jsonString)
			throws IOException, SQLException, ParseException, NoSuchAlgorithmException {
		boolean includeUser = true;
		boolean doNotIncludeUser = false;
		PepPersonalInfo personalInfo = JsonToPepObject.getPepPersonalInfoFromJsonString(jsonString);
		User user = JsonToPepObject.getUserInfoFromJsonString(jsonString);
		long personalInfoId = personalInfo.getId();

		// String pepPersonalInfoHashValue =
		// hashCodeGenerator.generateHashValueForPepPersonalInfo(personalInfo,
		// user,
		// includeUser);
		String pepPersonalInfoUpdateHashValue = hashCodeGenerator.generateHashValueForPepPersonalInfo(personalInfo,
				user, doNotIncludeUser);

		pepDaoImpl.updatePersonalInfo(personalInfo, user);
		pepDaoImpl.insertPepPersonalInfoUpdateTable(personalInfo, user, pepPersonalInfoUpdateHashValue);

		/* for pep office info */
		List<PepOfficeInfo> listPepOfficeInfo = JsonToPepObject.getPepOfficeInfoFromJsonString(jsonString);
		for (PepOfficeInfo pepOfficeInfo : listPepOfficeInfo) {
			String pepOfficeHashValue = hashCodeGenerator.generateHashValueForPepOffice(pepOfficeInfo, personalInfoId,
					user, includeUser);
			String pepOfficeUpdateHashValue = hashCodeGenerator.generateHashValueForPepOffice(pepOfficeInfo,
					personalInfoId, user, doNotIncludeUser);
			if (pepOfficeInfo.getId() != 0) {
				if (pepOfficeInfo.isChange()) {
					pepDaoImpl.updatePepOfficeInfo(pepOfficeInfo, personalInfoId, user);
					pepDaoImpl.insertPepOfficeInfoUpdateTable(pepOfficeInfo, personalInfoId, user,
							pepOfficeUpdateHashValue);
				}
				// Code for elected region
			} else {
				pepDaoImpl.insertPepOfficeInfo(pepOfficeInfo, personalInfoId, pepOfficeHashValue);
				pepDaoImpl.insertPepOfficeInfoUpdateTable(pepOfficeInfo, personalInfoId, user,
						pepOfficeUpdateHashValue);

				if (pepOfficeInfo.getElectedRegion() != null) {
					String pepElectedRegionHashValue = hashCodeGenerator.generateHashValueForPepElectedRegion(
							pepOfficeInfo.getElectedRegion(), personalInfoId, user, includeUser);
					String pepElectedRegionUpdateHashValue = hashCodeGenerator.generateHashValueForPepElectedRegion(
							pepOfficeInfo.getElectedRegion(), personalInfoId, user, doNotIncludeUser);
					if (pepOfficeInfo.getElectedRegion().getId() != 0) {
						if (pepOfficeInfo.getElectedRegion().isChange()) {
							// UPDATE CODE GOES HERE
						}
					} else {
						pepDaoImpl.insertPepElectedRegion(pepOfficeInfo.getElectedRegion(), personalInfoId,
								pepElectedRegionHashValue);
						pepDaoImpl.insertPepElectedRegionUpdateTable(pepOfficeInfo.getElectedRegion(), personalInfoId,
								user, pepElectedRegionUpdateHashValue);
					}
				}

			}

		}

		/*
		 * for pep type info PepTypeInfo pepTypeInfo =
		 * JsonToPepObject.getPepTypeInfoFromJsonString(jsonString); if
		 * (pepTypeInfo != null) { if(pepTypeInfo.getId()!=0){
		 * pepDaoImpl.updatePepTypeInfo(pepTypeInfo, personalInfoId, user);
		 * pepDaoImpl.insertPepTypeInfoUpdateTable(pepTypeInfo, personalInfoId,
		 * user); }else{
		 * 
		 * } }
		 */

		/* for pep family info */
		List<PepFamilyInfo> pepFamilyInfoList = JsonToPepObject.getPepFamilyInfoFromJsonNode(jsonString);
		if (pepFamilyInfoList.size() > 0) {
			for (PepFamilyInfo familyInfo : pepFamilyInfoList) {

				String pepFamilyHashValue = hashCodeGenerator.generateHashValueForFamily(familyInfo, personalInfoId,
						user, includeUser);
				String pepFamilyUpdateHashValue = hashCodeGenerator.generateHashValueForFamily(familyInfo,
						personalInfoId, user, doNotIncludeUser);
				if (familyInfo.getId() != 0) {
					if (familyInfo.isChange()) {
						pepDaoImpl.updatePepFamilyInfo(familyInfo, personalInfoId, user);
						pepDaoImpl.insertPepFamilyInfoUpdateTable(familyInfo, personalInfoId, user,
								pepFamilyUpdateHashValue);
					}
				} else {
					pepDaoImpl.insertPepFamilyInfo(familyInfo, personalInfoId, pepFamilyHashValue);
					pepDaoImpl.insertPepFamilyInfoUpdateTable(familyInfo, personalInfoId, user,
							pepFamilyUpdateHashValue);
				}

			}
		}

		List<PepAddressInfo> listPepAddressInfo = JsonToPepObject.getPepAddressInfoFromJsonString(jsonString);

		if (listPepAddressInfo.size() > 0) {
			for (PepAddressInfo pepAddressInfo : listPepAddressInfo) {
				String pepAddressHashValue = hashCodeGenerator.generateHashValueForPepAddress(pepAddressInfo,
						personalInfoId, user, includeUser);
				String pepAddressUpdateHashValue = hashCodeGenerator.generateHashValueForPepAddress(pepAddressInfo,
						personalInfoId, user, doNotIncludeUser);
				if (pepAddressInfo.getId() != 0) {
					if (pepAddressInfo.isChange()) {
						pepDaoImpl.updatePepAddressInfo(pepAddressInfo, personalInfoId, user);
						pepDaoImpl.insertPepAddressInfoUpdateTable(pepAddressInfo, personalInfoId, user,
								pepAddressUpdateHashValue);
					}
				} else {
					pepDaoImpl.insertPepAddressInfo(pepAddressInfo, personalInfoId, pepAddressHashValue);
					pepDaoImpl.insertPepAddressInfoUpdateTable(pepAddressInfo, personalInfoId, user,
							pepAddressUpdateHashValue);
				}

			}
		}

		/* for pep contact info */
		List<PepContactInfo> pepContactInfoList = JsonToPepObject.getPepContactInfoFromJsonNode(jsonString);
		if (pepContactInfoList.size() > 0) {
			for (PepContactInfo contactInfo : pepContactInfoList) {

				String pepContactHashValue = hashCodeGenerator.generateHashValueForContact(contactInfo, personalInfoId,
						user, includeUser);
				String pepContactUpdateHashValue = hashCodeGenerator.generateHashValueForContact(contactInfo,
						personalInfoId, user, doNotIncludeUser);
				if (contactInfo.getId() != 0) {
					if (contactInfo.isChange()) {
						pepDaoImpl.updatePepContactInfo(contactInfo, personalInfoId, user);
						pepDaoImpl.insertPepContactInfoUpdateTable(contactInfo, personalInfoId, user,
								pepContactUpdateHashValue);
					}
				} else {
					pepDaoImpl.insertPepContactInfo(contactInfo, personalInfoId, pepContactHashValue);
					pepDaoImpl.insertPepContactInfoUpdateTable(contactInfo, personalInfoId, user,
							pepContactUpdateHashValue);
				}

			}
		}

		/* for pep media info */
		List<PepMediaInfo> pepMediaInfoList = JsonToPepObject.getPepMediaInfoFromJsonString(jsonString);

		if (pepMediaInfoList.size() > 0) {
			for (PepMediaInfo pepMediaInfo : pepMediaInfoList) {

				String pepMedaiHashValue = hashCodeGenerator.generateHashValueForMedia(pepMediaInfo, personalInfoId,
						user, includeUser);
				String pepMedaiUpdateHashValue = hashCodeGenerator.generateHashValueForMedia(pepMediaInfo,
						personalInfoId, user, doNotIncludeUser);
				if (pepMediaInfo.getId() != 0) {
					if (pepMediaInfo.isChange()) {
						pepDaoImpl.updatePepMediaInfo(pepMediaInfo, personalInfoId, user);
						pepDaoImpl.insertPepMediaInfoUpdateTable(pepMediaInfo, personalInfoId, user,
								pepMedaiUpdateHashValue);
					}
				} else {
					pepDaoImpl.insertPepMediaInfo(pepMediaInfo, personalInfoId, pepMedaiHashValue);
					pepDaoImpl.insertPepMediaInfoUpdateTable(pepMediaInfo, personalInfoId, user,
							pepMedaiUpdateHashValue);
				}

			}
		}
	}

	public List<PepPersonalInfo> fectchPepData(ScreeningOfMigratedData pepDataByDate) throws IllegalArgumentException,
			SQLException, ParseException, JsonParseException, JsonMappingException, IOException {
		String newUniqueKey = "";
		ObjectMapper mapper = new ObjectMapper();
		String existingUniqueKey = "";
		boolean generateNewKey = true;
		existingUniqueKey = pepDaoImpl.getLatestUniqueKey();
		List<PepPersonalInfo> listPersonalInfo = mapper.readValue(pepDaoImpl.fetchPepData(pepDataByDate),
				new TypeReference<List<PepPersonalInfo>>() {
				});
		if (!constantEntity.isCountryCodeSet()) {
			constantEntity.setMapForCountryCode();
			constantEntity.convertListToMapForCountryCode();
		}
		for (PepPersonalInfo personalInfo : listPersonalInfo) {

			String CountryCode = constantEntity.getCodeFromMapCountryCode(personalInfo.getJurisdiction());
			existingUniqueKey = newUniqueKey;
			if (generateNewKey) {
				newUniqueKey = constantEntity.createNewKey(CountryCode, ConstantSanctionType.PEP_TYPE);
				generateNewKey = false;
			} else {
				existingUniqueKey = newUniqueKey;
				newUniqueKey = constantEntity.generateNewKey(existingUniqueKey, CountryCode);

			}
			if (newUniqueKey.equals("NPL0100034021D")) {
				System.out.println("Bangladesh");

			}

			pepDaoImpl.updatePepPersonalInfo(personalInfo, newUniqueKey);
		}
		return listPersonalInfo;

	}

}
