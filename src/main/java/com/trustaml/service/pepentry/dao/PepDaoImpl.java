package com.trustaml.service.pepentry.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.pepentry.model.ElectedRegion;
import com.trustaml.service.pepentry.model.PepAddressInfo;
import com.trustaml.service.pepentry.model.PepContactInfo;
import com.trustaml.service.pepentry.model.PepFamilyInfo;
import com.trustaml.service.pepentry.model.PepMediaInfo;
import com.trustaml.service.pepentry.model.PepOfficeInfo;
import com.trustaml.service.pepentry.model.PepPersonalInfo;
import com.trustaml.service.pepentry.model.PepTypeInfo;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

public class PepDaoImpl {
	@Inject
	DBConnection dbConnection;

	/***************************************************************/
	/*
	 * insertPepPepPersonalInfo method reads data from json string and writes
	 * into list_pep_info with fields
	 * (jurisdiction,document_code,category_code,code,country_prefix,title,
	 * first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
	 * second_name,called_by_name,previous_name,gender,date_of_birth,
	 * place_of_birth,country_of_birth,nationality,notes,updated_date)
	 *
	 */
	/**
	 * @param pepPersonalInfoHashValue
	 * @throws SQLException
	 * @throws ParseException
	 *************************************************************/

	public Long insertPepPersonalInfo(PepPersonalInfo personalInfo, String pepPersonalInfoHashValue)
			throws SQLException, ParseException {
		long personalInfoId = 0;
		String sql = "INSERT INTO list_pep_info (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getPepCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setString(25, pepPersonalInfoHashValue);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				personalInfoId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return personalInfoId;
	}

	/**************** END OF PERSIONAL INFO *******************/

	/***********************************
	 * INSERT METHOD FOR PEP OFFICE INFO
	 * 
	 * @param pepOfficeHashValue
	 * 
	 * @throws SQLException
	 * @throws ParseException
	 ***********************/

	/*
	 * insertPepOfficeInfo writes data into list_pep_office_info with fields
	 * (list_pep_info_id,designation,office_name,
	 * notes,term_year,date_of_appointment)
	 * 
	 */
	public void insertPepOfficeInfo(PepOfficeInfo pepOfficeInfo, long personalInfoId, String pepOfficeHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_pep_office_info (list_pep_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,document_code,hash) VALUES(?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepOfficeInfo.getDesignation());
			ps.setString(3, pepOfficeInfo.getOfficeName());
			ps.setLong(4, pepOfficeInfo.getProlongation());
			ps.setString(5, pepOfficeInfo.getNotes());
			ps.setLong(6, pepOfficeInfo.getTermYears());
			if (!pepOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(pepOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, pepOfficeInfo.getPepOfficeStatus());
			ps.setString(9, pepOfficeInfo.getPepCategory());
			ps.setString(10, pepOfficeHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/***********************************
	 * END OF PEP OFFICE INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP ELECTED REGION INFO
	 * 
	 * @throws SQLException
	 ***********************/
	/*
	 * insertPepElectedRegionInfo writes data into list_pep_elected_region with
	 * fields (list_pep_info_id,elected_district,area_number,party_name )
	 * 
	 */

	/*
	 * @SuppressWarnings("unused") private void
	 * insertPepElectedRegionInfo(PepOfficeInfo pepOfficeInfo, Connection con,
	 * long personalInfoId) throws SQLException {
	 * 
	 * boolean success = true; long pepElectedRegionInfoId = 0; String sql =
	 * "INSERT INTO list_pep_elected_region (list_pep_info_id,elected_district,area_number,party_name) VALUES(?,?,?,?)"
	 * ; Connection connection = null; PreparedStatement ps = null; try {
	 * connection = dbConnection.getConnection(); ps =
	 * connection.prepareStatement(sql); ps.setLong(1, personalInfoId);
	 * ps.setString(2, pepOfficeInfo.getElectedDistrict()); ps.setInt(3,
	 * pepOfficeInfo.getAreaNumber()); ps.setString(4,
	 * pepOfficeInfo.getPartyName()); success = ps.execute(); ResultSet rs =
	 * ps.getGeneratedKeys(); if (rs.next()) { pepElectedRegionInfoId =
	 * rs.getLong(1); } } finally { ps.close(); con.close(); } }
	 */

	/***********************************
	 * END OF PEP ELECTED REGION INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP TYPE INFO
	 * 
	 * @throws SQLException
	 ***********************/
	/*
	 * insertPepTypeInfo writes data into list_pep_type with fields
	 * (list_pep_info_id,pep_type,pep_status,is_active,start_date, end_date,
	 * notes)
	 * 
	 */
	public void insertPepTypeInfo(PepTypeInfo pepTypeInfo, long personalInfoId) throws SQLException {

		String sql = "INSERT INTO list_pep_type (list_pep_info_id,pep_type,pep_level,start_date,end_date,notes)"
				+ "VALUES(?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepTypeInfo.getPepType());
			ps.setString(3, pepTypeInfo.getpepLevel());
			ps.setDate(4, pepTypeInfo.getSqlStartDate());
			ps.setDate(5, pepTypeInfo.getSqlEndDate());
			ps.setString(6, pepTypeInfo.getNotes());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
	}

	/***********************************
	 * END OF PEP TYPE INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP FAMILY INFO
	 * 
	 * @param pepFamilyHashValue
	 * 
	 * @throws SQLException
	 ***********************/
	/*
	 * insertPepFamilyInfo writes data into list_pep_family_info with fields
	 * (list_pep_info_id,relationship_to,first_name,middle_name,
	 * last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name, notes)
	 * 
	 */
	public void insertPepFamilyInfo(PepFamilyInfo pepFamilyInfo, long personalInfoId, String pepFamilyHashValue)
			throws SQLException {
		String sql = "INSERT INTO list_pep_family_info (list_pep_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepFamilyInfo.getRelationshipTo());
			ps.setString(3, pepFamilyInfo.getFirstName());
			ps.setString(4, pepFamilyInfo.getMiddleName());
			ps.setString(5, pepFamilyInfo.getLastName());
			ps.setString(6, pepFamilyInfo.getLsfName());
			ps.setString(7, pepFamilyInfo.getLsmName());
			ps.setString(8, pepFamilyInfo.getLslName());
			ps.setString(9, pepFamilyInfo.getSecondName());
			ps.setString(10, pepFamilyInfo.getCalledByName());
			ps.setString(11, pepFamilyInfo.getNotes());
			ps.setString(12, pepFamilyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(13, pepFamilyHashValue);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
	}

	/***********************************
	 * END OF PEP FAMILY INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP ADDRESS INFO
	 * 
	 * @param pepAddressHashValue
	 * 
	 * @throws SQLException
	 ***********************/
	/*
	 * insertPepAddressInfo writes data into list_pep_address_info with fields
	 * (list_pep_info_id,country,state,province,district,mn_vdc,
	 * town_city_village,tole,street,house_number,notes)
	 * 
	 */
	public void insertPepAddressInfo(PepAddressInfo pepAddressInfo, long personalInfoId, String pepAddressHashValue)
			throws SQLException {

		String sql = "INSERT INTO list_pep_address_info (list_pep_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepAddressInfo.getCountry());
			ps.setString(3, pepAddressInfo.getState());
			ps.setString(4, pepAddressInfo.getProvince());
			ps.setString(5, pepAddressInfo.getDistrict());
			ps.setString(6, pepAddressInfo.getMnVdc());
			ps.setString(7, pepAddressInfo.getTownCityVillage());
			ps.setString(8, pepAddressInfo.getTole());
			ps.setString(9, pepAddressInfo.getStreet());
			ps.setString(10, pepAddressInfo.getHouseNumber());
			ps.setString(11, pepAddressInfo.getNotes());
			ps.setString(12, pepAddressInfo.getCommunicationType());
			ps.setString(13, pepAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, pepAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, pepAddressInfo.getPhoneNo());
			ps.setString(16, pepAddressInfo.getTelexNoCountryCode());
			ps.setString(17, pepAddressInfo.getTelexNoAreaCode());
			ps.setString(18, pepAddressInfo.getTelexNo());
			ps.setString(19, pepAddressInfo.getPagerNoCountryCode());
			ps.setString(20, pepAddressInfo.getPagerNoAreaCode());
			ps.setString(21, pepAddressInfo.getPagerNo());
			ps.setString(22, pepAddressHashValue);
			ps.execute();

		} finally {
			ps.close();
			connection.close();
		}
	}

	/***********************************
	 * END OF PEP ADDRESS INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP CONTACT INFO
	 * 
	 * @param pepContactHashValue
	 * 
	 * @throws SQLException
	 ***********************/
	/*
	 * insertPepContactInfo writes data into list_pep_contact_info with fields
	 * (list_pep_info_id,contact_number,contact_country_prefix,
	 * contact_extension,
	 * contact_type,contact_communication_type,email_address,notes)
	 * 
	 */
	public void insertPepContactInfo(PepContactInfo pepContactInfo, long personalInfoId, String pepContactHashValue)
			throws SQLException {
		String sql = "INSERT INTO list_pep_contact_info"
				+ "(list_pep_info_id,contact_number,contact_country_prefix,contact_extension"
				+ ",contact_communication_type,email_address,notes,hash) VALUES(?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepContactInfo.getContactNumber());
			ps.setString(3, pepContactInfo.getContactCountryPrefix());
			ps.setString(4, pepContactInfo.getContactExtension());
			ps.setString(5, pepContactInfo.getContactCommunicationType());
			ps.setString(6, pepContactInfo.getEmailAddress());
			ps.setString(7, pepContactInfo.getNotes());
			ps.setString(8, pepContactHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/***********************************
	 * END OF PEP CONTACT INFO
	 ***********************************/

	/***********************************
	 * INSERT METHOD FOR PEP MEDIA INFO
	 * 
	 * @param pepMedaiHashValue
	 * 
	 * @throws SQLException
	 * @throws ParseException
	 ***********************/
	/*
	 * insertPepMediaInfo writes data into list_pep_media_info with fields
	 * (list_pep_info_id,media_type,media_content,media_thumnels
	 * ,notes,upload_date)
	 * 
	 */
	public void insertPepMediaInfo(PepMediaInfo pepMediaInfo, long personalInfoId, String pepMedaiHashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO list_pep_media_info (list_pep_info_id,notes,published_date,extension_text,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepMediaInfo.getNotes());

			if (!pepMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(pepMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, pepMediaInfo.getExtensionText());
			ps.setString(5, pepMediaInfo.getMediaType());
			ps.setBytes(6, pepMediaInfo.getMediaContent().getBytes());
			ps.setString(7, pepMediaInfo.getSourceOfInformation());
			ps.setString(8, pepMediaInfo.getSourceType());
			ps.setString(9, pepMedaiHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPepPersonalInfoUpdateTable(PepPersonalInfo personalInfo, User user,
			String pepPersonalInfoUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_pep_info_update (jurisdiction,title,"
				+ "first_name,middle_name,last_name,lsf_name,lsm_name,lsl_name,"
				+ "second_name,called_by_name,previous_name,gender,date_of_birth,"
				+ "place_of_birth,country_of_birth,nationality,notes,document_code,maker,checker,reason,list_pep_info_id,reference_no,approved_by,correspondent,pan_number,citizen_number,approved_date,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getPepCategory());
			ps.setString(19, user.getUserName());
			ps.setString(20, user.getUserName());
			ps.setString(21, "No Reason");
			ps.setLong(22, personalInfo.getId());
			ps.setString(23, personalInfo.getReferenceNo());
			ps.setString(24, personalInfo.getApprovedBy());
			ps.setString(25, personalInfo.getCorrespondent());
			ps.setString(26, personalInfo.getPanNumber());
			ps.setString(27, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(28, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(28, java.sql.Types.DATE);
			}
			ps.setString(29, pepPersonalInfoUpdateHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertPepOfficeInfoUpdateTable(PepOfficeInfo pepOfficeInfo, long personalInfoId, User user,
			String pepOfficeUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_pep_office_info_update"
				+ "(list_pep_info_id,designation,office_name,prolongation,"
				+ "notes,term_year,date_of_appointment,office_status,maker,checker,reason,document_code,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepOfficeInfo.getDesignation());
			ps.setString(3, pepOfficeInfo.getOfficeName());
			ps.setLong(4, pepOfficeInfo.getProlongation());
			ps.setString(5, pepOfficeInfo.getNotes());
			ps.setLong(6, pepOfficeInfo.getTermYears());
			if (!pepOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(pepOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, pepOfficeInfo.getPepOfficeStatus());
			ps.setString(9, user.getUserName());
			ps.setString(10, user.getUserName());
			ps.setString(11, "no reason");
			ps.setString(12, pepOfficeInfo.getPepCategory());
			ps.setString(13, pepOfficeUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertPepTypeInfoUpdateTable(PepTypeInfo pepTypeInfo, long personalInfoId, User user)
			throws SQLException {
		String sql = "INSERT INTO list_pep_type (list_pep_info_id,pep_type,pep_level,start_date,end_date,"
				+ "notes,maker,checker,reason) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepTypeInfo.getPepType());
			ps.setString(3, pepTypeInfo.getpepLevel());
			ps.setDate(4, pepTypeInfo.getSqlStartDate());
			ps.setDate(5, pepTypeInfo.getSqlEndDate());
			ps.setString(6, pepTypeInfo.getNotes());
			ps.setString(7, user.getUserName());
			ps.setString(8, user.getUserName());
			ps.setString(9, "no reason");
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertPepFamilyInfoUpdateTable(PepFamilyInfo familyInfo, long personalInfoId, User user,
			String pepFamilyUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_pep_family_info_update"
				+ "(list_pep_info_id,relationship_to,first_name,middle_name,"
				+ "last_name,lsf_name,lsm_name,lsl_name,second_name,called_by_name,notes,maker,checker,reason,primary_identification_document_number,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, familyInfo.getRelationshipTo());
			ps.setString(3, familyInfo.getFirstName());
			ps.setString(4, familyInfo.getMiddleName());
			ps.setString(5, familyInfo.getLastName());
			ps.setString(6, familyInfo.getLsfName());
			ps.setString(7, familyInfo.getLsmName());
			ps.setString(8, familyInfo.getLslName());
			ps.setString(9, familyInfo.getSecondName());
			ps.setString(10, familyInfo.getCalledByName());
			ps.setString(11, familyInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, familyInfo.getPrimaryIdentificationDocumentNo());
			ps.setString(16, pepFamilyUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPepAddressInfoUpdateTable(PepAddressInfo pepAddressInfo, long personalInfoId, User user,
			String pepAddressUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_pep_address_info_update"
				+ "(list_pep_info_id,country,state,province,district,mn_vdc,"
				+ "town_city_village,tole,street,house_number,notes,maker,checker,reason,communication_type,phone_no_country_code,phone_no_area_code,phone_no,telex_no_country_code,telex_no_area_code,telex_no,pager_no_country_code,pager_no_area_code,pager_no,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepAddressInfo.getCountry());
			ps.setString(3, pepAddressInfo.getState());
			ps.setString(4, pepAddressInfo.getProvince());
			ps.setString(5, pepAddressInfo.getDistrict());
			ps.setString(6, pepAddressInfo.getMnVdc());
			ps.setString(7, pepAddressInfo.getTownCityVillage());
			ps.setString(8, pepAddressInfo.getTole());
			ps.setString(9, pepAddressInfo.getStreet());
			ps.setString(10, pepAddressInfo.getHouseNumber());
			ps.setString(11, pepAddressInfo.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setString(14, "no reason");
			ps.setString(15, pepAddressInfo.getCommunicationType());
			ps.setString(16, pepAddressInfo.getPhoneNoCountryCode());
			ps.setString(17, pepAddressInfo.getPhoneNoAreaCode());
			ps.setString(18, pepAddressInfo.getPhoneNo());
			ps.setString(19, pepAddressInfo.getTelexNoCountryCode());
			ps.setString(20, pepAddressInfo.getTelexNoAreaCode());
			ps.setString(21, pepAddressInfo.getTelexNo());
			ps.setString(22, pepAddressInfo.getPagerNoCountryCode());
			ps.setString(23, pepAddressInfo.getPagerNoAreaCode());
			ps.setString(24, pepAddressInfo.getPagerNo());
			ps.setString(25, pepAddressUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPepContactInfoUpdateTable(PepContactInfo pepContactInfo, long personalInfoId, User user,
			String pepContactUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_pep_contact_info_update"
				+ "(list_pep_info_id,contact_number,contact_country_prefix,contact_extension"
				+ ",contact_communication_type,email_address,notes,maker,checker,reason,hash)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepContactInfo.getContactNumber());
			ps.setString(3, pepContactInfo.getContactCountryPrefix());
			ps.setString(4, pepContactInfo.getContactExtension());
			ps.setString(5, pepContactInfo.getContactCommunicationType());
			ps.setString(6, pepContactInfo.getEmailAddress());
			ps.setString(7, pepContactInfo.getNotes());
			ps.setString(8, user.getUserName());
			ps.setString(9, user.getUserName());
			ps.setString(10, "no reason");
			ps.setString(11, pepContactUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPepMediaInfoUpdateTable(PepMediaInfo pepMediaInfo, long personalInfoId, User user,
			String pepMedaiUpdateHashValue) throws SQLException, ParseException {
		String sql = "INSERT INTO list_pep_media_info_update (list_pep_info_id,notes,published_date,extension_text,maker,checker,reason,media_type,media_content,source_of_information,source_type,hash) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepMediaInfo.getNotes());
			if (!pepMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(3, getBirthDate(pepMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, pepMediaInfo.getExtensionText());
			ps.setString(5, user.getUserName());
			ps.setString(6, user.getUserName());
			ps.setString(7, "no reason");
			ps.setString(8, pepMediaInfo.getMediaType());
			ps.setBytes(9, pepMediaInfo.getMediaContent().getBytes());
			ps.setString(10, pepMediaInfo.getSourceOfInformation());
			ps.setString(11, pepMediaInfo.getSourceType());
			ps.setString(12, pepMedaiUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePersonalInfo(PepPersonalInfo personalInfo, User user) throws SQLException, ParseException {
		String sql = "UPDATE list_pep_info SET jurisdiction=?,title=?,"
				+ "first_name=?,middle_name=?,last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,"
				+ "second_name=?,called_by_name=?,previous_name=?,gender=?,date_of_birth=?,"
				+ "place_of_birth=?,country_of_birth=?,nationality=?,notes=?,document_code=?,reference_no=?,approved_by=?,correspondent=?,pan_number=?,citizen_number=?,approved_date=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, personalInfo.getJurisdiction());
			ps.setString(2, personalInfo.getSalutation());
			ps.setString(3, personalInfo.getFirstName());
			ps.setString(4, personalInfo.getMiddleName());
			ps.setString(5, personalInfo.getLastName());
			ps.setString(6, personalInfo.getLsfName());
			ps.setString(7, personalInfo.getLsmName());
			ps.setString(8, personalInfo.getLslName());
			ps.setString(9, personalInfo.getSecondName());
			ps.setString(10, personalInfo.getCalledByName());
			ps.setString(11, personalInfo.getPreviousName());
			ps.setString(12, personalInfo.getGender());
			if (!personalInfo.getDateOfBirth().equals("")) {
				ps.setDate(13, getBirthDate(personalInfo.getDateOfBirth()));
			} else {
				ps.setNull(13, java.sql.Types.DATE);
			}
			ps.setString(14, personalInfo.getPlaceOfBirth());
			ps.setString(15, personalInfo.getCountryOfBirth());
			ps.setString(16, personalInfo.getNationality());
			ps.setString(17, personalInfo.getNotes());
			ps.setString(18, personalInfo.getPepCategory());
			ps.setString(19, personalInfo.getReferenceNo());
			ps.setString(20, personalInfo.getApprovedBy());
			ps.setString(21, personalInfo.getCorrespondent());
			ps.setString(22, personalInfo.getPanNumber());
			ps.setString(23, personalInfo.getCitizenNumber());
			if (!personalInfo.getApprovedDate().equals("")) {
				ps.setDate(24, getBirthDate(personalInfo.getApprovedDate()));
			} else {
				ps.setNull(24, java.sql.Types.DATE);
			}
			ps.setLong(25, personalInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepOfficeInfo(PepOfficeInfo pepOfficeInfo, long personalInfoId, User user)
			throws SQLException, ParseException {
		String sql = "UPDATE list_pep_office_info set list_pep_info_id=?,designation=?,office_name=?,prolongation=?,"
				+ "notes=?,term_year=?,date_of_appointment=?,office_status=?,document_code=? WHERE id=?";

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepOfficeInfo.getDesignation());
			ps.setString(3, pepOfficeInfo.getOfficeName());
			ps.setLong(4, pepOfficeInfo.getProlongation());
			ps.setString(5, pepOfficeInfo.getNotes());
			ps.setLong(6, pepOfficeInfo.getTermYears());
			if (!pepOfficeInfo.getSqlDateOfAppointment().equals("")) {
				ps.setDate(7, getBirthDate(pepOfficeInfo.getSqlDateOfAppointment()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, pepOfficeInfo.getPepOfficeStatus());
			ps.setString(9, pepOfficeInfo.getPepCategory());
			ps.setLong(10, pepOfficeInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepTypeInfo(PepTypeInfo pepTypeInfo, long personalInfoId, User user) throws SQLException {
		String sql = "UPDATE list_pep_type SET list_pep_info_id=?,pep_type=?,pep_level=?,start_date=?,end_date=?,"
				+ "notes=? WHERE id=?pepOfficeInfo";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepTypeInfo.getPepType());
			ps.setString(3, pepTypeInfo.getpepLevel());
			ps.setDate(4, pepTypeInfo.getSqlStartDate());
			ps.setDate(5, pepTypeInfo.getSqlEndDate());
			ps.setString(6, pepTypeInfo.getNotes());
			ps.setLong(7, pepTypeInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepAddressInfo(PepAddressInfo pepAddressInfo, long personalInfoId, User user)
			throws SQLException {
		String sql = "UPDATE list_pep_address_info set list_pep_info_id=?,country=?,state=?,province=?,district=?,mn_vdc=?,"
				+ "town_city_village=?,tole=?,street=?,house_number=?,notes=?,communication_type=?,phone_no_country_code=?,phone_no_area_code=?,phone_no=?,telex_no_country_code=?,telex_no_area_code=?,telex_no=?,pager_no_country_code=?,pager_no_area_code=?,pager_no=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepAddressInfo.getCountry());
			ps.setString(3, pepAddressInfo.getState());
			ps.setString(4, pepAddressInfo.getProvince());
			ps.setString(5, pepAddressInfo.getDistrict());
			ps.setString(6, pepAddressInfo.getMnVdc());
			ps.setString(7, pepAddressInfo.getTownCityVillage());
			ps.setString(8, pepAddressInfo.getTole());
			ps.setString(9, pepAddressInfo.getStreet());
			ps.setString(10, pepAddressInfo.getHouseNumber());
			ps.setString(11, pepAddressInfo.getNotes());
			ps.setString(12, pepAddressInfo.getCommunicationType());
			ps.setString(13, pepAddressInfo.getPhoneNoCountryCode());
			ps.setString(14, pepAddressInfo.getPhoneNoAreaCode());
			ps.setString(15, pepAddressInfo.getPhoneNo());
			ps.setString(16, pepAddressInfo.getTelexNoCountryCode());
			ps.setString(17, pepAddressInfo.getTelexNoAreaCode());
			ps.setString(18, pepAddressInfo.getTelexNo());
			ps.setString(19, pepAddressInfo.getPagerNoCountryCode());
			ps.setString(20, pepAddressInfo.getPagerNoAreaCode());
			ps.setString(21, pepAddressInfo.getPagerNo());
			ps.setLong(22, pepAddressInfo.getId());
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepFamilyInfo(PepFamilyInfo pepFamilyInfo, long personalInfoId, User user) throws SQLException {
		String sql = "UPDATE list_pep_family_info set list_pep_info_id=?,relationship_to=?,first_name=?,middle_name=?,"
				+ "last_name=?,lsf_name=?,lsm_name=?,lsl_name=?,second_name=?,called_by_name=?,notes=?,primary_identification_document_number=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepFamilyInfo.getRelationshipTo());
			ps.setString(3, pepFamilyInfo.getFirstName());
			ps.setString(4, pepFamilyInfo.getMiddleName());
			ps.setString(5, pepFamilyInfo.getLastName());
			ps.setString(6, pepFamilyInfo.getLsfName());
			ps.setString(7, pepFamilyInfo.getLsmName());
			ps.setString(8, pepFamilyInfo.getLslName());
			ps.setString(9, pepFamilyInfo.getSecondName());
			ps.setString(10, pepFamilyInfo.getCalledByName());
			ps.setString(11, pepFamilyInfo.getNotes());
			ps.setString(12, pepFamilyInfo.getPrimaryIdentificationDocumentNo());
			ps.setLong(13, pepFamilyInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepContactInfo(PepContactInfo pepContactInfo, long personalInfoId, User user)
			throws SQLException {
		String sql = "UPDATE list_pep_contact_info"
				+ " set list_pep_info_id=?,contact_number=?,contact_country_prefix=?,contact_extension=?"
				+ ",contact_communication_type=?,email_address=?,notes=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepContactInfo.getContactNumber());
			ps.setString(3, pepContactInfo.getContactCountryPrefix());
			ps.setString(4, pepContactInfo.getContactExtension());
			ps.setString(5, pepContactInfo.getContactCommunicationType());
			ps.setString(6, pepContactInfo.getEmailAddress());
			ps.setString(7, pepContactInfo.getNotes());
			ps.setLong(8, pepContactInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void updatePepMediaInfo(PepMediaInfo pepMediaInfo, long personalInfoId, User user)
			throws SQLException, ParseException {

		String sql = "UPDATE list_pep_media_info set list_pep_info_id=?,media_type=?,media_content=?"
				+ ",notes=?,published_date=?,extension_text=?,source_of_information=?,source_type=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, pepMediaInfo.getMediaType());
			ps.setBytes(3, pepMediaInfo.getMediaContent().getBytes());
			ps.setString(4, pepMediaInfo.getNotes());
			if (!pepMediaInfo.getPublishedDate().equals("")) {
				ps.setDate(5, getBirthDate(pepMediaInfo.getPublishedDate()));
			} else {
				ps.setNull(5, java.sql.Types.DATE);
			}
			ps.setString(6, pepMediaInfo.getExtensionText());
			ps.setString(7, pepMediaInfo.getSourceOfInformation());
			ps.setString(8, pepMediaInfo.getSourceType());
			ps.setLong(9, pepMediaInfo.getId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/*
	 * private static java.sql.Date new
	 * java.sql.Date(Calendar.getInstance().getTimeInMillis()) {
	 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 * java.util.Date today = new java.util.Date(); return new
	 * java.sql.Date(today.getTime()); }
	 */

	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	public void insertPepElectedRegion(ElectedRegion electedRegion, long personalInfoId,
			String pepElectedRegionHashValue) throws SQLException {
		String sql = "INSERT INTO list_pep_elected_region"
				+ "(list_pep_info_id,elected_district,area_number,party_name,notes,hash) VALUES(?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, electedRegion.getElectedDistrict());
			ps.setLong(3, electedRegion.getAreaNumber());
			ps.setString(4, electedRegion.getPartyName());
			ps.setString(5, electedRegion.getNotes());
			ps.setString(6, pepElectedRegionHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPepElectedRegionUpdateTable(ElectedRegion electedRegion, long personalInfoId, User user,
			String pepElectedRegionUpdateHashValue) throws SQLException {
		String sql = "INSERT INTO list_pep_elected_region_update"
				+ "(list_pep_info_id,elected_district,area_number,party_name,notes,maker,checker,hash) VALUES(?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, personalInfoId);
			ps.setString(2, electedRegion.getElectedDistrict());
			ps.setLong(3, electedRegion.getAreaNumber());
			ps.setString(4, electedRegion.getPartyName());
			ps.setString(5, electedRegion.getNotes());
			ps.setString(6, user.getUserName());
			ps.setString(7, user.getUserName());
			ps.setString(8, pepElectedRegionUpdateHashValue);
			ps.execute();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public String getLatestUniqueKey() throws SQLException {
		String uniquekey = null;
		String sql = "SELECT key from list_pep_info order by id desc limit 1";
		Connection con = null;
		Statement psmt = null;
		try {
			con = dbConnection.getConnection();
			psmt = con.createStatement();
			ResultSet rs = psmt.executeQuery(sql);
			while (rs.next()) {
				uniquekey = rs.getString(1);
			}

		} finally {
			psmt.close();
			con.close();
		}
		return uniquekey;
	}

	public String fetchPepData(ScreeningOfMigratedData pepDataByDate) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		// String dateRange = adverseDataByDate.getDateRange();
		// String dateFrom = dateRange.substring(0,
		// dateRange.indexOf("to")).trim();
		// String dateTo = dateRange.substring(dateRange.indexOf("to") + 2,
		// dateRange.length()).trim();
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_sanction_list(?,?)}");
			// stmt.setDate(1, getDate(dateFrom));
			// stmt.setDate(2, getDate(dateTo));
			stmt.setInt(1, 1);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();
		}
		return result;
	}

	public void updatePepPersonalInfo(PepPersonalInfo personalInfo, String newUniqueKey) throws SQLException {
		String sql = "UPDATE list_pep_info SET key=? WHERE id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, newUniqueKey);
			ps.setLong(2, personalInfo.getId());
			ps.executeUpdate();

		} finally {
			ps.close();
			con.close();
		}

	}

}