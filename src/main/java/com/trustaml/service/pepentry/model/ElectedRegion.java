package com.trustaml.service.pepentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ElectedRegion {

	private long id;

	@JsonProperty("elected_district")
	private String electedDistrict;

	@JsonProperty("area_number")
	private long areaNumber;

	@JsonProperty("party_name")
	private String partyName;

	@JsonProperty("elected_notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	public ElectedRegion() {
		super();
	}

	public ElectedRegion(long id, String electedDistrict, long areaNumber, String partyName, String notes,
			boolean change) {
		super();
		this.id = id;
		this.electedDistrict = electedDistrict;
		this.areaNumber = areaNumber;
		this.partyName = partyName;
		this.notes = notes;
		this.change = change;
	}

	public String getElectedDistrict() {
		return electedDistrict;
	}

	public void setElectedDistrict(String electedDistrict) {
		this.electedDistrict = electedDistrict;
	}

	public long getAreaNumber() {
		return areaNumber;
	}

	public void setAreaNumber(long areaNumber) {
		this.areaNumber = areaNumber;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
