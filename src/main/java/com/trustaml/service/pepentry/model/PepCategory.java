package com.trustaml.service.pepentry.model;

public enum PepCategory {
	L01("Government salaried"), L02("Industrial sector"), L03("Monetory sector"), L04("Business sector"), L05(
			"Serive sector"), L06("Social sector"), L07("Public welfare sector"), L08("Politicians"), L09("Ambassador");

	private final String pepCategory;

	PepCategory(String pepCategory) {
		this.pepCategory = pepCategory;
	}

	public String getPepCategory() {
		return this.pepCategory;
	}
}
