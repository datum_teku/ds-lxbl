package com.trustaml.service.pepentry.model;

public class PepCategoryCount {
	private PepCategory pepCategory;
	private int count;
	public PepCategoryCount(PepCategory pepCategory, int count) {
		super();
		this.pepCategory = pepCategory;
		this.count = count;
	}
	public PepCategory getPepCategory() {
		return pepCategory;
	}
	public void setPepCategory(PepCategory pepCategory) {
		this.pepCategory = pepCategory;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
