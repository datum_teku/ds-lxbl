package com.trustaml.service.pepentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PepContactInfo {

	private long id;

	@JsonProperty("contact_number")
	private String contactNumber;

	@JsonProperty("contact_country_prefix")
	private String contactCountryPrefix;

	@JsonProperty("contact_extension")
	private String contactExtension;

	@JsonProperty("communication_type")
	private String contactCommunicationType;

	@JsonProperty("email_address")
	private String emailAddress;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	public PepContactInfo(String contactNumber, String contactCountryPrefix, String contactExtension,
			String contactCommunicationType, String emailAddress, String notes, boolean change) {
		super();
		this.contactNumber = contactNumber;
		this.contactCountryPrefix = contactCountryPrefix;
		this.contactExtension = contactExtension;
		this.contactCommunicationType = contactCommunicationType;
		this.emailAddress = emailAddress;
		this.notes = notes;
		this.change = change;
	}

	public PepContactInfo() {
		super();
		this.contactNumber = "";
		this.contactCountryPrefix = "";
		this.contactExtension = "";
		this.contactCommunicationType = "";
		this.emailAddress = "";
		this.notes = "";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactCountryPrefix() {
		return contactCountryPrefix;
	}

	public void setContactCountryPrefix(String contactCountryPrefix) {
		this.contactCountryPrefix = contactCountryPrefix;
	}

	public String getContactExtension() {
		return contactExtension;
	}

	public void setContactExtension(String contactExtension) {
		this.contactExtension = contactExtension;
	}

	public String getContactCommunicationType() {
		return contactCommunicationType;
	}

	public void setContactCommunicationType(String contactCommunicationType) {
		this.contactCommunicationType = contactCommunicationType;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

}
