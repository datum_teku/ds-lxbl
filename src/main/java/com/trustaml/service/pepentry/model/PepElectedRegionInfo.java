package com.trustaml.service.pepentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PepElectedRegionInfo {
	
	private long id;
	
	@JsonProperty("elected-district")
	private String electedDistrict;
	
	@JsonProperty("area-number")
	private int areaNumber;
	
	@JsonProperty("party-name")
	private String  partyName;
	
	@JsonProperty("notes")
	private String notes;
	
	public PepElectedRegionInfo(String electedDistrict, int areaNumber, String partyName, String notes) {
		super();
		this.electedDistrict = electedDistrict;
		this.areaNumber = areaNumber;
		this.partyName = partyName;
		this.notes = notes;
	}
	
	public PepElectedRegionInfo() {
		super();
		this.electedDistrict ="";
		this.areaNumber =0;
		this.partyName ="";
		this.notes ="";
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getElectedDistrict() {
		return electedDistrict;
	}
	public void setElectedDistrict(String electedDistrict) {
		this.electedDistrict = electedDistrict;
	}
	public int getAreaNumber() {
		return areaNumber;
	}
	public void setAreaNumber(int areaNumber) {
		this.areaNumber = areaNumber;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	

}
