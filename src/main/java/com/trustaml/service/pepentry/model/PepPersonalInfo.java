package com.trustaml.service.pepentry.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PepPersonalInfo {

	private long id;
	@JsonProperty("jurisdiction")
	private String jurisdiction;

	@JsonProperty("pep_category")
	private String pepCategory;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("called_by_name")
	private String calledByName;

	@JsonProperty("previous_name")
	private String previousName;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("date_of_birth")
	private String dateOfBirth;

	@JsonProperty("place_of_birth")
	private String placeOfBirth;

	@JsonProperty("country_of_birth")
	private String countryOfBirth;

	@JsonProperty("nationality")
	private String nationality;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("change")
	private boolean change;

	@JsonProperty("reference_no")
	private String referenceNo;

	@JsonProperty("approved_by")
	private String approvedBy;

	@JsonProperty("correspondent")
	private String correspondent;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("citizen_number")
	private String citizenNumber;

	@JsonProperty("approved_date")
	private String approvedDate;

	public PepPersonalInfo(long id, String jurisdiction, String pepCategory, String salutation, String firstName,
			String middleName, String lastName, String lsfName, String lsmName, String lslName, String secondName,
			String calledByName, String previousName, String gender, String dateOfBirth, String placeOfBirth,
			String countryOfBirth, String nationality, String notes, boolean change, String referenceNo,
			String approvedBy, String correspondent, String panNumber, String citizenNumber, String approvedDate) {
		super();
		this.id = id;
		this.jurisdiction = jurisdiction;
		this.pepCategory = pepCategory;
		this.salutation = salutation;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.secondName = secondName;
		this.calledByName = calledByName;
		this.previousName = previousName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.placeOfBirth = placeOfBirth;
		this.countryOfBirth = countryOfBirth;
		this.nationality = nationality;
		this.notes = notes;
		this.change = change;
		this.referenceNo = referenceNo;
		this.approvedBy = approvedBy;
		this.correspondent = correspondent;
		this.panNumber = panNumber;
		this.citizenNumber = citizenNumber;
		this.approvedDate = approvedDate;
	}

	public PepPersonalInfo() {
		super();
		this.jurisdiction = "";
		this.jurisdiction = "";
		this.pepCategory = "";
		this.salutation = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.secondName = "";
		this.calledByName = "";
		this.previousName = "";
		this.gender = "";
		this.dateOfBirth = null;
		this.placeOfBirth = "";
		this.countryOfBirth = "";
		this.nationality = "";
		this.notes = "";

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getPepCategory() {
		return pepCategory;
	}

	public void setPepCategory(String pepCategory) {
		this.pepCategory = pepCategory;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCalledByName() {
		return calledByName;
	}

	public void setCalledByName(String calledByName) {
		this.calledByName = calledByName;
	}

	public String getPreviousName() {
		return previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getCorrespondent() {
		return correspondent;
	}

	public void setCorrespondent(String correspondent) {
		this.correspondent = correspondent;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getCitizenNumber() {
		return citizenNumber;
	}

	public void setCitizenNumber(String citizenNumber) {
		this.citizenNumber = citizenNumber;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

}
