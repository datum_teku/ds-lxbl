package com.trustaml.service.pepentry.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PepTypeInfo {

	private long id;

	@JsonProperty("pep_type")
	private String pepType;

	@JsonProperty("pep_level")
	private String pepLevel;

	@JsonProperty("start_date")
	private String startDate;

	@JsonProperty("end_date")
	private String endDate;

	@JsonProperty("notes")
	private String notes;

	public PepTypeInfo(String pepType, String pepLevel, String startDate, String endDate, String notes) {
		super();
		this.pepType = pepType;
		this.pepLevel = pepLevel;
		this.startDate = startDate;
		this.endDate = endDate;
		this.notes = notes;
	}

	public PepTypeInfo() {
		super();
		this.pepType = "";
		this.pepLevel = "";
		this.startDate = "";
		this.endDate = "";
		this.notes = "";
	}

	public Date getSqlStartDate() {

		String startDate = getStartDate();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date sqlDate = null;
		try {
			java.util.Date date = format.parse(startDate);
			sqlDate = new Date(date.getTime());
		} catch (ParseException e) {

		}
		return sqlDate;

	}

	public Date getSqlEndDate() {

		String endDate = getEndDate();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date sqlDate = null;
		try {
			java.util.Date date = format.parse(endDate);
			sqlDate = new Date(date.getTime());
		} catch (ParseException e) {

		}
		return sqlDate;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPepType() {
		return pepType;
	}

	public void setPepType(String pepType) {
		this.pepType = pepType;
	}

	public String getpepLevel() {
		return pepLevel;
	}

	public void setpepLevel(String pepLevel) {
		this.pepLevel = pepLevel;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
