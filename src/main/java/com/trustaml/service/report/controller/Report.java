package com.trustaml.service.report.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.report.dao.ReportDao;

@Path("/report")
public class Report {

	@Inject
	ReportDao reportDao;

	/**
	 * @param json
	 * @return save successful
	 * @description accepts JSON string and save report to database
	 * @throws NumberFormatException
	 * @throws SQLException
	 * @throws IOException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReport(String json)
			throws NumberFormatException, SQLException, IOException, TrustAmlEmptyJSONException, ParseException {
		if (!json.isEmpty()) {
			// riskDao.saveRisk(json);
			reportDao.saveReport(json);
			return ResponseReturn.sucess("save successful");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param code
	 * @return
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRiskTable(@QueryParam("code") String code) throws SQLException, TrustAmlEmptyJSONException {
		if (!code.isEmpty()) {
			return ResponseReturn.sucess(reportDao.getReportBy(code));
		} else {
			throw new TrustAmlEmptyJSONException("code is empty");

		}
	}
}
