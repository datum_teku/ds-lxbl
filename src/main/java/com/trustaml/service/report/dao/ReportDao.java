package com.trustaml.service.report.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.report.model.Report;

@Stateless
public class ReportDao {

	@Inject
	ReportDaoImpl reportDaoImpl;

	public void saveReport(String json)
			throws JsonParseException, JsonMappingException, IOException, SQLException, ParseException {
		Report report = new ObjectMapper().readValue(json, Report.class);
		reportDaoImpl.saveReport(report);

	}

	public Object getReportBy(String code) throws SQLException {
		// TODO Auto-generated method stub
		return reportDaoImpl.getReportBy(code);
	}

}
