package com.trustaml.service.report.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.report.model.Report;

public class ReportDaoImpl {

	@Inject
	DBConnection dbConnection;

	public void saveReport(Report report) throws SQLException, ParseException {
		String sql = "INSERT INTO nrb_d19 (data_id,type,description,bs_ref,content,maker) VALUES(?,?,?,?,?,?)";
		Connection con = dbConnection.getConnection();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, report.getDataId());
		ps.setString(2, report.getType());
		ps.setString(3, report.getDescription());

		if (!report.getBsRef().equals("")) {
			ps.setDate(4, getDate(report.getBsRef()));
		} else {
			ps.setNull(4, java.sql.Types.DATE);
		}
		System.out.println("" + report.getContent() + "and tehe he he " + report.getContent());
		ps.setString(5, report.getContent().toString());
		ps.setString(6, report.getUser().getUserName());

		ps.execute();
		ps.close();
		con.close();
	}

	private java.sql.Date getDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	public String getReportBy(String code) throws SQLException {
		Connection conn = dbConnection.getConnection();
		String result = "";
		CallableStatement stmt = conn.prepareCall("{call report_by_month(?,?)}");
		stmt.setString(1, code);
		stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
		stmt.executeUpdate();
		result = stmt.getString(2);
		conn.close();
		stmt.close();
		return result;
	}

}
