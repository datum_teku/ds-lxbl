package com.trustaml.service.report.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.trustaml.service.common.dto.User;

public class Report {

	@JsonProperty("data-id")
	private String dataId;

	@JsonProperty("type")
	private String type;

	@JsonProperty("description")
	private String description;

	@JsonProperty("bs-ref")
	private String bsRef;

	@JsonProperty("content")
	private JsonNode content;

	@JsonProperty("user")
	private User user;

	public Report() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Report(String dataId, String type, String description, String bsRef, JsonNode content, User user) {
		super();
		this.dataId = dataId;
		this.type = type;
		this.description = description;
		this.bsRef = bsRef;
		this.content = content;
		this.user = user;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBsRef() {
		return bsRef;
	}

	public void setBsRef(String bsRef) {
		this.bsRef = bsRef;
	}

	public JsonNode getContent() {
		return content;
	}

	public void setContent(JsonNode content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
