package com.trustaml.service.risk.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.risk.dao.RiskDao;

@Path("/calculate-risk")
public class CalculateRiskRestfulService {

	@Inject
	RiskDao riskDao;

	@Inject
	ConstantEntity constantEntity;

	/**
	 * @param screeningId
	 * @return object of risk match case
	 * @description uses procedure, return match object of risk using screening
	 *              id of natural
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@GET
	@Path("/natural/screening")
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateRiskNatural(@QueryParam("id") Long screeningId)
			throws SQLException, JsonParseException, JsonMappingException, IOException {

		if (screeningId == 0) {
			throw new NullPointerException();
		} else {
			JSONObject obj = new JSONObject();
			obj.put("data", 1);
			return ResponseReturn.sucess(riskDao.calculateRiskNatural(screeningId));
		}

	}

	/**
	 * @param screeningId
	 * @return object of risk match case
	 * @description uses procedure, return match object of risk using screening
	 *              id of legal
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@GET
	@Path("/legal/screening")
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateRiskLegal(@QueryParam("id") Long screeningId)
			throws SQLException, JsonParseException, JsonMappingException, IOException {

		if (screeningId == 0) {
			throw new NullPointerException();
		} else {
			return ResponseReturn.sucess(riskDao.calculateRiskLegal(screeningId));
		}

	}

	/**
	 * @param kycnId
	 * @return object of risk match case
	 * @description uses procedure, return match object of risk using kycn id
	 *              kyc natural
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */

	@GET
	@Path("/kycn")
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateKYCNRiskNatural(@QueryParam("id") Long kycnId)
			throws SQLException, JsonParseException, JsonMappingException, IOException {

		if (kycnId == 0) {
			throw new NullPointerException();
		} else {
			return ResponseReturn.sucess(riskDao.calculateKYCNRiskNatural(kycnId));
		}

	}

	/**
	 * @param kyclId
	 * @return object of risk match case
	 * @description uses procedure, return match object of risk using kycn id
	 *              kyc legal
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */

	@GET
	@Path("/kycl")
	@Produces(MediaType.APPLICATION_JSON)
	public Response calculateKYCNRiskLegal(@QueryParam("id") Long kyclId)
			throws SQLException, JsonParseException, JsonMappingException, IOException {

		if (kyclId == 0) {
			throw new NullPointerException();
		} else {
			return ResponseReturn.sucess(riskDao.calculateKYCNRiskLegal(kyclId));
		}

	}
}
