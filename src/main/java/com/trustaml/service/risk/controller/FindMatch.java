package com.trustaml.service.risk.controller;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.risk.dao.RiskDao;

@Path("/find-match")
public class FindMatch {

	@Inject
	RiskDao riskDao;

	@GET
	@Path("/natural")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findMatchNatural(@QueryParam("name") String name, @QueryParam("matchlevel") float matchLevel)
			throws SQLException {
		if (name.isEmpty()) {
			throw new NullPointerException();
		}
		if (matchLevel == 0) {
			throw new NullPointerException();
		} else {
			return ResponseReturn.sucess(riskDao.findMatchNatural(name, matchLevel));
		}

	}

	@GET
	@Path("/legal")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findMatchRiskLegal(@QueryParam("name") String name, @QueryParam("matchlevel") float matchLevel)
			throws SQLException {
		if (name.isEmpty()) {
			throw new NullPointerException();
		}
		if (matchLevel == 0) {
			throw new NullPointerException();
		} else {
			return ResponseReturn.sucess(riskDao.findMatchLegal(name, matchLevel));
		}
	}

}
