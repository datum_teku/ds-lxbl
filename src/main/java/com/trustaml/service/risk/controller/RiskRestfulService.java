package com.trustaml.service.risk.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.risk.dao.RiskDao;

@Path("/risk")
public class RiskRestfulService {
	@Inject
	RiskDao riskDao;

	@GET
	@Path("/all-table")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRiskTable() throws SQLException {
		return ResponseReturn.sucess(riskDao.getAllRiskTable());

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{table}")
	public Response getRisk(@PathParam("table") String table) throws SQLException {
		if (!table.isEmpty()) {
			riskDao.getRisk(table);
			return ResponseReturn.sucess(riskDao.getRisk(table));
		} else {
			return ResponseReturn.failure("table name is empty");
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateRisk(String json)
			throws NumberFormatException, SQLException, IOException, TrustAmlEmptyJSONException, NoSuchAlgorithmException {
		if (!json.isEmpty()) {
			riskDao.updateRisk(json);
			return ResponseReturn.sucess("update success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveRisk(String json)
			throws NumberFormatException, SQLException, IOException, TrustAmlEmptyJSONException {
		if (!json.isEmpty()) {
			riskDao.saveRisk(json);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

}
