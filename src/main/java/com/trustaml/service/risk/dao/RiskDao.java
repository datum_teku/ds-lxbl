package com.trustaml.service.risk.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.risk.model.BankService;
import com.trustaml.service.risk.model.BusinessEnvironment;
import com.trustaml.service.risk.model.Card;
import com.trustaml.service.risk.model.Country;
import com.trustaml.service.risk.model.CrimeEnvironment;
import com.trustaml.service.risk.model.Delivery;
import com.trustaml.service.risk.model.Industry;
import com.trustaml.service.risk.model.ListRisk;
import com.trustaml.service.risk.model.ObtainedRisk;
import com.trustaml.service.risk.model.Occupational;
import com.trustaml.service.risk.model.Pep;
import com.trustaml.service.risk.model.ProductDeposit;
import com.trustaml.service.risk.model.ProductLoan;
import com.trustaml.service.risk.model.Remittance;
import com.trustaml.service.risk.model.RiskMapper;
import com.trustaml.service.risk.util.JsonStringToRiskObject;

@Stateless
public class RiskDao {
	@Inject
	RiskDaoImpl riskDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ConstantEntity constantEntity;

	public List<Object> getRisk(String name) throws SQLException {
		return riskDaoImpl.getRisk(name);
	}

	public void updateRisk(String table, Integer risk, Long id) throws SQLException {
		String hashValue = hashCodeGenerator.generateHashValueForRisk(risk);
		riskDaoImpl.updateRisk(table, risk, id, hashValue);
	}

	public String getRiskTable() throws SQLException {
		return riskDaoImpl.getRiskTable();
	}

	public void insertRiskUpdate(String table, String data, Integer risk, String reason, User user)
			throws JsonParseException, SQLException, IOException, NoSuchAlgorithmException {

		String sql = "";
		if (table.equals("risk_bank_product_cards")) {
			sql = "insert into risk_bank_product_cards_update (risk_bank_product_cards_id, document_code, "
					+ "product_code, name, card_type, domestic, international, maximum_threshold, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			Card card = new ObjectMapper().readValue(data, Card.class);
			String hashValueForCard = hashCodeGenerator.generateHashValueForRiskCard(card, risk, reason, user);
			riskDaoImpl.insertPCRiskUpdate(sql, card, risk, reason, user, hashValueForCard);

		} else if (table.equals("risk_bank_product_deposit")) {
			sql = "insert into risk_bank_product_deposit_update (risk_bank_product_deposite_id, document_code, "
					+ "product_code,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml,local_currency, is_foreign_currency, foreign_currency, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ProductDeposit deposit = new ObjectMapper().readValue(data, ProductDeposit.class);
			String hashValueForDeposit = hashCodeGenerator.generateHashValueForRiskProductDeposit(deposit, risk, reason,
					user);
			riskDaoImpl.insertPDRiskUpdate(sql, deposit, risk, reason, user, hashValueForDeposit);

		} else if (table.equals("risk_bank_product_loan")) {
			sql = "insert into risk_bank_product_loan_update(risk_bank_product_loan_id, document_code, "
					+ "product_code,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ProductLoan productLoan = new ObjectMapper().readValue(data, ProductLoan.class);
			String hashValueForProductLoan = hashCodeGenerator.generateHashValueForRiskProductLoan(productLoan, risk,
					reason, user);
			riskDaoImpl.insertPLUpdate(sql, productLoan, risk, reason, user, hashValueForProductLoan);

		} else if (table.equals("risk_bank_services")) {
			sql = "insert into risk_bank_services_update(risk_bank_services_id, document_code, "
					+ "service_code,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			BankService bankService = new ObjectMapper().readValue(data, BankService.class);
			String hashValueForRisk = hashCodeGenerator.generateHashValueForRiskBankService(bankService, risk, reason,
					user);
			riskDaoImpl.insertBSRiskUpdate(sql, bankService, risk, reason, user, hashValueForRisk);

		} else if (table.equals("risk_country")) {
			sql = "insert into risk_country_update(risk_country_id, country_code_3a, "
					+ "country,name, numeric_code, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Country country = new ObjectMapper().readValue(data, Country.class);
			String hashValueForCountry = hashCodeGenerator.generateHashValueForCountry(country, risk, reason, user);
			riskDaoImpl.insertCountryRiskUpdate(sql, country, risk, reason, user, hashValueForCountry);

		} else if (table.equals("risk_delivery")) {
			sql = "insert into risk_delivery_update(risk_delivery_id, document_code, "
					+ "product_code,delivery_action,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Delivery delivery = new ObjectMapper().readValue(data, Delivery.class);
			String hashValueForDelivery = hashCodeGenerator.generateHashValueForDelivery(delivery, risk, reason, user);
			riskDaoImpl.insertDeliveryUpdate(sql, delivery, risk, reason, user, hashValueForDelivery);

		} else if (table.equals("risk_domestic_business_environment")) {
			sql = "insert into risk_domestic_business_environment_update(risk_domestic_business_environment_id, document_code, "
					+ "group_code,code,location,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			BusinessEnvironment businessEnvironment = new ObjectMapper().readValue(data, BusinessEnvironment.class);
			String hashValueForBusinessEnvironment = hashCodeGenerator
					.generateHashValueForRiskBusinessEnvironment(businessEnvironment, risk, reason, user);
			riskDaoImpl.insertBERiskUpdate(sql, businessEnvironment, risk, reason, user,
					hashValueForBusinessEnvironment);

		} else if (table.equals("risk_domestic_crime_environment")) {
			sql = "insert into risk_domestic_crime_environment_update(risk_domestic_crime_environment_id, document_code, "
					+ "group_code,code,location,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			CrimeEnvironment crimeEnvironment = new ObjectMapper().readValue(data, CrimeEnvironment.class);
			String hashValueForCrimeEnvironment = hashCodeGenerator
					.generateHashValueForRiskCrimeEnvironment(crimeEnvironment, risk, reason, user);
			riskDaoImpl.insertCERiskUpdate(sql, crimeEnvironment, risk, reason, user, hashValueForCrimeEnvironment);

		} else if (table.equals("risk_industry")) {
			sql = "insert into risk_industry_update(risk_industry_type_id, document_code, "
					+ "major_group_code,broad_group_code,group_detail_code,detail_code,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Industry industry = new ObjectMapper().readValue(data, Industry.class);
			String hashValueForIndustry = hashCodeGenerator.generateHashValueForRiskIndustry(industry, risk, reason,
					user);
			riskDaoImpl.insertIndustryUpdate(sql, industry, risk, reason, user, hashValueForIndustry);

		} else if (table.equals("risk_occupational")) {
			sql = "insert into risk_occupational_update(risk_occupational_id, document_code, "
					+ "major_group_code,minor_group_code,broad_group_code,detail_code,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Occupational occupational = new ObjectMapper().readValue(data, Occupational.class);
			String hashValueForOccupation = hashCodeGenerator.generateHashValueForRiskOccupational(occupational, risk,
					reason, user);
			riskDaoImpl.insertOccupationalUpdate(sql, occupational, risk, reason, user, hashValueForOccupation);

		} else if (table.equals("risk_pep")) {
			sql = "insert into risk_pep_update(risk_pep_id, category, " + "level,name, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Pep pep = new ObjectMapper().readValue(data, Pep.class);
			String hashValueForPep = hashCodeGenerator.generateHashValueForRiskPep(pep, risk, reason, user);
			riskDaoImpl.insertPepUpdate(sql, pep, risk, reason, user, hashValueForPep);

		} else if (table.equals("risk_bank_product_remittance")) {
			sql = "insert into risk_bank_product_remittance_update(risk_bank_product_remittance_id, document_code, "
					+ "product_code,name,inward_outward, is_active, "
					+ "active_date, disabled_date, risk_value_b, risk_value_taml, notes, maker, checker, approved, "
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			Remittance remittance = new ObjectMapper().readValue(data, Remittance.class);
			String hashValueForRemittance = hashCodeGenerator.generateHashValueForRiskRemittance(remittance, risk,
					reason, user);
			riskDaoImpl.insertRemitanceUpdate(sql, remittance, risk, reason, user, hashValueForRemittance);

		} else if (table.equals("risk_list_un_individual") || table.equals("risk_hot_list")
				|| table.equals("risk_list_investigation_info") || table.equals("risk_list_ofac_sdn_entry")
				|| table.equals("risk_list_un_entity")) {
			String reference_table_id = table + "_id";
			sql = "insert into " + table + "_update(" + reference_table_id
					+ ",document_code,type,risk_value_b,risk_value_taml,"
					+ "notes, is_active,active_date,disabled_date, maker, checker, approved,"
					+ "update_date, approved_date, reason,hash) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ListRisk listRisk = new ObjectMapper().readValue(data, ListRisk.class);
			String hashValueForCard = hashCodeGenerator.generateHashValueForRiskListRisk(listRisk, risk, reason, user);
			riskDaoImpl.insertListUpdate(sql, listRisk, risk, reason, user, hashValueForCard);
		}
	}

	/*
	 * public void insertPCRiskUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertPCRiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertPDRiskUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertPDRiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertBSRiskUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertBSRiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertBERiskUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertBERiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertCountryRiskUpdate(String sql, String data, Integer
	 * risk, String reason) throws JsonParseException, SQLException, IOException
	 * { riskDaoImpl.insertCountryRiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertCERiskUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertCERiskUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertDeliveryUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertDeliveryUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertIndustryUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertIndustryUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertOccupationalUpdate(String sql, String data, Integer
	 * risk, String reason) throws JsonParseException, SQLException, IOException
	 * { riskDaoImpl.insertOccupationalUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertPepUpdate(String sql, String data, Integer risk, String
	 * reason) throws JsonParseException, SQLException,
	 * TrustAmlEmptyJSONException, IOException {
	 * riskDaoImpl.insertPepUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertPLUpdate(String sql, String data, Integer risk, String
	 * reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertPLUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertRemitanceUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException, IOException {
	 * riskDaoImpl.insertRemitanceUpdate(sql, data, risk, reason); }
	 * 
	 * public void insertListUpdate(String sql, String data, Integer risk,
	 * String reason) throws JsonParseException, SQLException,
	 * TrustAmlEmptyJSONException, IOException {
	 * riskDaoImpl.insertListUpdate(sql, data, risk, reason); }
	 */

	@SuppressWarnings("unchecked")
	public void updateRisk(String json) throws JsonParseException, JsonMappingException, IOException,
			NumberFormatException, SQLException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> id = mapper.readValue(json, HashMap.class);
		Map<String, Map<String, String>> id1 = mapper.readValue(json, HashMap.class);
		String table = id.get("table");
		Map<String, String> mapData = id1.get("data");
		String data = mapper.writeValueAsString(mapData);
		String risk = id.get("risk");
		String reason = id.get("reason");
		String updateId = id.get("updateId");
		updateRisk(table, Integer.parseInt(risk), Long.parseLong(updateId));
		User user = JsonStringToRiskObject.getUserObjectFromJsonString(json);
		insertRiskUpdate(table, data, Integer.parseInt(risk), reason, user);
	}

	@SuppressWarnings("unchecked")
	public void saveRisk(String json)
			throws JsonParseException, JsonMappingException, IOException, NumberFormatException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> id = mapper.readValue(json, HashMap.class);
		Map<String, Map<String, String>> id1 = mapper.readValue(json, HashMap.class);
		String table = id.get("table");
		Map<String, String> mapData = id1.get("data");
		// String data = mapper.writeValueAsString(mapData);
		String risk = id.get("risk");
		String hashValue = hashCodeGenerator.generateHashValueForRisk(Integer.parseInt(risk));
		riskDaoImpl.saveRisk(table, Integer.parseInt(risk), hashValue);
	}

	public Object getAllRiskTable() throws SQLException {
		// TODO Auto-generated method stub
		return riskDaoImpl.getAllRiskTable();
	}

	public Object findMatchNatural(String name, float matchLevel) throws SQLException {
		return riskDaoImpl.findMatchNatural(name, matchLevel);

	}

	public Object findMatchLegal(String name, float matchLevel) throws SQLException {
		return riskDaoImpl.findMatchLegal(name, matchLevel);

	}

	public RiskMapper calculateRiskNatural(Long screeningId)
			throws SQLException, JsonParseException, JsonMappingException, IOException {
		RiskMapper riskMapper = new RiskMapper();
		List<ObtainedRisk> listObtainedRisk = null;
		Integer totalRisk = 0;
		ObjectMapper objMapper = new ObjectMapper();
		if (constantEntity.getSizeOfRiskMap() <= 0) {
			constantEntity.getRiskMapping();
		} else {
			listObtainedRisk = objMapper.readValue(riskDaoImpl.calculateRiskNatural(screeningId),
					objMapper.getTypeFactory().constructCollectionType(List.class, ObtainedRisk.class));
			for (ObtainedRisk obtainedRisk : listObtainedRisk) {
				String mappedRiskValue = constantEntity
						.getValueOfRisk(constantEntity.getRiskMappingName(obtainedRisk.getRisk()));
				totalRisk += Integer.parseInt(mappedRiskValue);
			}
		}
		riskMapper.setListObtainedRisk(listObtainedRisk);
		riskMapper.setTotalRisk(totalRisk);
		// Integer sum = obRisk.stream().mapToInt(obR ->
		// Integer.parseInt(obR.getRisk())).sum();
		return riskMapper;
	}

	public Object calculateKYCNRiskNatural(Long kycnId) throws SQLException {
		// TODO Auto-generated method stub

		return riskDaoImpl.calculateKYCNRiskNatural(kycnId);
	}

	public Object calculateKYCNRiskLegal(Long kyclId) throws SQLException {
		// TODO Auto-generated method stub
		return riskDaoImpl.calculateKYCNRiskLegal(kyclId);
	}

	public Object calculateRiskLegal(Long screeningId) throws SQLException {
		// TODO Auto-generated method stub
		return riskDaoImpl.calculateRiskLegal(screeningId);
	}

}
