package com.trustaml.service.risk.dao;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.risk.model.Adversemedia;
import com.trustaml.service.risk.model.BankService;
import com.trustaml.service.risk.model.BusinessEnvironment;
import com.trustaml.service.risk.model.Card;
import com.trustaml.service.risk.model.Country;
import com.trustaml.service.risk.model.CrimeEnvironment;
import com.trustaml.service.risk.model.Delivery;
import com.trustaml.service.risk.model.Industry;
import com.trustaml.service.risk.model.ListRisk;
import com.trustaml.service.risk.model.Occupational;
import com.trustaml.service.risk.model.Pep;
import com.trustaml.service.risk.model.ProductDeposit;
import com.trustaml.service.risk.model.ProductLoan;
import com.trustaml.service.risk.model.Remittance;
import com.trustaml.service.risk.model.Scheme;

public class RiskDaoImpl {

	@Inject
	DBConnection dbConnection;

	public List<Object> getRisk(String name) throws SQLException {
		List<Object> rd = new ArrayList<>();
		String sql = "select * from " + name;
		Statement stmt = null;
		Connection conn = null;

		try {
			conn = dbConnection.getConnection();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (name.equals("risk_bank_product_cards")) {
					Card card = new Card(rs.getLong("id"), rs.getString("document_code"), rs.getString("product_code"),
							rs.getString("name"), rs.getString("card_type"), rs.getBoolean("domestic"),
							rs.getBoolean("international"), rs.getBoolean("is_active"), rs.getInt("maximum_threshold"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(card);
				} else if (name.equals("risk_bank_product_deposit")) {
					ProductDeposit deposit = new ProductDeposit(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("product_code"), rs.getString("name"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("local_currency"),
							rs.getBoolean("is_foreign_currency"), rs.getString("foreign_currency"),
							rs.getString("notes"));
					rd.add(deposit);
				} else if (name.equals("risk_bank_product_loan")) {
					ProductLoan loan = new ProductLoan(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("product_code"), rs.getString("name"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(loan);
				} else if (name.equals("risk_bank_services")) {
					BankService bs = new BankService(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("service_code"), rs.getString("name"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(bs);
				} else if (name.equals("risk_country")) {
					Country country = new Country(rs.getLong("id"), rs.getString("country_code_3a"),
							rs.getString("country"), rs.getString("name"), rs.getString("numeric_code"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getString("notes"));
					rd.add(country);
				} else if (name.equals("risk_delivery")) {
					Delivery delivery = new Delivery(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("product_code"), rs.getString("delivery_action"), rs.getString("name"),
							rs.getBoolean("is_active"), rs.getDate("active_date"), rs.getDate("disabled_date"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(delivery);
				} else if (name.equals("risk_domestic_business_environment")) {
					BusinessEnvironment environment = new BusinessEnvironment(rs.getLong("id"),
							rs.getString("document_code"), rs.getString("group_code"), rs.getString("code"),
							rs.getString("location"), rs.getString("name"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(environment);
				} else if (name.equals("risk_domestic_crime_environment")) {
					CrimeEnvironment crimeEnvironment = new CrimeEnvironment(rs.getLong("id"),
							rs.getString("document_code"), rs.getString("group_code"), rs.getString("code"),
							rs.getString("location"), rs.getString("name"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getInt("risk_value_b"),
							rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(crimeEnvironment);
				} else if (name.equals("risk_industry")) {
					Industry industry = new Industry(rs.getLong("id"), rs.getString("jurisdiction_2a"),
							rs.getString("jurisdiction_3a"), rs.getString("document_code"),
							rs.getString("major_group_code"), rs.getString("broad_group_code"),
							rs.getString("group_detail_code"), rs.getString("detail_code"), rs.getString("name"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getString("notes"));
					rd.add(industry);
				} else if (name.equals("risk_occupational")) {
					Occupational occupational = new Occupational(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("major_group_code"), rs.getString("minor_group_code"),
							rs.getString("broad_group_code"), rs.getString("detail_code"), rs.getString("name"),
							rs.getBoolean("is_active"), rs.getDate("active_date"), rs.getDate("disabled_date"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getString("notes"));
					rd.add(occupational);
				} else if (name.equals("risk_pep")) {
					Pep pep = new Pep(rs.getLong("id"), rs.getString("category"), rs.getString("level"),
							rs.getString("name"), rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"),
							rs.getBoolean("is_active"), rs.getDate("active_date"), rs.getDate("disabled_date"),
							rs.getString("notes"));
					rd.add(pep);
				} else if (name.equals("risk_bank_product_remittance")) {
					Remittance remittance = new Remittance(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("product_code"), rs.getString("name"), rs.getString("inward_outward"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getBoolean("is_active"),
							rs.getDate("active_date"), rs.getDate("disabled_date"), rs.getString("notes"));
					rd.add(remittance);
				} else if (name.equals("risk_list_un_individual") || name.equals("risk_hot_list")
						|| name.equals("risk_list_investigation_info") || name.equals("risk_list_ofac_sdn_entry")
						|| name.equals("risk_list_un_entity")) {
					ListRisk list = new ListRisk(rs.getLong("id"), rs.getString("document_code"), rs.getString("type"),
							rs.getInt("risk_value_b"), rs.getInt("risk_value_taml"), rs.getString("notes"),
							rs.getBoolean("is_active"), rs.getDate("active_date"), rs.getDate("disabled_date"));
					rd.add(list);
				} else if (name.equals("risk_scheme")) {
					Scheme scheme = new Scheme(rs.getLong("id"), rs.getString("schm_code"), rs.getString("service"),
							rs.getLong("risk_value_taml"), rs.getLong("risk_value_b"), rs.getString("description"),
							rs.getBoolean("is_active"), rs.getDate("active_date"), rs.getDate("disable_date"),
							rs.getString("notes"), rs.getString("name"));
					rd.add(scheme);
				} else if (name.equals("risk_adverse_media_personal_info")) {
					Adversemedia adversemedia = new Adversemedia(rs.getLong("id"), rs.getString("document_code"),
							rs.getString("adverse_type"), rs.getLong("risk_value_b"), rs.getLong("risk_value_taml"),
							rs.getString("notes"), rs.getBoolean("is_active"), rs.getDate("active_date"),
							rs.getDate("disabled_date"), rs.getString("name"));
					rd.add(adversemedia);
				}

			}
		} finally

		{
			stmt.close();
			conn.close();
		}
		return rd;
	}

	public void updateRisk(String table, Integer riskValue, Long id, String hashValue) throws SQLException {

		String sql = "update " + table + " set risk_value_taml =?,hash=? where id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setInt(1, riskValue);
			ps.setString(2, hashValue);
			ps.setLong(3, id);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
	// Insert Risk List Update

	public void insertListUpdate(String sql, ListRisk listRisk, Integer risk, String reason, User user,
			String hashValueForList) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, listRisk.getId());
			ps.setString(2, listRisk.getDocumentcode());
			ps.setString(3, listRisk.getType());
			ps.setInt(4, listRisk.getRiskB());
			ps.setInt(5, risk);
			ps.setString(6, listRisk.getNotes());
			ps.setBoolean(7, listRisk.getActive());
			ps.setDate(8, new java.sql.Date(listRisk.getActiveDate().getTime()));
			if (listRisk.getDisabeDate() != null) {
				ps.setDate(9, new java.sql.Date(listRisk.getDisabeDate().getTime()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			ps.setString(10, user.getUserName());
			ps.setString(11, user.getUserName());
			ps.setBoolean(12, true);
			ps.setDate(13, new java.sql.Date(new Date().getTime()));
			ps.setDate(14, new java.sql.Date(new Date().getTime()));
			ps.setString(15, reason);
			ps.setString(16, hashValueForList);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Risk Pep Update

	public void insertPepUpdate(String sql, Pep pep, Integer risk, String reason, User user, String hashValueForPep)
			throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, pep.getId());
			ps.setString(2, pep.getCategory());
			ps.setString(3, pep.getLevel());
			ps.setString(4, pep.getName());
			ps.setBoolean(5, pep.getActive());
			ps.setDate(6, new java.sql.Date(pep.getActiveDate().getTime()));
			if (pep.getDisableDate() != null) {
				ps.setDate(7, new java.sql.Date(pep.getDisableDate().getTime()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setInt(8, pep.getRiskB());
			ps.setInt(9, risk);
			ps.setString(10, pep.getNotes());
			ps.setString(11, user.getUserName());
			ps.setString(12, user.getUserName());
			ps.setBoolean(13, true);
			ps.setDate(14, new java.sql.Date(new Date().getTime()));
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setString(16, reason);
			ps.setString(17, hashValueForPep);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	// Insert Risk Occupational Update

	public void insertOccupationalUpdate(String sql, Occupational occupational, Integer risk, String reason, User user,
			String hashValueForOccupation) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, occupational.getId());
			ps.setString(2, occupational.getDocumentCode());
			ps.setString(3, occupational.getMajorGroupCode());
			ps.setString(4, occupational.getMinorGroupCode());
			ps.setString(5, occupational.getBroadGroupCode());
			ps.setString(6, occupational.getDetailCode());
			ps.setString(7, occupational.getName());
			ps.setBoolean(8, occupational.getActive());
			ps.setDate(9, new java.sql.Date(occupational.getActiveDate().getTime()));
			if (occupational.getDisableDate() != null) {
				ps.setDate(10, new java.sql.Date(occupational.getDisableDate().getTime()));
			} else {
				ps.setNull(10, java.sql.Types.DATE);
			}
			ps.setInt(11, occupational.getRiskB());
			ps.setInt(12, risk);
			ps.setString(13, occupational.getNotes());
			ps.setString(14, user.getUserName());
			ps.setString(15, user.getUserName());
			ps.setBoolean(16, true);
			ps.setDate(17, new java.sql.Date(new Date().getTime()));
			ps.setDate(18, new java.sql.Date(new Date().getTime()));
			ps.setString(19, reason);
			ps.setString(20, hashValueForOccupation);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	// Insert Risk Industry Update

	public void insertIndustryUpdate(String sql, Industry industry, Integer risk, String reason, User user,
			String hashValueForIndustry) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, industry.getId());
			ps.setString(2, industry.getDocumentCode());
			ps.setString(3, industry.getMajorGroupCode());
			ps.setString(4, industry.getBroadGroupCode());
			ps.setString(5, industry.getGroupDetailCode());
			ps.setString(6, industry.getDetailCode());
			ps.setString(7, industry.getName());
			ps.setBoolean(8, industry.getActive());
			ps.setDate(9, new java.sql.Date(industry.getActiveDate().getTime()));
			if (industry.getDisableDate() != null) {
				ps.setDate(10, new java.sql.Date(industry.getDisableDate().getTime()));
			} else {
				ps.setNull(10, java.sql.Types.DATE);
			}
			ps.setInt(11, industry.getRiskB());
			ps.setInt(12, risk);
			ps.setString(13, industry.getNotes());
			ps.setString(14, user.getUserName());
			ps.setString(15, user.getUserName());
			ps.setBoolean(16, true);
			ps.setDate(17, new java.sql.Date(new Date().getTime()));
			ps.setDate(18, new java.sql.Date(new Date().getTime()));
			ps.setString(19, reason);
			ps.setString(20, hashValueForIndustry);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Domestic Crime Environment Update

	public void insertCERiskUpdate(String sql, CrimeEnvironment environment, Integer risk, String reason, User user,
			String hashValueForCrimeEnvironment) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, environment.getId());
			ps.setString(2, environment.getDocumentCode());
			ps.setString(3, environment.getGroupCode());
			ps.setString(4, environment.getCode());
			ps.setString(5, environment.getLocation());
			ps.setString(6, environment.getName());
			ps.setBoolean(7, environment.getActive());
			ps.setDate(8, new java.sql.Date(environment.getActiveDate().getTime()));
			if (environment.getDisableDate() != null) {
				ps.setDate(9, new java.sql.Date(environment.getDisableDate().getTime()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			ps.setInt(19, environment.getRiskB());
			ps.setInt(11, risk);
			ps.setString(12, environment.getNotes());
			ps.setString(13, user.getUserName());
			ps.setString(14, user.getUserName());
			ps.setBoolean(15, true);
			ps.setDate(16, new java.sql.Date(new Date().getTime()));
			ps.setDate(17, new java.sql.Date(new Date().getTime()));
			ps.setString(18, reason);
			ps.setString(18, hashValueForCrimeEnvironment);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Domestic Business Environment Update

	public void insertBERiskUpdate(String sql, BusinessEnvironment businessEnvironment, Integer risk, String reason,
			User user, String hashValueForBusinessEnvironment) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, businessEnvironment.getId());
			ps.setString(2, businessEnvironment.getDocumentCode());
			ps.setString(3, businessEnvironment.getGroupCode());
			ps.setString(4, businessEnvironment.getCode());
			ps.setString(5, businessEnvironment.getLocation());
			ps.setString(6, businessEnvironment.getName());
			ps.setBoolean(7, businessEnvironment.getActive());
			ps.setDate(8, new java.sql.Date(businessEnvironment.getActiveDate().getTime()));
			if (businessEnvironment.getDisableDate() != null) {
				ps.setDate(9, new java.sql.Date(businessEnvironment.getDisableDate().getTime()));
			} else {
				ps.setNull(9, java.sql.Types.DATE);
			}
			ps.setInt(10, businessEnvironment.getRiskB());
			ps.setInt(11, risk);
			ps.setString(12, businessEnvironment.getNotes());
			ps.setString(13, user.getUserName());
			ps.setString(14, user.getUserName());
			ps.setBoolean(15, true);
			ps.setDate(16, new java.sql.Date(new Date().getTime()));
			ps.setDate(17, new java.sql.Date(new Date().getTime()));
			ps.setString(18, reason);
			ps.setString(19, hashValueForBusinessEnvironment);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Delivery Risk Update

	public void insertDeliveryUpdate(String sql, Delivery delivery, Integer risk, String reason, User user,
			String hashValueForDelivery) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, delivery.getId());
			ps.setString(2, delivery.getDocumentCode());
			ps.setString(3, delivery.getProductCode());
			ps.setString(4, delivery.getAction());
			ps.setString(5, delivery.getName());
			ps.setBoolean(6, delivery.getActive());
			ps.setDate(7, new java.sql.Date(delivery.getActiveDate().getTime()));
			if (delivery.getDisableDate() != null) {
				ps.setDate(8, new java.sql.Date(delivery.getDisableDate().getTime()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			ps.setInt(9, delivery.getRiskB());
			ps.setInt(10, risk);
			ps.setString(11, delivery.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setBoolean(14, true);
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setDate(16, new java.sql.Date(new Date().getTime()));
			ps.setString(17, reason);
			ps.setString(18, hashValueForDelivery);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Country Risk Update

	public void insertCountryRiskUpdate(String sql, Country country, Integer risk, String reason, User user,
			String hashValueForCountry) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, country.getId());
			ps.setString(2, country.getCountryCode());
			ps.setString(3, country.getCounty());
			ps.setString(4, country.getName());
			ps.setString(5, country.getNumericCode());
			ps.setBoolean(6, country.getActive());
			ps.setDate(7, new java.sql.Date(country.getActiveDate().getTime()));
			if (country.getDisabledDate() != null) {
				ps.setDate(8, new java.sql.Date(country.getDisabledDate().getTime()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			ps.setInt(9, country.getRiskB());
			ps.setInt(10, risk);
			ps.setString(11, country.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setBoolean(14, true);
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setDate(16, new java.sql.Date(new Date().getTime()));
			ps.setString(17, reason);
			ps.setString(18, hashValueForCountry);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Banking Service Risk Update

	public void insertBSRiskUpdate(String sql, BankService bankService, Integer risk, String reason, User user,
			String hashValueForRisk) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, bankService.getId());
			ps.setString(2, bankService.getDocumentCode());
			ps.setString(3, bankService.getServiceCode());
			ps.setString(4, bankService.getName());
			ps.setBoolean(5, bankService.getActive());
			ps.setDate(6, new java.sql.Date(bankService.getActiveDate().getTime()));
			if (bankService.getDisabledDate() != null) {
				ps.setDate(7, new java.sql.Date(bankService.getDisabledDate().getTime()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setInt(8, bankService.getRiskB());
			ps.setInt(9, risk);
			ps.setString(10, bankService.getNotes());
			ps.setString(11, user.getUserName());
			ps.setString(12, user.getUserName());
			ps.setBoolean(13, true);
			ps.setDate(14, new java.sql.Date(new Date().getTime()));
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setString(16, reason);
			ps.setString(17, hashValueForRisk);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// insert product Loan Update

	public void insertPLUpdate(String sql, ProductLoan productLoan, Integer risk, String reason, User user,
			String hashValueForProductLoan) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, productLoan.getId());
			ps.setString(2, productLoan.getDocumentCode());
			ps.setString(3, productLoan.getProductCode());
			ps.setString(4, productLoan.getName());
			ps.setBoolean(5, productLoan.getActive());
			ps.setDate(6, new java.sql.Date(productLoan.getActiveDate().getTime()));
			if (productLoan.getDisableDate() != null) {
				ps.setDate(7, new java.sql.Date(productLoan.getDisableDate().getTime()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setInt(8, productLoan.getRiskB());
			ps.setInt(9, risk);
			ps.setString(10, productLoan.getNotes());
			ps.setString(11, user.getUserName());
			ps.setString(12, user.getUserName());
			ps.setBoolean(13, true);
			ps.setDate(14, new java.sql.Date(new Date().getTime()));
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setString(16, reason);
			ps.setString(17, hashValueForProductLoan);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	// Insert Product Deposit Risk Update

	public void insertPDRiskUpdate(String sql, ProductDeposit deposit, Integer risk, String reason, User user,
			String hashValueForDeposit) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, deposit.getId());
			ps.setString(2, deposit.getDocumentCode());
			ps.setString(3, deposit.getProductCode());
			ps.setString(4, deposit.getName());
			ps.setBoolean(5, deposit.getActive());
			ps.setDate(6, new java.sql.Date(deposit.getActiveDate().getTime()));
			if (deposit.getDisableDate() != null) {
				ps.setDate(7, new java.sql.Date(deposit.getDisableDate().getTime()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setInt(8, deposit.getRiskB());
			ps.setInt(9, risk);
			ps.setString(10, deposit.getLocalCurrency());
			ps.setBoolean(11, deposit.getForeign());
			ps.setString(12, deposit.getForeignCurrency());
			ps.setString(13, deposit.getNotes());
			ps.setString(14, user.getUserName());
			ps.setString(15, user.getUserName());
			ps.setBoolean(16, true);
			ps.setDate(17, new java.sql.Date(new Date().getTime()));
			ps.setDate(18, new java.sql.Date(new Date().getTime()));
			ps.setString(19, reason);
			ps.setString(20, hashValueForDeposit);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public void insertPCRiskUpdate(String sql, Card card, Integer risk, String reason, User user,
			String hashValueForCard) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, card.getId());
			ps.setString(2, card.getDocumentCode());
			ps.setString(3, card.getProductCode());
			ps.setString(4, card.getName());
			ps.setString(5, card.getType());
			ps.setBoolean(6, card.getDomestic());
			ps.setBoolean(7, card.getForeign());
			ps.setInt(8, card.getThershold());
			ps.setBoolean(9, card.getActive());
			ps.setDate(10, new java.sql.Date(card.getActiveDate().getTime()));
			if (card.getDisableDate() != null) {
				ps.setDate(11, new java.sql.Date(card.getDisableDate().getTime()));
			} else {
				ps.setNull(11, java.sql.Types.DATE);
			}
			ps.setInt(12, card.getRiskB());
			ps.setInt(13, risk);
			ps.setString(14, card.getNotes());
			ps.setString(15, user.getUserName());
			ps.setString(16, user.getUserName());
			ps.setBoolean(17, true);
			ps.setDate(18, new java.sql.Date(new Date().getTime()));
			ps.setDate(19, new java.sql.Date(new Date().getTime()));
			ps.setString(20, reason);
			ps.setString(21, hashValueForCard);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertRemitanceUpdate(String sql, Remittance remittance, Integer risk, String reason, User user,
			String hashValueForRemittance) throws SQLException, JsonParseException, IOException {
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, remittance.getId());
			ps.setString(2, remittance.getDocumentCode());
			ps.setString(3, remittance.getProductCode());
			ps.setString(4, remittance.getName());
			ps.setString(5, remittance.getInoutWard());
			ps.setBoolean(6, remittance.getActive());
			ps.setDate(7, new java.sql.Date(remittance.getActiveDate().getTime()));
			if (remittance.getDisableDate() != null) {
				ps.setDate(8, new java.sql.Date(remittance.getDisableDate().getTime()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			ps.setInt(9, remittance.getRiskB());
			ps.setInt(10, risk);
			ps.setString(11, remittance.getNotes());
			ps.setString(12, user.getUserName());
			ps.setString(13, user.getUserName());
			ps.setBoolean(14, true);
			ps.setDate(15, new java.sql.Date(new Date().getTime()));
			ps.setDate(16, new java.sql.Date(new Date().getTime()));
			ps.setString(17, reason);
			ps.setString(18, hashValueForRemittance);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public String getRiskTable() throws SQLException {

		String result = "";
		CallableStatement stmt = null;
		Connection conn = null;
		try {
			conn = dbConnection.getConnection();
			stmt = conn.prepareCall("{call master_risk_table(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			conn.close();
		}
		return result;
	}

	public void saveRisk(String table, Integer riskValue, String hashValue) throws SQLException {

		String sql = "INSERT INTO " + table + " (risk_value_taml,hash) values(?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setInt(1, riskValue);
			ps.setString(2, hashValue);
			ps.executeUpdate();

		} finally {
			ps.close();
			connection.close();
		}
	}

	public Object getAllRiskTable() throws SQLException {

		String result = "";
		Connection conn = null;
		CallableStatement stmt = null;
		try {
			conn = dbConnection.getConnection();
			stmt = conn.prepareCall("{call master_risk_table(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			conn.close();
		}

		return result;

	}

	public Object findMatchNatural(String name, float matchLevel) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_n_match_fields(?,?,?)}");
			stmt.setString(1, name);
			stmt.setFloat(2, matchLevel);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public Object findMatchLegal(String name, float matchLevel) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_l_match_fields(?,?,?)}");
			stmt.setString(1, name);
			stmt.setFloat(2, matchLevel);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public String calculateRiskNatural(Long screeningId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call related_n_match_data(?,?)}");
			stmt.setLong(1, screeningId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public String calculateKYCNRiskNatural(Long kycnId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call kycn_risk_profiling(?,?)}");
			stmt.setLong(1, kycnId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public Object calculateKYCNRiskLegal(Long kyclId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call kycl_risk_profiling(?,?)}");
			stmt.setLong(1, kyclId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	public Object calculateRiskLegal(Long screeningId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call related_l_match_data(?,?)}");
			stmt.setLong(1, screeningId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}
}
