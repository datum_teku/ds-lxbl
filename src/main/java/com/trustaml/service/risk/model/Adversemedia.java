package com.trustaml.service.risk.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Adversemedia {

	private long id;

	private String documentCode;

	private String adverseType;

	private long riskB;

	private long riskTaml;

	private String notes;

	private boolean isActive;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date activeDate;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date disabledDate;

	private String name;

	public Adversemedia() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Adversemedia(long id, String documentCode, String adverseType, long riskB, long riskTaml, String notes,
			boolean isActive, Date activeDate, Date disabledDate, String name) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.adverseType = adverseType;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
		this.isActive = isActive;
		this.activeDate = activeDate;
		this.disabledDate = disabledDate;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getAdverseType() {
		return adverseType;
	}

	public void setAdverseType(String adverseType) {
		this.adverseType = adverseType;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public long getRiskB() {
		return riskB;
	}

	public void setRiskB(long riskB) {
		this.riskB = riskB;
	}

	public long getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(long riskTaml) {
		this.riskTaml = riskTaml;
	}

}
