package com.trustaml.service.risk.model;

import java.util.Date;

public class BankService {
	private Long id;

	private String documentCode;

	private String serviceCode;

	private String name;

	private Boolean active;

	private Date activeDate;

	private Date disabledDate;

	private int riskB;

	private int riskTaml;

	private String notes;

	public BankService() {

	}

	public BankService(Long id, String documentCode, String serviceCode, String name, Boolean active, Date activeDate,
			Date disabledDate, int riskB, int riskTaml, String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.serviceCode = serviceCode;
		this.name = name;
		this.active = active;
		this.activeDate = activeDate;
		this.disabledDate = disabledDate;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
