package com.trustaml.service.risk.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Card {
	
	private Long id;

	private String documentCode;

	private String productCode;

	private String name;

	private String type;

	private Boolean domestic;

	private Boolean foreign;

	private Boolean active;

	private int thershold;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date activeDate;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date disableDate;

	private int riskB;

	private int riskTaml;

	private String notes;

	public Card() {

	}

	public Card(Long id, String documentCode, String productCode, String name, String type, Boolean domestic,
			Boolean foreign, Boolean active, int thershold, Date activeDate, Date disableDate, int riskB, int riskTaml,
			String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.productCode = productCode;
		this.name = name;
		this.type = type;
		this.domestic = domestic;
		this.foreign = foreign;
		this.active = active;
		this.thershold = thershold;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getDomestic() {
		return domestic;
	}

	public void setDomestic(Boolean domestic) {
		this.domestic = domestic;
	}

	public Boolean getForeign() {
		return foreign;
	}

	public void setForeign(Boolean foreign) {
		this.foreign = foreign;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getThershold() {
		return thershold;
	}

	public void setThershold(int thershold) {
		this.thershold = thershold;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

}
