package com.trustaml.service.risk.model;

import java.util.Date;

public class Country {
	private Long id;

	private String countryCode;

	private String county;

	private String name;

	private String numericCode;

	private int riskB;

	private int riskTaml;

	private Boolean active;

	private Date activeDate;

	private Date disabledDate;

	private String notes;

	public Country() {

	}
	public Country(Long id, String countryCode, String county, String name, String numericCode, int riskB, int riskTaml,
			Boolean active, Date activeDate, Date disabledDate, String notes) {
		super();
		this.id = id;
		this.countryCode = countryCode;
		this.county = county;
		this.name = name;
		this.numericCode = numericCode;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.active = active;
		this.activeDate = activeDate;
		this.disabledDate = disabledDate;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Date disabledDate) {
		this.disabledDate = disabledDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
