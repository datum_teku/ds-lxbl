package com.trustaml.service.risk.model;

import java.util.Date;

public class Delivery {
	private Long id;

	private String documentCode;

	private String productCode;

	private String action;

	private String name;

	private Boolean active;

	private Date activeDate;

	private Date disableDate;

	private int riskB;

	private int riskTaml;

	private String notes;

	public Delivery() {

	}

	public Delivery(Long id, String documentCode, String productCode, String action, String name, Boolean active,
			Date activeDate, Date disableDate, int riskB, int riskTaml, String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.productCode = productCode;
		this.action = action;
		this.name = name;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
