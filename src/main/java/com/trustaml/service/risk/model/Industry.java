package com.trustaml.service.risk.model;

import java.util.Date;

public class Industry {
	private Long id;

	private String jurisdiction2A;

	private String jurisdiction3A;

	private String documentCode;

	private String majorGroupCode;

	private String broadGroupCode;

	private String groupDetailCode;

	private String detailCode;

	private String name;

	private int riskB;

	private int riskTaml;

	private Boolean active;

	private Date activeDate;

	private Date disableDate;

	private String notes;

	public Industry() {

	}

	public Industry(Long id, String jurisdiction2a, String jurisdiction3a, String documentCode, String majorGroupCode,
			String broadGroupCode, String groupDetailCode, String detailCode, String name, int riskB, int riskTaml,
			Boolean active, Date activeDate, Date disableDate, String notes) {
		super();
		this.id = id;
		jurisdiction2A = jurisdiction2a;
		jurisdiction3A = jurisdiction3a;
		this.documentCode = documentCode;
		this.majorGroupCode = majorGroupCode;
		this.broadGroupCode = broadGroupCode;
		this.groupDetailCode = groupDetailCode;
		this.detailCode = detailCode;
		this.name = name;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJurisdiction2A() {
		return jurisdiction2A;
	}

	public void setJurisdiction2A(String jurisdiction2a) {
		jurisdiction2A = jurisdiction2a;
	}

	public String getJurisdiction3A() {
		return jurisdiction3A;
	}

	public void setJurisdiction3A(String jurisdiction3a) {
		jurisdiction3A = jurisdiction3a;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getMajorGroupCode() {
		return majorGroupCode;
	}

	public void setMajorGroupCode(String majorGroupCode) {
		this.majorGroupCode = majorGroupCode;
	}

	public String getBroadGroupCode() {
		return broadGroupCode;
	}

	public void setBroadGroupCode(String broadGroupCode) {
		this.broadGroupCode = broadGroupCode;
	}

	public String getDetailCode() {
		return detailCode;
	}

	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getGroupDetailCode() {
		return groupDetailCode;
	}

	public void setGroupDetailCode(String groupDetailCode) {
		this.groupDetailCode = groupDetailCode;
	}

}
