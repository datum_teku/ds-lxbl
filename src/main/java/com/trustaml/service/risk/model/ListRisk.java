package com.trustaml.service.risk.model;

import java.util.Date;

public class ListRisk {

	private Long id;
	
	private String documentcode;
	
	private String type;
	
	private int riskB;
	
	private int riskTaml;
	
	private String notes;
	
	private Boolean active;
	
	private Date activeDate;
	
	private Date disabeDate;
	
	
	public ListRisk(){
		
	}

	public ListRisk(Long id, String documentcode, String type, int riskB, int riskTaml, String notes, Boolean active,
			Date activeDate, Date disabeDate) {
		super();
		this.id = id;
		this.documentcode = documentcode;
		this.type = type;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
		this.active = active;
		this.activeDate = activeDate;
		this.disabeDate = disabeDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentcode() {
		return documentcode;
	}

	public void setDocumentcode(String documentcode) {
		this.documentcode = documentcode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisabeDate() {
		return disabeDate;
	}

	public void setDisabeDate(Date disabeDate) {
		this.disabeDate = disabeDate;
	}
	
	
	
}
