package com.trustaml.service.risk.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ObtainedRisk {

	private String type;
	private Long id;
	@JsonProperty("first_name")
	private String firstName;
	
	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	private String gender;
	private String nationality;
	private String designation;

	@JsonProperty("office_name")
	private String officeName;

	@JsonProperty("second_name")
	private String secondName;

	@JsonProperty("third_name")
	private String thirdName;

	@JsonProperty("fourth_name")
	private String fourthName;

	@JsonProperty("original_script_name")
	private String originalScriptName;

	@JsonProperty("identification_number")
	private String identificationNumber;

	@JsonProperty("date_of_birth")
	private String dateOfBirth;
	private String risk;

	public ObtainedRisk(String type, Long id, String firstName, String middleName, String lastName, String lsfName,
			String lsmName, String lslName, String gender, String nationality, String designation, String officeName,
			String secondName, String thirdName, String fourthName, String originalScriptName,
			String identificationNumber, String dateOfBirth, String risk) {
		super();
		this.type = type;
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.gender = gender;
		this.nationality = nationality;
		this.designation = designation;
		this.officeName = officeName;
		this.secondName = secondName;
		this.thirdName = thirdName;
		this.fourthName = fourthName;
		this.originalScriptName = originalScriptName;
		this.identificationNumber = identificationNumber;
		this.dateOfBirth = dateOfBirth;
		this.risk = risk;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getThirdName() {
		return thirdName;
	}

	public void setThirdName(String thirdName) {
		this.thirdName = thirdName;
	}

	public String getFourthName() {
		return fourthName;
	}

	public void setFourthName(String fourthName) {
		this.fourthName = fourthName;
	}

	public String getOriginalScriptName() {
		return originalScriptName;
	}

	public void setOriginalScriptName(String originalScriptName) {
		this.originalScriptName = originalScriptName;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public ObtainedRisk() {
		super();
		// TODO Auto-generated constructor stub
	}

}
