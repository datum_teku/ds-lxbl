package com.trustaml.service.risk.model;

import java.util.Date;

public class Occupational {
	private Long id;

	private String documentCode;

	private String majorGroupCode;

	private String minorGroupCode;

	private String broadGroupCode;

	private String detailCode;

	private String name;

	private Boolean active;

	private Date activeDate;

	private Date disableDate;

	private int riskB;

	private int riskTaml;

	private String notes;

	public Occupational() {

	}

	public Occupational(Long id, String documentCode, String majorGroupCode, String minorGroupCode,
			String broadGroupCode, String detailCode, String name, Boolean active, Date activeDate, Date disableDate,
			int riskB, int riskTaml, String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.majorGroupCode = majorGroupCode;
		this.minorGroupCode = minorGroupCode;
		this.broadGroupCode = broadGroupCode;
		this.detailCode = detailCode;
		this.name = name;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getMajorGroupCode() {
		return majorGroupCode;
	}

	public void setMajorGroupCode(String majorGroupCode) {
		this.majorGroupCode = majorGroupCode;
	}

	public String getMinorGroupCode() {
		return minorGroupCode;
	}

	public void setMinorGroupCode(String minorGroupCode) {
		this.minorGroupCode = minorGroupCode;
	}

	public String getBroadGroupCode() {
		return broadGroupCode;
	}

	public void setBroadGroupCode(String broadGroupCode) {
		this.broadGroupCode = broadGroupCode;
	}

	public String getDetailCode() {
		return detailCode;
	}

	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
