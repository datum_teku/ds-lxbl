package com.trustaml.service.risk.model;

import java.util.Date;

public class Pep {
	private Long id;

	private String category;

	private String level;

	private String name;

	private int riskB;

	private int riskTaml;

	private Boolean active;

	private Date activeDate;

	private Date disableDate;

	private String notes;

	public Pep() {

	}

	public Pep(Long id, String category, String level, String name, int riskB, int riskTaml, Boolean active,
			Date activeDate, Date disableDate, String notes) {
		super();
		this.id = id;
		this.category = category;
		this.level = level;
		this.name = name;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
