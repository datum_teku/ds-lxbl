package com.trustaml.service.risk.model;

import java.sql.Date;

public class ProductDeposit {

	
private Long id;
	
	private String documentCode;
	
	private String productCode;
	
	private String name;
	
	private Boolean active;
	
	private Date activeDate;
	
	private Date disableDate;
	
	private int riskB;
	
	private int riskTaml;
	
	private String localCurrency;
	
	private Boolean foreign;
	
	private String foreignCurrency;
	
	private String notes;
	public ProductDeposit(){
		
	}
	
	
	public ProductDeposit(Long id, String documentCode, String productCode, String name, Boolean active,
			Date activeDate, Date disableDate, int riskB, int riskTaml, String localCurrency, Boolean foreign,
			String foreignCurrency, String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.productCode = productCode;
		this.name = name;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.localCurrency = localCurrency;
		this.foreign = foreign;
		this.foreignCurrency = foreignCurrency;
		this.notes = notes;
	}


	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public Boolean getForeign() {
		return foreign;
	}

	public void setForeign(Boolean foreign) {
		this.foreign = foreign;
	}

	public String getForeignCurrency() {
		return foreignCurrency;
	}

	public void setForeignCurrency(String foreignCurrency) {
		this.foreignCurrency = foreignCurrency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	
	
	
	
}
