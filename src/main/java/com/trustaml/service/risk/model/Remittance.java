package com.trustaml.service.risk.model;

import java.util.Date;

public class Remittance {

	private Long id;

	private String documentCode;

	private String productCode;

	private String name;

	private String inoutWard;

	private int riskB;

	private int riskTaml;

	private Boolean active;

	private Date activeDate;

	private Date disableDate;

	private String notes;

	public Remittance() {

	}

	public Remittance(Long id, String documentCode, String productCode, String name, String inoutWard, int riskB,
			int riskTaml, Boolean active, Date activeDate, Date disableDate, String notes) {
		super();
		this.id = id;
		this.documentCode = documentCode;
		this.productCode = productCode;
		this.name = name;
		this.inoutWard = inoutWard;
		this.riskB = riskB;
		this.riskTaml = riskTaml;
		this.active = active;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInoutWard() {
		return inoutWard;
	}

	public void setInoutWard(String inoutWard) {
		this.inoutWard = inoutWard;
	}

	public int getRiskB() {
		return riskB;
	}

	public void setRiskB(int riskB) {
		this.riskB = riskB;
	}

	public int getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(int riskTaml) {
		this.riskTaml = riskTaml;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
