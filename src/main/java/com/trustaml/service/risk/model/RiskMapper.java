package com.trustaml.service.risk.model;

import java.util.ArrayList;
import java.util.List;

public class RiskMapper {
	List<ObtainedRisk> listObtainedRisk;
	Integer totalRisk;

	public RiskMapper() {
		super();
		listObtainedRisk = new ArrayList<>();

		// TODO Auto-generated constructor stub
	}

	public RiskMapper(List<ObtainedRisk> listObtainedRisk, Integer totalRisk) {
		super();
		this.listObtainedRisk = listObtainedRisk;
		this.totalRisk = totalRisk;
	}

	public List<ObtainedRisk> getListObtainedRisk() {
		return listObtainedRisk;
	}

	public void setListObtainedRisk(List<ObtainedRisk> listObtainedRisk) {
		this.listObtainedRisk = listObtainedRisk;
	}

	public Integer getTotalRisk() {
		return totalRisk;
	}

	public void setTotalRisk(Integer totalRisk) {
		this.totalRisk = totalRisk;
	}

}
