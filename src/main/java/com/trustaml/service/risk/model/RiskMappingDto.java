package com.trustaml.service.risk.model;

public class RiskMappingDto {
	private String roleType;
	private String value;

	public RiskMappingDto(String roleType, String value) {
		super();
		this.roleType = roleType;
		this.value = value;
	}

	public RiskMappingDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
