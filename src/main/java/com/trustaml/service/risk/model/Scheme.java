package com.trustaml.service.risk.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Scheme {

	@JsonProperty("id")
	private long id;

	private String schmCode;

	@JsonProperty("service")
	private String service;

	private long riskTaml;

	private long riskB;

	private String description;

	private boolean isActive;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date activeDate;

	@JsonDeserialize(using = CustomDateDeserialized.class)
	private Date disableDate;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("name")
	private String name;

	public Scheme() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Scheme(long id, String schmCode, String service, long riskTaml, long riskB, String description,
			boolean isActive, Date activeDate, Date disableDate, String notes, String name) {
		super();
		this.id = id;
		this.schmCode = schmCode;
		this.service = service;
		this.riskTaml = riskTaml;
		this.riskB = riskB;
		this.description = description;
		this.isActive = isActive;
		this.activeDate = activeDate;
		this.disableDate = disableDate;
		this.notes = notes;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSchmCode() {
		return schmCode;
	}

	public void setSchmCode(String schmCode) {
		this.schmCode = schmCode;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getRiskTaml() {
		return riskTaml;
	}

	public void setRiskTaml(long riskTaml) {
		this.riskTaml = riskTaml;
	}

	public long getRiskB() {
		return riskB;
	}

	public void setRiskB(long riskB) {
		this.riskB = riskB;
	}

}
