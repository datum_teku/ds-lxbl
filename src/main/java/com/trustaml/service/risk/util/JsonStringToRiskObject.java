package com.trustaml.service.risk.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.dto.User;

public class JsonStringToRiskObject {

	public static User getUserObjectFromJsonString(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		JsonNode userNode = jsonNode.get("user");
		User user = mapper.treeToValue(userNode, User.class);
		return user;
	}

	/*
	 * public static Card getCardObjectFromJsonString(String jsonString) throws
	 * JsonParseException, JsonMappingException, IOException { ObjectMapper
	 * mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode cardNode =
	 * jsonNode.get("data"); return mapper.treeToValue(cardNode, Card.class); }
	 * 
	 * public static ProductDeposit getProductDepositObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode productNode =
	 * jsonNode.get("data"); return mapper.treeToValue(productNode,
	 * ProductDeposit.class); }
	 * 
	 * public static ProductLoan getProductLoanObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode productLoanNode =
	 * jsonNode.get("data"); return mapper.treeToValue(productLoanNode,
	 * ProductLoan.class); }
	 * 
	 * public static BankService getBackServiceObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode bankServiceNode =
	 * jsonNode.get("data"); return mapper.treeToValue(bankServiceNode,
	 * BankService.class); }
	 * 
	 * public static Country getCountryRiskObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode countryNode =
	 * jsonNode.get("data"); return mapper.treeToValue(countryNode,
	 * Country.class); }
	 * 
	 * public static Delivery getDeliveryObjectFromJsonString(String jsonString)
	 * throws JsonParseException, JsonMappingException, IOException {
	 * ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode deliveryNode =
	 * jsonNode.get("data"); return mapper.treeToValue(deliveryNode,
	 * Delivery.class); }
	 * 
	 * public static BusinessEnvironment
	 * getBusinessEnvironmentObjectFromJsonString(String jsonString) throws
	 * JsonParseException, JsonMappingException, IOException { ObjectMapper
	 * mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode
	 * businessEnvironmentNode = jsonNode.get("data"); return
	 * mapper.treeToValue(businessEnvironmentNode, BusinessEnvironment.class); }
	 * 
	 * public static CrimeEnvironment
	 * getCrimeEnvironmentObjectFromJsonString(String jsonString) throws
	 * JsonParseException, JsonMappingException, IOException { ObjectMapper
	 * mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode
	 * crimeEnvironmentNode = jsonNode.get("data"); return
	 * mapper.treeToValue(crimeEnvironmentNode, CrimeEnvironment.class); }
	 * 
	 * public static Industry getIndustryObjectFromJsonString(String jsonString)
	 * throws JsonParseException, JsonMappingException, IOException {
	 * ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode industryNode =
	 * jsonNode.get("data"); return mapper.treeToValue(industryNode,
	 * Industry.class); }
	 * 
	 * public static Occupational getOccupationalObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode occupationalNode =
	 * jsonNode.get("data"); return mapper.treeToValue(occupationalNode,
	 * Occupational.class); }
	 * 
	 * public static Pep getPepObjectFromJsonString(String jsonString) throws
	 * JsonParseException, JsonMappingException, IOException { ObjectMapper
	 * mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode pepNode =
	 * jsonNode.get("data"); return mapper.treeToValue(pepNode, Pep.class); }
	 * 
	 * public static Remittance getRemittanceObjectFromJsonString(String
	 * jsonString) throws JsonParseException, JsonMappingException, IOException
	 * { ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode remittanceNode =
	 * jsonNode.get("data"); return mapper.treeToValue(remittanceNode,
	 * Remittance.class); }
	 * 
	 * public static ListRisk getListRiskObjectFromJsonString(String jsonString)
	 * throws JsonParseException, JsonMappingException, IOException {
	 * ObjectMapper mapper = new ObjectMapper(); JsonNode jsonNode =
	 * mapper.readValue(jsonString, JsonNode.class); JsonNode listRiskNode =
	 * jsonNode.get("data"); return mapper.treeToValue(listRiskNode,
	 * ListRisk.class); }
	 */

}
