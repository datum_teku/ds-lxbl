package com.trustaml.service.screening.common.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.screening.common.dao.ScreeningDao;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

@Path("/screening")
public class ScreeningRestfulService {

	@Inject
	ScreeningDao screeningDao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/migrate")
	public Response screeningOfMigratedData(List<ScreeningOfMigratedData> listScreeningOfMigratedData)
			throws Exception {
		screeningDao.screeningOfMigratedData(listScreeningOfMigratedData);
		return ResponseReturn.sucess("ok");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/migration-schedule")
	public Response getScheduleOfMigrationData() throws Exception {
		return ResponseReturn.sucess(screeningDao.getScheduleOfMigrationData());
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/periodic")
	public Response screeningRefresh(List<ScreeningOfMigratedData> listScreeningOfMigratedData) throws Exception {
		// screeningDao.screeningRefresh();
		return ResponseReturn.sucess("ok");
	}

}
