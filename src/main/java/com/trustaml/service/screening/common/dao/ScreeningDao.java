package com.trustaml.service.screening.common.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.screening.legal.model.RequestPrimaryRequestData;
import com.trustaml.service.screening.natural.dao.ScreeningNaturalDaoImpl;
import com.trustaml.service.screening.natural.model.ScreeningNRequest;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;
import com.trustaml.service.screening.natural.util.KYCNDataMigrationJSONNodeMapper;
import com.trustaml.service.toandfro.reply.dao.ReplyAndUpdateDaoImpl;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveLegal;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveNatural;
import com.trustaml.service.toandfro.reply.model.ReplyScreeningLegal;
import com.trustaml.service.toandfro.reply.model.ReplyScreeningNatural;

@Stateless
public class ScreeningDao {

	@Inject
	ScreeningDaoImpl screeningDaoImpl;

	@Inject
	ScreeningNaturalDaoImpl screeningNaturalDaoImpl;

	@Inject
	ReplyAndUpdateDaoImpl replyAndUpdateDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	public void screeningOfMigratedData(List<ScreeningOfMigratedData> listScreeningOfMigratedData) throws Exception {
		long startMilliseconds = System.currentTimeMillis();
		User user = new User();
		long migratedScreening = 0, matchedNotFound = 0;
		long totalMigratedData = 0;
		long alreadyMigratedData = 0;
		String[] sanctionList = { "PEP", "KYCN", "UN Entity", "Hot List", "Investigation", "Domestic Risk",
				"Previous Screening", "Adverse Media", "OFAC Entity", "Accuity", "World Check", "HMT", "EU" };
		List<KycnPersonalInfo> migratedData = null;
		List<Legal> legalMigratedData = null;
		for (ScreeningOfMigratedData screeningOfMigratedData : listScreeningOfMigratedData) {

			user.setUserName("TAML");
			user.setSolId(screeningOfMigratedData.getBranch());
			migratedScreening = 0;
			alreadyMigratedData = 0;
			totalMigratedData = 0;
			matchedNotFound = 0;
			Long scheduleId = screeningDaoImpl.insertScheduleInformation(screeningOfMigratedData);
			if (screeningOfMigratedData.getType().equals("natural")) {
				migratedData = new ArrayList<>();
				migratedData = screeningDaoImpl.fetchDataUsingSolId(screeningOfMigratedData);
				// number of branch screened
				if (migratedData != null) {
					if (migratedData.size() > 0) {
						for (KycnPersonalInfo kycnPersonalInfo : migratedData) {

							String data = screeningDaoImpl.screeningOfMigratedDataAgainstSanctionList(kycnPersonalInfo,
									screeningOfMigratedData);
							if (data == null) {
								System.out.println("No match found system error ");
							} else {
								JsonNode kycnDataMigrationRootNode = KYCNDataMigrationJSONNodeMapper
										.getMainJsonNode(data);
								Map<String, String> mapMatchedSantionList = KYCNDataMigrationJSONNodeMapper
										.getMatchSanctionListMap(kycnDataMigrationRootNode, sanctionList);
								boolean isAlreadyInserted = screeningDaoImpl
										.checkifAlreadyPresentInScreeningWorkflow(kycnPersonalInfo);
								if (mapMatchedSantionList.size() > 0) {
									if (!isAlreadyInserted) {
										Iterator it = mapMatchedSantionList.entrySet().iterator();
										while (it.hasNext()) {
											System.out.println(
													"match found for name: " + kycnPersonalInfo.getFirstName());
											Map.Entry pair = (Map.Entry) it.next();
											migratedScreening++;
											// System.out.println(pair.getKey()
											// + "
											// = " + pair.getValue());
											screeningDaoImpl.saveMatchedDetailWhileMigrating(kycnPersonalInfo, pair);
											it.remove();
										}
										screeningDaoImpl.saveMigratedDataToScreeningNaturalWorkFlow(kycnPersonalInfo,
												scheduleId);
										totalMigratedData++;
										System.out.println(
												"data migrated successfully for " + kycnPersonalInfo.getFirstName());
									} else {
										System.out.println(
												"data already migrated for  " + kycnPersonalInfo.getFirstName());
										alreadyMigratedData++;
									}
								} else {
									System.out.println(
											"no matched found after screening" + kycnPersonalInfo.getFirstName());
									String screeningLegalHashValue = hashCodeGenerator
											.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, user, true);
									Long screeningNaturalId = screeningNaturalDaoImpl
											.saveScreeningNaturalRequest(new ScreeningNRequest(kycnPersonalInfo), user);
									screeningNaturalDaoImpl.updateKycnPersonalInfo(screeningNaturalId, "id",
											kycnPersonalInfo.getId());
									ReplyAndSaveNatural replyAndSaveNatural = new ReplyAndSaveNatural();
									ReplyScreeningNatural replyScreeningNatural = new ReplyScreeningNatural();
									replyScreeningNatural.setScreeningNId(screeningNaturalId);
									replyScreeningNatural.setActionType(ConstantStatus.PROCEED_LOW_RISK);
									replyScreeningNatural.setMigrated(true);
									replyScreeningNatural
											.setReason("CASE OF DATA MIGRATION(AUTO DETECTED) FROM MIGRATION MODULE");
									replyAndSaveNatural.setUser(user);

									replyAndSaveNatural.setReply(replyScreeningNatural);
									// boolean checkIfAlreadyPresentInReplyTable
									// = screeningNaturalDaoImpl
									// .checkIfAlreadyPresentInReplyTable(replyScreeningNatural);
									// if (!checkIfAlreadyPresentInReplyTable) {
									Long replyId = replyAndUpdateDaoImpl.saveNaturalReply(replyAndSaveNatural);
									replyAndUpdateDaoImpl.saveActionAfterReplyForScreening(replyId, replyAndSaveNatural,
											scheduleId);
									// }

									// insert into reply table and insert into
									// action table
									matchedNotFound++;

								}
							}

						}
						long endMilliseconds = System.currentTimeMillis();
						long timeTaken = endMilliseconds - startMilliseconds;
						long diffMinutes = timeTaken / (60);
						System.out.println("total time spend in minute is " + diffMinutes);
						System.out.println("total number of natural screening" + migratedData.size());
						System.out.println("total migrated data " + migratedScreening);
						System.out.println("data already migrated for " + alreadyMigratedData);
						System.out.println("data migrated successfully " + totalMigratedData);
						screeningDaoImpl.saveDataForMigrationReporting(screeningOfMigratedData, migratedScreening,
								alreadyMigratedData, totalMigratedData, migratedData.size(), matchedNotFound, "natural",
								scheduleId);
					}
				} else {
					System.out.println("no data found for branch:: " + screeningOfMigratedData.getBranch());
					screeningDaoImpl.saveDataForMigrationReporting(screeningOfMigratedData, migratedScreening,
							alreadyMigratedData, totalMigratedData, 0, matchedNotFound, "natural", scheduleId);
				}

			} else if (screeningOfMigratedData.getType().equals("legal")) {
				migratedScreening = 0;
				alreadyMigratedData = 0;
				totalMigratedData = 0;
				matchedNotFound = 0;
				String[] sanctionListForLegal = { "UN Entity", "Hot List", "Domestic Risk", "Previous Screening",
						"Adverse Media", "Ofac Entity", "Accuity", "World Check", "HMT", "EU", "Industry Risk",
						"Country Risk" };
				legalMigratedData = new ArrayList<>();
				legalMigratedData = screeningDaoImpl.getMigratedData(screeningOfMigratedData);
				if (legalMigratedData != null) {
					for (Legal legal : legalMigratedData) {
						// total screeninig request

						String data = screeningDaoImpl.screeningOfMigratedDataAgainstSanctionList(legal,
								screeningOfMigratedData);
						if (data != null && !data.isEmpty()) {
							JsonNode kyclDataMigrationRootNode = KYCNDataMigrationJSONNodeMapper.getMainJsonNode(data);

							Map<String, String> mapMatchedSantionList = KYCNDataMigrationJSONNodeMapper
									.getMatchSanctionListMap(kyclDataMigrationRootNode, sanctionListForLegal);
							if (mapMatchedSantionList.size() > 0) {
								boolean isAlreadyInserted = screeningDaoImpl
										.checkifAlreadyPresentInScreeningLegalWorkflow(legal);
								if (!isAlreadyInserted) {
									Iterator it = mapMatchedSantionList.entrySet().iterator();
									while (it.hasNext()) {
										System.out.println("match found for legal" + legal.getNameOfTheInstitution());
										Map.Entry pair = (Map.Entry) it.next();
										// System.out.println(pair.getKey() + "
										// = " + pair.getValue());
										screeningDaoImpl.saveMatchedDetailWhileMigrating(legal, pair);
										it.remove();
									}
									screeningDaoImpl.saveMigratedDataToScreeningLegalWorkFlow(legal, scheduleId);
									totalMigratedData++;
									System.out.println(
											"data migrated successfully for " + legal.getNameOfTheInstitution());
								} else {
									alreadyMigratedData++;
									System.out.println("data already migrated for " + legal.getNameOfTheInstitution());
								}
							} else {
								matchedNotFound++;
								String screeningLegalHashValue = hashCodeGenerator.generateHashValueForKyclInfo(legal,
										true, user);
								Long screeningLegalId = screeningLegalDaoImpl.saveScreeningRequest(
										new RequestPrimaryRequestData(legal), user, screeningLegalHashValue);
								screeningLegalDaoImpl.updateKyclPersonalInfo(screeningLegalId, "id", legal.getId());
								// insert into reply table of legal and insert
								// into action table
								ReplyAndSaveLegal replyAndSaveLegal = new ReplyAndSaveLegal();
								ReplyScreeningLegal replyScreeningLegal = new ReplyScreeningLegal();
								replyScreeningLegal.setScreeningLegalId(screeningLegalId);
								replyScreeningLegal.setActionType(ConstantStatus.PROCEED_LOW_RISK);
								replyScreeningLegal.setMigrated(true);
								replyScreeningLegal
										.setReason("CASE OF DATA MIGRATION(AUTO DETECTED) FROM MIGRATION MODULE");
								replyAndSaveLegal.setUser(user);
								replyAndSaveLegal.setReply(replyScreeningLegal);

								Long replyId = screeningLegalDaoImpl.saveReply(replyAndSaveLegal);
								screeningLegalDaoImpl.saveActionAfterReplyForScreening(replyId, replyAndSaveLegal,
										scheduleId);
							}
						}

					}
					screeningDaoImpl.saveDataForMigrationReporting(screeningOfMigratedData, migratedScreening,
							alreadyMigratedData, totalMigratedData, legalMigratedData.size(), matchedNotFound, "legal",
							scheduleId);
				} else {
					System.out.println("no data found for branch:: " + screeningOfMigratedData.getBranch());
					screeningDaoImpl.saveDataForMigrationReporting(screeningOfMigratedData, migratedScreening,
							alreadyMigratedData, totalMigratedData, 0, matchedNotFound, "legal", scheduleId);
				}
			}

			if (legalMigratedData != null) {
				if (!legalMigratedData.isEmpty()) {
					System.out.println("total number of legal screening" + legalMigratedData.size());
				}
			}

		}

	}

	public Object getScheduleOfMigrationData() throws SQLException {
		return screeningDaoImpl.getListScheduleInformation();

	}

}
