package com.trustaml.service.screening.common.dao;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.screening.natural.model.ScreeningOfMigratedData;

public class ScreeningDaoImpl {

	@Inject
	DBConnection dbConnection;

	public long insertScheduleInformation(ScreeningOfMigratedData screeningOfMigratedData)
			throws SQLException, ParseException {
		long scheduleId = 0;
		String dateRange = screeningOfMigratedData.getDateRange();
		String dateFrom = dateRange.substring(0, dateRange.indexOf("to")).trim();
		String dateTo = dateRange.substring(dateRange.indexOf("to") + 2, dateRange.length()).trim();
		String sql = "INSERT INTO  kyc_migration_schedule (branch,date_from,date_to,match_level,type) VALUES(?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, screeningOfMigratedData.getBranch());
			ps.setDate(2, getDate(dateFrom));
			ps.setDate(3, getDate(dateTo));
			ps.setFloat(4, screeningOfMigratedData.getMatchLevel());
			ps.setString(5, screeningOfMigratedData.getType());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				scheduleId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return scheduleId;

	}

	private static java.sql.Date getDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	public List<KycnPersonalInfo> fetchDataUsingSolId(ScreeningOfMigratedData screeningOfMigratedData)
			throws SQLException, JsonParseException, JsonMappingException, IOException {

		// String sql="call
		// screening_n_match_fields("+kycnPersonalInfo.getFirstName()+"&"+kycnPersonalInfo.getLastName()+")";
		Connection con = null;
		CallableStatement stmt = null;
		List<KycnPersonalInfo> listKycnPersonalInfo = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_n_maker_migrated_data_list(?,?,?,?)}");
			stmt.setString(1, screeningOfMigratedData.getBranch());
			stmt.setDate(2, (java.sql.Date) screeningOfMigratedData.getFromDate());
			stmt.setDate(3, (java.sql.Date) screeningOfMigratedData.getToDate());
			stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			String result = stmt.getString(4);
			if (result != null && !result.isEmpty()) {
				KycnPersonalInfo[] arrayKycn = new ObjectMapper().readValue(result, KycnPersonalInfo[].class);
				// listKycnPersonalInfo = new ArrayList<>();
				listKycnPersonalInfo = Arrays.asList(arrayKycn);
			}
		} finally {
			stmt.close();
			con.close();
		}

		return listKycnPersonalInfo;

	}

	public String screeningOfMigratedDataAgainstSanctionList(KycnPersonalInfo migratedData,
			ScreeningOfMigratedData screeningOfMigratedData) throws SQLException {
		Connection con = null;
		CallableStatement stmt = null;
		String result = "";

		String name = "";
		String firstName = migratedData.getFirstName();
		String middleName = migratedData.getMiddleName();
		String lastName = migratedData.getLastName();

		// NAHARI & KHADKA & KARLI
		if (firstName != null && !firstName.isEmpty()) {
			if (firstName.contains(")")) {
				firstName = firstName.replace(")", "");
			}
			name = firstName.trim();
			name = name.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", " & ");
		}
		if (middleName != null && !middleName.isEmpty()) {
			middleName = middleName.trim();
			name += "&" + middleName.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", " & ").trim();
		}
		if (lastName != null && !lastName.isEmpty()) {
			lastName = lastName.trim();
			name += "&" + lastName.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "").trim();
		}
		if (name != null && !name.isEmpty()) {
			name.replaceAll("\\s+", "");
			try {

				con = dbConnection.getConnection();
				stmt = con.prepareCall("{call screening_n_match_fields_gap(?,?,?)}");
				stmt.setString(1, name);
				stmt.setFloat(2, screeningOfMigratedData.getMatchLevel());
				stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
				stmt.executeUpdate();
				result = stmt.getString(3);
				System.out.println("name of person " + name);
			} finally {
				stmt.close();
				con.close();
			}
		} else {
			System.out.println("name is null");
		}

		return result;
	}

	public boolean checkifAlreadyPresentInScreeningWorkflow(KycnPersonalInfo kycnPersonalInfo) throws SQLException {
		String sql = "select * from screening_n_workflow where screening_n_id=? and status=? and is_migrated=? and kycn_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		String status = "PROCEEDMIGRATEDATATOCHECKER";
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfo.getScreeningId());
			ps.setString(2, status);
			ps.setBoolean(3, true);
			ps.setLong(4, kycnPersonalInfo.getId());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			ps.close();
			connection.close();
		}
		return false;
	}

	public void saveMatchedDetailWhileMigrating(KycnPersonalInfo kycnPersonalInfo, Entry mentry) throws SQLException {
		String sql = "INSERT INTO  match_detail_of_migrated_kyc (match_type,kycn_id,detail) VALUES(?,?,?::json) ";
		Connection connection = null;
		PreparedStatement ps = null;
		// System.out.print("key is: " + mentry.getKey().toString() + " & Value
		// is: ");
		System.out.println(mentry.getValue());
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, mentry.getKey().toString());
			ps.setLong(2, kycnPersonalInfo.getId());
			ps.setString(3, mentry.getValue().toString());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void saveDataForMigrationReporting(ScreeningOfMigratedData screeningOfMigratedData, long migratedScreening,
			long alreadyMigratedData, long totalMigratedData, int noOfScreeningCount, long matchedNotFound,
			String kycType, Long scheduleId) throws SQLException {

		String sql = "INSERT INTO report_of_migrated_data(sol_id,from_date,to_date,total_number_screening,total_number_migrated_screening,total_number_already_migrated,total_number_no_match_found,kyc_type,schedule_id) VALUES(?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;

		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningOfMigratedData.getBranch());
			ps.setDate(2, (java.sql.Date) screeningOfMigratedData.getFromDate());
			ps.setDate(3, (java.sql.Date) screeningOfMigratedData.getToDate());
			ps.setLong(4, noOfScreeningCount);
			ps.setLong(5, totalMigratedData);
			ps.setLong(6, alreadyMigratedData);
			ps.setLong(7, matchedNotFound);
			ps.setString(8, kycType);
			ps.setLong(9, scheduleId);
			// total number of matched data
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	public List<Legal> getMigratedData(ScreeningOfMigratedData screeningOfMigratedData)
			throws SQLException, JsonParseException, JsonMappingException, IOException {
		Connection con = null;
		CallableStatement stmt = null;
		List<Legal> listKycnPersonalInfo = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_l_maker_migrated_data_list(?,?,?,?)}");
			stmt.setString(1, screeningOfMigratedData.getBranch());
			stmt.setDate(2, (java.sql.Date) screeningOfMigratedData.getFromDate());
			stmt.setDate(3, (java.sql.Date) screeningOfMigratedData.getToDate());
			stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			String result = stmt.getString(4);
			if (result != null && !result.isEmpty()) {
				Legal[] arrayKycn = new ObjectMapper().readValue(result, Legal[].class);
				listKycnPersonalInfo = Arrays.asList(arrayKycn);
			}
		} finally {
			stmt.close();
			con.close();
		}

		return listKycnPersonalInfo;
	}

	public String screeningOfMigratedDataAgainstSanctionList(Legal legal,
			ScreeningOfMigratedData screeningOfMigratedData) throws SQLException {
		Connection con = null;
		CallableStatement stmt = null;
		String result = "";

		if (legal.getNameOfTheInstitution() != null && !legal.getNameOfTheInstitution().isEmpty()) {
			try {
				con = dbConnection.getConnection();
				stmt = con.prepareCall("{call screening_l_match_fields_gap(?,?,?)}");
				stmt.setString(1,
						legal.getNameOfTheInstitution().replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", " & "));
				stmt.setFloat(2, screeningOfMigratedData.getMatchLevel());
				stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
				stmt.executeUpdate();
				result = stmt.getString(3);
			} finally {
				stmt.close();
				con.close();
			}
		}

		return result;
	}

	public void saveMigratedDataToScreeningLegalWorkFlow(Legal legal, Long scheduleId) throws SQLException {
		String sql = "INSERT INTO  screening_l_workflow (screening_l_id,status,is_migrated,kycl_id,schedule_id) VALUES(?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		String status = "PROCEEDMIGRATEDATATOCHECKER";
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, legal.getScreeningId());
			ps.setString(2, status);
			ps.setBoolean(3, true);
			ps.setLong(4, legal.getId());
			ps.setLong(5, scheduleId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public boolean checkifAlreadyPresentInScreeningLegalWorkflow(Legal legal) throws SQLException {
		String sql = "SELECT * FROM screening_l_workflow WHERE screening_l_id=? and status=? and is_migrated=? and kycl_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		String status = "PROCEEDMIGRATEDATATOCHECKER";
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, legal.getScreeningId());
			ps.setString(2, status);
			ps.setBoolean(3, true);
			ps.setLong(4, legal.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				return true;
			}
		} finally {
			ps.close();
			connection.close();
		}

		return false;
	}

	public void saveMatchedDetailWhileMigrating(Legal legal, Entry mentry) throws SQLException {
		String sql = "INSERT INTO  match_detail_of_migrated_kyc (match_type,kycl_id,detail) VALUES(?,?,?::json) ";
		Connection connection = null;
		PreparedStatement ps = null;
		// System.out.print("key is: " + mentry.getKey().toString() + " & Value
		// is: ");
		System.out.println(mentry.getValue());
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, mentry.getKey().toString());
			ps.setLong(2, legal.getId());
			ps.setString(3, mentry.getValue().toString());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void saveMigratedDataToScreeningNaturalWorkFlow(KycnPersonalInfo kycnPersonalInfo, Long scheduleId)
			throws SQLException {
		String sql = "INSERT INTO  screening_n_workflow (screening_n_id,status,is_migrated,kycn_id,schedule_id) VALUES(?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		String status = "PROCEEDMIGRATEDATATOCHECKER";
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, kycnPersonalInfo.getScreeningId());
			ps.setString(2, status);
			ps.setBoolean(3, true);
			ps.setLong(4, kycnPersonalInfo.getId());
			ps.setLong(5, scheduleId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public Object getListScheduleInformation() throws SQLException {
		Connection con = null;
		CallableStatement stmt = null;
		String result = "";
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call fetch_data_of_migration(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			con.close();
		}

		return result;
	}

}
