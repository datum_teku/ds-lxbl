package com.trustaml.service.screening.legal.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.model.ApplicationStatus;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDao;

@Path("/screening-legal")
public class ScreeningLRestFulService {

	@Inject
	ScreeningLegalDao screeningLegalDao;

	/**
	 * @param jsonString
	 * @return screening id
	 * @description accepts JSON String and save in screening table
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveScreeningLegal(String jsonString) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty()) {
			ApplicationStatus applicationStatus = screeningLegalDao.saveScreeningLegal(jsonString);
			String str = new ObjectMapper().writeValueAsString(applicationStatus);
			return ResponseReturn.sucess(str);
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

}
