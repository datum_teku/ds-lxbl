package com.trustaml.service.screening.legal.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.common.constant.MatchConstant;
import com.trustaml.service.common.model.ApplicationStatus;
import com.trustaml.service.screening.legal.model.RequestPrimaryMatchedData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityMatchData;
import com.trustaml.service.screening.legal.model.RequestRelatedMatchedPersonData;
import com.trustaml.service.screening.legal.model.ScreeningLegal;
import com.trustaml.service.screening.legal.model.ScreeningLegalRequestAttachments;
import com.trustaml.service.screening.legal.model.ScreeningLegalRequestRelatedEntity;
import com.trustaml.service.screening.legal.model.ScreeningLegalRequestRelatedPerson;

public class ScreeningLegalDao {
	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	@Inject
	ConstantEntity constantEntity;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	public ApplicationStatus saveScreeningLegal(String jsonString) throws JsonParseException, JsonMappingException,
			IOException, SQLException, ParseException, NoSuchAlgorithmException {
		if (constantEntity.getSizeOfLegalLinkTable() == 0) {
			constantEntity.setlegalLinkTable();
		}
		if (constantEntity.getSizeOfNaturalLinkTable() == 0) {
			constantEntity.setNaturalLinkTable();
		}
		if (constantEntity.getSizeOfLegalLinkColumnName() == 0) {
			constantEntity.setlegalLinkColumnName();
		}
		if (constantEntity.getSizeOfNaturalLinkColumnName() == 0) {
			constantEntity.setNaturalLinkColumnName();
		}

		int matchCase = 0;
		boolean screeningCompleted = true;
		boolean screeningRequested = true;
		boolean isRejected = true;
		ScreeningLegal screeningLegal = new ObjectMapper().readValue(jsonString, ScreeningLegal.class);
		String screeningLegalHashValue = hashCodeGenerator.generateHashValueForScreeningLegal(
				screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData(),
				screeningLegal.getUser());
		Long screeningId = screeningLegalDaoImpl.saveScreeningRequest(
				screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData(),
				screeningLegal.getUser(), screeningLegalHashValue);
		if(!screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
			if (matchCase == 0) {
				matchCase = 2;
			}
		}
		
		// LIST OF MATCH CASE MATCHED CASE
		for (RequestPrimaryMatchedData requestPrimaryMatchedData : screeningLegal.getScreeningLegalRequestPrimary()
				.getListRequestPrimaryMatchedData()) {
			if (requestPrimaryMatchedData.getType().equals(MatchConstant.UN_MATCH_CASE_LEGAL)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.OFAC_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.ACCUITY_SDN_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.HMT_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.EU_ENTITY_MATCH_CASE)) {
				// application rejected
				matchCase = 1;
			} else if (requestPrimaryMatchedData.getType().equals(MatchConstant.PEP_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.ADVERSE_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.HOT_LIST_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.INVESTIGATION_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.ACCUITY_PEP_ENTITY_MATCH_CASE)
					|| requestPrimaryMatchedData.getType().equals(MatchConstant.ACCUITY_ADVERSE_ENTITY_MATCH_CASE)) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}

			// GETTING TABLE NAME FROM DATABASE ACCORDING TO TYPE OF MATCH CASE
			String tableName = constantEntity.getLegalTableName(requestPrimaryMatchedData.getType());

			String requestPrimaryMatchedDataHashValue = hashCodeGenerator
					.generateHashValueForRequestPrimaryMatchedData(requestPrimaryMatchedData, screeningId, tableName);
			screeningLegalDaoImpl.saveMatchedRequest(requestPrimaryMatchedData, screeningId, tableName,
					requestPrimaryMatchedDataHashValue);
		}
		// REQUEST RELATED ENTITY
		for (ScreeningLegalRequestRelatedEntity screeningLegalRequestRelatedEntity : screeningLegal
				.getScreeningLegalRequestRelatedEntity()) {
			// Save in request
			String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
					screeningLegalRequestRelatedEntity.getRequestRelatedEntityRequestData(), screeningId,
					screeningLegal.getUser());
			Long requestRelatedId = screeningLegalDaoImpl.saveRequest(
					screeningLegalRequestRelatedEntity.getRequestRelatedEntityRequestData(), screeningLegal.getUser(),
					screeningLegalRequestEntityHashValue);
			
			if(!screeningLegalRequestRelatedEntity.getRequestRelatedEntityRequestData()
					.getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}

			// save screening_l_related

			String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
					.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningId,
							screeningLegalRequestRelatedEntity.getRequestRelatedEntityRequestData());
			screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningId,
					screeningLegalRequestRelatedEntity.getRequestRelatedEntityRequestData(),
					screeningLegalRequestRelatedEntityHashValue);
			
			
			
			// RELATED ENTITY MATCH CASE UPDATE THE REQUEST LEGAL DATA
			for (RequestRelatedEntityMatchData requestRelatedEntityMatchData : screeningLegalRequestRelatedEntity
					.getListRequestRelatedEntityMatchData()) {
				if (requestRelatedEntityMatchData.getType().equals(MatchConstant.UN_MATCH_CASE_LEGAL)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.OFAC_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.HMT_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.ACCUITY_SDN_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.EU_ENTITY_MATCH_CASE)) {
					matchCase = 1;
				} else if (requestRelatedEntityMatchData.getType().equals(MatchConstant.PEP_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.ADVERSE_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.HOT_LIST_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.INVESTIGATION_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.ACCUITY_PEP_ENTITY_MATCH_CASE)
						|| requestRelatedEntityMatchData.getType().equals(MatchConstant.ACCUITY_ADVERSE_ENTITY_MATCH_CASE)) {
					if (matchCase == 0) {
						matchCase = 2;
					}
				}
				// GETTING TABLE NAME FROM DATABASE ACCORDING TO TYPE OF MATCH
				// CASE
				String tableName = constantEntity.getLegalTableName(requestRelatedEntityMatchData.getType());

				String screeningLegalRequestRelatedMatchedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedMatchedRelatedEntity(requestRelatedEntityMatchData,
								requestRelatedId, tableName);
				screeningLegalDaoImpl.saveRelatedEntityMatchedData(requestRelatedEntityMatchData, requestRelatedId,
						tableName, screeningLegalRequestRelatedMatchedEntityHashValue);
			}
		}
		// REQUEST RELATED PERSON WITH MATCHED CASE
		for (ScreeningLegalRequestRelatedPerson screeningLegalRequestRelatedPerson : screeningLegal
				.getScreeningLegalRequestRelatedPerson()) {

			String screeningLegalRequestRelatedPersonHashValue = hashCodeGenerator
					.generateHashValueForReqestRelatedPerson(
							screeningLegalRequestRelatedPerson.getRequestRelatedPerson(), screeningLegal.getUser());
			Long relatedPersonScreeningId = screeningLegalDaoImpl.saveRelatedPerson(
					screeningLegalRequestRelatedPerson.getRequestRelatedPerson(), screeningLegal.getUser(),
					screeningLegalRequestRelatedPersonHashValue);
			
			if(!screeningLegalRequestRelatedPerson.getRequestRelatedPerson()
					.getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}
			
			// INSERT ANOTHER ROW TO screening_l_related

			String screeningLegalRelatedPersonHashValue = hashCodeGenerator.generateHashValueForRelatedPerson(
					screeningId, screeningLegalRequestRelatedPerson.getRequestRelatedPerson().getAccountsLSubType(),
					relatedPersonScreeningId);
			screeningLegalDaoImpl.saveRequestRelatedEntity1(screeningId,
					screeningLegalRequestRelatedPerson.getRequestRelatedPerson().getAccountsLSubType(),
					relatedPersonScreeningId, screeningLegalRelatedPersonHashValue);
			// LIST OF MATCH CASE MATCHED CASE
			for (RequestRelatedMatchedPersonData requestRelatedMatchedPersonData : screeningLegalRequestRelatedPerson
					.getListRequestRelatedMatchedPersonData()) {

				if (requestRelatedMatchedPersonData.getType().equals(MatchConstant.UN_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.OFAC_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.HMT_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.EU_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.ACCUITY_SDN_MATCH_CASE)) {
					matchCase = 1;
				} else if (requestRelatedMatchedPersonData.getType().equals(MatchConstant.PEP_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.ADVERSE_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.HOT_LIST_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.INVESTIGATION_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.ACCUITY_MATCH_CASE)
						|| requestRelatedMatchedPersonData.getType().equals(MatchConstant.ACCUITY_ADVERSE_MATCH_CASE)) {
					if (matchCase == 0) {
						matchCase = 2;
					}
				}
				// GETTING TABLE NAME FROM DATABASE ACCORDING TO TYPE OF MATCH
				// CASE
				String tableName = constantEntity.getNaturalTableName(requestRelatedMatchedPersonData.getType());
				String columnName = constantEntity.getNaturalColumnName(requestRelatedMatchedPersonData.getType());

				String screeningLegalRelatedMatchedPersonHashValue = hashCodeGenerator
						.generateHashValueForRelatedMatchedPerson(requestRelatedMatchedPersonData,
								relatedPersonScreeningId, tableName);
				screeningLegalDaoImpl.saveMatchedRelatedPerson(requestRelatedMatchedPersonData,
						relatedPersonScreeningId, tableName, columnName, screeningLegalRelatedMatchedPersonHashValue);
			}
		}

		for (ScreeningLegalRequestAttachments screeningLegalRequestAttachments : screeningLegal
				.getListScreeningLegalRequestAttachments()) {
			String screeningLegalAttachmentHashValue = hashCodeGenerator
					.generateHashValueForAttachment(screeningLegalRequestAttachments, screeningId);
			screeningLegalDaoImpl.saveLegalAttachments(screeningLegalRequestAttachments, screeningId,
					screeningLegalAttachmentHashValue);
			String status = ConstantStatus.PROCEED_MAKER_CHECKER;
			String screeningLegalWorkFlowHashValue = hashCodeGenerator.generateHashValueForWorkFlowTable(status,
					screeningId, true);

			screeningLegalDaoImpl.insertIntoScreeningWorkFlowTable(status, screeningId,
					screeningLegalWorkFlowHashValue);
		}

		for (String forwardTo : screeningLegal.getForwardTo()) {
			screeningLegalDaoImpl.saveForwardToUser(screeningId, forwardTo, screeningLegal.getAction(),
					screeningLegal.getUser());
		}
		ApplicationStatus applicationStatus = new ApplicationStatus();
		if (matchCase == 0) {
			// NO match case
			// /Screening Completed
			applicationStatus.setMessage(1);
			screeningRequested = false;
			isRejected = false;
			screeningLegalDaoImpl.insertIntoScreeningLegalWorkFlowHighRiskCustomer(ConstantStatus.PROCEED_CHECKER_MAKER,
					screeningId, "hash", screeningCompleted, screeningRequested, isRejected);

		} else if (matchCase == 1) {
			// OFAC AND UN match case
			// Rejected
			applicationStatus.setMessage(2);
			screeningCompleted = false;
			screeningRequested = false;
			screeningLegalDaoImpl.insertIntoScreeningLegalWorkFlowHighRiskCustomer(ConstantStatus.PROCEED_CHECKER_MAKER,
					screeningId, "hash", screeningCompleted, screeningRequested, isRejected);

		} else if (matchCase == 2) {
			// OTHER MATCH CASE AND UN match case
			// Procced to Checker
			applicationStatus.setMessage(3);
			screeningCompleted = false;
			isRejected = false;
			screeningLegalDaoImpl.insertIntoScreeningLegalWorkFlowHighRiskCustomer(ConstantStatus.PROCEED_MAKER_CHECKER,
					screeningId, "hash", screeningCompleted, screeningRequested, isRejected);
		}

		if (screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().isMigrated()
				&& (screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getKyclId() != 0
						|| !screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getCustId()
								.isEmpty())) {
			if (screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getKyclId() != 0) {
				screeningLegalDaoImpl.updateKyclPersonalInfo(screeningId, "id",
						screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getKyclId());
			} else if (!screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getCustId()
					.isEmpty()) {
				screeningLegalDaoImpl.updateKyclPersonalInfo(screeningId, "cust_id",
						screeningLegal.getScreeningLegalRequestPrimary().getRequestPrimaryRequestData().getCustId());
			}

		}
		applicationStatus.setId(screeningId);
		return applicationStatus;

	}

}
