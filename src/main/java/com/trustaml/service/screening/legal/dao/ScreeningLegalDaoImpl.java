package com.trustaml.service.screening.legal.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.screening.legal.model.RequestPrimaryMatchedData;
import com.trustaml.service.screening.legal.model.RequestPrimaryRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityMatchData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedMatchedPersonData;
import com.trustaml.service.screening.legal.model.RequestRelatedPerson;
import com.trustaml.service.screening.legal.model.ScreeningLegalRequestAttachments;
import com.trustaml.service.screening.natural.model.ScreeningNRelatedRequest;
import com.trustaml.service.toandfro.reply.model.AccountReply;
import com.trustaml.service.toandfro.reply.model.AttachmentInfo;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveLegal;

public class ScreeningLegalDaoImpl {
	@Inject
	DBConnection dbConnection;

	/**
	 * @param requestPrimaryRequestData
	 * @param user
	 * @param screeningLegalHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long saveScreeningRequest(RequestPrimaryRequestData requestPrimaryRequestData, User user,
			String screeningLegalHashValue) throws SQLException {

		Long screeningId = 0L;

		String sql = "INSERT INTO screening_l_request"
				+ "(name_of_institution,ls_name,date_of_establishment,country_of_issue,state,district,"
				+ "mn_vdc,type_of_industry,branch_sol_id,cust_id,status,purpose_of_screening,"
				+ " risk_value,zone,ward_no,pan_number,contact_number,email_id,nature_of_account,"
				+ "scheme_description,deposit_amount,notes,has_kyc,kycl_id,maker,account_sub_type,has_cust_id,hash,swift_input,province,date_of_establishment_bs,"
				+ "registration_number,read_only) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, requestPrimaryRequestData.getNameOfInstitution());
			ps.setString(2, requestPrimaryRequestData.getLsName());
			ps.setDate(3, requestPrimaryRequestData.getDateOfEstablishment());
			ps.setString(4, requestPrimaryRequestData.getCountryOfIssue());
			ps.setString(5, requestPrimaryRequestData.getState());
			ps.setString(6, requestPrimaryRequestData.getDistrict());
			ps.setString(7, requestPrimaryRequestData.getMnVdc());
			ps.setString(8, requestPrimaryRequestData.getTypeOfIndustry());
			ps.setString(9, user.getSolId());
			ps.setString(10, requestPrimaryRequestData.getCustId());
			ps.setString(11, requestPrimaryRequestData.getStatus());
			ps.setString(12, requestPrimaryRequestData.getPurposeOfScreening());
			ps.setLong(13, requestPrimaryRequestData.getRiskValue());
			ps.setString(14, requestPrimaryRequestData.getZone());
			ps.setString(15, requestPrimaryRequestData.getWardNo());
			ps.setString(16, requestPrimaryRequestData.getPanNumber());
			ps.setString(17, requestPrimaryRequestData.getContactNumber());
			ps.setString(18, requestPrimaryRequestData.getEmailId());
			ps.setString(19, requestPrimaryRequestData.getNatureOfAccount());
			ps.setString(20, requestPrimaryRequestData.getSchemeDescription());
			ps.setString(21, requestPrimaryRequestData.getDepositAmount());
			ps.setString(22, requestPrimaryRequestData.getNotes());
			ps.setBoolean(23, requestPrimaryRequestData.isHaskyc());
			ps.setLong(24, requestPrimaryRequestData.getKyclId());
			ps.setString(25, user.getUserName());
			ps.setString(26, requestPrimaryRequestData.getAccountsLSubType());
			ps.setBoolean(27, requestPrimaryRequestData.isHasCustId());
			ps.setString(28, screeningLegalHashValue);
			ps.setString(29, requestPrimaryRequestData.getSwiftInput());
			ps.setString(30, requestPrimaryRequestData.getProvince());
			ps.setDate(31, requestPrimaryRequestData.getDateOfEstablishmentBS());
			ps.setString(32, requestPrimaryRequestData.getRegistrationNo());
			ps.setBoolean(33, false);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				screeningId = rs.getLong(1);
			}
		} finally {
			ps.close();
			conn.close();
		}
		return screeningId;

	}

	/**
	 * @param requestPrimaryMatchedData
	 * @param screeningId
	 * @param tableName
	 * @param requestPrimaryMatchedDataHashValue
	 * @throws SQLException
	 */

	public void saveMatchedRequest(RequestPrimaryMatchedData requestPrimaryMatchedData, Long screeningId,
			String tableName, String requestPrimaryMatchedDataHashValue) throws SQLException {
		String sql = "INSERT INTO " + tableName + "(screening_l_request_id,linked_id,hash,risk_value) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestPrimaryMatchedData.getId());
			ps.setString(3, requestPrimaryMatchedDataHashValue);
			ps.setInt(4, requestPrimaryMatchedData.getRisk());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();

		}
	}

	/**
	 * @param requestRelatedEntityRequestData
	 * @param user
	 * @param screeningLegalRequestEntityHashValue
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public Long saveRequest(RequestRelatedEntityRequestData requestRelatedEntityRequestData, User user,
			String screeningLegalRequestEntityHashValue) throws SQLException, ParseException {
		Long requestId = 0L;
		String sql = "INSERT INTO screening_l_request (name_of_institution,ls_name,date_of_establishment,district,"
				+ "mn_vdc,type_of_industry,cust_id, zone,ward_no,pan_number,contact_number,email_id,"
				+ "notes,kycl_id,maker,account_sub_type,registration_number,country_of_issue,has_cust_id,has_kyc,hash,date_of_establishment_bs"
				+ ",province) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, requestRelatedEntityRequestData.getNameOfInstitution());
			ps.setString(2, requestRelatedEntityRequestData.getLsName());
			if (requestRelatedEntityRequestData.getDateOfEstablishment() != null
					&& !requestRelatedEntityRequestData.getDateOfEstablishment().equals("")) {
				ps.setDate(3, getBirthDate(requestRelatedEntityRequestData.getDateOfEstablishment()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			ps.setString(4, requestRelatedEntityRequestData.getDistrict());
			ps.setString(5, requestRelatedEntityRequestData.getMnVdc());
			ps.setString(6, requestRelatedEntityRequestData.getTypeOfIndustry());
			ps.setString(7, requestRelatedEntityRequestData.getCustId());
			ps.setString(8, requestRelatedEntityRequestData.getZone());
			ps.setString(9, requestRelatedEntityRequestData.getWardNo());
			ps.setString(10, requestRelatedEntityRequestData.getPanNumber());
			ps.setString(11, requestRelatedEntityRequestData.getContactNumber());
			ps.setString(12, requestRelatedEntityRequestData.getEmailId());
			ps.setString(13, requestRelatedEntityRequestData.getNotes());
			ps.setString(14, requestRelatedEntityRequestData.getKyclId());
			ps.setString(15, user.getUserName());
			ps.setString(16, requestRelatedEntityRequestData.getAccountsLSubType());
			ps.setString(17, requestRelatedEntityRequestData.getRegistrationNo());
			ps.setString(18, requestRelatedEntityRequestData.getCountryOfIssue());
			ps.setBoolean(19, requestRelatedEntityRequestData.isHasCustId());
			ps.setBoolean(20, requestRelatedEntityRequestData.isHaskyc());
			ps.setString(21, screeningLegalRequestEntityHashValue);
			ps.setDate(22, (java.sql.Date) requestRelatedEntityRequestData.getDateOfEstablishmentBS());
			ps.setString(23, requestRelatedEntityRequestData.getProvince());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestId = rs.getLong(1);
			}
		} finally {
			ps.close();
			conn.close();
		}
		return requestId;

	}

	/**
	 * @param requestRelatedEntityMatchData
	 * @param screeningId
	 * @param tableName
	 * @param screeningLegalRequestRelatedMatchedEntityHashValue
	 * @throws SQLException
	 */

	public void saveRelatedEntityMatchedData(RequestRelatedEntityMatchData requestRelatedEntityMatchData,
			Long screeningId, String tableName, String screeningLegalRequestRelatedMatchedEntityHashValue)
			throws SQLException {
		String sql = "INSERT INTO " + tableName + "(screening_l_request_id,linked_id,hash,risk_value) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestRelatedEntityMatchData.getId());
			ps.setString(3, screeningLegalRequestRelatedMatchedEntityHashValue);
			ps.setInt(4, requestRelatedEntityMatchData.getRisk());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param requestRelatedPerson
	 * @param validatedUser
	 * @param screeningLegalRequestRelatedPersonHashValue
	 * @return
	 * @throws SQLException
	 */

	public Long saveRelatedPerson(RequestRelatedPerson requestRelatedPerson, User validatedUser,
			String screeningLegalRequestRelatedPersonHashValue) throws SQLException {
		Long relatedId = 0L;
		String sql = "INSERT INTO  screening_n_request (first_name, middle_name, last_name, lsf_name, "
				+ "lsm_name, lsl_name, date_of_birth, country_of_issue, district, "
				+ "mn_vdc, maker, branch_sol_id, kyc_id, account_sub_type,notes,zone,"
				+ "salutation,gender,mobile_number,email_id,has_kyc,has_cust_id,cust_id,hash,date_of_birth_bs"
				+ ",primary_identification_type,primary_identification_no,province,ward_no,pan_number) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, requestRelatedPerson.getFirstName());
			ps.setString(2, requestRelatedPerson.getMiddleName());
			ps.setString(3, requestRelatedPerson.getLastName());
			ps.setString(4, requestRelatedPerson.getLsfName());
			ps.setString(5, requestRelatedPerson.getLsmName());
			ps.setString(6, requestRelatedPerson.getLslName());
			ps.setDate(7, requestRelatedPerson.getDateOfBirth());
			ps.setString(8, requestRelatedPerson.getCountryOfIssue());
			ps.setString(9, requestRelatedPerson.getDistrict());
			ps.setString(10, requestRelatedPerson.getMnVdc());
			ps.setString(11, validatedUser.getUserName());
			ps.setString(12, validatedUser.getSolId());
			ps.setLong(13, requestRelatedPerson.getKycnId());
			ps.setString(14, requestRelatedPerson.getAccountsLSubType());
			ps.setString(15, requestRelatedPerson.getNotes());
			ps.setString(16, requestRelatedPerson.getZone());
			ps.setString(17, requestRelatedPerson.getSalutation());
			ps.setString(18, requestRelatedPerson.getGender());
			ps.setString(19, requestRelatedPerson.getMobileNumber());
			ps.setString(20, requestRelatedPerson.getEmailId());
			ps.setBoolean(21, requestRelatedPerson.isHaskycl());
			ps.setBoolean(22, requestRelatedPerson.isHasCustId());
			ps.setString(23, requestRelatedPerson.getCustId());
			ps.setString(24, screeningLegalRequestRelatedPersonHashValue);
			ps.setDate(25, requestRelatedPerson.getRelatedDateOfBirthBS());
			ps.setString(26, requestRelatedPerson.getPrimaryIdentificationDocumentType());
			ps.setString(27, requestRelatedPerson.getPrimaryIdentificationDocumentNo());
			ps.setString(28, requestRelatedPerson.getProvince());
			ps.setString(29, requestRelatedPerson.getWardNo());
			ps.setString(30, requestRelatedPerson.getPanNumber());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				relatedId = rs.getLong(1);
			}
		} finally {
			conn.close();

		}
		return relatedId;
	}

	/**
	 * @param requestRelatedMatchedPersonData
	 * @param screeningId
	 * @param tableName
	 * @param columnName
	 * @param screeningLegalRelatedMatchedPersonHashValue
	 * @throws SQLException
	 */
	public void saveMatchedRelatedPerson(RequestRelatedMatchedPersonData requestRelatedMatchedPersonData,
			Long screeningId, String tableName, String columnName, String screeningLegalRelatedMatchedPersonHashValue)
			throws SQLException {
		String sql = null;
		if (tableName.equals("screening_n_request_hot_list_link")
				|| tableName.equals("screening_n_request_list_investigation_info_link")) {
			sql = "INSERT INTO " + tableName + "(screening_n_request_id," + columnName + ",hash,risk_value) "
					+ "VALUES (?,?,?,?)";
		} else {
			sql = "INSERT INTO " + tableName + "(screening_request_n_id," + columnName + ",hash,risk_value) "
					+ "VALUES (?,?,?,?)";
		}
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestRelatedMatchedPersonData.getId());
			ps.setString(3, screeningLegalRelatedMatchedPersonHashValue);
			ps.setInt(4, requestRelatedMatchedPersonData.getRisk());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param screeningLegalRequestAttachment
	 * @param screeningId
	 * @param screeningLegalAttachmentHashValue
	 * @throws SQLException
	 */
	public void saveLegalAttachments(ScreeningLegalRequestAttachments screeningLegalRequestAttachment, Long screeningId,
			String screeningLegalAttachmentHashValue) throws SQLException {
		String sql = "INSERT INTO screening_l_attachment(screening_l_request_id,scanned_document_type,scanned_content,extension,notes,hash) "
				+ "VALUES (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setString(2, screeningLegalRequestAttachment.getScannedDocumentType());
			ps.setBytes(3, screeningLegalRequestAttachment.getScannedContent().getBytes());
			ps.setString(4, screeningLegalRequestAttachment.getExtensionText());
			ps.setString(5, screeningLegalRequestAttachment.getNotes());
			ps.setString(6, screeningLegalAttachmentHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param requestId
	 * @param screeningId
	 * @param requestRelatedEntityRequestData
	 * @param screeningLegalRequestRelatedEntityHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long saveRequestRelatedEntity(Long requestId, Long screeningId,
			RequestRelatedEntityRequestData requestRelatedEntityRequestData,
			String screeningLegalRequestRelatedEntityHashValue) throws SQLException {
		Long requestRealtedId = 0L;
		String sql = "INSERT INTO screening_l_related(primary_screening_l_id,related_screening_l_id,related_type,notes,hash) "
				+ "VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestId);
			ps.setString(3, requestRelatedEntityRequestData.getAccountsLSubType());
			ps.setString(4, requestRelatedEntityRequestData.getNotes());
			ps.setString(5, screeningLegalRequestRelatedEntityHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestRealtedId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}

		return requestRealtedId;
	}

	/**
	 * @param screeningId
	 * @param accountsLSubType
	 * @param relatedScreeningId
	 * @param screeningLegalRelatedPersonHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long saveRequestRelatedEntity1(Long screeningId, String accountsLSubType, Long relatedScreeningId,
			String screeningLegalRelatedPersonHashValue) throws SQLException {
		Long requestRealtedId = 0L;
		String sql = "INSERT INTO screening_l_related(primary_screening_l_id,related_type,notes,related_screening_n_id,hash) "
				+ "VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setString(2, accountsLSubType);
			ps.setString(3, "");
			ps.setLong(4, relatedScreeningId);
			ps.setString(5, screeningLegalRelatedPersonHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestRealtedId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return requestRealtedId;
	}

	/**
	 * @param screeningLegalId
	 * @throws SQLException
	 */

	public void insertScreeningWorkflow(Long screeningLegalId) throws SQLException {
		String sql = "INSERT INTO screening_l_workflow(screening_l_id,screening_l_requested,status) "
				+ "VALUES (?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningLegalId);
			ps.setBoolean(2, true);
			ps.setString(3, ConstantStatus.PROCEED_MAKER_CHECKER);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param scCompleted
	 * @param screeningLegalId
	 * @param columnName
	 * @param workflowTableHashValue
	 * @throws SQLException
	 */
	public void updateWorkflowTable(String status, boolean scCompleted, long screeningLegalId, String columnName,
			String workflowTableHashValue) throws SQLException {
		String sql = "UPDATE screening_l_workflow SET status=?," + columnName + "=?,hash=? WHERE screening_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setBoolean(2, scCompleted);
			ps.setString(3, workflowTableHashValue);
			ps.setLong(4, screeningLegalId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param scCompleted
	 * @param screeningNaturalId
	 * @param columnName
	 * @throws SQLException
	 */

	public void updateWorkflowTableScreeningNatural(String status, boolean scCompleted, long screeningNaturalId,
			String columnName) throws SQLException {
		String sql = "UPDATE screening_n_workflow SET status=?," + columnName + "=? WHERE screening_n_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setBoolean(2, scCompleted);
			ps.setLong(3, screeningNaturalId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param screeningCompleted
	 * @param workflowCompleted
	 * @param screeningLId
	 * @throws SQLException
	 */
	public void updateWorkflowTableCompleted(String status, boolean screeningCompleted, boolean workflowCompleted,
			long screeningLId) throws SQLException {
		String sql = "UPDATE screening_l_workflow SET status=?,screening_completed=?,workflow_completed=? WHERE screening_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setBoolean(2, screeningCompleted);
			ps.setBoolean(3, workflowCompleted);
			ps.setLong(4, screeningLId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param screeningCompleted
	 * @param workflowCompleted
	 * @param screeningNaturalId
	 * @throws SQLException
	 */
	public void updateWorkflowTableCompletedScreeningNatural(String status, boolean screeningCompleted,
			boolean workflowCompleted, long screeningNaturalId) throws SQLException {
		String sql = "UPDATE screening_n_workflow SET status=?,screening_completed=?,workflow_completed=? WHERE screening_n_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setBoolean(2, screeningCompleted);
			ps.setBoolean(3, workflowCompleted);
			ps.setLong(4, screeningNaturalId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningLId
	 * @return
	 * @throws SQLException
	 */
	public String getScreeningLegalRequestObject(long screeningLId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call  screening_l_request_details_for_accounts(?,?)}");
			stmt.setLong(1, screeningLId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	/**
	 * @param requestPrimaryData
	 * @param screeningLegalId
	 * @param hashValue
	 * @return
	 * @throws SQLException
	 */

	public Long insertIntoAccountRequest(RequestPrimaryRequestData requestPrimaryData, long screeningLegalId,
			long hashValue) throws SQLException {
		long accountId = 0;
		String sql = "INSERT INTO accounts_l_request(screening_l_request_id,hash) VALUES (?,?)";
		// Relation and Entity ko nominee le garda db ko field ma dherai kura
		// insert gareko xaina

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, screeningLegalId);
			ps.setString(2, String.valueOf(hashValue));
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				accountId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();

		}
		return accountId;

	}

	/**
	 * @param status
	 * @param accountId
	 * @throws SQLException
	 */

	public void insertAccountLegalWorkFlow(String status, Long accountId) throws SQLException {
		String sql = "INSERT INTO accounts_l_workflow(accounts_l_id,status) VALUES (?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, accountId);
			ps.setString(2, status);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param accountLegalReply
	 * @param accountReplyHashValue
	 * @return
	 * @throws SQLException
	 */

	public long insertAccountLegalReply(AccountReply accountLegalReply, String accountReplyHashValue)
			throws SQLException {

		long accountLegalReplyId = 0;
		String sql = "INSERT INTO accounts_l_reply(accounts_l_request_id,cap,action_taken,hash) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, accountLegalReply.getAccountLegalReply().getAccountsLegalId());
			ps.setString(2, accountLegalReply.getUser().getUserName());
			ps.setBoolean(3, true);
			ps.setString(4, accountReplyHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				accountLegalReplyId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return accountLegalReplyId;

	}

	/**
	 * @param accountLegalReplyId
	 * @param accountLegalReply
	 * @param accountLegalActionHashValue
	 * @throws SQLException
	 */
	public void insertIntoAccountLegalAction(Long accountLegalReplyId, AccountReply accountLegalReply,
			String accountLegalActionHashValue) throws SQLException {
		String sql = "INSERT INTO accounts_l_action(accounts_l_reply_id,cap,reason,action_type,action_sub_type,cust_id,account_number,cbs_generated_name,hash) VALUES (?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, accountLegalReplyId);
			ps.setString(2, accountLegalReply.getUser().getUserName());
			ps.setString(3, accountLegalReply.getAccountLegalReply().getReason());
			ps.setString(4, accountLegalReply.getAccountLegalReply().getActionType());
			ps.setString(5, accountLegalReply.getAccountLegalReply().getActionSubType());
			ps.setString(6, accountLegalReply.getAccountLegalReply().getGeneratedCustomerId());
			ps.setString(7, accountLegalReply.getAccountLegalReply().getForacId());
			ps.setString(8, accountLegalReply.getAccountLegalReply().getCustName());
			ps.setString(9, accountLegalActionHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param accountLegalReply
	 * @param updateLegalRequestHashValue
	 * @throws SQLException
	 */

	public void updateAccountLegalRequest(AccountReply accountLegalReply, String updateLegalRequestHashValue)
			throws SQLException {
		String sql = "UPDATE accounts_l_request SET cust_id=?,account_number= ?,hash=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, accountLegalReply.getAccountLegalReply().getGeneratedCustomerId());
			ps.setString(2, accountLegalReply.getAccountLegalReply().getForacId());
			ps.setString(3, updateLegalRequestHashValue);
			ps.setLong(4, accountLegalReply.getAccountLegalReply().getAccountsLegalId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param accountOpened
	 * @param accountCompleted
	 * @param workflowCompleted
	 * @param legalAccountId
	 * @param freeze
	 * @param acoountRequested
	 * @param requestToUnfreeze
	 * @param aofReceived
	 * @param documentSent
	 * @param updateLegalWorkflowHashValue
	 * @throws SQLException
	 */
	public void updateAccountLegalWorkflow(String status, boolean accountOpened, boolean accountCompleted,
			boolean workflowCompleted, long legalAccountId, boolean freeze, boolean acoountRequested,
			boolean requestToUnfreeze, boolean aofReceived, boolean documentSent, String updateLegalWorkflowHashValue)
			throws SQLException {
		String sql = "UPDATE accounts_l_workflow SET status=?,account_opened=?,accounts_completed=?,workflow_completed=?,freezes=?,accounts_requested=?,request_to_unfreeze=?,aof_received=?,documents_sent=?,hash=? WHERE accounts_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setBoolean(2, accountOpened);
			ps.setBoolean(3, accountCompleted);
			ps.setBoolean(4, workflowCompleted);
			ps.setBoolean(5, freeze);
			ps.setBoolean(6, acoountRequested);
			ps.setBoolean(7, requestToUnfreeze);
			ps.setBoolean(8, aofReceived);
			ps.setBoolean(9, documentSent);
			ps.setString(10, updateLegalWorkflowHashValue);
			ps.setLong(11, legalAccountId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningId
	 * @param screeningLegalUpdateAfterRepairHashValue
	 * @throws SQLException
	 */
	public void updateWorkflowTableAfterRepair(long screeningId, String screeningLegalUpdateAfterRepairHashValue)
			throws SQLException {
		String sql = "UPDATE screening_l_workflow SET repaired=?,hash=? WHERE screening_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setString(2, screeningLegalUpdateAfterRepairHashValue);
			ps.setLong(3, screeningId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningId
	 * @param repairedId
	 * @param screeningLegalPreviousCrrentScreeningMappingHashValue
	 * @throws SQLException
	 */
	public void insertPreviousCurrentScreeningIdMapping(Long screeningId, long repairedId,
			String screeningLegalPreviousCrrentScreeningMappingHashValue) throws SQLException {
		String sql = "INSERT INTO table_current_previous_screening_id_mapping(prv_screening_id,new_screening_id,hash) VALUES (?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, repairedId);
			ps.setLong(2, screeningId);
			ps.setString(3, screeningLegalPreviousCrrentScreeningMappingHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param screeningId
	 * @param screeningLegalWorkFlowHashValue
	 * @throws SQLException
	 */
	public void insertIntoScreeningWorkFlowTable(String status, Long screeningId,
			String screeningLegalWorkFlowHashValue) throws SQLException {
		String sql = "INSERT INTO screening_l_workflow(status,screening_l_id,screening_l_requested,hash) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, screeningId);
			ps.setBoolean(3, true);
			ps.setString(4, screeningLegalWorkFlowHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param accountLegalLastActionId
	 * @throws SQLException
	 */
	public void updateCompleteDocumentsFlag(long accountLegalLastActionId) throws SQLException {
		String sql = "UPDATE accounts_l_action SET complete_documents=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, accountLegalLastActionId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param accountLegalRequestId
	 * @param requestToUnfreezeStatus
	 * @throws SQLException
	 */
	public void updateRequestToUnfreezeFlag(long accountLegalRequestId, boolean requestToUnfreezeStatus)
			throws SQLException {
		String sql = "UPDATE accounts_l_workflow SET request_to_unfreeze=? WHERE accounts_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(2, accountLegalRequestId);
			ps.setBoolean(1, requestToUnfreezeStatus);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param accountLegalRequestId
	 * @param dueDateOfPendingDocument
	 * @param dueDateComments
	 * @param accountLegalLastActionId
	 * @param attachmentInfo
	 * @param user
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateDueDateOfMakerReply(long accountLegalRequestId, String dueDateOfPendingDocument,
			String dueDateComments, long accountLegalLastActionId, AttachmentInfo attachmentInfo, User user)
			throws SQLException, ParseException {
		String sql = "INSERT INTO accounts_l_attachment(due_date_of_pending_document,due_date_comments,account_request_id,account_action_id,scanned_document_type,scanned_content,extension_text,maker) VALUES (?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);

			if (!dueDateOfPendingDocument.equals("")) {
				ps.setDate(1, getBirthDate(dueDateOfPendingDocument));
			} else {
				ps.setNull(1, java.sql.Types.DATE);
			}
			ps.setString(2, dueDateComments);
			ps.setLong(3, accountLegalRequestId);
			ps.setLong(4, accountLegalLastActionId);
			ps.setString(5, attachmentInfo.getScannedDocumentType());
			ps.setBytes(6, attachmentInfo.getScannedContent().getBytes());
			ps.setString(7, attachmentInfo.getExtensionText());
			ps.setString(8, user.getUserName());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date getBirthDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * @param accountLegalRequestId
	 * @param accountLegalRequestStatus
	 * @throws SQLException
	 */
	public void updateLegalRequest(long accountLegalRequestId, String accountLegalRequestStatus) throws SQLException {
		String sql = "UPDATE accounts_l_workflow SET status=?,documents_sent=? WHERE accounts_l_id=? AND status=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, ConstantStatus.PROCEED_MAKER_CAP);
			ps.setBoolean(2, true);
			ps.setLong(3, accountLegalRequestId);
			ps.setString(4, accountLegalRequestStatus);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param accountLegalRequestId
	 * @param dueDateOfPendingDocument
	 * @param dueDateComments
	 * @param accountLegalLastActionId
	 * @param user
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void updateDueDateOfMakerReply(long accountLegalRequestId, String dueDateOfPendingDocument,
			String dueDateComments, long accountLegalLastActionId, User user) throws SQLException, ParseException {
		String sql = "INSERT INTO accounts_l_attachment(due_date_of_pending_document,due_date_comments,account_request_id,account_action_id,maker,scanned_document_type,scanned_content,extension_text) VALUES (?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);

			if (!dueDateOfPendingDocument.equals("")) {
				ps.setDate(1, getBirthDate(dueDateOfPendingDocument));
			} else {
				ps.setNull(1, java.sql.Types.DATE);
			}
			ps.setString(2, dueDateComments);
			ps.setLong(3, accountLegalRequestId);
			ps.setLong(4, accountLegalLastActionId);
			ps.setString(5, user.getUserName());
			ps.setString(6, "");
			ps.setBytes(7, "".getBytes());
			ps.setString(8, "");
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param screeningLegalId
	 * @return
	 * @throws SQLException
	 */
	public String callProcedureRelatedPersonLDetails(long screeningLegalId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_l_related_person(?,?)}");
			stmt.setLong(1, screeningLegalId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	/**
	 * @param screeningLegalId
	 * @return
	 * @throws SQLException
	 */
	public String callProcedureRelatedEntityLDetails(long screeningLegalId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_l_related_entity(?,?)}");
			stmt.setLong(1, screeningLegalId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}

	/**
	 * @param requestId
	 * @param screeningLegalRequestId
	 * @param requestRelatedPerson
	 * @return
	 * @throws SQLException
	 */
	public Long saveRequestRelatedPerson(long requestId, long screeningLegalRequestId,
			ScreeningNRelatedRequest requestRelatedPerson) throws SQLException {
		Long requestRealtedId = 0L;
		String sql = "INSERT INTO screening_n_related(primary_screening_n_id,related_screening_n_id,related_type,notes) "
				+ "VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, requestId);
			ps.setLong(2, screeningLegalRequestId);
			ps.setString(3, requestRelatedPerson.getAccountSubType());
			ps.setString(4, requestRelatedPerson.getNotes());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestRealtedId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return requestRealtedId;

	}

	/**
	 * @param screeningId
	 * @param requestId
	 * @param requestRelatedEntityRequestData
	 * @param screeningLegalRequestRelatedEntityHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long saveNaturalRequestRelatedEntity(Long screeningId, Long requestId,
			RequestRelatedEntityRequestData requestRelatedEntityRequestData,
			String screeningLegalRequestRelatedEntityHashValue) throws SQLException {
		Long requestRealtedId = 0L;
		String sql = "INSERT INTO screening_n_related(primary_screening_n_id,related_screening_l_id,related_type,notes,hash) "
				+ "VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestId);
			ps.setString(3, requestRelatedEntityRequestData.getAccountsLSubType());
			ps.setString(4, requestRelatedEntityRequestData.getNotes());
			ps.setString(5, screeningLegalRequestRelatedEntityHashValue);
			ps.executeUpdate();
			// if (rowAffected > 0) {
			// AmlLogger.printData("screening_l_attachment saved successfully");
			// }
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestRealtedId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}

		return requestRealtedId;
	}

	/**
	 * @param screeningNaturalId
	 * @param forwardTo
	 * @param action
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public Long saveForwardToUser(Long screeningNaturalId, String forwardTo, String action, User user)
			throws SQLException {
		Long id = new Long(0);
		String sql = "INSERT INTO  screening_l_action_forward_to (screening_request_l_id,forward_to,forwarded_by,risk_rating,sol_id) VALUES(?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setString(2, forwardTo);
			ps.setString(3, user.getUserName());
			ps.setString(4, action);
			ps.setString(5, user.getSolId());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();

		}

		return id;

	}

	/**
	 * @param status
	 * @param screeningLegalId
	 * @param screeningLegalWorkFlowHashValue
	 * @throws SQLException
	 */
	public void insertIntoScreeningNaturalWorkFlowHighRiskCustomer(String status, Long screeningLegalId,
			String screeningLegalWorkFlowHashValue) throws SQLException {
		String sql = "INSERT INTO screening_l_workflow(status,screening_l_id,screening_l_requested,hash) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, screeningLegalId);
			ps.setBoolean(3, true);
			ps.setString(4, screeningLegalWorkFlowHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param proceedCheckerMaker
	 * @param screeningLegalId
	 * @param string
	 * @throws SQLException
	 */
	public void insertIntoScreeningNaturalWorkFlow(String proceedCheckerMaker, Long screeningLegalId, String string)
			throws SQLException {
		String sql = "INSERT INTO screening_l_workflow(status,screening_l_id,screening_completed,workflow_completed,hash) VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, proceedCheckerMaker);
			ps.setLong(2, screeningLegalId);
			ps.setBoolean(3, true);
			ps.setBoolean(4, true);
			ps.setString(5, string);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param replyAndSave
	 * @return
	 * @throws SQLException
	 */
	public Long saveReply(ReplyAndSaveLegal replyAndSave) throws SQLException {
		Long replyId = 0L;
		String sql = "INSERT INTO screening_l_reply (screening_l_request_id,checker) VALUES (?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, replyAndSave.getReply().getScreeningLegalId());
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				replyId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return replyId;
	}

	/**
	 * @param replyId
	 * @param replyAndSave
	 * @throws SQLException
	 */
	public void saveActionAfterReplyForScreening(Long replyId, ReplyAndSaveLegal replyAndSave, long scheduleId)
			throws SQLException {
		String sql = "INSERT INTO screening_l_action (screening_l_reply_id,checker,reason,action_type,is_migrated,schedule_id) VALUES (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, replyId);
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.setString(3, replyAndSave.getReply().getReason());
			ps.setString(4, replyAndSave.getReply().getActionType());
			ps.setBoolean(5, replyAndSave.getReply().isMigrated());
			ps.setLong(6, scheduleId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void insertIntoScreeningLegalWorkFlowHighRiskCustomer(String status, Long screeningLegalId,
			String screeningLegalWorkFlowHashValue, boolean screeningCompleted, boolean screeningRequested,
			boolean isRejected) throws SQLException {
		String sql = "INSERT INTO screening_l_workflow(status,screening_l_id,screening_l_requested,hash,screening_completed,rejected) VALUES (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, screeningLegalId);
			ps.setBoolean(3, screeningRequested);
			ps.setString(4, screeningLegalWorkFlowHashValue);
			ps.setBoolean(5, screeningCompleted);
			ps.setBoolean(6, isRejected);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void updateKyclPersonalInfo(Long screeningId, String colName, long kycId) throws SQLException {
		String sql = null;
		sql = "UPDATE kycl_info set screening_id=? WHERE " + colName + "=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, kycId);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

	public void updateKyclPersonalInfo(Long screeningId, String colName, String custId) throws SQLException {
		String sql = null;
		sql = "UPDATE kycl_info set screening_id=? WHERE " + colName + "=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setString(2, custId);
			ps.executeUpdate();
		} finally {
			con.close();
			ps.close();
		}

	}

}
