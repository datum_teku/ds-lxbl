package com.trustaml.service.screening.legal.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.kyc.kycl.model.Legal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPrimaryRequestData {

	private long id;

	@JsonProperty("branch_sol_id")
	private String branchSolId;

	@JsonProperty("username")
	private String username;

	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;

	@JsonProperty("accounts_l_sub_type")
	private String accountsLSubType;

	@JsonProperty("kycl_id")
	private long kyclId;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("name_of_institution")
	private String nameOfInstitution;

	@JsonProperty("ls_name")
	private String lsName;

	@JsonProperty("date_of_establishment")
	private Date dateOfEstablishment;

	@JsonProperty("registration_no")
	private String registrationNo;

	@JsonProperty("type_of_industry")
	private String typeOfIndustry;

	@JsonProperty("country_of_issue")
	private String countryOfIssue;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("ward_no")
	private String wardNo;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("contact_number")
	private String contactNumber;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("nature_of_account")
	private String natureOfAccount;

	@JsonProperty("scheme_description")
	private String schemeDescription;

	@JsonProperty("deposit_amount")
	private String depositAmount;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("find_match_index")
	private String findMatchIndex;

	@JsonProperty("registration_number")
	private String registrationNumber;

	@JsonProperty("request_date")
	private String requestDate;

	@JsonProperty("request_time")
	private String requestTime;

	@JsonProperty("existing_customer")
	private String existingCustomer;

	@JsonProperty("risk_value")
	private long riskValue;

	@JsonProperty("has_kyc")
	private boolean haskyc;

	@JsonProperty("active_user")
	private String activeUser;

	@JsonProperty("has_signatory")
	private boolean hasSignatory;

	@JsonProperty("num_signatory")
	private String numSignatory;

	@JsonProperty("has_mandate")
	private boolean hasMandate;

	@JsonProperty("num_mandate")
	private String numMandate;

	@JsonProperty("state")
	private String state;

	@JsonProperty("status")
	private String status;

	@JsonProperty("previous_screening_l_id")
	private long repairedId;

	@JsonProperty("has_cust_id")
	boolean hasCustId;

	@JsonProperty("swiftInput")
	public String swiftInput;

	@JsonProperty("migrated")
	public boolean migrated;

	@JsonProperty("date_of_establishment_bs")
	private Date dateOfEstablishmentBS;

	@JsonProperty("province")
	private String province;

	public RequestPrimaryRequestData(long id, String branchSolId, String username, String purposeOfScreening,
			String accountsLSubType, long kyclId, String custId, String nameOfInstitution, String lsName,
			Date dateOfEstablishment, String registrationNo, String typeOfIndustry, String countryOfIssue, String zone,
			String district, String mnVdc, String wardNo, String panNumber, String contactNumber, String emailId,
			String natureOfAccount, String schemeDescription, String depositAmount, String notes, String findMatchIndex,
			String registrationNumber, String requestDate, String requestTime, String existingCustomer, long riskValue,
			boolean haskyc, String activeUser, boolean hasSignatory, String numSignatory, boolean hasMandate,
			String numMandate, String state, String status, long repairedId, boolean hasCustId, String swiftInput) {
		super();
		this.id = id;
		this.branchSolId = branchSolId;
		this.username = username;
		this.purposeOfScreening = purposeOfScreening;
		this.accountsLSubType = accountsLSubType;
		this.kyclId = kyclId;
		this.custId = custId;
		this.nameOfInstitution = nameOfInstitution;
		this.lsName = lsName;
		this.dateOfEstablishment = dateOfEstablishment;
		this.registrationNo = registrationNo;
		this.typeOfIndustry = typeOfIndustry;
		this.countryOfIssue = countryOfIssue;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.wardNo = wardNo;
		this.panNumber = panNumber;
		this.contactNumber = contactNumber;
		this.emailId = emailId;
		this.natureOfAccount = natureOfAccount;
		this.schemeDescription = schemeDescription;
		this.depositAmount = depositAmount;
		this.notes = notes;
		this.findMatchIndex = findMatchIndex;
		this.registrationNumber = registrationNumber;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.existingCustomer = existingCustomer;
		this.riskValue = riskValue;
		this.haskyc = haskyc;
		this.activeUser = activeUser;
		this.hasSignatory = hasSignatory;
		this.numSignatory = numSignatory;
		this.hasMandate = hasMandate;
		this.numMandate = numMandate;
		this.state = state;
		this.status = status;
		this.repairedId = repairedId;
		this.hasCustId = hasCustId;
		this.swiftInput = swiftInput;
	}

	public RequestPrimaryRequestData() {
		super();
		this.branchSolId = "";
		this.username = "";
		this.purposeOfScreening = "";
		this.accountsLSubType = "";
		this.kyclId = 0;
		this.custId = "";
		this.nameOfInstitution = "";
		this.lsName = "";
		this.dateOfEstablishment = null;
		this.registrationNo = "";
		this.typeOfIndustry = "";
		this.countryOfIssue = "";
		this.zone = "";
		this.district = "";
		this.mnVdc = "";
		this.wardNo = "";
		this.panNumber = "";
		this.contactNumber = "";
		this.emailId = "";
		this.natureOfAccount = "";
		this.schemeDescription = "";
		this.depositAmount = "";
		this.notes = "";
		this.findMatchIndex = "";
		this.registrationNumber = "";
		this.requestDate = "";
		this.requestTime = "";
		this.existingCustomer = "";
		this.riskValue = 0;
		this.haskyc = false;
		this.activeUser = "";
		this.hasSignatory = false;
		this.numSignatory = "";
		this.hasMandate = false;
		this.numMandate = "";
		this.state = "";
		this.status = "";
		this.id = 0;
		this.repairedId = 0;
		this.swiftInput = "";
		this.migrated = false;
	}

	public RequestPrimaryRequestData(Legal legal) {
		this.nameOfInstitution = legal.getNameOfTheInstitution();
		// TODO Auto-generated constructor stub
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreening;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreening = purposeOfScreening;
	}

	public String getAccountsLSubType() {
		return accountsLSubType;
	}

	public void setAccountsLSubType(String accountsLSubType) {
		this.accountsLSubType = accountsLSubType;
	}

	public long getKyclId() {
		return kyclId;
	}

	public void setKyclId(long kyclId) {
		this.kyclId = kyclId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public String getLsName() {
		return lsName;
	}

	public void setLsName(String lsName) {
		this.lsName = lsName;
	}

	public Date getDateOfEstablishment() {
		return dateOfEstablishment;
	}

	public void setDateOfEstablishment(Date dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNatureOfAccount() {
		return natureOfAccount;
	}

	public void setNatureOfAccount(String natureOfAccount) {
		this.natureOfAccount = natureOfAccount;
	}

	public String getSchemeDescription() {
		return schemeDescription;
	}

	public void setSchemeDescription(String schemeDescription) {
		this.schemeDescription = schemeDescription;
	}

	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFindMatchIndex() {
		return findMatchIndex;
	}

	public void setFindMatchIndex(String findMatchIndex) {
		this.findMatchIndex = findMatchIndex;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}

	public String getExistingCustomer() {
		return existingCustomer;
	}

	public void setExistingCustomer(String existingCustomer) {
		this.existingCustomer = existingCustomer;
	}

	public long getRiskValue() {
		return riskValue;
	}

	public void setRiskValue(long riskValue) {
		this.riskValue = riskValue;
	}

	public boolean isHaskyc() {
		return haskyc;
	}

	public void setHaskyc(boolean haskyc) {
		this.haskyc = haskyc;
	}

	public String isActiveUser() {
		return activeUser;
	}

	public void setActiveUser(String activeUser) {
		this.activeUser = activeUser;
	}

	public boolean isHasSignatory() {
		return hasSignatory;
	}

	public void setHasSignatory(boolean hasSignatory) {
		this.hasSignatory = hasSignatory;
	}

	public String getNumSignatory() {
		return numSignatory;
	}

	public void setNumSignatory(String numSignatory) {
		this.numSignatory = numSignatory;
	}

	public boolean isHasMandate() {
		return hasMandate;
	}

	public void setHasMandate(boolean hasMandate) {
		this.hasMandate = hasMandate;
	}

	public String getNumMandate() {
		return numMandate;
	}

	public void setNumMandate(String numMandate) {
		this.numMandate = numMandate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getRepairedId() {
		return repairedId;
	}

	public void setRepairedId(long repairedId) {
		this.repairedId = repairedId;
	}

	public boolean isHasCustId() {
		return hasCustId;
	}

	public void setHasCustId(boolean hasCustId) {
		this.hasCustId = hasCustId;
	}

	public String getSwiftInput() {
		return swiftInput;
	}

	public void setSwiftInput(String swiftInput) {
		this.swiftInput = swiftInput;
	}

	public String getActiveUser() {
		return activeUser;
	}

	public boolean isMigrated() {
		return migrated;
	}

	public void setMigrated(boolean migrated) {
		this.migrated = migrated;
	}

	public Date getDateOfEstablishmentBS() {
		return dateOfEstablishmentBS;
	}

	public void setDateOfEstablishmentBS(Date dateOfEstablishmentBS) {
		this.dateOfEstablishmentBS = dateOfEstablishmentBS;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

}
