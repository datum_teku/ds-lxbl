package com.trustaml.service.screening.legal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestRelatedEntityMatchData {

	@JsonProperty("type")
	private String type;

	@JsonProperty("id")
	private long id;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("full_name")
	private String fullName;

	@JsonProperty("name_of_institution")
	private String nameOfInstitution;

	@JsonProperty("published_date")
	private String publishedDate;

	@JsonProperty("source_of_media")
	private String sourceOfMedia;

	@JsonProperty("location")
	private String location;

	@JsonProperty("un_list_type")
	private String unListType;

	@JsonProperty("original_script_name")
	private String originalScriptName;

	@JsonProperty("country")
	private String country;

	@JsonProperty("numeric_code")
	private String numericCode;

	@JsonProperty("country_of_issue")
	private String countryOfIssue;

	@JsonProperty("list_code")
	private String listCode;

	@JsonProperty("original_source")
	private String originalSource;

	@JsonProperty("place_of_birth")
	private String placeOfBirth;

	@JsonProperty("address")
	private String address;

	@JsonProperty("sdn_type")
	private String sdnType;

	@JsonProperty("risk")
	private Integer risk;

	public RequestRelatedEntityMatchData(String type, long id, String firstName, String middleName, String fullName,
			String nameOfInstitution, String publishedDate, String sourceOfMedia, String location, String unListType,
			String originalScriptName, String country, String numericCode, String countryOfIssue, String listCode,
			String originalSource, String placeOfBirth, String address, String sdnType, Integer risk) {
		super();
		this.type = type;
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.fullName = fullName;
		this.nameOfInstitution = nameOfInstitution;
		this.publishedDate = publishedDate;
		this.sourceOfMedia = sourceOfMedia;
		this.location = location;
		this.unListType = unListType;
		this.originalScriptName = originalScriptName;
		this.country = country;
		this.numericCode = numericCode;
		this.countryOfIssue = countryOfIssue;
		this.listCode = listCode;
		this.originalSource = originalSource;
		this.placeOfBirth = placeOfBirth;
		this.address = address;
		this.sdnType = sdnType;
		this.risk = risk;
	}

	public RequestRelatedEntityMatchData() {
		this.type = "";
		this.id = 0;
		this.firstName = "";
		this.middleName = "";
		this.fullName = "";
		this.nameOfInstitution = "";
		this.publishedDate = "";
		this.sourceOfMedia = "";
		this.location = "";
		this.unListType = "";
		this.originalScriptName = "";
		this.country = "";
		this.numericCode = "";
		this.countryOfIssue = "";
		this.listCode = "";
		this.originalSource = "";
		this.placeOfBirth = "";
		this.address = "";
		this.sdnType = "";
		this.risk = 0;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getSourceOfMedia() {
		return sourceOfMedia;
	}

	public void setSourceOfMedia(String sourceOfMedia) {
		this.sourceOfMedia = sourceOfMedia;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUnListType() {
		return unListType;
	}

	public void setUnListType(String unListType) {
		this.unListType = unListType;
	}

	public String getOriginalScriptName() {
		return originalScriptName;
	}

	public void setOriginalScriptName(String originalScriptName) {
		this.originalScriptName = originalScriptName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getListCode() {
		return listCode;
	}

	public void setListCode(String listCode) {
		this.listCode = listCode;
	}

	public String getOriginalSource() {
		return originalSource;
	}

	public void setOriginalSource(String originalSource) {
		this.originalSource = originalSource;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSdnType() {
		return sdnType;
	}

	public void setSdnType(String sdnType) {
		this.sdnType = sdnType;
	}

	public Integer getRisk() {
		return risk;
	}

	public void setRisk(Integer risk) {
		this.risk = risk;
	}

}
