package com.trustaml.service.screening.legal.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRelatedEntityRequestData {

	@JsonProperty("accounts_l_sub_type")
	private String accountsLSubType;

	@JsonProperty("kycl_id")
	private String kyclId;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("name_of_institution")
	private String nameOfInstitution;

	@JsonProperty("ls_name")
	private String lsName;

	@JsonProperty("date_of_establishment")
	private String dateOfEstablishment;

	@JsonProperty("registration_no")
	private String registrationNo;

	@JsonProperty("type_of_industry")
	private String typeOfIndustry;

	@JsonProperty("country_of_issue")
	private String countryOfIssue;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("province")
	private String province;

	
	@JsonProperty("ward_no")
	private String wardNo;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("contact_number")
	private String contactNumber;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("find_match_index")
	private String findMatchIndex;

	@JsonProperty("has_kyc")
	private boolean haskyc;

	@JsonProperty("has_cust_id")
	boolean hasCustId;

	@JsonProperty("screening_l_id")
	private long screeningLId;

	@JsonProperty("branch_sol_id")
	private String branchSolId;

	@JsonProperty("date_of_establishment_bs")
	private Date dateOfEstablishmentBS;
	

	@JsonProperty("repair_screening_l_request_id")
	private long repairScreeningLRequestId;

	public RequestRelatedEntityRequestData(String accountsLSubType, String kyclId, String custId,
			String nameOfInstitution, String lsName, String dateOfEstablishment, String registrationNo,
			String typeOfIndustry, String countryOfIssue, String zone, String district, String mnVdc, String wardNo,
			String panNumber, String contactNumber, String emailId, String notes, String findMatchIndex, boolean haskyc,
			boolean hasCustId, long screeningLId, String branchSolId,long repairScreeningLRequestId) {
		super();
		this.accountsLSubType = accountsLSubType;
		this.kyclId = kyclId;
		this.custId = custId;
		this.nameOfInstitution = nameOfInstitution;
		this.lsName = lsName;
		this.dateOfEstablishment = dateOfEstablishment;
		this.registrationNo = registrationNo;
		this.typeOfIndustry = typeOfIndustry;
		this.countryOfIssue = countryOfIssue;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.wardNo = wardNo;
		this.panNumber = panNumber;
		this.contactNumber = contactNumber;
		this.emailId = emailId;
		this.notes = notes;
		this.findMatchIndex = findMatchIndex;
		this.haskyc = haskyc;
		this.hasCustId = hasCustId;
		this.screeningLId = screeningLId;
		this.branchSolId = branchSolId;
		this.repairScreeningLRequestId = repairScreeningLRequestId;
	}

	public RequestRelatedEntityRequestData() {
		super();
		this.accountsLSubType = "";
		this.kyclId = "";
		this.custId = "";
		this.nameOfInstitution = "";
		this.lsName = "";
		this.dateOfEstablishment = null;
		this.registrationNo = "";
		this.typeOfIndustry = "";
		this.countryOfIssue = "";
		this.zone = "";
		this.district = "";
		this.mnVdc = "";
		this.wardNo = "";
		this.panNumber = "";
		this.contactNumber = "";
		this.emailId = "";
		this.notes = "";
		this.findMatchIndex = "";
		this.repairScreeningLRequestId = 0;
		this.province="";
	}

	public String getAccountsLSubType() {
		return accountsLSubType;
	}

	public void setAccountsLSubType(String accountsLSubType) {
		this.accountsLSubType = accountsLSubType;
	}

	public String getKyclId() {
		return kyclId;
	}

	public void setKyclId(String kyclId) {
		this.kyclId = kyclId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public String getLsName() {
		return lsName;
	}

	public void setLsName(String lsName) {
		this.lsName = lsName;
	}

	public String getDateOfEstablishment() {
		return dateOfEstablishment;
	}

	public void setDateOfEstablishment(String dateOfEstablishment) {
		this.dateOfEstablishment = dateOfEstablishment;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFindMatchIndex() {
		return findMatchIndex;
	}

	public void setFindMatchIndex(String findMatchIndex) {
		this.findMatchIndex = findMatchIndex;
	}

	public boolean isHasCustId() {
		return hasCustId;
	}

	public void setHasCustId(boolean hasCustId) {
		this.hasCustId = hasCustId;
	}

	public boolean isHaskyc() {
		return haskyc;
	}

	public void setHaskyc(boolean haskyc) {
		this.haskyc = haskyc;
	}

	public long getScreeningLId() {
		return screeningLId;
	}

	public void setScreeningLId(long screeningLId) {
		this.screeningLId = screeningLId;
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public Date getDateOfEstablishmentBS() {
		return dateOfEstablishmentBS;
	}

	public void setDateOfEstablishmentBS(Date dateOfEstablishmentBS) {
		this.dateOfEstablishmentBS = dateOfEstablishmentBS;
	}

	public long getRepairScreeningLRequestId() {
		return repairScreeningLRequestId;
	}

	public void setRepairScreeningLRequestId(long repairScreeningLRequestId) {
		this.repairScreeningLRequestId = repairScreeningLRequestId;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	
}
