package com.trustaml.service.screening.legal.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRelatedPerson {
	
	
	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;
	
	@JsonProperty("id")
	private long ScreeningId;

	@JsonProperty("accounts_l_sub_type")
	private String accountsLSubType;

	@JsonProperty("kycn_id")
	private long kycnId;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("mobile_number")
	private String mobileNumber;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("date_of_birth")
	private Date dateOfBirth;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationDocumentType;

	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationDocumentNo;

	@JsonProperty("country_of_issue")
	private String countryOfIssue;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("district")
	private String district;
	
	@JsonProperty("province")
	private String province;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("ward_no")
	private String wardNo;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("find_match_index")
	private String findMatchIndex;

	@JsonProperty("has_kyc")
	private boolean haskycl;

	@JsonProperty("has_cust_id")
	private boolean hasCustId;

	@JsonProperty("screening_n_id")
	private long screeningNId;

	@JsonProperty("branch_sol_id")
	private long branchSolId;

	@JsonProperty("date_of_birth_bs")
	private Date relatedDateOfBirthBS;
	
	@JsonProperty("repair_screening_n_request_id")
	private long repairScreeningNRequestId;

	public RequestRelatedPerson(String accountsLSubType, long kycnId, String custId, String salutation,
			String firstName, String middleName, String lastName, String lsfName, String lsmName, String lslName,
			String gender, String mobileNumber, String emailId, Date dateOfBirth,
			String primaryIdentificationDocumentType, String primaryIdentificationDocumentNo, String countryOfIssue,
			String zone, String district, String mnVdc, String wardNo, String panNumber, String notes,
			String findMatchIndex, boolean haskycl, boolean hasCustId, long screeningNId, long branchSolId) {
		super();
		this.accountsLSubType = accountsLSubType;
		this.kycnId = kycnId;
		this.custId = custId;
		this.salutation = salutation;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.dateOfBirth = dateOfBirth;
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
		this.countryOfIssue = countryOfIssue;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.wardNo = wardNo;
		this.panNumber = panNumber;
		this.notes = notes;
		this.findMatchIndex = findMatchIndex;
		this.haskycl = haskycl;
		this.hasCustId = hasCustId;
		this.screeningNId = screeningNId;
		this.branchSolId = branchSolId;
	}

	public RequestRelatedPerson() {
		super();
		this.accountsLSubType = "";
		this.kycnId = 0;
		this.custId = "";
		this.salutation = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.gender = "";
		this.mobileNumber = "";
		this.emailId = "";
		this.dateOfBirth = null;
		this.primaryIdentificationDocumentType = "";
		this.primaryIdentificationDocumentNo = "";
		this.countryOfIssue = "";
		this.zone = "";
		this.district = "";
		this.mnVdc = "";
		this.wardNo = "";
		this.panNumber = "";
		this.notes = "";
		this.findMatchIndex = "";
		this.repairScreeningNRequestId=0;
		this.province="";
	}

	public String getAccountsLSubType() {
		return accountsLSubType;
	}

	public void setAccountsLSubType(String accountsLSubType) {
		this.accountsLSubType = accountsLSubType;
	}

	public long getKycnId() {
		return kycnId;
	}

	public void setKycnId(long kycnId) {
		this.kycnId = kycnId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPrimaryIdentificationDocumentType() {
		return primaryIdentificationDocumentType;
	}

	public void setPrimaryIdentificationDocumentType(String primaryIdentificationDocumentType) {
		this.primaryIdentificationDocumentType = primaryIdentificationDocumentType;
	}

	public String getPrimaryIdentificationDocumentNo() {
		return primaryIdentificationDocumentNo;
	}

	public void setPrimaryIdentificationDocumentNo(String primaryIdentificationDocumentNo) {
		this.primaryIdentificationDocumentNo = primaryIdentificationDocumentNo;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getFindMatchIndex() {
		return findMatchIndex;
	}

	public void setFindMatchIndex(String findMatchIndex) {
		this.findMatchIndex = findMatchIndex;
	}

	public boolean isHaskycl() {
		return haskycl;
	}

	public void setHaskycl(boolean haskycl) {
		this.haskycl = haskycl;
	}

	public boolean isHasCustId() {
		return hasCustId;
	}

	public void setHasCustId(boolean hasCustId) {
		this.hasCustId = hasCustId;
	}

	public long getScreeningNId() {
		return screeningNId;
	}

	public void setScreeningNId(long screeningNId) {
		this.screeningNId = screeningNId;
	}

	public long getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(long branchSolId) {
		this.branchSolId = branchSolId;
	}

	public Date getRelatedDateOfBirthBS() {
		return relatedDateOfBirthBS;
	}

	public void setRelatedDateOfBirthBS(Date relatedDateOfBirthBS) {
		this.relatedDateOfBirthBS = relatedDateOfBirthBS;
	}

	public long getRepairScreeningNRequestId() {
		return repairScreeningNRequestId;
	}

	public void setRepairScreeningNRequestId(long repairScreeningNRequestId) {
		this.repairScreeningNRequestId = repairScreeningNRequestId;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreening;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreening = purposeOfScreening;
	}

	public long getScreeningId() {
		return ScreeningId;
	}

	public void setScreeningId(long screeningId) {
		ScreeningId = screeningId;
	}
	
	
	

	
	
}