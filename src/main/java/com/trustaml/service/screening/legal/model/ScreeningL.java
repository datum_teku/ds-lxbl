package com.trustaml.service.screening.legal.model;

import java.util.ArrayList;
import java.util.List;

public class ScreeningL {
	private RequestPrimaryRequestData requestPrimaryRequestData;
	private List<RequestPrimaryMatchedData> listRequestPrimaryMatchedData;
	private List<RequestRelatedEntityMatchData> listRequestRelatedEntityMatchData;
	private List<RequestRelatedEntityRequestData> listRequestRelatedEntityRequestData;
	private List<RequestRelatedPerson> listRequestRelatedPerson;
	private List<RequestRelatedMatchedPersonData> listRequestRelatedMatchedPersonData;

	public ScreeningL() {
		requestPrimaryRequestData = new RequestPrimaryRequestData();
		listRequestPrimaryMatchedData = new ArrayList<>();
		listRequestRelatedEntityMatchData = new ArrayList<>();
		listRequestRelatedEntityRequestData = new ArrayList<>();
		listRequestRelatedPerson = new ArrayList<>();
		listRequestRelatedMatchedPersonData = new ArrayList<>();
	}

	public RequestPrimaryRequestData getRequestPrimaryRequestData() {
		return requestPrimaryRequestData;
	}

	public void setRequestPrimaryRequestData(RequestPrimaryRequestData requestPrimaryRequestData) {
		this.requestPrimaryRequestData = requestPrimaryRequestData;
	}

	public List<RequestPrimaryMatchedData> getListRequestPrimaryMatchedData() {
		return listRequestPrimaryMatchedData;
	}

	public void setListRequestPrimaryMatchedData(List<RequestPrimaryMatchedData> listRequestPrimaryMatchedData) {
		this.listRequestPrimaryMatchedData = listRequestPrimaryMatchedData;
	}

	public List<RequestRelatedEntityMatchData> getListRequestRelatedEntityMatchData() {
		return listRequestRelatedEntityMatchData;
	}

	public void setListRequestRelatedEntityMatchData(
			List<RequestRelatedEntityMatchData> listRequestRelatedEntityMatchData) {
		this.listRequestRelatedEntityMatchData = listRequestRelatedEntityMatchData;
	}

	public List<RequestRelatedEntityRequestData> getListRequestRelatedEntityRequestData() {
		return listRequestRelatedEntityRequestData;
	}

	public void setListRequestRelatedEntityRequestData(
			List<RequestRelatedEntityRequestData> listRequestRelatedEntityRequestData) {
		this.listRequestRelatedEntityRequestData = listRequestRelatedEntityRequestData;
	}

	public List<RequestRelatedPerson> getListRequestRelatedPerson() {
		return listRequestRelatedPerson;
	}

	public void setListRequestRelatedPerson(List<RequestRelatedPerson> listRequestRelatedPerson) {
		this.listRequestRelatedPerson = listRequestRelatedPerson;
	}

	public List<RequestRelatedMatchedPersonData> getListRequestRelatedMatchedPersonData() {
		return listRequestRelatedMatchedPersonData;
	}

	public void setListRequestRelatedMatchedPersonData(
			List<RequestRelatedMatchedPersonData> listRequestRelatedMatchedPersonData) {
		this.listRequestRelatedMatchedPersonData = listRequestRelatedMatchedPersonData;
	}

}
