package com.trustaml.service.screening.legal.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreeningLegal {
	@JsonProperty("screening_l_request_primary")
	private ScreeningLegalRequestPrimary screeningLegalRequestPrimary;

	@JsonProperty("screening_l_request_related_entity")
	private List<ScreeningLegalRequestRelatedEntity> screeningLegalRequestRelatedEntity;

	@JsonProperty("screening_l_request_related_person")
	private List<ScreeningLegalRequestRelatedPerson> screeningLegalRequestRelatedPerson;

	@JsonProperty("screening_l_request_attachments")
	private List<ScreeningLegalRequestAttachments> listScreeningLegalRequestAttachments;

	@JsonProperty("user")
	private User user;

	@JsonProperty("forward_to")
	private List<String> forwardTo;

	@JsonProperty("action")
	private String action;

	public ScreeningLegal() {
		screeningLegalRequestPrimary = new ScreeningLegalRequestPrimary();
		screeningLegalRequestRelatedEntity = new ArrayList<>();
		screeningLegalRequestRelatedPerson = new ArrayList<>();
		listScreeningLegalRequestAttachments = new ArrayList<>();
		forwardTo = new ArrayList<>();

	}

	public ScreeningLegalRequestPrimary getScreeningLegalRequestPrimary() {
		return screeningLegalRequestPrimary;
	}

	public void setScreeningLegalRequestPrimary(ScreeningLegalRequestPrimary screeningLegalRequestPrimary) {
		this.screeningLegalRequestPrimary = screeningLegalRequestPrimary;
	}

	public List<ScreeningLegalRequestRelatedEntity> getScreeningLegalRequestRelatedEntity() {
		return screeningLegalRequestRelatedEntity;
	}

	public void setScreeningLegalRequestRelatedEntity(
			List<ScreeningLegalRequestRelatedEntity> screeningLegalRequestRelatedEntity) {
		this.screeningLegalRequestRelatedEntity = screeningLegalRequestRelatedEntity;
	}

	public List<ScreeningLegalRequestRelatedPerson> getScreeningLegalRequestRelatedPerson() {
		return screeningLegalRequestRelatedPerson;
	}

	public void setScreeningLegalRequestRelatedPerson(
			List<ScreeningLegalRequestRelatedPerson> screeningLegalRequestRelatedPerson) {
		this.screeningLegalRequestRelatedPerson = screeningLegalRequestRelatedPerson;
	}

	public List<ScreeningLegalRequestAttachments> getListScreeningLegalRequestAttachments() {
		return listScreeningLegalRequestAttachments;
	}

	public void setListScreeningLegalRequestAttachments(
			List<ScreeningLegalRequestAttachments> listScreeningLegalRequestAttachments) {
		this.listScreeningLegalRequestAttachments = listScreeningLegalRequestAttachments;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<String> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(List<String> forwardTo) {
		this.forwardTo = forwardTo;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
