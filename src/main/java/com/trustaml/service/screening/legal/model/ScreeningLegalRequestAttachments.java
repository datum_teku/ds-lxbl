package com.trustaml.service.screening.legal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningLegalRequestAttachments {

	@JsonProperty("scanned_document_type")
	private String scannedDocumentType;

	@JsonProperty("extension_text")
	private String extensionText;

	@JsonProperty("scanned_content")
	private String scannedContent;

	@JsonProperty("notes")
	private String notes;

	public String getScannedDocumentType() {
		return scannedDocumentType;
	}

	public void setScannedDocumentType(String scannedDocumentType) {
		this.scannedDocumentType = scannedDocumentType;
	}

	public String getExtensionText() {
		return extensionText;
	}

	public void setExtensionText(String extensionText) {
		this.extensionText = extensionText;
	}

	public String getScannedContent() {
		return scannedContent;
	}

	public void setScannedContent(String scannedContent) {
		this.scannedContent = scannedContent;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
