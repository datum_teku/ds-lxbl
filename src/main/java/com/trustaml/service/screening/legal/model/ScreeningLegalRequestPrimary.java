package com.trustaml.service.screening.legal.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningLegalRequestPrimary {

	@JsonProperty("screening_l_request_data")
	private RequestPrimaryRequestData requestPrimaryRequestData;

	@JsonProperty("screening_l_match_data")
	private List<RequestPrimaryMatchedData> listRequestPrimaryMatchedData;

	public ScreeningLegalRequestPrimary() {
		requestPrimaryRequestData = new RequestPrimaryRequestData();
		listRequestPrimaryMatchedData = new ArrayList<>();

	}

	public RequestPrimaryRequestData getRequestPrimaryRequestData() {
		return requestPrimaryRequestData;
	}

	public void setRequestPrimaryRequestData(RequestPrimaryRequestData requestPrimaryRequestData) {
		this.requestPrimaryRequestData = requestPrimaryRequestData;
	}

	public List<RequestPrimaryMatchedData> getListRequestPrimaryMatchedData() {
		return listRequestPrimaryMatchedData;
	}

	public void setListRequestPrimaryMatchedData(List<RequestPrimaryMatchedData> listRequestPrimaryMatchedData) {
		this.listRequestPrimaryMatchedData = listRequestPrimaryMatchedData;
	}

}
