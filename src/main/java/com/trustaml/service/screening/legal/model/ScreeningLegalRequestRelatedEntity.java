package com.trustaml.service.screening.legal.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningLegalRequestRelatedEntity {

	@JsonProperty("related_entity_match_data")
	private List<RequestRelatedEntityMatchData> listRequestRelatedEntityMatchData;

	@JsonProperty("related_entity_request_data")
	private RequestRelatedEntityRequestData requestRelatedEntityRequestData;

	public ScreeningLegalRequestRelatedEntity() {
		requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
		listRequestRelatedEntityMatchData = new ArrayList<>();
	}

	public List<RequestRelatedEntityMatchData> getListRequestRelatedEntityMatchData() {
		return listRequestRelatedEntityMatchData;
	}

	public void setListRequestRelatedEntityMatchData(
			List<RequestRelatedEntityMatchData> listRequestRelatedEntityMatchData) {
		this.listRequestRelatedEntityMatchData = listRequestRelatedEntityMatchData;
	}

	public RequestRelatedEntityRequestData getRequestRelatedEntityRequestData() {
		return requestRelatedEntityRequestData;
	}

	public void setRequestRelatedEntityRequestData(RequestRelatedEntityRequestData requestRelatedEntityRequestData) {
		this.requestRelatedEntityRequestData = requestRelatedEntityRequestData;
	}

}
