package com.trustaml.service.screening.legal.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningLegalRequestRelatedPerson {

	@JsonProperty("related_person_request_data")
	private RequestRelatedPerson requestRelatedPerson;

	@JsonProperty("related_person_match_data")
	private List<RequestRelatedMatchedPersonData> listRequestRelatedMatchedPersonData;

	public ScreeningLegalRequestRelatedPerson() {
		requestRelatedPerson = new RequestRelatedPerson();
		listRequestRelatedMatchedPersonData = new ArrayList<>();
	}

	public RequestRelatedPerson getRequestRelatedPerson() {
		return requestRelatedPerson;
	}

	public void setRequestRelatedPerson(RequestRelatedPerson requestRelatedPerson) {
		this.requestRelatedPerson = requestRelatedPerson;
	}

	public List<RequestRelatedMatchedPersonData> getListRequestRelatedMatchedPersonData() {
		return listRequestRelatedMatchedPersonData;
	}

	public void setListRequestRelatedMatchedPersonData(
			List<RequestRelatedMatchedPersonData> listRequestRelatedMatchedPersonData) {
		this.listRequestRelatedMatchedPersonData = listRequestRelatedMatchedPersonData;
	}

}
