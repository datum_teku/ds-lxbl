package com.trustaml.service.screening.natural.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.model.ApplicationStatus;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.screening.natural.dao.ScreeningNaturalDao;

@Path("/screening-natural")
public class ScreeningNaturalRestfulService {

	@Inject
	ScreeningNaturalDao screeningNaturalDao;

	/**
	 * @param jsonString
	 * @return screening natural id
	 * @description accepts JSON String and save in sccrening natural table
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveScreeningNatural(String jsonString) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException {
		if (!jsonString.isEmpty()) {
			ApplicationStatus applicationStatus = screeningNaturalDao.saveScreeningNatural(jsonString);
			String str = new ObjectMapper().writeValueAsString(applicationStatus);
			return ResponseReturn.sucess(str);
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/*
	 * @POST
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON)
	 * 
	 * @Path("/migrate") public Response
	 * screeningOfMigratedData(List<ScreeningOfMigratedData>
	 * listScreeningOfMigratedData) throws Exception { // JSONObject obj = new
	 * JSONObject(); // obj.put("data", "data");
	 * screeningNaturalDao.screeningOfMigratedData(listScreeningOfMigratedData);
	 * return ResponseReturn.sucess("ok"); }
	 * 
	 * @GET
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Path("/migration-schedule") public Response getScheduleOfMigrationData()
	 * throws Exception { return
	 * ResponseReturn.sucess(screeningNaturalDao.getScheduleOfMigrationData());
	 * }
	 */
}
