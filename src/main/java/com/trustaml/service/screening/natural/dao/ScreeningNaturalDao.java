package com.trustaml.service.screening.natural.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.common.constant.MatchConstant;
import com.trustaml.service.common.model.ApplicationStatus;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.natural.model.RequestRelatedNaturalEntity;
import com.trustaml.service.screening.natural.model.RequestRelatedNaturalPerson;
import com.trustaml.service.screening.natural.model.ScreeningNAttachment;
import com.trustaml.service.screening.natural.model.ScreeningNMatchInfo;
import com.trustaml.service.screening.natural.model.ScreeningNRelatedRequest;
import com.trustaml.service.screening.natural.model.ScreeningNRequest;
import com.trustaml.service.screening.natural.model.ScreeningNatural;

@Stateless
public class ScreeningNaturalDao {

	@Inject
	ScreeningNaturalDaoImpl screeningNaturalDaoImpl;

	@Inject
	ConstantEntity constantEntity;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	/**
	 * @param jsonString
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */
	public ApplicationStatus saveScreeningNatural(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, ParseException {

		if (constantEntity.getSizeOfLegalLinkTable() == 0) {
			constantEntity.setlegalLinkTable();
		}
		if (constantEntity.getSizeOfNaturalLinkTable() == 0) {
			constantEntity.setNaturalLinkTable();
		}
		if (constantEntity.getSizeOfLegalLinkColumnName() == 0) {
			constantEntity.setlegalLinkColumnName();
		}
		if (constantEntity.getSizeOfNaturalLinkColumnName() == 0) {
			constantEntity.setNaturalLinkColumnName();
		}
		int matchCase = 0;
		boolean screeningCompleted = true;
		boolean screeningRequested = true;
		boolean isRejected = true;

		ScreeningNatural screeningNatural = new ObjectMapper().readValue(jsonString, ScreeningNatural.class);
		ScreeningNRequest screeningNRequest = screeningNatural.getRequestNaturalRequestPrimary().getScreeningNRequest();
		screeningNRequest.setBranchSolId(screeningNatural.getUser().getSolId());
		Long screeningNaturalId = screeningNaturalDaoImpl.saveScreeningNaturalRequest(screeningNRequest,
				screeningNatural.getUser());
		if(!screeningNRequest.getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
			if (matchCase == 0) {
				matchCase = 2;
			}
		}
		for (ScreeningNAttachment attachment : screeningNatural.getScreeningNAttachmentList()) {
			screeningNaturalDaoImpl.saveScreeningNaturalAttachment(attachment, screeningNatural.getUser(),
					screeningNaturalId);
		}
		for (ScreeningNMatchInfo matchObject : screeningNatural.getRequestNaturalRequestPrimary().getMatchInfoList()) {
			if (matchObject.getType().equals(MatchConstant.UN_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.OFAC_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.HMT_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.ACCUITY_SDN_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.EU_MATCH_CASE)) {
				// application rejected
				matchCase = 1;
			} else if (matchObject.getType().equals(MatchConstant.PEP_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.ADVERSE_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.HOT_LIST_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.INVESTIGATION_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.ACCUITY_MATCH_CASE)
					|| matchObject.getType().equals(MatchConstant.ACCUITY_ADVERSE_MATCH_CASE)) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}
				
			String tableName = constantEntity.getNaturalTableName(matchObject.getType());
			screeningNaturalDaoImpl.saveMatchedRequest(matchObject, screeningNaturalId, tableName, "hash",
					"screening_request_n_id");
		}
		for (RequestRelatedNaturalPerson requestRelatedNaturalPerson : screeningNatural
				.getListRequestRealatedNaturalPerson()) {

			ScreeningNRelatedRequest screeningNRelatedRequest = requestRelatedNaturalPerson
					.getScreeningNRelatedRequest();
			long realatedNaturalId = screeningNaturalDaoImpl.saveRequestNatural(screeningNRelatedRequest,
					screeningNatural.getUser());
			screeningLegalDaoImpl.saveRequestRelatedPerson(screeningNaturalId, realatedNaturalId,
					screeningNRelatedRequest);
			//if related person is not from Nepal, it is forwarded to checker
			if(!screeningNRelatedRequest.getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}

			for (ScreeningNMatchInfo screeningNMatchInfo : requestRelatedNaturalPerson.getListScreeningNMatchInfo()) {
				if (screeningNMatchInfo.getType().equals(MatchConstant.UN_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.OFAC_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.HMT_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.EU_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_SDN_MATCH_CASE)) {
					// application rejected
					matchCase = 1;
				} else if (screeningNMatchInfo.getType().equals(MatchConstant.PEP_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ADVERSE_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.HOT_LIST_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.INVESTIGATION_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_ADVERSE_MATCH_CASE)) {
					if (matchCase == 0) {
						matchCase = 2;
					}
				}
				String tableName = constantEntity.getNaturalTableName(screeningNMatchInfo.getType());
				screeningNaturalDaoImpl.saveMatchedRequest(screeningNMatchInfo, realatedNaturalId, tableName, "hash",
						"screening_request_n_id");
			}
		}
		for (RequestRelatedNaturalEntity requestRelatedNaturalEntity : screeningNatural
				.getListRequestRealatedNaturalEntity()) {
			RequestRelatedEntityRequestData screeningNRelatedRequest = requestRelatedNaturalEntity
					.getScreeningNRelatedRequest();
			long screeningLegalRelatedId = screeningLegalDaoImpl.saveRequest(screeningNRelatedRequest,
					screeningNatural.getUser(), "hash");
			screeningLegalDaoImpl.saveNaturalRequestRelatedEntity(screeningNaturalId, screeningLegalRelatedId,
					screeningNRelatedRequest, "hash");
			
			if(!screeningNRelatedRequest.getCountryOfIssue().equalsIgnoreCase("NEPAL")) {
				if (matchCase == 0) {
					matchCase = 2;
				}
			}
			
			
			// long realatedNaturalEntityId =
			// screeningNaturalDaoImpl.saveRequestNatural(screeningNRelatedRequest,
			// screeningNatural.getUser());
			for (ScreeningNMatchInfo screeningNMatchInfo : requestRelatedNaturalEntity.getListScreeningNMatchInfo()) {
				if (screeningNMatchInfo.getType().equals(MatchConstant.UN_MATCH_CASE_LEGAL)
						|| screeningNMatchInfo.getType().equals(MatchConstant.OFAC_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.EU_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.HMT_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_SDN_ENTITY_MATCH_CASE)) {
					// application rejected
					matchCase = 1;
				} else if (screeningNMatchInfo.getType().equals(MatchConstant.PEP_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ADVERSE_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.HOT_LIST_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.INVESTIGATION_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_PEP_ENTITY_MATCH_CASE)
						|| screeningNMatchInfo.getType().equals(MatchConstant.ACCUITY_ADVERSE_ENTITY_MATCH_CASE)) {
					if (matchCase == 0) {
						matchCase = 2;
					}
				}
				String tableName = constantEntity.getLegalTableName(screeningNMatchInfo.getType());
				screeningNaturalDaoImpl.saveMatchedRequest(screeningNMatchInfo, screeningLegalRelatedId, tableName,
						"hash", "screening_l_request_id");
			}
		}
		for (String forwardTo : screeningNatural.getForwardTo()) {
			screeningNaturalDaoImpl.saveForwardToUser(screeningNaturalId, forwardTo, screeningNatural.getAction(),
					screeningNatural.getUser());
		}
		ApplicationStatus applicationStatus = new ApplicationStatus();
		if (matchCase == 0) {
			// NO match case
			// /Screening Completed
			applicationStatus.setMessage(1);
			screeningRequested = false;
			isRejected = false;
			screeningNaturalDaoImpl.insertIntoScreeningNaturalWorkFlowHighRiskCustomer(
					ConstantStatus.PROCEED_CHECKER_MAKER, screeningNaturalId, "hash", screeningCompleted,
					screeningRequested, isRejected);
		} else if (matchCase == 1) {
			// OFAC AND UN match case
			// Rejected
			applicationStatus.setMessage(2);
			screeningCompleted = false;
			screeningRequested = false;
			screeningNaturalDaoImpl.insertIntoScreeningNaturalWorkFlowHighRiskCustomer(
					ConstantStatus.PROCEED_CHECKER_MAKER, screeningNaturalId, "hash", screeningCompleted,
					screeningRequested, isRejected);
		} else if (matchCase == 2) {
			// OTHER MATCH CASE AND UN match case
			// Procced to Checker
			applicationStatus.setMessage(3);
			screeningCompleted = false;

			isRejected = false;
			screeningNaturalDaoImpl.insertIntoScreeningNaturalWorkFlowHighRiskCustomer(
					ConstantStatus.PROCEED_MAKER_CHECKER, screeningNaturalId, "hash", screeningCompleted,
					screeningRequested, isRejected);
		}
		if (screeningNRequest.isMigrated()
				&& (screeningNRequest.getKycnId() != 0 || !screeningNRequest.getCustId().isEmpty())) {
			if (screeningNRequest.getKycnId() != 0) {
				screeningNaturalDaoImpl.updateKycnPersonalInfo(screeningNaturalId, "id", screeningNRequest.getKycnId());
			} else if (!screeningNRequest.getCustId().isEmpty()) {
				screeningNaturalDaoImpl.updateKycnPersonalInfo(screeningNaturalId, "cust_id",
						screeningNRequest.getCustId());
			}
		}
		applicationStatus.setId(screeningNaturalId);
		return applicationStatus;

	}

}
