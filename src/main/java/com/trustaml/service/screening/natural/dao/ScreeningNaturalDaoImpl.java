package com.trustaml.service.screening.natural.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.inject.Inject;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.screening.natural.model.ScreeningNAttachment;
import com.trustaml.service.screening.natural.model.ScreeningNMatchInfo;
import com.trustaml.service.screening.natural.model.ScreeningNRelatedRequest;
import com.trustaml.service.screening.natural.model.ScreeningNRequest;
import com.trustaml.service.toandfro.reply.model.KYCReply;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalComment;
import com.trustaml.service.toandfro.reply.model.ScreeningNaturalComment;

public class ScreeningNaturalDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @param screeningNRequest
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public Long saveScreeningNaturalRequest(ScreeningNRequest screeningNRequest, User user) throws SQLException {
		Long screeningRequestId = 0L;
		String sql = "INSERT INTO  screening_n_request (first_name, middle_name, last_name, lsf_name, "
				+ "lsm_name, lsl_name, date_of_birth, primary_identification_type,primary_identification_no, "
				+ "country_of_issue, state, district,mn_vdc, maker, branch_sol_id, has_kyc, kyc_id, "
				+ "purpose_of_screening,notes,zone,province,cust_id,sender_receiver,"
				+ "salutation,gender,mobile_number,email_id,nature_of_account,scheme_description,"
				+ "deposit_amount,pan_number,ward_no,date_of_birth_bs)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, screeningNRequest.getFirstName());
			ps.setString(2, screeningNRequest.getMiddleName());
			ps.setString(3, screeningNRequest.getLastName());
			ps.setString(4, screeningNRequest.getLsfName());
			ps.setString(5, screeningNRequest.getLsmName());
			ps.setString(6, screeningNRequest.getLslName());
			ps.setDate(7, screeningNRequest.getDateOfBirth());
			ps.setString(8, screeningNRequest.getPrimaryIdentificationType());
			ps.setString(9, screeningNRequest.getPrimaryIdentificationNo());
			ps.setString(10, screeningNRequest.getCountryOfIssue());
			ps.setString(11, screeningNRequest.getState());
			ps.setString(12, screeningNRequest.getDistrict());
			ps.setString(13, screeningNRequest.getMnVdc());
			ps.setString(14, user.getUserName());
			ps.setString(15, user.getSolId());
			ps.setBoolean(16, screeningNRequest.isHasKycn());
			ps.setLong(17, screeningNRequest.getKycnId());
			ps.setString(18, screeningNRequest.getPurposeOfScreeningN());
			ps.setString(19, screeningNRequest.getNotes());
			ps.setString(20, screeningNRequest.getZone());
			ps.setString(21, screeningNRequest.getProvince());
			ps.setString(22, screeningNRequest.getCustId());
			ps.setString(23, screeningNRequest.getSenderReceiver());
			ps.setString(24, screeningNRequest.getSalutation());
			ps.setString(25, screeningNRequest.getGender());
			ps.setString(26, screeningNRequest.getMobileNumber());
			ps.setString(27, screeningNRequest.getEmailId());
			ps.setString(28, screeningNRequest.getNatureOfAccount());
			ps.setString(29, screeningNRequest.getSchemeDescription());
			ps.setString(30, screeningNRequest.getDepositAmount());
			ps.setString(31, screeningNRequest.getPanNumber());
			ps.setString(32, screeningNRequest.getWardNo());
			ps.setDate(33, screeningNRequest.getDateOfBirthBS());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				screeningRequestId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return screeningRequestId;

	}

	/**
	 * @param attachment
	 * @param user
	 * @param screeningNaturalId
	 * @throws SQLException
	 */
	public void saveScreeningNaturalAttachment(ScreeningNAttachment attachment, User user, Long screeningNaturalId)
			throws SQLException {
		String sql = "INSERT INTO screening_n_attachment(screening_request_n_id, scanned_document_type, "
				+ "scanned_content,extension_text, notes,maker) VALUES (?, ?, ?, ?, ?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setString(2, attachment.getScannedDocumentType());
			ps.setBytes(3, attachment.getScannedContent().getBytes());
			ps.setString(4, attachment.getExtensionText());
			ps.setString(5, attachment.getNotes());
			ps.setString(6, user.getUserName());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param requestPrimaryMatchedData
	 * @param screeningId
	 * @param tableName
	 * @param requestPrimaryMatchedDataHashValue
	 * @param columnName
	 * @throws SQLException
	 */
	public void saveMatchedRequest(ScreeningNMatchInfo requestPrimaryMatchedData, Long screeningId, String tableName,
			String requestPrimaryMatchedDataHashValue, String columnName) throws SQLException {
		String sql = "INSERT INTO " + tableName + "(" + columnName + ",linked_id,hash,risk_value) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningId);
			ps.setLong(2, requestPrimaryMatchedData.getMatchId());
			ps.setString(3, requestPrimaryMatchedDataHashValue);
			ps.setInt(4, requestPrimaryMatchedData.getRisk());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param status
	 * @param screeningId
	 * @param screeningLegalWorkFlowHashValue
	 * @throws SQLException
	 */
	public void insertIntoScreeningNaturalWorkFlowHighRiskCustomer(String status, Long screeningId,
			String screeningLegalWorkFlowHashValue) throws SQLException {
		String sql = "INSERT INTO screening_n_workflow(status,screening_n_id,screening_n_requested,hash) VALUES (?,?,?,?)";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, screeningId);
			ps.setBoolean(3, true);
			ps.setString(4, screeningLegalWorkFlowHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}
	}

	/**
	 * @param isNominee
	 * @param isSignatory
	 * @param isMandate
	 * @param isJointAccount
	 * @param screeningNaturalId
	 * @throws SQLException
	 */
	public void updateWorkflowOfNatural(boolean isNominee, boolean isSignatory, boolean isMandate,
			boolean isJointAccount, Long screeningNaturalId) throws SQLException {
		String sql = "UPDATE screening_n_request SET has_nominee = ?, has_signatory = ?, has_mandate=?, "
				+ "has_joint_account=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, isNominee);
			ps.setBoolean(2, isSignatory);
			ps.setBoolean(3, isMandate);
			ps.setBoolean(4, isJointAccount);
			ps.setLong(5, screeningNaturalId);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNRelatedRequest
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public long saveRequestNatural(ScreeningNRelatedRequest screeningNRelatedRequest, User user) throws SQLException {
		Long screeningRequestId = 0L;
		String sql = "INSERT INTO  screening_n_request (first_name, middle_name, last_name, lsf_name, "
				+ "lsm_name, lsl_name, date_of_birth, primary_identification_type,"
				+ "primary_identification_no, country_of_issue, state, district, "
				+ "mn_vdc, maker, branch_sol_id, has_kyc, kyc_id, account_sub_type,notes,zone,province,"
				+ "salutation,gender,mobile_number,email_id,hash,date_of_birth_bs,ward_no,pan_number)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, screeningNRelatedRequest.getFirstName());
			ps.setString(2, screeningNRelatedRequest.getMiddleName());
			ps.setString(3, screeningNRelatedRequest.getLastName());
			ps.setString(4, screeningNRelatedRequest.getLsfName());
			ps.setString(5, screeningNRelatedRequest.getLsmName());
			ps.setString(6, screeningNRelatedRequest.getLslName());
			ps.setDate(7, screeningNRelatedRequest.getDateOfBirth());
			ps.setString(8, screeningNRelatedRequest.getPrimaryIdentificationType());
			ps.setString(9, screeningNRelatedRequest.getPrimaryIdentificationNo());
			ps.setString(10, screeningNRelatedRequest.getCountryOfIssue());
			ps.setString(11, screeningNRelatedRequest.getZone());
			ps.setString(12, screeningNRelatedRequest.getDistrict());
			ps.setString(13, screeningNRelatedRequest.getMnVdc());
			ps.setString(14, user.getUserName());
			ps.setString(15, user.getSolId());
			ps.setBoolean(16, screeningNRelatedRequest.isHasKyc());
			ps.setLong(17, screeningNRelatedRequest.getKycId());
			ps.setString(18, screeningNRelatedRequest.getAccountSubType());
			ps.setString(19, screeningNRelatedRequest.getNotes());
			ps.setString(20, screeningNRelatedRequest.getZone());
			ps.setString(21, screeningNRelatedRequest.getProvince());
			ps.setString(22, screeningNRelatedRequest.getSalutation());
			ps.setString(23, screeningNRelatedRequest.getGender());
			ps.setString(24, screeningNRelatedRequest.getMobileNumber());
			ps.setString(25, screeningNRelatedRequest.getEmailId());
			ps.setString(26, "HASH");
			ps.setDate(27, (java.sql.Date) screeningNRelatedRequest.getDateOfBirthBS());
			ps.setString(28, screeningNRelatedRequest.getWardNo());
			ps.setString(29, screeningNRelatedRequest.getPanNo());
			
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				screeningRequestId = rs.getLong(1);
				rs.close();
				// System.out.println("ScreeningNRelatedRequestId :
				// "+screeningNRelatedRequestId);
			}
		} finally {
			ps.close();
			con.close();
		}
		return screeningRequestId;
	}

	/**
	 * @param screeningNaturalId
	 * @param forwardTo
	 * @param action
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public Long saveForwardToUser(Long screeningNaturalId, String forwardTo, String action, User user)
			throws SQLException {
		Long id = new Long(0);
		String sql = "INSERT INTO  screening_n_action_forward_to (screening_request_n_id,forward_to,forwarded_by,risk_rating,sol_id) VALUES(?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setString(2, forwardTo);
			ps.setString(3, user.getUserName());
			ps.setString(4, action);
			ps.setString(5, user.getSolId());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				id = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return id;
	}

	/**
	 * @param proceedCheckerMaker
	 * @param screeningNaturalId
	 * @param string
	 * @throws SQLException
	 */
	public void insertIntoScreeningNaturalWorkFlow(String proceedCheckerMaker, Long screeningNaturalId, String string)
			throws SQLException {
		String sql = "INSERT INTO screening_n_workflow(status,screening_n_id,screening_completed,workflow_completed,hash) VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, proceedCheckerMaker);
			ps.setLong(2, screeningNaturalId);
			ps.setBoolean(3, true);
			ps.setBoolean(4, true);
			ps.setString(5, string);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNaturalComment
	 * @throws SQLException
	 */
	public void updateNaturalForwardApplicationForComments(ScreeningNaturalComment screeningNaturalComment)
			throws SQLException {
		String sql = "UPDATE screening_n_action_forward_to SET comment = ?, comment_date = ?,replied_by=?,replied_by_sol_id=? WHERE id=? and screening_request_n_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningNaturalComment.getComment());
			java.sql.Date date = getCurrentDatetime();
			ps.setDate(2, date);
			ps.setString(3, screeningNaturalComment.getUser().getUserName());
			ps.setString(4, screeningNaturalComment.getUser().getSolId());
			ps.setLong(5, screeningNaturalComment.getId());
			ps.setLong(6, screeningNaturalComment.getScreeningNaturalId());
			ps.executeUpdate();

		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @return current date
	 */
	public java.sql.Date getCurrentDatetime() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	/**
	 * @param screeningNaturalId
	 * @param forwardTo
	 * @param action
	 * @param user
	 * @param savedId
	 * @throws SQLException
	 */
	public void saveForwardToUserUpdateTable(Long screeningNaturalId, String forwardTo, String action, User user,
			Long savedId) throws SQLException {
		String sql = "INSERT INTO  screening_n_action_forward_to_update (screening_request_n_id,forward_to,forwarded_by,risk_rating,sol_id,screening_n_action_forward_id) VALUES(?,?,?,?,?,?) ";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setString(2, forwardTo);
			ps.setString(3, user.getUserName());
			ps.setString(4, action);
			ps.setString(5, user.getSolId());
			ps.setLong(6, savedId);
			ps.executeUpdate();

		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNId
	 * @return
	 * @throws SQLException
	 */
	public String callProcedureRelatedPersonLDetails(long screeningNId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_n_related_person(?,?)}");
			stmt.setLong(1, screeningNId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();
		}
		return result;
	}

	/**
	 * @param screeningNId
	 * @return
	 * @throws SQLException
	 */
	public String callProcedureRelatedEntityLDetails(long screeningNId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_n_related_entity(?,?)}");
			stmt.setLong(1, screeningNId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();
		}
		return result;
	}

	/**
	 * @param screeningLegalComment
	 * @throws SQLException
	 */
	public void updateLegalForwardApplicationForComments(ScreeningLegalComment screeningLegalComment)
			throws SQLException {
		String sql = "UPDATE screening_l_action_forward_to SET comment = ?, comment_date = ?,replied_by=?,replied_by_sol_id=? WHERE id=? and screening_request_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningLegalComment.getComment());
			java.sql.Date date = getCurrentDatetime();
			ps.setDate(2, date);
			ps.setString(3, screeningLegalComment.getUser().getUserName());
			ps.setString(4, screeningLegalComment.getUser().getSolId());
			ps.setLong(5, screeningLegalComment.getId());
			ps.setLong(6, screeningLegalComment.getScreeningLegalId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param kycReply
	 * @throws SQLException
	 */
	public void saveKYCReply(KYCReply kycReply) throws SQLException {
		String sql = null;
		if (kycReply.getName().equals("Natural")) {
			sql = "UPDATE kycn_approved set approved=?,approved_date=?,user_name=? WHERE kycn_personal_info_id=? ";
		} else if (kycReply.getName().equals("Legal")) {
			sql = "UPDATE kycl_approved set approved=?,approved_date=?,user_name=? WHERE kycl_info_id=?";
		}
		if (!sql.equals(null)) {
			Connection connection = null;
			PreparedStatement ps = null;
			try {
				connection = dbConnection.getConnection();
				ps = connection.prepareStatement(sql);
				ps.setBoolean(1, kycReply.getApproved());
				ps.setTimestamp(2, getCurrentDateAndtime());
				ps.setString(3, kycReply.getUser().getUserName());
				ps.setLong(4, kycReply.getId());
				ps.executeUpdate();

			} finally {
				ps.close();
				connection.close();
			}
		} else {
			System.out.println("Query is null");
		}
	}

	public void insertIntoScreeningNaturalWorkFlowHighRiskCustomer(String status, Long screeningId,
			String screeningLegalWorkFlowHashValue, boolean screeningCompleted, boolean screeningRequested,
			boolean isRejected) throws SQLException {
		String sql = "INSERT INTO screening_n_workflow(status,screening_n_id,screening_n_requested,hash,screening_completed,rejected) VALUES (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, screeningId);
			ps.setBoolean(3, screeningRequested);
			ps.setString(4, screeningLegalWorkFlowHashValue);
			ps.setBoolean(5, screeningCompleted);
			ps.setBoolean(6, isRejected);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	public void updateKycnPersonalInfo(Long screeningNaturalId, String colName, long kycnId) throws SQLException {
		String sql = null;
		sql = "UPDATE kycn_personal_info set screening_id=? WHERE " + colName + "=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setLong(2, kycnId);
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}

	}

	public void updateKycnPersonalInfo(Long screeningNaturalId, String colName, String custId) throws SQLException {
		String sql = null;
		sql = "UPDATE kycn_personal_info set screening_id=? WHERE " + colName + "=? ";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, screeningNaturalId);
			ps.setString(2, custId);
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}

	}

	/**
	 * @return current time stamp
	 */
	public java.sql.Timestamp getCurrentDateAndtime() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	}
	
	
	public String callProcedurePrimaryCustomerDetails(long screeningNId) throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call screening_n_primary_person(?,?)}");
			stmt.setLong(1, screeningNId);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return result;
	}
}
