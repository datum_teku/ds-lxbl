package com.trustaml.service.screening.natural.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestNaturalRequestPrimary {

	@JsonProperty("screening_n_request_data")
	private ScreeningNRequest screeningNRequest;

	@JsonProperty("screening_n_match_data")
	private List<ScreeningNMatchInfo> matchInfoList;

	public RequestNaturalRequestPrimary(ScreeningNRequest screeningNRequest, List<ScreeningNMatchInfo> matchInfoList) {
		super();
		this.screeningNRequest = screeningNRequest;
		this.matchInfoList = matchInfoList;
	}

	public RequestNaturalRequestPrimary() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreeningNRequest getScreeningNRequest() {
		return screeningNRequest;
	}

	public void setScreeningNRequest(ScreeningNRequest screeningNRequest) {
		this.screeningNRequest = screeningNRequest;
	}

	public List<ScreeningNMatchInfo> getMatchInfoList() {
		return matchInfoList;
	}

	public void setMatchInfoList(List<ScreeningNMatchInfo> matchInfoList) {
		this.matchInfoList = matchInfoList;
	}

}
