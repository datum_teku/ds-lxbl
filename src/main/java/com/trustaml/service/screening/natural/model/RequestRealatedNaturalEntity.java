package com.trustaml.service.screening.natural.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestRealatedNaturalEntity {

	@JsonProperty("related_entity_request_data")
	private ScreeningNRelatedRequest screeningNRelatedRequest;

	@JsonProperty("related_entity_match_data")
	private List<ScreeningNMatchInfo> listScreeningNMatchInfo;

	public RequestRealatedNaturalEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequestRealatedNaturalEntity(ScreeningNRelatedRequest screeningNRelatedRequest,
			List<ScreeningNMatchInfo> listScreeningNMatchInfo) {
		super();
		this.screeningNRelatedRequest = screeningNRelatedRequest;
		this.listScreeningNMatchInfo = listScreeningNMatchInfo;
	}

	public ScreeningNRelatedRequest getScreeningNRelatedRequest() {
		return screeningNRelatedRequest;
	}

	public void setScreeningNRelatedRequest(ScreeningNRelatedRequest screeningNRelatedRequest) {
		this.screeningNRelatedRequest = screeningNRelatedRequest;
	}

	public List<ScreeningNMatchInfo> getListScreeningNMatchInfo() {
		return listScreeningNMatchInfo;
	}

	public void setListScreeningNMatchInfo(List<ScreeningNMatchInfo> listScreeningNMatchInfo) {
		this.listScreeningNMatchInfo = listScreeningNMatchInfo;
	}

}
