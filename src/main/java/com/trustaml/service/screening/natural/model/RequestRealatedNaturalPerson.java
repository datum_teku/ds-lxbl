package com.trustaml.service.screening.natural.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestRealatedNaturalPerson {

	@JsonProperty("related_person_request_data")
	private ScreeningNRelatedRequest screeningNRelatedRequest;

	@JsonProperty("screening_n_match_data")
	private List<ScreeningNMatchInfo> listScreeningNMatchInfo;

	public RequestRealatedNaturalPerson(ScreeningNRelatedRequest screeningNRelatedRequest,
			List<ScreeningNMatchInfo> listScreeningNMatchInfo) {
		super();
		this.screeningNRelatedRequest = screeningNRelatedRequest;
		this.listScreeningNMatchInfo = listScreeningNMatchInfo;
	}

	public RequestRealatedNaturalPerson() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreeningNRelatedRequest getScreeningNRelatedRequest() {
		return screeningNRelatedRequest;
	}

	public void setScreeningNRelatedRequest(ScreeningNRelatedRequest screeningNRelatedRequest) {
		this.screeningNRelatedRequest = screeningNRelatedRequest;
	}

	public List<ScreeningNMatchInfo> getListScreeningNMatchInfo() {
		return listScreeningNMatchInfo;
	}

	public void setListScreeningNMatchInfo(List<ScreeningNMatchInfo> listScreeningNMatchInfo) {
		this.listScreeningNMatchInfo = listScreeningNMatchInfo;
	}

}
