package com.trustaml.service.screening.natural.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;

public class RequestRelatedNaturalEntity {

	@JsonProperty("related_entity_request_data")
	private RequestRelatedEntityRequestData screeningNRelatedRequest;

	@JsonProperty("related_entity_match_data")
	private List<ScreeningNMatchInfo> listScreeningNMatchInfo;

	public RequestRelatedNaturalEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequestRelatedEntityRequestData getScreeningNRelatedRequest() {
		return screeningNRelatedRequest;
	}

	public void setScreeningNRelatedRequest(RequestRelatedEntityRequestData screeningNRelatedRequest) {
		this.screeningNRelatedRequest = screeningNRelatedRequest;
	}

	public List<ScreeningNMatchInfo> getListScreeningNMatchInfo() {
		return listScreeningNMatchInfo;
	}

	public void setListScreeningNMatchInfo(List<ScreeningNMatchInfo> listScreeningNMatchInfo) {
		this.listScreeningNMatchInfo = listScreeningNMatchInfo;
	}

}
