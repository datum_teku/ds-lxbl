package com.trustaml.service.screening.natural.model;

import java.sql.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

//class for table screening_action 

@JsonFilter("FilterAction")
public class ScreeningNAction {
	
    @JsonProperty("reply-id")
	long replyId;	
	@JsonProperty("reason")
	private String reason;
	@JsonProperty("action-type")
	private String action;
	@JsonProperty("reply-date")
	private Date action_date;
	@JsonProperty("reply-time")
	private Time action_time;
	@JsonProperty("replied")
	private boolean replied;
	@JsonProperty("checker")
	private String checker;
	
	public ScreeningNAction(long replyId, String reason, String action, Date action_date, 
			Time action_time, boolean replied, String checker) {
		super();
		this.replyId = replyId;		
		this.reason = reason;
		this.action = action;
		this.action_date = action_date;
		this.action_time = action_time;
		this.replied = replied;
		this.checker = checker;
	}

	public ScreeningNAction() {
		super();
		this.replyId = 0;		
		this.reason = null;
		this.action = null;
		this.checker="";
		this.action_date = null;
		this.action_time = null;
		this.replied = false;
	}
	
		public Date getAction_date() {
		return action_date;
	}

	public void setAction_date(Date action_date) {
		this.action_date = action_date;
	}

	public Time getAction_time() {
		return action_time;
	}

	public void setAction_time(Time action_time) {
		this.action_time = action_time;
	}

	public boolean isReplied() {
		return replied;
	}

	public void setReplied(boolean replied) {
		this.replied = replied;
	}

		public long getReplyId() {
			return replyId;
		}
		public void setReplyId(long replyId) {
			this.replyId = replyId;
		}
		public String getReason() {
			return reason;
		}
		public void setReason(String reason) {
			this.reason = reason;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String string) {
			this.action = string;
		}

		public String getChecker() {
			return checker;
		}

		public void setChecker(String checker) {
			this.checker = checker;
		}
}