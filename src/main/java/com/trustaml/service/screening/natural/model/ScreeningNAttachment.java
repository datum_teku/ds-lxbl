package com.trustaml.service.screening.natural.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningNAttachment {
	@JsonProperty("screening_request_id")
	private long screeningRequestId;
	@JsonProperty("scanned_document_type")
	private String scannedDocumentType;
	@JsonProperty("scanned_content")
	private String scannedContent;
	@JsonProperty("extension_text")
	private String extensionText;
	@JsonProperty("notes")
	String notes;
	@JsonProperty("action_id")
	private String screeningActionId;
	
	public ScreeningNAttachment(long screeningRequestId, String scannedDocumentType, String scannedContent,
			String fileType, String notes,String screeningActionId) {
		super();
		this.screeningRequestId = screeningRequestId;
		this.scannedDocumentType = scannedDocumentType;
		this.scannedContent = scannedContent;
		this.extensionText = fileType;
		this.notes = notes;
		this.screeningActionId=screeningActionId;
	}
	public ScreeningNAttachment() {
		super();
		this.screeningRequestId =0 ;
		this.scannedDocumentType = "";
		this.scannedContent = "";
		this.notes = "";
		this.extensionText ="";
		this.screeningActionId="";
	}
	

	public String getScreeningActionId() {
		return screeningActionId;
	}
	public void setScreeningActionId(String screeningActionId) {
		this.screeningActionId = screeningActionId;
	}
	public String getExtensionText() {
		return extensionText;
	}
	public void setExtensionText(String extentionText) {
		this.extensionText = extentionText;
	}
	public long getScreeningRequestId() {
		return screeningRequestId;
	}
	public void setScreeningRequestId(long screeningRequestId) {
		this.screeningRequestId = screeningRequestId;
	}
	public String getScannedDocumentType() {
		return scannedDocumentType;
	}
	public void setScannedDocumentType(String scannedDocumentType) {
		this.scannedDocumentType = scannedDocumentType;
	}
	public String getScannedContent() {
		return scannedContent;
	}
	public void setScannedContent(String scannedContent) {
		this.scannedContent = scannedContent;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	

}