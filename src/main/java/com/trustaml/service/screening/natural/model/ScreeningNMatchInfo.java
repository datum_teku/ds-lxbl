package com.trustaml.service.screening.natural.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreeningNMatchInfo {
    @JsonProperty ("type")
    private String type;
    
    @JsonProperty("id")
    private long matchId;
    
    @JsonProperty("risk")
    private int risk;
    
    @JsonProperty("match")
    private boolean match;
    
    public ScreeningNMatchInfo() {
        super();
        this.type = "";
        this.matchId = 0;
        this.risk = 0;
        this.match = false;
    }

    public ScreeningNMatchInfo(String type, long matchId, int risk, boolean match) {
        super();
        this.type = type;
        this.matchId = matchId;
        this.risk = risk;
        this.match = match;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }
    
    
}
