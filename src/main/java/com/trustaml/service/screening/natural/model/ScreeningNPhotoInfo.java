package com.trustaml.service.screening.natural.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningNPhotoInfo {
	@JsonProperty("is-photo-available")
	private boolean isPhotoAvailavble;
	@JsonProperty("photo-data")
	private String photoData;	
	private String extensionText;	

	public ScreeningNPhotoInfo(boolean isPhotoAvailavble, String photoData) {
		super();
		this.isPhotoAvailavble = isPhotoAvailavble;
		this.photoData = photoData;
		this.extensionText = "jpg";
	}

	public ScreeningNPhotoInfo() {
		super();
		this.isPhotoAvailavble = false;
		this.photoData = "";
		this.extensionText = "jpg";
	}

	public boolean getIsPhotoAvailavble() {
		return isPhotoAvailavble;
	}

	public void setIsPhotoAvailavble(boolean isPhotoAvailavble) {
		this.isPhotoAvailavble = isPhotoAvailavble;
	}

	public String getPhotoData() {
		return photoData;
	}

	public void setPhotoData(String photoData) {
		this.photoData = photoData;
	}

	public String getExtensionText() {
		return extensionText;
	}

	public void setExtensionText(String extensionText) {
		this.extensionText = extensionText;
	}

}
