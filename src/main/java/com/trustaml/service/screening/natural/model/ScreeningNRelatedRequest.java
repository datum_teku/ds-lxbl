package com.trustaml.service.screening.natural.model;

import java.sql.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreeningNRelatedRequest {
	@JsonProperty("id")
	private long id;
	
	@JsonProperty("branch-sol-id")
	private String branchSolId;
	
	@JsonProperty("purpose_of_screening_n")
	private String purposeOfScreeningN;
	
	@JsonProperty("has_kyc")
	private boolean hasKyc;
	
	@JsonProperty("kyc_id")
	private long kycId;
	
	@JsonProperty("first_name")
	private String firstName;
	
	@JsonProperty("middle_name")
	private String middleName;
	
	@JsonProperty("last_name")
	private String lastName;
	
	@JsonProperty("lsf_name")
	private String lsfName;
	
	@JsonProperty("lsm_name")
	private String lsmName;
	
	@JsonProperty("lsl_name")
	private String lslName;
	
	@JsonProperty("date_of_birth")
	private java.sql.Date dateOfBirth;
	
	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationType;
	
	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationNo;
	
	@JsonProperty("country_of_issue")
	private String countryOfIssue;
	
	@JsonProperty("zone")
	private String zone;
	
	@JsonProperty("district")
	private String district;
	
	@JsonProperty("mn_vdc")
	private String mnVdc;
	
	@JsonProperty("username")
	private String maker;
	
	@JsonProperty("request_date")
	private java.sql.Date requestDate;
	
	@JsonProperty("request_time")
	private java.sql.Time requestTime;
	
	@JsonProperty("cust_id")
	private String custId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("screening_completed")
	private boolean screeningCompleted;
	
	@JsonProperty("risk")
	private int risk;
	
	@JsonProperty("workflow_completed")
	private boolean workflowCompleted;
	
	@JsonProperty("account_type")
	private String accountType;
	
	@JsonProperty("accounts_n_sub_type")
	private String accountSubType;
	
	@JsonProperty("hasNominee")
	private boolean hasNominee;
	
	@JsonProperty("num_hasNominee")
	private int numNominee;
	
	@JsonProperty("hasMandate")
	private boolean hasMandate;
	
	@JsonProperty("num_hasMandate")
	private int numMandate;
	
	@JsonProperty("hasSignatory")
	private boolean hasSignatory;
	
	@JsonProperty("num_hasSignatory")
	private int numSignatory;
	
	@JsonProperty("find_match_index")
	private String matchIndex;
	
	@JsonProperty("notes")
	private String notes;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("province")
	private String province;

	@JsonProperty("salutation")
	private String salutation;
	
	@JsonProperty("gender")
	private String gender;
	
	@JsonProperty("mobile_number")
	private String mobileNumber;
	
	@JsonProperty("email_id")
	private String emailId;
	
	@JsonProperty("name_of_institution")
	private String nameOfInstitution;

	@JsonProperty("date_of_birth_bs")
	private Date dateOfBirthBS;

	
	@JsonProperty("ward_no")
	private String wardNo;
	
	@JsonProperty("pan_number")
	private String panNo;

	
	@JsonProperty("repair_screening_n_request_id")
	private long repairScreeningNRequestId;
	
	public ScreeningNRelatedRequest() {
		super();
		this.id = 0;
		this.branchSolId = "";
		this.purposeOfScreeningN = "";
		this.hasKyc = false;
		this.kycId = 0;
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.dateOfBirth = null;
		this.primaryIdentificationType = "";
		this.primaryIdentificationNo = "";
		this.countryOfIssue = "";
		this.zone = "";
		this.district = "";
		this.mnVdc = "";
		this.maker = "";
		this.requestDate = null;
		this.requestTime = null;
		this.custId = "";
		this.status = "";
		this.screeningCompleted = false;
		this.risk = 0;
		this.workflowCompleted = false;
		this.accountType = "";
		this.accountSubType = "";
		this.hasNominee = false;
		this.numNominee = 0;
		this.hasMandate = false;
		this.numMandate = 0;
		this.hasSignatory = false;
		this.numSignatory = 0;
		this.matchIndex = "0.5";
		this.notes = "";
		this.state = "";
		this.province = "";

		this.salutation = "";
		this.gender = "";
		this.mobileNumber = "";
		this.emailId = "";
		this.repairScreeningNRequestId = 0;
		this.wardNo="";
		this.panNo="";

	}

	public ScreeningNRelatedRequest(long id, String branchSolId, String purposeOfScreeningN, boolean hasKyc, long kycId,
			String firstName, String middleName, String lastName, String lsfName, String lsmName, String lslName,
			Date dateOfBirth, String primaryIdentificationType, String primaryIdentificationNo, String countryOfIssue,
			String zone, String district, String mnVdc, String maker, Date requestDate, Time requestTime, String custId,
			String status, boolean screeningCompleted, int risk, boolean workflowCompleted, String accountType,
			String accountSubType, boolean hasNominee, int numNominee, boolean hasMandate, int numMandate,
			boolean hasSignatory, int numSignatory, String matchIndex, String notes, String state, String province,
			String salutation, String gender, String mobileNumber, String emailId, String nameOfInstitution,
			long repairScreeningNRequestId) {
		super();
		this.id = id;
		this.branchSolId = branchSolId;
		this.purposeOfScreeningN = purposeOfScreeningN;
		this.hasKyc = hasKyc;
		this.kycId = kycId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.dateOfBirth = dateOfBirth;
		this.primaryIdentificationType = primaryIdentificationType;
		this.primaryIdentificationNo = primaryIdentificationNo;
		this.countryOfIssue = countryOfIssue;
		this.zone = zone;
		this.district = district;
		this.mnVdc = mnVdc;
		this.maker = maker;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.custId = custId;
		this.status = status;
		this.screeningCompleted = screeningCompleted;
		this.risk = risk;
		this.workflowCompleted = workflowCompleted;
		this.accountType = accountType;
		this.accountSubType = accountSubType;
		this.hasNominee = hasNominee;
		this.numNominee = numNominee;
		this.hasMandate = hasMandate;
		this.numMandate = numMandate;
		this.hasSignatory = hasSignatory;
		this.numSignatory = numSignatory;
		this.matchIndex = matchIndex;
		this.notes = notes;
		this.state = state;
		this.province = province;
		this.salutation = salutation;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.nameOfInstitution = nameOfInstitution;
		this.repairScreeningNRequestId = repairScreeningNRequestId;
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreeningN;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreeningN = purposeOfScreening;
	}

	public boolean isHasKyc() {
		return hasKyc;
	}

	public void setHasKyc(boolean hasKyc) {
		this.hasKyc = hasKyc;
	}

	public long getKycId() {
		return kycId;
	}

	public void setKycId(long kycId) {
		this.kycId = kycId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public java.sql.Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(java.sql.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPrimaryIdentificationType() {
		return primaryIdentificationType;
	}

	public void setPrimaryIdentificationType(String primaryIdentificationType) {
		this.primaryIdentificationType = primaryIdentificationType;
	}

	public String getPrimaryIdentificationNo() {
		return primaryIdentificationNo;
	}

	public void setPrimaryIdentificationNo(String primaryIdentificationNo) {
		this.primaryIdentificationNo = primaryIdentificationNo;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public java.sql.Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(java.sql.Date requestDate) {
		this.requestDate = requestDate;
	}

	public java.sql.Time getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(java.sql.Time requestTime) {
		this.requestTime = requestTime;
	}

	public String getMatchIndex() {
		return matchIndex;
	}

	public void setMatchIndex(String matchIndex) {
		this.matchIndex = matchIndex;
	}

	public String getPurposeOfScreeningN() {
		return purposeOfScreeningN;
	}

	public void setPurposeOfScreeningN(String purposeOfScreeningN) {
		this.purposeOfScreeningN = purposeOfScreeningN;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isScreeningCompleted() {
		return screeningCompleted;
	}

	public void setScreeningCompleted(boolean screeningCompleted) {
		this.screeningCompleted = screeningCompleted;
	}

	public int getRisk() {
		return risk;
	}

	public void setRisk(int risk) {
		this.risk = risk;
	}

	public boolean isWorkflowCompleted() {
		return workflowCompleted;
	}

	public void setWorkflowCompleted(boolean workflowCompleted) {
		this.workflowCompleted = workflowCompleted;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public boolean hasNominee() {
		return hasNominee;
	}

	public void setNominee(boolean hasNominee) {
		this.hasNominee = hasNominee;
	}

	public int getNumNominee() {
		return numNominee;
	}

	public void setNumNominee(int numNominee) {
		this.numNominee = numNominee;
	}

	public boolean hasMandate() {
		return hasMandate;
	}

	public void setMandate(boolean hasMandate) {
		this.hasMandate = hasMandate;
	}

	public int getNumMandate() {
		return numMandate;
	}

	public void setNumMandate(int numMandate) {
		this.numMandate = numMandate;
	}

	public boolean hasSignatory() {
		return hasSignatory;
	}

	public void setSignatory(boolean hasSignatory) {
		this.hasSignatory = hasSignatory;
	}

	public int getNumSignatory() {
		return numSignatory;
	}

	public void setNumSignatory(int numSignatory) {
		this.numSignatory = numSignatory;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isHasNominee() {
		return hasNominee;
	}

	public void setHasNominee(boolean hasNominee) {
		this.hasNominee = hasNominee;
	}

	public boolean isHasMandate() {
		return hasMandate;
	}

	public void setHasMandate(boolean hasMandate) {
		this.hasMandate = hasMandate;
	}

	public boolean isHasSignatory() {
		return hasSignatory;
	}

	public void setHasSignatory(boolean hasSignatory) {
		this.hasSignatory = hasSignatory;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getDateOfBirthBS() {
		return dateOfBirthBS;
	}

	public void setDateOfBirthBS(Date dateOfBirthBS) {
		this.dateOfBirthBS = dateOfBirthBS;
	}
	
	

	public long getRepairScreeningNRequestId() {
		return repairScreeningNRequestId;
	}

	public void setRepairScreeningNRequestId(long repairScreeningNRequestId) {
		this.repairScreeningNRequestId = repairScreeningNRequestId;
	}

	@Override
	public String toString() {
		return "ScreeningNRelatedRequest [id=" + id + ", branchSolId=" + branchSolId + ", purposeOfScreeningN="
				+ purposeOfScreeningN + ", hasKyc=" + hasKyc + ", kycId=" + kycId + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", lsfName=" + lsfName + ", lsmName="
				+ lsmName + ", lslName=" + lslName + ", dateOfBirth=" + dateOfBirth + ", primaryIdentificationType="
				+ primaryIdentificationType + ", primaryIdentificationNo=" + primaryIdentificationNo
				+ ", countryOfIssue=" + countryOfIssue + ", zone=" + zone + ", district=" + district + ", mnVdc="
				+ mnVdc + ", maker=" + maker + ", requestDate=" + requestDate + ", requestTime=" + requestTime
				+ ", custId=" + custId + ", status=" + status + ", screeningCompleted=" + screeningCompleted + ", risk="
				+ risk + ", workflowCompleted=" + workflowCompleted + ", accountType=" + accountType
				+ ", accountSubType=" + accountSubType + ", hasNominee=" + hasNominee + ", numNominee=" + numNominee
				+ ", hasMandate=" + hasMandate + ", numMandate=" + numMandate + ", hasSignatory=" + hasSignatory
				+ ", numSignatory=" + numSignatory + ", matchIndex=" + matchIndex + ", notes=" + notes + ", state="
				+ state + ", province=" + province + ", salutation=" + salutation + ", gender=" + gender
				+ ", mobileNumber=" + mobileNumber + ", emailId=" + emailId + "]";
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

}