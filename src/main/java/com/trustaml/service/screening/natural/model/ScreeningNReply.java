package com.trustaml.service.screening.natural.model;

import com.fasterxml.jackson.annotation.JsonProperty;

//class for table screening_reply

public class ScreeningNReply {
	@JsonProperty("id")
	private long id;
	@JsonProperty("screening_n_id")
	private long screeningNId;
	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;
	@JsonProperty("action_type")
	private String actionType;
	@JsonProperty("reason")
	private String reason;
	
	private boolean actionTaken;
	@JsonProperty("reply_date")
	private java.sql.Date replyDate;
	@JsonProperty("reply_time")
	private java.sql.Time replyTime;
	@JsonProperty("checker")
	private String checker;
    
	
	public ScreeningNReply() {
        super();
        id = 0;
        this.screeningNId = 0;
        this.purposeOfScreening = "";
        this.actionType = "";
        this.reason = "";
        this.actionTaken = false;
        this.replyDate = null;
        this.replyTime = null;
        this.checker = "";
    }
	
	public ScreeningNReply(long id, long screeningNId, String purposeOfScreening, String actionType, String reason, 
	        boolean actionTaken, java.sql.Date replyDate, java.sql.Time replyTime, String checker) {
        super();
        this.id = id;
        this.screeningNId = screeningNId;
        this.purposeOfScreening = purposeOfScreening;
        this.actionType = actionType;
        this.reason = reason;
        this.actionTaken = actionTaken;
        this.replyDate = replyDate;
        this.replyTime = replyTime;
        this.checker = checker;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getScreeningNId() {
        return screeningNId;
    }

    public void setScreeningNId(long screeningNId) {
        this.screeningNId = screeningNId;
    }

    public String getPurposeOfScreening() {
        return purposeOfScreening;
    }

    public void setPurposeOfScreening(String purposeOfScreening) {
        this.purposeOfScreening = purposeOfScreening;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(boolean actionTaken) {
        this.actionTaken = actionTaken;
    }

    public java.sql.Date getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(java.sql.Date replyDate) {
        this.replyDate = replyDate;
    }

    public java.sql.Time getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(java.sql.Time replyTime) {
        this.replyTime = replyTime;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }
		
}