package com.trustaml.service.screening.natural.model;

import java.sql.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreeningNRequest {
	@JsonProperty("id")
	private long id;

	@JsonProperty("branch-sol-id")
	private String branchSolId;

	@JsonProperty("purpose_of_screening_n")
	private String purposeOfScreeningN;

	@JsonProperty("sender_receiver")
	private String senderReceiver;

	@JsonProperty("has_kycn")
	private boolean hasKycn;

	@JsonProperty("kycn_id")
	private long kycnId;

	@JsonProperty("has_cust_id")
	private boolean hasCustId;

	@JsonProperty("cust_id")
	private String custId;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("lsf_name")
	private String lsfName;

	@JsonProperty("lsm_name")
	private String lsmName;

	@JsonProperty("lsl_name")
	private String lslName;

	@JsonProperty("date_of_birth")
	private java.sql.Date dateOfBirth;

	@JsonProperty("primary_identification_document_type")
	private String primaryIdentificationType;

	@JsonProperty("primary_identification_document_no")
	private String primaryIdentificationNo;

	@JsonProperty("country_of_issue")
	private String countryOfIssue;

	@JsonProperty("state")
	private String state;

	@JsonProperty("district")
	private String district;

	@JsonProperty("mn_vdc")
	private String mnVdc;

	@JsonProperty("ward_no")
	private String wardNo;

	@JsonProperty("username")
	private String maker;

	@JsonProperty("request_date")
	private java.sql.Date requestDate;

	@JsonProperty("request_time")
	private java.sql.Time requestTime;

	@JsonProperty("status")
	private String status;

	@JsonProperty("screening_completed")
	private boolean screeningCompleted;

	@JsonProperty("risk")
	private int risk;

	@JsonProperty("workflow_completed")
	private boolean workflowCompleted;

	@JsonProperty("account_type")
	private String accountType;

	@JsonProperty("account_sub_type")
	private String accountSubType;

	@JsonProperty("has_nominee")
	private boolean hasNominee;

	@JsonProperty("num_nominee")
	private int numNominee;

	@JsonProperty("has_mandate")
	private boolean hasMandate;

	@JsonProperty("num_mandate")
	private int numMandate;

	@JsonProperty("has_signatory")
	private boolean hasSignatory;

	@JsonProperty("num_signatory")
	private int numSignatory;

	@JsonProperty("has_joint_account_holder")
	private boolean hasJointAccountHolder;

	@JsonProperty("num_joint_account_holder")
	private int numJointAccountHolder;

	@JsonProperty("find_match_index")
	private float matchIndex;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("zone")
	private String zone;

	@JsonProperty("province")
	private String province;

	@JsonProperty("repair_screening_n_request_id")
	private long repairScreeningNRequestId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("mobile_number")
	private String mobileNumber;

	@JsonProperty("email_id")
	private String emailId;

	@JsonProperty("nature_of_account")
	private String natureOfAccount;

	@JsonProperty("scheme_description")
	private String schemeDescription;

	@JsonProperty("deposit_amount")
	private String depositAmount;

	@JsonProperty("pan_number")
	private String panNumber;

	@JsonProperty("repaired")
	private boolean repaired;

	@JsonProperty("migrated")
	private boolean migrated;

	@JsonProperty("date_of_birth_bs")
	private Date dateOfBirthBS;

	public ScreeningNRequest() {
		super();
		this.id = 0;
		this.branchSolId = "";
		this.purposeOfScreeningN = "";
		this.senderReceiver = "";
		this.hasKycn = false;
		this.kycnId = 0;
		this.hasCustId = false;
		this.custId = "";
		this.firstName = "";
		this.middleName = "";
		this.lastName = "";
		this.lsfName = "";
		this.lsmName = "";
		this.lslName = "";
		this.dateOfBirth = null;
		this.primaryIdentificationType = "";
		this.primaryIdentificationNo = "";
		this.countryOfIssue = "";
		this.state = "";
		this.district = "";
		this.mnVdc = "";
		this.wardNo = "";
		this.maker = "";
		this.requestDate = null;
		this.requestTime = null;
		this.status = "";
		this.screeningCompleted = false;
		this.risk = 0;
		this.workflowCompleted = false;
		this.accountType = "";
		this.accountSubType = "";
		this.hasNominee = false;
		this.numNominee = 0;
		this.hasMandate = false;
		this.numMandate = 0;
		this.hasSignatory = false;
		this.numSignatory = 0;
		this.hasJointAccountHolder = false;
		this.numJointAccountHolder = 0;
		this.matchIndex = (float) 0.0;
		this.notes = "";
		this.zone = "";
		this.province = "";
		this.repairScreeningNRequestId = 0;
		this.salutation = "";
		this.gender = "";
		this.mobileNumber = "";
		this.emailId = "";
		this.natureOfAccount = "";
		this.schemeDescription = "";
		this.depositAmount = "";
		this.panNumber = "";
		this.repaired = false;
		this.migrated = false;
	}

	public ScreeningNRequest(long id, String branchSolId, String purposeOfScreeningN, String senderReceiver,
			boolean hasKycn, long kycnId, boolean hasCustId, String custId, String firstName, String middleName,
			String lastName, String lsfName, String lsmName, String lslName, Date dateOfBirth,
			String primaryIdentificationType, String primaryIdentificationNo, String countryOfIssue, String state,
			String district, String mnVdc, String maker, Date requestDate, Time requestTime, String status,
			boolean screeningCompleted, int risk, boolean workflowCompleted, String accountType, String accountSubType,
			boolean hasNominee, int numNominee, boolean hasMandate, int numMandate, boolean hasSignatory,
			int numSignatory, boolean hasJointAccountHolder, int numJointAccountHolder, float matchIndex, String notes,
			String zone, String province, long repairScreeningNRequestId, String salutation, String gender,
			String mobileNumber, String emailId, String natureOfAccount, String schemeDescription, String depositAmount,
			String panNumber, String wardNo) {
		super();
		this.id = id;
		this.branchSolId = branchSolId;
		this.purposeOfScreeningN = purposeOfScreeningN;
		this.senderReceiver = senderReceiver;
		this.hasKycn = hasKycn;
		this.kycnId = kycnId;
		this.hasCustId = hasCustId;
		this.custId = custId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsfName = lsfName;
		this.lsmName = lsmName;
		this.lslName = lslName;
		this.dateOfBirth = dateOfBirth;
		this.primaryIdentificationType = primaryIdentificationType;
		this.primaryIdentificationNo = primaryIdentificationNo;
		this.countryOfIssue = countryOfIssue;
		this.state = state;
		this.district = district;
		this.mnVdc = mnVdc;
		this.maker = maker;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.status = status;
		this.screeningCompleted = screeningCompleted;
		this.risk = risk;
		this.workflowCompleted = workflowCompleted;
		this.accountType = accountType;
		this.accountSubType = accountSubType;
		this.hasNominee = hasNominee;
		this.numNominee = numNominee;
		this.hasMandate = hasMandate;
		this.numMandate = numMandate;
		this.hasSignatory = hasSignatory;
		this.numSignatory = numSignatory;
		this.hasJointAccountHolder = hasJointAccountHolder;
		this.numJointAccountHolder = numJointAccountHolder;
		this.matchIndex = matchIndex;
		this.notes = notes;
		this.zone = zone;
		this.province = province;
		this.repairScreeningNRequestId = repairScreeningNRequestId;
		this.salutation = salutation;
		this.gender = gender;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.natureOfAccount = natureOfAccount;
		this.schemeDescription = schemeDescription;
		this.depositAmount = depositAmount;
		this.panNumber = panNumber;
		this.wardNo = wardNo;
	}

	public ScreeningNRequest(KycnPersonalInfo kycnPersonalInfo) {
		this.firstName = kycnPersonalInfo.getFirstName();
		this.middleName = kycnPersonalInfo.getMiddleName();
		this.lastName = kycnPersonalInfo.getLastName();

	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getNatureOfAccount() {
		return natureOfAccount;
	}

	public void setNatureOfAccount(String natureOfAccount) {
		this.natureOfAccount = natureOfAccount;
	}

	public String getSchemeDescription() {
		return schemeDescription;
	}

	public void setSchemeDescription(String schemeDescription) {
		this.schemeDescription = schemeDescription;
	}

	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBranchSolId() {
		return branchSolId;
	}

	public void setBranchSolId(String branchSolId) {
		this.branchSolId = branchSolId;
	}

	public String getPurposeOfScreeningN() {
		return purposeOfScreeningN;
	}

	public void setPurposeOfScreeningN(String purposeOfScreeningN) {
		this.purposeOfScreeningN = purposeOfScreeningN;
	}

	public boolean isHasKycn() {
		return hasKycn;
	}

	public void setHasKycn(boolean hasKycn) {
		this.hasKycn = hasKycn;
	}

	public long getKycnId() {
		return kycnId;
	}

	public void setKycnId(long kycnId) {
		this.kycnId = kycnId;
	}

	public boolean isHasCustId() {
		return hasCustId;
	}

	public void setHasCustId(boolean hasCustId) {
		this.hasCustId = hasCustId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLsfName() {
		return lsfName;
	}

	public void setLsfName(String lsfName) {
		this.lsfName = lsfName;
	}

	public String getLsmName() {
		return lsmName;
	}

	public void setLsmName(String lsmName) {
		this.lsmName = lsmName;
	}

	public String getLslName() {
		return lslName;
	}

	public void setLslName(String lslName) {
		this.lslName = lslName;
	}

	public java.sql.Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(java.sql.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPrimaryIdentificationType() {
		return primaryIdentificationType;
	}

	public void setPrimaryIdentificationType(String primaryIdentificationType) {
		this.primaryIdentificationType = primaryIdentificationType;
	}

	public String getPrimaryIdentificationNo() {
		return primaryIdentificationNo;
	}

	public void setPrimaryIdentificationNo(String primaryIdentificationNo) {
		this.primaryIdentificationNo = primaryIdentificationNo;
	}

	public String getCountryOfIssue() {
		return countryOfIssue;
	}

	public void setCountryOfIssue(String countryOfIssue) {
		this.countryOfIssue = countryOfIssue;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getMnVdc() {
		return mnVdc;
	}

	public void setMnVdc(String mnVdc) {
		this.mnVdc = mnVdc;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public java.sql.Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(java.sql.Date requestDate) {
		this.requestDate = requestDate;
	}

	public java.sql.Time getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(java.sql.Time requestTime) {
		this.requestTime = requestTime;
	}

	public float getMatchIndex() {
		return matchIndex;
	}

	public void setMatchIndex(float matchIndex) {
		this.matchIndex = matchIndex;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getSenderReceiver() {
		return senderReceiver;
	}

	public void setSenderReceiver(String senderReceiver) {
		this.senderReceiver = senderReceiver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isScreeningCompleted() {
		return screeningCompleted;
	}

	public void setScreeningCompleted(boolean screeningCompleted) {
		this.screeningCompleted = screeningCompleted;
	}

	public int getRisk() {
		return risk;
	}

	public void setRisk(int risk) {
		this.risk = risk;
	}

	public boolean isWorkflowCompleted() {
		return workflowCompleted;
	}

	public void setWorkflowCompleted(boolean workflowCompleted) {
		this.workflowCompleted = workflowCompleted;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public boolean isHasNominee() {
		return hasNominee;
	}

	public void setHasNominee(boolean hasNominee) {
		this.hasNominee = hasNominee;
	}

	public int getNumNominee() {
		return numNominee;
	}

	public void setNumNominee(int numNominee) {
		this.numNominee = numNominee;
	}

	public boolean isHasMandate() {
		return hasMandate;
	}

	public void setHasMandate(boolean hasMandate) {
		this.hasMandate = hasMandate;
	}

	public int getNumMandate() {
		return numMandate;
	}

	public void setNumMandate(int numMandate) {
		this.numMandate = numMandate;
	}

	public boolean isHasSignatory() {
		return hasSignatory;
	}

	public void setHasSignatory(boolean hasSignatory) {
		this.hasSignatory = hasSignatory;
	}

	public int getNumSignatory() {
		return numSignatory;
	}

	public void setNumSignatory(int numSignatory) {
		this.numSignatory = numSignatory;
	}

	public boolean isHasJointAccountHolder() {
		return hasJointAccountHolder;
	}

	public void setHasJointAccountHolder(boolean hasJointAccountHolder) {
		this.hasJointAccountHolder = hasJointAccountHolder;
	}

	public int getNumJointAccountHolder() {
		return numJointAccountHolder;
	}

	public void setNumJointAccountHolder(int numJointAccountHolder) {
		this.numJointAccountHolder = numJointAccountHolder;
	}

	public long getRepairScreeningNRequestId() {
		return repairScreeningNRequestId;
	}

	public void setRepairScreeningNRequestId(long repairScreeningNRequestId) {
		this.repairScreeningNRequestId = repairScreeningNRequestId;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public boolean isRepaired() {
		return repaired;
	}

	public void setRepaired(boolean repaired) {
		this.repaired = repaired;
	}

	public boolean isMigrated() {
		return migrated;
	}

	public void setMigrated(boolean migrated) {
		this.migrated = migrated;
	}

	public Date getDateOfBirthBS() {
		return dateOfBirthBS;
	}

	public void setDateOfBirthBS(Date dateOfBirthBS) {
		this.dateOfBirthBS = dateOfBirthBS;
	}

}