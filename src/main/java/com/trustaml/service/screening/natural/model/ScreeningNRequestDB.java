package com.trustaml.service.screening.natural.model;

import java.sql.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningNRequestDB {
    @JsonProperty("id")
    private long id;
    @JsonProperty("branch_sol_id")
    private String branchSolId;
    @JsonProperty("purpose_of_screening_n")
    private String purposeOfScreeningN;
    @JsonProperty("is_existing_customer")
    private boolean existingCustomer;
    @JsonProperty("kycn_id")
    private String kycnId;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("middle_name")
    private String middleName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("lsf_name")
    private String lsfName;
    @JsonProperty("lsm_name")
    private String lsmName;
    @JsonProperty("lsl_name")
    private String lslName;
    @JsonProperty("date_of_birth")
    private java.sql.Date dateOfBirth;
    @JsonProperty("primary_identification_type")
    private String primaryIdentificationType;
    @JsonProperty("primary_identification_document_no")
    private String primaryIdentificationNo;
    @JsonProperty("country_of_issue")
    private String countryOfIssue;
    @JsonProperty("zone")
    private String zone;
    @JsonProperty("district")
    private String district;
    @JsonProperty("mn_vdc")
    private String mnVdc;
    @JsonProperty("username")
    private String maker;
    @JsonProperty("request_date")
    private java.sql.Date requestDate;
    @JsonProperty("request_time")
    private java.sql.Time requestTime;
    @JsonProperty("cust_id")
    private String custId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("screening_completed")
    private boolean screeningCompleted;
    @JsonProperty("risk")
    private int risk;
    @JsonProperty("workflow_completed")
    private boolean workflowCompleted;
    @JsonProperty("account_type")
    private String accountType;
    @JsonProperty("account_sub_type")
    private String accountSubType;
    @JsonProperty("hasNominee")
    private boolean hasNominee;
    @JsonProperty("num_hasNominee")
    private int numNominee;
    @JsonProperty("hasMandate")
    private boolean hasMandate;
    @JsonProperty("num_hasMandate")
    private int numMandate;
    @JsonProperty("hasSignatory")
    private boolean hasSignatory;
    @JsonProperty("num_hasSignatory")
    private int numSignatory;    
    @JsonProperty("find_match_index")
    private String matchIndex;

    public ScreeningNRequestDB() {
        super();
        this.id = 0;
        this.branchSolId = "";
        this.purposeOfScreeningN = "";
        this.existingCustomer = false;
        this.kycnId = "";
        this.firstName = "";
        this.middleName = "";
        this.lastName = "";
        this.lsfName = "";
        this.lsmName = "";
        this.lslName = "";
        this.dateOfBirth = null;
        this.primaryIdentificationType = "";
        this.primaryIdentificationNo = "";
        this.countryOfIssue = "";
        this.zone = "";
        this.district = "";
        this.mnVdc = "";
        this.maker = "";
        this.requestDate = null;
        this.requestTime = null;
        this.custId = "";
        this.status = "";
        this.screeningCompleted = false;
        this.risk = 0;
        this.workflowCompleted = false;
        this.accountType = "";
        this.accountSubType = "";
        this.hasNominee = false;
        this.numNominee = 0;
        this.hasMandate = false;
        this.numMandate = 0;
        this.hasSignatory = false;
        this.numSignatory = 0;
        this.matchIndex = "0.5";
    }

    

    public ScreeningNRequestDB(long id, String branchSolId, String purposeOfScreeningN, boolean existingCustomer, String kycnId, String firstName, String middleName, String lastName, String lsfName, String lsmName, String lslName, Date dateOfBirth, String primaryIdentificationType, String primaryIdentificationNo, String countryOfIssue, String zone, String district, String mnVdc, String maker,
            Date requestDate, Time requestTime, String custId, String status, boolean screeningCompleted, int risk, boolean workflowCompleted, String accountType, String accountSubType, boolean hasNominee, int numNominee, boolean hasMandate, int numMandate, boolean hasSignatory, int numSignatory, String matchIndex) {
        super();
        this.id = id;
        this.branchSolId = branchSolId;
        this.purposeOfScreeningN = purposeOfScreeningN;
        this.existingCustomer = existingCustomer;
        this.kycnId = kycnId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.lsfName = lsfName;
        this.lsmName = lsmName;
        this.lslName = lslName;
        this.dateOfBirth = dateOfBirth;
        this.primaryIdentificationType = primaryIdentificationType;
        this.primaryIdentificationNo = primaryIdentificationNo;
        this.countryOfIssue = countryOfIssue;
        this.zone = zone;
        this.district = district;
        this.mnVdc = mnVdc;
        this.maker = maker;
        this.requestDate = requestDate;
        this.requestTime = requestTime;
        this.custId = custId;
        this.status = status;
        this.screeningCompleted = screeningCompleted;
        this.risk = risk;
        this.workflowCompleted = workflowCompleted;
        this.accountType = accountType;
        this.accountSubType = accountSubType;
        this.hasNominee = hasNominee;
        this.numNominee = numNominee;
        this.hasMandate = hasMandate;
        this.numMandate = numMandate;
        this.hasSignatory = hasSignatory;
        this.numSignatory = numSignatory;
        this.matchIndex = matchIndex;
    }



    public String getBranchSolId() {
        return branchSolId;
    }

    public void setBranchSolId(String branchSolId) {
        this.branchSolId = branchSolId;
    }

    public String getPurposeOfScreening() {
        return purposeOfScreeningN;
    }

    public void setPurposeOfScreening(String purposeOfScreening) {
        this.purposeOfScreeningN = purposeOfScreening;
    }

    public boolean getExistingCustomer() {
        return existingCustomer;
    }

    public void setExistingCustomer(boolean existingCustomer) {
        this.existingCustomer = existingCustomer;
    }

    public String getKycnId() {
        return kycnId;
    }

    public void setKycnId(String kycnId) {
        this.kycnId = kycnId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLsfName() {
        return lsfName;
    }

    public void setLsfName(String lsfName) {
        this.lsfName = lsfName;
    }

    public String getLsmName() {
        return lsmName;
    }

    public void setLsmName(String lsmName) {
        this.lsmName = lsmName;
    }

    public String getLslName() {
        return lslName;
    }

    public void setLslName(String lslName) {
        this.lslName = lslName;
    }

    public java.sql.Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.sql.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPrimaryIdentificationType() {
        return primaryIdentificationType;
    }

    public void setPrimaryIdentificationType(String primaryIdentificationType) {
        this.primaryIdentificationType = primaryIdentificationType;
    }

    public String getPrimaryIdentificationNo() {
        return primaryIdentificationNo;
    }

    public void setPrimaryIdentificationNo(String primaryIdentificationNo) {
        this.primaryIdentificationNo = primaryIdentificationNo;
    }

    public String getCountryOfIssue() {
        return countryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        this.countryOfIssue = countryOfIssue;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMnVdc() {
        return mnVdc;
    }

    public void setMnVdc(String mnVdc) {
        this.mnVdc = mnVdc;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public java.sql.Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(java.sql.Date requestDate) {
        this.requestDate = requestDate;
    }

    public java.sql.Time getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(java.sql.Time requestTime) {
        this.requestTime = requestTime;
    }

    public String getMatchIndex() {
        return matchIndex;
    }

    public void setMatchIndex(String matchIndex) {
        this.matchIndex = matchIndex;
    }

    public String getPurposeOfScreeningN() {
        return purposeOfScreeningN;
    }

    public void setPurposeOfScreeningN(String purposeOfScreeningN) {
        this.purposeOfScreeningN = purposeOfScreeningN;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isScreeningCompleted() {
        return screeningCompleted;
    }

    public void setScreeningCompleted(boolean screeningCompleted) {
        this.screeningCompleted = screeningCompleted;
    }

    public int getRisk() {
        return risk;
    }

    public void setRisk(int risk) {
        this.risk = risk;
    }

    public boolean isWorkflowCompleted() {
        return workflowCompleted;
    }

    public void setWorkflowCompleted(boolean workflowCompleted) {
        this.workflowCompleted = workflowCompleted;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountSubType() {
        return accountSubType;
    }

    public void setAccountSubType(String accountSubType) {
        this.accountSubType = accountSubType;
    }



    public boolean hasNominee() {
        return hasNominee;
    }



    public void setNominee(boolean hasNominee) {
        this.hasNominee = hasNominee;
    }



    public int getNumNominee() {
        return numNominee;
    }



    public void setNumNominee(int numNominee) {
        this.numNominee = numNominee;
    }



    public boolean hasMandate() {
        return hasMandate;
    }



    public void setMandate(boolean hasMandate) {
        this.hasMandate = hasMandate;
    }



    public int getNumMandate() {
        return numMandate;
    }



    public void setNumMandate(int numMandate) {
        this.numMandate = numMandate;
    }



    public boolean hasSignatory() {
        return hasSignatory;
    }



    public void setSignatory(boolean hasSignatory) {
        this.hasSignatory = hasSignatory;
    }



    public int getNumSignatory() {
        return numSignatory;
    }



    public void setNumSignatory(int numSignatory) {
        this.numSignatory = numSignatory;
    }
    
    

}
