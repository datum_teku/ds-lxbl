package com.trustaml.service.screening.natural.model;

import java.util.Date;

public class ScreeningNResultTemplate {

	String firstName;
	String middleName;
	String lastName;
	String lsf;
	String lsm;
	String lsl;
	Date dateOfBirth;
	String country;	
	String primaryIdentificationType;
	String primaryIdentificationNo;
	String maker;
	Date requestDate;
	String requestTime;
	String organisationName;
	String designation;
	String pepOrUnId;
	public ScreeningNResultTemplate(String firstName, String middleName, String lastName, String lsf, String lsm, String lsl,
			Date dateOfBirth, String country, String primaryIdentificationType, String primaryIdentificationNo,
			String maker, Date requestDate, String requestTime, String organisationName, String designation,
			String pepOrUnId) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.lsf = lsf;
		this.lsm = lsm;
		this.lsl = lsl;
		this.dateOfBirth = dateOfBirth;
		this.country = country;
		this.primaryIdentificationType = primaryIdentificationType;
		this.primaryIdentificationNo = primaryIdentificationNo;
		this.maker = maker;
		this.requestDate = requestDate;
		this.requestTime = requestTime;
		this.organisationName = organisationName;
		this.designation = designation;
		this.pepOrUnId = pepOrUnId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLsf() {
		return lsf;
	}
	public void setLsf(String lsf) {
		this.lsf = lsf;
	}
	public String getLsm() {
		return lsm;
	}
	public void setLsm(String lsm) {
		this.lsm = lsm;
	}
	public String getLsl() {
		return lsl;
	}
	public void setLsl(String lsl) {
		this.lsl = lsl;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPrimaryIdentificationType() {
		return primaryIdentificationType;
	}
	public void setPrimaryIdentificationType(String primaryIdentificationType) {
		this.primaryIdentificationType = primaryIdentificationType;
	}
	public String getPrimaryIdentificationNo() {
		return primaryIdentificationNo;
	}
	public void setPrimaryIdentificationNo(String primaryIdentificationNo) {
		this.primaryIdentificationNo = primaryIdentificationNo;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	public String getOrganisationName() {
		return organisationName;
	}
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getPepOrUnId() {
		return pepOrUnId;
	}
	public void setPepOrUnId(String pepOrUnId) {
		this.pepOrUnId = pepOrUnId;
	}
	
}
