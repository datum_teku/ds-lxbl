package com.trustaml.service.screening.natural.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ScreeningNatural {

	@JsonProperty("screening_n_request_primary")
	private RequestNaturalRequestPrimary requestNaturalRequestPrimary;

	@JsonProperty("screening_n_request_attachments")
	private List<ScreeningNAttachment> screeningNAttachmentList;

	@JsonProperty("screening_n_request_related_person")
	private List<RequestRelatedNaturalPerson> listRequestRealatedNaturalPerson;

	@JsonProperty("screening_n_request_related_entity")
	private List<RequestRelatedNaturalEntity> listRequestRealatedNaturalEntity;

	@JsonProperty("user")
	private User user;

	@JsonProperty("action")
	private String action;

	@JsonProperty("forward_to")
	private List<String> forwardTo;

	public ScreeningNatural() {
		super();
		forwardTo = new ArrayList<String>();

		// TODO Auto-generated constructor stub
	}

	public ScreeningNatural(RequestNaturalRequestPrimary requestNaturalRequestPrimary,
			List<ScreeningNAttachment> screeningNAttachmentList,
			List<RequestRelatedNaturalPerson> listRequestRealatedNaturalPerson,
			List<RequestRelatedNaturalEntity> listRequestRealatedNaturalEntity, User user, String action,
			List<String> forwardTo) {
		super();
		this.requestNaturalRequestPrimary = requestNaturalRequestPrimary;
		this.screeningNAttachmentList = screeningNAttachmentList;
		this.listRequestRealatedNaturalPerson = listRequestRealatedNaturalPerson;
		this.listRequestRealatedNaturalEntity = listRequestRealatedNaturalEntity;
		this.user = user;
		this.action = action;
		this.forwardTo = forwardTo;
	}

	public RequestNaturalRequestPrimary getRequestNaturalRequestPrimary() {
		return requestNaturalRequestPrimary;
	}

	public void setRequestNaturalRequestPrimary(RequestNaturalRequestPrimary requestNaturalRequestPrimary) {
		this.requestNaturalRequestPrimary = requestNaturalRequestPrimary;
	}

	public List<ScreeningNAttachment> getScreeningNAttachmentList() {
		return screeningNAttachmentList;
	}

	public void setScreeningNAttachmentList(List<ScreeningNAttachment> screeningNAttachmentList) {
		this.screeningNAttachmentList = screeningNAttachmentList;
	}

	public List<RequestRelatedNaturalPerson> getListRequestRealatedNaturalPerson() {
		return listRequestRealatedNaturalPerson;
	}

	public void setListRequestRealatedNaturalPerson(
			List<RequestRelatedNaturalPerson> listRequestRealatedNaturalPerson) {
		this.listRequestRealatedNaturalPerson = listRequestRealatedNaturalPerson;
	}

	public List<RequestRelatedNaturalEntity> getListRequestRealatedNaturalEntity() {
		return listRequestRealatedNaturalEntity;
	}

	public void setListRequestRealatedNaturalEntity(
			List<RequestRelatedNaturalEntity> listRequestRealatedNaturalEntity) {
		this.listRequestRealatedNaturalEntity = listRequestRealatedNaturalEntity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<String> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(List<String> forwardTo) {
		this.forwardTo = forwardTo;
	}

}
