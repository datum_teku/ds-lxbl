package com.trustaml.service.screening.natural.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScreeningOfMigratedData {

	@JsonProperty("branch")
	private String branch;

	@JsonProperty("range")
	private String dateRange;

	@JsonProperty("to_date")
	private Date toDate;

	@JsonProperty("from_date")
	private Date fromDate;

	@JsonProperty("level")
	private float matchLevel;

	@JsonProperty("kyc_type")
	private String type;

	public ScreeningOfMigratedData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreeningOfMigratedData(String branch, String dateRange, Date toDate, Date fromDate, float matchLevel,
			String type) {
		super();
		this.branch = branch;
		this.dateRange = dateRange;
		this.toDate = toDate;
		this.fromDate = fromDate;
		this.matchLevel = matchLevel;
		this.type = type;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public float getMatchLevel() {
		return matchLevel;
	}

	public void setMatchLevel(float matchLevel) {
		this.matchLevel = matchLevel;
	}

	public String getDateRange() {
		return dateRange;
	}

	public void setDateRange(String dateRange) throws ParseException {
		String dateFrom = dateRange.substring(0, dateRange.indexOf("to")).trim();
		String dateTo = dateRange.substring(dateRange.indexOf("to") + 2, dateRange.length()).trim();
		setToDate(getDate(dateTo));
		setFromDate(getDate(dateFrom));
		this.dateRange = dateRange;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private static java.sql.Date getDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

}
