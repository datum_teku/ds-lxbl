package com.trustaml.service.screening.natural.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class KYCNDataMigrationJSONNodeMapper {
	public static JsonNode getMainJsonNode(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readValue(jsonString, JsonNode.class);
		return jsonNode;
	}

	final static public boolean getMatchSanctionList(JsonNode rootNode, String[] toCompareJSONNode)
			throws JsonProcessingException {
		boolean matchCase = false;
		ObjectMapper mapper = new ObjectMapper();
		for (String strJsonNode : toCompareJSONNode) {
			JsonNode jsonNode = rootNode.get(strJsonNode);
			String matchedNode = mapper.treeToValue(jsonNode, String.class);
			if (matchedNode != null) {
				matchCase = true;
				return matchCase;
			} else {
				matchCase = false;
			}
		}
		return matchCase;
	}

	final static public Map<String, String> getMatchSanctionListMap(JsonNode rootNode, String[] toCompareJSONNode)
			throws IOException {
		Map<String, String> mapMatchedSanctionList = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		for (String strJsonNode : toCompareJSONNode) {
			JsonNode jsonNode = rootNode.get(strJsonNode);
			if (jsonNode != null) {
				String matchedNode = mapper.treeToValue(jsonNode, String.class);
				if (matchedNode != null) {
					mapMatchedSanctionList.put(strJsonNode, matchedNode);

				}
			}

		}
		return mapMatchedSanctionList;
	}
}
