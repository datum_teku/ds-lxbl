package com.trustaml.service.swift.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.swift.dao.SwiftPartDao;

@Path("/swift")
public class SwiftRestFulService {

	@Inject
	SwiftPartDao swiftDao;

	/**
	 * @param swiftMt103
	 * @return size of swift json
	 * @description accepts JSON String and saves it in table
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws SQLException
	 * @throws IOException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveSwift(String swiftMt103) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, SQLException, IOException, ParseException, NoSuchAlgorithmException {
		if (!swiftMt103.isEmpty() || !swiftMt103.equals("")) {
			Integer count = swiftDao.insertMeta(swiftMt103);
			return ResponseReturn.sucess(count.toString());
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}

	}

}
