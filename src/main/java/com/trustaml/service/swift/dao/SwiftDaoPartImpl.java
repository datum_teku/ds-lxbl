package com.trustaml.service.swift.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.swift.model.mt103prtfile.FileMeta;
import com.trustaml.service.swift.model.mt103prtmessage.Field20;
import com.trustaml.service.swift.model.mt103prtmessage.Field23B;
import com.trustaml.service.swift.model.mt103prtmessage.Field32A;
import com.trustaml.service.swift.model.mt103prtmessage.Field50K;
import com.trustaml.service.swift.model.mt103prtmessage.Field52A;
import com.trustaml.service.swift.model.mt103prtmessage.Field53A;
import com.trustaml.service.swift.model.mt103prtmessage.Field57A;
import com.trustaml.service.swift.model.mt103prtmessage.Field59;
import com.trustaml.service.swift.model.mt103prtmessage.Field70;
import com.trustaml.service.swift.model.mt103prtmessage.Field71A;
import com.trustaml.service.swift.model.mt103prtmessage.InstanceTypeAndTransmission;
import com.trustaml.service.swift.model.mt103prtmessage.Interventions;
import com.trustaml.service.swift.model.mt103prtmessage.MessageHeader;
import com.trustaml.service.swift.model.mt103prtmessage.MessageTrailer;
import com.trustaml.service.swift.model.mt103prtmessage.Receiver;
import com.trustaml.service.swift.model.mt103prtmessage.Sender;

public class SwiftDaoPartImpl {
	@Inject
	DBConnection dbConnection;

	/*
	 * public void insertMt103(String swiftMessage, String maker) { // TODO
	 * 
	 * }
	 */
	/**
	 * @param meta
	 * @param fileMetaHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertMeta(FileMeta meta, String fileMetaHashValue) throws SQLException {

		String sql = "insert into swift_meta(descriptions,date,time, others,hash) values (?,?,?,?,?)";
		Long metaId = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, meta.getDescription());
			ps.setDate(2, meta.getDate());
			ps.setTime(3, meta.getTime());
			ps.setString(4, meta.getOthers());
			ps.setString(5, fileMetaHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				metaId = rs.getLong(1);
			}

		} finally {
			ps.close();
			conn.close();
		}
		return metaId;

	}

	// public void insertListMessageHeader(MessageHeader messageHeader, Long
	// metaId) throws SQLException {
	// insertMessageHeader(messageHeader, metaId);
	// }
	/**
	 * @param messageHeader
	 * @param metaId
	 * @param messageHeaderHashValue
	 * @return
	 * @throws SQLException
	 */
	public Long insertMessageHeader(MessageHeader messageHeader, Long metaId, String messageHeaderHashValue)
			throws SQLException {
		String sql = "insert into swift_message_header (nica_swift_meta_id, swift_input,hash) values (?,?,?)";
		Long headerId = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, metaId);
			ps.setString(2, messageHeader.getSwiftInput());
			ps.setString(3, messageHeaderHashValue);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				headerId = rs.getLong(1);
			}

		} finally {
			ps.close();
			conn.close();
		}
		return headerId;

	}

	/**
	 * @param sender
	 * @param headerId
	 * @param senderHashValue
	 * @throws SQLException
	 */
	public void insertSender(Sender sender, Long headerId, String senderHashValue) throws SQLException {
		String sql = "insert into swift_sender(nica_swift_message_header_id, swift_code, bank_name, city, country_code,hash) values (?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = dbConnection.getConnection();
			ps = conn.prepareStatement(sql);
			ps.setLong(1, headerId);
			ps.setString(2, sender.getSwiftCode());
			ps.setString(3, sender.getBankName());
			ps.setString(4, sender.getCity());
			ps.setString(5, sender.getCode());
			ps.setString(6, senderHashValue);
			ps.executeUpdate();
		} finally {
			conn.close();
			ps.close();
		}

	}

	/**
	 * @param receiver
	 * @param headerId
	 * @param receiverHashValue
	 * @throws SQLException
	 */
	public void insertReceiver(Receiver receiver, Long headerId, String receiverHashValue) throws SQLException {
		String sql = "insert into swift_receiver(nica_swift_message_header_id, swift_code, bank_name, branch_name, city, country_code,hash) values (?,?,?,?,?,?,?)";
		Connection conn = dbConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql);
		try {
			ps.setLong(1, headerId);
			ps.setString(2, receiver.getSwiftCode());
			ps.setString(3, receiver.getBankName());
			ps.setString(4, receiver.getBranchName());
			ps.setString(5, receiver.getCity());
			ps.setString(6, receiver.getCode());
			ps.setString(7, receiverHashValue);
			ps.executeUpdate();
		} finally {
			conn.close();
			ps.close();
		}
	}

	/**
	 * @param transmission
	 * @param metaId
	 * @param instanceTypeAndTransmissionHashValue
	 * @throws SQLException
	 */
	public void insertInstanceTypeAndTransmission(InstanceTypeAndTransmission transmission, Long metaId,
			String instanceTypeAndTransmissionHashValue) throws SQLException {
		String sql = "insert into swift_instance_type_and_transmission(nica_swift_meta_id, message_input_reference, descriptions, network_delivery_status, priority_delivery,hash) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, transmission.getReference());
			ps.setString(3, transmission.getDescription());
			ps.setString(4, transmission.getStatus());
			ps.setString(5, transmission.getPriority());
			ps.setString(6, instanceTypeAndTransmissionHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param interventions
	 * @param metaId
	 * @param intervention
	 * @throws SQLException
	 */
	public void insertIntervention(Interventions interventions, Long metaId, String intervention) throws SQLException {
		String sql = "insert into swift_interventions(nica_swift_meta_id, operator, category, creation_time, application,hash) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, interventions.getOperator());
			ps.setString(3, interventions.getCategory());
			ps.setString(4, interventions.getCreationTime());
			ps.setString(5, interventions.getApplication());
			ps.setString(6, intervention);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param messageTrailer
	 * @param metaId
	 * @param messageTrailerHashValue
	 * @throws SQLException
	 */
	public void insertMessageTrailer(MessageTrailer messageTrailer, Long metaId, String messageTrailerHashValue)
			throws SQLException {
		String sql = "insert into swift_message_trailer(nica_swift_meta_id, pki_signature,hash) values (?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, messageTrailer.getSignature());
			ps.setString(3, messageTrailerHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field20
	 * @param metaId
	 * @param field20HashValue
	 * @throws SQLException
	 */
	public void insertField20(Field20 field20, Long metaId, String field20HashValue) throws SQLException {
		String sql = "insert into swift_message_text_20(nica_swift_meta_id, description, value,hash) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field20.getDescription());
			ps.setString(3, field20.getValue());
			ps.setString(4, field20HashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field23b
	 * @param metaId
	 * @param field23bHashValue
	 * @throws SQLException
	 */
	public void insertField23B(Field23B field23b, Long metaId, String field23bHashValue) throws SQLException {
		String sql = "insert into swift_message_text_23b(nica_swift_meta_id, description, value,hash) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field23b.getDescription());
			ps.setString(3, field23b.getValue());
			ps.setString(4, field23bHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field32a
	 * @param metaId
	 * @param field32aHashValue
	 * @throws SQLException
	 */
	public void insertField32A(Field32A field32a, Long metaId, String field32aHashValue) throws SQLException {
		String sql = "insert into swift_message_text_32a(nica_swift_meta_id, description, date, amount , currency,hash) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field32a.getDescription());
			ps.setDate(3, new java.sql.Date(field32a.getDate().getTime()));
			ps.setString(4, field32a.getAmount());
			ps.setString(5, field32a.getCurrency());
			ps.setString(6, field32aHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field50k
	 * @param metaId
	 * @param file50kHashValue
	 * @throws SQLException
	 */
	public void insertField50K(Field50K field50k, Long metaId, String file50kHashValue) throws SQLException {
		String sql = "insert into swift_message_text_50k(nica_swift_meta_id,account_number,address,first_name,midddle_name,last_name,hash) values (?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field50k.getAccount());
			ps.setString(3, field50k.getAddress());
			ps.setString(4, field50k.getFirstName());
			ps.setString(5, field50k.getMiddleName());
			ps.setString(6, field50k.getLastName());
			ps.setString(7, file50kHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field57a
	 * @param metaId
	 * @param field57aHashValue
	 * @throws SQLException
	 */
	public void insertField57A(Field57A field57a, Long metaId, String field57aHashValue) throws SQLException {
		String sql = "insert into swift_message_text_57aa(nica_swift_meta_id,description,swift_code,bank_name,address,country_code,hash) values (?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field57a.getDescription());
			ps.setString(3, field57a.getSwiftCode());
			ps.setString(4, field57a.getBankName());
			ps.setString(5, field57a.getAddress());
			ps.setString(6, field57a.getCode());
			ps.setString(7, field57aHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field59
	 * @param metaId
	 * @param file59KHashValue
	 * @throws SQLException
	 */
	public void insertField59(Field59 field59, Long metaId, String file59KHashValue) throws SQLException {
		String sql = "insert into swift_message_text_59(nica_swift_meta_id,description,acccount_number,name,address,hash) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field59.getDescription());
			ps.setString(3, field59.getAccount());
			ps.setString(4, field59.getName());
			ps.setString(5, field59.getAddress());
			ps.setString(6, file59KHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}
	}

	/**
	 * @param field70
	 * @param metaId
	 * @param field70HashValue
	 * @throws SQLException
	 */
	public void insertField70(Field70 field70, Long metaId, String field70HashValue) throws SQLException {
		String sql = "insert into swift_message_text_70(nica_swift_meta_id,description,value,hash) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field70.getDescription());
			ps.setString(3, field70.getValue());
			ps.setString(4, field70HashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param field71a
	 * @param metaId
	 * @param field71aHashValue
	 * @throws SQLException
	 */
	public void insertField71A(Field71A field71a, Long metaId, String field71aHashValue) throws SQLException {
		String sql = "insert into swift_message_text_71a(nica_swift_meta_id,description,value,hash) values (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field71a.getDescription());
			ps.setString(3, field71a.getValue());
			ps.setString(4, field71aHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param field52a
	 * @param metaId
	 * @param field52aHashValue
	 * @throws SQLException
	 */
	public void insertField52A(Field52A field52a, Long metaId, String field52aHashValue) throws SQLException {
		String sql = "insert into swift_message_text_52a(nica_swift_meta_id, swift_code,bank_name,address, country_code,hash) values (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field52a.getSwiftCode());
			ps.setString(3, field52a.getBankName());
			ps.setString(4, field52a.getAddress());
			ps.setString(5, field52a.getCode());
			ps.setString(6, field52aHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param field53a
	 * @param metaId
	 * @param field53aHashValue
	 * @throws SQLException
	 */
	public void insertField53A(Field53A field53a, Long metaId, String field53aHashValue) throws SQLException {
		String sql = "insert into swift_message_text_53a(nica_swift_meta_id, swift_code,bank_name,address, country_code,account_number,hash) values (?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, metaId);
			ps.setString(2, field53a.getSwiftCode());
			ps.setString(3, field53a.getBankName());
			ps.setString(4, field53a.getAddress());
			ps.setString(5, field53a.getCode());
			ps.setString(6, field53a.getAccount());
			ps.setString(7, field53aHashValue);
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

}
