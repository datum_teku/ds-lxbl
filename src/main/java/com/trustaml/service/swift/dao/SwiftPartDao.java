package com.trustaml.service.swift.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.screening.legal.model.RequestPrimaryRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.swift.model.Swift;
import com.trustaml.service.swift.model.SwiftDto;

@Stateless
public class SwiftPartDao {

	@Inject
	SwiftDaoPartImpl swiftDaoImpl;

	@Inject
	DBConnection dbConnection;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	/*
	 * public void insertMt103(String swiftMessage, String maker) {
	 * swiftDaoImpl.insertMt103(swiftMessage, maker); }
	 */
	/**
	 * @param swiftMT103
	 * @return
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public Integer insertMeta(String swiftMT103) throws SQLException, JsonParseException, JsonMappingException,
			IOException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {

		ObjectMapper mapper = new ObjectMapper();
		Swift swift = mapper.readValue(swiftMT103, Swift.class);

		for (SwiftDto swiftDto : swift.getListSwiftDto()) {
			// if (null != swiftDto) {
			// String fileMetaHashValue =
			// hashCodeGenerator.generateHashValueForSwiftFileMeta(swiftDto.getFileMeta());
			// Long metaId = swiftDaoImpl.insertMeta(swiftDto.getFileMeta(),
			// fileMetaHashValue);
			// if (null != metaId) {
			long screeningLegalRequestId = 0;
			// String messageHeaderHashValue = hashCodeGenerator
			// .generateHashValueForMessageHeader(swiftDto.getMessageHeader(),
			// metaId);
			// Long headerId =
			// swiftDaoImpl.insertMessageHeader(swiftDto.getMessageHeader(),
			// metaId,
			// messageHeaderHashValue);

			if (swiftDto.getMessageText().getField50k() != null) {
				RequestPrimaryRequestData requestPrimaryRequestData = new RequestPrimaryRequestData();
				requestPrimaryRequestData.setPurposeOfScreening("SWIFT");
				requestPrimaryRequestData.setAccountsLSubType("Primary account holder");
				// requestPrimaryRequestData
				// .setAccountsLSubType(swiftDto.getMessageText().getField50k().getDescription());
				requestPrimaryRequestData.setMnVdc(swiftDto.getMessageText().getField50k().getAddress());
				requestPrimaryRequestData.setNameOfInstitution(swiftDto.getMessageText().getField50k().getFirstName()
						+ " " + swiftDto.getMessageText().getField50k().getMiddleName() + " "
						+ swiftDto.getMessageText().getField50k().getLastName());
				requestPrimaryRequestData.setNotes("ACCOUNT= " + swiftDto.getMessageText().getField50k().getAccount());
				if (swiftDto.getMessageHeader().getSwiftInput() != null) {
					requestPrimaryRequestData.setSwiftInput(swiftDto.getMessageHeader().getSwiftInput());
				} else {
					requestPrimaryRequestData.setSwiftInput(swiftDto.getMessageHeader().getSwiftOutput());
				}

				String screeningLegalHashValue = hashCodeGenerator
						.generateHashValueForScreeningLegal(requestPrimaryRequestData, swift.getUser());
				screeningLegalRequestId = screeningLegalDaoImpl.saveScreeningRequest(requestPrimaryRequestData,
						swift.getUser(), screeningLegalHashValue);
				// String file50KHashValue = hashCodeGenerator
				// .generateHashValueForFile50K(swiftDto.getMessageText().getField50k(),
				// metaId);
				// swiftDaoImpl.insertField50K(swiftDto.getMessageText().getField50k(),
				// metaId, file50KHashValue);

				if (swiftDto.getMessageHeader().getSender() != null) {
					RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
					requestRelatedEntityRequestData
							.setNameOfInstitution(swiftDto.getMessageHeader().getSender().getBankName());
					requestRelatedEntityRequestData.setAccountsLSubType("Linked entity");
					requestRelatedEntityRequestData
							.setNotes("SWIFT CODE " + swiftDto.getMessageHeader().getSender().getSwiftCode());
					String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
							requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
					long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
							swift.getUser(), screeningLegalRequestEntityHashValue);
					String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
							.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
									requestRelatedEntityRequestData);
					screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
							requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);

					// String senderHashValue = hashCodeGenerator
					// .generateHashValueForSender(swiftDto.getMessageHeader().getSender(),
					// headerId);
					// swiftDaoImpl.insertSender(swiftDto.getMessageHeader().getSender(),
					// headerId,
					// senderHashValue);
				}
			} else {
				if (swiftDto.getMessageHeader().getSender() != null) {
					// String senderHashValue = hashCodeGenerator
					// .generateHashValueForSender(swiftDto.getMessageHeader().getSender(),
					// headerId);
					// swiftDaoImpl.insertSender(swiftDto.getMessageHeader().getSender(),
					// headerId,
					// senderHashValue);
					RequestPrimaryRequestData requestPrimaryRequestData = new RequestPrimaryRequestData();
					requestPrimaryRequestData
							.setNameOfInstitution(swiftDto.getMessageHeader().getSender().getBankName());
					requestPrimaryRequestData.setPurposeOfScreening("SWIFT");
					requestPrimaryRequestData.setMnVdc(swiftDto.getMessageHeader().getSender().getCity());
					requestPrimaryRequestData.setAccountsLSubType("Primary account holder");
					requestPrimaryRequestData
							.setNotes("SWIFT CODE " + swiftDto.getMessageHeader().getSender().getSwiftCode());

					if (swiftDto.getMessageHeader().getSwiftInput() != null) {
						requestPrimaryRequestData.setSwiftInput(swiftDto.getMessageHeader().getSwiftInput());
					} else {
						requestPrimaryRequestData.setSwiftInput(swiftDto.getMessageHeader().getSwiftOutput());
					}
					String screeningLegalHashValue = hashCodeGenerator
							.generateHashValueForScreeningLegal(requestPrimaryRequestData, swift.getUser());
					screeningLegalRequestId = screeningLegalDaoImpl.saveScreeningRequest(requestPrimaryRequestData,
							swift.getUser(), screeningLegalHashValue);
				}
			}
			if (swiftDto.getMessageText().getField59() != null) {
				RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
				requestRelatedEntityRequestData.setNameOfInstitution(swiftDto.getMessageText().getField59().getName());
				requestRelatedEntityRequestData
						.setAccountsLSubType(swiftDto.getMessageText().getField59().getDescription());
				requestRelatedEntityRequestData
						.setNotes("ACCOUNT= " + swiftDto.getMessageText().getField59().getAccount());

				// Save in request
				String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
						requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
				long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
						swift.getUser(), screeningLegalRequestEntityHashValue);

				String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
								requestRelatedEntityRequestData);
				screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
						requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);

				// String file59KHashValue = hashCodeGenerator
				// .generateHashValueForFile59K(swiftDto.getMessageText().getField59(),
				// metaId);
				// swiftDaoImpl.insertField59(swiftDto.getMessageText().getField59(),
				// metaId, file59KHashValue);
			}

			if (swiftDto.getMessageHeader().getReceiver() != null) {
				RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
				requestRelatedEntityRequestData
						.setNameOfInstitution(swiftDto.getMessageHeader().getReceiver().getBankName());
				requestRelatedEntityRequestData.setMnVdc(swiftDto.getMessageHeader().getReceiver().getCity());
				requestRelatedEntityRequestData
						.setNotes("SWIFT CODE " + swiftDto.getMessageHeader().getReceiver().getSwiftCode());
				requestRelatedEntityRequestData.setAccountsLSubType("Receiver");

				String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
						requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
				long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
						swift.getUser(), screeningLegalRequestEntityHashValue);

				String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
								requestRelatedEntityRequestData);
				screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
						requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);

				// String receiverHashValue = hashCodeGenerator
				// .generateHashValueForReceiver(swiftDto.getMessageHeader().getReceiver(),
				// headerId);
				// swiftDaoImpl.insertReceiver(swiftDto.getMessageHeader().getReceiver(),
				// headerId,
				// receiverHashValue);

			}
			// if (swiftDto.getInstanceTypeAndTransmission() != null) {
			// String instanceTypeAndTransmissionHashValue = hashCodeGenerator
			// .generateHashValueForInstanceTypeAndTransmission(
			// swiftDto.getInstanceTypeAndTransmission(), metaId);
			// swiftDaoImpl.insertInstanceTypeAndTransmission(swiftDto.getInstanceTypeAndTransmission(),
			// metaId, instanceTypeAndTransmissionHashValue);
			// }
			// if (swiftDto.getInterventions() != null) {
			// String interventionHashValue = hashCodeGenerator
			// .generateHashValueForInterventions(swiftDto.getInterventions(),
			// metaId);
			// swiftDaoImpl.insertIntervention(swiftDto.getInterventions(),
			// metaId, interventionHashValue);
			// }
			// if (swiftDto.getMessageTrailer() != null) {
			// String messageTrailerHashValue = hashCodeGenerator
			// .generateHashValueForMessageTrailer(swiftDto.getMessageTrailer(),
			// metaId);
			// swiftDaoImpl.insertMessageTrailer(swiftDto.getMessageTrailer(),
			// metaId,
			// messageTrailerHashValue);
			// }
			//
			// if (swiftDto.getMessageText().getField20() != null) {
			// String field20HashValue = hashCodeGenerator
			// .generateHashValueForField20HashValue(swiftDto.getMessageText().getField20(),
			// metaId);
			// swiftDaoImpl.insertField20(swiftDto.getMessageText().getField20(),
			// metaId, field20HashValue);
			// }
			// if (swiftDto.getMessageText().getField23b() != null) {
			// String field23BHashValue = hashCodeGenerator
			// .generateHashValueForField23B(swiftDto.getMessageText().getField23b(),
			// metaId);
			// swiftDaoImpl.insertField23B(swiftDto.getMessageText().getField23b(),
			// metaId, field23BHashValue);
			// }
			// if (swiftDto.getMessageText().getField32a() != null) {
			// String field32AHashValue = hashCodeGenerator
			// .generateHashValueForField32A(swiftDto.getMessageText().getField32a(),
			// metaId);
			// swiftDaoImpl.insertField32A(swiftDto.getMessageText().getField32a(),
			// metaId, field32AHashValue);
			// }

			if (swiftDto.getMessageText().getField57a() != null) {
				RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
				requestRelatedEntityRequestData
						.setNameOfInstitution(swiftDto.getMessageText().getField57a().getBankName());
				requestRelatedEntityRequestData
						.setAccountsLSubType(swiftDto.getMessageText().getField57a().getDescription());
				requestRelatedEntityRequestData.setMnVdc(swiftDto.getMessageText().getField57a().getAddress());
				requestRelatedEntityRequestData
						.setNotes("SWIFT CODE = " + swiftDto.getMessageText().getField57a().getSwiftCode());
				String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
						requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
				long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
						swift.getUser(), screeningLegalRequestEntityHashValue);
				String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
								requestRelatedEntityRequestData);

				screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
						requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);

				// String field57AHashValue = hashCodeGenerator
				// .generateHashValueForField57A(swiftDto.getMessageText().getField57a(),
				// metaId);
				// swiftDaoImpl.insertField57A(swiftDto.getMessageText().getField57a(),
				// metaId, field57AHashValue);
			}

			// if (swiftDto.getMessageText().getField70() != null) {
			// String field70HashValue = hashCodeGenerator
			// .generateHashValueForField70(swiftDto.getMessageText().getField70(),
			// metaId);
			// swiftDaoImpl.insertField70(swiftDto.getMessageText().getField70(),
			// metaId, field70HashValue);
			// }
			// if (swiftDto.getMessageText().getField71a() != null) {
			// String field71AHashValue = hashCodeGenerator
			// .generateHashValueForField71A(swiftDto.getMessageText().getField71a(),
			// metaId);
			// swiftDaoImpl.insertField71A(swiftDto.getMessageText().getField71a(),
			// metaId, field71AHashValue);
			// }
			if (swiftDto.getMessageText().getField52a() != null) {
				RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();
				requestRelatedEntityRequestData
						.setNameOfInstitution(swiftDto.getMessageText().getField52a().getBankName());
				requestRelatedEntityRequestData
						.setAccountsLSubType(swiftDto.getMessageText().getField52a().getDescription());
				requestRelatedEntityRequestData.setMnVdc(swiftDto.getMessageText().getField52a().getAddress());
				requestRelatedEntityRequestData
						.setNotes("SWIFT CODE = " + swiftDto.getMessageText().getField52a().getSwiftCode());
				String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
						requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
				long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
						swift.getUser(), screeningLegalRequestEntityHashValue);
				String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
								requestRelatedEntityRequestData);
				screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
						requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);
				// String field52AHashValue = hashCodeGenerator
				// .generateHashValueForField52A(swiftDto.getMessageText().getField52a(),
				// metaId);
				// swiftDaoImpl.insertField52A(swiftDto.getMessageText().getField52a(),
				// metaId, field52AHashValue);
			}
			if (swiftDto.getMessageText().getField53a() != null) {

				RequestRelatedEntityRequestData requestRelatedEntityRequestData = new RequestRelatedEntityRequestData();

				// System.out.println("bank name is " +
				// swiftDto.getMessageText().getField53a().getBankName());
				// System.out.println("bank name is " +
				// swiftDto.getMessageText().getField53a().getDescription());
				// System.out.println("bank name is " +
				// swiftDto.getMessageText().getField53a().getSwiftCode());
				//
				// if
				// (swiftDto.getMessageText().getField53a().getDescription()
				// .equals("Sender's Correspondent - FI BIC")) {
				// System.out
				// .println("bank name is " +
				// swiftDto.getMessageText().getField53a().getSwiftCode());
				// }
				requestRelatedEntityRequestData
						.setNameOfInstitution(swiftDto.getMessageText().getField53a().getBankName());
				requestRelatedEntityRequestData
						.setAccountsLSubType(swiftDto.getMessageText().getField53a().getDescription());
				requestRelatedEntityRequestData.setMnVdc(swiftDto.getMessageText().getField53a().getAddress());
				requestRelatedEntityRequestData
						.setNotes("SWIFT CODE = " + swiftDto.getMessageText().getField53a().getSwiftCode());

				String screeningLegalRequestEntityHashValue = hashCodeGenerator.generateHashValueForRelatedEntity(
						requestRelatedEntityRequestData, screeningLegalRequestId, swift.getUser());
				long requestRelatedId = screeningLegalDaoImpl.saveRequest(requestRelatedEntityRequestData,
						swift.getUser(), screeningLegalRequestEntityHashValue);
				String screeningLegalRequestRelatedEntityHashValue = hashCodeGenerator
						.generateHashValueForReqestRelatedRelatedEntity(requestRelatedId, screeningLegalRequestId,
								requestRelatedEntityRequestData);
				screeningLegalDaoImpl.saveRequestRelatedEntity(requestRelatedId, screeningLegalRequestId,
						requestRelatedEntityRequestData, screeningLegalRequestRelatedEntityHashValue);
				// String field53AHashValue = hashCodeGenerator
				// .generateHashValueForField53A(swiftDto.getMessageText().getField53a(),
				// metaId);
				// swiftDaoImpl.insertField53A(swiftDto.getMessageText().getField53a(),
				// metaId, field53AHashValue);
			}

			screeningLegalDaoImpl.insertScreeningWorkflow(screeningLegalRequestId);

			// }

		}
		// }
		return swift.getListSwiftDto().size();
	}

}
