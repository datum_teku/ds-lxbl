package com.trustaml.service.swift.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class Swift {

	@JsonProperty("SWIFT")
	List<SwiftDto> listSwiftDto;
	User user;

	public Swift(List<SwiftDto> listSwiftDto, User user) {
		super();
		this.listSwiftDto = listSwiftDto;
		this.user = user;
	}

	public Swift() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<SwiftDto> getListSwiftDto() {
		return listSwiftDto;
	}

	public void setListSwiftDto(List<SwiftDto> listSwiftDto) {
		this.listSwiftDto = listSwiftDto;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
