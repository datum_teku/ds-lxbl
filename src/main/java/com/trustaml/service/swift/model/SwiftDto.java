package com.trustaml.service.swift.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.swift.model.mt103prtfile.FileMeta;
import com.trustaml.service.swift.model.mt103prtmessage.InstanceTypeAndTransmission;
import com.trustaml.service.swift.model.mt103prtmessage.Interventions;
import com.trustaml.service.swift.model.mt103prtmessage.MessageHeader;
import com.trustaml.service.swift.model.mt103prtmessage.MessageText;
import com.trustaml.service.swift.model.mt103prtmessage.MessageTrailer;

public class SwiftDto {
	@JsonProperty("META")
	private FileMeta fileMeta;

	@JsonProperty("MESSAGE_HEADER")
	private MessageHeader messageHeader;

	@JsonProperty("INSTANCE_TYPE_AND_TRANSMISSION")
	private InstanceTypeAndTransmission instanceTypeAndTransmission;

	@JsonProperty("MESSAGE_TEXT")
	private MessageText messageText;

	@JsonProperty("MESSAGE_TRAILER")
	private MessageTrailer messageTrailer;

	@JsonProperty("INTERVENTIONS")
	private Interventions interventions;
	

	public SwiftDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SwiftDto(FileMeta fileMeta, MessageHeader messageHeader,
			InstanceTypeAndTransmission instanceTypeAndTransmission, MessageText messageText,
			MessageTrailer messageTrailer, Interventions interventions) {
		super();
		this.fileMeta = fileMeta;
		this.messageHeader = messageHeader;
		this.instanceTypeAndTransmission = instanceTypeAndTransmission;
		this.messageText = messageText;
		this.messageTrailer = messageTrailer;
		this.interventions = interventions;
	}

	public MessageHeader getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(MessageHeader messageHeader) {
		this.messageHeader = messageHeader;
	}

	public InstanceTypeAndTransmission getInstanceTypeAndTransmission() {
		return instanceTypeAndTransmission;
	}

	public void setInstanceTypeAndTransmission(InstanceTypeAndTransmission instanceTypeAndTransmission) {
		this.instanceTypeAndTransmission = instanceTypeAndTransmission;
	}

	public MessageText getMessageText() {
		return messageText;
	}

	public void setMessageText(MessageText messageText) {
		this.messageText = messageText;
	}

	public MessageTrailer getMessageTrailer() {
		return messageTrailer;
	}

	public void setMessageTrailer(MessageTrailer messageTrailer) {
		this.messageTrailer = messageTrailer;
	}

	public Interventions getInterventions() {
		return interventions;
	}

	public void setInterventions(Interventions interventions) {
		this.interventions = interventions;
	}

	public FileMeta getFileMeta() {
		return fileMeta;
	}

	public void setFileMeta(FileMeta fileMeta) {
		this.fileMeta = fileMeta;
	}

}
