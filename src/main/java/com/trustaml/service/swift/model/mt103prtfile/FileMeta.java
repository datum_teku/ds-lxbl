package com.trustaml.service.swift.model.mt103prtfile;

import java.sql.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FileMeta {

	@JsonProperty("ID")
	private long id;
	@JsonProperty("DESCRIPTION")
	private String description;
	@JsonProperty("OTHERS")
	private String others;
	@JsonProperty("DATE")
	private java.sql.Date date;
	@JsonProperty("TIME")
	private java.sql.Time time;

	public FileMeta() {
		super();
		this.id = 0;
		this.description = "";
		this.others = "";
		this.date = null;
		this.time = null;
	}

	public FileMeta(long id, String description, String others, Date date, Time time) {
		super();
		this.id = id;
		this.description = description;
		this.others = others;
		this.date = date;
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public java.sql.Time getTime() {
		return time;
	}

	public void setTime(java.sql.Time time) {
		this.time = time;
	}

}
