package com.trustaml.service.swift.model.mt103prtfile;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageHeader {

	@JsonProperty("SWIFT_INPUT")
	private String swiftInput;

	@JsonProperty("SENDER")
	private Map<String, String> sender;

	@JsonProperty("RECEIVER")
	private Map<String, String> receiver;

	public String getSwiftInput() {
		return swiftInput;
	}

	public void setSwiftInput(String swiftInput) {
		this.swiftInput = swiftInput;
	}

	public Map<String, String> getSender() {
		return sender;
	}

	public void setSender(Map<String, String> sender) {
		this.sender = sender;
	}

	public Map<String, String> getReceiver() {
		return receiver;
	}

	public void setReceiver(Map<String, String> receiver) {
		this.receiver = receiver;
	}

}
