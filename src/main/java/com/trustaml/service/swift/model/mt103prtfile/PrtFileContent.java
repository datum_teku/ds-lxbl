package com.trustaml.service.swift.model.mt103prtfile;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PrtFileContent {

	private FileMeta meta;
	private List<MessageHeader> messageHeader;
	private List<Map<String, String>> instance;
	private List<Map<String, Map<String, String>>> messageText;
	private List<Map<String, String>> trailer;
	private List<Map<String, String>> interventions;

	public PrtFileContent() {
		super();
		this.meta = null;
		this.messageHeader = null;
		this.instance = null;
		this.messageText = null;
		this.trailer = null;
		this.interventions = null;
	}

	public PrtFileContent(FileMeta meta, List<MessageHeader> messageHeader, List<Map<String, String>> instance,
			List<Map<String, Map<String, String>>> messageText, List<Map<String, String>> trailer,
			List<Map<String, String>> interventions) {
		super();
		this.meta = meta;
		this.messageHeader = messageHeader;
		this.instance = instance;
		this.messageText = messageText;
		this.trailer = trailer;
		this.interventions = interventions;
	}

	@JsonProperty("InstanceTypeAndTransmission")
	public List<Map<String, String>> getInstance() {
		return instance;
	}

	public void setInstance(List<Map<String, String>> instance) {
		this.instance = instance;
	}

	@JsonProperty("messageHeader")
	public List<MessageHeader> getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(List<MessageHeader> messageHeader) {
		this.messageHeader = messageHeader;
	}

	@JsonProperty("MessageText")
	public List<Map<String, Map<String, String>>> getMessageText() {
		return messageText;
	}

	public void setMessageText(List<Map<String, Map<String, String>>> messageText) {
		this.messageText = messageText;
	}

	@JsonProperty("MessageTrailer")
	public List<Map<String, String>> getTrailer() {
		return trailer;
	}

	public void setTrailer(List<Map<String, String>> trailer) {
		this.trailer = trailer;
	}

	@JsonProperty("Interventions")
	public List<Map<String, String>> getInterventions() {
		return interventions;
	}

	public void setInterventions(List<Map<String, String>> interventions) {
		this.interventions = interventions;
	}

	@JsonProperty("meta")
	public FileMeta getMeta() {
		return meta;
	}

	public void setMeta(FileMeta meta) {
		this.meta = meta;
	}

}
