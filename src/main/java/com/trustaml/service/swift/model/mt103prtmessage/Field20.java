package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field20 {

	public Field20() {
		super();
		this.id = null;
		this.description = "";
		this.value = "";
	}

	public Field20(Long id, String description, String value) {
		super();
		this.id = id;
		this.description = description;
		this.value = value;
	}

	@JsonProperty("ID")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("VALUE")
	private String value;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
