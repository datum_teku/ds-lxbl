package com.trustaml.service.swift.model.mt103prtmessage;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.trustaml.service.swift.util.DateDeserialized;

public class Field32A {

	public Field32A() {
		super();
		this.description = "";
		this.amount = "";
		this.currency = "";
		this.date = null;
		this.id = null;
	}

	public Field32A(String description, String amount, String currency, java.sql.Date date, Long id) {
		super();
		this.description = description;
		this.amount = amount;
		this.currency = currency;
		this.date = date;
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("AMOUNT")
	private String amount;

	@JsonProperty("CURRENCY")
	private String currency;

	@JsonProperty("DATE")
	@JsonDeserialize(using = DateDeserialized.class)
	private java.sql.Date date;

	@JsonProperty("ID")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

}
