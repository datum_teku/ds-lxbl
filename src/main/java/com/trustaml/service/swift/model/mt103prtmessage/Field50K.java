package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Field50K {

	public Field50K() {
		super();
		this.id = null;
		this.citizenNo = "";
		this.panNo = "";
		this.description = "";
		this.account = "";
		this.address = "";
		this.firstName = "";
		this.lastName = "";
		this.middleName = "";
	}

	public Field50K(Long id, String citizenNo, String panNo, String description, String account, String address,
			String firstName, String lastName, String middleName) {
		super();
		this.id = id;
		this.citizenNo = citizenNo;
		this.panNo = panNo;
		this.description = description;
		this.account = account;
		this.address = address;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
	}

	@JsonProperty("ID")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private String citizenNo;

	@JsonProperty("PANNO")
	private String panNo;

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("ACCOUNT")
	private String account;

	@JsonProperty("ADDRESS")
	private String address;

	@JsonProperty("FIRST_NAME")
	private String firstName;

	@JsonProperty("LAST_NAME")
	private String lastName;

	@JsonProperty("MIDDLE_NAME")
	private String middleName;

	@JsonProperty("CITIZENSHIPNO")
	public String getCitizenNo() {
		return citizenNo;
	}

	public void setCitizenNo(String citizenNo) {
		this.citizenNo = citizenNo;
	}

	@JsonProperty("CTZNO")
	public String getCTZNo() {
		return citizenNo;
	}

	public void setCTZNO(String ctzNo) {
		this.citizenNo = ctzNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

}
