package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field52A {

	public Field52A() {
		super();
		this.id = null;
		this.description = "";
		this.swiftCode = "";
		this.bankName = "";
		this.address = "";
		this.code = "";
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("SWIFT_CODE")
	private String swiftCode;

	@JsonProperty("BANK_NAME")
	private String bankName;

	@JsonProperty("ADDRESS")
	private String address;

	@JsonProperty("CODE")
	private String code;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
