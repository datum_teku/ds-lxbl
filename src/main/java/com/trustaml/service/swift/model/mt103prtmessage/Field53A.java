package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field53A {

	public Field53A() {
		super();
		this.id = null;
		this.description = "";
		this.account = "";
		this.swiftCode = "";
		this.bankName = "";
		this.address = "";
		this.code = "";
	}

	public Field53A(Long id, String description, String account, String swiftCode, String bankName, String address,
			String code) {
		super();
		this.id = id;
		this.description = description;
		this.account = account;
		this.swiftCode = swiftCode;
		this.bankName = bankName;
		this.address = address;
		this.code = code;
	}

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("ACCOUNT")
	private String account;

	@JsonProperty("SWIFT_CODE")
	private String swiftCode;

	@JsonProperty("BANK_NAME")
	private String bankName;

	@JsonProperty("ADDRESS")
	private String address;

	@JsonProperty("CODE")
	private String code;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
