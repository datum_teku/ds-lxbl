package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field57A {

	public Field57A() {
		super();
		this.id = null;
		this.description = "";
		this.swiftCode = "";
		this.bankName = "";
		this.branchName = "";
		this.address = "";
		this.code = "";
	}

	public Field57A(Long id, String description, String swiftCode, String bankName, String branchName, String address,
			String code) {
		super();
		this.id = id;
		this.description = description;
		this.swiftCode = swiftCode;
		this.bankName = bankName;
		this.branchName = branchName;
		this.address = address;
		this.code = code;
	}

	@JsonProperty("ID")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("SWIFT_CODE")
	private String swiftCode;

	@JsonProperty("BANK_NAME")
	private String bankName;

	@JsonProperty("BRANCH_NAME")
	private String branchName;

	@JsonProperty("ADDRESS")
	private String address;

	@JsonProperty("CODE")
	private String code;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
