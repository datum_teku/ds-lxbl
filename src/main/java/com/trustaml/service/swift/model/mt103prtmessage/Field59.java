package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field59 {

	public Field59() {
		super();
		this.id = null;
		this.description = "";
		this.account = "";
		this.name = "";
	}

	public Field59(Long id, String description, String account, String name, String address) {
		super();
		this.id = id;
		this.description = description;
		this.account = account;
		this.name = name;
		this.address = address;
	}

	private Long id;

	@JsonProperty("ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("ACCOUNT")
	private String account;

	@JsonProperty("NAME")
	private String name;

	@JsonProperty("ADDRESS")
	private String address;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
