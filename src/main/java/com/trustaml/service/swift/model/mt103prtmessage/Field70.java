package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field70 {

	public Field70() {
		super();
		this.id = null;
		this.description = "";
		this.value = "";
	}

	public Field70(Long id, String description, String value) {
		super();
		this.id = id;
		this.description = description;
		this.value = value;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("VALUE")
	private String value;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
