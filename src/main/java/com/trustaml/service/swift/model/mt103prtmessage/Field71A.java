package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Field71A {

	public Field71A() {
		super();
		this.id = null;
		this.value = "";
		this.description = "";
	}

	public Field71A(Long id, String value, String description) {
		super();
		this.id = id;
		this.value = value;
		this.description = description;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("VALUE")
	private String value;

	@JsonProperty("DESCRIPTION")
	private String description;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
