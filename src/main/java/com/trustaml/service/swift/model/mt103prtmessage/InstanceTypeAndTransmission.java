package com.trustaml.service.swift.model.mt103prtmessage;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InstanceTypeAndTransmission {

	public InstanceTypeAndTransmission() {
		super();
		this.id = null;
		this.reference = "";
		this.description = "";
		this.status = "";
		this.priority = "";
	}

	public InstanceTypeAndTransmission(Long id, String reference, String description, String status, String priority) {
		super();
		this.id = id;
		this.reference = reference;
		this.description = description;
		this.status = status;
		this.priority = priority;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("MESSAGE_INPUT_REFERENCE")
	private String reference;

	@JsonProperty("DESCRIPTION")
	private String description;

	@JsonProperty("NETWORK_DELIVERY_STATUS")
	private String status;

	@JsonProperty("PRIORITY_DELIVERY")
	private String priority;

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
