package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Interventions {

	public Interventions() {
		super();
		this.id = null;
		this.operator = "";
		this.category = "";
		this.application = "";
		this.creationTime = "";
	}

	public Interventions(Long id, String operator, String category, String application, String creationTime) {
		super();
		this.id = id;
		this.operator = operator;
		this.category = category;
		this.application = application;
		this.creationTime = creationTime;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("OPERATOR")
	private String operator;

	@JsonProperty("CATEGORY")
	private String category;

	@JsonProperty("APPLICATION")
	private String application;

	@JsonProperty("CREATIONTIME")
	private String creationTime;

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
