package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageHeader {

	public MessageHeader() {
		super();
		this.id = null;
		this.swiftInput = "";
		this.sender = null;
		this.receiver = null;
		this.swiftOutput = "";
	}

	public MessageHeader(Long id, String swiftInput, String swiftOutput, Sender sender, Receiver receiver) {
		super();
		this.id = id;
		this.swiftInput = swiftInput;
		this.sender = sender;
		this.receiver = receiver;
		this.swiftOutput = swiftOutput;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("SWIFT_INPUT")
	private String swiftInput;

	@JsonProperty("SWIFT_OUTPUT")
	private String swiftOutput;

	@JsonProperty("SENDER")
	private Sender sender;

	@JsonProperty("RECEIVER")
	private Receiver receiver;

	public String getSwiftInput() {
		return swiftInput;
	}

	public void setSwiftInput(String swiftInput) {
		this.swiftInput = swiftInput;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSwiftOutput() {
		return swiftOutput;
	}

	public void setSwiftOutput(String swiftOutput) {
		this.swiftOutput = swiftOutput;
	}

}
