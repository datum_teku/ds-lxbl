package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageText {

	public MessageText() {

	}

	public MessageText(Field20 field20, Field23B field23b, Field32A field32a, Field50K field50k, Field52A field52a,
			Field53A field53a, Field57A field57a, Field59 field59, Field70 field70, Field71A field71a) {
		super();
		this.field20 = field20;
		this.field23b = field23b;
		this.field32a = field32a;
		this.field50k = field50k;
		this.field52a = field52a;
		this.field53a = field53a;
		this.field57a = field57a;
		this.field59 = field59;
		this.field70 = field70;
		this.field71a = field71a;
	}

	@JsonProperty("20")
	private Field20 field20;

	@JsonProperty("23B")
	private Field23B field23b;

	@JsonProperty("32A")
	private Field32A field32a;

	@JsonProperty("50K")
	private Field50K field50k;

	@JsonProperty("52A")
	private Field52A field52a;

	@JsonProperty("53A")
	private Field53A field53a;

	@JsonProperty("57A")
	private Field57A field57a;

	@JsonProperty("59")
	private Field59 field59;

	@JsonProperty("70")
	private Field70 field70;

	@JsonProperty("71A")
	private Field71A field71a;

	public Field20 getField20() {
		return field20;
	}

	public void setField20(Field20 field20) {
		this.field20 = field20;
	}

	public Field23B getField23b() {
		return field23b;
	}

	public void setField23b(Field23B field23b) {
		this.field23b = field23b;
	}

	public Field32A getField32a() {
		return field32a;
	}

	public void setField32a(Field32A field32a) {
		this.field32a = field32a;
	}

	public Field50K getField50k() {
		return field50k;
	}

	public void setField50k(Field50K field50k) {
		this.field50k = field50k;
	}

	public Field52A getField52a() {
		return field52a;
	}

	public void setField52a(Field52A field52a) {
		this.field52a = field52a;
	}

	public Field53A getField53a() {
		return field53a;
	}

	public void setField53a(Field53A field53a) {
		this.field53a = field53a;
	}

	public Field57A getField57a() {
		return field57a;
	}

	public void setField57a(Field57A field57a) {
		this.field57a = field57a;
	}

	public Field59 getField59() {
		return field59;
	}

	public void setField59(Field59 field59) {
		this.field59 = field59;
	}

	public Field70 getField70() {
		return field70;
	}

	public void setField70(Field70 field70) {
		this.field70 = field70;
	}

	public Field71A getField71a() {
		return field71a;
	}

	public void setField71a(Field71A field71a) {
		this.field71a = field71a;
	}

}
