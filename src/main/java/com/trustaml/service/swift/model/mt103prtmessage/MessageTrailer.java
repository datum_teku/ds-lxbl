package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageTrailer {

	public MessageTrailer() {
		super();
		this.signature = "";
	}

	public MessageTrailer(String signature) {
		super();
		this.signature = signature;
	}

	@JsonProperty("PKISIGNATURE")
	private String signature;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
