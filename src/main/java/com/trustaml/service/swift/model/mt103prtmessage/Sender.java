package com.trustaml.service.swift.model.mt103prtmessage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sender {

	public Sender() {
		super();
		this.id = null;
		this.swiftCode = "";
		this.bankName = "";
		this.city = "";
		this.code = "";
		this.branchName = "";
	}

	public Sender(Long id, String swiftCode, String bankName, String city, String code, String branchName) {
		super();
		this.id = id;
		this.swiftCode = swiftCode;
		this.bankName = bankName;
		this.city = city;
		this.code = code;
		this.branchName = branchName;
	}

	@JsonProperty("ID")
	private Long id;

	@JsonProperty("SWIFT_CODE")
	private String swiftCode;

	@JsonProperty("BANK_NAME")
	private String bankName;

	@JsonProperty("CITY")
	private String city;

	@JsonProperty("CODE")
	private String code;

	@JsonProperty("BRANCH_NAME")
	private String branchName;

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
