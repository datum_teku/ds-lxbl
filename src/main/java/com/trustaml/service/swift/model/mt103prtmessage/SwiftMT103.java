package com.trustaml.service.swift.model.mt103prtmessage;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.swift.model.mt103prtfile.FileMeta;

public class SwiftMT103 {

	public SwiftMT103() {

	}

	public SwiftMT103(FileMeta meta, List<MessageHeader> messageHeader, List<InstanceTypeAndTransmission> transmission,
			List<MessageText> messageText, List<MessageTrailer> messageTrailer, List<Interventions> interventions) {
		super();
		this.meta = meta;
		this.messageHeader = messageHeader;
		this.transmission = transmission;
		this.messageText = messageText;
		this.messageTrailer = messageTrailer;
		this.interventions = interventions;
	}

	@JsonProperty("META")
	private FileMeta meta;
	@JsonProperty("MESSAGE_HEADER")
	private List<MessageHeader> messageHeader;

	@JsonProperty("INSTANCE_TYPE_AND_TRANSMISSION")
	private List<InstanceTypeAndTransmission> transmission;

	@JsonProperty("MESSAGE_TEXT")
	private List<MessageText> messageText;

	@JsonProperty("MESSAGE_TRAILER")
	private List<MessageTrailer> messageTrailer;

	@JsonProperty("INTERVENTIONS")
	private List<Interventions> interventions;

	public FileMeta getMeta() {
		return meta;
	}

	public void setMeta(FileMeta meta) {
		this.meta = meta;
	}

	public List<MessageHeader> getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(List<MessageHeader> messageHeader) {
		this.messageHeader = messageHeader;
	}

	public List<InstanceTypeAndTransmission> getTransmission() {
		return transmission;
	}

	public void setTransmission(List<InstanceTypeAndTransmission> transmission) {
		this.transmission = transmission;
	}

	public List<MessageText> getMessageText() {
		return messageText;
	}

	public void setMessageText(List<MessageText> messageText) {
		this.messageText = messageText;
	}

	public List<MessageTrailer> getMessageTrailer() {
		return messageTrailer;
	}

	public void setMessageTrailer(List<MessageTrailer> messageTrailer) {
		this.messageTrailer = messageTrailer;
	}

	public List<Interventions> getInterventions() {
		return interventions;
	}

	public void setInterventions(List<Interventions> interventions) {
		this.interventions = interventions;
	}

}
