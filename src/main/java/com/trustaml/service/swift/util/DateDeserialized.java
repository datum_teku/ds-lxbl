package com.trustaml.service.swift.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class DateDeserialized extends JsonDeserializer<Date> {
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

	@Override
	public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String str = p.getText();
		try {
			return new java.sql.Date(dateFormat.parse(str).getTime());
		} catch (ParseException e) {

		}
		return new java.sql.Date(ctxt.parseDate(str).getTime());
	}
}
