package com.trustaml.service.swift.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.base.CharMatcher;
import com.trustaml.service.swift.model.mt103prtfile.FileMeta;
import com.trustaml.service.swift.model.mt103prtfile.MessageHeader;
import com.trustaml.service.swift.model.mt103prtfile.PrtFileContent;

public class Mt103PrtFileReader {

	public static PrtFileContent fileReader(byte[] mt103PrtFileContent) throws IOException {
		PrtFileContent prtFileContent = new PrtFileContent();
		// Setting file meta info
		InputStream is = new ByteArrayInputStream(mt103PrtFileContent);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		FileMeta fileMeta = new FileMeta();
		String fileContentString = "";

		String line = "";
		List<String> data = new ArrayList<>();
		int i = 0;

		while ((line = br.readLine()) != null) {
			data.add(line);
			if (line.startsWith("_")) {
				String meta = data.get(i - 1);
				String[] metaInfo = meta.split("\\s+");
				List<String> listMeta = Arrays.asList(metaInfo);
				String time = listMeta.get(0);
				String[] times = time.split("-");
				List<String> listTimes = Arrays.asList(times);

				fileMeta.setDescription(listMeta.get(1));
				fileMeta.setOthers(listMeta.get(2));

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

				java.util.Date parsed = null;
				java.util.Date parsedTime = null;
				try {
					parsed = dateFormat.parse(listTimes.get(0));
					parsedTime = timeFormat.parse(listTimes.get(1));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
				java.sql.Time sqlTime = new java.sql.Time(parsedTime.getTime());

				fileMeta.setDate(sqlDate);
				fileMeta.setTime(sqlTime);

				// meta info of the file
				prtFileContent.setMeta(fileMeta);
				// end reading file meta info

			} else if (data.size() > 2) {
				// reading file content
				fileContentString = fileContentString + line + "\n";
			}
			i++;
		}
		// finished reading file meta and fileContentString.

		br.close();
		is.close();

		// Reading file contents after meta and putting into a key-value map
		Map<String, List<String>> content = new LinkedHashMap<String, List<String>>();

		Pattern p = Pattern.compile("-{10,30}"); // the pattern to search for

		Matcher m = p.matcher(fileContentString);

		if (m.find()) {

			String[] tokens = fileContentString.split("-{10,30}");

			List<String> contents = Arrays.asList(tokens);

			for (int index = 1; index < contents.size() - 1; index += 2) {

				if (content.containsKey(contents.get(index).trim())) {
					content.get(contents.get(index).trim()).add(contents.get(index + 1));
				} else {
					List<String> value = new ArrayList<>();
					value.add(contents.get(index + 1));
					content.put(contents.get(index).trim(), value);
				}
			}
		}
		// finished reading file contents

		// reading header section from file contents
		List<String> headerData = content.get("Message Header");
		List<Map<String, String>> headerDataMap = returnOnlyKeyValueSection(headerData);

		List<MessageHeader> headersList = readMessageHeaderSection(headerDataMap);
		if (headersList != null) {
			prtFileContent.setMessageHeader(headersList);
		}

		// reading instance section from file contents
		List<String> instanceDataMap = content.get("Instance Type and Transmission");
		List<Map<String, String>> instance = readInstanceSection(instanceDataMap);
		if (instance != null) {
			prtFileContent.setInstance(instance);
		}

		List<String> interventionsData = content.get("Interventions");
		List<Map<String, String>> interventions = returnOnlyKeyValueSection(interventionsData);
		if (interventions != null) {
			prtFileContent.setInterventions(interventions);
		}

		List<String> trailerData = content.get("Message Trailer");
		List<Map<String, String>> trailer = readMessageTrailer(trailerData);
		if (trailer != null) {
			prtFileContent.setTrailer(trailer);
		}

		List<String> messageData = content.get("Message Text");
		List<Map<String, Map<String, String>>> messageText = readMessageText(messageData);
		if (messageText != null)
			prtFileContent.setMessageText(messageText);
		return prtFileContent;
	}

	public static List<MessageHeader> readMessageHeaderSection(List<Map<String, String>> headerDataMap)
			throws IOException {
		List<MessageHeader> headers = new ArrayList<>();

		for (Map<String, String> map : headerDataMap) {
			String swiftInput = map.get("SWIFTINPUT");

			String sender = map.get("SENDER").trim();

			MessageHeader header = new MessageHeader();

			Map<String, String> senderMap = new LinkedHashMap<>();

			Map<String, String> receiverMap = new LinkedHashMap<>();

			String[] senders = sender.split("\n");

			if (senders.length > 3) {
				senderMap.put("SWIFT_CODE", Arrays.asList(senders).get(0));
				senderMap.put("BANK_NAME", Arrays.asList(senders).get(1).trim());
				senderMap.put("BRANCH_NAME", Arrays.asList(senders).get(2).replace("(", "").replace(")", "").trim());
				String address = Arrays.asList(senders).get(3);

				String nationCode = address.substring(address.lastIndexOf(" ") + 1);
				String city = address.replace(nationCode, "").trim();
				senderMap.put("CITY", city);
				senderMap.put("CODE", nationCode);
			} else if (senders.length == 3) {
				senderMap.put("SWIFT_CODE", Arrays.asList(senders).get(0));
				senderMap.put("BANK_NAME", Arrays.asList(senders).get(1));

				String address = Arrays.asList(senders).get(2);

				String nationCode = address.substring(address.lastIndexOf(" ") + 1);
				String city = address.replace(nationCode, "").trim();
				senderMap.put("CITY", city);
				senderMap.put("CODE", nationCode);
			}

			String receiver = map.get("RECEIVER");

			String[] receivers = receiver.split("\n");

			if (receivers.length > 3) {
				receiverMap.put("SWIFT_CODE", Arrays.asList(receivers).get(0).trim());
				receiverMap.put("BANK_NAME", Arrays.asList(receivers).get(1).trim());
				receiverMap.put("BRANCH_NAME",
						Arrays.asList(receivers).get(2).replace("(", "").replace(")", "").trim());
				String address = Arrays.asList(receivers).get(3).trim();

				String nationCode = address.substring(address.lastIndexOf(" ") + 1);
				String city = address.replace(nationCode, "").trim();
				receiverMap.put("CITY", city);
				receiverMap.put("CODE", nationCode);
			} else if (receivers.length == 3) {
				receiverMap.put("SWIFT_CODE", Arrays.asList(receivers).get(0).trim());
				receiverMap.put("BANK_NAME", Arrays.asList(receivers).get(1).trim());

				String address = Arrays.asList(receivers).get(2).trim();

				String nationCode = address.substring(address.lastIndexOf(" ") + 1);
				String city = address.replace(nationCode, "").trim();
				receiverMap.put("CITY", city);
				receiverMap.put("CODE", nationCode);
			}

			header.setSwiftInput(swiftInput);

			header.setSender(senderMap);

			header.setReceiver(receiverMap);

			headers.add(header);
		}

		return headers;

	}

	public static List<Map<String, String>> readInstanceSection(List<String> instanceDataMap) throws IOException {
		List<Map<String, String>> instanceMap = new ArrayList<Map<String, String>>();
		Pattern p = Pattern.compile("\\w*(\\s|\\/)\\w*\\s\\w*\\s+:");

		for (String data : instanceDataMap) {
			List<String> list = new ArrayList<>();
			Map<String, String> map = new HashMap<>();

			Matcher m = p.matcher(data);

			while (m.find()) {
				list.add(m.group());
			}

			int i = 0;
			for (i = 0; i < list.size() - 1; i++) {
				map.put(list.get(i).trim().replaceAll(" +", "").replace(":", ""),
						return32A(list.get(i), list.get(i + 1), data));
			}
			if (i == list.size() - 1) {
				map.put(list.get(i).trim().replaceAll(" +", "").replace(":", ""), returnLast(list.get(i), data));

			}

			map.put("DESCRIPTION", returnFirstValue(list.get(0), data));

			String reference = map.get("MessageInputReference");
			map.remove("MessageInputReference");
			map.put("MESSAGE_INPUT_REFERENCE", reference);

			String status = map.get("NetworkDeliveryStatus");

			map.remove("NetworkDeliveryStatus");
			map.put("NETWORK_DELIVERY_STATUS", status);

			String priority = map.get("Priority/Delivery");
			map.remove("Priority/Delivery");
			map.put("PRIORITY_DELIVERY", priority);

			instanceMap.add(map);
		}
		return instanceMap;
	}

	public static List<Map<String, String>> readMessageTrailer(List<String> trailerData) throws IOException {

		List<Map<String, String>> listTrailer = new ArrayList<>();

		Pattern p = Pattern.compile("\\w*\\s\\w*:");

		for (String data : trailerData) {
			Map<String, String> map = new HashMap<>();
			List<String> list = new ArrayList<>();

			Matcher m = p.matcher(data);

			while (m.find()) {
				list.add(m.group());
			}

			int i = 0;
			for (i = 0; i < list.size(); i++) {
				map.put(list.get(i).replaceAll(" +", "").replaceAll(":", "").toUpperCase(),
						returnLast(list.get(i), data));

			}
			listTrailer.add(map);
		}

		return listTrailer;

	}

	public static List<Map<String, Map<String, String>>> readMessageText(List<String> messageData) throws IOException {

		List<Map<String, Map<String, String>>> listField = new ArrayList<>();

		Pattern p = Pattern.compile("[0-9]+[A-Z]*:");

		for (String data : messageData) {

			Map<String, Map<String, String>> field = new LinkedHashMap<String, Map<String, String>>();

			List<String> list = new ArrayList<>();

			Matcher m = p.matcher(data);

			while (m.find()) {

				list.add(m.group(0));
			}
			int i = 0;
			for (i = 0; i < list.size() - 1; i++) {

				field.put(list.get(i).replace(":", ""), returnFieldValues(list.get(i), list.get(i + 1), data));

			}

			if (i == list.size() - 1) {
				field.put(list.get(i).replace(":", ""), returnLastFieldValues(list.get(i), data));

			}

			if (field.containsKey("32A")) {
				fieldValueNested(field, "32A");
			}

			if (field.containsKey("50K")) {
				fieldValue50KNested(field, "50K");
			}

			if (field.containsKey("52A")) {
				get52AField(field);
			}

			if (field.containsKey("53A")) {
				get53AField(field);
			}
			if (field.containsKey("57A")) {
				get57AField(field);
			}
			if (field.containsKey("59")) {
				get59Field(field);
			}

			listField.add(field);
		}

		return listField;

	}

	public static Map<String, String> returnLastFieldValues(String lastField, String data) {

		Map<String, String> fields = new HashMap<>();

		Matcher matcher = Pattern.compile(lastField).matcher(data);

		String match = "";

		if (matcher.find()) {
			match = data.substring(matcher.end()).trim();
		}

		String[] fieldString = match.split("\\s{2,}");

		List<String> fieldStringList = Arrays.asList(fieldString);

		String value = "";

		fields.put("DESCRIPTION", fieldStringList.get(0).replaceAll(":", "").trim());

		for (int index = 1; index < fieldString.length; index++) {
			value = value + " " + fieldStringList.get(index);
		}
		fields.put("VALUE", value.trim());

		return fields;

	}

	public static Map<String, String> returnFieldValues(String firstField, String secoundField, String data) {

		Map<String, String> fields = new HashMap<>();

		Matcher m = Pattern.compile(Pattern.quote(firstField) + "([\\S\\s]*?)" + Pattern.quote(secoundField))
				.matcher(data);

		String match = "";

		if (m.find()) {
			match = m.group(1);
		}

		String[] fieldString = match.split("\n");

		List<String> fieldStringList = Arrays.asList(fieldString);

		String value = "";

		fields.put("DESCRIPTION", fieldStringList.get(0).replaceAll(":", "").trim());

		for (int index = 1; index < fieldString.length; index++) {
			value = value + fieldStringList.get(index) + "\n";
		}
		fields.put("VALUE", value.trim());

		return fields;
	}

	public static void fieldValueNested(Map<String, Map<String, String>> field, String key) {

		String value = "";

		String description = field.get(key).get("DESCRIPTION");

		List<String> list32a = new ArrayList<>();
		Map<String, String> map32a = new LinkedHashMap<>();

		if (field.containsKey(key)) {

			value = field.get(key).get("VALUE");

			Pattern p = Pattern.compile("(?:[A-Z][a-z]*\\s+:)");

			Matcher m = p.matcher(value);

			while (m.find()) {
				list32a.add(m.group(0).replace("\\s", "").trim());
			}

			map32a.put("DESCRIPTION", description);

			String data = field.get("32A").get("VALUE");

			add32A(map32a, list32a, data);

			field.remove(key);

			field.put(key, map32a);

		}

	}

	public static void fieldValue50KNested(Map<String, Map<String, String>> field, String key) {

		String value = "";

		String description = field.get(key).get("DESCRIPTION");

		List<String> list50k = new ArrayList<>();
		Map<String, String> map50k = new LinkedHashMap<>();

		if (field.containsKey(key)) {

			value = field.get(key).get("VALUE");

			Pattern p = Pattern.compile("(?:[A-Z ]*(:|\\.))");

			Matcher m = p.matcher(value);

			while (m.find()) {
				list50k.add(m.group(0).replace("\\s", "").trim());
			}

			add5oK(map50k, list50k, value);

			map50k.put("DESCRIPTION", description);

			String[] values = value.split("\n");

			map50k.put("ACCOUNT", Arrays.asList(values).get(0).replace("/", "").trim());

			String name = Arrays.asList(values).get(1).replace("", "").trim();
			String address = Arrays.asList(values).get(2).replace("", "").trim();
			map50k.put("ADDRESS", address);

			int index = name.indexOf(" ");

			String firstName = name.substring(0, index);

			String lastName = name.substring(name.lastIndexOf(" ") + 1);

			map50k.put("FIRST_NAME", firstName);

			String middleName = textBetweenWords(name, firstName, lastName);

			if (middleName.isEmpty()) {
				map50k.put("MIDDLE_NAME", "");
				map50k.put("LAST_NAME", lastName);

			} else {
				map50k.put("MIDDLE_NAME", middleName);
				map50k.put("LAST_NAME", lastName);
			}

			field.remove(key);

			field.put(key, map50k);

		}

	}

	public static void add32A(Map<String, String> map32a, List<String> list32a, String data) {

		int i = 0;
		for (i = 0; i < list32a.size() - 1; i++) {

			map32a.put(list32a.get(i).trim().replace(":", "").replaceAll(" ", "").toUpperCase(),
					return32A(list32a.get(i), list32a.get(i + 1), data).replaceAll("\\(.*?\\) ?", "").trim());

		}

		if (i == list32a.size() - 1) {

			map32a.put(list32a.get(i).trim().replace(":", "").replaceAll(" ", "").toUpperCase(),
					returnLast32A(list32a.get(i), data));

		}
	}

	public static void add5oK(Map<String, String> map50k, List<String> list50k, String data) {

		int i = 0;

		for (i = 0; i < list50k.size() - 1; i++) {

			map50k.put(list50k.get(i).replace(":", "").replaceAll(" +", "").replaceAll("\\.", ""),
					return32A(list50k.get(i), list50k.get(i + 1), data));

		}

		if (i == list50k.size() - 1) {

			map50k.put(list50k.get(i).replace(":", "").replaceAll(" +", "").replaceAll("\\.", ""),
					returnLast(list50k.get(i), data));

		}

	}

	public static String return32A(String firstField, String secoundField, String data) {

		Matcher m = Pattern.compile(Pattern.quote(firstField) + "([\\S\\s]*?)" + Pattern.quote(secoundField))
				.matcher(data);

		String match = "";

		if (m.find()) {
			match = m.group(1);
		}

		String fieldString = match.replaceAll(" +", " ").replace(":", "").trim();

		return fieldString;

	}

	public static String returnLast32A(String lastField, String data) {

		Matcher matcher = Pattern.compile(lastField).matcher(data);

		String match = "";

		if (matcher.find()) {
			match = data.substring(matcher.end()).trim();
		}

		String fieldString = match.replaceAll(" +", " ").replace(":", "").replace("#", "").trim();

		return fieldString;
	}

	public static String returnLast(String lastField, String data) {

		Matcher matcher = Pattern.compile(lastField).matcher(data);

		String match = "";

		if (matcher.find()) {
			match = data.substring(matcher.end()).trim();
		}

		String fieldString = match.replaceAll(" +", " ").replace(":", "").trim();

		return fieldString;
	}

	public static String returnFirstValue(String firstField, String data) {

		Matcher matcher = Pattern.compile(firstField).matcher(data);

		String match = "";

		if (matcher.find()) {
			match = data.substring(0, matcher.start()).trim();
		}

		String fieldString = match.replace(":", "").trim();

		return fieldString;
	}

	public static void get59Field(Map<String, Map<String, String>> field) {
		Map<String, String> map = new LinkedHashMap<>();

		Map<String, String> field59 = field.get("59");
		String value = field59.get("VALUE");
		String description = field59.get("DESCRIPTION");
		field.remove("59");

		map.put("DESCRIPTION", description);

		String account = CharMatcher.DIGIT.retainFrom(value);

		map.put("ACCOUNT", account);

		String name = value.substring(account.length() + 1, value.length()).trim();

		map.put("NAME", name);

		field.put("59", map);

	}

	public static void get53AField(Map<String, Map<String, String>> field) {
		Map<String, String> map = new LinkedHashMap<>();

		Map<String, String> field53 = field.get("53A");

		String value = field53.get("VALUE");

		String description = field53.get("DESCRIPTION");

		field.remove("53A");

		map.put("DESCRIPTION", description);

		String[] values = value.split("\n");

		String account_number = Arrays.asList(values).get(0).replace("/", "").trim();
		String swift_code = Arrays.asList(values).get(1).trim();
		String bank_name = Arrays.asList(values).get(2).trim();
		String address = Arrays.asList(values).get(3).trim();
		String code = address.substring(address.lastIndexOf(" ") + 1);

		map.put("ACCOUNT", account_number);
		map.put("SWIFT_CODE", swift_code);
		map.put("BANK_NAME", bank_name);
		map.put("ADDRESS", address.replace(code, "").trim());
		map.put("CODE", code);

		field.put("53A", map);

	}

	public static void get52AField(Map<String, Map<String, String>> field) {
		Map<String, String> map = new LinkedHashMap<>();

		Map<String, String> field53 = field.get("52A");

		String value = field53.get("VALUE");

		String description = field53.get("DESCRIPTION");

		map.put("DESCRIPTION", description);

		String[] values = value.split("\n");

		String swift_code = Arrays.asList(values).get(0).trim();
		String bank_name = Arrays.asList(values).get(1).trim();
		String address = Arrays.asList(values).get(2).trim();
		String code = address.substring(address.lastIndexOf(" ") + 1);

		map.put("SWIFT_CODE", swift_code);
		map.put("BANK_NAME", bank_name);
		map.put("ADDRESS", address.replace(code, "").trim());
		map.put("CODE", code);

		field.remove("52A");

		field.put("52A", map);

	}

	public static void get57AField(Map<String, Map<String, String>> field) {
		Map<String, String> map = new LinkedHashMap<>();

		Map<String, String> field57 = field.get("57A");

		String value = field57.get("VALUE");

		String description = field57.get("DESCRIPTION");

		String[] values = value.split("\n");

		field.remove("57A");

		if (values.length == 3) {
			map.put("DESCRIPTION", description);
			map.put("SWIFT_CODE", Arrays.asList(values).get(0));
			map.put("BANK_NAME", Arrays.asList(values).get(1).trim());
			String address = Arrays.asList(values).get(2);
			String nationCode = address.substring(address.lastIndexOf(" ") + 1);
			String city = address.replace(nationCode, "").trim();
			map.put("ADDRESS", city);
			map.put("CODE", nationCode);
		} else if (values.length != 3) {
			map.put("DESCRIPTION", description);
			map.put("SWIFT_CODE", Arrays.asList(values).get(0));
			map.put("BANK_NAME", Arrays.asList(values).get(1).trim());
			map.put("BRANCH_NAME", Arrays.asList(values).get(2).replace("(", "").replace(")", "").trim());
			String address = Arrays.asList(values).get(3);
			String nationCode = address.substring(address.lastIndexOf(" ") + 1);
			String city = address.replace(nationCode, "").trim();
			map.put("ADDRESS", city);
			map.put("CODE", nationCode);
		}
		field.put("57A", map);
	}

	public static List<Map<String, String>> returnOnlyKeyValueSection(List<String> sectionContent) throws IOException {

		List<String> list = new ArrayList<>();

		List<Map<String, String>> listMap = new ArrayList<>();

		Pattern p = Pattern.compile("\\w*(\\s|\\/)\\w*\\s\\w*\\s+:");

		for (String data : sectionContent) {

			Map<String, String> map = new HashMap<>();

			Matcher m = p.matcher(data);

			while (m.find()) {
				list.add(m.group());
			}

			int i = 0;
			for (i = 0; i < list.size() - 1; i++) {
				map.put(list.get(i).trim().replaceAll(" +", "").replace(":", "").toUpperCase(),
						return32A(list.get(i), list.get(i + 1), data));
			}
			if (i == list.size() - 1) {
				map.put(list.get(i).trim().replaceAll(" +", "").replace(":", "").toUpperCase(),
						returnLast(list.get(i), data));

			}

			listMap.add(map);
		}

		return listMap;
	}

	public static String textBetweenWords(String sentence, String firstWord, String secondWord) {

		Matcher m = Pattern.compile(Pattern.quote(firstWord) + "(.*?)" + Pattern.quote(secondWord)).matcher(sentence);

		String match = "";

		if (m.find()) {
			match = m.group(1).trim();
		}
		return match;
	}

}
