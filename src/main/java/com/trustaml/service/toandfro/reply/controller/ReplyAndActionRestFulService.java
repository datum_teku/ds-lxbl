package com.trustaml.service.toandfro.reply.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.toandfro.reply.dao.MakerCheckerDao;
import com.trustaml.service.toandfro.reply.dao.ReplyAndUpdateDao;

@Path("/to-fro")
public class ReplyAndActionRestFulService {

	@Inject
	ReplyAndUpdateDao replyAndUpdateDao;

	@Inject
	MakerCheckerDao makerCheckerDao;

	/**
	 * @param jsonString
	 * @return save success
	 * @description accepts JSON String and save reply for KYC
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Path("/kyc-reply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveKYCReply(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.saveReplyForKYC(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save reply and action of screening legal from checker to
	 *              maker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Path("/checker-maker/screening-l-reply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReplyAndActionForLegal(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.saveReplyAndAction(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save comment for screening legal reply from checker to maker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Path("/checker-maker/screening-l-reply-comment")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReplyCommentForScreeningLegal(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.saveReplyCommentForScreeningLegal(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save reply of screening natural from checker to maker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Path("/checker-maker/screening-n-reply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReplyAndActionForScreeningNatural(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.saveReplyAndActionForScreeningNatural(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save comment of screening natural reply from checker to
	 *              maker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 */
	@POST
	@Path("/checker-maker/screening-n-reply-comment")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveReplyCommentForScreeningNatural(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.saveReplyCommentForScreeningNatural(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save screening natural response from maker to checker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	@POST
	@Path("/maker-checker/screening-n-response")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response makerToCheckerScreeningNaturalResponse(String jsonString) throws JsonParseException,
			JsonMappingException, IOException, SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			makerCheckerDao.makerToCheckerScreeningNaturalResponse(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	
	@POST
	@Path("/maker-checker/screening-n-others-response")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response makerToCheckerScreeningNaturalOthersResponse(String jsonString) throws Exception {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			long kycnId = makerCheckerDao.makerToCheckerScreeningNaturalOthersResponse(jsonString);
			return ResponseReturn.sucess(String.valueOf(kycnId));
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save screening legal reponse from maker to checker
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	@POST
	@Path("/maker-checker/screening-l-response")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response makerToChecker(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			makerCheckerDao.makerToChecker(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	// @POST
	// @Path("/checker-cap")
	// @Consumes(MediaType.APPLICATION_JSON)
	// public Response makerToCapAndViceVersa(String jsonString)
	// throws JsonParseException, JsonMappingException, IOException,
	// SQLException, TrustAmlEmptyJSONException {
	// if (!jsonString.isEmpty() || !jsonString.equals("")) {
	// makerCheckerDao.makerToCapAndViceVersa(jsonString);
	// return ResponseReturn.sucess("save success");
	// } else {
	// throw new TrustAmlEmptyJSONException("json is empty");
	// }
	// }
	/**
	 * @param jsonString
	 * @return save success
	 * @description save legal reply of account
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	@POST
	@Path("/account-l-reply")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response accountLegalReply(String jsonString) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.accountLegalReply(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}

	/**
	 * @param jsonString
	 * @return save success
	 * @description save account legal reponse
	 * @throws TrustAmlEmptyJSONException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */
	@POST
	@Path("/account-l-response")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response accountLegalResponse(String jsonString) throws TrustAmlEmptyJSONException, JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			replyAndUpdateDao.accountLegalResponse(jsonString);
			return ResponseReturn.sucess("save success");
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}
}
