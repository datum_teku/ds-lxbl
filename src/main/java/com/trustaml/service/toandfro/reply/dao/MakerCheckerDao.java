package com.trustaml.service.toandfro.reply.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.kyc.kycl.dao.KyclDaoImpl;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycl.model.RelatedEntity;
import com.trustaml.service.kyc.kycn.dao.KYCNDaoImpl;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedPerson;
import com.trustaml.service.screening.natural.dao.ScreeningNaturalDaoImpl;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalResponse;
import com.trustaml.service.toandfro.reply.model.ScreeningNaturalResponse;

@Stateless
public class MakerCheckerDao {

	@Inject
	MakerCheckerDaoImpl makerCheckerDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	@Inject
	ScreeningNaturalDaoImpl screeningNaturalDaoImpl;

	@Inject
	KYCNDaoImpl kycnDaoImpl;

	@Inject
	KyclDaoImpl kyclDaoImpl;

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */

	public void makerToChecker(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, ParseException, NoSuchAlgorithmException {
		ScreeningLegalResponse screeningLegalResponse = new ObjectMapper().readValue(jsonString,
				ScreeningLegalResponse.class);
		String relatedPerson = screeningNaturalDaoImpl
				.callProcedureRelatedPersonLDetails(screeningLegalResponse.getScreeningLegalId());
		String relatedEntity = screeningNaturalDaoImpl
				.callProcedureRelatedEntityLDetails(screeningLegalResponse.getScreeningLegalId());
		if (relatedPerson != null) {
			RequestRelatedPerson[] arrayRequestRelatedPersonData = new ObjectMapper().readValue(relatedPerson,
					RequestRelatedPerson[].class);
			List<RequestRelatedPerson> listRequestRelatedPersonData = Arrays.asList(arrayRequestRelatedPersonData);
			for (RequestRelatedPerson requestRelatedPerson : listRequestRelatedPersonData) {
				if (!requestRelatedPerson.isHaskycl()) {
					KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
					KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
					KycnAddressInfo kycnAddressInfo = new KycnAddressInfo();
					kycnPersonalInfo.setSalutation(requestRelatedPerson.getSalutation());
					kycnPersonalInfo.setFirstName(requestRelatedPerson.getFirstName());
					kycnPersonalInfo.setMiddleName(requestRelatedPerson.getMiddleName());
					kycnPersonalInfo.setLastName(requestRelatedPerson.getLastName());
					kycnPersonalInfo.setLsfName(requestRelatedPerson.getLsfName());
					kycnPersonalInfo.setLslName(requestRelatedPerson.getLslName());
					kycnPersonalInfo.setLsmName(requestRelatedPerson.getLsmName());
					kycnPersonalInfo.setDateOfBirth(requestRelatedPerson.getDateOfBirth());
					kycnPersonalInfo.setPanNumber(requestRelatedPerson.getPanNumber());
					kycnPersonalInfo.setNotes(requestRelatedPerson.getNotes());
					kycnPersonalInfo.setScreeningId(requestRelatedPerson.getScreeningNId());
					kycnIdentificationInfo.setPrimaryIdentificationDocumentNo(
							requestRelatedPerson.getPrimaryIdentificationDocumentNo());
					kycnIdentificationInfo.setPrimaryIdentificationDocumentType(
							requestRelatedPerson.getPrimaryIdentificationDocumentType());
					kycnIdentificationInfo.setCountryOfIssue(requestRelatedPerson.getCountryOfIssue());

					kycnAddressInfo.setZone(requestRelatedPerson.getZone());
					kycnAddressInfo.setDistrict(requestRelatedPerson.getDistrict());
					kycnAddressInfo.setMnVdc(requestRelatedPerson.getMnVdc());
					kycnAddressInfo.setEmailId(requestRelatedPerson.getEmailId());
					kycnAddressInfo.setPhoneNo(requestRelatedPerson.getMobileNumber());
					kycnAddressInfo.setWardNumber(requestRelatedPerson.getWardNo());
					boolean includeUser = true;
					boolean doNotIncludeUser = false;
					String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
							kycnPersonalInfo, screeningLegalResponse.getUser(), includeUser);
					long kycnPersonalId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo,
							screeningLegalResponse.getUser(), hashValueForKycnPersonInfo);
					kycnPersonalInfo.setId(kycnPersonalId);
//					String hashValueForKycnPersonInfoUpdate = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
//							kycnPersonalInfo, screeningLegalResponse.getUser(), includeUser);
					String hashValueForKycnIdentificationInfo = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
									screeningLegalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
					String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
									screeningLegalResponse.getUser(), includeUser, kycnPersonalId);

					kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalId,
							hashValueForKycnIdentificationInfo, screeningLegalResponse.getUser());
					kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalId,
							hashValueForKycnIdentificationInfoUpdate, screeningLegalResponse.getUser());

					String hashValueForKycnAddressInfo = hashCodeGenerator.generateHashValueForKycnAddressInfo(
							kycnAddressInfo, screeningLegalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
					String hashValueForKycnAddressInfoUpdate = hashCodeGenerator.generateHashValueForKycnAddressInfo(
							kycnAddressInfo, screeningLegalResponse.getUser(), includeUser, kycnPersonalId);
					kycnDaoImpl.insertKycnAddressInfo(kycnAddressInfo, kycnPersonalId, hashValueForKycnAddressInfo,screeningLegalResponse.getUser());
					kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
							kycnPersonalId, screeningLegalResponse.getUser(), hashValueForKycnAddressInfoUpdate);
				}
			}
		}
		if (relatedEntity != null) {
			boolean includeUser = true;
			boolean doNotIncludeUser = false;
			// AmlLogger.printData("related Entity is not null");
			RequestRelatedEntityRequestData[] arrayRequestRelatedEntityData = new ObjectMapper()
					.readValue(relatedEntity, RequestRelatedEntityRequestData[].class);
			List<RequestRelatedEntityRequestData> RequestRelatedEntityRequestData = Arrays
					.asList(arrayRequestRelatedEntityData);
			for (RequestRelatedEntityRequestData requestRelatedEntityRequestData : RequestRelatedEntityRequestData) {

				if (!requestRelatedEntityRequestData.isHaskyc()) {
					Legal legal = new Legal();
					RegistrationInfo registrationInfo = new RegistrationInfo();
					RelatedEntity relatedEntityInfo = new RelatedEntity();
					Address address = new Address();
					legal.setNameOfTheInstitution(requestRelatedEntityRequestData.getNameOfInstitution());
					legal.setLsName(requestRelatedEntityRequestData.getLsName());
					legal.setDateOfEstablishment(requestRelatedEntityRequestData.getDateOfEstablishment());
					legal.setTypeOfIndustry(requestRelatedEntityRequestData.getTypeOfIndustry());
					// legal.setCustId(requestRelatedEntityRequestData.getCustId());
					legal.setPanNumber(requestRelatedEntityRequestData.getPanNumber());
					legal.setNotes(requestRelatedEntityRequestData.getNotes());
					legal.setScreeningId(requestRelatedEntityRequestData.getScreeningLId());
					registrationInfo.setRegdNumber(requestRelatedEntityRequestData.getRegistrationNo());
					legal.setPrimarySolId(requestRelatedEntityRequestData.getBranchSolId());
					address.setZone(requestRelatedEntityRequestData.getZone());
					address.setDistrict(requestRelatedEntityRequestData.getDistrict());
					address.setMnVdc(requestRelatedEntityRequestData.getMnVdc());
					address.setWardNumber(requestRelatedEntityRequestData.getWardNo());
					address.setEmailId(requestRelatedEntityRequestData.getEmailId());
					address.setPhoneNo(requestRelatedEntityRequestData.getContactNumber());
					relatedEntityInfo.setEntityType(requestRelatedEntityRequestData.getAccountsLSubType());
					String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal, doNotIncludeUser,
							screeningLegalResponse.getUser());
					String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal, includeUser,
							screeningLegalResponse.getUser());
					long kyclId = kyclDaoImpl.saveLegalInfo(legal,screeningLegalResponse.getUser(), kyclHashCode);
					kyclDaoImpl.saveLegalInfoInUpdateTable(legal, screeningLegalResponse.getUser(), kyclUpdateHashCode);
					String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							doNotIncludeUser, screeningLegalResponse.getUser());

					String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							includeUser, screeningLegalResponse.getUser());
					kyclDaoImpl.saveAddress(address, kyclId, addressHashCode,screeningLegalResponse.getUser());
					kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, screeningLegalResponse.getUser(),
							addressUpdateHashCode);
					String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
							registrationInfo, kyclId, doNotIncludeUser, screeningLegalResponse.getUser());

					String registrationInfoUpdateHashCode = hashCodeGenerator
							.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId, includeUser,
									screeningLegalResponse.getUser());
					kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
					kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId,
							screeningLegalResponse.getUser(), registrationInfoUpdateHashCode);
					String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntityInfo, kyclId, doNotIncludeUser, screeningLegalResponse.getUser());
					String relatedEntityUpdateHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntityInfo, kyclId, includeUser, screeningLegalResponse.getUser());

					kyclDaoImpl.saveRelatedEntityInfo(relatedEntityInfo, kyclId, relatedEntityHashCode);
					kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntityInfo, kyclId,
							screeningLegalResponse.getUser(), relatedEntityUpdateHashCode);
				}

			}
		}
		makerCheckerDaoImpl.updateScreeningLegalRelatedRequest(screeningLegalResponse);
		makerCheckerDaoImpl.disablePreviouslyLinkedLegalCustomerRequest(screeningLegalResponse);
		makerCheckerDaoImpl.updateScreeningLegalRequest(screeningLegalResponse);
		makerCheckerDaoImpl.updateScreeningLegalWorkflow(screeningLegalResponse);
		/*
		 * String status = ""; MakerChecker makerChecker = new
		 * ObjectMapper().readValue(jsonString, MakerChecker.class); for
		 * (AttachmentInfo attachmentInfo :
		 * makerChecker.getListAttachmentInfo()) {
		 * makerCheckerDaoImpl.saveAttachment(attachmentInfo,
		 * makerChecker.getScreeningLegalInfo(), makerChecker.getUser()); } if
		 * (makerChecker.getScreeningLegalInfo().getScreeningLegalRequestStatus(
		 * ) .equals(ConstantStatus.PROCEED_CHECKER_MAKER)) { status =
		 * ConstantStatus.PROCEED_MAKER_CHECKER; } else if
		 * (makerChecker.getScreeningLegalInfo().getScreeningLegalRequestStatus(
		 * ) .equals(ConstantStatus.PROCEED_MAKER_CHECKER)) { status =
		 * ConstantStatus.PROCEED_CHECKER_MAKER; }
		 * 
		 * String hashCodeForScreeningWorkflow = hashCodeGenerator
		 * .generateHashValueForScreeningWorkFlow(makerChecker.
		 * getScreeningLegalInfo(), status);
		 * makerCheckerDaoImpl.updateScreeningWorkflow(makerChecker.
		 * getScreeningLegalInfo(), status, hashCodeForScreeningWorkflow);
		 */
	}

	// public void makerToCapAndViceVersa(String jsonString) {
	//
	// }
	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public void makerToCheckerScreeningNaturalResponse(String jsonString) throws JsonParseException,
			JsonMappingException, IOException, SQLException, ParseException, NoSuchAlgorithmException {
		ScreeningNaturalResponse screeningNaturalResponse = new ObjectMapper().readValue(jsonString,
				ScreeningNaturalResponse.class);

		String relatedPerson = screeningNaturalDaoImpl
				.callProcedureRelatedPersonLDetails(screeningNaturalResponse.getScreeningNId());
		String relatedEntity = screeningNaturalDaoImpl
				.callProcedureRelatedEntityLDetails(screeningNaturalResponse.getScreeningNId());
		
		if (relatedPerson != null) {
			RequestRelatedPerson[] arrayRequestRelatedPersonData = new ObjectMapper().readValue(relatedPerson,
					RequestRelatedPerson[].class);
			List<RequestRelatedPerson> listRequestRelatedPersonData = Arrays.asList(arrayRequestRelatedPersonData);
			for (RequestRelatedPerson requestRelatedPerson : listRequestRelatedPersonData) {
				if (!requestRelatedPerson.isHaskycl()) {
					KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
					KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
					KycnAddressInfo kycnAddressInfo = new KycnAddressInfo();
					kycnPersonalInfo.setSalutation(requestRelatedPerson.getSalutation());
					kycnPersonalInfo.setFirstName(requestRelatedPerson.getFirstName());
					kycnPersonalInfo.setMiddleName(requestRelatedPerson.getMiddleName());
					kycnPersonalInfo.setLastName(requestRelatedPerson.getLastName());
					kycnPersonalInfo.setLsfName(requestRelatedPerson.getLsfName());
					kycnPersonalInfo.setLslName(requestRelatedPerson.getLslName());
					kycnPersonalInfo.setLsmName(requestRelatedPerson.getLsmName());
					kycnPersonalInfo.setDateOfBirth(requestRelatedPerson.getDateOfBirth());
					kycnPersonalInfo.setPanNumber(requestRelatedPerson.getPanNumber());
					kycnPersonalInfo.setNotes(requestRelatedPerson.getNotes());
					kycnPersonalInfo.setScreeningId(requestRelatedPerson.getScreeningNId());
					kycnIdentificationInfo.setPrimaryIdentificationDocumentNo(
							requestRelatedPerson.getPrimaryIdentificationDocumentNo());
					kycnIdentificationInfo.setPrimaryIdentificationDocumentType(
							requestRelatedPerson.getPrimaryIdentificationDocumentType());
					kycnIdentificationInfo.setCountryOfIssue(requestRelatedPerson.getCountryOfIssue());

					kycnAddressInfo.setZone(requestRelatedPerson.getZone());
					kycnAddressInfo.setDistrict(requestRelatedPerson.getDistrict());
					kycnAddressInfo.setMnVdc(requestRelatedPerson.getMnVdc());
					kycnAddressInfo.setEmailId(requestRelatedPerson.getEmailId());
					kycnAddressInfo.setPhoneNo(requestRelatedPerson.getMobileNumber());
					kycnAddressInfo.setWardNumber(requestRelatedPerson.getWardNo());
					boolean includeUser = true;
					boolean doNotIncludeUser = false;
					String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
							kycnPersonalInfo, screeningNaturalResponse.getUser(), includeUser);
					long kycnPersonalId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo,
							screeningNaturalResponse.getUser(), hashValueForKycnPersonInfo);
					kycnPersonalInfo.setId(kycnPersonalId);
					String hashValueForKycnPersonInfoUpdate = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
							kycnPersonalInfo, screeningNaturalResponse.getUser(), includeUser);
					String hashValueForKycnIdentificationInfo = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
									screeningNaturalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
					String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
							.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
									screeningNaturalResponse.getUser(), includeUser, kycnPersonalId);

					kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalId,
							hashValueForKycnIdentificationInfo, screeningNaturalResponse.getUser());
					kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalId,
							hashValueForKycnIdentificationInfoUpdate, screeningNaturalResponse.getUser());

					String hashValueForKycnAddressInfo = hashCodeGenerator.generateHashValueForKycnAddressInfo(
							kycnAddressInfo, screeningNaturalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
					String hashValueForKycnAddressInfoUpdate = hashCodeGenerator.generateHashValueForKycnAddressInfo(
							kycnAddressInfo, screeningNaturalResponse.getUser(), includeUser, kycnPersonalId);
					kycnDaoImpl.insertKycnAddressInfo(kycnAddressInfo, kycnPersonalId, hashValueForKycnAddressInfo,
							screeningNaturalResponse.getUser());
					kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
							kycnPersonalId, screeningNaturalResponse.getUser(), hashValueForKycnAddressInfoUpdate);
				}
			}
		}
		if (relatedEntity != null) {
			boolean includeUser = true;
			boolean doNotIncludeUser = false;
			// AmlLogger.printData("related Entity is not null");
			RequestRelatedEntityRequestData[] arrayRequestRelatedEntityData = new ObjectMapper()
					.readValue(relatedEntity, RequestRelatedEntityRequestData[].class);
			List<RequestRelatedEntityRequestData> RequestRelatedEntityRequestData = Arrays
					.asList(arrayRequestRelatedEntityData);
			for (RequestRelatedEntityRequestData requestRelatedEntityRequestData : RequestRelatedEntityRequestData) {

				if (!requestRelatedEntityRequestData.isHaskyc()) {
					Legal legal = new Legal();
					RegistrationInfo registrationInfo = new RegistrationInfo();
					RelatedEntity relatedEntityInfo = new RelatedEntity();
					Address address = new Address();
					legal.setNameOfTheInstitution(requestRelatedEntityRequestData.getNameOfInstitution());
					legal.setLsName(requestRelatedEntityRequestData.getLsName());
					legal.setDateOfEstablishment(requestRelatedEntityRequestData.getDateOfEstablishment());
					legal.setTypeOfIndustry(requestRelatedEntityRequestData.getTypeOfIndustry());
					// legal.setCustId(requestRelatedEntityRequestData.getCustId());
					legal.setPanNumber(requestRelatedEntityRequestData.getPanNumber());
					legal.setNotes(requestRelatedEntityRequestData.getNotes());
					legal.setScreeningId(requestRelatedEntityRequestData.getScreeningLId());
					registrationInfo.setRegdNumber(requestRelatedEntityRequestData.getRegistrationNo());
					legal.setPrimarySolId(requestRelatedEntityRequestData.getBranchSolId());
					address.setZone(requestRelatedEntityRequestData.getZone());
					address.setDistrict(requestRelatedEntityRequestData.getDistrict());
					address.setMnVdc(requestRelatedEntityRequestData.getMnVdc());
					address.setWardNumber(requestRelatedEntityRequestData.getWardNo());
					address.setEmailId(requestRelatedEntityRequestData.getEmailId());
					address.setPhoneNo(requestRelatedEntityRequestData.getContactNumber());
					relatedEntityInfo.setEntityType(requestRelatedEntityRequestData.getAccountsLSubType());
					String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal, doNotIncludeUser,
							screeningNaturalResponse.getUser());
					String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal, includeUser,
							screeningNaturalResponse.getUser());
					long kyclId = kyclDaoImpl.saveLegalInfo(legal, screeningNaturalResponse.getUser(), kyclHashCode);
					kyclDaoImpl.saveLegalInfoInUpdateTable(legal, screeningNaturalResponse.getUser(),
							kyclUpdateHashCode);
					String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							doNotIncludeUser, screeningNaturalResponse.getUser());

					String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
							includeUser, screeningNaturalResponse.getUser());
					kyclDaoImpl.saveAddress(address, kyclId, addressHashCode,screeningNaturalResponse.getUser());
					kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, screeningNaturalResponse.getUser(),
							addressUpdateHashCode);
					String registrationInfoHashCode = hashCodeGenerator.generateHashValueForRegistrationAddressStatus(
							registrationInfo, kyclId, doNotIncludeUser, screeningNaturalResponse.getUser());

					String registrationInfoUpdateHashCode = hashCodeGenerator
							.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId, includeUser,
									screeningNaturalResponse.getUser());
					kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
					kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId,
							screeningNaturalResponse.getUser(), registrationInfoUpdateHashCode);
					String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntityInfo, kyclId, doNotIncludeUser, screeningNaturalResponse.getUser());
					String relatedEntityUpdateHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
							relatedEntityInfo, kyclId, includeUser, screeningNaturalResponse.getUser());

					kyclDaoImpl.saveRelatedEntityInfo(relatedEntityInfo, kyclId, relatedEntityHashCode);
					kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntityInfo, kyclId,
							screeningNaturalResponse.getUser(), relatedEntityUpdateHashCode);
				}

			}
		}
		makerCheckerDaoImpl.updateScreeningNaturalRelatedRequest(screeningNaturalResponse);
		makerCheckerDaoImpl.disablePreviouslyLinkedCustomerRequest(screeningNaturalResponse);
		makerCheckerDaoImpl.updateScreeningNaturalRequest(screeningNaturalResponse);
		makerCheckerDaoImpl.updateScreeningNaturalWorkflow(screeningNaturalResponse);
	}
	
	
	
	public long makerToCheckerScreeningNaturalOthersResponse(String jsonString) throws Exception {
		long kycnPersonalId = 0;
		
		ScreeningNaturalResponse screeningNaturalResponse = new ObjectMapper().readValue(jsonString,
				ScreeningNaturalResponse.class);
		
		String primaryCustomer = screeningNaturalDaoImpl
				.callProcedurePrimaryCustomerDetails(screeningNaturalResponse.getScreeningNId());

		if (primaryCustomer != null) {
			RequestRelatedPerson[] arrayRequestRelatedPersonData = new ObjectMapper().readValue(primaryCustomer,
					RequestRelatedPerson[].class);
			List<RequestRelatedPerson> listRequestRelatedPersonData = Arrays.asList(arrayRequestRelatedPersonData);
			for (RequestRelatedPerson requestPrimaryPerson : listRequestRelatedPersonData) {

				if (!requestPrimaryPerson.isHaskycl()) {
					System.out.println(requestPrimaryPerson.getPurposeOfScreening());

					if (requestPrimaryPerson.getPurposeOfScreening().equals("Others")) {

						KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
						KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
						KycnAddressInfo kycnAddressInfo = new KycnAddressInfo();
						kycnPersonalInfo.setScreeningId(requestPrimaryPerson.getScreeningId());
						kycnPersonalInfo.setSalutation(requestPrimaryPerson.getSalutation());
						kycnPersonalInfo.setFirstName(requestPrimaryPerson.getFirstName());
						kycnPersonalInfo.setMiddleName(requestPrimaryPerson.getMiddleName());
						kycnPersonalInfo.setLastName(requestPrimaryPerson.getLastName());
						kycnPersonalInfo.setLsfName(requestPrimaryPerson.getLsfName());
						kycnPersonalInfo.setLslName(requestPrimaryPerson.getLslName());
						kycnPersonalInfo.setLsmName(requestPrimaryPerson.getLsmName());
						kycnPersonalInfo.setDateOfBirth(requestPrimaryPerson.getDateOfBirth());
						kycnPersonalInfo.setPanNumber(requestPrimaryPerson.getPanNumber());
						kycnPersonalInfo.setNotes(requestPrimaryPerson.getNotes());
						kycnPersonalInfo.setSimplifiedKyc(true);
						
						kycnIdentificationInfo.setPrimaryIdentificationDocumentNo(
								requestPrimaryPerson.getPrimaryIdentificationDocumentNo());
						kycnIdentificationInfo.setPrimaryIdentificationDocumentType(
								requestPrimaryPerson.getPrimaryIdentificationDocumentType());
						kycnIdentificationInfo.setCountryOfIssue(requestPrimaryPerson.getCountryOfIssue());

						kycnAddressInfo.setZone(requestPrimaryPerson.getZone());
						kycnAddressInfo.setDistrict(requestPrimaryPerson.getDistrict());
						kycnAddressInfo.setMnVdc(requestPrimaryPerson.getMnVdc());
						kycnAddressInfo.setEmailId(requestPrimaryPerson.getEmailId());
						kycnAddressInfo.setPhoneNo(requestPrimaryPerson.getMobileNumber());
						kycnAddressInfo.setWardNumber(requestPrimaryPerson.getWardNo());
						
						boolean includeUser = true;
						boolean doNotIncludeUser = false;
						String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
								kycnPersonalInfo, screeningNaturalResponse.getUser(), includeUser);
						kycnPersonalId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo,
								screeningNaturalResponse.getUser(), hashValueForKycnPersonInfo);
						kycnPersonalInfo.setId(kycnPersonalId);
						
						System.out.println("KYC ID : "+kycnPersonalId);
						String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
								.generateHashValueForKycnPersonalInfo(kycnPersonalInfo,
										screeningNaturalResponse.getUser(), includeUser);
						String hashValueForKycnIdentificationInfo = hashCodeGenerator
								.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
										screeningNaturalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
						String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
								.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
										screeningNaturalResponse.getUser(), includeUser, kycnPersonalId);

						kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalId,
								hashValueForKycnIdentificationInfo, screeningNaturalResponse.getUser());
						kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalId,
								hashValueForKycnIdentificationInfoUpdate, screeningNaturalResponse.getUser());

						String hashValueForKycnAddressInfo = hashCodeGenerator.generateHashValueForKycnAddressInfo(
								kycnAddressInfo, screeningNaturalResponse.getUser(), doNotIncludeUser, kycnPersonalId);
						String hashValueForKycnAddressInfoUpdate = hashCodeGenerator
								.generateHashValueForKycnAddressInfo(kycnAddressInfo,
										screeningNaturalResponse.getUser(), includeUser, kycnPersonalId);
						kycnDaoImpl.insertKycnAddressInfo(kycnAddressInfo, kycnPersonalId, hashValueForKycnAddressInfo,
								screeningNaturalResponse.getUser());
						kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
								kycnPersonalId, screeningNaturalResponse.getUser(), hashValueForKycnAddressInfoUpdate);
						
						kycnDaoImpl.updateScreeningKycId(kycnPersonalId,requestPrimaryPerson.getScreeningId());

					}

				}
			}
		}
		
		return kycnPersonalId;
		
	
	}
}
