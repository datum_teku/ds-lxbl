package com.trustaml.service.toandfro.reply.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.toandfro.reply.model.AttachmentInfo;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalInfo;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalResponse;
import com.trustaml.service.toandfro.reply.model.ScreeningNaturalResponse;

public class MakerCheckerDaoImpl {
	@Inject
	DBConnection dbConnection;

	/**
	 * @param attachmentInfo
	 * @param screeningLegalInfo
	 * @param user
	 * @throws SQLException
	 */
	public void saveAttachment(AttachmentInfo attachmentInfo, ScreeningLegalInfo screeningLegalInfo, User user)
			throws SQLException {
		String sql = "INSERT INTO screening_l_attachment (screening_l_request_id,screening_l_action_id,scanned_document_type,scanned_content,extension) VALUES (?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, screeningLegalInfo.getScreeningLegalId());
			ps.setLong(2, screeningLegalInfo.getLastActionId());
			ps.setString(3, attachmentInfo.getScannedDocumentType());
			ps.setBytes(4, attachmentInfo.getScannedContent().getBytes());
			ps.setString(5, attachmentInfo.getExtensionText());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningLegalInfo
	 * @param status
	 * @param hashCodeForScreeningWorkflow
	 * @throws SQLException
	 */
	public void updateScreeningWorkflow(ScreeningLegalInfo screeningLegalInfo, String status,
			String hashCodeForScreeningWorkflow) throws SQLException {
		String sql = "UPDATE screening_l_workflow SET screening_completed=?,workflow_completed=?,status=?,hash=? WHERE screening_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, false);
			ps.setBoolean(2, false);
			ps.setString(3, status);
			ps.setString(4, hashCodeForScreeningWorkflow);
			ps.setLong(5, screeningLegalInfo.getScreeningLegalId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNaturalResponse
	 * @throws SQLException
	 */
	public void updateScreeningNaturalRequest(ScreeningNaturalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_n_request SET cust_id=?,account_number=?,cbs_generated_name=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningNaturalResponse.getGeneratedCustomerId());
			ps.setString(2, screeningNaturalResponse.getForacid());
			ps.setString(3, screeningNaturalResponse.getCustName());
			ps.setLong(4, screeningNaturalResponse.getScreeningNId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
	
	
	public void disablePreviouslyLinkedCustomerRequest(ScreeningNaturalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_n_request SET is_active=? WHERE cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, false);
			ps.setString(2, screeningNaturalResponse.getGeneratedCustomerId());
			
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
	
	
	public void updateScreeningLegalRelatedRequest(ScreeningLegalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_n_request SET related_account_number=? FROM screening_l_related WHERE screening_n_request.id=screening_l_related.related_screening_n_id AND screening_l_related.primary_screening_l_id=?;;";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningNaturalResponse.getForacid());
			ps.setLong(2, screeningNaturalResponse.getScreeningLegalId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param screeningNaturalResponse
	 * @throws SQLException
	 */
	public void updateScreeningNaturalWorkflow(ScreeningNaturalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_n_workflow SET kycn_requested=? WHERE screening_n_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, screeningNaturalResponse.getScreeningNId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNaturalResponse
	 * @throws SQLException
	 */
	public void updateScreeningLegalRequest(ScreeningLegalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_l_request SET cust_id=?,account_number=?,cbs_generated_name=? WHERE id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningNaturalResponse.getGeneratedCustomerId());
			ps.setString(2, screeningNaturalResponse.getForacid());
			ps.setString(3, screeningNaturalResponse.getCustName());
			ps.setLong(4, screeningNaturalResponse.getScreeningLegalId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
	
	
	public void disablePreviouslyLinkedLegalCustomerRequest(ScreeningLegalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_l_request SET is_active=? WHERE cust_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, false);
			ps.setString(2, screeningNaturalResponse.getGeneratedCustomerId());
			
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param screeningNaturalResponse
	 * @throws SQLException
	 */
	public void updateScreeningLegalWorkflow(ScreeningLegalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_l_workflow SET kycl_requested=? WHERE screening_l_id=?";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setBoolean(1, true);
			ps.setLong(2, screeningNaturalResponse.getScreeningLegalId());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}
	
	public void updateScreeningNaturalRelatedRequest(ScreeningNaturalResponse screeningNaturalResponse) throws SQLException {
		String sql = "UPDATE screening_n_request SET related_account_number=? FROM screening_n_related WHERE screening_n_request.id=screening_n_related.related_screening_n_id AND screening_n_related.primary_screening_n_id=?;";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, screeningNaturalResponse.getForacid());
			ps.setLong(2, screeningNaturalResponse.getScreeningNId());
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

}
