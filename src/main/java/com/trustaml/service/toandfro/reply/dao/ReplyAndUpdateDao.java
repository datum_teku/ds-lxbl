package com.trustaml.service.toandfro.reply.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.ConstantEntity;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.kyc.kycl.dao.KyclDaoImpl;
import com.trustaml.service.kyc.kycl.model.Address;
import com.trustaml.service.kyc.kycl.model.Legal;
import com.trustaml.service.kyc.kycl.model.RegistrationInfo;
import com.trustaml.service.kyc.kycl.model.RelatedEntity;
import com.trustaml.service.kyc.kycn.dao.KYCNDaoImpl;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfo;
import com.trustaml.service.kyc.kycn.model.KycnAddressInfoUpdate;
import com.trustaml.service.kyc.kycn.model.KycnIdentificationInfo;
import com.trustaml.service.kyc.kycn.model.KycnPersonalInfo;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedPerson;
import com.trustaml.service.screening.natural.dao.ScreeningNaturalDaoImpl;
import com.trustaml.service.toandfro.reply.model.AccountLegalResponse;
import com.trustaml.service.toandfro.reply.model.AccountReply;
import com.trustaml.service.toandfro.reply.model.AccountResponse;
import com.trustaml.service.toandfro.reply.model.AttachmentInfo;
import com.trustaml.service.toandfro.reply.model.KYCReply;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveLegal;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveNatural;
import com.trustaml.service.toandfro.reply.model.ScreeningLegalComment;
import com.trustaml.service.toandfro.reply.model.ScreeningNaturalComment;

@Stateless
public class ReplyAndUpdateDao {

	@Inject
	ConstantEntity constantEntity;

	@Inject
	ReplyAndUpdateDaoImpl replyAndUpdateDaoImpl;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	@Inject
	KyclDaoImpl kyclDaoImpl;

	@Inject
	KYCNDaoImpl kycnDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	@Inject
	ScreeningNaturalDaoImpl screeningNaturalDaoImpl;

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	public void saveReplyAndAction(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ReplyAndSaveLegal replyAndSaveLegal = new ObjectMapper().readValue(jsonString, ReplyAndSaveLegal.class);
		long scheduleId = 0l;
		Long replyId = screeningLegalDaoImpl.saveReply(replyAndSaveLegal);
		screeningLegalDaoImpl.saveActionAfterReplyForScreening(replyId, replyAndSaveLegal, scheduleId);
		boolean scCompleted = true;

		if (replyAndSaveLegal.getReply().getActionType().equals(ConstantStatus.PROCEED_HIGH_RISK)
				|| replyAndSaveLegal.getReply().getActionType().equals(ConstantStatus.PROCEED_MEDIUM_RISK)
				|| replyAndSaveLegal.getReply().getActionType().equals(ConstantStatus.PROCEED_LOW_RISK)) {
			String status = ConstantStatus.PROCEED_CHECKER_MAKER;// proceed
																	// checker
																	// maker
			screeningLegalDaoImpl.updateWorkflowTable(status, scCompleted,
					replyAndSaveLegal.getReply().getScreeningLegalId(), "screening_completed", "hash");
		} else if (replyAndSaveLegal.getReply().getActionType().equals(ConstantStatus.REJECT)) {// reject
			String status = ConstantStatus.PROCEED_CHECKER_MAKER;

			screeningLegalDaoImpl.updateWorkflowTableCompleted(status, scCompleted, scCompleted,
					replyAndSaveLegal.getReply().getScreeningLegalId());

		}
		// else {// reject
		// String status = ConstantStatus.PROCEED_CHECKER_MAKER;
		// boolean screeningCompleted = true;
		// boolean workflowCompleted = true;
		// screeningLegalDaoImpl.updateWorkflowTableCompletedScreeningNatural(status,
		// screeningCompleted,
		// workflowCompleted,
		// replyAndSaveNatural.getReply().getScreeningNId());
		//
		// }

		for (String forwardTo : replyAndSaveLegal.getReply().getForwardTo()) {
			screeningLegalDaoImpl.saveForwardToUser(replyAndSaveLegal.getReply().getScreeningLegalId(), forwardTo,
					replyAndSaveLegal.getReply().getActionType(), replyAndSaveLegal.getUser());

			// screeningNaturalDaoImpl.saveForwardToUserUpdateTable(replyAndSaveNatural.getReply().getScreeningNId(),
			// forwardTo, replyAndSaveNatural.getReply().getActionType(),
			// replyAndSaveNatural.getUser(), savedId);

		}

	}

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public void accountLegalReply(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, ParseException, NoSuchAlgorithmException {

		AccountReply accountLegalReply = new ObjectMapper().readValue(jsonString, AccountReply.class);
		// ProcedureCallModelForRelatedEntityAndPerson
		// relatedPojo=screeningLegalDaoImpl.callProcedure(accountLegalReply.getAccountLegalReply().getScreeningLegalId());
		String accountReplyHashValue = hashCodeGenerator.generateHashValueForAccountReply(accountLegalReply);

		Long accountLegalReplyId = screeningLegalDaoImpl.insertAccountLegalReply(accountLegalReply,
				accountReplyHashValue);

		String accountLegalActionHashValue = hashCodeGenerator.generateHashValueForLegalAction(accountLegalReplyId,
				accountLegalReply);
		screeningLegalDaoImpl.insertIntoAccountLegalAction(accountLegalReplyId, accountLegalReply,
				accountLegalActionHashValue);
		boolean accountOpened = false;
		boolean workflowCompleted = false;
		boolean accountCompleted = false;
		boolean freeze = false;
		boolean accountRequested = true;
		boolean requestToUnfreeze = false;
		boolean aofReceived = false;
		boolean documentSent = false;

		// final String
		// actionType=accountLegalReply.getAccountLegalReply().getActionType();
		if (accountLegalReply.getAccountLegalReply().getActionType().equals(ConstantStatus.REJECTED)) {
			String status = ConstantStatus.PROCEED_CAP_MAKER;
			workflowCompleted = true;
			accountCompleted = true;
			String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
					accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent);
			screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

		} else if (accountLegalReply.getAccountLegalReply().getActionType()
				.equals(ConstantStatus.ADDITIONAL_DOCUMENTS_CAP)) {
			String status = ConstantStatus.PROCEED_CAP_MAKER;
			String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
					accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent);
			screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

		} else if (accountLegalReply.getAccountLegalReply().getActionType().equals(ConstantStatus.ACCOUNT_OPENED)) {
			if (accountLegalReply.getAccountLegalReply().getActionSubType()
					.equals(ConstantStatus.DOCUMENT_AOF_PENDING)) {
				String status = ConstantStatus.PROCEED_CAP_MAKER;
				accountOpened = true;
				String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
						accountOpened, accountCompleted, workflowCompleted,
						accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
						requestToUnfreeze, aofReceived, documentSent);
				screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted,
						workflowCompleted, accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze,
						accountRequested, requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

			} else if (accountLegalReply.getAccountLegalReply().getActionSubType()
					.equals(ConstantStatus.DOCUMENT_AOF_RECEIVED)) {
				documentSent = true;
				String status = ConstantStatus.PROCEED_MAKER_CAP;
				accountOpened = true;
				aofReceived = true;
				String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
						accountOpened, accountCompleted, workflowCompleted,
						accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
						requestToUnfreeze, aofReceived, documentSent);
				screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted,
						workflowCompleted, accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze,
						accountRequested, requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

			} else if (accountLegalReply.getAccountLegalReply().getActionSubType()
					.equals(ConstantStatus.DEFICIT_DOCUMENTS)) {
				String status = ConstantStatus.PROCEED_CAP_MAKER;
				accountOpened = true;
				String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
						accountOpened, accountCompleted, workflowCompleted,
						accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
						requestToUnfreeze, aofReceived, documentSent);
				screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted,
						workflowCompleted, accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze,
						accountRequested, requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

			} else if (accountLegalReply.getAccountLegalReply().getActionSubType()
					.equals(ConstantStatus.PROCEED_FOR_KYC)) {
				String status = ConstantStatus.PROCEED_CAP_MAKER;
				accountOpened = true;
				workflowCompleted = true;
				accountCompleted = true;
				documentSent = true;

				String relatedPerson = screeningLegalDaoImpl.callProcedureRelatedPersonLDetails(
						accountLegalReply.getAccountLegalReply().getScreeningLegalId());
				String relatedEntity = screeningLegalDaoImpl.callProcedureRelatedEntityLDetails(
						accountLegalReply.getAccountLegalReply().getScreeningLegalId());
				ObjectMapper objectMapper = new ObjectMapper();
				if (relatedPerson != null) {
					// AmlLogger.printData("relatedPerson is not null");
					RequestRelatedPerson[] arrayRequestRelatedPersonData = objectMapper.readValue(relatedPerson,
							RequestRelatedPerson[].class);
					List<RequestRelatedPerson> listRequestRelatedPersonData = Arrays
							.asList(arrayRequestRelatedPersonData);

					for (RequestRelatedPerson requestRelatedPerson : listRequestRelatedPersonData) {
						if (!requestRelatedPerson.isHaskycl()) {

							KycnPersonalInfo kycnPersonalInfo = new KycnPersonalInfo();
							KycnIdentificationInfo kycnIdentificationInfo = new KycnIdentificationInfo();
							KycnAddressInfo kycnAddressInfo = new KycnAddressInfo();
							kycnPersonalInfo.setSalutation(requestRelatedPerson.getSalutation());
							kycnPersonalInfo.setFirstName(requestRelatedPerson.getFirstName());
							kycnPersonalInfo.setMiddleName(requestRelatedPerson.getMiddleName());
							kycnPersonalInfo.setLastName(requestRelatedPerson.getLastName());
							kycnPersonalInfo.setLsfName(requestRelatedPerson.getLsfName());
							kycnPersonalInfo.setLslName(requestRelatedPerson.getLslName());
							kycnPersonalInfo.setLsmName(requestRelatedPerson.getLsmName());
							kycnPersonalInfo.setDateOfBirth(requestRelatedPerson.getDateOfBirth());
							kycnPersonalInfo.setPanNumber(requestRelatedPerson.getPanNumber());
							kycnPersonalInfo.setNotes(requestRelatedPerson.getNotes());
							kycnPersonalInfo.setScreeningId(requestRelatedPerson.getScreeningNId());
							kycnIdentificationInfo.setPrimaryIdentificationDocumentNo(
									requestRelatedPerson.getPrimaryIdentificationDocumentNo());
							kycnIdentificationInfo.setPrimaryIdentificationDocumentType(
									requestRelatedPerson.getPrimaryIdentificationDocumentType());
							kycnIdentificationInfo.setCountryOfIssue(requestRelatedPerson.getCountryOfIssue());

							kycnAddressInfo.setZone(requestRelatedPerson.getZone());
							kycnAddressInfo.setDistrict(requestRelatedPerson.getDistrict());
							kycnAddressInfo.setMnVdc(requestRelatedPerson.getMnVdc());
							kycnAddressInfo.setEmailId(requestRelatedPerson.getEmailId());
							kycnAddressInfo.setPhoneNo(requestRelatedPerson.getMobileNumber());
							kycnAddressInfo.setWardNumber(requestRelatedPerson.getWardNo());
							boolean includeUser = true;
							boolean doNotIncludeUser = false;
							String hashValueForKycnPersonInfo = hashCodeGenerator.generateHashValueForKycnPersonalInfo(
									kycnPersonalInfo, accountLegalReply.getUser(), includeUser);
							long kycnPersonalId = kycnDaoImpl.insertKycnPersonalInfo(kycnPersonalInfo,
									accountLegalReply.getUser(), hashValueForKycnPersonInfo);
							kycnPersonalInfo.setId(kycnPersonalId);
							String hashValueForKycnPersonInfoUpdate = hashCodeGenerator
									.generateHashValueForKycnPersonalInfo(kycnPersonalInfo, accountLegalReply.getUser(),
											includeUser);
							kycnDaoImpl.insertIntoKycnPersonalInfoUpdateTable(kycnPersonalInfo,
									accountLegalReply.getUser(), hashValueForKycnPersonInfoUpdate);
							String hashValueForKycnIdentificationInfo = hashCodeGenerator
									.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
											accountLegalReply.getUser(), doNotIncludeUser, kycnPersonalId);
							String hashValueForKycnIdentificationInfoUpdate = hashCodeGenerator
									.generateHashValueForKycnIdentificationInfo(kycnIdentificationInfo,
											accountLegalReply.getUser(), includeUser, kycnPersonalId);

							kycnDaoImpl.insertKycnIdentificationInfo(kycnIdentificationInfo, kycnPersonalId,
									hashValueForKycnIdentificationInfo, accountLegalReply.getUser());
							kycnDaoImpl.insertKycnIdentificationInfoUpdateTable(kycnIdentificationInfo, kycnPersonalId,
									hashValueForKycnIdentificationInfoUpdate, accountLegalReply.getUser());

							String hashValueForKycnAddressInfo = hashCodeGenerator.generateHashValueForKycnAddressInfo(
									kycnAddressInfo, accountLegalReply.getUser(), doNotIncludeUser, kycnPersonalId);
							String hashValueForKycnAddressInfoUpdate = hashCodeGenerator
									.generateHashValueForKycnAddressInfo(kycnAddressInfo, accountLegalReply.getUser(),
											includeUser, kycnPersonalId);
							kycnDaoImpl.insertKycnAddressInfo(kycnAddressInfo, kycnPersonalId,
									hashValueForKycnAddressInfo,accountLegalReply.getUser());
							kycnDaoImpl.insertKycnAddressInfoUpdateTable(new KycnAddressInfoUpdate(kycnAddressInfo),
									kycnPersonalId, accountLegalReply.getUser(), hashValueForKycnAddressInfoUpdate);

						}

					}
				}
				if (relatedEntity != null) {
					boolean includeUser = true;
					boolean doNotIncludeUser = false;
					// AmlLogger.printData("related Entity is not null");
					RequestRelatedEntityRequestData[] arrayRequestRelatedEntityData = objectMapper
							.readValue(relatedEntity, RequestRelatedEntityRequestData[].class);
					List<RequestRelatedEntityRequestData> RequestRelatedEntityRequestData = Arrays
							.asList(arrayRequestRelatedEntityData);
					for (RequestRelatedEntityRequestData requestRelatedEntityRequestData : RequestRelatedEntityRequestData) {

						if (!requestRelatedEntityRequestData.isHaskyc()) {
							Legal legal = new Legal();
							RegistrationInfo registrationInfo = new RegistrationInfo();
							RelatedEntity relatedEntityInfo = new RelatedEntity();
							Address address = new Address();
							legal.setNameOfTheInstitution(requestRelatedEntityRequestData.getNameOfInstitution());
							legal.setLsName(requestRelatedEntityRequestData.getLsName());
							legal.setDateOfEstablishment(requestRelatedEntityRequestData.getDateOfEstablishment());
							legal.setTypeOfIndustry(requestRelatedEntityRequestData.getTypeOfIndustry());
							// legal.setCustId(requestRelatedEntityRequestData.getCustId());
							legal.setPanNumber(requestRelatedEntityRequestData.getPanNumber());
							legal.setNotes(requestRelatedEntityRequestData.getNotes());
							legal.setScreeningId(requestRelatedEntityRequestData.getScreeningLId());
							registrationInfo.setRegdNumber(requestRelatedEntityRequestData.getRegistrationNo());
							legal.setPrimarySolId(requestRelatedEntityRequestData.getBranchSolId());
							address.setZone(requestRelatedEntityRequestData.getZone());
							address.setDistrict(requestRelatedEntityRequestData.getDistrict());
							address.setMnVdc(requestRelatedEntityRequestData.getMnVdc());
							address.setWardNumber(requestRelatedEntityRequestData.getWardNo());
							address.setEmailId(requestRelatedEntityRequestData.getEmailId());
							address.setPhoneNo(requestRelatedEntityRequestData.getContactNumber());
							relatedEntityInfo.setEntityType(requestRelatedEntityRequestData.getAccountsLSubType());
							String kyclHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal,
									doNotIncludeUser, accountLegalReply.getUser());
							String kyclUpdateHashCode = hashCodeGenerator.generateHashValueForKyclInfo(legal,
									includeUser, accountLegalReply.getUser());
							long kyclId = kyclDaoImpl.saveLegalInfo(legal,accountLegalReply.getUser(), kyclHashCode);
							kyclDaoImpl.saveLegalInfoInUpdateTable(legal, accountLegalReply.getUser(),
									kyclUpdateHashCode);
							String addressHashCode = hashCodeGenerator.generateHashValueForAddress(address, kyclId,
									doNotIncludeUser, accountLegalReply.getUser());

							String addressUpdateHashCode = hashCodeGenerator.generateHashValueForAddress(address,
									kyclId, includeUser, accountLegalReply.getUser());
							kyclDaoImpl.saveAddress(address, kyclId, addressHashCode, accountLegalReply.getUser());
							kyclDaoImpl.saveAddressInUpdateTable(address, kyclId, accountLegalReply.getUser(),
									addressUpdateHashCode);
							String registrationInfoHashCode = hashCodeGenerator
									.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId,
											doNotIncludeUser, accountLegalReply.getUser());

							String registrationInfoUpdateHashCode = hashCodeGenerator
									.generateHashValueForRegistrationAddressStatus(registrationInfo, kyclId,
											includeUser, accountLegalReply.getUser());
							kyclDaoImpl.saveRegistrationInfo(registrationInfo, kyclId, registrationInfoHashCode);
							kyclDaoImpl.saveRegistrationInfoInUpdateTable(registrationInfo, kyclId,
									accountLegalReply.getUser(), registrationInfoUpdateHashCode);
							String relatedEntityHashCode = hashCodeGenerator.generateHashValueForRelatedEntityStatus(
									relatedEntityInfo, kyclId, doNotIncludeUser, accountLegalReply.getUser());
							String relatedEntityUpdateHashCode = hashCodeGenerator
									.generateHashValueForRelatedEntityStatus(relatedEntityInfo, kyclId, includeUser,
											accountLegalReply.getUser());

							kyclDaoImpl.saveRelatedEntityInfo(relatedEntityInfo, kyclId, relatedEntityHashCode);
							kyclDaoImpl.saveRelatedEntityInfoInUpdateTable(relatedEntityInfo, kyclId,
									accountLegalReply.getUser(), relatedEntityUpdateHashCode);
						}

					}
				}
				String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
						accountOpened, accountCompleted, workflowCompleted,
						accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
						requestToUnfreeze, aofReceived, documentSent);

				screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted,
						workflowCompleted, accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze,
						accountRequested, requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

				// call procedures

			} else if (accountLegalReply.getAccountLegalReply().getActionSubType()
					.equals(ConstantStatus.DUE_DATE_ACKNOWLEDGED)
					|| accountLegalReply.getAccountLegalReply().getActionSubType()
							.equals(ConstantStatus.DUE_DATE_REJECTED)) {
				String status = ConstantStatus.PROCEED_CAP_MAKER;
				accountOpened = true;

				String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
						accountOpened, accountCompleted, workflowCompleted,
						accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
						requestToUnfreeze, aofReceived, documentSent);
				screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted,
						workflowCompleted, accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze,
						accountRequested, requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);
			}

		} else if (accountLegalReply.getAccountLegalReply().getActionType().equals(ConstantStatus.FREEZE_ACCOUNT)) {
			String status = ConstantStatus.PROCEED_CAP_MAKER;
			accountOpened = true;
			freeze = true;
			accountRequested = true;

			String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
					accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent);
			screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);

		} else if (accountLegalReply.getAccountLegalReply().getActionType().equals(ConstantStatus.UN_FREEZE_ACCOUNT)) {
			String status = ConstantStatus.PROCEED_MAKER_CAP;
			accountOpened = true;
			accountRequested = true;

			String updateLegalWorkflowHashValue = hashCodeGenerator.generateHashValueForUpdateWorkflow(status,
					accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent);
			screeningLegalDaoImpl.updateAccountLegalWorkflow(status, accountOpened, accountCompleted, workflowCompleted,
					accountLegalReply.getAccountLegalReply().getAccountsLegalId(), freeze, accountRequested,
					requestToUnfreeze, aofReceived, documentSent, updateLegalWorkflowHashValue);
		}
		String updateLegalRequestHashValue = hashCodeGenerator
				.generateHashValueForUdpateAccountLegalRequest(accountLegalReply);
		screeningLegalDaoImpl.updateAccountLegalRequest(accountLegalReply, updateLegalRequestHashValue);

	}

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void accountLegalResponse(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException, ParseException {
		AccountResponse accountLegalResponse = new ObjectMapper().readValue(jsonString, AccountResponse.class);
		AccountLegalResponse accountResponse = accountLegalResponse.getAccountLegalResponse();
		if (accountResponse.getCompleteDocument()) {

			screeningLegalDaoImpl.updateCompleteDocumentsFlag(accountResponse.getAccountLegalLastActionId());
		}
		if (accountResponse.getRequestToUnfreezeStatus()) {
			screeningLegalDaoImpl.updateRequestToUnfreezeFlag(accountResponse.getAccountLegalRequestId(),
					accountResponse.getRequestToUnfreezeStatus());
		}

		for (AttachmentInfo attachmentInfo : accountLegalResponse.getListAttachmentInfo()) {
			screeningLegalDaoImpl.updateDueDateOfMakerReply(accountResponse.getAccountLegalRequestId(),
					accountResponse.getDueDateOfPendingDocument(), accountResponse.getDueDateComments(),
					accountResponse.getAccountLegalLastActionId(), attachmentInfo, accountLegalResponse.getUser());
		}

		if (accountResponse.getDueDateOfPendingDocument() != null
				&& !accountResponse.getDueDateOfPendingDocument().isEmpty()) {
			screeningLegalDaoImpl.updateDueDateOfMakerReply(accountResponse.getAccountLegalRequestId(),
					accountResponse.getDueDateOfPendingDocument(), accountResponse.getDueDateComments(),
					accountResponse.getAccountLegalLastActionId(), accountLegalResponse.getUser());

		}
		screeningLegalDaoImpl.updateLegalRequest(accountResponse.getAccountLegalRequestId(),
				accountResponse.getAccountLegalRequestStatus());
	}

	/**
	 * @param jsonString
	 * @throws SQLException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public void saveReplyAndActionForScreeningNatural(String jsonString)
			throws SQLException, JsonParseException, JsonMappingException, IOException {
		ReplyAndSaveNatural replyAndSaveNatural = new ObjectMapper().readValue(jsonString, ReplyAndSaveNatural.class);
		Long scheduleId = 0l;
		Long replyId = replyAndUpdateDaoImpl.saveNaturalReply(replyAndSaveNatural);
		replyAndUpdateDaoImpl.saveActionAfterReplyForScreening(replyId, replyAndSaveNatural, scheduleId);

		if (replyAndSaveNatural.getReply().getActionType().equals(ConstantStatus.PROCEED_HIGH_RISK)
				|| replyAndSaveNatural.getReply().getActionType().equals(ConstantStatus.PROCEED_MEDIUM_RISK)
				|| replyAndSaveNatural.getReply().getActionType().equals(ConstantStatus.PROCEED_LOW_RISK)) {
			String status = ConstantStatus.PROCEED_CHECKER_MAKER;// proceed
																	// checker
																	// // maker
			boolean scCompleted = true;
			screeningLegalDaoImpl.updateWorkflowTableScreeningNatural(status, scCompleted,
					replyAndSaveNatural.getReply().getScreeningNId(), "screening_completed");
		} else if (replyAndSaveNatural.getReply().getActionType().equals(ConstantStatus.REJECT)) {// reject
			String status = ConstantStatus.PROCEED_CHECKER_MAKER;
			boolean screeningCompleted = true;
			boolean workflowCompleted = true;
			screeningLegalDaoImpl.updateWorkflowTableCompletedScreeningNatural(status, screeningCompleted,
					workflowCompleted, replyAndSaveNatural.getReply().getScreeningNId());

		}

		for (String forwardTo : replyAndSaveNatural.getReply().getForwardTo()) {
			screeningNaturalDaoImpl.saveForwardToUser(replyAndSaveNatural.getReply().getScreeningNId(), forwardTo,
					replyAndSaveNatural.getReply().getActionType(), replyAndSaveNatural.getUser());
		}

	}

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	public void saveReplyCommentForScreeningNatural(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ScreeningNaturalComment screeningNaturalComment = new ObjectMapper().readValue(jsonString,
				ScreeningNaturalComment.class);
		screeningNaturalDaoImpl.updateNaturalForwardApplicationForComments(screeningNaturalComment);

	}

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	public void saveReplyCommentForScreeningLegal(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		ScreeningLegalComment screeningLegalComment = new ObjectMapper().readValue(jsonString,
				ScreeningLegalComment.class);
		screeningNaturalDaoImpl.updateLegalForwardApplicationForComments(screeningLegalComment);

	}

	/**
	 * @param jsonString
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 */
	public void saveReplyForKYC(String jsonString)
			throws JsonParseException, JsonMappingException, IOException, SQLException {
		KYCReply kycReply = new ObjectMapper().readValue(jsonString, KYCReply.class);
		screeningNaturalDaoImpl.saveKYCReply(kycReply);

	}

}
