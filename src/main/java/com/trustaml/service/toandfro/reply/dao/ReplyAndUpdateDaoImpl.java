package com.trustaml.service.toandfro.reply.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.toandfro.reply.model.ReplyAndSave;
import com.trustaml.service.toandfro.reply.model.ReplyAndSaveNatural;

public class ReplyAndUpdateDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @param replyAndSave
	 * @return
	 * @throws SQLException
	 */
	public Long saveReply(ReplyAndSave replyAndSave) throws SQLException {

		Long replyId = 0L;
		String sql = "INSERT INTO screening_l_reply (screening_l_request_id,checker) VALUES (?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, replyAndSave.getReply().getScreeningLId());
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				replyId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return replyId;

	}

	/**
	 * @param replyAndSave
	 * @return
	 * @throws SQLException
	 */
	public Long saveNaturalReply(ReplyAndSaveNatural replyAndSave) throws SQLException {

		Long replyId = 0L;
		String sql = "INSERT INTO screening_n_reply (screening_request_n_id,checker) VALUES (?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setLong(1, replyAndSave.getReply().getScreeningNId());
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				replyId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return replyId;

	}

	/**
	 * @param replyId
	 * @param replyAndSave
	 * @throws SQLException
	 */
	public void saveActionAfterReply(Long replyId, ReplyAndSave replyAndSave) throws SQLException {
		String sql = "INSERT INTO screening_l_action (screening_l_reply_id,checker,reason,action_type) VALUES (?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, replyId);
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.setString(3, replyAndSave.getReply().getReason());
			ps.setString(4, replyAndSave.getReply().getActionType());
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

	/**
	 * @param replyId
	 * @param replyAndSave
	 * @throws SQLException
	 */
	public void saveActionAfterReplyForScreening(Long replyId, ReplyAndSaveNatural replyAndSave, long scheduleId)
			throws SQLException {
		String sql = "INSERT INTO screening_n_action (screening_reply_n_id,checker,reason,action_type,is_migrated,schedule_id) VALUES (?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, replyId);
			ps.setString(2, replyAndSave.getUser().getUserName());
			ps.setString(3, replyAndSave.getReply().getReason());
			ps.setString(4, replyAndSave.getReply().getActionType());
			ps.setBoolean(5, replyAndSave.getReply().isMigrated());
			ps.setLong(6, scheduleId);
			ps.executeUpdate();
		} finally {
			connection.close();
		}

	}

}
