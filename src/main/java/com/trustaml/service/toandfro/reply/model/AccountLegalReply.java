package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountLegalReply {

	@JsonProperty("accounts_l_id")
	private long accountsLegalId;

	@JsonProperty("screening_l_id")
	private long screeningLegalId;

	@JsonProperty("action_type")
	private String actionType;

	@JsonProperty("action_sub_type")
	private String actionSubType;

	@JsonProperty("reason")
	private String reason;

	@JsonProperty("generated_customer_id")
	private String generatedCustomerId;

	@JsonProperty("cust_name")
	private String custName;

	@JsonProperty("foracid")
	private String foracId;

	public AccountLegalReply(long accountsLegalId, long screeningLegalId, String actionType, String actionSubType,
			String reason, String generatedCustomerId, String custName, String foracId) {
		super();
		this.accountsLegalId = accountsLegalId;
		this.screeningLegalId = screeningLegalId;
		this.actionType = actionType;
		this.actionSubType = actionSubType;
		this.reason = reason;
		this.generatedCustomerId = generatedCustomerId;
		this.custName = custName;
		this.foracId = foracId;
	}

	public AccountLegalReply() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getAccountsLegalId() {
		return accountsLegalId;
	}

	public void setAccountsLegalId(long accountsLegalId) {
		this.accountsLegalId = accountsLegalId;
	}

	public long getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(long screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionSubType() {
		return actionSubType;
	}

	public void setActionSubType(String actionSubType) {
		this.actionSubType = actionSubType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getGeneratedCustomerId() {
		return generatedCustomerId;
	}

	public void setGeneratedCustomerId(String generatedCustomerId) {
		this.generatedCustomerId = generatedCustomerId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getForacId() {
		return foracId;
	}

	public void setForacId(String foracId) {
		this.foracId = foracId;
	}

}
