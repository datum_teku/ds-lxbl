package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountLegalResponse {

	@JsonProperty("screening_l_id")
	private String screeningLegalId;

	@JsonProperty("accounts_l_request_id")
	private long accountLegalRequestId;

	@JsonProperty("accounts_l_last_action_id")
	private long accountLegalLastActionId;

	@JsonProperty("accounts_l_request_status")
	private String accountLegalRequestStatus;

	@JsonProperty("request_to_unfreeze_status")
	private boolean requestToUnfreezeStatus;

	@JsonProperty("complete_documents")
	private boolean completeDocument;

	@JsonProperty("due_date_of_pending_documents")
	private String dueDateOfPendingDocument;

	@JsonProperty("due_date_comments")
	private String dueDateComments;

	public AccountLegalResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountLegalResponse(String screeningLegalId, long accountLegalRequestId, long accountLegalLastActionId,
			String accountLegalRequestStatus, boolean requestToUnfreezeStatus, boolean completeDocument,
			String dueDateOfPendingDocument, String dueDateComments) {
		super();
		this.screeningLegalId = screeningLegalId;
		this.accountLegalRequestId = accountLegalRequestId;
		this.accountLegalLastActionId = accountLegalLastActionId;
		this.accountLegalRequestStatus = accountLegalRequestStatus;
		this.requestToUnfreezeStatus = requestToUnfreezeStatus;
		this.completeDocument = completeDocument;
		this.dueDateOfPendingDocument = dueDateOfPendingDocument;
		this.dueDateComments = dueDateComments;
	}

	public String getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(String screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

	public long getAccountLegalRequestId() {
		return accountLegalRequestId;
	}

	public void setAccountLegalRequestId(long accountLegalRequestId) {
		this.accountLegalRequestId = accountLegalRequestId;
	}

	public long getAccountLegalLastActionId() {
		return accountLegalLastActionId;
	}

	public void setAccountLegalLastActionId(long accountLegalLastActionId) {
		this.accountLegalLastActionId = accountLegalLastActionId;
	}

	public String getAccountLegalRequestStatus() {
		return accountLegalRequestStatus;
	}

	public void setAccountLegalRequestStatus(String accountLegalRequestStatus) {
		this.accountLegalRequestStatus = accountLegalRequestStatus;
	}

	public boolean getRequestToUnfreezeStatus() {
		return requestToUnfreezeStatus;
	}

	public void setRequestToUnfreezeStatus(boolean requestToUnfreezeStatus) {
		this.requestToUnfreezeStatus = requestToUnfreezeStatus;
	}

	public boolean getCompleteDocument() {
		return completeDocument;
	}

	public void setCompleteDocument(boolean completeDocument) {
		this.completeDocument = completeDocument;
	}

	public String getDueDateOfPendingDocument() {
		return dueDateOfPendingDocument;
	}

	public void setDueDateOfPendingDocument(String dueDateOfPendingDocument) {
		this.dueDateOfPendingDocument = dueDateOfPendingDocument;
	}

	public String getDueDateComments() {
		return dueDateComments;
	}

	public void setDueDateComments(String dueDateComments) {
		this.dueDateComments = dueDateComments;
	}

}
