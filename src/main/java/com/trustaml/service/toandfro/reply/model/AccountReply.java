package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class AccountReply {

	@JsonProperty("account_legal_reply")
	private AccountLegalReply accountLegalReply;

	@JsonProperty("user")
	private User user;

	public AccountReply() {
		super();
	}

	public AccountReply(AccountLegalReply accountLegalReply, User user) {
		super();
		this.accountLegalReply = accountLegalReply;
		this.user = user;
	}

	public AccountLegalReply getAccountLegalReply() {
		return accountLegalReply;
	}

	public void setAccountLegalReply(AccountLegalReply accountLegalReply) {
		this.accountLegalReply = accountLegalReply;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
