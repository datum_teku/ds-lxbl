package com.trustaml.service.toandfro.reply.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class AccountResponse {

	@JsonProperty("attachment_info")
	private List<AttachmentInfo> listAttachmentInfo;

	@JsonProperty("accounts_l_info")
	private AccountLegalResponse accountLegalResponse;

	@JsonProperty("user")
	private User user;

	public AccountResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountResponse(List<AttachmentInfo> listAttachmentInfo, AccountLegalResponse accountLegalResponse,
			User user) {
		super();
		this.listAttachmentInfo = listAttachmentInfo;
		this.accountLegalResponse = accountLegalResponse;
		this.user = user;
	}

	public List<AttachmentInfo> getListAttachmentInfo() {
		return listAttachmentInfo;
	}

	public void setListAttachmentInfo(List<AttachmentInfo> listAttachmentInfo) {
		this.listAttachmentInfo = listAttachmentInfo;
	}

	public AccountLegalResponse getAccountLegalResponse() {
		return accountLegalResponse;
	}

	public void setAccountLegalResponse(AccountLegalResponse accountLegalResponse) {
		this.accountLegalResponse = accountLegalResponse;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
