package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AttachmentInfo {

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("scanned_document_type")
	private String scannedDocumentType;

	@JsonProperty("scanned_content")
	private String scannedContent;

	@JsonProperty("extension_text")
	private String extensionText;

	public AttachmentInfo(String notes, String scannedDocumentType, String scannedContent, String extensionText) {
		super();
		this.notes = notes;
		this.scannedDocumentType = scannedDocumentType;
		this.scannedContent = scannedContent;
		this.extensionText = extensionText;
	}

	public AttachmentInfo() {
		this.notes = "";
		this.scannedDocumentType = "";
		this.scannedContent = "";
		this.extensionText = "";
		// TODO Auto-generated constructor stub
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getScannedDocumentType() {
		return scannedDocumentType;
	}

	public void setScannedDocumentType(String scannedDocumentType) {
		this.scannedDocumentType = scannedDocumentType;
	}

	public String getScannedContent() {
		return scannedContent;
	}

	public void setScannedContent(String scannedContent) {
		this.scannedContent = scannedContent;
	}

	public String getExtensionText() {
		return extensionText;
	}

	public void setExtensionText(String extensionText) {
		this.extensionText = extensionText;
	}

}
