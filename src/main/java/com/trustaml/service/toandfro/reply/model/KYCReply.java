package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class KYCReply {

	@JsonProperty("approved")
	protected Boolean approved;

	@JsonProperty("name")
	protected String name;

	@JsonProperty("id")
	protected Long id;

	@JsonProperty("user")
	protected User user;

	public KYCReply(Boolean approved, String name, Long id, User user) {
		super();
		this.approved = approved;
		this.name = name;
		this.id = id;
		this.user = user;
	}

	public KYCReply() {
		super();
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
