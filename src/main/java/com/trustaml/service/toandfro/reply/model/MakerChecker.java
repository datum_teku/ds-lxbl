package com.trustaml.service.toandfro.reply.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class MakerChecker {

	@JsonProperty("screening_l_info")
	private ScreeningLegalInfo screeningLegalInfo;

	@JsonProperty("attachment_info")
	private List<AttachmentInfo> listAttachmentInfo;

	@JsonProperty("user")
	private User user;

	public MakerChecker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MakerChecker(ScreeningLegalInfo screeningLegalInfo, List<AttachmentInfo> listAttachmentInfo, User user) {
		super();
		this.screeningLegalInfo = screeningLegalInfo;
		this.listAttachmentInfo = listAttachmentInfo;
		this.user = user;
	}

	public ScreeningLegalInfo getScreeningLegalInfo() {
		return screeningLegalInfo;
	}

	public void setScreeningLegalInfo(ScreeningLegalInfo screeningLegalInfo) {
		this.screeningLegalInfo = screeningLegalInfo;
	}

	public List<AttachmentInfo> getListAttachmentInfo() {
		return listAttachmentInfo;
	}

	public void setListAttachmentInfo(List<AttachmentInfo> listAttachmentInfo) {
		this.listAttachmentInfo = listAttachmentInfo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
