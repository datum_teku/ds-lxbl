package com.trustaml.service.toandfro.reply.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.screening.legal.model.RequestRelatedEntityRequestData;
import com.trustaml.service.screening.legal.model.RequestRelatedPerson;

public class ProcedureCallModelForRelatedEntityAndPerson {

	@JsonProperty("screening_l_related_entity")
	private List<RequestRelatedEntityRequestData> listRelatedEntity;

	@JsonProperty("screening_l_related_person")
	private List<RequestRelatedPerson> listRelatedPerson;

	public ProcedureCallModelForRelatedEntityAndPerson(List<RequestRelatedEntityRequestData> listRelatedEntity,
			List<RequestRelatedPerson> listRelatedPerson) {
		super();
		this.listRelatedEntity = listRelatedEntity;
		this.listRelatedPerson = listRelatedPerson;
	}

	public ProcedureCallModelForRelatedEntityAndPerson() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<RequestRelatedEntityRequestData> getListRelatedEntity() {
		return listRelatedEntity;
	}

	public void setListRelatedEntity(List<RequestRelatedEntityRequestData> listRelatedEntity) {
		this.listRelatedEntity = listRelatedEntity;
	}

	public List<RequestRelatedPerson> getListRelatedPerson() {
		return listRelatedPerson;
	}

	public void setListRelatedPerson(List<RequestRelatedPerson> listRelatedPerson) {
		this.listRelatedPerson = listRelatedPerson;
	}

}
