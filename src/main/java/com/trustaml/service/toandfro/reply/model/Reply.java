package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Reply {

	@JsonProperty("screening_l_id")
	private long screeningLId;

	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;

	@JsonProperty("action_type")
	private String actionType;

	@JsonProperty("reason")
	private String reason;

	public Reply() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reply(long screeningLId, String purposeOfScreening, String actionType, String reason) {
		super();
		this.screeningLId = screeningLId;
		this.purposeOfScreening = purposeOfScreening;
		this.actionType = actionType;
		this.reason = reason;
	}

	public long getScreeningLId() {
		return screeningLId;
	}

	public void setScreeningLId(long screeningLId) {
		this.screeningLId = screeningLId;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreening;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreening = purposeOfScreening;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
