package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ReplyAndSave {

	@JsonProperty("reply")
	Reply reply;

	@JsonProperty("user")
	User user;

	public ReplyAndSave() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyAndSave(Reply reply, User user) {
		super();
		this.reply = reply;
		this.user = user;
	}

	public Reply getReply() {
		return reply;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
