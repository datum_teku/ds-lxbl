package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ReplyAndSaveLegal {
	@JsonProperty("reply")
	ReplyScreeningLegal reply;

	@JsonProperty("user")
	User user;

	public ReplyAndSaveLegal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyAndSaveLegal(ReplyScreeningLegal reply, User user) {
		super();
		this.reply = reply;
		this.user = user;
	}

	public ReplyScreeningLegal getReply() {
		return reply;
	}

	public void setReply(ReplyScreeningLegal reply) {
		this.reply = reply;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
