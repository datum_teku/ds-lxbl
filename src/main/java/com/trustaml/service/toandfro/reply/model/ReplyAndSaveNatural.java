package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ReplyAndSaveNatural {
	@JsonProperty("reply")
	ReplyScreeningNatural reply;

	@JsonProperty("user")
	User user;

	public ReplyAndSaveNatural() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyAndSaveNatural(ReplyScreeningNatural reply, User user) {
		super();
		this.reply = reply;
		this.user = user;
	}

	public ReplyScreeningNatural getReply() {
		return reply;
	}

	public void setReply(ReplyScreeningNatural reply) {
		this.reply = reply;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
