package com.trustaml.service.toandfro.reply.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReplyScreeningLegal {

	@JsonProperty("screening_l_id")
	private long screeningLegalId;

	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;

	@JsonProperty("action_type")
	private String actionType;

	@JsonProperty("reason")
	private String reason;

	@JsonProperty("forward_to")
	private List<String> forwardTo;

	@JsonProperty("migrate")
	private boolean isMigrated;

	public ReplyScreeningLegal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyScreeningLegal(long screeningLegalId, String purposeOfScreening, String actionType, String reason,
			List<String> forwardTo) {
		super();
		this.screeningLegalId = screeningLegalId;
		this.purposeOfScreening = purposeOfScreening;
		this.actionType = actionType;
		this.reason = reason;
		this.forwardTo = forwardTo;
	}

	public long getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(long screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreening;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreening = purposeOfScreening;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<String> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(List<String> forwardTo) {
		this.forwardTo = forwardTo;
	}

	public boolean isMigrated() {
		return isMigrated;
	}

	public void setMigrated(boolean isMigrated) {
		this.isMigrated = isMigrated;
	}

}
