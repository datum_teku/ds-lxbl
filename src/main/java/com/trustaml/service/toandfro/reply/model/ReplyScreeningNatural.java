package com.trustaml.service.toandfro.reply.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReplyScreeningNatural {

	@JsonProperty("screening_n_id")
	private long screeningNId;

	@JsonProperty("purpose_of_screening")
	private String purposeOfScreening;

	@JsonProperty("action_type")
	private String actionType;

	@JsonProperty("reason")
	private String reason;

	@JsonProperty("forward_to")
	private List<String> forwardTo;

	@JsonProperty("migrate")
	private boolean isMigrated;

	public ReplyScreeningNatural() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReplyScreeningNatural(long screeningNId, String purposeOfScreening, String actionType, String reason,
			List<String> forwardTo) {
		super();
		this.screeningNId = screeningNId;
		this.purposeOfScreening = purposeOfScreening;
		this.actionType = actionType;
		this.reason = reason;
		this.forwardTo = forwardTo;
	}

	public long getScreeningNId() {
		return screeningNId;
	}

	public void setScreeningNId(long screeningNId) {
		this.screeningNId = screeningNId;
	}

	public String getPurposeOfScreening() {
		return purposeOfScreening;
	}

	public void setPurposeOfScreening(String purposeOfScreening) {
		this.purposeOfScreening = purposeOfScreening;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<String> getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(List<String> forwardTo) {
		this.forwardTo = forwardTo;
	}

	public boolean isMigrated() {
		return isMigrated;
	}

	public void setMigrated(boolean isMigrated) {
		this.isMigrated = isMigrated;
	}

}
