package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ScreeningLegalComment {

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("screening_l_id")
	private long screeningLegalId;

	@JsonProperty("user")
	private User user;

	@JsonProperty("id")
	private long id;

	public ScreeningLegalComment(String comment, long screeningLegalId, User user, long id) {
		super();
		this.comment = comment;
		this.screeningLegalId = screeningLegalId;
		this.user = user;
		this.id = id;
	}

	public ScreeningLegalComment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(long screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

}
