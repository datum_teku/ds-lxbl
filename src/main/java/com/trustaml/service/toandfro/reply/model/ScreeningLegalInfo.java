package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScreeningLegalInfo {

	@JsonProperty("screening_l_request_status")
	private String screeningLegalRequestStatus;

	@JsonProperty("screening_l_id")
	private long screeningLegalId;

	@JsonProperty("last_action_id")
	private long lastActionId;

	public ScreeningLegalInfo(String screeningLegalRequestStatus, long screeningLegalId, long lastActionId) {
		super();
		this.screeningLegalRequestStatus = screeningLegalRequestStatus;
		this.screeningLegalId = screeningLegalId;
		this.lastActionId = lastActionId;
	}

	public ScreeningLegalInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getScreeningLegalRequestStatus() {
		return screeningLegalRequestStatus;
	}

	public void setScreeningLegalRequestStatus(String screeningLegalRequestStatus) {
		this.screeningLegalRequestStatus = screeningLegalRequestStatus;
	}

	public long getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(long screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

	public long getLastActionId() {
		return lastActionId;
	}

	public void setLastActionId(long lastActionId) {
		this.lastActionId = lastActionId;
	}

}
