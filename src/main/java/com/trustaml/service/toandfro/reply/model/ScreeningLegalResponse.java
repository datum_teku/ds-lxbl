package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ScreeningLegalResponse {

	@JsonProperty("generated_customer_id")
	private String generatedCustomerId;

	@JsonProperty("cust_name")
	private String custName;

	@JsonProperty("screening_l_id")
	private long screeningLegalId;

	@JsonProperty("foracid")
	private String foracid;

	@JsonProperty("user")
	private User user;

	public ScreeningLegalResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreeningLegalResponse(String generatedCustomerId, String custName, long screeningLegalId, String foracid,
			User user) {
		super();
		this.generatedCustomerId = generatedCustomerId;
		this.custName = custName;
		this.screeningLegalId = screeningLegalId;
		this.foracid = foracid;
		this.user = user;
	}

	public String getGeneratedCustomerId() {
		return generatedCustomerId;
	}

	public void setGeneratedCustomerId(String generatedCustomerId) {
		this.generatedCustomerId = generatedCustomerId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public long getScreeningLegalId() {
		return screeningLegalId;
	}

	public void setScreeningLegalId(long screeningLegalId) {
		this.screeningLegalId = screeningLegalId;
	}

	public String getForacid() {
		return foracid;
	}

	public void setForacid(String foracid) {
		this.foracid = foracid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
