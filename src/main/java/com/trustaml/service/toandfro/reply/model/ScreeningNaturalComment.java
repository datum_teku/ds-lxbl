package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ScreeningNaturalComment {

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("screening_n_id")
	private long screeningNaturalId;

	@JsonProperty("user")
	private User user;

	@JsonProperty("id")
	private long id;

	public ScreeningNaturalComment(String comment, long screeningNaturalId, User user, long id) {
		super();
		this.comment = comment;
		this.screeningNaturalId = screeningNaturalId;
		this.user = user;
		this.id = id;
	}

	public ScreeningNaturalComment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getScreeningNaturalId() {
		return screeningNaturalId;
	}

	public void setScreeningNaturalId(long screeningNaturalId) {
		this.screeningNaturalId = screeningNaturalId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
