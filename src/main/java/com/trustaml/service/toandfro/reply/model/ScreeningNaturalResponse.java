package com.trustaml.service.toandfro.reply.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class ScreeningNaturalResponse {

	@JsonProperty("generated_customer_id")
	private String generatedCustomerId;

	@JsonProperty("cust_name")
	private String custName;

	@JsonProperty("screening_n_id")
	private long screeningNId;

	@JsonProperty("foracid")
	private String foracid;

	@JsonProperty("user")
	private User user;

	public ScreeningNaturalResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreeningNaturalResponse(String generatedCustomerId, String custName, long screeningNId, String foracid,
			User user) {
		super();
		this.generatedCustomerId = generatedCustomerId;
		this.custName = custName;
		this.screeningNId = screeningNId;
		this.foracid = foracid;
		this.user = user;
	}

	public String getGeneratedCustomerId() {
		return generatedCustomerId;
	}

	public void setGeneratedCustomerId(String generatedCustomerId) {
		this.generatedCustomerId = generatedCustomerId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public long getScreeningNId() {
		return screeningNId;
	}

	public void setScreeningNId(long screeningNId) {
		this.screeningNId = screeningNId;
	}

	public String getForacid() {
		return foracid;
	}

	public void setForacid(String foracid) {
		this.foracid = foracid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
