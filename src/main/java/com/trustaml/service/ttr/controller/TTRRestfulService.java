package com.trustaml.service.ttr.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.ttr.dao.TTRDao;
import com.trustaml.service.ttr.model.Flag;
import com.trustaml.service.ttr.model.Report;

@Path("/ttr-rest-ful")
public class TTRRestfulService {
	@Inject
	TTRDao ttrDao;

	@GET
	@Path("/forward-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forwardTTRList() throws SQLException, JsonParseException, JsonMappingException, IOException {
		return ResponseReturn.sucess(ttrDao.forwardTTRList());
	}

	@GET
	@Path("/forward-reschedule")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forwardReschedule() throws SQLException {
		return ResponseReturn.sucess(ttrDao.isScheduleExists());
	}

	@GET
	@Path("/forward-detail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forwardTTRDetail(@QueryParam("code") String code,@QueryParam("trans_date") String transDate) throws SQLException {
		return ResponseReturn.sucess(ttrDao.findByCode(code,transDate));

	}

	@POST
	@Path("/forward-submit")
	@Produces(MediaType.APPLICATION_JSON)
	public Response markReportBulkSubmit(List<Flag> flagList) throws SQLException {
		return ResponseReturn.sucess(String.valueOf(ttrDao.updateFlagBulk(flagList, "user")));
	}

	@GET
	@Path("/report-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reportList() throws SQLException {
		return ResponseReturn.sucess(ttrDao.findAllReport());
	}

	@POST
	@Path("/report-submit")
	@Produces(MediaType.APPLICATION_JSON)
	public Response forwardTTRSubmit(Report report) throws SQLException {
		return ResponseReturn.sucess(String.valueOf(ttrDao.updateReport(report, "username")));
	}

	@GET
	@Path("/report-detail-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reportDetailList(@QueryParam("id") Long id) throws SQLException {
		return ResponseReturn.sucess(ttrDao.findReportLogById(id));

	}

	@GET
	@Path("/report-trans-detail-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reportTransDetailList(@QueryParam("id") Long id) throws SQLException {
		return ResponseReturn.sucess(ttrDao.findAllTransInfo(id));

	}

	@GET
	@Path("/multi-trans-personal-info")
	@Produces(MediaType.APPLICATION_JSON)
	public Response multiTransPersonalInfo(@QueryParam("code") String code,@QueryParam("trans_date") String transDate) throws SQLException {
		return ResponseReturn.sucess(ttrDao.findMultiTransByCode(code,transDate));
	}

	@GET
	@Path("/multi-trans-detail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response multiTransDetail(@QueryParam("flagId") Long id) throws SQLException {
		return ResponseReturn.sucess(ttrDao.findMultiTransByFlagId(id));
	}

}
