package com.trustaml.service.ttr.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.ttr.model.Flag;
import com.trustaml.service.ttr.model.Report;

@Stateless
public class TTRDao {

	@Inject
	TTRDaoImpl ttrDaoImpl;

	public Object forwardTTRList() throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.forwardTTRList();
	}

	public Object isScheduleExists() throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.isScheduleExists();
	}

	public Object findById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.findById(id);
	}

	public Object findAll() throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.findAllReport();
	}

	public Object findReportLogById(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.findReportLogById(id);
	}

	public Object findAllTransInfo(Long id) throws SQLException {
		// TODO Auto-generated method stub
		return ttrDaoImpl.findAllTransInfo(id);
	}

	public Object findByCode(String code, String transDate) throws SQLException {
		return ttrDaoImpl.findByCode(code,transDate);
	}

	public Object findMultiTransByCode(String code, String transDate) throws SQLException {
		return ttrDaoImpl.findMultiTransByCode(code,transDate);
	}

	// Multiple Transaction Info
	public Object findMultiTransByFlagId(Long id) throws SQLException {
		return ttrDaoImpl.findMultiTransByFlagId(id);
	}

	public int updateReport(Report report, String username) throws SQLException {
		ttrDaoImpl.updateReport(report);
		ttrDaoImpl.saveReportLog(report, "username");
		return ttrDaoImpl.updateReport(report);
	}

	public int updateFlag(Flag flag, String username) throws SQLException {
		Long reportId = ttrDaoImpl.findReportByDate();
		if (reportId <= 0) {
			reportId = ttrDaoImpl.saveReport(new Report(reportId), "username");
		}
		ttrDaoImpl.saveReportLog(new Report(reportId), "username");
		ttrDaoImpl.updateFlag(flag, reportId);
		return ttrDaoImpl.saveFlagLog(flag, reportId, "username");
	}

	public Object findAllReport() throws SQLException {
		return ttrDaoImpl.findAllReport();
	}

	public int updateFlagBulk(List<Flag> flagList, String username) throws SQLException {
		int rowAffected = 0;
		Long reportId = ttrDaoImpl.findReportByDate();
		if (reportId <= 0) {
			reportId = ttrDaoImpl.saveReport(new Report(reportId), "username");
		}
		for (Flag flag : flagList) {
			rowAffected += ttrDaoImpl.updateFlag(flag, reportId);
			ttrDaoImpl.saveFlagLog(flag, reportId, "username");
		}

		return rowAffected;

	}

	// ============ Write To Database Table ===============

	public void writeToFlagTable(String json, String username)
			throws JsonParseException, JsonMappingException, SQLException, IOException {
		// JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
		// for (Map.Entry<String, JsonElement> obj : jsonObject.entrySet()) {
		// String key = obj.getKey();
		// if (key.equals("data")) {
		// JsonArray jsonArray = jsonObject.getAsJsonArray(key);
		// for (Object entry : jsonArray) {
		// JsonObject jO = (JsonObject) entry;
		// if
		// (jO.get("type").getAsString().equals(ReportTTRConstants.REPORT_SINGLE_TRANSACTION))
		// {
		// JsonArray ttrResults = jO.getAsJsonArray("ttr_result");
		// Flag flag = new Flag(jO.get("name").getAsString(),
		// jO.get("code").getAsString(),
		// jO.get("type").getAsString());
		// if (ttrResults.size() > 0) {
		// for (Object ttrResult : ttrResults) {
		// Long id = ttrDaoImpl.saveFlag(flag, "username");
		// JsonObject ttrResultJsonObject = (JsonObject) ttrResult;
		// String ttrResultString = ttrResultJsonObject.toString();
		// TransInfo transInfo = new ObjectMapper().readValue(ttrResultString,
		// TransInfo.class);
		// ttrDaoImpl.saveTransInfo(transInfo, id);
		// }
		// }
		// } else if
		// (jO.get("type").getAsString().equals(ReportTTRConstants.REPORT_MULTIPLE_TRANSACTION))
		// {
		// JsonArray ttrResults = jO.getAsJsonArray("ttr_result");
		// Flag flag = new Flag(jO.get("name").getAsString(),
		// jO.get("code").getAsString(),
		// jO.get("type").getAsString());
		// if (ttrResults.size() > 0) {
		// for (Object ttrResult : ttrResults) {
		// JsonArray ttrResultJsonArray = (JsonArray) ttrResult;
		// Long id = ttrDaoImpl.saveFlag(flag, "username");
		// for (Object ttrResultJson : ttrResultJsonArray) {
		// JsonObject ttrResultJsonObject = (JsonObject) ttrResultJson;
		// String ttrResultString = ttrResultJsonObject.toString();
		// TransInfo transInfo = new ObjectMapper().readValue(ttrResultString,
		// TransInfo.class);
		// ttrDaoImpl.saveTransInfo(transInfo, id);
		// }
		// }
		// }
		// }
		//
		// }
		// }
		// }
	}
}
