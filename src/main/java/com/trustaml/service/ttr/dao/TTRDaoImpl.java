package com.trustaml.service.ttr.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.ttr.model.Flag;
import com.trustaml.service.ttr.model.Report;
import com.trustaml.service.ttr.model.TransInfo;
import com.trustaml.service.ttr.util.ReportTTRConstants;

public class TTRDaoImpl {

	@Inject
	DBConnection dbConnection;

	public String forwardTTRList() throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_flag(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			con.close();

		}
		return (result == null) ? "[ ]" : result;
	}

	public boolean isScheduleExists() throws SQLException {
		String sql = "select * from ttr_flag where status='rescheduled' and reschedule_date =current_date";
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			rs.close();
			ps.close();
			con.close();
		}
		return false;
	}

	public String findById(Long id) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_flag_trans(?,?)}");
			stmt.setLong(1, id);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();
		}
		return (result == null) ? "[ ]" : result;
	}

	public String findAllReport() throws SQLException {
		String result = "";
		CallableStatement stmt = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_report(?)}");
			stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(1);
		} finally {
			stmt.close();
			con.close();
		}
		return (result == null) ? "[ ]" : result;
	}

	public Object findReportLogById(Long id) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_report_history(?,?)}");
			stmt.setLong(1, id);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();
		}
		return (result == null) ? "[ ]" : result;
	}

	public Object findAllTransInfo(Long id) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_report_detail(?,?)}");
			stmt.setLong(1, id);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			con.close();
			stmt.close();
		}
		return (result == null) ? "[ ]" : result;
	}

	public String findByCode(String code, String transDate) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_flag_trans(?,?,?)}");
			stmt.setString(1, code.toLowerCase());
			stmt.setString(2, transDate);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			stmt.close();
			con.close();
		}
		return result;
	}

	public String findMultiTransByCode(String code, String transDate) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_multi_trans_personal_info(?,?,?)}");
			stmt.setString(1, code);
			stmt.setString(2, transDate);
			stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(3);
		} finally {
			stmt.close();
			con.close();
		}
		return (result == null) ? "[ ]" : result;
	}

	public String findMultiTransByFlagId(Long id) throws SQLException {
		String result = "";
		Connection con = null;
		CallableStatement stmt = null;
		try {
			con = dbConnection.getConnection();
			stmt = con.prepareCall("{call ttr_multi_trans_info(?,?)}");
			stmt.setLong(1, id);
			stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmt.executeUpdate();
			result = stmt.getString(2);
		} finally {
			stmt.close();
			con.close();

		}
		return (result == null) ? "[ ]" : result;
	}

	/*
	 * public int updateFlagBulk(List<Flag> flagList) { int rowAffected = 0; try
	 * { for (Flag flag : flagList) { rowAffected += updateFlag(flag,
	 * "username"); } } catch (Exception e) { e.printStackTrace(); } return
	 * rowAffected; }
	 */

	/*
	 * public int updateFlag(Flag flag, String username) { Optional<LocalDate>
	 * date = flag.getRescheduleDate(); Optional<String> notes =
	 * flag.getNotes(); Long reportNewId = null; Long reportId = null; String
	 * sql =
	 * "update ttr_flag set status = ?, report_id=?, reschedule_date=?, notes=? where id = ?"
	 * ; Connection con = null; PreparedStatement ps = null; try { con =
	 * dbConnection.getConnection(); ps = con.prepareStatement(sql);
	 * ps.setString(1, flag.getStatus()); if
	 * (flag.getStatus().equals(ReportTTRConstants.REPORT_TTR_FORWARD)) {
	 * reportId = findReportByDate(); if (reportId > 0) { reportNewId =
	 * reportId; ps.setLong(2, reportId); } else { reportNewId = saveReport(new
	 * Report(reportId), username); ps.setLong(2, reportNewId); } } else {
	 * ps.setNull(2, java.sql.Types.BIGINT); } if (date.isPresent()) {
	 * ps.setDate(3, java.sql.Date.valueOf(date.get())); } else { ps.setNull(3,
	 * java.sql.Types.DATE); } if (notes.isPresent()) { ps.setString(4,
	 * notes.get()); } else { ps.setNull(4, java.sql.Types.VARCHAR); }
	 * ps.setLong(5, flag.getId()); if (ps.executeUpdate() > 0) { return
	 * saveFlagLog(con, flag, reportNewId, username); } } catch (Exception e) {
	 * e.printStackTrace(); } finally { try { ps.close(); con.close(); } catch
	 * (Exception e) { e.printStackTrace(); } } return 0; }
	 */

	/*
	 * public void writeToFlagTable(JsonObject jsonObject) throws SQLException,
	 * JsonParseException, JsonMappingException, IOException { Connection con =
	 * null; try { con = dbConnection.getConnection();
	 * 
	 * } finally { con.close(); }
	 * 
	 * }
	 */

	public Long saveFlag(Flag flag, String string) throws SQLException {
		String sql = "insert into ttr_flag(flag_type,flag_name, flag_code, maker,checker) values(?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, flag.getType());
			ps.setString(2, flag.getName());
			ps.setString(3, flag.getCode());
			ps.setString(4, "username");
			ps.setString(5, "username");
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				Long id = rs.getLong(1);
				return id;
			}
		} finally {
			ps.close();
		}
		return Long.valueOf(0);
	}

	public void saveTransInfo(TransInfo transInfo, Long id) throws SQLException {
		String sql = "insert into ttr_flag_trans(branch_sol_id,amount,trans_type,trans_id, account_number, trans_date, notes, ttr_flag_id, source_of_fund) values (?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, transInfo.getBranchId());
			ps.setDouble(2, transInfo.getAmount());
			ps.setString(3, transInfo.getTransType());
			ps.setString(4, transInfo.getTransId());
			ps.setString(5, transInfo.getAccountNo());
			ps.setDate(6, Date.valueOf(transInfo.getTransDate()));
			ps.setString(7, transInfo.getDescription());
			ps.setLong(8, id);
			ps.setString(9, transInfo.getSourceFund());
			ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}
	}

	public int updateReport(Report report) throws SQLException {
		String sql = "update ttr_report set status=?, reported_to=?,notes=? where id=?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, report.getStatus());
			ps.setString(2, report.getReportedTo());
			ps.setString(3, report.getNotes());
			ps.setLong(4, report.getId());
			if (ps.executeUpdate() > 0) {
				return ps.executeUpdate();
			}
		} finally {
			ps.close();
			con.close();
		}
		return 0;

	}

	public int updateFlag(Flag flag, Long reportId) {
		Optional<LocalDate> date = flag.getRescheduleDate();
		Optional<String> notes = flag.getNotes();
		String sql = "update ttr_flag set status = ?, report_id=?, reschedule_date=?, notes=? where id = ?";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, flag.getStatus());
			if (flag.getStatus().equals(ReportTTRConstants.REPORT_TTR_FORWARD)) {
				ps.setLong(2, reportId);
			} else {
				ps.setNull(2, java.sql.Types.BIGINT);
			}
			if (date.isPresent()) {
				ps.setDate(3, java.sql.Date.valueOf(date.get()));
			} else {
				ps.setNull(3, java.sql.Types.DATE);
			}
			if (notes.isPresent()) {
				ps.setString(4, notes.get());
			} else {
				ps.setNull(4, java.sql.Types.VARCHAR);
			}
			ps.setLong(5, flag.getId());
			if (ps.executeUpdate() > 0) {
				return ps.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public Long saveReport(Report report, String string) throws SQLException {
		String sql = "insert into ttr_report(maker,checker) values(?,?)";
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, "usernane");
			ps.setString(2, "username");
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				Long id = rs.getLong(1);
				// saveReportLog(con, report, "username");
				return id;
			}
		} finally {
			ps.close();
			con.close();
		}
		return Long.valueOf(0);

	}

	public int saveReportLog(Report report, String username) throws SQLException {
		String sql = "insert into ttr_report_update(ttr_report_id,maker,checker,status,reported_to,notes) values(?,?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, report.getId());
			ps.setString(2, "username");
			ps.setString(3, "username");
			ps.setString(4, report.getStatus());
			ps.setString(5, report.getReportedTo());
			ps.setString(6, report.getNotes());
			return ps.executeUpdate();
		} finally {
			ps.close();
			con.close();
		}

	}

	public int saveFlagLog(Flag flag, Long reportNewId, String username) throws SQLException {
		Optional<LocalDate> date = flag.getRescheduleDate();
		Optional<String> notes = flag.getNotes();
		String sql = "insert into ttr_flag_update(ttr_flag_id,report_id, status,reschedule_date,notes, maker, checker) values(?,?,?,?,?,?,?)";
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, flag.getId());
			if (flag.getStatus().equals(ReportTTRConstants.REPORT_TTR_FORWARD)) {
				ps.setLong(2, reportNewId);
			} else {
				ps.setNull(2, java.sql.Types.BIGINT);
			}
			ps.setString(3, flag.getStatus());
			if (date.isPresent()) {
				ps.setDate(4, java.sql.Date.valueOf(date.get()));
			} else {
				ps.setNull(4, java.sql.Types.DATE);
			}
			if (notes.isPresent()) {
				ps.setString(5, notes.get());
			} else {
				ps.setNull(5, java.sql.Types.VARCHAR);
			}
			ps.setString(6, "username");
			ps.setString(7, "username");
			return ps.executeUpdate();
		} finally {
			ps.close();
		}
	}

	public Long findReportByDate() throws SQLException {
		String sql = "SELECT ttr_report.id FROM ttr_report WHERE created_date > current_date - interval '15' day";
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dbConnection.getConnection();
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("id");
			}
		} finally {
			rs.close();
			ps.close();
			con.close();
		}
		return new Long(0);
	}

}
