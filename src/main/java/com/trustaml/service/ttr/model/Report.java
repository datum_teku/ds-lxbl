package com.trustaml.service.ttr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Report {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("reported_to")
	private String reportedTo;

	@JsonProperty("notes")
	private String notes;

	@JsonProperty("status")
	private String status;

	public Report() {

	}

	public Report(Long id) {
		super();
		this.id = id;
		this.notes = "";
		this.reportedTo = "";
		this.status = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReportedTo() {
		return reportedTo;
	}

	public void setReportedTo(String reportedTo) {
		this.reportedTo = reportedTo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
