package com.trustaml.service.ttr.model;

import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.trustaml.service.ttr.util.LocalDateDeserializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransInfo {

	@JsonProperty("branch_id")
	private String branchId;

	@JsonProperty("amount")
	private Double amount;

	@JsonProperty("trans_type")
	private String transType;

	@JsonProperty("trans_id")
	private String transId;

	@JsonProperty("account_no")
	private String accountNo;

	@JsonProperty("trans_date")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate transDate;

	@JsonProperty("description")
	private String description;

	@JsonProperty("source_of_income")
	private String sourceFund;

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public LocalDate getTransDate() {
		return transDate;
	}

	public void setTransDate(LocalDate transDate) {
		this.transDate = transDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSourceFund() {
		return sourceFund;
	}

	public void setSourceFund(String sourceFund) {
		this.sourceFund = sourceFund;
	}
}
