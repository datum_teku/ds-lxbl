package com.trustaml.service.ttr.util;

public class ReportTTRConstants {

	public static final String REPORT_TTR_FORWARD = "marked_for_reporting";

	public static final String REPORT_SINGLE_TRANSACTION = "single_transaction";

	public static final String REPORT_MULTIPLE_TRANSACTION = "multiple_transactions";
}
