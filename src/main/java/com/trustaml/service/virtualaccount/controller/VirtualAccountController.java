package com.trustaml.service.virtualaccount.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.trustaml.service.common.exception.type.TrustAmlEmptyJSONException;
import com.trustaml.service.common.service.ResponseReturn;
import com.trustaml.service.virtualaccount.dao.VirtualAccountDao;

@Path("/virtual-account")
public class VirtualAccountController {

	@Inject
	VirtualAccountDao virtualAccountDao;

	/**
	 * @param jsonString
	 * @return size of JSON
	 * @description accepts JSON String and save list of JSON also returns size
	 *              of JSON String
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws TrustAmlEmptyJSONException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveVirtualAccount(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, TrustAmlEmptyJSONException, ParseException, NoSuchAlgorithmException {
		if (!jsonString.isEmpty() || !jsonString.equals("")) {
			Integer count = virtualAccountDao.saveVirtualAccount(jsonString);
			return ResponseReturn.sucess(count.toString());
		} else {
			throw new TrustAmlEmptyJSONException("json is empty");
		}
	}
}
