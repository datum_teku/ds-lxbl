package com.trustaml.service.virtualaccount.dao;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trustaml.service.common.HashCodeGenerator;
import com.trustaml.service.common.constant.ConstantStatus;
import com.trustaml.service.screening.legal.dao.ScreeningLegalDaoImpl;
import com.trustaml.service.virtualaccount.model.VirtualAccount;
import com.trustaml.service.virtualaccount.model.VirtualAccountDto;

@Stateless
public class VirtualAccountDao {

	@Inject
	VirtualAccountDaoImpl virtualAccountDaoImpl;

	@Inject
	ScreeningLegalDaoImpl screeningLegalDaoImpl;

	@Inject
	HashCodeGenerator hashCodeGenerator;

	/**
	 * @param jsonString
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 */
	public Integer saveVirtualAccount(String jsonString) throws JsonParseException, JsonMappingException, IOException,
			SQLException, ParseException, NoSuchAlgorithmException {
		VirtualAccountDto virtualAccountDto = new ObjectMapper().readValue(jsonString, VirtualAccountDto.class);
		for (VirtualAccount virtualAccount : virtualAccountDto.getListVirtualAccount()) {
			String hashValue = hashCodeGenerator.generateHashValueForVirtualAccount(virtualAccount);
			virtualAccountDaoImpl.saveVirtualAccount(virtualAccount, hashValue);
			long primaryRequestId = virtualAccountDaoImpl.insertIntoScreeningRequestPrimary(virtualAccount,
					virtualAccountDto.getUser());
			long relatedRequestId = virtualAccountDaoImpl.insertIntoScreeningRequestRelated(virtualAccount,
					virtualAccountDto.getUser());
			virtualAccountDaoImpl.insertIntoScreeningRequestRelated(primaryRequestId, relatedRequestId);
			String status = ConstantStatus.PROCEED_MAKER_CHECKER;

			String screeningLegalWorkFlowHashValue = hashCodeGenerator.generateHashValueForWorkFlowTable(status,
					primaryRequestId, true);
			screeningLegalDaoImpl.insertIntoScreeningWorkFlowTable(status, primaryRequestId,
					screeningLegalWorkFlowHashValue);
		}
		return virtualAccountDto.getListVirtualAccount().size();
	}

}
