package com.trustaml.service.virtualaccount.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import com.trustaml.service.common.database.DBConnection;
import com.trustaml.service.common.dto.User;
import com.trustaml.service.virtualaccount.model.VirtualAccount;

public class VirtualAccountDaoImpl {

	@Inject
	DBConnection dbConnection;

	/**
	 * @param virtualAccount
	 * @param hashValue
	 * @throws SQLException
	 * @throws ParseException
	 */
	public void saveVirtualAccount(VirtualAccount virtualAccount, String hashValue)
			throws SQLException, ParseException {
		String sql = "INSERT INTO nica_virtual_accounts(ref_dr,npr_amount,virtual_acc_no,channel,remitter,reference_no,"
				+ "transfer_amount,date,actual_income,serial_no,trx_date,in_words,"
				+ " ref_no,beneficiary_name,net_amount,branch,gmail,init_date,inr_amount,"
				+ "beneficiary_acc_no,business_date,va_no,hash) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setString(1, virtualAccount.getRefDr());
			ps.setString(2, virtualAccount.getNprAmount());
			ps.setString(3, virtualAccount.getVirtualAccNo());
			ps.setString(4, virtualAccount.getChannel());
			ps.setString(5, virtualAccount.getRemitter());
			ps.setString(6, virtualAccount.getReferenceNo());
			ps.setString(7, virtualAccount.getTransferAmount());
			if (!virtualAccount.getDate().equals("")) {
				ps.setDate(8, convertToSQLDate(virtualAccount.getDate()));
			} else {
				ps.setNull(8, java.sql.Types.DATE);
			}
			ps.setString(9, virtualAccount.getActualIncome());
			ps.setString(10, virtualAccount.getSerialNo());
			if (!virtualAccount.getTrxDate().equals("")) {
				ps.setDate(11, convertToSQLDate(virtualAccount.getTrxDate()));
			} else {
				ps.setNull(11, java.sql.Types.DATE);
			}
			ps.setString(12, virtualAccount.getInWords());
			ps.setString(13, virtualAccount.getRefNo());
			ps.setString(14, virtualAccount.getBeneficiaryName());
			ps.setString(15, virtualAccount.getNetAmount());
			ps.setString(16, virtualAccount.getBranch());
			ps.setString(17, virtualAccount.getGmail());
			if (!virtualAccount.getInitDate().equals("")) {
				ps.setDate(18, convertToSQLDate(virtualAccount.getInitDate()));
			} else {
				ps.setNull(18, java.sql.Types.DATE);
			}
			ps.setString(19, virtualAccount.getInrAmount());
			ps.setString(20, virtualAccount.getBeneficiaryAccNo());
			if (!virtualAccount.getBusinessDate().equals("")) {
				ps.setDate(21, convertToSQLDate(virtualAccount.getBusinessDate()));
			} else {
				ps.setNull(21, java.sql.Types.DATE);
			}
			ps.setString(22, virtualAccount.getVaNo());
			ps.setString(23, hashValue);
			ps.executeUpdate();
		} finally {
			connection.close();
			ps.close();
		}

	}

	/**
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date convertToSQLDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * @param virtualAccount
	 * @param user
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public long insertIntoScreeningRequestPrimary(VirtualAccount virtualAccount, User user)
			throws SQLException, ParseException {
		long requestId = 0;
		String sql = "INSERT INTO screening_l_request (init_date,trx_amount,channel,npr_amount,ref_no,branch,"
				+ "trx_date,purpose_of_screening,name_of_institution,maker,branch_sol_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			if (virtualAccount.getInitDate() != null && !virtualAccount.getInitDate().equals("")) {
				ps.setDate(1, getSQLFormatDate(virtualAccount.getInitDate()));
			} else {
				ps.setNull(1, java.sql.Types.DATE);
			}

			ps.setString(2, virtualAccount.getTransferAmount());
			ps.setString(3, virtualAccount.getChannel());
			ps.setString(4, virtualAccount.getNprAmount());
			ps.setString(5, virtualAccount.getRefNo());
			ps.setString(6, virtualAccount.getBranch());
			if (virtualAccount.getTrxDate() != null && !virtualAccount.getTrxDate().equals("")) {
				ps.setDate(7, getSQLFormatDate(virtualAccount.getTrxDate()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, "Virtual account");
			ps.setString(9, virtualAccount.getRemitter());
			ps.setString(10, user.getUserName());
			ps.setString(11, user.getSolId());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestId = rs.getLong(1);
			}
		} finally

		{
			ps.close();
			connection.close();
		}
		return requestId;
	}

	/**
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	private static java.sql.Date getSQLFormatDate(String date) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = df.parse(date);
		return new java.sql.Date(dt.getTime());
	}

	/**
	 * @param virtualAccount
	 * @param user
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */

	public long insertIntoScreeningRequestRelated(VirtualAccount virtualAccount, User user)
			throws SQLException, ParseException {

		long requestId = 0;
		String sql = "INSERT INTO screening_l_request (benificiary_ac,va_no,"
				+ " net_ammount,inr_amount,ref_dr,reference_no,bus_date,virtual_ac_no,s_no,name_of_institution,maker,branch_sol_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql, new String[] { "id" });
			ps.setString(1, virtualAccount.getBeneficiaryAccNo());
			ps.setString(2, virtualAccount.getVaNo());
			ps.setString(3, virtualAccount.getNetAmount());
			ps.setString(4, virtualAccount.getInrAmount());
			ps.setString(5, virtualAccount.getRefDr());
			ps.setString(6, virtualAccount.getReferenceNo());
			if (virtualAccount.getBusinessDate() != null && !virtualAccount.getBusinessDate().equals("")) {
				ps.setDate(7, getSQLFormatDate(virtualAccount.getBusinessDate()));
			} else {
				ps.setNull(7, java.sql.Types.DATE);
			}
			ps.setString(8, virtualAccount.getVirtualAccNo());
			ps.setString(9, virtualAccount.getSerialNo());
			ps.setString(10, virtualAccount.getBeneficiaryName());
			ps.setString(11, user.getUserName());
			ps.setString(12, user.getSolId());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				requestId = rs.getLong(1);
			}
		} finally {
			ps.close();
			connection.close();
		}
		return requestId;

	}

	/**
	 * @param primaryRequestId
	 * @param relatedRequestId
	 * @throws SQLException
	 */
	public void insertIntoScreeningRequestRelated(long primaryRequestId, long relatedRequestId) throws SQLException {
		String sql = "INSERT INTO screening_l_related (primary_screening_l_id,related_screening_l_id,related_type) VALUES (?,?,?)";
		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = dbConnection.getConnection();
			ps = connection.prepareStatement(sql);
			ps.setLong(1, primaryRequestId);
			ps.setLong(2, relatedRequestId);
			ps.setString(3, "Benificiary");
			ps.executeUpdate();
		} finally {
			ps.close();
			connection.close();
		}

	}

}
