package com.trustaml.service.virtualaccount.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VirtualAccount {

	private long id;

	@JsonProperty("ref_dr")
	private String refDr;

	@JsonProperty("npr_amount")
	private String nprAmount;

	@JsonProperty("virtual_ac_no")
	private String virtualAccNo;

	@JsonProperty("channel")
	private String channel;

	@JsonProperty("remitter")
	private String remitter;

	@JsonProperty("reference_no")
	private String referenceNo;

	@JsonProperty("trx_amount")
	private String transferAmount;

	@JsonProperty("date")
	private String date;

	@JsonProperty("actual_com")
	private String actualIncome;

	@JsonProperty("s_no")
	private String serialNo;

	@JsonProperty("trx_date")
	private String trxDate;

	@JsonProperty("in_words")
	private String inWords;

	@JsonProperty("ref_no")
	private String refNo;

	@JsonProperty("beneficiary_name")
	private String beneficiaryName;

	@JsonProperty("net_amount")
	private String netAmount;

	@JsonProperty("branch")
	private String branch;

	@JsonProperty("dist_list")
	private String gmail;

	@JsonProperty("init_date")
	private String initDate;

	@JsonProperty("inr_amount")
	private String inrAmount;

	@JsonProperty("beneficiary_ac")
	private String beneficiaryAccNo;

	@JsonProperty("bus_date")
	private String businessDate;

	@JsonProperty("va_no")
	private String vaNo;

	public VirtualAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VirtualAccount(long id, String refDr, String nprAmount, String virtualAccNo, String channel, String remitter,
			String referenceNo, String transferAmount, String date, String actualIncome, String serialNo,
			String trxDate, String inWords, String refNo, String beneficiaryName, String netAmount, String branch,
			String gmail, String initDate, String inrAmount, String beneficiaryAccNo, String businessDate,
			String vaNo) {
		super();
		this.id = id;
		this.refDr = refDr;
		this.nprAmount = nprAmount;
		this.virtualAccNo = virtualAccNo;
		this.channel = channel;
		this.remitter = remitter;
		this.referenceNo = referenceNo;
		this.transferAmount = transferAmount;
		this.date = date;
		this.actualIncome = actualIncome;
		this.serialNo = serialNo;
		this.trxDate = trxDate;
		this.inWords = inWords;
		this.refNo = refNo;
		this.beneficiaryName = beneficiaryName;
		this.netAmount = netAmount;
		this.branch = branch;
		this.gmail = gmail;
		this.initDate = initDate;
		this.inrAmount = inrAmount;
		this.beneficiaryAccNo = beneficiaryAccNo;
		this.businessDate = businessDate;
		this.vaNo = vaNo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRefDr() {
		return refDr;
	}

	public void setRefDr(String refDr) {
		this.refDr = refDr;
	}

	public String getNprAmount() {
		return nprAmount;
	}

	public void setNprAmount(String nprAmount) {
		this.nprAmount = nprAmount;
	}

	public String getVirtualAccNo() {
		return virtualAccNo;
	}

	public void setVirtualAccNo(String virtualAccNo) {
		this.virtualAccNo = virtualAccNo;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRemitter() {
		return remitter;
	}

	public void setRemitter(String remitter) {
		this.remitter = remitter;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getActualIncome() {
		return actualIncome;
	}

	public void setActualIncome(String actualIncome) {
		this.actualIncome = actualIncome;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}

	public String getInWords() {
		return inWords;
	}

	public void setInWords(String inWords) {
		this.inWords = inWords;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getGmail() {
		return gmail;
	}

	public void setGmail(String gmail) {
		this.gmail = gmail;
	}

	public String getInitDate() {
		return initDate;
	}

	public void setInitDate(String initDate) {
		this.initDate = initDate;
	}

	public String getInrAmount() {
		return inrAmount;
	}

	public void setInrAmount(String inrAmount) {
		this.inrAmount = inrAmount;
	}

	public String getBeneficiaryAccNo() {
		return beneficiaryAccNo;
	}

	public void setBeneficiaryAccNo(String beneficiaryAccNo) {
		this.beneficiaryAccNo = beneficiaryAccNo;
	}

	public String getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(String businessDate) {
		this.businessDate = businessDate;
	}

	public String getVaNo() {
		return vaNo;
	}

	public void setVaNo(String vaNo) {
		this.vaNo = vaNo;
	}

}
