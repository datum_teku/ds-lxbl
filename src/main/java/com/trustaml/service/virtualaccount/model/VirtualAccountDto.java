package com.trustaml.service.virtualaccount.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trustaml.service.common.dto.User;

public class VirtualAccountDto {

	@JsonProperty("virtual-account")
	private List<VirtualAccount> listVirtualAccount;

	@JsonProperty("user")
	private User user;

	public VirtualAccountDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VirtualAccountDto(List<VirtualAccount> listVirtualAccount, User user) {
		super();
		this.listVirtualAccount = listVirtualAccount;
		this.user = user;
	}

	public List<VirtualAccount> getListVirtualAccount() {
		return listVirtualAccount;
	}

	public void setListVirtualAccount(List<VirtualAccount> listVirtualAccount) {
		this.listVirtualAccount = listVirtualAccount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
